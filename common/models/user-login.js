'use strict';
var ObjectId = require('mongodb').ObjectID;
var bcrypt;
var moment = require('moment');
var status = require('../../utils/statusMessage');
try {
    // Try the native module first
    bcrypt = require('bcrypt');
    // Browserify returns an empty object
    if (bcrypt && typeof bcrypt.compare !== 'function') {
        bcrypt = require('bcryptjs');
    }
} catch (err) {
    // Fall back to pure JS impl
    bcrypt = require('bcryptjs');
}
module.exports = function (UserLogin) {
    UserLogin.doLoginByMPIN = function (userId, mpin, deviceId, cb) {
        var self = this;

        if (mpin === '' || mpin === undefined) {
            var err = new Error('MPIN is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        if (deviceId === '' || deviceId === undefined) {
            var err = new Error('Device Id is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        var UserLoginCollection = self.getDataSource().connector.collection(UserLogin.modelName);
      
        

        UserLoginCollection.aggregate({
            $match: {
                _id: ObjectId(userId),
                mPIN: mpin,
                deviceId: deviceId,
                status: true
            }
        },
            function (err, records) {

                if (err) {
                    console.log(err);
                    return cb(err);
                }
                if (!records) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                if (records.length == 0) {
                    var err = new Error('No Records Found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                if (records.length > 0) {
                    UserLogin.app.models.UserLogin.login({
                        username: records[0].username,
                        password: records[0].viewPassword,
                        mobileNum: records[0].mobileNum
                    },
                        'User',
                        function (err, results) {
                            return cb(false, results);
                        });
                }

            })

    };

    UserLogin.remoteMethod(
        'doLoginByMPIN', {
            description: 'Do Login By MPIN',
            accepts: [{
                arg: 'userId',
                type: 'string'
            }, {
                arg: 'mpin',
                type: 'number'
            }, {
                arg: 'deviceId',
                type: 'string'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    UserLogin.doLoginByWEB = function (username, password, cb) {
        var self = this;
        //Praveen
        if (username === '' || username === undefined) {
            var err = new Error('Username is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        if (password === '' || password === undefined) {
            var err = new Error('Password is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        var UserLoginCollection = self.getDataSource().connector.collection(UserLogin.modelName);

        UserLoginCollection.aggregate({
            $match: {
                username: username,
                viewPassword: password,
                status: true
            }
        },
            function (err, records) {

                if (err) {
                    console.log(err);
                    return cb(err);
                }
                if (!records) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                if (records.length == 0) {
                    var err = new Error('No Records Found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                if (records.length > 0) {
                    UserLogin.app.models.UserLogin.login({
                        username: records[0].username,
                        password: records[0].viewPassword,
                        mobileNum: records[0].mobileNum
                    },
                        'User',
                        function (err, results) {
                            return cb(false, results);
                        });
                }

            })

    };

    UserLogin.remoteMethod(
        'doLoginByWEB', {
            description: 'Do Login By WEB',
            accepts: [{
                arg: 'username',
                type: 'string'
            }, {
                arg: 'password',
                type: 'string'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );
    //-------------------------Praveen Kumar(24-07-2018)----------------------------
    UserLogin.login = function(credentials, include, cb) {
        var self = this;
        if (cb == null) {
            cb = include;
        }
        //Praveen
        if (credentials.username === undefined || credentials.username == "") {
            var err = new Error('Username is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }

        if (credentials.password === undefined || credentials.password == "") {
            var err = new Error('Password is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }

        if (credentials.loginBy === "mob" && (credentials.mobile == "" || credentials.mobile === undefined)) {
            var err = new Error('Mobile Number is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }

        let loginCredential = {}

        if (credentials.loginBy === "mob") {
            loginCredential = {
                //status: true,
                username: credentials.username,
                //mobile: credentials.mobile
            }
        } else {
            loginCredential = {
                //status: true,
                username: credentials.username
            }
        }

        self.findOne({
            where: loginCredential,
            include: [{
                    relation: 'userInfo',
                    scope: {
                        where: {
                            status: true
                        },
                        include: ["blockUserDetail"]
                    }
                },
                {
                    relation: 'userAccess'
                },
                {
                    relation: 'company',
                    scope: {
                        include: ["logo"]
                    }
                },
                {
                    relation: 'image'
                },
 		{
                    relation: 'country'
                },
            ]

        }, function(err, user) {
            if (err) {
                console.log(err);
                return cb(err);
            }
            if (!user) {
                var err = new Error('Oops!!!, Invalid Username or Password.');
                err.statusCode = 401;
                err.code = 'LOGIN_FAILED';
                return cb(err);
            }
            if (user.length == 0) {
                var err = new Error('Oops!!!, Invalid Username or Password.');
                err.statusCode = 401;
                err.code = 'LOGIN_FAILED';
                return cb(err);
            }



            //--------------------------------PK(26-06-2019 12:05PM)-----------------------
            //Condition updated to active and inactive company
            if (JSON.parse(JSON.stringify(user)).company.status == true) {

                if (user.status == true) { 
					//Condition updated to Block or Unblock User
                    const userDetail = JSON.parse(JSON.stringify(user));
					if (!('blockUserDetail' in userDetail.userInfo[0]) || (new Date() < new Date(userDetail.userInfo[0].blockUserDetail.blockDate) && userDetail.userInfo[0].blockUserDetail.releaseDate == null)) {
						//if (!('blockUserDetail' in userDetail.userInfo[0]) || !(userDetail.userInfo[0].blockUserDetail.releaseDate == null)) {
                        //if (!('blockUserDetail' in userDetail.userInfo[0]) || !((new Date(userDetail.userInfo[0].blockUserDetail.blockDate) < new Date()) && (new Date(userDetail.userInfo[0].blockUserDetail.releaseDate) >= new Date()))) {
                        bcrypt.compare(credentials.password, user.password, function(err, isMatch) {
                            if (err) {
                                console.log(err);
                                return fn(err);
                            }
                            if (!isMatch) {
                                var defaultError = new Error('Oops!!!, Invalid Username or Password.');
                                defaultError.statusCode = 401;
                                defaultError.code = 'LOGIN_FAILED';
                                return cb(defaultError);
                            } else {
                                let accesstokens = [];
                                user.createAccessToken(86400, function(err, token) {
                                    if (err)
                                        return cb(err);

                                    if (!accesstokens || accesstokens === undefined) {
                                        accesstokens = [];
                                    }
                                    accesstokens.push(token);
                                    user.accessToken = accesstokens[0];


                                    self.app.models.AccessToken.replaceOrCreate(accesstokens, function(err, updatedAccessToken) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        return cb(false, user);
                                    });
                                });
                            }
                        });
                    } else {
                        var err = new Error(`${userDetail.userInfo[0].blockUserDetail.blockReason}`);
                        err.statusCode = 401;
                        err.code = 'LOGIN_FAILED';
                        return cb(err);
                    }
                } else {
                    var err = new Error('Status In-active, Can Not Login Further.');
                    err.statusCode = 401;
                    err.code = 'LOGIN_FAILED';
                    return cb(err);
                }




            } else {
                var err = new Error('Your Subscription Is Expired, Kindly Contact To Your Head Office');
                err.statusCode = 401;
                err.code = 'LOGIN_FAILED';
                return cb(err);
            }
        })

    };
    //---------------------------------END------------------------------------------------------


    //---------------Rahul-27-12-2018---------------------------------------------------------

     UserLogin.updatedUserStatus = function(params, cb) {
        var self = this;
        var userCollection = this.getDataSource().connector.collection(UserLogin.modelName);
        userCollection.update({
            companyId: ObjectId(params.companyId),
            _id: ObjectId(params.modifyUserId)
        }, {
            $set: {
                updatedAt: new Date(),
                updatedBy: ObjectId(params.updatedBy),
                "status": params.changeTo,
            }
        }, function(err, result) {
            if (err) {
                return err;
            }
            //console.log(result);

            UserLogin.app.models.UserInfo.updateUserStatus(params, function(err, result) {
                if (err) {
                    return cb(err);
                } else {
                    return cb(false, "User Updated Successfully !!!");
                }
            });
        })




    }
    UserLogin.remoteMethod(
        'updatedUserStatus', {
            description: 'Delete user',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )



    //--------------------------modified by preeti arora ---------------
    UserLogin.editUser = function (params, cb) {
        var self = this;
        var userLogEditCollection = this.getDataSource().connector.collection(UserLogin.modelName);
        let desig;
        let desigLevel;

        if (params.designation.designation === undefined) {
            desig = params.designation;
        } else {
            desig = params.designation.designation;
        }

        if (params.designation.designationLevel === undefined) {
            desigLevel = params.designationLevel;
        } else {
            desigLevel = params.designation.designationLevel;
        }
        //-------------------change in userLogin
        var editvalue = {
            _id: params.id,
            name: params.name,
            dateOfBirth: params.dateOfBirth,
            dateOfAnniversary: params.dateOfAnniversary,
            dateOfJoining: params.dateOfJoining,
            address: params.address,
            aadhaar: params.aadhaar,
            mobile: params.mobile,
            email: params.email,
            PAN: params.pan,
            username: params.username,
            designation: desig,
            lockingPeriod: params.lockingPeriod,
            designationLevel: desigLevel,
            reportingDate: params.reportingDate,
            updatedBy: ObjectId(params.updatedBy),
            updatedAt: params.updatedAt
        }
        UserLogin.find({
            where: {
                companyId: ObjectId(params.companyId),
                _id: ObjectId(params.id)
            }
        }, function (err, response) {
            if (response.length > 0) {
                userLogEditCollection.update({
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params.id)
                }, {
                        $set:
                            {
                                name: params.name,
                                dateOfBirth: new Date(params.dateOfBirth),
                                dateOfAnniversary: new Date(params.dateOfAnniversary),
                                dateOfJoining: new Date(params.dateOfJoining),
                                aadhaar: params.aadhaar,
                                address: params.address,
                                mobile: params.mobile,
                                email: params.email,
                                employeeCode:params.employeeCode,
                                PAN: params.pan,
                                lockingPeriod: params.lockingPeriod,
                                username: params.username,
                                status: params.status,
                                designation: desig,
                                designationLevel: desigLevel,
                                updatedBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(),
								 // updateBankInfo
                                bankName: params.bankName,
                                bankAccountNumber: params.bankAccountNumber,
                                ifsc: params.ifsc
								

                            }
                    })

            } else {
                var err = new Error('User cannot be Edit.');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            }
        })
        //-------------------change in userinfo
        var edituserIn = {
            _id: params.id,
            name: params.name,
            companyId: params.companyId,
            status: params.status,
            designation: desig,
            designationLevel: desigLevel,
            lockingPeriod: params.lockingPeriod,
            reportingDate: new Date(params.reportingDate),
            updatedBy: ObjectId(params.updatedBy),
            updatedAt: params.updatedAt,
			blockDate:params.blockDate,
			blockMsg:params.blockMsg,
		   divisionId:params.divisionId //code edit by umesh Dec-19

        }

        UserLogin.app.models.UserInfo.editUserinfo(edituserIn, function (err, result) {
        });

        //-------------------change in Hierarchy-------------- 
        var mgrArray = [];
        var createHier = {};
        let whereObj={
            companyId: params.companyId,
        }
        if(params.designationLevel>1){
            whereObj["supervisorId"]=params.id
               }else{
            whereObj["userId"]=params.id;
         }
        UserLogin.app.models.Hierarchy.find({where:whereObj},function(err,hierarchyRes) {
            if(err){
                console.log(err);
            }
     
                if (params.immediateSupervisorName != null) {
                    for (var i = 0; i < params.immediateSupervisorName.length; i++) {
                        createHier = {
                            companyId: params.companyId,
                            userId: params.id,
                            userName: params.name,
                            userDesignation: params.designation,
                            userDesignationLevel: params.designationLevel,
                            supervisorId: ObjectId(params.immediateSupervisorName[i].id),
                            supervisorName: params.immediateSupervisorName[i].name,
                            supervisorDesignation: params.immediateSupervisorName[i].designation,
                            immediateSupervisorId: ObjectId(params.immediateSupervisorName[i].id),
                            immediateSupervisorName: params.immediateSupervisorName[i].name,
                            supervisorDesignationLevel:params.immediateSupervisorName[i].designationLevel,
                            status: params.status,
                            createdAt: new Date(),
                        }
                        mgrArray.push(createHier);
                    }
        
                    UserLogin.app.models.Hierarchy.createHierarchy(mgrArray, function (err, result) {
                        if (err) {
                            console.log(err);
                            return cb(err);
                        } else {
                            console.log("User has been updated")
                            return cb(false, result)
                        }
                    });
                } else {
                    createHier = {
                        companyId: params.companyId,
                        userId: params.id,
                        userName: params.name,
                        status: params.status,
                        supervisorId: null,
                    }
                    mgrArray.push(createHier);
        
                    UserLogin.app.models.Hierarchy.createHierarchy(mgrArray, function (err, result) {
                        if (err) {
                            console.log(err);
                            return cb(err);
                        } else {
                            return cb(false, result)
                        }
                    });
                }
          if(hierarchyRes.length>0){
                let where={
                    companyId: params.companyId,
                }
                let set={
                    updatedAt:new Date(),
                }
                if(params.designationLevel>1){
                    where["supervisorId"]=params.id
                    set["supervisorName"]=params.name;
                    set["supervisorDesignation"]=params.designation;
                    set["supervisorDesignationLevel"]=params.designationLevel;
                    set["immediateSupervisorId"]= params.id;
                    set["immediateSupervisorName"]=params.name;
                    
                 }else{
                    where["userId"]=params.id;
                    set["userName"]=params.name;
                    set["userDesignationLevel"]=params.designationLevel;
                    set["userDesignation"]=params.designation;
                 }
                 
                UserLogin.app.models.Hierarchy.updateAll(where,set,function(err, res){
                    if(err){
                      console.log(err)
                    }
                    console.log(res);
        
                })

           }
            
        })

    }
    UserLogin.remoteMethod(
        'editUser', {
            description: 'Edit user',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],

            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )

    //---------------------------------------------------------------------------

 //-----------------------------------Preeti and PK Sir(26-04-2019)-------------------------
    UserLogin.makePassword = function (params,cb) {
        var self = this;
        var userCollection = this.getDataSource().connector.collection(UserLogin.modelName);

        UserLogin.find({
            where: {
                companyId: ObjectId(params.companyId),
                //  _id:ObjectId(params.id)
            }
        }, function (err, response) {
            
          for(let i=0;i<response.length;i++){
            //for(let i=0;i<1;i++){

            bcrypt.hash(response[i].viewPassword, 10, function (err, hash) {
                //console.log(i + " -- "+hash);
                userCollection.update({
                    _id : ObjectId(response[i].id)
                },{
                    $set :{
                        password : hash,
                        updatedAt: new Date()
                    }
                },function(err, res){
                    console.log(res);
                    
                })
            });
          }

          return  cb(false, "response")

        })
    }
    UserLogin.remoteMethod(
        'makePassword', {
            description: 'Make User Password From Migrated Data',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
    //-----------------------------------------END-----------------------------------
    //----------------------------Praveen Kumar(03-04-2019)----------------------------
    UserLogin.changePasswordAPI = function (params, cb) {
        var self = this;
        var userLoginCollection = this.getDataSource().connector.collection(UserLogin.modelName);

        if (params.userId === undefined || params.userId == "") {
            var err = new Error('User Id is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }

        if (params.oldPassword === undefined || params.oldPassword == "") {
            var err = new Error('Old Password is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }

        if (params.newPassword === undefined || params.newPassword == "") {
            var err = new Error('New Password is undefined or empty');
            err.statusCode = 401;
            err.code = 'LOGIN_FAILED';
            return cb(err);
        }
        let MAX_PASSWORD_LENGTH = 72;
        let len = Buffer.byteLength(params.newPassword, 'utf8');
        if (len > MAX_PASSWORD_LENGTH) {
            err = new Error('The password entered was too long. Max length is ' + MAX_PASSWORD_LENGTH + '(entered ' + len + ')');
            err.code = 'PASSWORD_TOO_LONG';
            err.statusCode = 422;
            return cb(err);
        }

        this.findById(params.userId, function (err, user) {
            if (err) {
                console.log(err);
                return cb(err);
            }
            if (!user) {
                const err = new Error(`User ${params.userId} not found`);
                err.statusCode = 401;
                err.code = 'USER_NOT_FOUND';
                return cb(err);
            }


            if (user.password && params.oldPassword) {
                bcrypt.compare(params.oldPassword, user.password, function (err, isMatch) {
                    if (err) return cb(false, err);
                    if (!isMatch) {
                        var defaultError = new Error('Invalid Current Password');
                        defaultError.statusCode = 401;
                        defaultError.code = 'INVALID_PASSWORD';
                        return cb(defaultError);
                    }

                    bcrypt.hash(params.newPassword, 10, function (err, hash) {
                        if (err) {
                            return (err);
                        }

                        userLoginCollection.update({
                            _id: ObjectId(params.userId)

                        }, {
                                $set: {
                                    password: hash,
                                    viewPassword: params.newPassword,
                                    updatedAt: new Date()
                                }
                            },
                            function (err, result) {
                                if (err) {
                                    return err;
                                }
                                return cb(false, result.result.n)
                            });
                    });
                });
            } else {
                return cb(false, false)
            }
        });

    }
    UserLogin.remoteMethod(
        'changePasswordAPI', {
            description: 'Change User\'s Password - User Defined API',
            accepts: [{
                arg: 'params',
                type: 'object',
                http: { source: 'body' }
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'post'
            }
        }
    )
    //-------------------------------------END-----------------------------------------
	
	 //--------------------API for scheduling sunday submission-----------------//
     UserLogin.SubmitSundaysOnUserInfo = function (params, cb) {
        var self = this;
        let userInfo = [];
        const date = "";
        date = moment(new Date()).format("YYYY-MM-DD");
        if(dayNum==0){
            date=moment(new Date()).format("YYYY-MM-DD");
        }
        // if(holidayObj.length>0){
        //     date=holidayObj[0].holidayDate
        // }
    
        UserLogin.find({
          where: {
            companyId: {
              inq: params.companyId
            },
            status: true, 
          },
          "include": [{
              "relation": "DCRMaster",
              "scope": {
                where: {
                  dcrDate: date
                }
              }
            }, {
              "relation": "userInfo",
              "scope": {
                // where: {
                //     "designationLevel" : {neq:0}
                //     },
                fields: ["stateId", "districtId","designationLevel"]
              }
            }
    
          ]
        }, function (err, result) {
          var DCRNotSubmitRes = JSON.parse(JSON.stringify(result));
          DCRNotSubmitRes.forEach(element => {
            if (element.DCRMaster.length == 0) { 
                if(element.userInfo[0].designationLevel !=0){
                    userInfo.push({
                     companyId: ObjectId(element.companyId),
                     submitBy: ObjectId(element.userInfo[0].userId),
                     stateId: ObjectId(element.userInfo[0].stateId),
                     districtId: ObjectId(element.userInfo[0].districtId),
                     dcrDate: date
                   })
                }
            }else{
            }
    
          });
    
           UserLogin.app.models.DCRMaster.SubmitSundaysOnDCRMaster(userInfo,function(err, finalResult){
               //return cb(false,finalResult)
          })
    
         })
    
      };

 
      UserLogin.remoteMethod(
        'SubmitSundaysOnUserInfo', {
          description: 'Insert Sundays',
          accepts: [{
            arg: 'params',
            type: 'object'
          }],
          returns: {
            root: true,
            type: 'array'
          },
          http: {
            verb: 'get'
          }
        }
      )

    //--------------------------------------------------
	 //--------------------API for scheduling sunday submission-----------------//
     UserLogin.SubmitSundaysOnUserInfo = function (params, cb) {
        var self = this;
        let userInfo = [];
        var dayNum = moment().day(); //0 is Sunday---
        var date = "";
        date = moment(new Date()).format("YYYY-MM-DD");
        // if(dayNum==0){
        //     date=moment(new Date()).format("YYYY-MM-DD");
        // }
        // if(holidayObj.length>0){
        //     date=holidayObj[0].holidayDate
        // }
    
        UserLogin.find({
          where: {
            companyId: {
              inq: params.companyId
            },
            status: true, 
          },
          "include": [{
              "relation": "DCRMaster",
              "scope": {
                where: {
                  dcrDate: date
                }
              }
            }, {
              "relation": "UserInfo",
              "scope": {
                // where: {
                //     "designationLevel" : {neq:0}
                //     },
                fields: ["stateId", "districtId","designationLevel"]
              }
            }
    
          ]
        }, function (err, result) {

          var DCRNotSubmitRes = JSON.parse(JSON.stringify(result));

        //   DCRNotSubmitRes.forEach(element => {
        //     //if (element.DCRMaster.length == 0) { 
        //         if(element.userInfo[0].designationLevel !=0){
        //             userInfo.push({
        //              companyId: ObjectId(element.companyId),
        //              submitBy: ObjectId(element.userInfo[0].userId),
        //              stateId: ObjectId(element.userInfo[0].stateId),
        //              districtId: ObjectId(element.userInfo[0].districtId),
        //              dcrDate: new Date("24-01-2021")
        //            })
        //         }
        //     // }else{
        //     // }
    
        //   });

    
        //    UserLogin.app.models.DCRMaster.SubmitSundaysOnDCRMaster(userInfo,function(err, finalResult){
        //        //return cb(false,finalResult)
        //   })
    
         })
    
      };

    UserLogin.remoteMethod(
        'SubmitSundaysOnUserInfo', {
            description: 'Insert Sundays',
            accepts: [
                {
                    arg: 'companyId',
                    type: 'string'
                }
            ]
        }
    )

    //--------------------------------------------------

// ERP MASTER APIs Work Start By Ravi on 04/29/2021

    UserLogin.create_or_update = function (params, cb) {
        if (params) {
            if (params.hasOwnProperty('companyCode') && params.hasOwnProperty('stateCode') && params.hasOwnProperty('hqCode') && params.hasOwnProperty('employeeCode') && params.hasOwnProperty('employeeName') && params.hasOwnProperty('designationCode')) {
                UserLogin.app.models.CompanyMaster.findOne({ "where": { erpCode: parseInt(params.companyCode) } }, function (err1, companyId) {
                    if (err1) { return cb(false, status.getStatusMessage(2)) }
                    UserLogin.app.models.State.findOne({ "where": { erpCode: parseInt(params.stateCode) } }, function (err2, stateId) {
                        if (err2) { return cb(false, status.getStatusMessage(2)) }
                        UserLogin.app.models.District.findOne({ "where": { erpCode: parseInt(params.hqCode) } }, function (err3, districtId) {
                            if (err3) { return cb(false, status.getStatusMessage(2)) }
                            if (companyId && stateId && districtId) {
                                UserLogin.findOne({ where: { erpCode: parseInt(params['employeeCode']) } }, function (err4, isEmployeeExits) {
                                    if (err4) { return cb(false, status.getStatusMessage(2)) }
                                    if (isEmployeeExits) {
                                        isEmployeeExits.companyId = ObjectId(isEmployeeExits.companyId);
                                        isEmployeeExits.name = params.employeeName;
                                        isEmployeeExits.age = params.hasOwnProperty('age') ? params.age : isEmployeeExits.age;
                                        isEmployeeExits.gender = params.hasOwnProperty('gender') ? params.gender : isEmployeeExits.gender;
                                        isEmployeeExits.email = params.hasOwnProperty('email') ? params.email : isEmployeeExits.email;
                                        isEmployeeExits.address = params.hasOwnProperty('address') ? params.address : isEmployeeExits.address;
                                        isEmployeeExits.dateOfBirth = params.hasOwnProperty('dateOfBirth') ? new Date(params.dateOfBirth) : isEmployeeExits.dateOfBirth;
                                        isEmployeeExits.dateOfAnniversary = params.hasOwnProperty('dateOfAnniversary') ? new Date(params.dateOfAnniversary) : isEmployeeExits.dateOfAnniversary;
                                        isEmployeeExits.dateOfJoining = params.hasOwnProperty('dateOfJoining') ? new Date(params.dateOfJoining) : isEmployeeExits.dateOfJoining;
                                        isEmployeeExits.aadhaar = params.hasOwnProperty('aadhaar') ? params.aadhaar : isEmployeeExits.aadhaar;
                                        isEmployeeExits.PAN = params.hasOwnProperty('PAN') ? params.PAN : isEmployeeExits.PAN;
                                        isEmployeeExits.DLNumber = params.hasOwnProperty('DLNumber') ? params.DLNumber : isEmployeeExits.DLNumber;
                                        isEmployeeExits.mobile = params.hasOwnProperty('mobile') ? params.mobile : isEmployeeExits.mobile;
                                        UserLogin.replaceOrCreate(isEmployeeExits, function (err, replace) {
                                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                            if (replace) {
                                                UserLogin.app.models.UserInfo.findOne({ "where": { userId: ObjectId(isEmployeeExits.id) } }, function (err, isInfoExists) {
                                                    if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                                    UserLogin.app.models.Designation.findOne({ "where": { erpCode: parseInt(params.designationCode), companyId: ObjectId(companyId.id) } }, function (err, designationUpdate) {
                                                        if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                                        if (isInfoExists && designationUpdate) {
                                                            isInfoExists.companyId = ObjectId(isInfoExists.companyId),
                                                                isInfoExists.companyName = companyId.name,
                                                                isInfoExists.userId = ObjectId(isInfoExists.userId),
                                                                isInfoExists.stateId = ObjectId(isInfoExists.stateId),
                                                                isInfoExists.districtId = ObjectId(isInfoExists.districtId),
                                                                isInfoExists.name = params.employeeName,
                                                                isInfoExists.designation = designationUpdate.designation,
                                                                isInfoExists.designationLevel = designationUpdate.designationLevel
                                                            UserLogin.app.models.UserInfo.replaceOrCreate(isInfoExists, function (err, updateInfo) {
                                                                if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                                                if (updateInfo) {
                                                                    return cb(false, status.getStatusMessage(13))
                                                                }
                                                            })
                                                        }
                                                    });

                                                })
                                                //return cb(false, status.getStatusMessage(13))
                                            }
                                        })
                                    } else {
                                        const empObj = {
                                            "companyId": ObjectId(companyId.id),
                                            "companyName": companyId.name,
                                            "erpCode": parseInt(params.employeeCode),
                                            "name": params.employeeName,
                                            "age": params.hasOwnProperty('age') ? params.age : 0,
                                            "gender": params.hasOwnProperty('gender') ? params.gender : "NA",
                                            "address": params.hasOwnProperty('address') ? params.address : "NA",
                                            "dateOfBirth": params.hasOwnProperty('dateOfBirth') ? new Date(params.dateOfBirth) : new Date(),
                                            "dateOfAnniversary": params.hasOwnProperty('dateOfAnniversary') ? new Date(params.dateOfAnniversary) : new Date(),
                                            "dateOfJoining": params.hasOwnProperty('dateOfJoining') ? new Date(params.dateOfJoining) : new Date(),
                                            "aadhaar": params.hasOwnProperty('aadhaar') ? params.aadhaar : "NA",
                                            "PAN": params.hasOwnProperty('PAN') ? params.PAN : "NA",
                                            "DLNumber": params.hasOwnProperty('DLNumber') ? params.DLNumber : "NA",
                                            "mobile": params.hasOwnProperty('mobile') ? params.mobile : 9999999999,
                                            "email": params.hasOwnProperty('email') ? params.email : params.employeeName.replace(/\s+/g, "") + "@gmail.com",
                                            "username": params.employeeName.replace(/\s+/g, ""),
                                            "viewPassword": params.employeeName.replace(/\s+/g, ""),
                                            "password": params.employeeName.replace(/\s+/g, ""),
                                            "status": true,
                                            "isMPINStatus": false,
                                            "divisionId": "divisionId",
                                            "divisionName": "divisionName",
                                            //"imageId": ObjectId("5ca6a4a5b0980e55a1bb19b3"),
                                            "mPIN": 1234,
                                            //"deviceId": "357110098587401",
                                            "countryId": ObjectId("5d68ce582aaaf707a05e2f9d")
                                        }
                                        UserLogin.replaceOrCreate(empObj, function (err, create) {
                                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                            if (create) {
                                                UserLogin.app.models.Designation.findOne({ "where": { erpCode: parseInt(params.designationCode), companyId: ObjectId(companyId.id) } }, function (err, designation) {
                                                    if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                                    const infoObj = {
                                                        "companyId": ObjectId(companyId.id),
                                                        "companyName": companyId.name,
                                                        "userId": ObjectId(create.id),
                                                        "stateId": ObjectId(stateId.id),
                                                        "districtId": ObjectId(districtId.id),
                                                        "name": params.employeeName,
                                                        "assignDistricts": ["erp"],
                                                        "designation": designation.designation,
                                                        "designationLevel": designation.designationLevel,
                                                        "lockingPeriod": 3,
                                                        "status": true,
                                                        "rL": 1,
                                                        "editLockingPeriod": 3,
                                                        "reportingDate": new Date(),
                                                        "blockDate": new Date(),
                                                        "blockMsg": ""
                                                    }
                                                    UserLogin.app.models.UserInfo.replaceOrCreate(infoObj, function (err, createInfo) {
                                                        if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                                        if (createInfo) {
                                                            return cb(false, status.getStatusMessage(12))
                                                        }
                                                    })
                                                });
                                            }
                                        })
                                    }
                                })
                            } else {
                                return cb(false, status.getStatusMessage(7));
                            }
                        });
                    });
                });
            } else {
                return cb(false, status.getStatusMessage(1));
            }
        } else {
            return cb(false, status.getStatusMessage(0))
        }
    }

    UserLogin.remoteMethod(
        'create_or_update', {
        description: 'Add and Update UserLogin Master',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post'
        }
    });

    //END
};
