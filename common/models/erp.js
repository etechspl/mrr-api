'use strict';
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail');
var moment = require('moment');
var async = require('async');
module.exports = function (Erp) {

    //-------ERP Services call by Ravindra 05-07-2021-------
    /*
    Sales apis starts
    */
    Erp.getPrimaryCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;

        let json = {
            //userID: param.userId,
            FromDate: fromDate,
            ToDate: toDate,
            Hqids: param.Hqids,
            Flag: "PS",
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPrimaryCount' });
                // console.log(err);
            }


        });

    }
    Erp.remoteMethod(
        'getPrimaryCount', {
        description: 'get getPrimaryCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getCollectionCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;
        let json = {
            //userID: param.userId,
            //"month": month,
            //"year": year,
            FromDate: fromDate,
            ToDate: toDate,
            Hqids: param.Hqids,
            Flag: "CL",
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getCollectionCount' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getCollectionCount', {
        description: 'get getCollectionCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getOutstandingCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;

        let json = {
            FromDate: fromDate,
            ToDate: toDate,
            Hqids: param.Hqids,
            Flag: "OS",
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getOutstandingCount' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getOutstandingCount', {
        description: 'get getOutstandingCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getStateWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            stateIDs: param.stateIDs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "StateWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                // console.log(err);
            }

        });
    }
    Erp.remoteMethod(
        'getStateWiseDetails', {
        description: 'getStateWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getHQWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HqIds: hqIds,
                    Flag: param.Flag,
                }
                url = param.APIEndPoint + "HQWiseData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                        // console.log(err);
                    }

                });
            } else {
                return cb(false, [])
            }
        })

    }
    Erp.remoteMethod(
        'getHQWiseDetails', {
        description: 'getHQWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getPartyWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            HQs: param.HQs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "PartyWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            console.log("---", err, "----", res);

            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getPartyWiseDetails', {
        description: 'getPartyWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    /*
    Product wise Sales apis starts
    */
    Erp.getStateWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            stateIDs: param.stateIDs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "StateProductWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                // console.log(err);
            }

        });
    }
    Erp.remoteMethod(
        'getStateWiseProductDetails', {
        description: 'getStateWiseProductDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getHQWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HQIDs: hqIds,
                    Flag: param.Flag
                }
                url = param.APIEndPoint + "HQProductWiseData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                        // console.log(err);
                    }

                });
            } else {
                return cb(false, [])
            }
        })

    }
    Erp.remoteMethod(
        'getHQWiseProductDetails', {
        description: 'getHQWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getPartyWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            HQIDs: param.HQIDs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "PartyProductWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getPartyWiseProductDetails', {
        description: 'getPartyWiseProductDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getERPStateData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "StateDropDown";
        const request = require('request');
        let json = { userID: param.userId };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getERPStateData' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getERPStateData', {
        description: 'getERPStateData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getERPHeadquarterData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "StateHQDropDown";
        const request = require('request');
        let stateIdss = "";
        if (param.StateIds) { stateIdss = param.StateIds }
        let json = { Userid: param.userId, StateIds: stateIdss };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getERPHeadquarterData' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getERPHeadquarterData', {
        description: 'getERPHeadquarterData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getDesignationData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "DesignationDropDown";
        const request = require('request');
        request({
            url: url, // Online url
            method: "GET",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            // json: json
        }, function (err, res, body) {
            let result = JSON.parse(body);
            if (!err) {
                return cb(false, result.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getDesignationData' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getDesignationData', {
        description: 'getDesignationData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Erp.getManagerData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "ManagerDropDown";
        const request = require('request');
        let json = { Designationid: param.Designationid };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getManagerData' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'getManagerData', {
        description: 'getManagerData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    //END

    // MGR and MR Level API for MOBILE APP and WEB ALSO

    /* 
    Total Count api PS/SR/CL/OS
    json parameter {userId:"employeeCodeFromSession",stateIds:"",fromDate:"01-05-2021",toDate:"30-05-2021",Flag:"PS",APIEndPoint:"apiUrlFromSession"} for calling api
    */
    Erp.totalCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalCount";
        // let today = moment();
        // let date = today.format('DD-MM-YYYY');
        // var month = today.format('M');
        // var year = today.format('YYYY');
        // let fromDate = "01-" + month + "-" + year;
        // let toDate = date;
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    //userID: param.userId,
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    Hqids: hqIds,
                    Flag: param.Flag,
                }
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPrimaryCount' });
                        // console.log(err);
                    }

                });

            } else {
                return cb(false, [])
            }
        })

    }
    Erp.remoteMethod(
        'totalCount', {
        description: 'get total count',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    sales apis start
    Headquarterwise Count PS/SR/CL/OS
    json parameter {userId,StateIds,fromDate,toDate,Flag,APIEndPoint} for calling api
    */
    Erp.headquarterWiseCount = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    //userID: param.userId,
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HqIds: hqIds,
                    Flag: param.Flag,
                }
                url = param.APIEndPoint + "HQWiseData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getHqWiseDetails' });
                        // console.log(err);
                    }

                });
            } else {
                return cb(false, [])
            }
        })
    }
    Erp.remoteMethod(
        'headquarterWiseCount', {
        description: 'headquarterWiseCount',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    Partywise Count PS/SR/CL/OS
    json parameter {HQs,fromDate,toDate,Flag,APIEndPoint} for calling api
    */
    Erp.partyWiseCount = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            HQs: param.HQs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "PartyWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            console.log("---", err, "----", res);

            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'partyWiseCount', {
        description: 'partyWiseCount',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    productwise sales apis start
    Hqwise Productwise Count PS/SR/CL/OS
    json parameter {userId,stateIds,fromDate,toDate,Flag,APIEndPoint} for calling api
    */
    Erp.hQWiseProductWiseCount = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        console.log('param',param);
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }

                let json = {
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HQIDs: hqIds,
                    Flag: param.Flag
                }
                url = param.APIEndPoint + "HQProductWiseData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                        // console.log(err);
                    }

                });
            } else {
                return cb(false, [])
            }
        })

    }
    Erp.remoteMethod(
        'hQWiseProductWiseCount', {
        description: 'hQWiseProductWiseCount',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    Partywise Productwise Count PS/SR/CL/OS
    json parameter {HQIDs,fromDate,toDate,Flag,APIEndPoint} for calling api
    */
    Erp.partyWiseProductWiseCount = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = {
            FromDate: param.fromDate,
            ToDate: param.toDate,
            HQIDs: param.HQIDs,
            Flag: param.Flag
        }
        url = param.APIEndPoint + "PartyProductWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'Erp', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    Erp.remoteMethod(
        'partyWiseProductWiseCount', {
        description: 'partyWiseProductWiseCount',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    //Mobile App Reports APIs

    /* 
    Hqwise consolidated report api PS/SR/CL/OS
    json parameter {userId,StateIds,fromDate,toDate,APIEndPoint} for calling api
    */
    Erp.getHQWiseConsolidatedDetails = function (param, cb) {
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HqIds: hqIds
                }
                url = param.APIEndPoint + "HQWiseData";
                async.parallel({
                    primary: (cb) => {
                        json['Flag'] = "PS";
                        request({
                            url: url,
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                                // console.log(err);
                            }
                        });
                    },
                    outstanding: (cb) => {
                        json['Flag'] = "OS";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            // body goes here 
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                            }

                        });

                    },
                    collection: (cb) => {
                        json['Flag'] = "CL";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                            }

                        });
                    },
                    salesreturn: (cb) => {
                        json['Flag'] = "PS";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            // body goes here 
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                                // console.log(err);
                            }

                        });
                    }
                },
                    (err, res) => {
                        if (err) return cb(err);
                        let finalResult = [];
                        let stateCode = [];
                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            res.primary.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            res.outstanding.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                            res.salesreturn.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            res.collection.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        }
                        const StateSet = new Set(stateCode);
                        const values = StateSet.values();
                        const stateCodeArray = Array.from(values);
                        if (stateCodeArray.length > 0) {
                            stateCodeArray.forEach(state => {
                                let primary = 0, outstanding = 0, collection = 0, salesReturn = 0;

                                const stateCode = state.split("@")[0];
                                const stateName = state.split("@")[1];
                                const hqCode = state.split("@")[2];
                                const hqName = state.split("@")[3];
                                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                                    let stateFoundForPrimary = res.primary.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundForPrimary.length > 0) {
                                        primary = stateFoundForPrimary[0].primarysale;
                                    }
                                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                                    let stateFoundSecondary = res.outstanding.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundSecondary.length > 0) {
                                        outstanding = stateFoundSecondary[0].outstandingamount;
                                    }
                                } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                                    let stateFoundSalesReturn = res.salesreturn.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundSalesReturn.length > 0) {
                                        salesReturn = stateFoundSalesReturn[0].salereturn;
                                    }
                                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                                    let stateFoundCollection = res.collection.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundCollection.length > 0) {
                                        collection = stateFoundCollection[0].collectionamount;
                                    }
                                }
                                finalResult.push({
                                    stateCode,
                                    stateName,
                                    hqCode,
                                    hqName,
                                    primary,
                                    outstanding,
                                    collection,
                                    salesReturn
                                })
                            });
                            return cb(false, finalResult)

                        } else {
                            return cb(false, [])
                        }

                    })
            } else {
                return cb(false, [])
            }
        })
    }
    Erp.remoteMethod(
        'getHQWiseConsolidatedDetails', {
        description: 'Get HQ Wise Consolidated Details',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    partywise consolidated report api PS/SR/CL/OS
    json parameter {HQs,fromDate,toDate,APIEndPoint} for calling api
    */
    Erp.getPartyWiseConsolidatedDetails = function (param, cb) {
        let url;
        const request = require('request');
        url = param.APIEndPoint + "PartyWiseData";
        let json = { HQs: param.HQs, FromDate: param.fromDate, ToDate: param.toDate }
        async.parallel({
            primary: (cb) => {
                json['Flag'] = "PS";
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                json['Flag'] = "OS";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }
                });
            },
            collection: (cb) => {
                json['Flag'] = "CL";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                json['Flag'] = "PS";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {

                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = 0, outstanding = 0, collection = 0, salesReturn = 0;
                        const hqCode = state.split("@")[0];
                        const hqName = state.split("@")[1];
                        const partyCode = state.split("@")[2];
                        const partyName = state.split("@")[3];

                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            let stateFoundForPrimary = res.primary.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundForPrimary.length > 0) {
                                primary = stateFoundForPrimary[0].primarysale;
                            }
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].outstandingamount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            let stateFoundSalesReturn = res.salesreturn.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSalesReturn.length > 0) {
                                salesReturn = stateFoundSalesReturn[0].salereturn;
                            }
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].collectionamount;
                            }
                        }
                        finalResult.push({
                            hqCode,
                            hqName,
                            partyCode,
                            partyName,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    Erp.remoteMethod(
        'getPartyWiseConsolidatedDetails', {
        description: 'Get Party Wise Consolidated Details',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    Hqwise product consolidated report api PS/SR/CL/OS
    json parameter {userId,StateIds,fromDate,toDate,APIEndPoint} for calling api
    */
    Erp.getHQWiseConsolidatedProductDetails = function (param, cb) {
        let url;
        const request = require('request');
        let hqIdsArray = [];
        let hqIds;
        let param1 = { userId: param.userId, StateIds: param.StateIds, APIEndPoint: param.APIEndPoint }
        this.getERPHeadquarterData(param1, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        hqIdsArray.push(parseInt(element.code))
                    });
                    hqIds = hqIdsArray.toString();
                } else {
                    hqIds = "";
                }
                let json = {
                    FromDate: param.fromDate,
                    ToDate: param.toDate,
                    HQIDs: hqIds
                }
                url = param.APIEndPoint + "HQProductWiseData";
                async.parallel({
                    primary: (cb) => {
                        json['Flag'] = "PS";
                        request({
                            url: url,
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                                // console.log(err);
                            }
                        });
                    },
                    outstanding: (cb) => {
                        json['Flag'] = "OS";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            // body goes here 
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                            }

                        });

                    },
                    collection: (cb) => {
                        json['Flag'] = "CL";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                            }

                        });
                    },
                    salesreturn: (cb) => {
                        json['Flag'] = "PS";
                        request({
                            url: url, // Online url
                            method: "POST",
                            headers: {
                                // header info - in case of authentication enabled   
                                "Content-Type": "application/json",
                            }, //json        
                            // body goes here 
                            json: json
                        }, function (err, res, body) {
                            if (!err) {
                                cb(null, body.item)
                            } else {
                                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                                // console.log(err);
                            }

                        });
                    }
                },
                    (err, res) => {
                        if (err) return cb(err);
                        let finalResult = [];
                        let stateCode = [];
                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            res.primary.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            res.outstanding.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                            res.salesreturn.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            res.collection.forEach(primData => {
                                stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQName}`)
                            });

                        }
                        const StateSet = new Set(stateCode);
                        const values = StateSet.values();
                        const stateCodeArray = Array.from(values);
                        if (stateCodeArray.length > 0) {
                            stateCodeArray.forEach(state => {
                                let primary = [], outstanding = 0, collection = 0, salesReturn = [], totalPrimary = 0, totalSalesReturn = 0;

                                const stateCode = state.split("@")[0];
                                const stateName = state.split("@")[1];
                                const hqCode = state.split("@")[2];
                                const hqName = state.split("@")[3];
                                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                                    res.primary.forEach(obj => {
                                        if (obj.StateCode == stateCode && obj.HQCode == hqCode) {
                                            totalPrimary += (obj.primarysaleqty * obj.itemrate);
                                            primary.push(
                                                {
                                                    amount: (obj.primarysaleqty * obj.itemrate).toFixed(2),
                                                    productCode: obj.productcode,
                                                    productName: obj.productname,
                                                    qty: obj.primarysaleqty,
                                                })
                                        }
                                    })

                                }
                                if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                                    let stateFoundSecondary = res.outstanding.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundSecondary.length > 0) {
                                        outstanding = stateFoundSecondary[0].outstandingamount;
                                    }
                                }
                                if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {

                                    res.salesreturn.forEach(obj => {
                                        if (obj.HQCode == hqCode) {
                                            totalSalesReturn += (obj.salereturnqty * obj.itemrate);
                                            salesReturn.push(
                                                {
                                                    amount: (obj.salereturnqty * obj.itemrate).toFixed(2),
                                                    productCode: obj.productcode,
                                                    productName: obj.productname,
                                                    qty: obj.salereturnqty,
                                                })
                                        }
                                    })
                                }
                                if (res.hasOwnProperty("collection") && res.collection != undefined) {
                                    let stateFoundCollection = res.collection.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                                    if (stateFoundCollection.length > 0) {
                                        collection = stateFoundCollection[0].collectionamount;
                                    }
                                }
                                totalPrimary = totalPrimary.toFixed(2)
                                totalSalesReturn = totalSalesReturn.toFixed(2)
                                finalResult.push({
                                    stateCode,
                                    stateName,
                                    hqCode,
                                    hqName,
                                    primary,
                                    totalPrimary,
                                    outstanding,
                                    collection,
                                    totalSalesReturn,
                                    salesReturn
                                })
                            });
                            return cb(false, finalResult)

                        } else {
                            return cb(false, [])
                        }

                    })
            } else {
                return cb(false, [])
            }
        });
    }
    Erp.remoteMethod(
        'getHQWiseConsolidatedProductDetails', {
        description: 'Get HQ Wise Consolidated Product Details',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /* 
    partywise product consolidated report api PS/SR/CL/OS
    json parameter {HQIDs,fromDate,toDate,APIEndPoint} for calling api
    */
    Erp.getPartyWiseConsolidatedProductDetails = function (param, cb) {
        let url;
        const request = require('request');
        url = param.APIEndPoint + "PartyProductWiseData";
        let json = { HQIDs: param.HQIDs, FromDate: param.fromDate, ToDate: param.toDate }
        async.parallel({
            primary: (cb) => {
                json['Flag'] = "PS";
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                json['Flag'] = "OS";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }
                });
            },
            collection: (cb) => {
                json['Flag'] = "CL";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                json['Flag'] = "PS";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {

                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQName}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = [], outstanding = 0, collection = 0, salesReturn = [], totalPrimary = 0, totalSalesReturn = 0;
                        const hqCode = state.split("@")[0];
                        const hqName = state.split("@")[1];
                        const partyCode = state.split("@")[2];
                        const partyName = state.split("@")[3];

                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            res.primary.forEach(obj => {
                                if (obj.HQCode == hqCode && obj.PartyCode == partyCode) {
                                    totalPrimary += (obj.primarysaleqty * obj.itemrate);
                                    primary.push(
                                        {
                                            amount: (obj.primarysaleqty * obj.itemrate).toFixed(2),
                                            productCode: obj.productcode,
                                            productName: obj.productname,
                                            qty: obj.primarysaleqty,
                                        })
                                }
                            })
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].outstandingamount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            res.salesreturn.forEach(obj => {
                                if (obj.HQCode == hqCode && obj.PartyCode == partyCode) {
                                    totalSalesReturn += (obj.salereturnqty * obj.itemrate);
                                    salesReturn.push(
                                        {
                                            amount: (obj.salereturnqty * obj.itemrate).toFixed(2),
                                            productCode: obj.productcode,
                                            productName: obj.productname,
                                            qty: obj.salereturnqty,
                                        })
                                }
                            })
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].collectionamount;
                            }
                        }
                        totalPrimary = totalPrimary.toFixed(2)
                        totalSalesReturn = totalSalesReturn.toFixed(2)
                        finalResult.push({
                            hqCode,
                            hqName,
                            partyCode,
                            partyName,
                            totalPrimary,
                            totalSalesReturn,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    Erp.remoteMethod(
        'getPartyWiseConsolidatedProductDetails', {
        description: 'Get Party Wise Consolidated Product Details',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    //END

};
