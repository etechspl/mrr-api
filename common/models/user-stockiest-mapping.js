var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail')
'use strict';

module.exports = function (Userstockiestmapping) {
    Userstockiestmapping.getUserStockistDetail = function (param, cb) {
        var self = this;
        var UserStockistInfoCollection = self.getDataSource().connector.collection(Userstockiestmapping.modelName);
        let districtIds = [];
        let userIds = [];

        for (let i = 0; i < param.districts.length; i++) {
            districtIds.push(ObjectId(param.districts[i]));
        }
        for (let j = 0; j < param.employeeIds.length; j++) {
            userIds.push(ObjectId(param.employeeIds[j]));
        }

        UserStockistInfoCollection.aggregate(
            // Stage 1
            {
                $match: {
                    companyId: ObjectId(param.companyId),
                    stkDistrictId: { $in: districtIds },
                    userId: { $in: userIds },
					status:true
                }
            },

            // Stage 2
            {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "UserData"
                }
            },

            // Stage 3
            {
                $lookup: {
                    "from": "District",
                    "localField": "stkDistrictId",
                    "foreignField": "_id",
                    "as": "District"
                }
            },

            // Stage 4
            {
                $unwind: "$UserData"
            },

            // Stage 5
            {
                $unwind: "$District"
            },

            // Stage 6
            {
                $project: {
                    stockistName: "$stkName",
                    districtName: "$District.districtName",
                    userName: "$UserData.name"
                }
            },
            function (err, response) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Userstockiestmapping', errorObject: err, paramsObject: param, methodName: 'getUserStockistDetail' });
                }
                if (response.length > 0) {
                    return cb(false, response);
                } else {
                    return cb(false, []);
                }
            });

    };

    Userstockiestmapping.remoteMethod(
        'getUserStockistDetail', {
            description: 'get User Id On District Basis',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Userstockiestmapping.deleteMappedStockists = function (param, cb) {
        var self = this;
        var UserStockistInfoCollection = self.getDataSource().connector.collection(Userstockiestmapping.modelName);
        let stockistIds = [];

        for (let i = 0; i < param.stockist.length; i++) {
            stockistIds.push(ObjectId(param.stockist[i]._id));
        }

        UserStockistInfoCollection.remove({
            _id: { $in: stockistIds }
        }, function (err, result) {
            if(err){
                sendMail.sendMail({ collectionName: 'Userstockiestmapping', errorObject: err, paramsObject: param, methodName: 'deleteMappedStockists' });
            }
            var record = [];
            record.push({
                result: result.result
            });
            return cb(false, record)
        });
    };

    Userstockiestmapping.remoteMethod(
        'deleteMappedStockists', {
            description: 'deleteStockists',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Userstockiestmapping.getMappedStockistForFilter = function (param, cb) {
        var self = this;
        var UserStockistInfoCollection = self.getDataSource().connector.collection(Userstockiestmapping.modelName);
        let userIds = [];
        for (let i = 0; i < param.userId.length; i++) {
            userIds.push(ObjectId(param.userId[i]));
        }
        UserStockistInfoCollection.aggregate(
            // Stage 1
            {
                $match: {
                    companyId: ObjectId(param.companyId),
                    userId: {$in:userIds},
					status:true
                }
            },
            // Stage 2
    {
        $group: {
        _id:{
          stkId:"$stkId",
          stkName:"$stkName",
          userId:"$userId"
        }
        }
      },
  
      // Stage 3
      {
        $lookup: {
            "from" : "Providers",
            "localField" : "_id.stkId",
            "foreignField" : "_id",
            "as" : "providers"
        }
      },
  
      // Stage 4
      {
        $project: {
            _id:0,
        stkId:"$_id.stkId",
        stkName:"$_id.stkName",
        stkCode: { $arrayElemAt: ["$providers.providerCode", 0] }
        }
      },
            function (err, response) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Userstockiestmapping', errorObject: err, paramsObject: param, methodName: 'getMappedStockistForFilter' });
                }
                if (response.length > 0) {
                    return cb(false, response);
                } else {
                    return cb(false, []);
                }
            });

    };

    Userstockiestmapping.remoteMethod(
        'getMappedStockistForFilter', {
            description: 'get User Id On Employee Basis',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );


};
