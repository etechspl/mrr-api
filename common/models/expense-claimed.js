'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var asyncLoop = require('node-async-loop');

module.exports = function(Expenseclaimed) {
    Expenseclaimed.getUserWiseExpense = function(params, cb) {
        
        let self = this
        let parameters = params;
        let ExpenseClaimedAggregate = self.getDataSource().connector.collection(Expenseclaimed.modelName);
        async.parallel({
            expense: function(cb) {
                Expenseclaimed.app.models.UserInfo.getUserIds(params, function(err, userResult) {
                    if(err){
                        sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getUserWiseExpense'});
                    }
                    ExpenseClaimedAggregate.aggregate({
                        $match: {
                            companyId: ObjectId(params.companyId),
                            dcrDate: {
                                $gte: new Date(params.startDate),
                                $lte: new Date(params.endDate)
                            },
                            userId: {
                                $in: userResult[0].userIds
                            }
                        }
                    }, {
                        $group: {
                            _id: {
                                "userId": "$userId"
                            },
                            distance: {
                                $sum: "$distance"
                            },
                            fare: {
                                $sum: "$fare"
                            },
                            miscExpense: {
                                $sum: "$miscExpense"
                            },
                            da: {
                                $sum: "$da"
                            },
                            daMgr: {
                                $sum: "$daMgr"
                            },
                            daAdmin: {
                                $sum: "$daAdmin"
                            },
                            distanceMgr: {
                                $sum: "$distanceMgr"
                            },
                            distanceAdmin: {
                                $sum: "$distanceAdmin"
                            },
                            fareMgr: {
                                $sum: "$fareMgr"
                            },
                            fareAdmin: {
                                $sum: "$fareAdmin"
                            },
                            miscExpenseMgr: {
                                $sum: "$miscExpenseMgr"
                            },
                            miscExpenseAdmin: {
                                $sum: "$miscExpenseAdmin"
                            },
                            totalExpense: {
                                $sum: "$totalExpense"
                            },
                            totalExpenseMgr: {
                                $sum: "$totalExpenseMgr"
                            },
                            totalExpenseAdmin: {
                                $sum: "$totalExpenseAdmin"
                            },
                            appByMgr: {
                                $addToSet: "$appByMgr"
                            },
                            appByAdm: {
                                $addToSet: "$appByAdm"
                            }
                        }
                    }, function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getUserWiseExpense'});
                            // console.log(err);
                            cb(false, err);
                        }
                        cb(false, result)
                    });
                })
            },
            userInfo: function(cb) {
                let divisionId = [];

                let where = {
                    companyId: params.companyId,
                    status: {
                        inq: params.status
                    },
                    designationLevel: {
                        neq: 0
                    }
                };
                
                if (params.isDivisionExist == true) {
                  for (var i = 0; i < params.division.length; i++) {
                    divisionId.push(ObjectId(params.division[i]));
                  }
                  where.divisionId = { inq: divisionId };
                }

                if (params["type"].toLowerCase() === "state") {
                    where.stateId = {
                        inq: params.stateId
                    }
                } else if (params["type"].toLowerCase() === 'headquarter') {

                    where.stateId = {
                        inq: params.stateId
                    }

                    where.districtId = {
                        inq: params.districtIds
                    }
                } else if (params["type"].toLowerCase() === 'employee wise') {
                    where.designation = params.designation;
                }
                if (params.rL === 0) {
                    Expenseclaimed.app.models.UserInfo.find({
                        where: where,
                        fields: {
                            name: 1,
                            designation: 1,
                            userId: 1,
                            divisionId: 1,
                            daCategory: 1
                        },
                        order: ['designationLevel DESC', 'name ASC']

                    }, function(err, result) {
                        if(err){
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getUserWiseExpense'});
                        }
                        cb(false, result);
                    })
                } else if (params.rL > 1) {
                    Expenseclaimed.app.models.Hierarchy.getManagerHierarchyInArrayBasedOnStateOrDistrictOrDesignationwise(params, function(err, result) {
                        if (result.length > 0) {
                            where["userId"] = {
                                inq: result[0].userIds
                            }
                            Expenseclaimed.app.models.UserInfo.find({
                                where: where,
                                fields: {
                                    name: 1,
                                    designation: 1,
                                    userId: 1,
                                    divisionId: 1,
                                    daCategory: 1
                                },
                                order: ['designationLevel DESC', 'name ASC']
                            }, function(err, result) {
                                if(err){
                                    sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getUserWiseExpense'});
                                }
                                cb(false, result);
                            })
                        }
                    })

                }

            }
        }, function(err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getUserWiseExpense'});
            }
            // console.log("===================================");
            // console.log("", response);
            // console.log("====================================");
            let finalResult = [];
            for (let i = 0; i < response.userInfo.length; i++) {
                finalResult.push({
                    ...response.userInfo[i],
                    ...(response.expense.find((itmInner) => itmInner._id.userId.toString() === response.userInfo[i].userId.toString()))
                });
            }

            cb(false, finalResult)

        });
    }

    Expenseclaimed.remoteMethod(
        'getUserWiseExpense', {
            description: 'getUserWiseExpense',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        })
		//---------------------get consolidated expense claimed by preeti arora 24-09-2019---------\
        
    Expenseclaimed.getConsolidatedExpense = function (params, cb) {
        let self = this
        let parameters = params;
        // console.log("parameters in expese clamied--",typeof(params.companyId));
        let where = {
          companyId: ObjectId(params.companyId),
            dcrDate: {
                $gte: new Date(params.startDate),
                $lte: new Date(params.endDate)
            }
        };
        let stateIds=[];
        let hqIds=[];
        let userIds=[];
      
        let ExpenseClaimedAggregate = self.getDataSource().connector.collection(Expenseclaimed.modelName);
        if (params["type"].toLowerCase() === "state") {
            for(var i=0;i<params.stateId.length;i++){
                stateIds.push(ObjectId(params.stateId[i]))
            }
            where.stateId = {$in:stateIds}

        } else if (params["type"].toLowerCase() === 'headquarter') {
            for(var i=0;i<params.stateId.length;i++){
                stateIds.push(ObjectId(params.stateId[i]))
            }
            for(var i=0;i<params.districtIds.length;i++){
                hqIds.push(ObjectId(params.districtIds[i]))
            }
            where.stateId = {$in:stateIds}
            where.districtId =  {$in:hqIds}  
            
        } else if (params["type"].toLowerCase() === 'employee wise') {
            for(var i=0;i<params.userId.length;i++){
                userIds.push(ObjectId(params.userId[i]))
            }
            where.userId =userIds;
        }
        ExpenseClaimedAggregate.aggregate(

                // Stage 1
                {
                    $match: where
                },
             // Stage 2
               {
                $group: {
                    _id: {
                            userId: "$userId",
                            appAdmDate: "$appAdmDate"
                        },
                        claimedExpense: {
                            $sum: "$fare"
                        },
                        approvedExpenseByAdmin: {
                            $sum: "$fareAdmin"
                        },
                        approvedExpenseByMgr: {
                            $sum: "$fareMgr"
                        },
                        totalExpense: {
                            $sum: "$totalExpense"
                        }
                    }},

                // Stage 3
                {
                    $lookup: {
                        "from": "UserInfo",
                        "localField": "_id.userId",
                        "foreignField": "userId",
                        "as": "loginData"
                    }
                },

                // Stage 4
                {
                    $unwind: "$loginData"
                },

                // Stage 5
                {
                    $lookup: {
                        "from": "District",
                        "localField": "loginData.districtId",
                        "foreignField": "_id",
                        "as": "disData"
                    }
                },

                // Stage 6
                {
                    $unwind: "$disData"
                },

                // Stage 7
                {
                    $lookup: {
                        "from": "State",
                        "localField": "disData.stateId",
                        "foreignField": "_id",
                        "as": "stateData"
                    }
                },

                // Stage 8
                {
                    $unwind: "$stateData"
                },

                // Stage 9
                {
                    $project: {
                        name: "$loginData.name",
                        state: "$stateData.stateName",
                        district: "$disData.districtName",
                        designation: "$loginData.designation",
                        claimedExpense: 1,
                        approvedExpenseByAdmin: 1,
                        approvedExpenseByMgr: 1,
                        AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                        appAdmDate: "$_id.appAdmDate",
                        totalExpense: 1


                    }
                },
            function (err, result) {
                // console.log("claimed Result=",result)
                if (err) {
                    sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpense'});
                    return cb(err);
                }
                return cb(false, result);
            }

        );


    },

        Expenseclaimed.remoteMethod(
            'getConsolidatedExpense', {
                description: 'getConsolidatedExpense',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            })
    //-----------------------------------------------------------------------
	 //-------------------------------------Praveen Kumar(22-07-2019)-----------------------------
    Expenseclaimed.updateDetails = function(where, data, cb) {
        var self = this;
        const ExpenseclaimedCollection = self.getDataSource().connector.collection(Expenseclaimed.modelName);
        ExpenseclaimedCollection.update(where, {
            $set: data
        }, function(err, ExpenseclaimedResult) {
            if (err) {
                sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpense'});
                // console.log(err)
            }
            cb(false, ExpenseclaimedResult.result)
        })

    }

    Expenseclaimed.remoteMethod(
        'updateDetails', {
            description: 'update expense details',
            accepts: [{
                arg: 'where',
                type: 'object'
            }, {
                arg: 'data',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //--------------------------------------------END--------------------------------------------

	//--------------------Praveen Kumar(2019-12-19)---------------------------------
	// by ravi override of pk method given by pk
	 Expenseclaimed.getConsolidatedExpenseNew = function(params, cb) {
        let self = this
            //// console.log(params);

        const ExpenseClaimedAggregate = self.getDataSource().connector.collection(Expenseclaimed.modelName);
        let where = {
            companyId: ObjectId(params.companyId),
            dcrDate: {
                $gte: new Date(params.dateRange.begin),
                $lte: new Date(params.dateRange.end)
                    //$gte: new Date("2019-06-01"),
                    //$lte: new Date("2019-06-30")
            }
        };
        const dynamicSort = {
            claimedExpense: -1,
            state: 1,
            district: 1,
            designationLevel: 1,
            name: 1
        }

        let stateIds = [];
        let hqIds = [];
        let userIds = [];

        Expenseclaimed.app.models.Expenses.findOne({
            where: {
                companyId: params.companyId
            }
        }, function(err, expeseHead) {
            if(err){
                sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
            }
            // console.log("Result : ", expeseHead);




            // // console.log('============================');
            // // console.log(dynamicGroup);
            // // console.log('============================');
            // // console.log(dynamicProject);


            if (params.type === "State" && params.lookingFor == "consolidated") {
                let dynamicGroup = {
                    _id: {
                        stateId: "$stateId"
                    },
                    claimedExpense: {
                        $sum: "$totalExpense"
                    },
                    approvedExpenseByAdmin: {
                        $sum: "$totalExpenseAdmin"
                    },
                    approvedExpenseByMgr: {
                        $sum: "$totalExpenseMgr"
                    },
                    numberOfClaims: {
                        $addToSet: "$userId"
                    },
                    sumExpAdm: {
                        $sum: "$sumExpAdm"
                    },
                    subtExpAdm: {
                        $sum: "$subtExpAdm"
                    }
                };
                let dynamicProject = {
                    state: { $arrayElemAt: ["$stateData.stateName", 0] },
                    claimedExpense: 1,
                    approvedExpenseByAdmin: 1,
                    approvedExpenseByMgr: 1,
                    AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                    numberOfClaims: {
                        $size: "$numberOfClaims"
                    },
                    sumExpAdm: 1,
                    subtExpAdm: 1
                }

                for (const exp of expeseHead.expenses) {
                    const newExpHead = {
                        $sum: `$miscExpSelf.${exp.replace(/\s/g,'')}`
                    }
                    const newExpHeadForAdmin = {
                        $sum: `$miscExpADM.${exp.replace(/\s/g,'')}`
                    }

                    dynamicGroup = {
                        ...dynamicGroup,
                        [`${exp.replace(/\s/g,'')}`]: newExpHead,
                        [`Adm${exp.replace(/\s/g,'')}`]: newExpHeadForAdmin

                    }

                    dynamicProject = {
                        ...dynamicProject,
                        [`${exp.replace(/\s/g,'')}`]: 1,
                        [`Ded${exp.replace(/\s/g,'')}`]: { $subtract: [`$${exp.replace(/\s/g,'')}`, `$Adm${exp.replace(/\s/g,'')}`] },
                        expenseHead: { $literal: expeseHead.expenses }
                    }
                }


                for (let i = 0; i < params.stateIds.length; i++) {
                    stateIds.push(ObjectId(params.stateIds[i]))
                }
                where.stateId = { $in: stateIds }

                ExpenseClaimedAggregate.aggregate({
                        $match: where
                    }, {
                        $group: dynamicGroup
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        }
                    }, {
                        $project: dynamicProject
                    }, {
                        $sort: dynamicSort
                    },
                    function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );

            } else if (params.type === "State" && params.lookingFor == "employeeWise") {
                let dynamicGroup = {
                    _id: {
                        userId: "$userId"
                    },
                    claimedExpense: {
                        $sum: "$totalExpense"
                    },
                    approvedExpenseByAdmin: {
                        $sum: "$totalExpenseAdmin"
                    },
                    approvedExpenseByMgr: {
                        $sum: "$totalExpenseMgr"
                    },
                    sumExpAdm: {
                        $sum: "$sumExpAdm"
                    },
                    subtExpAdm: {
                        $sum: "$subtExpAdm"
                    },
                    claimedDA: {
                        $sum: "$da"
                    },
                    claimedTA: {
                        $sum: "$fare"
                    }

                };
                let dynamicProject = {
                    name: "$userData.name",
                    state: { $arrayElemAt: ["$stateData.stateName", 0] },
                    district: { $arrayElemAt: ["$disData.districtName", 0] },
                    designation: "$userData.designation",
                    designationLevel: "$userData.designationLevel",
                    joiningDate: "$userData.reportingDate",
                    claimedExpense: 1,
                    approvedExpenseByAdmin: 1,
                    approvedExpenseByMgr: 1,
                    AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                    sumExpAdm: 1,
					division:"$divisionData.divisionName",
                    subtExpAdm: 1,
                    claimedDA: 1,
                    claimedTA: 1
                }

                for (const exp of expeseHead.expenses) {
                    const newExpHead = {
                        $sum: `$miscExpSelf.${exp.replace(/\s/g,'')}`
                    }
                    const newExpHeadForAdmin = {
                        $sum: `$miscExpADM.${exp.replace(/\s/g,'')}`
                    }

                    dynamicGroup = {
                        ...dynamicGroup,
                        [`${exp.replace(/\s/g,'')}`]: newExpHead,
                        [`Adm${exp.replace(/\s/g,'')}`]: newExpHeadForAdmin

                    }

                    dynamicProject = {
                        ...dynamicProject,
                        [`${exp.replace(/\s/g,'')}`]: 1,
                        [`Ded${exp.replace(/\s/g,'')}`]: { $subtract: [`$${exp.replace(/\s/g,'')}`, `$Adm${exp.replace(/\s/g,'')}`] },
                        expenseHead: { $literal: expeseHead.expenses }
                    }
                }

                for (let i = 0; i < params.stateIds.length; i++) {
                    stateIds.push(ObjectId(params.stateIds[i]))
                }
                where.stateId = { $in: stateIds }
                   let match={}
                

                if(params.isDivisionExist==true){
                    let divisionId=[];
                    for(var i=0;i<params.divisionIds.length;i++){
                        divisionId.push(ObjectId(params.divisionIds[i]))
                    }
                     match["userData.divisionId"]={$in:divisionId}
                }
                ExpenseClaimedAggregate.aggregate({
                        $match: where
                    }, {
                        $group: dynamicGroup
                    }, {
                        $lookup: {
                            "from": "UserInfo",
                            "localField": "_id.userId",
                            "foreignField": "userId",
                            "as": "userData"
                        }
                    }, {
                        $unwind: "$userData"
                    },{
						$unwind:"$userData.divisionId"
					},{
						$match:match
					}, {
                        $lookup: {
                            "from": "State",
                            "localField": "userData.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "userData.districtId",
                            "foreignField": "_id",
                            "as": "disData"
                        }
                    },{
                        $lookup: {
                            "from": "DivisionMaster",
                            "localField": "userData.divisionId",
                            "foreignField": "_id",
                            "as": "divisionData"
                        }
                    }, {
                        $unwind: "$divisionData"
                    }, {
                        $project: dynamicProject
                    }, {
                        $sort: dynamicSort
                    },
                    function(err, result) {
						// console.log("result -",result )
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );

            } else if (params.type === 'Headquarter' && params.lookingFor == "consolidated") {
                let dynamicGroup = {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId"
                    },
                    claimedExpense: {
                        $sum: "$totalExpense"
                    },
                    approvedExpenseByAdmin: {
                        $sum: "$totalExpenseAdmin"
                    },
                    approvedExpenseByMgr: {
                        $sum: "$totalExpenseMgr"
                    },
                    numberOfClaims: {
                        $addToSet: "$userId"
                    },
                    sumExpAdm: {
                        $sum: "$sumExpAdm"
                    },
                    subtExpAdm: {
                        $sum: "$subtExpAdm"
                    }

                };
                let dynamicProject = {
                    name: "$loginData.name",
                    state: { $arrayElemAt: ["$stateData.stateName", 0] },
                    district: { $arrayElemAt: ["$disData.districtName", 0] },
                    claimedExpense: 1,
                    approvedExpenseByAdmin: 1,
                    approvedExpenseByMgr: 1,
                    AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                    numberOfClaims: {
                        $size: "$numberOfClaims"
                    },
                    sumExpAdm: 1,
                    subtExpAdm: 1
                }

                for (const exp of expeseHead.expenses) {
                    const newExpHead = {
                        $sum: `$miscExpSelf.${exp.replace(/\s/g,'')}`
                    }
                    const newExpHeadForAdmin = {
                        $sum: `$miscExpADM.${exp.replace(/\s/g,'')}`
                    }

                    dynamicGroup = {
                        ...dynamicGroup,
                        [`${exp.replace(/\s/g,'')}`]: newExpHead,
                        [`Adm${exp.replace(/\s/g,'')}`]: newExpHeadForAdmin

                    }

                    dynamicProject = {
                        ...dynamicProject,
                        [`${exp.replace(/\s/g,'')}`]: 1,
                        [`Ded${exp.replace(/\s/g,'')}`]: { $subtract: [`$${exp.replace(/\s/g,'')}`, `$Adm${exp.replace(/\s/g,'')}`] },
                        expenseHead: { $literal: expeseHead.expenses }
                    }
                }

                for (let i = 0; i < params.stateIds.length; i++) {
                    stateIds.push(ObjectId(params.stateIds[i]))
                }
                for (let i = 0; i < params.districtIds.length; i++) {
                    hqIds.push(ObjectId(params.districtIds[i]))
                }
                where.stateId = { $in: stateIds }
                where.districtId = { $in: hqIds }


                ExpenseClaimedAggregate.aggregate({
                        $match: where
                    }, {
                        $group: dynamicGroup
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "_id.districtId",
                            "foreignField": "_id",
                            "as": "disData"
                        }
                    }, {
                        $project: dynamicProject
                    }, {
                        $sort: dynamicSort
                    },
                    function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );

            } else if (params.type === 'Headquarter' && params.lookingFor == "employeeWise") {
                let dynamicGroup = {
                    _id: {
                        userId: "$userId"
                    },
                    claimedExpense: {
                        $sum: "$totalExpense"
                    },
                    approvedExpenseByAdmin: {
                        $sum: "$totalExpenseAdmin"
                    },
                    approvedExpenseByMgr: {
                        $sum: "$totalExpenseMgr"
                    },
                    sumExpAdm: {
                        $sum: "$sumExpAdm"
                    },
                    subtExpAdm: {
                        $sum: "$subtExpAdm"
                    },
                    claimedDA: {
                        $sum: "$da"
                    },
                    claimedTA: {
                        $sum: "$fare"
                    }
                };
                let dynamicProject = {
                    name: "$userData.name",
                    state: { $arrayElemAt: ["$stateData.stateName", 0] },
                    district: { $arrayElemAt: ["$disData.districtName", 0] },
                    designation: "$userData.designation",
                    designationLevel: "$userData.designationLevel",
                    joiningDate: "$userData.reportingDate",
                    claimedExpense: 1,
                    approvedExpenseByAdmin: 1,
                    approvedExpenseByMgr: 1,
                    AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                    sumExpAdm: 1,
                    subtExpAdm: 1,
                    claimedDA: 1,
                    claimedTA: 1
                }

                for (const exp of expeseHead.expenses) {
                    const newExpHead = {
                        $sum: `$miscExpSelf.${exp.replace(/\s/g,'')}`
                    }
                    const newExpHeadForAdmin = {
                        $sum: `$miscExpADM.${exp.replace(/\s/g,'')}`
                    }

                    dynamicGroup = {
                        ...dynamicGroup,
                        [`${exp.replace(/\s/g,'')}`]: newExpHead,
                        [`Adm${exp.replace(/\s/g,'')}`]: newExpHeadForAdmin

                    }

                    dynamicProject = {
                        ...dynamicProject,
                        [`${exp.replace(/\s/g,'')}`]: 1,
                        [`Ded${exp.replace(/\s/g,'')}`]: { $subtract: [`$${exp.replace(/\s/g,'')}`, `$Adm${exp.replace(/\s/g,'')}`] },
                        expenseHead: { $literal: expeseHead.expenses }
                    }
                }

                for (let i = 0; i < params.stateIds.length; i++) {
                    stateIds.push(ObjectId(params.stateIds[i]))
                }
                for (let i = 0; i < params.districtIds.length; i++) {
                    hqIds.push(ObjectId(params.districtIds[i]))
                }
                where.stateId = { $in: stateIds }
                where.districtId = { $in: hqIds }


                ExpenseClaimedAggregate.aggregate({
                        $match: where
                    }, {
                        $group: dynamicGroup
                    }, {
                        $lookup: {
                            "from": "UserInfo",
                            "localField": "_id.userId",
                            "foreignField": "userId",
                            "as": "userData"
                        }
                    }, {
                        $unwind: "$userData"
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "userData.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "userData.districtId",
                            "foreignField": "_id",
                            "as": "disData"
                        }
                    }, {
                        $project: dynamicProject
                    }, {
                        $sort: dynamicSort
                    },
                    function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );

            } else if (params.type === 'Employee Wise') {
                let dynamicGroup = {
                    _id: {
                        userId: "$userId"
                    },
                    claimedExpense: {
                        $sum: "$totalExpense"
                    },
                    approvedExpenseByAdmin: {
                        $sum: "$totalExpenseAdmin"
                    },
                    approvedExpenseByMgr: {
                        $sum: "$totalExpenseMgr"
                    },
                    sumExpAdm: {
                        $sum: "$sumExpAdm"
                    },
                    subtExpAdm: {
                        $sum: "$subtExpAdm"
                    },
                    claimedDA: {
                        $sum: "$da"
                    },
                    claimedTA: {
                        $sum: "$fare"
                    }
                };
                let dynamicProject = {
                    name: "$userData.name",
                    state: { $arrayElemAt: ["$stateData.stateName", 0] },
                    district: { $arrayElemAt: ["$disData.districtName", 0] },
                    designation: "$userData.designation",
                    designationLevel: "$userData.designationLevel",
                    joiningDate: "$userData.reportingDate",
                    claimedExpense: 1,
                    approvedExpenseByAdmin: 1,
                    approvedExpenseByMgr: 1,
                    AmountDeduction: { $subtract: ["$claimedExpense", "$approvedExpenseByAdmin"] },
                    sumExpAdm: 1,
                    subtExpAdm: 1,
                    claimedDA: 1,
                    claimedTA: 1
                };

                for (const exp of expeseHead.expenses) {
                    const newExpHead = {
                        $sum: `$miscExpSelf.${exp.replace(/\s/g,'')}`
                    }
                    const newExpHeadForAdmin = {
                        $sum: `$miscExpADM.${exp.replace(/\s/g,'')}`
                    }

                    dynamicGroup = {
                        ...dynamicGroup,
                        [`${exp.replace(/\s/g,'')}`]: newExpHead,
                        [`Adm${exp.replace(/\s/g,'')}`]: newExpHeadForAdmin

                    }

                    dynamicProject = {
                        ...dynamicProject,
                        [`${exp.replace(/\s/g,'')}`]: 1,
                        [`Ded${exp.replace(/\s/g,'')}`]: { $subtract: [`$${exp.replace(/\s/g,'')}`, `$Adm${exp.replace(/\s/g,'')}`] },
                        expenseHead: { $literal: expeseHead.expenses }
                    }
                }
                for (var i = 0; i < params.userIds.length; i++) {
                    userIds.push(ObjectId(params.userIds[i]))
                }
                where.userId = { $in: userIds };


                ExpenseClaimedAggregate.aggregate({
                        $match: where
                    }, {
                        $group: dynamicGroup
                    }, {
                        $lookup: {
                            "from": "UserInfo",
                            "localField": "_id.userId",
                            "foreignField": "userId",
                            "as": "userData"
                        }
                    }, {
                        $unwind: "$userData"
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "userData.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "userData.districtId",
                            "foreignField": "_id",
                            "as": "disData"
                        }
                    }, {
                        $project: dynamicProject
                    }, {
                        $sort: dynamicSort
                    },
                    function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Expenseclaimed", errorObject: err, paramsObject: params, methodName: 'getConsolidatedExpenseNew'});
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );
            }
        });




    }

    Expenseclaimed.remoteMethod(
        'getConsolidatedExpenseNew', {
            description: 'getConsolidatedExpenseNew',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    )
	
	//--------------------------------------------END----------------------------------------
};