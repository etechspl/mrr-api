'use strict';
var sendMail = require('../../utils/sendMail')
var ObjectId = require('mongodb').ObjectID;

module.exports = function(Container) {


    Container.afterRemote('upload', function(ctx, unused, next) {
        var self = this;

        let file = ctx.result.result.files.image[0];
        let fields = ctx.result.result.fields;
        let FileLibrary = Container.app.models.FileLibrary;
        // console.log("Image Upload For : "+fields.uploadingImageFor[0])

        if (fields.uploadingImageFor[0] == "userImage") {
			let UserLogin = Container.app.models.UserLogin;
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: 'upload', methodName: 'afterRemote'});
                    // console.log(err)
                }
                UserLogin.update({
                    id: fields.userId[0]
                }, {

                    imageId: ObjectId(result.id),
                    imageUploaded: false

                }, function(err, userResult) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                        // console.log(err)
                    }
                });
                //ctx.result.fileId = result.id
                let returnResult = {
                    companyId: fields.companyId[0],
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    fileId: result.id
                }
                ctx.result = returnResult
				next();
            });

        } else if (fields.uploadingImageFor[0] == "dcrImage") {
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                    // console.log(err)
                }
                //ctx.result.fileId = result.id
                let returnResult = {
                    companyId: fields.companyId[0],
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    fileId: result.id,
					userId : fields.userId[0]
					
                }
                ctx.result = returnResult
				next();
            });
        } else if (fields.uploadingImageFor[0] == "dcrProviderVisit") {
			// console.log("I am in : dcrProviderVisit...")
			let DCRProviderVisitDetails = Container.app.models.DCRProviderVisitDetails;
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                    // console.log(err)
                }               
				
				DCRProviderVisitDetails.update({
                    id: fields.dcrProviderVisitId[0]
                }, {
                    fileId: ObjectId(result.id),
					updatedAt : new Date()
                }, function(err, userResult) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                        // console.log(err)
                    }
					
					//ctx.result.fileId = result.id
                let returnResult = {
                    companyId: fields.companyId[0],
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    fileId: result.id,
					providerId : fields.dcrProviderVisitId[0]
					
                }
				
				// console.log("PKKKKKKKKK : ",returnResult)
                ctx.result = returnResult
				next();
					
                });
				
				
            });
        } else if (fields.uploadingImageFor[0] == "dcrExpense") {
			let DCRMaster = Container.app.models.DCRMaster;
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                    // console.log(err)
                }    

				let passingObject = {
					updateImageFor : "Expense",
					companyId : fields.companyId[0],
					dcrId : fields.dcrId[0],
					dcrExpenseId : fields.dcrExpenseId[0],
					fileId : result.id
				}				
				
				DCRMaster.updateImage(passingObject, function(err, userResult) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                        // console.log(err)
                    }
					
					//ctx.result.fileId = result.id
                let returnResult = {
                    companyId: ObjectId(fields.companyId[0]),
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    fileId: result.id,
					expenseId : fields.dcrExpenseId[0],
					dcrId : fields.dcrId[0],					
                }			
                ctx.result = returnResult
				next();
					
                });				
				
            });
        } else if (fields.uploadingImageFor[0] == "MailAttachments") {
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                    // console.log(err)
                }               
                //ctx.result.fileId = result.id
                let returnResult = {
                    companyId: fields.companyId[0],
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    fileId: result.id
                }
                ctx.result = returnResult
                next();
            });

        }else if (fields.uploadingImageFor[0] == "Signature") {
			// console.log("I am in : dcrProviderVisit...")
			let DCRProviderVisitDetails = Container.app.models.DCRProviderVisitDetails;
            let fileUploadData = {
                companyId: ObjectId(fields.companyId[0]),
                container: file.container,
                modifiedFileName: file.name,
                originalFileName: file.originalFilename,
                fileContentType: file.type,
                fileSize: file.size,
                filePath: '/client/img',
                //uploadBy : fields.uploadBy[0]
            };

            FileLibrary.create(fileUploadData, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                    // console.log(err)
                }               
				
				DCRProviderVisitDetails.update({
                    id: fields.dcrProviderVisitId[0]
                }, {
                    signId: ObjectId(result.id),
					updatedAt : new Date()
                }, function(err, userResult) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Container", errorObject: err, paramsObject: ctx, methodName: 'afterRemote'});
                        // console.log(err)
                    }
					
					//ctx.result.fileId = result.id
                let returnResult = {
                    companyId: fields.companyId[0],
                    container: file.container,
                    modifiedFileName: file.name,
                    originalFileName: file.originalFilename,
                    fileContentType: file.type,
                    fileSize: file.size,
                    filePath: '/client/img',
                    signId: result.id,
					providerId : fields.dcrProviderVisitId[0]
					
                }
				
				// console.log("PKKKKKKKKK : ",returnResult)
                ctx.result = returnResult
				next();
					
                });
				
				
            });
        }


    });
};