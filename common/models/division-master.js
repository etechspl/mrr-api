'use strict';
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
module.exports = function(Divisionmaster) {
    Divisionmaster.getDivisions = function(params, cb) {
        var self = this;
        var DivisionMasterCollection = this.getDataSource().connector.collection(Divisionmaster.modelName);
        var userInfoCollection = this.getDataSource().connector.collection(Divisionmaster.app.models.UserInfo.modelName);
        if (params.designationLevel == 0) {
            DivisionMasterCollection.aggregate({
                $match: {
                    companyId: ObjectId(params.companyId)
                }
            }, {
                $sort: {
                    divisionName: 1
                }
            }, {
                $group: {
                    _id: {

                    },
                    divisionObj: {
                        $addToSet: {
                            "divisionName": "$divisionName",
                            "divisionId": "$_id"
                        }
                    },
                    divisionIds: {
                        $addToSet: "$_id"
                    }
                }
            }, function(err, result) {
                if (err) {
                    sendMail.sendMail({collectionName: "Divisionmaster", errorObject: err, paramsObject: params, methodName: 'getDivisions'});
                    return cb(false, err)
                }
                if (result.length > 0) {
                    return cb(false, result)
                } else {
                    return cb(false, [])
                }
            });
        } else if (params.designationLevel > 1) {
            Divisionmaster.app.models.Hierarchy.find({
                where: {
                    companyId: params.companyId,
                    supervisorId: params.supervisorId,
                    //userDesignationLevel: 1, commenting because we need to get all users designations users under manager
                    status: true
                }
            }, function(err, hrcyResult) {
                if (err) {
                    sendMail.sendMail({collectionName: "Divisionmaster", errorObject: err, paramsObject: params, methodName: 'getDivisions'});
                    return cb(false, err)
                }
				// console.log("Hry Result : ",hrcyResult)
                if (hrcyResult.length > 0) {
                    let passingUserIds = [];
                    for (let i = 0; i < hrcyResult.length; i++) {
                        passingUserIds.push(hrcyResult[i].userId);
                    }

                    userInfoCollection.aggregate({
                        $match: {
                            companyId: ObjectId(params.companyId),
                            userId: {
                                $in: passingUserIds
                            },
                            status: true
                        }
                    },{
						$unwind:"$divisionId"
					}, {
                        $lookup: {
                            "from": "DivisionMaster",
                            "localField": "divisionId",
                            "foreignField": "_id",
                            "as": "data"
                        }
                    }, {
                        $unwind: "$data" 
                    }, {
                        $sort: {
                            "data.divisionName": -1
                        }
                    }, {
                        $group: {
                            _id: {

                            },
                            divisionObj: {
                                $addToSet: {
                                    "divisionName": "$data.divisionName",
                                    "divisionId": "$data._id"
                                }
                            },
                            divisionIds: {
                                $addToSet: "$data._id"
                            }
                        }
                    }, function(err, result) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Divisionmaster", errorObject: err, paramsObject: params, methodName: 'getDivisions'});
                            return cb(false, err)
                        }
                        if (result.length > 0) {
                            return cb(false, result)
                        } else {
                            return cb(false, [])
                        }
                    });
                } else {
                    return cb(false, [])
                }

            });


        }
    }
    Divisionmaster.remoteMethod(
        'getDivisions', {
            description: 'Get Division List on Admin or Manager Hierarchys',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    )

    
    Divisionmaster.getUserDivision = function(params, cb) {
        var self = this;
        var DivisionMasterCollection = this.getDataSource().connector.collection(Divisionmaster.modelName);
        var userInfoCollection = this.getDataSource().connector.collection(Divisionmaster.app.models.UserInfo.modelName);
        // console.log(params);

        userInfoCollection.aggregate({
            $match: {
                companyId: ObjectId(params.companyId),
                userId: ObjectId(params.userId),
                status: true
            }
        }, {
            $lookup: {
                "from": "DivisionMaster",
                "localField": "divisionId",
                "foreignField": "_id",
                "as": "data"
            }
        }, {
            $unwind: "$data"
        }, {
            $sort: {
                "data.divisionName": -1
            }
        }, {
            $group: {
                _id: {

                },
                divisionObj: {
                    $addToSet: {
                        "divisionName": "$data.divisionName",
                        "divisionId": "$data._id"
                    }
                },
                divisionIds: {
                    $addToSet: "$data._id"
                }
            }
        }, function(err, result) {
            if (err) {
                sendMail.sendMail({collectionName: "Divisionmaster", errorObject: err, paramsObject: params, methodName: 'getUserDivision'});
                return cb(false, err)
            }
            if (result.length > 0) {
                return cb(false, result)
            } else {
                return cb(false, [])
            }
        });

    }
    Divisionmaster.remoteMethod(
        'getUserDivision', {
            description: 'Get User\'s Division List',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    )
	//-----------Preeti Arora 30-01-2020 To get the division with mapped states--------------------
	Divisionmaster.getDivisionWithMappedState = function (params, cb) {
        
        var self = this;
        var DivisionMasterCollection = this.getDataSource().connector.collection(Divisionmaster.modelName);
        var userInfoCollection = this.getDataSource().connector.collection(Divisionmaster.app.models.UserInfo.modelName);
        if (params.designationLevel == 0) {
            DivisionMasterCollection.aggregate(
                // Stage 1
                {
                    $match: {
                        companyId: ObjectId(params.companyId)
                    }
                },

                // Stage 2
                {
                    $lookup: {
                        "from": "State",
                        "localField": "_id",
                        "foreignField": "assignedDivision",
                        "as": "stateData"
                    }
                },

                // Stage 3
                {
                    $sort: {
                        divisionName: 1
                    }
                },

                // Stage 4
                {
                    $unwind: { path: "$stateData", preserveNullAndEmptyArrays: true }

                },

                // Stage 5
                {
                    $group: {
                        _id: {
                            _id: "$_id"
                        },
                        divisionObj: {
                            $addToSet: {
                                "divisionName": "$divisionName",
                                "divisionId": "$_id",
                                "status": "$status"
                            }
                        },
                        divisionIds: {
                            $addToSet: "$_id"
                        },
                        state: {
                            $push: {
                                "stateName": "$stateData.stateName",
                                "stateId": "$stateData._id"
                            }
                        }
                    }
                },

                // Stage 6
                {
                    $project: {
                        divisionId: { $arrayElemAt: ["$divisionObj.divisionId", 0] },
                        divisionName: { $arrayElemAt: ["$divisionObj.divisionName", 0] },
                        states: "$state",
                        status: { $arrayElemAt: ["$divisionObj.status", 0] },
                    }
                },
                function (err, result) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Divisionmaster", errorObject: err, paramsObject: params, methodName: 'getDivisionWithMappedState'});
                        return cb(false, err)
                    }
                    if (result.length > 0) {
                        return cb(false, result)
                    } else {
                        return cb(false, [])
                    }
                });
        }

    }


    Divisionmaster.remoteMethod(
        'getDivisionWithMappedState', {
        description: 'Get Division List on Admin or Manager Hierarchys',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
	//------------------------------------end-------------------------------
};