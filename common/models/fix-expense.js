'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;

module.exports = function(Fixexpense) {
    Fixexpense.getFixExpense = function(params, cb) {
        var self = this;
        var fixExpenseCollection = self.getDataSource().connector.collection(Fixexpense.modelName);
        let fixWhere = {
            companyId: params.companyId
        };
        if (params.type === "category wise") {
            fixWhere = {
                "category": params.category,
            };
        }

        if (params.isDivisionExist === true) {
            fixWhere.divisionId = params.divisionId
        }

        Fixexpense.app.models.Category.findOne({
            where: fixWhere
        }, function(err, res) {
            if(err){
                sendMail.sendMail({collectionName: "Fixexpense", errorObject: err, paramsObject: params, methodName: 'getFixExpense'});
            }
			// console.log("fic expenes areaa--",res)
            if (res !== null) {
                Fixexpense.find({
                    where: {
                        categoryId: res.id
                    },
                    fields: {
                        fixExpense: 1,
                        amount: 1
                    }
                }, function(err, fixExp) {
                    if(err){
                        sendMail.sendMail({collectionName: "Fixexpense", errorObject: err, paramsObject: params, methodName: 'getFixExpense'});
                    }
                    return cb(false, fixExp)

                })
            } else {
                return cb(false, []);
            }
        })
    }

    Fixexpense.remoteMethod(
        'getFixExpense', {
            description: 'Get Fix Expense of the User',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'params',
                    type: 'object'
                }
            ],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
};