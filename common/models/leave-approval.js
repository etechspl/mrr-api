'use strict';
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');

module.exports = function (Leaveapproval) {

  Leaveapproval.getLeaveApprovalRequest = function (params, cb) {
    var self = this;
    console.log(params)
    var LeaveApprovalCollection = self.getDataSource().connector.collection(Leaveapproval.modelName);

    if (params.type === 'dashboardCount') {
      LeaveApprovalCollection.aggregate({
        $match: {
          companyId: ObjectId(params.companyId),
          appStatus: { $ne: "approved" }
        }
      },

        {
          $group: {
            _id: { userId: "$userId" },
            leaveCount: { $sum: 1 }
          }
        },

        {
          $lookup: {
            "from": "UserInfo",
            "localField": "_id.userId",
            "foreignField": "userId",
            "as": "userDetails"
          }
        },

        {
          $unwind: "$userDetails"
        },

        {
          $project: {
            "userDetails": "$userDetails",
            "leaveCount": "$leaveCount"

          }
        },

        {
          cursor: {
            batchSize: 50
          },

          allowDiskUse: true
        }, function (err, result) {
          console.log('result >>>>> ', result);

          return cb(false, result)
        })

    } else if (params.type === 'districtCount') {
      LeaveApprovalCollection.aggregate({
        $match: {
          companyId: ObjectId(params.companyId),
          status: false,
          appStatus: { $ne: "approved" }
        }
      }, {
        $group: {
          _id: {
            "stateId": "$stateId",
            "districtId": "$districtId",
          },
          districtLeaveCount: { $sum: 1 }
        }
      }, {
        $lookup: {
          "from": "State",
          "localField": "_id.stateId",
          "foreignField": "_id",
          "as": "states"
        }
      }, {
        $lookup: {
          "from": "District",
          "localField": "_id.districtId",
          "foreignField": "_id",
          "as": "district"
        }
      }, {
        $project: {
          _id: 0,
          stateId: "$_id.stateId",
          districtId: "$_id.districtId",
          stateName: {
            $arrayElemAt: ["$states.stateName", 0]
          },
          districtName: {
            $arrayElemAt: ["$district.districtName", 0]
          },
          districtLeaveCount: "$districtLeaveCount"
        }
      }, {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      }, function (err, result) {
        return cb(false, result)
      })
    } else if (params.type === 'districtWise') {
      LeaveApprovalCollection.aggregate(

        {
          $match: {
            companyId: ObjectId(params.companyId),
            districtId: ObjectId(params.districtId),
            appStatus: { $ne: "approved" }
          }
        },
        {
          $group: {
            _id: {
              userId: "$userId",
              districtId: "$districtId"
            },
            leaveCount: { $sum: 1 }
          }
        },

        {
          $lookup: {
            "from": "District",
            "localField": "_id.districtId",
            "foreignField": "_id",
            "as": "district"
          }
        },

        {
          $lookup: {
            "from": "UserInfo",
            "localField": "_id.userId",
            "foreignField": "userId",
            "as": "userDetails"
          }
        },

        {
          $project: {
            _id: 0,
            "userDetails": { $arrayElemAt: ["$userDetails", 0] },
            "district": { $arrayElemAt: ["$district.districtName", 0] },
            "leaveCount": "$leaveCount"
          }
        }, {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      }, function (err, result) {
        console.log('districtWise result >>>>> ', result);
        return cb(false, result)

      });
    } else if (params.type === 'userLeaveList') {
      LeaveApprovalCollection.aggregate(
        {
          $match: {
            companyId: ObjectId(params.companyId),
            userId: ObjectId(params.userId),
            appStatus: { $ne: "approved" }
          }
        }, {
        $sort: {
          requestDate: 1
        }
      },
        {
          cursor: {
            batchSize: 50
          },

          allowDiskUse: true
        }, function (err, result) {
          return cb(false, result)
        });

    }
  }

  Leaveapproval.remoteMethod(
    'getLeaveApprovalRequest', {
    description: 'get leaves based on CompanyId',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  );
  //----------------------Preeti Post Leaves 24-03-2021-----------------
  Leaveapproval.insertLeaves = function (params, cb) {
    var self = this;
    let requestDate=[];
    let insertLeaves=[];
    params.forEach(element => {
      requestDate.push(new Date(element.requestDate));
      insertLeaves.push({
        "designationLevel": element.designationLevel,
        "requestDate": new Date(element.requestDate),
        "appStatus": element.appStatus,
        "status": element.status,
         districtId:element.districtId,
         stateId:element.stateId,
        "leaveReason": element.leaveReason,
        "leaveType":element.leaveType,
        "userId": ObjectId(element.userId),
        "companyId": ObjectId(element.companyId),
      })
    });
    var LeaveApprovalCollection = self.getDataSource().connector.collection(Leaveapproval.modelName);
    let whereObj={
      companyId:ObjectId(params[0].companyId),
      userId:ObjectId(params[0].userId),
      requestDate:{$in:requestDate}
    }
    LeaveApprovalCollection.aggregate(
        // Stage 1
        {
          $match:whereObj
        },function(err,res){
          if(err){
            return cb(err)
          }
          if(res.length>0){
            let AlreadySubmittedDates=[];
            res.forEach(check => {
              AlreadySubmittedDates.push(" "+moment(check.requestDate).format("YYYY-MM-DD"))
            });
            let msg="You have already requested the leave for the selected "+ AlreadySubmittedDates+ " dates";
            let returnMsg={
              error:msg
            }
            return cb(false,returnMsg);
          }else{
            Leaveapproval.create(insertLeaves,(err,res)=>{
              if(err){
                return cb(err)
              }
              return cb(false,res)
            })
          }
        }    
    );
      }

  Leaveapproval.remoteMethod(
    'insertLeaves', {
    description: 'Insert Leaves',
    accepts: [{
      arg: 'params',
      type: 'array',
      http: {
        source: 'body'
    }
    }],

    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'post'
    }
  }
  );
  //------------------------End----------------------------------------

};



 /* Admin Dashboard - Count
 * Manager Dashboard - Count
  type  : dashboadCount
  match & Group by //admins --> admin --> state & hq sum
  match - companyId // admin --> level 4> shate & hq group else employee wise
  manager - company id & his hierarchy id
 * Count ---<> Click Details info
 * HeadQuarterwise
 * Employee wise  - leave count
 * Approve or disapprove option
 *
 *
 */