'use strict';
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail')
var moment = require('moment');
var async = require('async');


module.exports = function (Samplegiftissuedetail) {
    
    Samplegiftissuedetail.sampleIssue = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
        //// console.log(params);
        let dMatch = {}
        if (params.formDetail.reportType == 'Employeewise') {
            dMatch = {
                "where": {
                    "companyId": params.companyId,
                    "stateId": { inq: params.formDetail.stateInfo },
                    "userId": { inq: params.formDetail.userInfo }
                },
                "fields1": "userId",
                "fields2": "districtId",
                "designationLevel": 1

            }
        } else {
            dMatch = {
                "where": {
                    "companyId": params.companyId,
                    "stateId": { inq: params.formDetail.stateInfo },
                    "designationLevel": params.formDetail.designation,

                },
                "fields1": "userId",
                "fields2": "districtId"

            }
        }
        //// console.log(dMatch);
        Samplegiftissuedetail.app.models.UserInfo.find(dMatch, function (err, userIdResult) {
            if(err){
                sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'sampleIssue' });
            }
            for (let i = 0; i < userIdResult.length; i++) {
                for (let j = 0; j < params.sampleIssueDetail.length; j++) {
                    if (params.sampleIssueDetail[j].quantity == null || params.sampleIssueDetail[j].quantity < 0 || params.sampleIssueDetail[j].quantity == undefined || params.sampleIssueDetail[j].quantity == '') {

                    } else {
                        let finalobj = [];
                        let ddMatch = {};
                        let tempData = {};
                        if (params.formDetail.moduleTypeName == 'sample') {
                            ddMatch = {
                                where: {
                                    companyId: params.companyId,
                                    userId: userIdResult[i].userId,
                                    productId: params.sampleIssueDetail[j].productId,
                                    type: params.formDetail.moduleTypeName
                                },
                                fields: {
                                    opening: 1,
                                    balance: 1
                                },
                                order: 'issueDate DESC',
                                limit: 1
                            };
                            tempData = {
                                companyId: ObjectId(params.companyId),
                                stateId: ObjectId(userIdResult[i].stateId),
                                districtId: ObjectId(userIdResult[i].districtId),
                                designationLevel: userIdResult[i].designationLevel,
                                userId: userIdResult[i].userId,
                                productId: ObjectId(params.sampleIssueDetail[j].productId),
                                receipt: params.sampleIssueDetail[j].quantity,
                                opening: 0,
                                balance: params.sampleIssueDetail[j].quantity,
                                issueDate: new Date(),
                                month: new Date().getMonth() + 1,
                                year: new Date().getFullYear(),
                                type: params.formDetail.moduleTypeName,
                                issuedBy: ObjectId(params.submitBy)
                            };
                        } else if (params.formDetail.moduleTypeName == 'gift') {
                            ddMatch = {
                                where: {
                                    companyId: params.companyId,
                                    userId: userIdResult[i].userId,
                                    productId: params.sampleIssueDetail[j].giftId,
                                    type: params.formDetail.moduleTypeName
                                },
                                fields: {
                                    opening: 1,
                                    balance: 1
                                },
                                order: 'issueDate DESC',
                                limit: 1
                            };
                            tempData = {
                                companyId: ObjectId(params.companyId),
                                stateId: ObjectId(userIdResult[i].stateId),
                                districtId: ObjectId(userIdResult[i].districtId),
                                designationLevel: userIdResult[i].designationLevel,
                                userId: userIdResult[i].userId,
                                productId: ObjectId(params.sampleIssueDetail[j].giftId),
                                receipt: params.sampleIssueDetail[j].quantity,
                                opening: 0,
                                balance: params.sampleIssueDetail[j].quantity,
                                issueDate: new Date(),
                                month: new Date().getMonth() + 1,
                                year: new Date().getFullYear(),
                                type: params.formDetail.moduleTypeName,
                                issuedBy: ObjectId(params.submitBy)
                            };
                        }
                        Samplegiftissuedetail.find(ddMatch, function (err, result) {
                            if(err){
                                sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'sampleIssue' });
                            }
                            if (result.length > 0) {
                                console.log("result->",result);
                                tempData.opening = result[0].balance ? result[0].balance:0 ;
                                tempData.balance = params.sampleIssueDetail[j].quantity ? params.sampleIssueDetail[j].quantity:0 + result[0].balance ? result[0].balance:0;
                                finalobj.push(tempData);
                            } else {
                                finalobj.push(tempData);
                            }
                            Samplegiftissuedetail.create(finalobj, function (err, res) {
                                if (err) {
                                    sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'sampleIssue' });
                                    // console.log(err);
                                    return cb(err);
                                }
                            });
                        });
                    }
                }
            }
            var res = [];
            res.push({
                status: "OK"
            })
            return cb(false, res)
        });
    }
    Samplegiftissuedetail.remoteMethod(
        'sampleIssue', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object',
                http: { source: 'body' }
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'post'
            }
        }
    );
    
 //----------------------get sample balance- by preeti 26-09-2019-----------------
    Samplegiftissuedetail.getSampleBalance = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
        SampleCollection.aggregate(

              {
                $match: {
                   "userId":ObjectId(params.userId),
                   "companyId":ObjectId(params.companyId),
                 }
              },
          
              // Stage 2
              {
                $sort: 
                {
                "sampleData.issueDate":1
                }
              },
          
              // Stage 3
              {
                $group: {
                 _id:{
                                  productId:"$productId"
                                },
                                balance :{
                                $last : "$balance"
                                },
                                rowId:{
                                    $last : "$_id"
                                    }
                }
              },
          
              // Stage 4
              {
                $project: {
                _id:"$_id.productId",
                 sampleIssueBalance:"$balance",
                 
                }
            },function(err,res){
                if(err){
                    sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'getSampleBalance' });
                }
                return cb(false,res)
            }
          
          );
          

    } 
    Samplegiftissuedetail.remoteMethod(
        'getSampleBalance', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
	
    //-------------------------------------------
    Samplegiftissuedetail.getSampleGiftIssueDetail = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
        let dMatch = {};
        let dLookup = {};
        let dProject = {};
        let stateIds = [];
        let districtIds = [];
        let userIds = [];
        let month = typeof (params.formDetail.month) == "object" ?  params.formDetail.month : [params.formDetail.month];
        if (params.formDetail.moduleTypeName == 'sample') {
            dLookup = {
                "from": "Products",
                "localField": "productId",
                "foreignField": "_id",
                "as": "sampleGiftName"
            };
            dProject = {
                _id: 0,
                stateId: { $arrayElemAt: ["$stateData._id", 0] },
                stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                districtId: { $arrayElemAt: ["$districtData._id", 0] },
                districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                userName: { $arrayElemAt: ["$userData.name", 0] },
                userId: { $arrayElemAt: ["$userData._id", 0] },
                productName: { $arrayElemAt: ["$sampleGiftName.productName", 0] },
                opening: 1,
                receipt: 1,
                balance: 1,
                issueDate: { $dateToString: { format: "%d-%m-%Y", date: { $add: ["$issueDate", 0] } } }
            }
        } else if (params.formDetail.moduleTypeName == 'gift') {
            dLookup = {
                "from": "Gifts",
                "localField": "productId",
                "foreignField": "_id",
                "as": "sampleGiftName"
            };
            dProject = {
                _id: 0,
                stateId: { $arrayElemAt: ["$stateData._id", 0] },
                stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                districtId: { $arrayElemAt: ["$districtData._id", 0] },
                districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                userName: { $arrayElemAt: ["$userData.name", 0] },
                userId: { $arrayElemAt: ["$userData._id", 0] },
                productName: { $arrayElemAt: ["$sampleGiftName.giftName", 0] },
                opening: 1,
                receipt: 1,
                balance: 1,
                issueDate: { $dateToString: { format: "%d-%m-%Y", date: { $add: ["$issueDate", 0] } } }
            }
        }
        if (params.formDetail.userInfo != null) {
            for (var iii = 0; iii < params.formDetail.userInfo.length; iii++) {
                userIds.push(ObjectId(params.formDetail.userInfo[iii]));
            }
        }
        if (params.rL == 0) {
            if (params.formDetail.stateInfo.length > 0) {
                for (var i = 0; i < params.formDetail.stateInfo.length; i++) {
                    stateIds.push(ObjectId(params.formDetail.stateInfo[i]));
                }
            }
            if (params.formDetail.districtInfo != null) {
                for (var ii = 0; ii < params.formDetail.districtInfo.length; ii++) {
                    districtIds.push(ObjectId(params.formDetail.districtInfo[ii]));
                }
            }
            if (params.formDetail.reportType == 'Employeewise') {
                dMatch = {
                    "companyId": ObjectId(params.companyId),
                    "stateId": {
                        $in: stateIds
                    },
                    "userId": {
                        $in: userIds
                    },
                    "type": params.formDetail.moduleTypeName,
                    "month": {
                        $in: month,
                      },
                    "year":{$in: params.formDetail.year}
                }
            } else if (params.formDetail.reportType == 'Statewise') {
                dMatch = {
                    "companyId": ObjectId(params.companyId),
                    "stateId": {
                        $in: stateIds
                    },
					"designationLevel":params.formDetail.designation,
                    "type": params.formDetail.moduleTypeName,
                    "month": {
                        $in: month,
                      },
                    "year": {$in: params.formDetail.year}
                }
            } else if (params.formDetail.reportType == 'Headquarterwise') {
                dMatch = {
                    "companyId": ObjectId(params.companyId),
                    "stateId": {
                        $in: stateIds
                    },
                    "districtId": {
                        $in: districtIds
                    },
					"designationLevel":params.formDetail.designation,
                    "type": params.formDetail.moduleTypeName,
                    "month": {
                        $in: month,
                      },
                    "year": {$in: params.formDetail.year}
                }
            }
        } else if (params.rL > 1) {

            dMatch = {
                "companyId": ObjectId(params.companyId),
                "userId": {
                    $in: userIds
                },
                "type": params.formDetail.moduleTypeName,
                "month": {
                    $in: month,
                  },
                "year": {$in: params.formDetail.year}
            }
        } else if (params.rL == 1) {

            dMatch = {
                "companyId": ObjectId(params.companyId),
                "userId": {
                    $in: userIds
                },
                "type": params.formDetail.moduleTypeName,
                "month": {
                    $in: month,
                  },
                "year": {$in: params.formDetail.year}
            }
        }
        SampleCollection.aggregate({
            $match: dMatch
        },
            // Stage 3
            {
                $lookup: {
                    "from": "State",
                    "localField": "stateId",
                    "foreignField": "_id",
                    "as": "stateData"
                }
            },

            // Stage 4
            {
                $lookup: {
                    "from": "District",
                    "localField": "districtId",
                    "foreignField": "_id",
                    "as": "districtData"
                }
            },
            // Stage 5
            {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "userData"
                }
            },
            // Stage 5
            {
                $lookup: dLookup
            },

            // Stage 6
            {
                $project: dProject
            }, function (err, sampleGiftDetails) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'getSampleGiftIssueDetail' });
                }
                return cb(false, sampleGiftDetails)
            });


    }
    Samplegiftissuedetail.remoteMethod(
        'getSampleGiftIssueDetail', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Samplegiftissuedetail.getSampleGiftIssueDetailForEdit = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
        let dMatch = {};
        let dLookup = {};
        let dProject = {};
        let stateIds = [];
        let userIds = [];
        if (params.formDetail.moduleTypeName == 'sample') {
            dLookup = {
                "from": "Products",
                "localField": "_id.productId",
                "foreignField": "_id",
                "as": "sampleGiftName"
            };
            dProject = {
                _id: 0,
                id: "$lastProduct.id",
                stateId: { $arrayElemAt: ["$stateData._id", 0] },
                stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                districtId: { $arrayElemAt: ["$districtData._id", 0] },
                districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                userName: { $arrayElemAt: ["$userData.name", 0] },
                userId: { $arrayElemAt: ["$userData._id", 0] },
                productName: { $arrayElemAt: ["$sampleGiftName.productName", 0] },
                productId: "$_id.productId",
                opening: "$lastProduct.opening",
                receipt: "$lastProduct.receipt",
                balance: "$lastProduct.balance",
                issueDate: { $dateToString: { format: "%Y-%m-%d %H:%M:%S", date: { $add: ["$lastProduct.issueDate", 0] } } }
            }
        } else if (params.formDetail.moduleTypeName == 'gift') {
            dLookup = {
                "from": "Gifts",
                "localField": "_id.productId",
                "foreignField": "_id",
                "as": "sampleGiftName"
            };
            dProject = {
                _id: 0,
                id: "$lastProduct.id",
                stateId: { $arrayElemAt: ["$stateData._id", 0] },
                stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                districtId: { $arrayElemAt: ["$districtData._id", 0] },
                districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                userName: { $arrayElemAt: ["$userData.name", 0] },
                userId: { $arrayElemAt: ["$userData._id", 0] },
                productName: { $arrayElemAt: ["$sampleGiftName.giftName", 0] },
                productId: "$_id.productId",
                opening: "$lastProduct.opening",
                receipt: "$lastProduct.receipt",
                balance: "$lastProduct.balance",
                issueDate: { $dateToString: { format: "%Y-%m-%d %H:%M:%S", date: { $add: ["$lastProduct.issueDate", 0] } } }
            }
        }
        if (params.formDetail.userInfo != null) {
            for (var iii = 0; iii < params.formDetail.userInfo.length; iii++) {
                userIds.push(ObjectId(params.formDetail.userInfo[iii]));
            }
        }
        if (params.rL == 0) {
            if (params.formDetail.stateInfo.length > 0) {
                for (var i = 0; i < params.formDetail.stateInfo.length; i++) {
                    stateIds.push(ObjectId(params.formDetail.stateInfo[i]));
                }
            }
            dMatch = {
                "companyId": ObjectId(params.companyId),
                "stateId": {
                    $in: stateIds
                },
                "userId": {
                    $in: userIds
                },
                "type": params.formDetail.moduleTypeName,
            }
        } else if (params.rL > 1) {
            dMatch = {
                "companyId": ObjectId(params.companyId),
                "userId": {
                    $in: userIds
                },
                "type": params.formDetail.moduleTypeName,
            }
        }

        SampleCollection.aggregate({
            $match: dMatch
        },

            // Stage 2
            {
                $sort: {
                    issueDate: 1
                }
            },

            // Stage 3
            {
                $group: {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId",
                        productId: "$productId",
                        userId: "$userId"
                    },
                    lastProduct: {
                        $last: {
                            id: "$_id",
                            opening: "$opening",
                            receipt: "$receipt",
                            balance: "$balance",
                            issueDate: "$issueDate"
                        }
                    }
                }
            },

            // Stage 4
            {
                $lookup: {
                    "from": "State",
                    "localField": "_id.stateId",
                    "foreignField": "_id",
                    "as": "stateData"
                }
            },

            // Stage 5
            {
                $lookup: {
                    "from": "District",
                    "localField": "_id.districtId",
                    "foreignField": "_id",
                    "as": "districtData"
                }
            },

            // Stage 6
            {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "_id.userId",
                    "foreignField": "_id",
                    "as": "userData"
                }
            },

            // Stage 7
            {
                $lookup: dLookup
            },

            // Stage 8
            {
                $project: dProject
            }, function (err, sampleGiftDetails) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'getSampleGiftIssueDetailForEdit' });
                }
                return cb(false, sampleGiftDetails)
            });


    }
    Samplegiftissuedetail.remoteMethod(
        'getSampleGiftIssueDetailForEdit', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Samplegiftissuedetail.modifySampleIssueDetail = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
        if (params.modifyIssueDetail.length > 0) {
            for (let j = 0; j < params.modifyIssueDetail.length; j++) {
                if (params.modifyIssueDetail[j].receipt == null || params.modifyIssueDetail[j].receipt < 0 || params.modifyIssueDetail[j].receipt == undefined || params.modifyIssueDetail[j].receipt == '') {
                } else {
                    let balance = params.modifyIssueDetail[j].receipt + params.modifyIssueDetail[j].opening;
                    SampleCollection.update({
                        companyId: ObjectId(params.companyId),
                        _id: ObjectId(params.modifyIssueDetail[j].id)
                    }, {
                            $set: {
                                receipt: params.modifyIssueDetail[j].receipt,
                                balance: balance,
                                updatedAt: new Date()
                            }
                        },function (err, result) {
                            if(err){
                                sendMail.sendMail({ collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'modifySampleIssueDetail' });
                            }
                        });
                }
            }
        }
        var result = [];
        result.push({
            status:"OK"
        })
        return cb(false, result);
    }
    Samplegiftissuedetail.remoteMethod(
        'modifySampleIssueDetail', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
	 // --------------------by preeti arora 13-08-2019---edited on 27-10-2020 by preeti---------------//
   Samplegiftissuedetail.checkSampleIssueBalance = function(params, cb) { 
    var self = this;
    var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
    let productIds = [];
    for (var k = 0; k < params.length; k++) {
      if(params[k].hasOwnProperty("productDetails")){
        for (var i = 0; i < params[k].productDetails.length; i++) {
          if(params[0].productDetails[i]==undefined){
          }else{
            productIds.push(ObjectId(params[0].productDetails[i].productId));
          }
        }
      }
      if(params[k].hasOwnProperty("giftDetails")){
        params[k].giftDetails.forEach(element => {
          if(element==undefined){
          }else{
            productIds.push(ObjectId(element.giftId));
          }   
             });
      }
    }


    let match = {
      productId: {$in: productIds},
      companyId: ObjectId(params[0].companyId),
      userId: ObjectId(params[0].submitBy),
    };

    SampleCollection.aggregate(
            // Stage 1
      {
        $match: match,
      },

            // Stage 2
      {
        $sort: {
          issueDate: 1,
        },
      },
            // Stage 3
      {
        $group: {
          _id: {
            productId: '$productId',
          },
          balance: {
            $last: '$balance',
          },
          rowId: {
            $last: '$_id',
          },
          type:{$addToSet:"$type"}
        },
      },{
          $project:{
            _id:"$_id.productId",
            balance:1,
            rowId:1,
            type:{$arrayElemAt:["$type",0]}
          }
      },
         function(err, sampleIssueResult) {
              if (sampleIssueResult.length <= 0) {
                      // if samples not issued.
                return cb(false, 'Sample Is not issued By Admin');
              } else {
                let balanceUpdate=0;
                for (var i = 0; i < sampleIssueResult.length; i++) {
                  if(sampleIssueResult[i].type=="sample"){  // samples will be updated--
                    for (var j = 0; j < params[0].productDetails.length; j++) {
                      if (sampleIssueResult[i]._id.equals(ObjectId(params[0].productDetails[j].productId))) {
                        
                        balanceUpdate = sampleIssueResult[i].balance - params[0].productDetails[j].productQuantity;
                       let filter = {
                          _id: ObjectId(sampleIssueResult[i].rowId),
                        };
                        let update = {
                          balance: balanceUpdate,
                          updatedAt: new Date(),
                        };
                        if (balanceUpdate < 0) {  // if balance is 0.
                                      // console.log("low balance");
                        } else {
                          SampleCollection.update(filter, {$set: update}, {multi: false}, function(err, sampleresult) {
                            if (err) {
                              sendMail.sendMail({collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'checkSampleIssueBalance'});
                              return cb(false, sampleresult);
                            }
                          });
                        }
                      }
                    }
                  }else{  // gifts will be updated----
                    for (var j = 0; j < params[0].giftDetails.length; j++) {
                      if (sampleIssueResult[i]._id.equals(ObjectId(params[0].giftDetails[j].giftId))) { 
                       balanceUpdate = parseInt(sampleIssueResult[i].balance) - parseInt(params[0].giftDetails[j].giftQty);
                        let filter = {
                          _id: ObjectId(sampleIssueResult[i].rowId),
                        };
                        let update = {
                          balance: balanceUpdate,
                          updatedAt: new Date(),
                        };
                        if (balanceUpdate < 0) {  // if balance is 0.
                         // console.log("low balance");
                        } else {
                          SampleCollection.update(filter, {$set: update}, {multi: false}, function(err, sampleresult) {
                            if (err) {
                              sendMail.sendMail({collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'checkSampleIssueBalance'});
                              return cb(false, sampleresult);
                            }
                          });
                        }
                      }
                    }
                  }

                  

                }
              }

              if (err) {
                sendMail.sendMail({collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'checkSampleIssueBalance'});
                return cb(err);
              }
              return cb(false, sampleIssueResult);
            }

        );
  };
    Samplegiftissuedetail.remoteMethod(
        'checkSampleIssueBalance', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //-----------------------------end-------------------------------//
//-----------Get doctor wise sample issue details by preeti arora 16-10-2020---------------------------

  //-----------Get doctor wise sample issue details by preeti arora 16-10-2020---------------------------
  Samplegiftissuedetail.giftIssueReportDoctorWise = function(params, cb) {
    var self = this;
    var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
    var dcrProviderVisitCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.DCRProviderVisitDetails.modelName);
    var providerCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Providers.modelName);
    var giftCollection=self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Gifts.modelName);
    var productCollection=self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Products.modelName);

    let year = params.year;
    let month = params.month;
	const fromDate = (moment(new Date(year,(month-1),1)).format("YYYY-MM-DD"))+"T00:00:00.000Z";
	const toDate = (moment(new Date(year,month,0)).format("YYYY-MM-DD"))+"T00:00:00.000Z";
	
    //let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
    //let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");
    //console.log("fromDate : ",fromDate)
	//console.log("toDate : ",toDate)
	
	let userId=[];
    if(typeof(params.userId)=="string"){
       userId.push(ObjectId(params.userId))
    }else{
      params.userId.forEach(element => {
        userId.push(ObjectId(element))
      });
    }

    let filter={
      companyId:ObjectId(params.companyId),
      submitBy:{$in:userId},
      dcrDate:{$gte:new Date(fromDate),$lte:new Date(toDate)},
      giftDetails:{$ne:[]} ,
      providerType:{$in:params.providerType}
    }
    //console.log(filter);
    
    if(params.type=="gift"){

      dcrProviderVisitCollection.aggregate(

        // Stage 1
        {
          $match: filter
        },
    
        // Stage 2
        {
          $lookup: {
              "from" : "Providers",
              "localField" : "providerId",
              "foreignField" : "_id",
              "as" : "ProviderData"
          }
        },
    
        // Stage 3
        {
          $lookup: {
              "from" : "UserInfo",
              "localField" : "submitBy",
              "foreignField" : "userId",
              "as" : "userData"
          }
        },
    
        // Stage 4
        {
          $lookup: {
              "from" : "District",
              "localField" : "districtId",
              "foreignField" : "_id",
              "as" : "disData"
          }
        },
    
        // Stage 5
        {
          $lookup: {
              "from" : "State",
              "localField" : "stateId",
              "foreignField" : "_id",
              "as" : "stateData"
          }
        },
    
        // Stage 6
        {
          $unwind: {
                     path: "$giftDetails",
                    preserveNullAndEmptyArrays: true
          }
        },
    
        // Stage 7
        {
          $match: {
          "giftDetails.giftId":{$in:params.productId}
          }
        },
    
        // Stage 8
        {
          $project: {
            dcrDate:1,
            providerType:1,
            giftName:"$giftDetails.giftName",
            state:{ $arrayElemAt: ["$stateData.stateName", 0] },
            district:{ $arrayElemAt: ["$disData.districtName", 0] },
            userName:"$userData.name",
            providerName:{ $arrayElemAt: ["$ProviderData.providerName", 0] },
            category:{ $arrayElemAt: ["$ProviderData.category", 0] },
            giftQty:"$giftDetails.giftQty"
          }
        },function(err,giftResult){
          if (err){ return cb(err)}
          else{
            return cb(false,giftResult)
          }

        }
    
    );
    }
    else if(params.type=="dateWise"){
      let userId=[];
      if(typeof(params.userId)=="string"){
        userId.push(ObjectId(params.userId))
     }else{
       params.userId.forEach(element => {
         userId.push(ObjectId(element))
       });
     }
          let providerMatch={
            companyId:ObjectId(params.companyId),
            userId:{$in:userId},
            status:true,
            providerType:{$in:params.providerType},
            appStatus:"approved",
          }
          let giftMatch={
            companyId:ObjectId(params.companyId),
            status:true
          }
          async.parallel({
            Providers:(callback)=> {
                providerCollection.aggregate(
                  // Stage 1
                  {
                    $match: providerMatch
                    
                  },
                  // Stage 2
                  {
                    $project: {
                        providerName:1,
                        providerCode:1,
                        category:1,
                        status:1,
                        providerType:1
                    }
                  },function(err,result){
                    if(err){
                      return cb(err);
                    }else{
                      if(result.length>0){
                        callback(null, result);
                      }else{
                        callback(null, []);
                      }
                    }
  
                  }
              );
            },
            giftData:(callback)=> {
              giftCollection.aggregate(
                // Stage 1
                {
                  $match: giftMatch
                  
                },
                // Stage 2
                {
                  $project: {
                      _id:1,
                      providerCode:1,
                      giftName:1
                  }
                },function(err,result){
                  if(err){
                    return cb(err);
                  }else{
                    if(result.length>0){
                      callback(null, result);
                    }else{
                      callback(null, []);
                    }
                  }

                }
              );
            },
            providerIssuedGiftData:(callback)=> {
              dcrProviderVisitCollection.aggregate(
                // Stage 1
                {
                  $match: filter
                },
            
                // Stage 2
                {
                  $lookup: {
                      "from" : "Providers",
                      "localField" : "providerId",
                      "foreignField" : "_id",
                      "as" : "ProviderData"
                  }
                },
            
                // Stage 3
                {
                  $lookup: {
                      "from" : "UserInfo",
                      "localField" : "submitBy",
                      "foreignField" : "userId",
                      "as" : "userData"
                  }
                },
            
                // Stage 4
                {
                  $lookup: {
                      "from" : "District",
                      "localField" : "districtId",
                      "foreignField" : "_id",
                      "as" : "disData"
                  }
                },
            
                // Stage 5
                {
                  $lookup: {
                      "from" : "State",
                      "localField" : "stateId",
                      "foreignField" : "_id",
                      "as" : "stateData"
                  }
                },
            
                // Stage 6
                {
                  $unwind: {
                             path: "$giftDetails",
                            preserveNullAndEmptyArrays: true
                  }
                },
                // Stage 8
                {
                  $project: {
                    dcrDate:1,
                    providerType:1,
                    giftName:"$giftDetails.giftName",
                    state:{ $arrayElemAt: ["$stateData.stateName", 0] },
                    district:{ $arrayElemAt: ["$disData.districtName", 0] },
                    providerName:{ $arrayElemAt: ["$ProviderData.providerName", 0] },
                    category:{ $arrayElemAt: ["$ProviderData.category", 0] },
                    providerId:{$arrayElemAt: ["$ProviderData._id", 0]},
                    giftId:"$giftDetails.giftId"
                  }
                },function(err,result){
                  if (err){ return cb(err)}
                  if(result.length>0){
                    callback(null, result);
                  }else{
                    callback(null, []);
                  }
      
                }
            
            );
            }
         },
          function(err, results) {
            if(err){
              return cb(err)
            }
            if(results.Providers.length>0){
              let final =[];
              results.Providers.forEach(provideData => {
                let temp=[];
                let i =1;
                results.giftData.forEach(giftColumns => {

                const giftFilter = results.providerIssuedGiftData.filter(function (obj) {                    
                    return obj.giftId.toString() == giftColumns._id.toString() && obj.providerId.toString() == provideData._id.toString();
                });  

                if(giftFilter.length>0){
               
                    let isssueDate= giftFilter.map(data => moment(data.dcrDate).format("YYYY-MM-DD"));
                    temp.push({[`gf${i}`] : isssueDate.toString()});

                }else{
                  //No Gift Issued Found.....
                  temp.push({[`gf${i}`] : "-------"});
                }  
                i++;               
                });   
                
                final.push({
                  docName :  provideData.providerName,
                  category: provideData.category,
                  providerType: provideData.providerType,
                  giftIssuedData : temp
                })

              });
              return cb(false,{IssueData : final, totalGift :results.giftData })

            }else{
              return cb(false,[])
            }
          
          });

        
          
    }


    else if(params.type=="sampleDateWise"){
      let userId=[];
      if(typeof(params.userId)=="string"){
        userId.push(ObjectId(params.userId))
     }else{
       params.userId.forEach(element => {
         userId.push(ObjectId(element))
       });
     }
          let providerMatch={
            companyId:ObjectId(params.companyId),
            userId:{$in:userId},
            status:true,
            providerType:{$in:params.providerType},
            appStatus:"approved",
          }
          let giftMatch={
            companyId:ObjectId(params.companyId),
            status:true
          }
          async.parallel({
            Providers:(callback)=> {
                providerCollection.aggregate(
                  // Stage 1
                  {
                    $match: providerMatch
                    
                  },
                  // Stage 2
                  {
                    $project: {
                        providerName:1,
                        providerCode:1,
                        category:1,
                        status:1,
                        providerType:1
                    }
                  },function(err,result){
                    if(err){
                      return cb(err);
                    }else{
                      if(result.length>0){
                        callback(null, result);
                      }else{
                        callback(null, []);
                      }
                    }
  
                  }
              );
            },
            giftData:(callback)=> {
              productCollection.aggregate(
                // Stage 1
                {
                  $match: giftMatch
                  
                },
                // Stage 2
                {
                  $project: {
                      _id:1,
                      providerCode:1,
                    productName:1
                  }
                },function(err,result){
                  if(err){
                    return cb(err);
                  }else{
                    if(result.length>0){
                      callback(null, result);
                    }else{
                      callback(null, []);
                    }
                  }

                }
              );
            },
            providerIssuedGiftData:(callback)=> {
              dcrProviderVisitCollection.aggregate(
                // Stage 1
                {
                  $match: filter
                },
            
                // Stage 2
                {
                  $lookup: {
                      "from" : "Providers",
                      "localField" : "providerId",
                      "foreignField" : "_id",
                      "as" : "ProviderData"
                  }
                },
            
                // Stage 3
                {
                  $lookup: {
                      "from" : "UserInfo",
                      "localField" : "submitBy",
                      "foreignField" : "userId",
                      "as" : "userData"
                  }
                },
            
                // Stage 4
                {
                  $lookup: {
                      "from" : "District",
                      "localField" : "districtId",
                      "foreignField" : "_id",
                      "as" : "disData"
                  }
                },
            
                // Stage 5
                {
                  $lookup: {
                      "from" : "State",
                      "localField" : "stateId",
                      "foreignField" : "_id",
                      "as" : "stateData"
                  }
                },
            
                // Stage 6
                {
                  $unwind: {
                             path: "$productDetails",
                            preserveNullAndEmptyArrays: true
                  }
                },
                // Stage 8
                {
                  $project: {
                    dcrDate:1,
                    providerType:1,
                    giftName:"$productDetails.productName",
                    state:{ $arrayElemAt: ["$stateData.stateName", 0] },
                    district:{ $arrayElemAt: ["$disData.districtName", 0] },
                    providerName:{ $arrayElemAt: ["$ProviderData.providerName", 0] },
                    category:{ $arrayElemAt: ["$ProviderData.category", 0] },
                    providerId:{$arrayElemAt: ["$ProviderData._id", 0]},
                  giftId:"$productDetails.productId"
                  }
                },function(err,result){
                  if (err){ return cb(err)}
                  if(result.length>0){
                    callback(null, result);
                  }else{
                    callback(null, []);
                  }
      
                }
            
            );
            }
         },
          function(err, results) {

            console.log("results",results);

            if(err){
              return cb(err)
            }
            if(results.Providers.length>0){
              let final =[];
              results.Providers.forEach(provideData => {
                let temp=[];
                let i =1;
                results.giftData.forEach(giftColumns => {

                const giftFilter = results.providerIssuedGiftData.filter(function (obj) {   

                  console.log("obj+++++++++",obj);                 
                    return obj.giftId.toString() == giftColumns._id.toString() && obj.providerId.toString() == provideData._id.toString();
                });  

                if(giftFilter.length>0){
               
                    let isssueDate= giftFilter.map(data => moment(data.dcrDate).format("YYYY-MM-DD"));
                    temp.push({[`gf${i}`] : isssueDate.toString()});

                }else{
                  //No Gift Issued Found.....
                  temp.push({[`gf${i}`] : "-------"});
                }  
                i++;               
                });   
                
                final.push({
                  docName :  provideData.providerName,
                  category: provideData.category,
                  providerType: provideData.providerType,
                  giftIssuedData : temp
                })

              });
              return cb(false,{IssueData : final, totalGift :results.giftData })

            }else{
              return cb(false,[])
            }
          
          });

        
          
    }


    
  };
  Samplegiftissuedetail.remoteMethod(
        'giftIssueReportDoctorWise', {
          description: 'Get Doctor Wise Sample And Gift Issue Details',
          accepts: [{
            arg: 'params',
            type: 'object',
          }],
          returns: {
            root: true,
            type: 'array',
          },
          http: {
            verb: 'get',
          },
        }
    );
  //-------------------End-----------------------------------------------------------


  
  //-----------Get doctor wise sample issue details by Rahul Saini 27-02-2020---------------------------
  Samplegiftissuedetail.sampleIssueReportDoctorWise = function(params, cb) {
    var self = this;
    var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
    var dcrProviderVisitCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.DCRProviderVisitDetails.modelName);
    var providerCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Providers.modelName);
    var giftCollection=self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Gifts.modelName);
    var productCollection=self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Products.modelName);

    let year = params.year;
    let month = params.month;
    let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
    let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");
    let userId=[];
    if(typeof(params.userId)=="string"){
       userId.push(ObjectId(params.userId))
    }else{
      params.userId.forEach(element => {
        userId.push(ObjectId(element))
      });
    }

    let filter={
      companyId:ObjectId(params.companyId),
      submitBy:{$in:userId},
      dcrDate:{$gte:new Date(fromDate),$lte:new Date(toDate)},
      productDetails:{$ne:[]} ,
      providerType:{$in:params.providerType}
    }
   
     if(params.type=="sample"){

      dcrProviderVisitCollection.aggregate(

        // Stage 1
        {
          $match: filter
        },
    
        // Stage 2
        {
          $lookup: {
              "from" : "Providers",
              "localField" : "providerId",
              "foreignField" : "_id",
              "as" : "ProviderData"
          }
        },
    
        // Stage 3
        {
          $lookup: {
              "from" : "UserInfo",
              "localField" : "submitBy",
              "foreignField" : "userId",
              "as" : "userData"
          }
        },
    
        // Stage 4
        {
          $lookup: {
              "from" : "District",
              "localField" : "districtId",
              "foreignField" : "_id",
              "as" : "disData"
          }
        },
    
        // Stage 5
        {
          $lookup: {
              "from" : "State",
              "localField" : "stateId",
              "foreignField" : "_id",
              "as" : "stateData"
          }
        },
    
        // Stage 6
        {
          $unwind: {
                     path: "$productDetails",
                    preserveNullAndEmptyArrays: true
          }
        },
    
        // Stage 7
        {
          $match: {
          "productDetails.productId":{$in:params.productId}
          }
        },
    
        // Stage 8
        {
          $project: {
            dcrDate:1,
            providerType:1,
            giftName:"$productDetails.productName",
            state:{ $arrayElemAt: ["$stateData.stateName", 0] },
            district:{ $arrayElemAt: ["$disData.districtName", 0] },
            userName:"$userData.name",
            providerName:{ $arrayElemAt: ["$ProviderData.providerName", 0] },
            category:{ $arrayElemAt: ["$ProviderData.category", 0] },
            giftQty:"$productDetails.productQuantity"
          }
        },function(err,giftResult){
          if (err){ return cb(err)}
          else{
            return cb(false,giftResult)
          }

        }
    
    );
    }


     if(params.type=="sampleDateWise"){
      let userId=[];
      if(typeof(params.userId)=="string"){
        userId.push(ObjectId(params.userId))
     }else{
       params.userId.forEach(element => {
         userId.push(ObjectId(element))
       });
     }
          let providerMatch={
            companyId:ObjectId(params.companyId),
            userId:{$in:userId},
            status:true,
            providerType:{$in:params.providerType},
            appStatus:"approved",
          }
          let giftMatch={
            companyId:ObjectId(params.companyId),
            status:true
          }
          async.parallel({
            Providers:(callback)=> {
                providerCollection.aggregate(
                  // Stage 1
                  {
                    $match: providerMatch
                    
                  },
                  // Stage 2
                  {
                    $project: {
                        providerName:1,
                        providerCode:1,
                        category:1,
                        status:1,
                        providerType:1
                    }
                  },function(err,result){
                    if(err){
                      return cb(err);
                    }else{
                      if(result.length>0){
                        callback(null, result);
                      }else{
                        callback(null, []);
                      }
                    }
  
                  }
              );
            },
            sampleData:(callback)=> {
              productCollection.aggregate(
                // Stage 1
                {
                  $match: giftMatch
                  
                },
                // Stage 2
                {
                  $project: {
                      _id:1,
                      providerCode:1,
                    productName:1
                  }
                },function(err,result){
                  if(err){
                    return cb(err);
                  }else{
                    if(result.length>0){
                      callback(null, result);
                    }else{
                      callback(null, []);
                    }
                  }

                }
              );
            },
            providerIssuedGiftData:(callback)=> {
              dcrProviderVisitCollection.aggregate(
                // Stage 1
                {
                  $match: filter
                },
            
                // Stage 2
                {
                  $lookup: {
                      "from" : "Providers",
                      "localField" : "providerId",
                      "foreignField" : "_id",
                      "as" : "ProviderData"
                  }
                },
            
                // Stage 3
                {
                  $lookup: {
                      "from" : "UserInfo",
                      "localField" : "submitBy",
                      "foreignField" : "userId",
                      "as" : "userData"
                  }
                },
            
                // Stage 4
                {
                  $lookup: {
                      "from" : "District",
                      "localField" : "districtId",
                      "foreignField" : "_id",
                      "as" : "disData"
                  }
                },
            
                // Stage 5
                {
                  $lookup: {
                      "from" : "State",
                      "localField" : "stateId",
                      "foreignField" : "_id",
                      "as" : "stateData"
                  }
                },
            
                // Stage 6
                {
                  $unwind: {
                             path: "$productDetails",
                            preserveNullAndEmptyArrays: true
                  }
                },
                // Stage 8
                {
                  $project: {
                    dcrDate:1,
                    providerType:1,
                    productName:"$productDetails.productName",
                    state:{ $arrayElemAt: ["$stateData.stateName", 0] },
                    district:{ $arrayElemAt: ["$disData.districtName", 0] },
                    providerName:{ $arrayElemAt: ["$ProviderData.providerName", 0] },
                    category:{ $arrayElemAt: ["$ProviderData.category", 0] },
                    providerId:{$arrayElemAt: ["$ProviderData._id", 0]},
                    productId:"$productDetails.productId"
                  }
                },function(err,result){
                  if (err){ return cb(err)}
                  if(result.length>0){
                    callback(null, result);
                  }else{
                    callback(null, []);
                  }
      
                }
            
            );
            }
         },
          function(err, results) {

           

            if(err){
              return cb(err)
            }
            if(results.Providers.length>0){
              let final =[];
              results.Providers.forEach(provideData => {
                let temp=[];
                let i =1;
                results.sampleData.forEach(giftColumns => {

                const giftFilter = results.providerIssuedGiftData.filter(function (obj) {   

                             
                    return obj.productId.toString() == giftColumns._id.toString() && obj.providerId.toString() == provideData._id.toString();
                });  

                if(giftFilter.length>0){
               
                    let isssueDate= giftFilter.map(data => moment(data.dcrDate).format("YYYY-MM-DD"));
                    temp.push({[`gf${i}`] : isssueDate.toString()});

                }else{
                  //No Gift Issued Found.....
                  temp.push({[`gf${i}`] : "-------"});
                }  
                i++;               
                });   
                
                final.push({
                  docName :  provideData.providerName,
                  category: provideData.category,
                  providerType: provideData.providerType,
                  giftIssuedData : temp
                })

              });
              return cb(false,{IssueData : final, totalGift :results.sampleData })

            }else{
              return cb(false,[])
            }
          
          });

        
          
    }


    
  };
  Samplegiftissuedetail.remoteMethod(
        'sampleIssueReportDoctorWise', {
          description: 'Get Doctor Wise Sample And Gift Issue Details',
          accepts: [{
            arg: 'params',
            type: 'object',
          }],
          returns: {
            root: true,
            type: 'array',
          },
          http: {
            verb: 'get',
          },
        }
    );
  //-------------------End-----------------------------------------------------------

  
  //----------Get Gift Issue Balance by preeti arora 26-10-2020-----------------------------

  Samplegiftissuedetail.getGiftDetailWithBalance = function(params, cb) {
    var self = this;
    var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
    var dcrProviderVisitCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.DCRProviderVisitDetails.modelName);
    var giftCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Gifts.modelName);
    let giftMatch={
      companyId:ObjectId(params.companyId),
      status:params.status
    }
    if(params.giftIssue==false){
      giftCollection.aggregate(
        // Stage 1
        {
          $match: giftMatch
          
        },
        // Stage 2
        {
          $project: {
            _id:0,
              id:"$_id",
              giftName:1,
              giftCode:1,
              giftCost:1
          }
        },function(err,result){
          if(err){
            return cb(err);
          }else{
            if(result.length>0){
              return cb(null, result);
            }else{
              return cb(null, []);
            }
          }

        }
      );

    }else{
      async.parallel({
        giftData:(callback)=> {
          giftCollection.aggregate(
            // Stage 1
            {
              $match: giftMatch
              
            },
            // Stage 2
            {
              $project: {
                  _id:1,
                  giftName:1,
                  giftCode:1,
                  giftCost:1
              }
            },function(err,result){
              if(err){
                return cb(err);
              }else{
                if(result.length>0){
                  callback(null, result);
                }else{
                  callback(null, []);
                }
              }

            }
          );
        },
        sampleDetail:function(cb){
          let sampleParams={companyId:ObjectId(params.companyId),userId:ObjectId(params.userId),"type" : "gift"}
          SampleCollection.aggregate(

            {
              $match: sampleParams
            },
                    // Stage 2
            {
              $sort:
              {
                'issueDate': 1,
              },
            },
            {
              $group: {
                _id: {
                  productId: '$productId',
                },
                balance: {
                  $last: '$balance',
                },
                rowId: {
                  $last: '$_id',
                },
              },
            },
      
            {
              $project: {
                _id: '$_id.productId',
                sampleIssueBalance: '$balance',
      
              },
            }, function(err, res) {
              if (err) {
                sendMail.sendMail({collectionName: 'Samplegiftissuedetail', errorObject: err, paramsObject: params, methodName: 'getSampleBalance'});
              }
             cb(false, res);
            }
                );
        }
     },function(err,asyResponse){
       if(err){
         return cb(err)
       }else{
         let finalData = [];
         if(asyResponse.giftData.length>0){
          asyResponse.giftData.forEach(item => {
            let filterObj=asyResponse.sampleDetail.filter(function(sampleObj){
              return sampleObj._id.toString()==item._id.toString();
           });
           if (filterObj.length > 0) {
             finalData.push({
              id:item._id,
              giftName:item.giftName,
              giftCode:item.giftCode,
              giftCost:item.giftCost,
              giftIssueBalance:filterObj[0].sampleIssueBalance,
             })
           }else{
             finalData.push({
              id:item._id,
              giftName:item.giftName,
              giftCode:item.giftCode,
              giftCost:item.giftCost,
              giftIssueBalance:0,
              })
           }
            });   

            
            console.log(finalData)
     
           return cb(false,finalData)
              }else{
                return cb(false,[])
              }
         }

     })
    }
   

  };
  Samplegiftissuedetail.remoteMethod(
        'getGiftDetailWithBalance', {
          description: 'Get Gift and Its Balance Details',
          accepts: [{
            arg: 'params',
            type: 'object',
          }],
          returns: {
            root: true,
            type: 'array',
          },
          http: {
            verb: 'get',
          },
        }
    );
  //------------------------------End-------------------------------------------------------
  //-----get Receipt, Issue, Balanace of gift issue annualy of selected user by preeti arora 27-10-2020----------
  Samplegiftissuedetail.getRIBOfGiftIssue = function(params, cb) {
    var self = this;
    var SampleCollection = self.getDataSource().connector.collection(Samplegiftissuedetail.modelName);
    var giftCollection=self.getDataSource().connector.collection(Samplegiftissuedetail.app.models.Gifts.modelName);

    let filter={
           companyId: ObjectId(params.companyId),
            userId: ObjectId(params.userId),
            year: params.year,
            "type" : "gift" 
          }
    let giftMatch={
            companyId: ObjectId(params.companyId),
            status:true
          }
    async.parallel({
      IssueData:(cb)=>{
        SampleCollection.aggregate(
          // Stage 1
          {
            $match: filter
          },
      
          // Stage 2
          {
            $lookup: {
                "from" : "UserInfo",
                "localField" : "userId",
                "foreignField" : "userId",
                "as" : "userData"
            }
          },
      
          // Stage 3
          {
            $unwind: "$userData"
          },
      
          // Stage 4
          {
            $lookup: {
                "from" : "District",
                "localField" : "userData.districtId",
                "foreignField" : "_id",
                "as" : "disData"
            }
          },
      
          // Stage 5
          {
            $unwind: "$disData"
          },
      
          // Stage 6
          {
            $lookup: {
                "from" : "Gifts",
                "localField" : "productId",
                "foreignField" : "_id",
                "as" : "giftData"
            }
          },
      
          // Stage 7
          {
            $unwind: "$giftData"
          },
      
          // Stage 8
          {
            $group: {
            _id:{productId:"$productId",month:"$month",year:"$year",userId:"$userId"},
            balance:{$last:"$balance"},
            opening:{$last:"$opening"},
            receipt:{$last:"$receipt"},
            dis:{$addToSet:"$disData.districtName"},
            user:{$addToSet:"$userData.name"},
            giftName:{$addToSet:"$giftData.giftName"}
            }
          },
      
          // Stage 9
          {
            $project: {
            giftId:"$_id.productId",
            giftName:{$arrayElemAt:["$giftName",0]},
            userId:"$_id.userId",
            month:"$_id.month",
            year:"$_id.year",
            balance:1,
            opening:1,
            receipt:1,
            dis:{$arrayElemAt:["$dis",0]},
            user:{$arrayElemAt:["$user",0]}
            }
          },
      
          // Stage 10
          {
            $group: {
            _id:{},
            data:{$push:{month:"$month",year:"$year",balance:"$balance",opening:"$opening",receipt:"$receipt",giftId:"$giftId",giftName:"$giftName"}},
            userId:{$push:"$userId"},
            userName:{$push:"$user"},
            dis:{$push:"$dis"},
            giftName:{$push:"$giftName"}
            }
          },
      
          // Stage 11
          {
            $project: {
              _id:0,
            district:{$arrayElemAt:["$dis",0]},
            userName:{$arrayElemAt:["$userName",0]},
            userId:{$arrayElemAt:["$userId",0]},
            giftData:"$data"
            }
          }
          ,function(err,result) {
            if(err){
             cb(err)
            }
            if(result.length){
               cb(null,result)
            }
            
          }
      );
      },
      giftData:(cb)=> {
        giftCollection.aggregate(
          // Stage 1
          {
            $match: giftMatch
            
          },
          // Stage 2
          {
            $project: {
                _id:1,
                providerCode:1,
                giftName:1
            }
          },function(err,result){
            if(err){
              return cb(err);
            }else{
              if(result.length>0){
                cb(null, result);
              }else{
                cb(null, []);
              }
            }

          }
        );
      },
    },function(err,asyResponse) {
      if(err){
        return cb(err)
      }
      if (asyResponse.giftData.length > 0) {

        let fromDate = new moment.utc(params.year + "-01-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
        let toDate = new moment.utc(params.year + "-12-31").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");
        const monthYearArray = monthNameAndYearBTWTwoDate(fromDate, toDate);
        let finalObject = [];
       
        asyResponse.giftData.forEach((item, index) => {
          let details = [];

          let i =0 ;
          for (const month of monthYearArray) {
            const matchedObject = asyResponse.IssueData[0].giftData.filter(function (obj) {
              return obj.month == month.month && obj.year == month.year && item._id.toString() == obj.giftId.toString();

            });
            if (matchedObject.length > 0) {
              details.push(matchedObject[0])
            } else {
              details.push({
                month: month.month,
                year: month.year,
                balance: 0,
                opening: 0,
                receipt: 0,
                giftId: item._id,
                giftName: item.giftName
              })

            }
          }

          finalObject.push({
            hqName: asyResponse.IssueData[0].district,
            userName: asyResponse.IssueData[0].userName,
            giftName: item.giftName,
            details,
          });
        })
        return cb(false, finalObject)
      }else{
        return cb(false,[])
      }
      
    })

  };
  Samplegiftissuedetail.remoteMethod(
        'getRIBOfGiftIssue', {
          description: 'Get RIB Of Gift Issue',
          accepts: [{
            arg: 'params',
            type: 'object',
          }],
          returns: {
            root: true,
            type: 'array',
          },
          http: {
            verb: 'get',
          },
        }
    );

  //----------------------------------------------end------------------------------------------------------------------------
  function monthNameAndYearBTWTwoDate(fromDate, toDate) {
    let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let arr = [];
    let datFrom = new Date(fromDate);
    let datTo = new Date(toDate);
    let fromYear = datFrom.getFullYear();
    let toYear = datTo.getFullYear();
    let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
    for (let i = datFrom.getMonth(); i <= diffYear; i++) {
        arr.push({
            monthName: monthNames[i % 12],
            month: (i % 12) + 1,
            year: Math.floor(fromYear + (i / 12)),
            endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
        });
    }
    return arr;
}
};
