'use strict';

module.exports = function(Longlatinfo) {

    Longlatinfo.getLatLong = function(params, cb) {
        var self = this;
        var LonglatinfoCollection = self.getDataSource().connector.collection(Longlatinfo.modelName);
        LonglatinfoCollection.aggregate(
            // Stage 1
            {
              $geoNear: {
              near : {
                          type: "Point",  
                          coordinates: [ parseFloat(params.lat), parseFloat(params.long)]
                      },
                      distanceField: "dist.calculated",
                      maxDistance: 2000,
                      includeLocs:"dist.location",
                      spherical :true
                  
              }
            },function(err, result) {
                if (err) {
                    console.log(err);
                    return cb(false, []);
                }
                if (!result) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                return cb(false, result);
            });
    }
    Longlatinfo.remoteMethod('getLatLong', {
        description: "Get Location detail",
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }

    })
      
};
