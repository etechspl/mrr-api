'use strict';
const ObjectId = require('mongodb').ObjectID;
module.exports = function(Sentitem) {
	Sentitem.getMails = function(param, cb) {

        const self = this;
        const SentItemCollection = self.getDataSource().connector.collection(Sentitem.modelName);
        SentItemCollection.aggregate({
                $match: {
                    from: ObjectId(param.loggedInId)
                }
            }, {
                $unwind: {
                    path: "$tos",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "tos",
                    "foreignField": "userId",
                    "as": "userDetailTo"
                }
            }, {
                $unwind: {
                    path: "$userDetailTo",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "userDetailTo.districtId",
                    "foreignField": "_id",
                    "as": "districtDetailTo"
                }
            }, {
                $unwind: {
                    path: "$cc",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "cc",
                    "foreignField": "userId",
                    "as": "userDetailCC"
                }
            }, {
                $unwind: {
                    path: "$userDetailCC",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "userDetailCC.districtId",
                    "foreignField": "_id",
                    "as": "districtDetailCC"
                }
            }, {
                $unwind: {
                    path: "$bcc",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "bcc",
                    "foreignField": "userId",
                    "as": "userDetailBCC"
                }
            }, {
                $unwind: {
                    path: "$userDetailBCC",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "userDetailBCC.districtId",
                    "foreignField": "_id",
                    "as": "districtDetailBCC"
                }
            }, {
                $group: {
                    _id: {
                        From: "$from",
                        mailDate: "$mailDate"
                    },
                    To: {
                        $addToSet: {
                            id: "$userDetailTo.userId",
                            name: "$userDetailTo.name",
                            designation: "$userDetailTo.designation",
                            district: { $arrayElemAt: ["$districtDetailTo.districtName", 0] }
                        }
                    },
                    CC: {
                        $addToSet: {
                            id: "$userDetailCC.userId",
                            name: "$userDetailCC.name",
                            designation: "$userDetailCC.designation",

                            district: { $arrayElemAt: ["$districtDetailCC.districtName", 0] }
                        }
                    },
                    BCC: {
                        $addToSet: {
                            id: "$userDetailBCC.userId",
                            name: "$userDetailBCC.name",
                            designation: "$userDetailBCC.designation",
                            district: { $arrayElemAt: ["$districtDetailBCC.districtName", 0] }
                        }
                    },
                    subject: {
                        $addToSet: "$subject"
                    },
                    message: {
                        $addToSet: "$message"
                    },
                    time: {
                        $addToSet: "$mailDate"
                    },
                    attachments: {
                        $addToSet: "$fileId"
                    },
                    readStatus: {
                        $addToSet: "$readStatus"
                    },
                    mailId: { $addToSet: "$_id" }
                }
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "_id.From",
                    "foreignField": "userId",
                    "as": "fromUser"
                }
            }, {
                $unwind: {
                    path: "$fromUser",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "fromUser.districtId",
                    "foreignField": "_id",
                    "as": "districtFromUser"
                }
            }, {
                $project: {
                    _id: 0,
                    fromId: "$_id.From",
                    fromName: "$fromUser.name",
                    fromUserDesignation: "$fromUser.designation",
                    fromUserDistrict: { $arrayElemAt: ["$districtFromUser.districtName", 0] },
                    To: 1,
                    CC: 1,
                    BCC: 1,
                    Subject: { $arrayElemAt: ["$subject", 0] },
                    Message: { $arrayElemAt: ["$message", 0] },
                    MailDateTime: { $arrayElemAt: ["$time", 0] },
                    attachments: { $arrayElemAt: ["$attachments", 0] },
                    readStatus: { $arrayElemAt: ["$readStatus", 0] },
                    readColour: {
                        $cond: [{ $gte: [{ $arrayElemAt: ["$readStatus", 0] }, true] }, "#ebedf2", "#fff"]
                    },
                    mailId: { $arrayElemAt: ["$mailId", 0] }
                }
            }, {
                $sort: {
                    MailDateTime: -1
                }
            },

            function(err, records) {
                if (err) {
                    return cb(err);
                }

                const newResult = records.map(mapResult => {
                    mapResult.To = mapResult.To.filter(value => Object.keys(value).length !== 0)
                    mapResult.CC = mapResult.CC.filter(value => Object.keys(value).length !== 0)
                    mapResult.BCC = mapResult.BCC.filter(value => Object.keys(value).length !== 0)

                    return mapResult;
                });
                return cb(false, newResult);
            });

    }
    Sentitem.remoteMethod(
        'getMails', {
            description: 'get all sent mail',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: { root: true, type: 'array' },
            http: { verb: 'get' }
        }
    );
};
