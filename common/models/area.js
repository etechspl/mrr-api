'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
const moment = require('moment');

module.exports = function (Area) {
       
	   
	 //----------------------------Preeti Arora(12-07-2019)---------------------------------

	 Area.observe('after save', function(ctx, next) {
        if (ctx.instance === undefined) {
            next()
        }else{
            let areaObj = JSON.parse(JSON.stringify(ctx.instance));
            Area.update({id:areaObj.id},{areaStringId:areaObj.id},function(err, expenseRes) {
                if(err){
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: 'after save', methodName: 'observe'});
                }
                next();
            })
        }
    });
	//------------------------------
	    //----------------------------Praveen Kumar(05-12-2018)---------------------------------

	Area.getAreaList = function (parameters, cb) {
        var self = this;
        var AreaCollection = self.getDataSource().connector.collection(Area.modelName);

        if (parameters.designationLevel == 0) {
            self.find({
                where: {
                    "companyId": parameters.companyId,
                    "districtId": {
                        "inq": parameters.districtIds
                    },
				"appStatus": "approved",
                "status": true
                },
                "order": "areaName ASC",
                "include": ["state", "district"]
            }, function (err, assignedAreaResult) {
                if (err) {
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: parameters, methodName: 'getAreaList'});
                    // console.log(err)
                }
                let passingResult = []

                for (let i = 0; i < assignedAreaResult.length; i++) {
                    let jsonAssignedAreaResult = JSON.parse(JSON.stringify(assignedAreaResult[i]));
                    //let jsonAssignedAreaResult = JSON.parse(stringfyAssignedAreaResult)
                    passingResult.push({
                        stateId: jsonAssignedAreaResult.state.id,
                        stateName: jsonAssignedAreaResult.state.stateName,
                        districtId: jsonAssignedAreaResult.district.id,
                        districtName: jsonAssignedAreaResult.district.districtName,
                        id: jsonAssignedAreaResult.id,
                        areaName: jsonAssignedAreaResult.areaName,
                        type: jsonAssignedAreaResult.type,
                    });
                }
                cb(false, passingResult);

            })
        } else if (parameters.designationLevel >= 2) {
            Area.app.models.Hierarchy.find({
                where: {
                    companyId: parameters.companyId,
                    supervisorId: { inq: parameters.userIds },
                     userDesignationLevel: 1,
                    //userDesignation: { $in: ["SE", "MR"] },                   
                    status: true
                }
            }, function (err, hrcyResult) {
                if (err) {
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: parameters, methodName: 'getAreaList'});
                    // console.log(err);
                }
                let passingMRUserIds = [];
                for (let i = 0; i < hrcyResult.length; i++) {
                    passingMRUserIds.push(hrcyResult[i].userId);
                }
                passingMRUserIds.push(parameters.userIds[0]);

                Area.app.models.UserAreaMapping.find({
                    where: {
                        companyId: parameters.companyId,
                        userId: {
                            inq: passingMRUserIds
                        },
                        districtId: { inq: parameters.districtIds },
                        status: true,
                    },
                    order: "areaName ASC",
                    include: ["state", "district", "areaInfo"]
                }, function (err, assignedAreaResult) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: parameters, methodName: 'getAreaList'});
                        // console.log(err);
                    }
                    let passingResult = []
                    for (let i = 0; i < assignedAreaResult.length; i++) {
                        let jsonAssignedAreaResult = JSON.parse(JSON.stringify(assignedAreaResult[i]));
                        passingResult.push({
                            stateId: jsonAssignedAreaResult.state.id,
                            stateName: jsonAssignedAreaResult.state.stateName,
                            districtId: jsonAssignedAreaResult.district.id,
                            districtName: jsonAssignedAreaResult.district.districtName,
                            id: jsonAssignedAreaResult.areaInfo.id,
                            areaName: jsonAssignedAreaResult.areaInfo.areaName,
                            type: jsonAssignedAreaResult.areaInfo.type,
                        });
                    }
                    cb(false, passingResult);
                });
            });
			
        } else if (parameters.designationLevel == 1) {
            let userIdArr = [];
            for (var w in parameters.userIds) {
                userIdArr.push(parameters.userIds[w]);
            }
            Area.app.models.UserAreaMapping.find({
                where: {
                    companyId: parameters.companyId,
                    userId: {
                        inq: userIdArr
                    },
                    status: true,
                },
                order: "areaName ASC",
                include: ["state", "district", "areaInfo"]
            }, function (err, assignedAreaResult) {
                if (err) {
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: parameters, methodName: 'getAreaList'});
                    // console.log(err);
                }
                let passingResult = []
                for (let i = 0; i < assignedAreaResult.length; i++) {
                    let jsonAssignedAreaResult = JSON.parse(JSON.stringify(assignedAreaResult[i]));
					// console.log("Area Info : ",jsonAssignedAreaResult.areaInfo);
                    passingResult.push({
                        stateId: jsonAssignedAreaResult.state.id,
                        stateName: jsonAssignedAreaResult.state.stateName,
                        districtId: jsonAssignedAreaResult.district.id,
                        districtName: jsonAssignedAreaResult.district.districtName,
                        id: jsonAssignedAreaResult.areaInfo.id,
                        areaName: jsonAssignedAreaResult.areaInfo.areaName,
                        type: jsonAssignedAreaResult.areaInfo.type,
                    });
                }
                cb(false, passingResult);
            });
        }
    };
    Area.remoteMethod(
        'getAreaList', {
            description: 'get Area List Basis',
            accepts: [{
                arg: 'parameters',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        });


    Area.editArea = function (params, cb) {
        var self = this;
        var AreaCollection = this.getDataSource().connector.collection(Area.modelName);

        Area.find({
            where: {
                companyId: ObjectId(params.companyId),
                _id: ObjectId(params.id)
            }
        }, function (err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'editArea'});
            }
            if (response.length > 0) {
                AreaCollection.update({
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params.id)
                }, {
                        $set:
                        {
                            areaName: params.areaName,
                            areaCode: params.areaCode,
                            type: params.type,
                            updatedBy: ObjectId(params.updatedBy),
                            updatedAt: new Date()
                        }
                    }, function (err, res) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'editArea'});
                            // console.log(err);
                            return cb(err);
                        } else {
                            return cb(false, res)
                        }
                    })

            } else {
                var err = new Error('Area cannot be Edit.');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            }
        })



    }


    Area.remoteMethod(
        'editArea', {
            description: 'editArea',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
    
    Area.deleteArea = function (params, cb) {
        var self = this;
        var AreaCollection = this.getDataSource().connector.collection(Area.modelName);
        let aSet = {};
        if (params.rL === 1) {
			//---future we will add delete providers entry before approval
            if (params.validationRole > 0) {
				if(params.appStatus =="pending"){
					 aSet = {
                    status: false,
                    delStatus: 'approved',
                    mgrDelDate: new Date(),
                    finalDelDate: new Date(),
                    delByMgr: ObjectId(params.updatedBy),
                    finalDelBy: ObjectId(params.updatedBy)
                };
				}
				  else{
					  aSet = {
                    delStatus: 'pending',
                    mgrDelDate: new Date('1900-01-02T 00:00:00'),
                    finalDelDate: new Date('1900-01-02T 00:00:00'),
                    delByMgr: '',
                    finalDelBy: ''
                };  
				  }
            }
			else {
                aSet = {
                    status: false,
                    delStatus: 'approved',
                    mgrDelDate: new Date(),
                    finalDelDate: new Date(),
                    delByMgr: ObjectId(params.updatedBy),
                    finalDelBy: ObjectId(params.updatedBy)
                };
            }
        } else if(params.rL === 2){
            if (params.validationRole == 0 || params.validationRole == 1) {
                aSet = {
                    status: false,
                    delStatus: 'approved',
                    deleteReason:params.deleteReason,
                    mgrDelDate: new Date(),
                    finalDelDate: new Date(),
                    delByMgr: ObjectId(params.updatedBy),
                    finalDelBy: ObjectId(params.updatedBy)
                };
            } else if (params.validationRole == 2) {
                aSet = {
                    delStatus: 'InProcess',
                    deleteReason:params.deleteReason,
                    mgrDelDate: new Date(),
                    finalDelDate: new Date('1900-01-02T 00:00:00'),
                    delByMgr: ObjectId(params.updatedBy),
                    finalDelBy: ''
                };
            }
        }else if(params.rL === 0){
            aSet = {
                status: false,
                delStatus: 'approved',
                mgrDelDate: new Date(),
                finalDelDate: new Date(),
                delByMgr: ObjectId(params.updatedBy),
                finalDelBy: ObjectId(params.updatedBy)
            };
        }
        Area.find({
            where: {
                companyId: ObjectId(params.companyId),
                _id: ObjectId(params.areaId),
               // status: true
            }
        }, function (err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'deleteArea'});
            }
            if (response.length > 0) {
				// console.log("aset--",aSet)
				// console.log("response--",response)

			AreaCollection.update({
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params.areaId)
                }, {
                        $set:
                            aSet
                    }, function (err, res) {
                        if (err) {
                            return cb(err);
                        } else {
                            return cb(false, res)
                        }
                    });               

            } else {
                var err = new Error('Area cannot be deleted.');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            }
        })
    }
    Area.remoteMethod(
        'deleteArea', {
            description: 'delete Area',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
    
    Area.approveArea = function (params, cb) {
        var self = this;
        var AreaCollection = this.getDataSource().connector.collection(Area.modelName);
        let aSet = {};
        if (params.requestType === 'approve') {
            if (params.rL > 1) {
                if (params.validationRole === 1) {
                    aSet = {
                        status: true,
                        appStatus: 'approved',
                        mgrAppDate: new Date(),
                        finalAppDate: new Date(),
                        appByMgr: ObjectId(params.updatedBy),
                        finalAppBy: ObjectId(params.updatedBy)
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        appStatus: 'InProcess',
                        mgrAppDate: new Date(),
                        finalAppDate: new Date('1900-01-02T 00:00:00'),
                        appByMgr: ObjectId(params.updatedBy),
                        finalAppBy: ''
                    };
                }

            } else {
                if (params.validationRole === 1) {
                    aSet = {
                        status: true,
                        appStatus: 'approved',
                        mgrAppDate: new Date(),
                        finalAppDate: new Date(),
                        appByMgr: ObjectId(params.updatedBy),
                        finalAppBy: ObjectId(params.updatedBy)
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        status: true,
                        appStatus: 'approved',
                        finalAppDate: new Date(),
                        finalAppBy: ObjectId(params.updatedBy)
                    };
                }
            }
        } else if (params.requestType === 'delete') {
            if (params.rL > 1) {
                if (params.validationRole === 1) {
                    aSet = {
                        status: false,
                        delStatus: 'approved',
                        mgrDelDate: new Date(),
                        finalDelDate: new Date(),
                        delByMgr: ObjectId(params.updatedBy),
                        finalDelBy: ObjectId(params.updatedBy)
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        appStatus: 'InProcess',
                        mgrDelDate: new Date(),
                        finalDelDate: new Date('1900-01-02T 00:00:00'),
                        delByMgr: ObjectId(params.updatedBy),
                        finalDelBy: ''
                    };
                }

            } else {
                if (params.validationRole === 1) {
                    aSet = {
                        status: false,
                        delStatus: 'approved',
                        mgrDelDate: new Date(),
                        finalDelDate: new Date(),
                        delByMgr: ObjectId(params.updatedBy),
                        finalDelBy: ObjectId(params.updatedBy)
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        status: false,
                        delStatus: 'approved',
                        finalDelDate: new Date(),
                        finalDelBy: ObjectId(params.updatedBy)
                    };
                }
            }
        }
        else if (params.requestType === 'rejectAddRequest') {
            if (params.rL > 1) {
                if (params.validationRole === 1) {
                    aSet = {
                rejectionAddDate: new Date(),
                rejectedBy: ObjectId(params.updatedBy),
                rejectionMessage: params.rejectionMessage,
                rejectAddStatus:true,
                updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        appStatus: 'InProcess',
                        rejectionAddDate: new Date(),
                        rejectedBy: ObjectId(params.updatedBy),
                        rejectionMessage: params.rejectionMessage,
                        rejectAddStatus:true,
                        updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                    };
                }
            } else {
                if (params.validationRole === 1) {
                    aSet = {
                        rejectionAddDate: new Date(),
                rejectedBy: ObjectId(params.updatedBy),
                rejectionMessage: params.rejectionMessage,
                rejectAddStatus:true,
                updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                    };
                } else if (params.validationRole === 2) {
                    aSet = {
                        appStatus: 'InProcess',
                        rejectionAddDate: new Date(),
                        rejectedBy: ObjectId(params.updatedBy),
                        rejectionMessage: params.rejectionMessage,
                        rejectAddStatus:true,
                        updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                    };
                }
            }
        }
        AreaCollection.update({
            companyId: ObjectId(params.companyId),
            _id: { $in: params.areaIds }
        }, {
                $set:
                    aSet
            }, {
                multi: true
            }, function (err, res) {
                if (err) {
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'approveArea'});
                    return cb(err);
                }
                let result = {
                    status: res.result.nModified
                }
                cb(false, result);
            })

    }
    Area.remoteMethod(
        'approveArea', {
            description: 'approveArea ',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
	
	  //getting daily Allouwance if stp is not exist based on the area type
   //getting daily Allouwance if stp is not exist based on the area type
  Area.getAreaDailyAllowance = function(params, cb) {
    var self = this;
    var AreaCollection = self.getDataSource().connector.collection(Area.modelName);
    let daMatch={
        "da.companyId": ObjectId(params.companyId),
        "da.designation":params.designation
    }
    if(params.hasOwnProperty("userId")){
      daMatch["da.userId"]=ObjectId(params.userId)
    }

    AreaCollection.aggregate({
        $match: {
          companyId: ObjectId(params.companyId),
          stateId: ObjectId(params.stateId),
          _id: ObjectId(params.areaId)
        }
      }, {
        $lookup: {
          "from": "DailyAllowance",
          "localField": "stateId",
          "foreignField": "stateId",
          "as": "da"
        }
      }, {
        $unwind: "$da"
       },
    {
        $match: daMatch
      }, {
        $project: {
          type: "$type",
          da: {
            $cond: {
              if: {
                $or: [{
                    $eq: ["$type", "HQ"]
                  },
                  {
                    $eq: ["$type", "hq"]
                  },
                  {
                    $eq: ["$type", "Hq"]
                  },
                  {
                    $eq: ["$type", "hQ"]
                  },
                ]

              },
              then: "$da.daHQ",
              else: {
                $cond: {
                  if: {
                    $or: [{
                        $eq: ["$type", "EX"]
                      },
                      {
                        $eq: ["$type", "Ex"]
                      },
                      {
                        $eq: ["$type", "eX"]
                      },
                      {
                        $eq: ["$type", "ex"]
                      }
                    ]

                  },
                  then: "$da.daEX",

                  else: "$da.daOUT"
                }
              }
            }
          }
        }
      },
       {
        cursor: {
          batchSize: 50
        },

        allowDiskUse: true
      },
      function(err, result) {
          if(err){
            sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowance'});
          }
        return cb(false, result);
      });

  };

 
	Area.remoteMethod(
    'getAreaDailyAllouwane', {
      description: 'get Area Daily Allouwance',
      accepts: [{
        arg: 'parameters',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }
    });
	
	Area.getAreaDailyAllowanceForCategory = function(params, cb) {
        var self = this;
        var AreaCollection = self.getDataSource().connector.collection(Area.modelName);
        AreaCollection.aggregate({
                $match: {
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params.areaId)
                }
            }, {
                cursor: {
                    batchSize: 50
                },

                allowDiskUse: true
            },
            function(err, result) {
                if(err){
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowanceForCategory'});
                }
                //return cb(false, result);
                if (result.length > 0) {
                    let areaType;
                    if(result[0].type !=null){
                     areaType = result[0].type.toLowerCase();
                    }else{
                        areaType='ex'
                    }
                    Area.app.models.DailyAllowance.find({
                        where: {
                            companyId: params.companyId,
                            type: params.daType,
                            category: params.category,
                            divisionId: params.divisionId
                        }
                    }, function(err, resp) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowanceForCategory'});
                            var err = new Error('records is undefined or empty');
                            err.statusCode = 404;
                            err.code = 'RECORDS ARE NOT FOUND';
                            return cb(err);
                        }
                        let arrRes = [];
                        let obj = {};
                        // console.log("Area Type is ", areaType);
                        if (areaType === 'hq') {
                            obj["da"] = resp[0].daHQ;
                            obj["type"] = areaType;
                        } else if (areaType === 'ex') {
                            obj["da"] = resp[0].daEX;
                            obj["type"] = areaType;
                        } else if (areaType === 'out') {
                            obj["da"] = resp[0].daOUT;
                            obj["type"] = areaType;
                        } else {
                            obj["da"] = 0;
                            obj["type"] = "";
                        }
                        arrRes.push(obj)
                        return cb(false, arrRes);
                    })
                }

            });

    };


    Area.remoteMethod(
	'getAreaDailyAllowanceForCategory', {
		description: 'get Area Daily Allouwance',
		accepts: [{
			arg: 'parameters',
			type: 'object'
		}],
		returns: {
			root: true,
			type: 'array'
		},
		http: {
			verb: 'get'
		}
	});

    Area.getAreaDailyAllowanceForCategoryReplica = function(params){
        return new Promise((resolve, reject) => {
            Area.find({
                where: {
                    companyId: params.companyId,
                    _id: params.areaId
                }
            },
            function(err, result) {
                if(err){
                    sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowanceForCategory'});
                }
                //return cb(false, result);
                if (result.length > 0) {
                    let areaType = result[0].type.toLowerCase();
                    Area.app.models.DailyAllowance.find({
                        where: {
                            companyId: params.companyId,
                            type: params.daType,
                            category: params.category,
                            divisionId: params.divisionId
                        }
                    }, function(err, resp) {
                        if (err) {
                            sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowanceForCategory'});
                            var err = new Error('records is undefined or empty');
                            err.statusCode = 404;
                            err.code = 'RECORDS ARE NOT FOUND';
                            return(err);
                        }
                        let arrRes = [];
                        let obj = {};
                        // console.log("Area Type is ", areaType);
                        if (areaType === 'hq') {
                            obj["da"] = resp[0].daHQ;
                            obj["type"] = areaType;
                        } else if (areaType === 'ex') {
                            obj["da"] = resp[0].daEX;
                            obj["type"] = areaType;
                        } else if (areaType === 'out') {
                            obj["da"] = resp[0].daOUT;
                            obj["type"] = areaType;
                        } else {
                            obj["da"] = 0;
                            obj["type"] = "";
                        }
                        arrRes.push(obj)
                        resolve(arrRes)  ;
                    })
                }

            });
            
          })
    };
    Area.remoteMethod(
	'getAreaDailyAllowanceForCategoryReplica', {
		description: 'get Area Daily Allouwance replica',
		accepts: [{
			arg: 'parameters',
			type: 'object'
		}],
		returns: {
			root: true,
			type: 'array'
		},
		http: {
			verb: 'get'
		}
	});

    Area.getAreaDailyAllowanceReplica = function(params) {
        return new Promise((resolve, reject) => {
        var self = this;
        
        var AreaCollection = params.connection;
        
    
        let daMatch={
            "da.companyId": ObjectId(params.companyId),
            "da.designation":params.designation
        }
        if(params.hasOwnProperty("userId")){
          daMatch["da.userId"]=ObjectId(params.userId)
        }
    
        AreaCollection.aggregate({
            $match: {
              companyId: ObjectId(params.companyId),
              stateId: ObjectId(params.stateId),
              _id: ObjectId(params.areaId)
            }
          }, {
            $lookup: {
              "from": "DailyAllowance",
              "localField": "stateId",
              "foreignField": "stateId",
              "as": "da"
            }
          }, {
            $unwind: "$da"
           },
        {
            $match: daMatch
          }, {
            $project: {
              type: "$type",
              da: {
                $cond: {
                  if: {
                    $or: [{
                        $eq: ["$type", "HQ"]
                      },
                      {
                        $eq: ["$type", "hq"]
                      },
                      {
                        $eq: ["$type", "Hq"]
                      },
                      {
                        $eq: ["$type", "hQ"]
                      },
                    ]
    
                  },
                  then: "$da.daHQ",
                  else: {
                    $cond: {
                      if: {
                        $or: [{
                            $eq: ["$type", "EX"]
                          },
                          {
                            $eq: ["$type", "Ex"]
                          },
                          {
                            $eq: ["$type", "eX"]
                          },
                          {
                            $eq: ["$type", "ex"]
                          }
                        ]
    
                      },
                      then: "$da.daEX",
    
                      else: "$da.daOUT"
                    }
                  }
                }
              }
            }
          },
           {
            cursor: {
              batchSize: 50
            },
    
            allowDiskUse: true
          },
          function(err, result) {
              if(err){
                sendMail.sendMail({collectionName: "Area", errorObject: err, paramsObject: params, methodName: 'getAreaDailyAllowance'});
              }
            resolve (result);
          });
    
        })
    
    
    
      };
    
     
        Area.remoteMethod(
        'getAreaDailyAllouwaneReplica', {
          description: 'get Area Daily Allouwance Replica',
          accepts: [{
            arg: 'parameters',
            type: 'object'
          }],
          returns: {
            root: true,
            type: 'array'
          },
          http: {
            verb: 'get'
          }
        });

};

