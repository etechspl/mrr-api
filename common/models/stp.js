'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
const geolib = require('geolib');
const _1KM = 1000;
var NodeGeocoder = require('node-geocoder');

module.exports = function(Stp) {
  
  
    Stp.updateAllRecord = function(records, cb) {
      var self = this;
      var StpCollection = self.getDataSource().connector.collection(Stp.modelName);
      if (records.length === 0 || records === "") {
        var err = new Error('records is undefined or empty');
        err.statusCode = 404;
        err.code = 'RECORDS ARE NOT FOUND';
        return cb(err);
      }

      let returnData = records;
      for (let w = 0; w < records.length; w++) {

        let set = {};
        set = records[w];

        //set["frcId"] = ObjectId(set["frcId"]);
	    	set["updatedAt"]=new Date();

        if (set.hasOwnProperty("appByMgr") === true) {
          set["appByMgr"] = ObjectId(set["appByMgr"]);
        }
        if (set.hasOwnProperty("appByAdm") === true) {
          set["appByAdm"] = ObjectId(set["appByAdm"]);
        }

        if (set.hasOwnProperty("mgrAppDate") === true) {
          set["mgrAppDate"] = new Date(set["mgrAppDate"]);
        }

        if (set.hasOwnProperty("finalAppDate") === true) {
          set["finalAppDate"] = new Date(set["finalAppDate"]);
        }

        StpCollection.update({
          "_id": ObjectId(records[w].id)
        }, {
          $set: set
        },
        function(err, result) {
          if (err) {
            sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: records, methodName: 'updateAllRecord' });
            console.log(err);
          }

        })
    }

    return cb(false, returnData);

  };


  Stp.remoteMethod(
    'updateAllRecord', {
      description: 'Update the STP Record and return all the data',
      accepts: [{
        arg: 'records',
        type: 'array',
        http: {
          source: 'body'
        }
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'post'
      }
    }
  );
  
 //updated by Rahul Choudhary 08-06-2020 For required Division filter
  Stp.getStpApprovalRequest = function(params, cb) {
        var self = this;
        var StpCollection = self.getDataSource().connector.collection(Stp.modelName);
     
        if (params.obj.type === 'state') {
          let filter = {};
          if(params.obj.isDivisionExist == true){
      
          let div = [];
          for (var i = 0; i < params.obj.division.length; i++) {
              div.push(ObjectId(params.obj.division[i]));
          }
         filter = {
            companyId: ObjectId(params.obj.companyId),
            divisionId: {inq : div},
            status : true
        }
      }else{
        filter = {
          companyId: ObjectId(params.obj.companyId),
          status : true
      }
      }
          Stp.app.models.UserInfo.find({
            where: filter,
        },
            function (err, response) {
                if (err) {
                  sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
                    //  console.log(err);
                }
    
                let userIds = [];
                for (var i = 0; i < response.length; i++) {
                    userIds.push(ObjectId(response[i].userId))
                }

              StpCollection.aggregate({
                $match: {
                    companyId: ObjectId(params.obj.companyId),
                    appByAdm: '',
					          status: false,
                    delStatus: {$ne : "approved"},
                    userId : {$in : userIds}
                }
            }, {
                $group: {
                    _id: {
                        "stateId": "$stateId",
                        "districtId": "$districtId"
                    },
                    request: {
                        $addToSet: "$userId"
                    }
                }
            }, {
                $lookup: {
                    "from": "State",
                    "localField": "_id.stateId",
                    "foreignField": "_id",
                    "as": "states"
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "_id.districtId",
                    "foreignField": "_id",
                    "as": "district"
                }
            }, {
                $project: {
                    _id: 0,
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    stateName: {
                        $arrayElemAt: ["$states.stateName", 0]
                    },
                    districtName: {
                        $arrayElemAt: ["$district.districtName", 0]
                    },
                    request: {
                        $size: "$request"
                    }

                }
            }, {
                $sort: {
                    stateName: 1,
                    districtName: 1
                }
            }, {
                cursor: {
                    batchSize: 50
                },
                allowDiskUse: true
            }, function(err, result) {
              if(err){
                sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
              }
                return cb(false, result)
            })
          })
        } else if (params.obj.type === 'user') {
          let filter = {};
          if(params.obj.isDivisionExist == true){
          let div = [];
      for (var i = 0; i < params.obj.division.length; i++) {
          div.push(ObjectId(params.obj.division[i]));
      }
        filter = {
        companyId: ObjectId(params.obj.companyId),
        stateId: ObjectId(params.obj.stateId),
        districtId :ObjectId(params.obj.districtId), 
        divisionId: {inq : div},
        status : true
    }
  }else{
    filter = {
      companyId: ObjectId(params.obj.companyId),
      stateId: ObjectId(params.obj.stateId),
      districtId :ObjectId(params.obj.districtId),
      status : true
  }
  }
    Stp.app.models.UserInfo.find({
      where: filter,
  },
      function (err, response) {
          if (err) {
            sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
              //  console.log(err);
          }

          let userIds = [];
          for (var i = 0; i < response.length; i++) {
              userIds.push(ObjectId(response[i].userId))
          }

            StpCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.obj.companyId),
                        appByAdm: '',
                        stateId: ObjectId(params.obj.stateId),
						            status: false,
                        delStatus: {$ne : "approved"},
                        userId : {$in : userIds}
                    }
                }, {
                    $group: {
                        _id: {
                            "stateId": "$stateId",
                            "districtId": "$districtId",
                            "userId": "$userId"
                        },
                        request: {
                            $sum: 1
                        }
                    }
                }, {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "states"
                    }
                }, {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "district"
                    }
                }, {
                    $project: {
                        _id: 0,
                        stateId: "$_id.stateId",
                        districtId: "$_id.districtId",
                        userId: "$_id.userId",

                        stateName: {
                            $arrayElemAt: ["$states.stateName", 0]
                        },
                        districtName: {
                            $arrayElemAt: ["$district.districtName", 0]
                        },
                        request: 1
                    }
                }, {
                    $lookup: {
                        "from": "UserInfo",
                        "localField": "userId",
                        "foreignField": "userId",
                        "as": "users"
                    }
                }, {
                    $unwind: {
                        path: "$users",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $unwind: {
                        path: "$users.divisionId",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $lookup: {
                        "from": "DivisionMaster",
                        "localField": "users.divisionId",
                        "foreignField": "_id",
                        "as": "division"
                    }
                }, {
                    $project: {
                        name: "$users.name",
                        designation: "$users.designation",
                        _id: 0,
                        stateId: 1,
                        districtId: 1,
                        userId: 1,
                        stateName: 1,
                        districtName: 1,
                        request: 1,
                        divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                        divisionId: { $arrayElemAt: ["$division._id", 0] },
                        daCategory: "$users.daCategory"
                    }
                }, {
                    $sort: {
                        stateName: 1,
                        districtName: 1,
                        name: 1
                    }
                }, {
                    cursor: {
                        batchSize: 50
                    },
                    allowDiskUse: true
                },
                function(err, result) {
                  if(err){
                    sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
                  }
                    return cb(false, result)
                })
              })
        } else if (params.obj.type === "hierarchy") {
          
            Stp.app.models.Hierarchy.getManagerHierarchyInArray({
                companyId: params.obj.companyId,
                supervisorId: [params.obj.userId]
            }, function(err, result) {
              if(err){
                sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
              }
                let userIds = [];
                if (result.length > 0) {
                    for (const userId of result[0].userId) {
                        userIds.push(userId)
                    }

                    StpCollection.aggregate({
                        $match: {
                            companyId: ObjectId(params.obj.companyId),
                            appByMgr: '',
                            appByAdm: '',
                            userId: {
                                $in: userIds
                            },
							              status: false,
                    delStatus: {$ne : "approved"}
                        }
                    }, {
                        $group: {
                            _id: {
                                "stateId": "$stateId",
                                "districtId": "$districtId",
                                "userId": "$userId"
                            },
                            request: {
                                $sum: 1
                            }
                        }
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "states"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "_id.districtId",
                            "foreignField": "_id",
                            "as": "district"
                        }
                    }, {
                        $project: {
                            _id: 0,
                            stateId: "$_id.stateId",
                            districtId: "$_id.districtId",
                            userId: "$_id.userId",
                            stateName: {
                                $arrayElemAt: ["$states.stateName", 0]
                            },
                            districtName: {
                                $arrayElemAt: ["$district.districtName", 0]
                            },
                            request: 1
                        }
                    }, {
                        $lookup: {
                            "from": "UserInfo",
                            "localField": "userId",
                            "foreignField": "userId",
                            "as": "users"
                        }
                    }, {
                        $unwind: {
                            path: "$users",
                            preserveNullAndEmptyArrays: true
                        }
                    }, {
                        $unwind: {
                            path: "$users.divisionId",
                            preserveNullAndEmptyArrays: true
                        }
                    }, {
                        $lookup: {
                            "from": "DivisionMaster",
                            "localField": "users.divisionId",
                            "foreignField": "_id",
                            "as": "division"
                        }
                    }, {
                        $project: {
                            name: "$users.name",
                            designation: "$users.designation",
                            _id: 0,
                            stateId: 1,
                            districtId: 1,
                            userId: 1,
                            stateName: 1,
                            districtName: 1,
                            request: 1,
                            divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                            divisionId: { $arrayElemAt: ["$division._id", 0] },
                            daCategory: "$users.daCategory"
                        }
                    }, {
                        $sort: {
                            stateName: 1,
                            districtName: 1,
                            name: 1
                        }
                    }, {
                        cursor: {
                            batchSize: 50
                        },
                        allowDiskUse: true
                    }, function(err, result) {
                      if(err){
                        sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequest' });
                      }
                        return cb(false, result)
                    })
                }


            })
        }


    }

  Stp.remoteMethod(
    'getStpApprovalRequest', {
      description: 'get Users based on stateId',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }
    }
  );
  
  
   //------------------------------Preeti --getting distance between lat longs--------------------------------//
  Stp.getDistanceBasedOnArea = function (params, cb) {
    var options = {
      provider: 'google',
      // Optional depending on the providers
      httpAdapter: 'https', // Default
      apiKey: 'AIzaSyCKCxPLVooXnPEFe17z4a65p-7DpJmsA2o', // for Mapquest, OpenCage, Google Premier
      formatter: null // 'gpx', 'string', ...
    };
    let fromAreaUpdated = "";
    let toAreaUpdated = "";
    let areas = [];
    var areaIds = [params.fromArea, params.toArea]
    Stp.app.models.Area.find({
      where: {
        id: { inq: areaIds }
      },
      "include": {
        "relation": "district",
        "scope": {
          "fields": [
            "districtName"
          ]
        }
      }
    }, function (err, results) {
      if(err){
        sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getDistanceBasedOnArea' });
      }

      let result = JSON.parse(JSON.stringify(results));
      if (result.length > 1) {
        fromAreaUpdated = result[0].areaName + "," + result[0].district.districtName;
        toAreaUpdated = result[1].areaName + "," + result[1].district.districtName;
      }
      else {
        fromAreaUpdated = result[0].areaName + "," + result[0].district.districtName;
        toAreaUpdated = result[0].areaName + "," + result[0].district.districtName;
        // 
      }
      areas = [fromAreaUpdated, toAreaUpdated]
      //--------------------------------
      var geocoder = NodeGeocoder(options);
      setTimeout(()=>{       

      geocoder.batchGeocode(areas, function (err, res) {
        if(err){
          sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getDistanceBasedOnArea' });
        }
        var fromDistance = {
          latitude: res[0].value[0].latitude,
          longitude: res[0].value[0].longitude

        }
        var toDistance = {
          latitude: res[1].value[0].latitude,
          longitude: res[1].value[0].longitude
        }
        let updateLocation = [
          {
            areaId: params.fromArea.id,
            latitude: res[0].value[0].latitude,
            longitude: res[0].value[0].longitude

          },
          {
            areaId: params.toArea.id,
            latitude: res[1].value[0].latitude,
            longitude: res[1].value[0].longitude
          }

        ]
        Stp.app.models.UserAreaMapping.updateLocation(updateLocation,
          function (err, UpdateResult) {
            if(err){
              sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getDistanceBasedOnArea' });
            }
          });

        let value = geolib.getPreciseDistance(fromDistance, toDistance, { accuracy: 0.01 });
        var km = parseInt(value) / _1KM;
        var rounundedKMValue = Math.round(km);

        let result = [{
          distanceInKM: rounundedKMValue,
          distanceInMeter: value
        }];
        return cb(false, result);
      });
    },100)
      //--------------------------------

    })
  }
  Stp.remoteMethod(
    'getDistanceBasedOnArea', {
      description: 'get Distance between two areas in KM',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }

    }
  );

  //--------------------
  //--------------nipun (17-01-2020) ------end //--------------nipun (17-01-2020) ------deleting multiple items using ids array
  Stp.removestpApproval = function(params, cb){
    var self = this;
    var StpCollection = self.getDataSource().connector.collection(Stp.modelName);
    
    let where={
     _id:{inq:params}
    }

    let updateObj={
      status:false,
      delStatus:"approved",
      updatedAt:new Date()

    }
    Stp.update(where,updateObj,
    function(err, result){
      if(err){
        sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'removestpApproval' });
      }
      return cb(false, JSON.stringify(result));
    });
  }
  Stp.remoteMethod('removestpApproval',{
    accepts: {arg: 'params', type: 'array'},
    http: {verb: 'get', path: '/removestpApproval'},
    returns: { arg:'result', type: 'object'}
  });
  //--------------nipun (17-01-2020) ------end
    
  //---------------------get all STP details mgr wise-22-01-2020--by preeti arora---------------------

  
  //---------------------get all STP details mgr wise-22-01-2020-by preeti----------------------

  Stp.getStpDetails = function (params, cb) {
    var self = this;
    var StpCollection = self.getDataSource().connector.collection(Stp.modelName);
    let userIds=[]
    for(var i=0;i<params.userIds.length;i++){
        userIds.push(ObjectId(params.userIds[i]))
    }
    let match={
        companyId:ObjectId(params.companyId),
        status:true,
        appStatus:"approved",
        userId:{$in:userIds}
    }

	if(params.isDivisionExist== true){
		
	}
    StpCollection.aggregate(

          {
            $match:  match
          },
      
          // Stage 2
          {
            $lookup: {
                "from" : "UserInfo",
                "localField" : "userId",
                "foreignField" : "userId",
                "as" : "userData"
            }
          },
      
          // Stage 3
          {
            $lookup: {
                "from" : "Area",
                "localField" : "fromArea",
                "foreignField" : "_id",
                "as" : "fromAreas"
            }
          },
      
          // Stage 4
          {
            $lookup: {
                "from" : "Area",
                "localField" : "toArea",
                "foreignField" : "_id",
                "as" : "toAreas"
            }
          },
      
          // Stage 5
          {
            $unwind: "$userData"
          },
      
          // Stage 6
          {
            $lookup: {
                "from" : "District",
                "localField" : "districtId",
                "foreignField" : "_id",
                "as" : "disData"
            }
          },
          // Stage 9
          {
            $project: {
            distance:1,
            type:1,
            status:1,
            appStatus:1,
			fromArea:1,
			toArea:1,
            fromAreaName:{ $arrayElemAt: ["$fromAreas.areaName", 0]},
            toAreaName:{ $arrayElemAt: ["$toAreas.areaName", 0]},
            freqVisit:1,
            userName:"$userData.name",
            district:{$arrayElemAt:["$disData.districtName",0]},
            }
          },function(err,res){
            if(err){
              sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpDetails' });
            }
              if(res.length>0){
                  return cb(false,res)
              }
              else{
                return cb(false,[])
              }
          }
    )
      
  }
  Stp.remoteMethod(
    'getStpDetails', {
      description: 'get Distance between two areas in KM',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }

    }
  );
  //------------------------end--------------------------------
  //-------------------------preeti arora (19-02-2020) start --- New Update upload by Rahul Choudhary-as required-on 20-08-2020--------
  
  Stp.editSTPApproval = function (params, cb) {
    var self = this;
    let STPBackup=[];  

      let where={
        _id: params.id,
        companyId: params.companyId
      }
      Stp.find({
        where: where
      }  , function (err, res) {
        if(err){
          sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'editSTPApproval' });
        }
          if (res.length > 0) {
            if (res[0].hasOwnProperty('STPBackup')) {
              for (var i = 0; i < res[0].STPBackup.length; i++) {
                STPBackup.push(res[0].STPBackup[i])
              }
              STPBackup.push({
                "distance" : res[0].distance,
                "type" : res[0].type, 
                "userId" : res[0].userId, 
                "status" : res[0].status, 
                "frcId" : res[0].frcId, 
                 updatedAt: new Date()
              })

            }
            else {
              STPBackup.push({
                "distance" : res[0].distance,
                "type" : res[0].type, 
                "userId" : res[0].userId, 
                "status" : res[0].status, 
                "frcId" : res[0].frcId, 
                 updatedAt: new Date()
             })
            }

          } else {
            return cb(false, [])
          }
          


          let where = {
             _id: params.id,
             companyId: params.companyId,
           }
           let updateObj = {
             "distance":params.distance,
             "type":params.type,
             frcId:params.frcId,
             updatedAt: new Date(),
             STPBackup: STPBackup
           }
           Stp.update(where, updateObj, function (err, res) {
            return cb(false,res)
})

    })
}

Stp.remoteMethod(
  'editSTPApproval', {
  description: 'get Distance between two areas in KM',
  accepts: [{
    arg: 'params',
    type: 'object'
  }],
  returns: {
    root: true,
    type: 'array'
  },
  http: {
    verb: 'post'
  }

}
);
  //------------------------end--------------------------------
  
  //-------------------------Rahul CHoudhary (migrate STP) 24-04-2020 -------------
  Stp.migrateSTPDetails = function (params, cb) {
    var self = this;
    var MigrateCollection = this.getDataSource().connector.collection(Stp.modelName);
    
    Stp.find({
      where: {
          companyId: params.companyId,
          userId:params.employeeId,
          id:params.id
      }

  },function(err, response) {
   
    if(err){
      sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'migrateSTPDetails' });
    }
    let res1 = JSON.parse(JSON.stringify(response));
    let app_By_Mgr;
    let app_By_Adm;
    if (response.length > 0) {

    Stp.find({
      where: {
          companyId: params.companyId,
          userId:params.employeeIdTo,
          fromArea:ObjectId(res1[0].fromArea),
          toArea:ObjectId(res1[0].toArea),
          distance:params.distance,
          type:params.type
      }

  },function(err, result) {
if(result.length>0){

  console.log("Allerady Exist!");
  return cb(false,[]);
}else{
  if(res1[0].appByMgr==''||res1[0].appByMgr==undefined){
    app_By_Mgr='';
  }else{
    app_By_Mgr=ObjectId(res1[0].appByMgr);
  }
  if(res1[0].appByAdm==''||res1[0].appByAdm==undefined){
    app_By_Adm='';
  }else{
    app_By_Adm=ObjectId(res1[0].appByAdm);
  }
  
        let migrateDetails = {
          distance : params.distance, 
          type : res1[0].type, 
          createdAt : new Date(), 
          userId : ObjectId(params.employeeIdTo), 
          updatedAt : new Date(), 
          mgrAppDate : new Date(), 
          mgrDelDate : new Date(), 
          finalDelDate : new Date(), 
          finalAppDate : new Date(), 
          status : res1[0].status, 
          appStatus : res1[0].appStatus, 
          freqVisit : res1[0].freqVisit, 
          companyId : ObjectId(res1[0].companyId), 
          stateId : ObjectId(res1[0].stateId), 
          districtId  : ObjectId(res1[0].districtId), 
          frcId : ObjectId(res1[0].frcId), 
          fromArea : ObjectId(res1[0].fromArea), 
          toArea : ObjectId(res1[0].toArea), 
          appByMgr  :app_By_Mgr, 
          appByAdm : app_By_Adm, 
          AreaType : res1[0].AreaType,
      }
      MigrateCollection.insertOne(migrateDetails, function(err, res) {
        
          if (err) {
            sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'migrateSTPDetails' });
             console.log(err);
              return cb(err);
          }
          return cb(false, res)
      })
}
  })
    }else{
      return cb(false,[])
    }
  })
  }

  Stp.remoteMethod(
    'migrateSTPDetails', {
    description: 'get Migrate STP from one employee to another',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  
  }
  );
  
  
  //------------Updated By Rahul Choudhary for required DIvision filter
  Stp.getStpApprovalRequestBasedOnDivision = function(params, cb) {
	  console.log("params :",params);
    var self = this;
    var StpCollection = self.getDataSource().connector.collection(Stp.modelName);
    if (params.obj.type === 'state') {

      let div = [];
      for (var i = 0; i < params.obj.division.length; i++) {
          div.push(ObjectId(params.obj.division[i]));
      }
    let  filter = {
        companyId: ObjectId(params.obj.companyId),
        divisionId: {inq : div},
        status : true
    }
      Stp.app.models.UserInfo.find({
        where: filter,
    },
        function (err, response) {
            if (err) {
              sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequestBasedOnDivision' });
                //  console.log(err);
            }

            let userIds = [];
            for (var i = 0; i < response.length; i++) {
                userIds.push(ObjectId(response[i].userId))
            }
          StpCollection.aggregate({
            $match: {
              companyId: ObjectId(params.obj.companyId),
              appByAdm: '',
             status: false,
              delStatus: {$ne : "approved"},
              userId:  {$in : userIds }
            }
        },
        // Stage 2
    {
      $group: {
      _id: {
                              "stateId": "$stateId",
                              "districtId": "$districtId",
                              "userId" : "$userId"
                          },
                          request: {
                              $addToSet: "$userId"
                          }
      }
    },

    // Stage 3
    {
      $lookup: {
          "from" : "UserInfo",
          "localField" : "_id.userId",
          "foreignField" : "userId",
          "as" : "userIds"
      }
    },

    // Stage 4
    {
      $unwind: "$userIds"
    }, {
      $unwind: "$userIds.divisionId"
    },

    // Stage 5
    {
      $group: {
      _id: {
                              "divisionId":"$userIds.divisionId",
                              "stateId1": "$userIds.stateId",
                              "districtId1": "$userIds.districtId"
                          },
                          request: {
                              $addToSet: "$userIds.userId"
                          }
      }
    },

    // Stage 6
    {
      $lookup: {
         "from": "State",
              "localField": "_id.stateId1",
              "foreignField": "_id",
              "as": "states"
      }
    },

    // Stage 7
    {
      $lookup: {
          "from": "District",
              "localField": "_id.districtId1",
              "foreignField": "_id",
              "as": "district"
      }
    },

    // Stage 8
    {
      $lookup: {
          "from": "DivisionMaster",
          "localField": "_id.divisionId",
          "foreignField": "_id",
          "as": "division"
      }
    },

    // Stage 9
    {
      $project: {
      
              _id: 0,
              divisionId: "$_id.divisionId",
              stateId: "$_id.stateId1",
              districtId: "$_id.districtId1",
              divisionName: {
                  $arrayElemAt: ["$division.divisionName", 0]
              },
              stateName: {
                  $arrayElemAt: ["$states.stateName", 0]
              },
              districtName: {
                  $arrayElemAt: ["$district.districtName", 0]
              },
              request: {
                  $size: "$request"
              }
      }
    }, {
            $sort: {
                stateName: 1,
                districtName: 1
            }
        }, {
            cursor: {
                batchSize: 50
            },
            allowDiskUse: true
        }, function(err, result) {
			console.log("result :",result);
          if(err){
            sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequestBasedOnDivision' });
          }
            return cb(false, result)
        })
            })
    } else if (params.obj.type === 'user') {
        StpCollection.aggregate({
                $match: {
                    companyId: ObjectId(params.obj.companyId),
                    appByAdm: '',
                    stateId: ObjectId(params.obj.stateId),
                   status: false,
                    delStatus: {$ne : "approved"}
                }
            }, {
                $group: {
                    _id: {
                        "stateId": "$stateId",
                        "districtId": "$districtId",
                        "userId": "$userId"
                    },
                    request: {
                        $sum: 1
                    }
                }
            }, {
                $lookup: {
                    "from": "State",
                    "localField": "_id.stateId",
                    "foreignField": "_id",
                    "as": "states"
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "_id.districtId",
                    "foreignField": "_id",
                    "as": "district"
                }
            }, {
                $project: {
                    _id: 0,
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    userId: "$_id.userId",

                    stateName: {
                        $arrayElemAt: ["$states.stateName", 0]
                    },
                    districtName: {
                        $arrayElemAt: ["$district.districtName", 0]
                    },
                    request: 1
                }
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "userId",
                    "foreignField": "userId",
                    "as": "users"
                }
            }, {
                $unwind: {
                    path: "$users",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $unwind: {
                    path: "$users.divisionId",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "DivisionMaster",
                    "localField": "users.divisionId",
                    "foreignField": "_id",
                    "as": "division"
                }
            }, {
                $project: {
                    name: "$users.name",
                    designation: "$users.designation",
                    _id: 0,
                    stateId: 1,
                    districtId: 1,
                    userId: 1,
                    stateName: 1,
                    districtName: 1,
                    request: 1,
                    divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                    divisionId: { $arrayElemAt: ["$division._id", 0] },
                    daCategory: "$users.daCategory"
                }
            }, {
                $sort: {
                    stateName: 1,
                    districtName: 1,
                    name: 1
                }
            }, {
                cursor: {
                    batchSize: 50
                },
                allowDiskUse: true
            },
            function(err, result) {
              if(err){
                sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequestBasedOnDivision' });
              }
                return cb(false, result)
            })
    } else if (params.obj.type === "hierarchy") {
        Stp.app.models.Hierarchy.getManagerHierarchyInArray({
            companyId: params.obj.companyId,
            supervisorId: [params.obj.userId]
        }, function(err, result) {
          if(err){
            sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequestBasedOnDivision' });
          }
            let userIds = [];
            if (result.length > 0) {
                for (const userId of result[0].userId) {
                    userIds.push(userId)
                }

                StpCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.obj.companyId),
                        appByMgr: '',
                        appByAdm: '',
                        userId: {
                            $in: userIds
                        },
                        status: false,
                delStatus: {$ne : "approved"}
                    }
                }, {
                    $group: {
                        _id: {
                            "stateId": "$stateId",
                            "districtId": "$districtId",
                            "userId": "$userId"
                        },
                        request: {
                            $sum: 1
                        }
                    }
                }, {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "states"
                    }
                }, {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "district"
                    }
                }, {
                    $project: {
                        _id: 0,
                        stateId: "$_id.stateId",
                        districtId: "$_id.districtId",
                        userId: "$_id.userId",
                        stateName: {
                            $arrayElemAt: ["$states.stateName", 0]
                        },
                        districtName: {
                            $arrayElemAt: ["$district.districtName", 0]
                        },
                        request: 1
                    }
                }, {
                    $lookup: {
                        "from": "UserInfo",
                        "localField": "userId",
                        "foreignField": "userId",
                        "as": "users"
                    }
                }, {
                    $unwind: {
                        path: "$users",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $unwind: {
                        path: "$users.divisionId",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $lookup: {
                        "from": "DivisionMaster",
                        "localField": "users.divisionId",
                        "foreignField": "_id",
                        "as": "division"
                    }
                }, {
                    $project: {
                        name: "$users.name",
                        designation: "$users.designation",
                        _id: 0,
                        stateId: 1,
                        districtId: 1,
                        userId: 1,
                        stateName: 1,
                        districtName: 1,
                        request: 1,
                        divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                        divisionId: { $arrayElemAt: ["$division._id", 0] },
                        daCategory: "$users.daCategory"
                    }
                }, {
                    $sort: {
                        stateName: 1,
                        districtName: 1,
                        name: 1
                    }
                }, {
                    cursor: {
                        batchSize: 50
                    },
                    allowDiskUse: true
                }, function(err, result) {
console.log("result : " ,result);
                  
                  if(err){
                    sendMail.sendMail({ collectionName: 'Stp', errorObject: err, paramsObject: params, methodName: 'getStpApprovalRequestBasedOnDivision' });
                  }
                    return cb(false, result)
                })
            }


        })
    }


}

Stp.remoteMethod(
'getStpApprovalRequestBasedOnDivision', {
  description: 'get Users based on stateId Divisionwise',
  accepts: [{
    arg: 'params',
    type: 'object'
  }],
  returns: {
    root: true,
    type: 'array'
  },
  http: {
    verb: 'get'
  }
}
);
//----------Preeti arora----------
Stp.taggedLocation = function (params, cb) {    
  var options = {
    provider: 'google',
    // Optional depending on the providers
    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyCKCxPLVooXnPEFe17z4a65p-7DpJmsA2o', // for Mapquest, OpenCage, Google Premier
    formatter: null // 'gpx', 'string', ...
  };

  var geocoder = NodeGeocoder(options);
  
  geocoder.geocode({ address: params.address }, function(err, res){
  if(err){
    console.log(err);
    return cb(false,err)
    
  }else{
    if(res.length>0){
      return cb(false,res)
    }else{
      return cb(false,[])

    }
  }
    


  })
  

}
Stp.remoteMethod(
  'taggedLocation', {
    description: 'tagged Location',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }

  }
);
  
};