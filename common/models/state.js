﻿'use strict';
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail');
var moment = require('moment');
var async = require('async');
var status = require('../../utils/statusMessage');
module.exports = function (State) {

    State.getState = function (parameters, cb) {
        var self = this;
        let dynamicMatch = {};
        if (parameters.isDivisionExist == true) {
            let divisionIds = []
            for (let i = 0; i < parameters.division.length; i++) {
                divisionIds.push(ObjectId(parameters.division[i]))
            }

            dynamicMatch = {
                assignedTo: ObjectId(parameters.companyId),
                assignedDivision: {
                    $in: divisionIds
                }

            }
        } else if (parameters.isDivisionExist == false || parameters.isDivisionExist == undefined) {
            dynamicMatch = {
                assignedTo: ObjectId(parameters.companyId)
            }
        }
        var StateCollection = self.getDataSource().connector.collection(State.modelName);
        StateCollection.aggregate(

            // Stage 1
            {
                $match: dynamicMatch
            },

            // Stage 2
            {
                $sort: {
                    stateName: 1
                }
            },

            // Stage 3
            {
                $group: {
                    _id: {},
                    stateIds: { $push: "$_id" },
                    stateInfo: {
                        $push: {
                            "id": "$_id",
                            "stateName": "$stateName"
                        }
                    }
                }
            }, function (err, result) {
                if (err) {
                    sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: parameters, methodName: 'getState' });
                    // console.log("No Record Found");
                    return cb(err);
                }
                return cb(false, result)

            }
        );

    }
    State.remoteMethod(
        'getState', {
        description: 'get State',
        accepts: [{
            arg: 'parameters',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //-------ERP Services call by Preeti Arora 11-02-2020-------

    State.getPrimaryCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalPrimaryCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;

        let json = {
            userID: param.userId,
            FromDate: fromDate,
            ToDate: toDate
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getPrimaryCount' });
                // console.log(err);
            }


        });

    }
    State.remoteMethod(
        'getPrimaryCount', {
        description: 'get getPrimaryCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getCollectionCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalCollectionCount";
        let today = moment();
        var month = today.format('M');
        var year = today.format('YYYY');
        let json = {
            userID: param.userId,
            "month": month,
            "year": year,
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getCollectionCount' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getCollectionCount', {
        description: 'get getCollectionCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getOutstandingCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalOutstandingCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;

        let json = {
            userID: param.userId,
            FromDate: fromDate,
            ToDate: toDate
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getOutstandingCount' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getOutstandingCount', {
        description: 'get getOutstandingCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );


    // State.getStateWiseDetails = function(param, cb) {
    //     var self = this;
    //     const request = require('request');
    //     let url="";
    //     let json={}
    //     let today=moment();
    //     let date=today.format('DD-MM-YYYY');
    //     var month = today.format('M');
    //     var year  = today.format('YYYY');
    //     let fromDate =  "01-"+ month + "-" + year;
    //     let toDate = date;
    //     if(param.type=="Primary"){
    //       url=param.APIEndPoint+"StateWisePrimaryData";
    //        json={
    //         userID: param.userId, 
    //         FromDate:fromDate,
    //         ToDate:toDate
    //        }
    //     }else if(param.type=="Outstanding"){
    //       url=param.APIEndPoint+"StateWiseOutstandingData";
    //       json={
    //         userID: param.userId, 
    //        }
    //     }else if(param.type=="Collection"){
    //       url=param.APIEndPoint+"StateWiseCollectionData";
    //       json={
    //         userID: param.userId, 
    //         month:month,
    //         year:year
    //        }
    //     }   
    //     request({
    //         url: url, // Online url
    //         method: "POST",
    //         headers: {
    //             // header info - in case of authentication enabled   
    //             "Content-Type": "application/json",
    //         }, //json        
    //         // body goes here 
    //         json: json
    //     }, function(err, res, body) { 
    //         if (!err) {
    //             return cb(false,body.item)
    //         } else {
    //             sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
    //             // console.log(err);
    //         }


    //     });

    // }
    // State.remoteMethod(
    //     'getStateWiseDetails', {
    //         description: 'get getStateWiseDetails',
    //         accepts: [{
    //             arg: 'param',
    //             type: 'object'
    //         }],
    //         returns: {
    //             root: true,
    //             type: 'array'
    //         },
    //         http: {
    //             verb: 'get'
    //         }
    //     }
    // );

    //---------------------End----------------------------


    //Done  by ravi on 15-10-2020
    State.getStateWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let stateCode = []
        State.app.models.State.getERPStateData(param, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        stateCode.push(element.code)
                    });
                } else {
                    stateCode = "";
                }
                let json = { userID: param.userId, StateIds: stateCode.toString() }
                if (param.type == "Primary") {
                    url = param.APIEndPoint + "StateWisePrimaryData";
                    json['FromDate'] = param.fromDate;
                    json['ToDate'] = param.toDate;
                } else if (param.type == "Outstanding") {
                    url = param.APIEndPoint + "StateWiseOutstandingData";
                } else if (param.type == "Collection") {
                    url = param.APIEndPoint + "StateWiseCollectionData";
                    json['Month'] = param.month;
                    json['Year'] = param.year;
                } else if (param.type == "SalesReturn") {
                    url = param.APIEndPoint + "StateWiseSalesReturnData";
                    json['FromDate'] = param.fromDate;
                    json['ToDate'] = param.toDate;
                    //json['FromDate'] = '01-08-2020';
                    // json['ToDate'] = '30-08-2020';
                }
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });

            } else {
                return cb(false, [])
            }


        })




    }
    State.remoteMethod(
        'getStateWiseDetails', {
        description: 'getStateWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getStateWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let stateCode = [];
        State.app.models.State.getERPStateData(param, function (err, res) {
            if (!err) {
                if (res.length > 0) {
                    res.forEach(element => {
                        stateCode.push(element.code)
                    });
                } else {
                    stateCode = "";
                }
                let json = { userID: param.userId, StateIds: stateCode.toString() }
                if (param.type == "Primary") {
                    url = param.APIEndPoint + "StateProductWisePrimaryData";
                    json['FromDate'] = param.fromDate;
                    json['ToDate'] = param.toDate;
                } else if (param.type == "Outstanding") {
                    url = param.APIEndPoint + "StateWiseOutstandingData";
                } else if (param.type == "Collection") {
                    url = param.APIEndPoint + "StateWiseCollectionData";
                    json['Month'] = param.month;
                    json['Year'] = param.year;
                } else if (param.type == "SalesReturn") {
                    url = param.APIEndPoint + "StateProductWiseSalesReturnData";
                    json['FromDate'] = param.fromDate;
                    json['ToDate'] = param.toDate;
                    //json['FromDate'] = '01-08-2020';
                    // json['ToDate'] = '30-08-2020';
                }
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        return cb(false, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });

            } else {
                return cb(false, [])
            }


        })
    }
    State.remoteMethod(
        'getStateWiseProductDetails', {
        description: 'getStateWiseProductDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    State.getHQWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = { userID: param.userId, stateIds: param.stateCode }
        if (param.type == "Primary") {
            url = param.APIEndPoint + "HeadQuarterWisePrimaryData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
        } else if (param.type == "Outstanding") {
            url = param.APIEndPoint + "HeadQuarterWiseOutstandingData";
        } else if (param.type == "Collection") {
            url = param.APIEndPoint + "HeadquarterWiseCollectionData";
            json['Month'] = param.month;
            json['Year'] = param.year;
        } else if (param.type == "SalesReturn") {
            url = param.APIEndPoint + "HeadquarterWiseSalesReturnData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
            //json['FromDate'] = '01-08-2020';
            //json['ToDate'] = '30-08-2020';
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getHQWiseDetails', {
        description: 'getHQWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getHQWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = { userID: param.userId, stateIds: param.stateCode }
        if (param.type == "Primary") {
            url = param.APIEndPoint + "HQProductWisePrimaryData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
        } else if (param.type == "Outstanding") {
            url = param.APIEndPoint + "HeadQuarterWiseOutstandingData";
        } else if (param.type == "Collection") {
            url = param.APIEndPoint + "HeadquarterWiseCollectionData";
            json['Month'] = param.month;
            json['Year'] = param.year;
        } else if (param.type == "SalesReturn") {
            url = param.APIEndPoint + "HQProductWiseSalesReturnData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
            //json['FromDate'] = '01-08-2020';
            //json['ToDate'] = '30-08-2020';
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getHQWiseProductDetails', {
        description: 'getHQWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getPartyWiseDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = { userID: param.userId, HQIds: param.headquaterCode }
        if (param.type == "Primary") {
            url = param.APIEndPoint + "PartyWisePrimaryData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
        } else if (param.type == "Outstanding") {
            url = param.APIEndPoint + "PartyWiseOutstandingData";
        } else if (param.type == "Collection") {
            url = param.APIEndPoint + "PartyWiseCollectionData";
            json['Month'] = param.month;
            json['Year'] = param.year;
        } else if (param.type == "SalesReturn") {
            url = param.APIEndPoint + "PartyWiseSalesReturnData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
            //json['FromDate'] = '01-08-2020';
            // json['ToDate'] = '30-08-2020';
        }
        console.log("json--url--=>", json, url)
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            console.log("---", err, "----", res);

            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getPartyWiseDetails', {
        description: 'getPartyWiseDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    State.getPartyWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = { userID: param.userId, HQIds: param.headquaterCode }
        if (param.type == "Primary") {
            url = param.APIEndPoint + "PartyProductWisePrimaryData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
        } else if (param.type == "Outstanding") {
            url = param.APIEndPoint + "PartyWiseOutstandingData";
        } else if (param.type == "Collection") {
            url = param.APIEndPoint + "PartyWiseCollectionData";
            json['Month'] = param.month;
            json['Year'] = param.year;
        } else if (param.type == "SalesReturn") {
            url = param.APIEndPoint + "PartyProductWiseSalesReturnData";
            json['FromDate'] = param.fromDate;
            json['ToDate'] = param.toDate;
            //json['FromDate'] = '01-08-2020';
            // json['ToDate'] = '30-08-2020';
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getPartyWiseDetails' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getPartyWiseProductDetails', {
        description: 'getPartyWiseProductDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getERPStateData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "StateDropDown";
        const request = require('request');
        let json = { userID: param.userId };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getERPStateData' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getERPStateData', {
        description: 'getERPStateData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getERPHeadquarterData = function (param, cb) {
        var self = this;
        let url = param.APIEndPoint + "StateHQDropDown";
        const request = require('request');
        let json = { userid: param.userId, StateIds: param.stateId };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getERPHeadquarterData' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getERPHeadquarterData', {
        description: 'getERPHeadquarterData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getDesignationData = function (param, cb) {
        var self = this;
        //let url = "http://103.11.85.61/PharmaMRApiForSpey/api/DesignationDropDown";
        let url = param.APIEndPoint + "DesignationDropDown";
        //param.APIEndPoint + "ManagerDropDown";
        const request = require('request');
        request({
            url: url, // Online url
            method: "GET",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            // json: json
        }, function (err, res, body) {
            let result = JSON.parse(body);
            if (!err) {
                return cb(false, result.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getDesignationData' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getDesignationData', {
        description: 'getDesignationData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getManagerData = function (param, cb) {
        var self = this;
        //let url = "http://103.11.85.61/PharmaMRApiForSpey/api/ManagerDropDown"//param.APIEndPoint + "ManagerDropDown";
        let url = param.APIEndPoint + "ManagerDropDown";
        const request = require('request');
        let json = { Designationid: param.Designationid };
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getManagerData' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getManagerData', {
        description: 'getManagerData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getSalesReturnCount = function (param, cb) {
        var self = this;
        const request = require('request');
        let url = param.APIEndPoint + "TotalSalesReturnCount";
        let today = moment();
        let date = today.format('DD-MM-YYYY');
        var month = today.format('M');
        var year = today.format('YYYY');
        let fromDate = "01-" + month + "-" + year;
        let toDate = date;

        let json = {
            userID: param.userId,
            FromDate: fromDate,
            ToDate: toDate
        }
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getSalesReturnCount' });
                // console.log(err);
            }


        });

    }
    State.remoteMethod(
        'getSalesReturnCount', {
        description: 'get getSalesReturnCount',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    //END
    //------state wise api for mobile app only.. 11-01-2020-- by preeti arora--
    State.getStateWiseConsolidatedDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });

            },
            collection: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                    });
                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                    });

                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);

                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = 0, outstanding = 0, collection = 0, salesReturn = 0;

                        const stateCode = state.split("@")[0];
                        const stateName = state.split("@")[1];
                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            let stateFoundForPrimary = res.primary.filter(obj => obj.StateCode == stateCode);
                            if (stateFoundForPrimary.length > 0) {
                                primary = stateFoundForPrimary[0].amount;
                            }
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => obj.StateCode == stateCode);
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].amount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            let stateFoundSalesReturn = res.salesreturn.filter(obj => obj.StateCode == stateCode);
                            if (stateFoundSalesReturn.length > 0) {
                                salesReturn = stateFoundSalesReturn[0].Amount;
                            }
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => obj.StateCode == stateCode);
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].AMOUNT;
                            }
                        }
                        finalResult.push({
                            stateCode,
                            stateName,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    State.remoteMethod(
        'getStateWiseConsolidatedDetails', {
        description: 'Get State Wise Consolidated Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    State.getHQWiseConsolidatedDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadQuarterWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadQuarterWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });

            },
            collection: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadquarterWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadquarterWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HeadquaterCode}@${primData.Headquater}`)
                    });

                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.headquarterCode}@${primData.headquarter}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQ}`)
                    });

                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQ}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = 0, outstanding = 0, collection = 0, salesReturn = 0;

                        const stateCode = state.split("@")[0];
                        const stateName = state.split("@")[1];
                        const hqCode = state.split("@")[2];
                        const hqName = state.split("@")[3];
                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            let stateFoundForPrimary = res.primary.filter(obj => (obj.StateCode == stateCode && obj.HeadquaterCode == hqCode));
                            if (stateFoundForPrimary.length > 0) {
                                primary = stateFoundForPrimary[0].primSales;
                            }
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.StateCode == stateCode && obj.headquarterCode == hqCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].amount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            let stateFoundSalesReturn = res.salesreturn.filter(obj => (obj.HQCode == hqCode));
                            if (stateFoundSalesReturn.length > 0) {
                                salesReturn = stateFoundSalesReturn[0].Amount;
                            }
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].AMOUNT;
                            }
                        }
                        finalResult.push({
                            stateCode,
                            stateName,
                            hqCode,
                            hqName,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    State.remoteMethod(
        'getHQWiseConsolidatedDetails', {
        description: 'Get HQ Wise Consolidated Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    State.getPartyWiseConsolidatedDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }
                });
            },
            collection: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {

                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.HeadquaterCode}@${primData.Headquater}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.headquarterCode}@${primData.headquarter}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQ}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQ}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = 0, outstanding = 0, collection = 0, salesReturn = 0;
                        const hqCode = state.split("@")[0];
                        const hqName = state.split("@")[1];
                        const partyCode = state.split("@")[2];
                        const partyName = state.split("@")[3];

                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            let stateFoundForPrimary = res.primary.filter(obj => (obj.HeadquaterCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundForPrimary.length > 0) {
                                primary = stateFoundForPrimary[0].primSales;
                            }
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.headquarterCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].amount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            let stateFoundSalesReturn = res.salesreturn.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSalesReturn.length > 0) {
                                salesReturn = stateFoundSalesReturn[0].Amount;
                            }
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].AMOUNT;
                            }
                        }
                        finalResult.push({
                            hqCode,
                            hqName,
                            partyCode,
                            partyName,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    State.remoteMethod(
        'getPartyWiseConsolidatedDetails', {
        description: 'Get Party Wise Consolidated Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    //------------END--------------------
    //-----------------product wise api--Preeti arora 22-03-2021----------------------------------------//
    State.getStateWiseConsolidatedProductDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateProductWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            salesreturn: (cb) => {
                let json = { Userid: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateProductWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });

            },
            collection: (cb) => {
                let json = { userID: param.userId, StateIds: param.stateCode }
                url = param.APIEndPoint + "StateWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
        }, (err, res) => {
            if (err) return cb(err);
            let finalResult = [];
            let stateCode = [];
            if (res.hasOwnProperty("primary") && res.primary != undefined) {
                res.primary.forEach(primData => {
                    stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                });
            } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                res.salesreturn.forEach(primData => {
                    stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                });
            }
            if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                res.outstanding.forEach(primData => {
                    stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                });

            } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                res.collection.forEach(primData => {
                    stateCode.push(`${primData.StateCode}@${primData.StateName}`)
                });

            }
            const StateSet = new Set(stateCode);
            const values = StateSet.values();
            const stateCodeArray = Array.from(values);
            if (stateCodeArray.length > 0) {
                stateCodeArray.forEach(state => {
                    var primary = [], salesReturn = [], outstanding = 0, collection = 0, totalPrimary = 0, totalSalesReturn = 0;
                    const stateCode = state.split("@")[0];
                    const stateName = state.split("@")[1];
                    if (res.hasOwnProperty("primary") && res.primary != undefined) {
                        res.primary.forEach(obj => {
                            if (obj.StateCode == stateCode) {
                                totalPrimary += (obj.qty * obj.creditvalue);
                                primary.push(
                                    {
                                        amount: (obj.qty * obj.creditvalue).toFixed(2),
                                        productCode: obj.ProductCode,
                                        productName: obj.ProductName,
                                        qty: obj.qty,
                                    })
                            }
                        });
                    }
                    if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                        res.salesreturn.forEach(obj => {
                            if (obj.StateCode == stateCode) {
                                totalSalesReturn += (obj.qty * obj.creditvalue);
                                salesReturn.push(
                                    {
                                        amount: (obj.qty * obj.creditvalue).toFixed(2),
                                        productCode: obj.ProductCode,
                                        productName: obj.ProductName,
                                        qty: obj.qty,
                                    })
                            }
                        });
                    }
                    if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                        let stateFoundSecondary = res.outstanding.filter(obj => obj.StateCode == stateCode);
                        if (stateFoundSecondary.length > 0) {
                            outstanding = stateFoundSecondary[0].amount;
                        }
                    } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                        let stateFoundCollection = res.collection.filter(obj => obj.StateCode == stateCode);
                        if (stateFoundCollection.length > 0) {
                            collection = stateFoundCollection[0].AMOUNT;
                        }
                    }
                    totalPrimary = totalPrimary.toFixed(2)
                    totalSalesReturn = totalSalesReturn.toFixed(2)
                    finalResult.push({
                        stateCode,
                        stateName,
                        totalPrimary,
                        primary,
                        collection,
                        outstanding,
                        totalSalesReturn,
                        salesReturn
                    })
                });
                return cb(false, finalResult)
            } else {
                return cb(false, [])
            }
        })




    }
    State.remoteMethod(
        'getStateWiseConsolidatedProductDetails', {
        description: 'Get State Wise Consolidated Product Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    State.getHQWiseConsolidatedProductDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HQProductWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadQuarterWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });

            },
            collection: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HeadquarterWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                let json = { userID: param.userId, stateIds: param.stateCode }
                url = param.APIEndPoint + "HQProductWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HeadquaterCode}@${primData.Headquater}`)
                    });

                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.headquarterCode}@${primData.headquarter}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQ}`)
                    });

                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.StateCode}@${primData.StateName}@${primData.HQCode}@${primData.HQ}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = [], outstanding = 0, collection = 0, salesReturn = [], totalPrimary = 0, totalSalesReturn = 0;

                        const stateCode = state.split("@")[0];
                        const stateName = state.split("@")[1];
                        const hqCode = state.split("@")[2];
                        const hqName = state.split("@")[3];
                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            res.primary.forEach(obj => {
                                if (obj.StateCode == stateCode && obj.HeadquaterCode == hqCode) {
                                    totalPrimary += (obj.qty * obj.creditvalue);
                                    primary.push(
                                        {
                                            amount: (obj.qty * obj.creditvalue).toFixed(2),
                                            productCode: obj.ProductCode,
                                            productName: obj.ProductName,
                                            qty: obj.qty,
                                        })
                                }
                            })

                        }
                        if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.StateCode == stateCode && obj.headquarterCode == hqCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].amount;
                            }
                        }
                        if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {

                            res.salesreturn.forEach(obj => {
                                if (obj.HQCode == hqCode) {
                                    totalSalesReturn += (obj.qty * obj.creditvalue);
                                    salesReturn.push(
                                        {
                                            amount: (obj.qty * obj.creditvalue).toFixed(2),
                                            productCode: obj.ProductCode,
                                            productName: obj.ProductName,
                                            qty: obj.qty,
                                        })
                                }
                            })
                        }
                        if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.StateCode == stateCode && obj.HQCode == hqCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].AMOUNT;
                            }
                        }
                        totalPrimary = totalPrimary.toFixed(2)
                        totalSalesReturn = totalSalesReturn.toFixed(2)
                        finalResult.push({
                            stateCode,
                            stateName,
                            hqCode,
                            hqName,
                            primary,
                            totalPrimary,
                            outstanding,
                            collection,
                            totalSalesReturn,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    State.remoteMethod(
        'getHQWiseConsolidatedProductDetails', {
        description: 'Get HQ Wise Consolidated Product Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    State.getPartyWiseConsolidatedProductDetails = function (param, cb) {
        let url;
        const request = require('request');
        async.parallel({
            primary: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyProductWisePrimaryData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;
                request({
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }
                });
            },
            outstanding: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWiseOutstandingData";
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }
                });
            },
            collection: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyWiseCollectionData";
                json['Month'] = param.month;
                json['Year'] = param.year;
                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    json: json
                }, function (err, res, body) {
                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                    }

                });
            },
            salesreturn: (cb) => {
                let json = { userID: param.userId, HQIds: param.headquaterCode }
                url = param.APIEndPoint + "PartyProductWiseSalesReturnData";
                json['FromDate'] = param.fromDate;
                json['ToDate'] = param.toDate;

                request({
                    url: url, // Online url
                    method: "POST",
                    headers: {
                        // header info - in case of authentication enabled   
                        "Content-Type": "application/json",
                    }, //json        
                    // body goes here 
                    json: json
                }, function (err, res, body) {

                    if (!err) {
                        cb(null, body.item)
                    } else {
                        sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getStateWiseDetails' });
                        // console.log(err);
                    }

                });
            }
        },
            (err, res) => {
                if (err) return cb(err);
                let finalResult = [];
                let stateCode = [];
                if (res.hasOwnProperty("primary") && res.primary != undefined) {
                    res.primary.forEach(primData => {
                        stateCode.push(`${primData.HeadquaterCode}@${primData.Headquater}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                    res.outstanding.forEach(primData => {
                        stateCode.push(`${primData.headquarterCode}@${primData.headquarter}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                } if (res.hasOwnProperty("salesreturn") && res.salesReturn != undefined) {
                    res.salesreturn.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQ}@${primData.PartyCode}@${primData.PartyName}`)
                    });
                } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                    res.collection.forEach(primData => {
                        stateCode.push(`${primData.HQCode}@${primData.HQ}@${primData.PartyCode}@${primData.PartyName}`)
                    });

                }
                const StateSet = new Set(stateCode);
                const values = StateSet.values();
                const stateCodeArray = Array.from(values);
                if (stateCodeArray.length > 0) {
                    stateCodeArray.forEach(state => {
                        let primary = [], outstanding = 0, collection = 0, salesReturn = [], totalPrimary = 0, totalSalesReturn = 0;
                        const hqCode = state.split("@")[0];
                        const hqName = state.split("@")[1];
                        const partyCode = state.split("@")[2];
                        const partyName = state.split("@")[3];

                        if (res.hasOwnProperty("primary") && res.primary != undefined) {
                            res.primary.forEach(obj => {
                                if (obj.HeadquaterCode == hqCode && obj.PartyCode == partyCode) {
                                    totalPrimary += (obj.qty * obj.creditvalue);
                                    primary.push(
                                        {
                                            amount: (obj.qty * obj.creditvalue).toFixed(2),
                                            productCode: obj.ProductCode,
                                            productName: obj.ProductName,
                                            qty: obj.qty,
                                        })
                                }
                            })
                        } if (res.hasOwnProperty("outstanding") && res.outstanding != undefined) {
                            let stateFoundSecondary = res.outstanding.filter(obj => (obj.headquarterCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundSecondary.length > 0) {
                                outstanding = stateFoundSecondary[0].amount;
                            }
                        } if (res.hasOwnProperty("salesreturn") && res.salesreturn != undefined) {
                            res.salesreturn.forEach(obj => {
                                if (obj.HeadquaterCode == hqCode && obj.PartyCode == partyCode) {
                                    totalSalesReturn += (obj.qty * obj.creditvalue);
                                    salesReturn.push(
                                        {
                                            amount: (obj.qty * obj.creditvalue).toFixed(2),
                                            productCode: obj.ProductCode,
                                            productName: obj.ProductName,
                                            qty: obj.qty,
                                        })
                                }
                            })
                        } if (res.hasOwnProperty("collection") && res.collection != undefined) {
                            let stateFoundCollection = res.collection.filter(obj => (obj.HQCode == hqCode && obj.PartyCode == partyCode));
                            if (stateFoundCollection.length > 0) {
                                collection = stateFoundCollection[0].AMOUNT;
                            }
                        }
                        totalPrimary = totalPrimary.toFixed(2)
                        totalSalesReturn = totalSalesReturn.toFixed(2)
                        finalResult.push({
                            hqCode,
                            hqName,
                            partyCode,
                            partyName,
                            totalPrimary,
                            totalSalesReturn,
                            primary,
                            outstanding,
                            collection,
                            salesReturn
                        })
                    });
                    return cb(false, finalResult)

                } else {
                    return cb(false, [])
                }

            })




    }
    State.remoteMethod(
        'getPartyWiseConsolidatedProductDetails', {
        description: 'Get Party Wise Consolidated Product Details',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    //-----------------------End------------------------------------------------------------------------//

    // ERP MASTER APIs Work Start By Ravi on 04/27/2021

    State.create_or_update = function (params, cb) {
        if (params) {
            if (params.hasOwnProperty('stateName') && params.hasOwnProperty('stateCode')) {
                State.findOne({ where: { erpCode: parseInt(params['stateCode']) } }, function (err, isStateExits) {
                    if (err) { return cb(false, status.getStatusMessage(2)) }
                    if (isStateExits) {
                        let assignedCompanys = [];
                        if (isStateExits.assignedTo.length) {
                            isStateExits.assignedTo.forEach(element => {
                                assignedCompanys.push(ObjectId(element));
                            });
                        }
                        isStateExits.stateName = params.stateName;
                        //isStateExits.stateCode = params.stateCode;
                        isStateExits.countryId = ObjectId(isStateExits.countryId);
                        isStateExits.assignedTo = assignedCompanys;
                        isStateExits.oldInfo = ["old Info"];
                        isStateExits.assignedDivision = ["division"];
                        State.replaceOrCreate(isStateExits, function (err, replace) {
                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                            if (replace) {
                                return cb(false, status.getStatusMessage(4))
                            }
                        })
                    } else {
                        const stateObj = {
                            "stateName": params.stateName,
                            "stateCode": params.stateCode.toString(),
                            "erpCode": params.stateCode,
                            "assignedTo": ["erp"],
                            "enableFor": "All",
                            "countryId": ObjectId("5d68ce582aaaf707a05e2f9d"),
                            "assignedDivision": ["erp"]
                        }
                        State.replaceOrCreate(stateObj, function (err, create) {
                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                            if (create) {
                                return cb(false, status.getStatusMessage(3))
                            }
                        })
                    }
                })
            } else {
                return cb(false, status.getStatusMessage(1));
            }
        } else {
            return cb(false, status.getStatusMessage(0))
        }
    }

    State.remoteMethod(
        'create_or_update', {
        description: 'Add and Update State Master',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post'
        }
    });

    //END
    //---------------------Preeti 8-05-2021-------------
    State.getNewHQWiseProductDetails = function (param, cb) {
        var self = this;
        let url;
        const request = require('request');
        let json = { HQIDs: param.hqCode, fromDate: param.fromDate, toDate: param.toDate, Flag: param.flag };
        url = param.APIEndPoint + "HQProductWiseData";
        request({
            url: url, // Online url
            method: "POST",
            headers: {
                // header info - in case of authentication enabled   
                "Content-Type": "application/json",
            }, //json        
            // body goes here 
            json: json
        }, function (err, res, body) {
            if (!err) {
                return cb(false, body.item)
            } else {
                sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getHQWiseDetails' });
                // console.log(err);
            }

        });

    }
    State.remoteMethod(
        'getNewHQWiseProductDetails', {
        description: 'get New HQ Wise ProductDetails',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //--------------------------------
};
