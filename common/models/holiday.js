'use strict';

module.exports = function(Holiday) {

    Holiday.createHolyday = function(params, cb) {
        let checkDivision=[];
        let divisionArray = [];
        let insertObj=[];

        var holydayCollection = this.getDataSource().connector.collection(Holiday.modelName);

        if(params.divisionId!=null){
            for (var i = 0; i < params.divisionId.length; i++) {
              let divisionObject={
                divisionId:ObjectId(params.divisionId[i])
              }
              checkDivision.push(ObjectId(params.divisionId[i]));
              divisionArray.push(divisionObject);
            }
          }

          for (var j = 0; j < params.stateInfo.length; j++) {
            var holydayDetails = {
                companyId: ObjectId(params.companyId),
                divisionId: {inq:checkDivision},
                stateId : params.stateInfo[j],
                holydayName: params.holiday,
                holydayDate : params.holidayDate,
                updatedBy: "",
                createdAt: params.createdAt,
                updatedAt: params.updatedAt
            }
            insertObj.push(holydayDetails);

           
          Holyday.find({
              where: {
                  companyId: holydayDetails.companyId,
                  divisionId: holydayDetails.divisionId,
                  stateId : holydayDetails.stateId,
                  holydayName: holydayDetails.holydayName,
                  holydayDate : holydayDetails.holydayDate
              }
  
          }, function(err, response) {
              if (response.length > 0) {
                  var err = new Error('Holyday Already Exists');
                  err.statusCode = 409;
                  err.code = 'Validation failed';
                  return cb(err);
              } else {
                
                   holydayCollection.insertMany(insertObj, function(err, res) {
                    if (err) {
                       // return cb(err);
                     }
                  })
  
              }
  
          })
      }
      
      return cb(false)
}
    
    Holiday.remoteMethod(
        'createHolyday', {
            description: 'Created Holiday',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
	
	
	//////////////////////////////Rahul Saini/////////////////////
	
  Holiday.getholidayDetails = function (params, cb) {
	  console.log("params-Holiday-",params);
    var self = this;
     var viewProductCollection = this.getDataSource().connector.collection(Holiday.modelName);
    let selectedStates = [];
  
   
    for (var i = 0; i < params.stateInfo.length; i++) {
      selectedStates.push(ObjectId(params.stateInfo[i]));
    }
   

    match={
      companyId: ObjectId(params.companyId),
      assignedStates:{$in:selectedStates},
      status: true
    }
   
    viewProductCollection.aggregate(
     {
      // Stage 1
      $match:match
     },
      // Stage 2
      {
        $unwind:{ path: "$assignedStates", preserveNullAndEmptyArrays: true }
      },

      
    
     
      
      // Stage 7
      {
        $project: {
          _id: 1,
          holidayName: 1,
         holidayDate:1,
         
         
        }
      }
    
        ,
      function (err, result) {
        cb(false,result)
      })

  }
  Holiday.remoteMethod(
    'getholidayDetails', {
      description: 'View Holiday',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'object'
      },
      http: {
        verb: 'get'
      }
    }
  )

};
