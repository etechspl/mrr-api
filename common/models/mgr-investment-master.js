'use strict';
var sendMail = require("../../utils/sendMail");
var ObjectId = require("mongodb").ObjectID;
var moment = require("moment");

module.exports = function(Mgrinvestmentmaster) {

    Mgrinvestmentmaster.getInvestmentDetails = function (params, cb) {
        var self = this;
        var invCollection = self.getDataSource().connector.collection(Mgrinvestmentmaster.modelName);
        var aMatch = {};
        if (params.type == "State" || params.type == "Headquarter") {
        Mgrinvestmentmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
            
            function (err, userRecords) {
                if(err){
                    sendMail.sendMail({collectionName: "Mgrinvestmentmaster", errorObject: err, paramsObject: params, methodName: "Mgrinvestmentmaster.getInvestmentDetails"});
                }

                if (userRecords.length > 0) {
                    invCollection.aggregate(
                        {
                            $match: {
                                submitBy: {
                                    $in: userRecords[0].userIds
                                },
                                submissionDate: {
                                    $gte: new Date(params.fromDate),
                                    $lte: new Date(params.toDate)

                                }
                            }
                        },
                         // Stage 2
                         {
                            $lookup: {
                                "from": "MGRInvestmentVisitDetails",
                                "localField": "_id",
                                "foreignField": "investmentId",
                                "as": "investmentDetail"
                            }
                        }, 
                         // Stage 3
                        {
                            $unwind: {
                                path: "$investmentDetail",
                                preserveNullAndEmptyArrays: true
                            }
                        },
                        // Stage 4
                        {
                            $group: {

                                _id: {
                                    "submitBy": "$submitBy"
                                },
                                totalInvestmentdate: {
                                    $addToSet: "$submissionDate"
                                },
                                lastsubmissionDate: {
                                    $last: "$submissionDate"
                                },
                                RMPCount: {
                                    $sum: {
                                        $cond: [{
                                            $and: [{
                                                $eq: ["$investmentDetail.providerType", "RMP"]
                                            }]
                                        }, 1, 0]
                                    }
                                },
                                DrugStoreCount: {
                                    $sum: {
                                        $cond: [{
                                            $or: [{
                                                $eq: ["$investmentDetail.providerType", "Drug"]
                                            }
                                            ]
                                        }, 1, 0]
                                    }
                                },
                                stockistCount: {
                                    $sum: {
                                        $cond: [{
                                            $and: [{
                                                $eq: ["$investmentDetail.providerType", "Stockist"]
                                            }]
                                        }, 1, 0]
                                    }
                                },
                                doctorPOB: {
                                    $sum: {
                                        $cond: [{
                                            $and: [{
                                                $eq: ["$investmentDetail.providerType", "RMP"]
                                            }]
                                        }, "$investmentDetail.productPob", 0]
                                    }
                                },
                                vendorPOB: {
                                    $sum: {
                                        $cond: [{
                                            $or: [{
                                                $eq: ["$investmentDetail.providerType", "Drug"]
                                            },
                                            {
                                                $eq: ["$investmentDetail.providerType", "Stockist"]
                                            }
                                            ]
                                        }, "$investmentDetail.productPob", 0]
                                    }
                                }
                            }

                            },
                            // Stage 5
    {
        $lookup: {
           "from": "UserInfo",
           "localField": "_id.submitBy",
           "foreignField": "userId",
           "as": "UserDetail"
        }
      },
  
      // Stage 6
      {
        $unwind: {
         path: "$UserDetail",
         preserveNullAndEmptyArrays: true
        }
        
      },
  
      // Stage 7
      {
        $unwind: {
        path: "$UserDetail.divisionId",
        preserveNullAndEmptyArrays: true
        }
      },
      
    // Stage 8
    {
        $lookup: {
           "from": "DivisionMaster",
           "localField": "UserDetail.divisionId",
           "foreignField": "_id",
           "as": "division"
        }
      },
  
      // Stage 9
      {
        $lookup: {
        "from": "UserLogin",
        "localField": "UserDetail.userId",
        "foreignField": "_id",
        "as": "userLogin"
        }
      },
  
      // Stage 10
      {
        $unwind: { 
                   path: "$userLogin",
                   preserveNullAndEmptyArrays: true
        }
      },
  
      // Stage 11
      {
        $lookup: {
           "from": "District",
           "localField": "UserDetail.districtId",
           "foreignField": "_id",
           "as": "districtDetail"
        }
      },
  
      // Stage 12
      {
        $unwind: {
                  path: "$districtDetail",
                  preserveNullAndEmptyArrays: true
                                            }
      },
// Stage 13
{
    $project: {
    
                                        _id: 0,
                                        userId: "$_id.submitBy",
                                        totalInvestmentdate: {
                                            $size: "$totalInvestmentdate"
                                        },
                                        divisionName: {
                                            $arrayElemAt: ["$division.divisionName", 0]
                                        },
                                        divisionId: {
                                            $arrayElemAt: ["$division._id", 0]
                                        },
                                        lastsubmissionDate: "$lastsubmissionDate",
                                        UserName: "$UserDetail.name",
                                        UserDesignation: "$UserDetail.designation",
                                        RMPCount: 1,
                                        DrugStoreCount: 1,
                                        stockistCount: 1,
                                        doctorPOB: 1,
                                        vendorPOB: 1,
                                        mobile: "$userLogin.mobile",
                                        dateOfJoining: "$userLogin.dateOfJoining",
                                        headquarter: "$districtDetail.districtName",
                                        employeeCode: "$userLogin.employeeCode",
    }
  },

  // Stage 14
  {
    $sort: {
    UserName: 1
    }
  },
   {
    allowDiskUse: true
}, function (err, result) {
   
    if (err) {
        sendMail.sendMail({collectionName: "Mgrinvestmentmaster", errorObject: err, paramsObject: params, methodName: "Mgrinvestmentmaster.getInvestmentDetails"});
        return cb(err);
    }
    if (!result) {
        var err = new Error('no records found');
        err.statusCode = 404;
        err.code = 'NOT FOUND';
        return cb(err);
    }
    return cb(false, result)
});

                } else {
                    cb(false, []);
                }
            });

        }else{

            var userIds = [];

            for (var i = 0; i < params.userIds.length; i++) {
                userIds.push(ObjectId(params.userIds[i]));
            }
            aMatch = {
                companyId:ObjectId(params.companyId),
                submitBy: {
                    $in: userIds
                },
                submissionDate: {
                    //$gte: new Date(moment.utc(params.fromDate).format()),
                    //$lte: new Date(moment.utc(params.toDate).add(1, 'days').format())
                    $gte: new Date(params.fromDate),
                    $lte: new Date(params.toDate)
                }
            }

            invCollection.aggregate(
                // Stage 1
                {
                    $match: aMatch
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "MGRInvestmentVisitDetails",
                        "localField": "_id",
                        "foreignField": "investmentId",
                        "as": "investmentDetail"
                    }
                }, 
                 // Stage 3
                {
                    $unwind: {
                        path: "$investmentDetail",
                        preserveNullAndEmptyArrays: true
                    }
                },
                // Stage 4
                {
                    $group: {

                        _id: {
                            "submitBy": "$submitBy"
                        },
                        totalInvestmentdate: {
                            $addToSet: "$submissionDate"
                        },
                        lastsubmissionDate: {
                            $last: "$submissionDate"
                        },
                        RMPCount: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$investmentDetail.providerType", "RMP"]
                                    }]
                                }, 1, 0]
                            }
                        },
                        DrugStoreCount: {
                            $sum: {
                                $cond: [{
                                    $or: [{
                                        $eq: ["$investmentDetail.providerType", "Drug"]
                                    }
                                    ]
                                }, 1, 0]
                            }
                        },
                        stockistCount: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$investmentDetail.providerType", "Stockist"]
                                    }]
                                }, 1, 0]
                            }
                        },
                        doctorPOB: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$investmentDetail.providerType", "RMP"]
                                    }]
                                }, "$investmentDetail.productPob", 0]
                            }
                        },
                        vendorPOB: {
                            $sum: {
                                $cond: [{
                                    $or: [{
                                        $eq: ["$investmentDetail.providerType", "Drug"]
                                    },
                                    {
                                        $eq: ["$investmentDetail.providerType", "Stockist"]
                                    }
                                    ]
                                }, "$investmentDetail.productPob", 0]
                            }
                        }
                    }

                    },
                    // Stage 5
                    {
                        $lookup: {
                           "from": "UserInfo",
                           "localField": "_id.submitBy",
                           "foreignField": "userId",
                           "as": "UserDetail"
                        }
                      },
                  
                      // Stage 6
                      {
                        $unwind: {
                         path: "$UserDetail",
                         preserveNullAndEmptyArrays: true
                        }
                        
                      },
                  
                      // Stage 7
                      {
                        $unwind: {
                        path: "$UserDetail.divisionId",
                        preserveNullAndEmptyArrays: true
                        }
                      },
                      
                    // Stage 8
                    {
                        $lookup: {
                           "from": "DivisionMaster",
                           "localField": "UserDetail.divisionId",
                           "foreignField": "_id",
                           "as": "division"
                        }
                      },
                  
                      // Stage 9
                      {
                        $lookup: {
                        "from": "UserLogin",
                        "localField": "UserDetail.userId",
                        "foreignField": "_id",
                        "as": "userLogin"
                        }
                      },
                  
                      // Stage 10
                      {
                        $unwind: { 
                                   path: "$userLogin",
                                   preserveNullAndEmptyArrays: true
                        }
                      },
                  
                      // Stage 11
                      {
                        $lookup: {
                           "from": "District",
                           "localField": "UserDetail.districtId",
                           "foreignField": "_id",
                           "as": "districtDetail"
                        }
                      },
                  
                      // Stage 12
                      {
                        $unwind: {
                                  path: "$districtDetail",
                                  preserveNullAndEmptyArrays: true
                                                            }
                      },
                // Stage 13
                {
                    $project: {
                    
                                                        _id: 0,
                                                        userId: "$_id.submitBy",
                                                        totalInvestmentdate: {
                                                            $size: "$totalInvestmentdate"
                                                        },
                                                        divisionName: {
                                                            $arrayElemAt: ["$division.divisionName", 0]
                                                        },
                                                        divisionId: {
                                                            $arrayElemAt: ["$division._id", 0]
                                                        },
                                                        lastsubmissionDate: "$lastsubmissionDate",
                                                        UserName: "$UserDetail.name",
                                                        UserDesignation: "$UserDetail.designation",
                                                        RMPCount: 1,
                                                        DrugStoreCount: 1,
                                                        stockistCount: 1,
                                                        doctorPOB: 1,
                                                        vendorPOB: 1,
                                                        mobile: "$userLogin.mobile",
                                                        dateOfJoining: "$userLogin.dateOfJoining",
                                                        headquarter: "$districtDetail.districtName",
                                                        employeeCode: "$userLogin.employeeCode",
                    }
                }, {
                    $sort: {
                        UserName: 1
                    }
                }, {
                    allowDiskUse: true
                },
                function (err, result) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Mgrinvestmentmaster", errorObject: err, paramsObject: params, methodName: "Mgrinvestmentmaster.getInvestmentDetails"});
                        return cb(err);
                    }
                    if (!result) {
                        var err = new Error('no records found');
                        err.statusCode = 404;
                        err.code = 'NOT FOUND';
                        return cb(err);
                    }
                    return cb(false, result)
                });
            
                    
                }
                
            }
    Mgrinvestmentmaster.remoteMethod(
        'getInvestmentDetails', {
            description: 'investmentDetails for selected details',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],

            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        });



        Mgrinvestmentmaster.getUserDateWiseInvestmentDetail=function(userId, fromDate, toDate, cb) {
            var self = this;
        var investmentCollection = self.getDataSource().connector.collection(Mgrinvestmentmaster.modelName);

        investmentCollection.aggregate(
            // Stage 1
            {
                $match: {
                    submitBy: ObjectId(userId),
                    submissionDate: {
                        $gte: new Date(fromDate),
                        $lte: new Date(toDate)

                    }
                }
            },
            // Stage 2
            {
                $lookup: {
                    "from": "MGRInvestmentVisitDetails",
                    "localField": "_id",
                    "foreignField": "investmentId",
                    "as": "investmentDetail"
                }
            }, 
             // Stage 3
            {
                $unwind: {
                    path: "$investmentDetail",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                   "from": "UserInfo",
                   "localField": "submitBy",
                   "foreignField": "userId",
                   "as": "UserDetail"
                }
              },
          
              // Stage 6
              {
                $unwind: {
                 path: "$UserDetail",
                 preserveNullAndEmptyArrays: true
                }

        },{
        $group: {
            _id: {
                "investmentId": "$_id",
                "userId": "$submitBy",
                "submissionDate": "$submissionDate"
            },
            RMPCount: {
                $sum: {
                    $cond: [{
                        $and: [
                            { $eq: ["$investmentDetail.providerType", "RMP"] },
                            //  { $ne: ["$dcrDetail.providerStatus", "unlisted"]}
                        ]
                    }, 1, 0]
                }
            }, 
            doctorPOB: {
                $sum: {
                    $cond: [{
                        $and: [
                            { $eq: ["$investmentDetail.providerType", "RMP"] },
                        ]
                    }, "$investmentDetail.productPob", 0]
                }
            },
            DrugStoreCount: {
                $sum: {
                    $cond: [{
                        $or: [
                            { $eq: ["$investmentDetail.providerType", "Drug"] },
                        ]
                    }, 1, 0]
                }
            }, stockistCount: {
                $sum: {
                    $cond: [{
                        $or: [
                            { $eq: ["$investmentDetail.providerType", "Stockist"] },
                        ]
                    }, 1, 0]
                }
            },
            vendorPOB: {
                $sum: {
                    $cond: [{
                        $or: [
                            { $eq: ["$investmentDetail.providerType", "Drug"] },
                            { $eq: ["$investmentDetail.providerType", "Stockist"] }
                        ]
                    }, "$investmentDetail.productPob", 0]
                }
            }, jointCalls: {
                $addToSet: "$investmentDetail.providerId"
            },
            visitedBlock: { $addToSet: "$visitedBlock" },
            jointWork: { $addToSet: "$jointWork" },
            productDetails : { $addToSet: "$investmentDetail.productDetails" },
            giftDetails : { $addToSet: "$investmentDetail.giftDetails" },
            remarks: { $addToSet: "$remarks" },
            name: { $addToSet: "$UserDetail.name" },


        }
    },
    // Stage 5
    {
    $project: {
        _id: 0,
        investmentId: "$_id.investmentId",
        userId: "$_id.userId",
        submissionDate: "$_id.submissionDate",
        RMPCount: 1,
        stockistCount: 1,
        DrugStoreCount: 1,
        visitedBlock: { $arrayElemAt: ["$visitedBlock", 0] },
        jointWork: { $arrayElemAt: ["$jointWork", 0] },
        productDetails: 1,
        giftDetails: 1,
        remarks: { $arrayElemAt: ["$remarks", 0] },
        doctorPOB: 1,
        vendorPOB: 1,
        userName: { $arrayElemAt: ["$name", 0] },

    }
}, {
    $sort: {
        submissionDate: 1

    }
},
function (err, result) {
    if (err) {
        sendMail.sendMail({collectionName: "Mgrinvestmentmaster", errorObject: err, paramsObject: params, methodName: "Mgrinvestmentmaster.getUserDateWiseInvestmentDetail"});
        return cb(err);
    }
    if (!result) {
        var err = new Error('no records found');
        err.statusCode = 404;
        err.code = 'NOT FOUND';
        return cb(err);
    }
    return cb(false, result)

});


    }
    
    Mgrinvestmentmaster.remoteMethod('getUserDateWiseInvestmentDetail', {
        description: "Date wise Investment details",
        accepts: [{
            arg: 'userId',
            type: 'string'
        },
        {
            arg: 'fromDate',
            type: 'string'
        },
        {
            arg: 'toDate',
            type: 'string'
        }
        ],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }

    })


    Mgrinvestmentmaster.getUserInvestmentDetail=function(userId, fromDate, toDate, cb) {
        var self = this;
    var investmentCollection = self.getDataSource().connector.collection(Mgrinvestmentmaster.modelName);

    investmentCollection.aggregate(
        // Stage 1
        {
            $match: {
                submitBy: ObjectId(userId),
                submissionDate: {
                    $gte: new Date(fromDate),
                    $lte: new Date(toDate)

                }
            }
        },
        // Stage 2
        {
            $lookup: {
                "from": "MGRInvestmentVisitDetails",
                "localField": "_id",
                "foreignField": "investmentId",
                "as": "investmentDetail"
            }
        }, 
         // Stage 3
        {
            $unwind: {
                path: "$investmentDetail",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
               "from": "UserInfo",
               "localField": "submitBy",
               "foreignField": "userId",
               "as": "UserDetail"
            }
          },
      
          // Stage 6
          {
            $unwind: {
             path: "$UserDetail",
             preserveNullAndEmptyArrays: true
            }

    },
    {
        $lookup: {
            "from" : "Providers",
            "localField" : "investmentDetail.providerId",
            "foreignField" : "_id",
            "as" : "providers"
        }
      },
  
      // Stage 6
      {
        $unwind: {
         path: "$providers",
         preserveNullAndEmptyArrays: true
        }

},
{
    $lookup: {
        "from" : "Area",
    "localField" : "providers.blockId",
    "foreignField" : "_id",
    "as" : "areas"
    }
  },

  // Stage 6
  {
    $unwind: {
     path: "$areas",
     preserveNullAndEmptyArrays: true
    }

},


{
    $group: {

        _id: {
            "investmentId": "$_id",
            "userId": "$submitBy",
            "submissionDate": "$submissionDate",
            "providerId":"$investmentDetail.providerId"
        },
       pob: {
            $sum: {
                $cond: [{
                }, "$investmentDetail.productPob", 0]
            }
        },
        doctorPOB: {
            $sum: {
                $cond: [{
                    $and: [
                        { $eq: ["$investmentDetail.providerType", "RMP"] },
                    ]
                }, "$investmentDetail.productPob", 0]
            }
        },vendorPOB: {
            $sum: {
                $cond: [{
                    $or: [
                        { $eq: ["$investmentDetail.providerType", "Drug"] },
                        { $eq: ["$investmentDetail.providerType", "Stockist"] }
                    ]
                }, "$investmentDetail.productPob", 0]
            }
        },
        productDetails : { $addToSet: "$investmentDetail.productDetails" },
        giftDetails : { $addToSet: "$investmentDetail.giftDetails" },
        providerName : { $addToSet: "$providers.providerName" },
        providerType : { $addToSet: "$providers.providerType" },
        qualification : { $addToSet: "$providers.degree" },
        areaName : { $addToSet: "$areas.areaName" },
    }
},
// Stage 5
{
$project: {
    _id: 0,
    investmentId: "$_id.investmentId",
    userId: "$_id.userId",
    submissionDate: "$_id.submissionDate",
    productDetails: 1,
    giftDetails: 1,
    doctorPOB: 1,
    vendorPOB: 1,
    pob : 1,
    providerName : { $arrayElemAt: ["$providerName", 0] },
        providerType: { $arrayElemAt: ["$providerType", 0] },
        qualification : { $arrayElemAt: ["$qualification", 0] },
        areaName: { $arrayElemAt: ["$areaName", 0] },
}
}, {
$sort: {
    submissionDate: 1

}
},
function (err, result) {
if (err) {
    sendMail.sendMail({collectionName: "Mgrinvestmentmaster", errorObject: err, paramsObject: params, methodName: "Mgrinvestmentmaster.getUserInvestmentDetail"});
    return cb(err);
}
if (!result) {
    var err = new Error('no records found');
    err.statusCode = 404;
    err.code = 'NOT FOUND';
    return cb(err);
}
return cb(false, result)

});


}

Mgrinvestmentmaster.remoteMethod('getUserInvestmentDetail', {
    description: "Date wise Investment details",
    accepts: [{
        arg: 'userId',
        type: 'string'
    },
    {
        arg: 'fromDate',
        type: 'string'
    },
    {
        arg: 'toDate',
        type: 'string'
    }
    ],
    returns: {
        root: true,
        type: 'array'
    },
    http: {
        verb: 'get'
    }

})

};
