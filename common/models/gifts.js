var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail')
'use strict';
module.exports = function(Gifts) {

    Gifts.createGift = function(params, cb) {
        var self = this;
        var GiftCollection = this.getDataSource().connector.collection(Gifts.modelName);
        
        Gifts.find({
            where: {

                companyId: params.companyId,
                giftName: params.giftName,
                giftCode: params.giftCode
            }

        }, function(err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'createGift'});
            }
            if (response.length > 0) {
                var err = new Error('Gift Already Exists');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            } else {
                var giftDetails = {
                    companyId: ObjectId(params.companyId),
                    giftName: params.giftName,
                    giftCode: params.giftCode,
                    giftCost: params.giftMrp,
                    assignedStates: params.assignedStates,
                    createdBy: ObjectId(params.id),
                    updatedBy: "",
                    status: params.status,
                    createdAt: params.createdAt,
                    updatedAt: params.updatedAt

                }
                GiftCollection.insertOne(giftDetails, function(err, res) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'createGift'});
                        // console.log(err);
                        return cb(err);
                    }
                    return cb(false, res)
                })

            }

        })

    }
    Gifts.remoteMethod(
        'createGift', {
            description: 'Created Gift',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )

    //---------------------------------
    Gifts.getGiftDetail = function(out, cb) {
        var self = this;
        var viewGiftCollection = this.getDataSource().connector.collection(Gifts.modelName);

        viewGiftCollection.aggregate({
                // Stage 1

                $match: {
                    companyId: ObjectId(out.companyId),
                    status: true
                }
            }, {
                $group: {
                    "_id": {
                        _id: "$_id",
                        giftName: "$giftName",
                        giftCode: "$giftCode",
                        giftCost: "$giftCost",
                        status: "$status"
                    }

                }
            }, {
                $project: {
                    _id: 1,
                    _id: "$_id._id",
                    giftName: "$_id.giftName",
                    giftCode: "$_id.giftCode",
                    giftCost: "$_id.giftCost",
                    status: "$_id.status"
                }
            },


            function(err, result) {
                if(err){
                    sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'getGiftDetail'});
                }
                cb(false, result)

            })

    }
    Gifts.remoteMethod(
        'getGiftDetail', {
            description: 'View Gift',
            accepts: [{
                arg: 'out',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
    
    Gifts.editGift = function(params, cb) {

        var self = this;
        var GiftEditCollection = this.getDataSource().connector.collection(Gifts.modelName);

        var editvalue = {
            _id: params.id,
            giftName: params.giftName,
            giftCode: params.giftCode,
            giftCost: params.giftCost

        }
        Gifts.find({
            where: {
                companyId: ObjectId(params.companyId),
                _id: ObjectId(params.id)
            }
        }, function(err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'editGift'});
            }
            if (response.length > 0) {
                GiftEditCollection.update({
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params.id)
                }, {
                    $set: {
                        giftName: params.giftName,
                        giftCode: params.giftCode,
                        giftCost: params.giftCost,
                        updatedBy: ObjectId(params.id),
                        updatedAt: new Date()

                    }
                }, function(err, res) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'editGift'});
                        // console.log(err);
                        return cb(err);
                    } else {
                        return cb(false, res)
                        // console.log("Gift has been updated")
                    }
                })

            } else {
                var err = new Error('Gift cannot be Edit.');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            }
        })



    }


    Gifts.remoteMethod(
        'editGift', {
            description: 'Edit Gifts',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )





    Gifts.deleteGift = function(params, cb) {
        var self = this;
        var GiftCollection = this.getDataSource().connector.collection(Gifts.modelName);

        Gifts.find({
            where: {
                companyId: ObjectId(params.companyId),
                _id: ObjectId(params._id)
            }
        }, function(err, response) {
            if(err){
                sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'deleteGift'});
            }
            if (response.length > 0) {
                GiftCollection.update({
                    companyId: ObjectId(params.companyId),
                    _id: ObjectId(params._id)
                }, {
                    $set: {
                        deletedAt: new Date(),
                        deletedBy: ObjectId(params.id),
                        "status": false
                    }
                }, function(err, res) {
                    if (err) {
                        sendMail.sendMail({collectionName: "Gifts", errorObject: err, paramsObject: params, methodName: 'deleteGift'});
                        // console.log(err);
                        return cb(err);
                    } else {
                        return cb(false, res)
                        // console.log("Status has been inactive")
                    }
                })

            } else {
                var err = new Error('Gift cannot be deleted.');
                err.statusCode = 409;
                err.code = 'Validation failed';
                return cb(err);
            }
        })
    }
    Gifts.remoteMethod(
        'deleteGift', {
            description: 'Delete Gifts',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )


};