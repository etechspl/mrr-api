'use strict';
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
const moment = require('moment');
module.exports = function (Userareamapping) {
    Userareamapping.updateAllRecord = function (records, cb) {
        var self = this;
        var response = [];
        var ids = [];
        var UserareaMappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        let returnData = records;
        for (var w = 0; w < records.length; w++) {
            let set = {};
            if (records[w].appStatus == "approved" || records[w].appStatus == "InProcess") {
                set = {
                    "appByMgr": ObjectId(records[w].appByMgr),
                    "appStatus": records[w].appStatus,
                    "finalAppBy": ObjectId(records[w].finalAppBy),
                    "finalAppDate": new Date(records[w].finalAppDate),
                    "mgrAppDate": new Date(records[w].mgrAppDate),
                    "status": records[w].status,
                };
            } else if (records[w].delStatus != "") {
                set = {
                    "delStatus": records[w].delStatus,
                    "delByMgr": ObjectId(records[w].delByMgr),
                    "mgrDelDate": new Date(records[w].mgrDelDate),
                    "finalDelBy": ObjectId(records[w].finalDelBy),
                    "finalDelDate": new Date(records[w].finalDelDate),
                    "status": records[w].status,
                };
            }
            //console.log(set);
            //console.log(records[w].id);
            UserareaMappingCollection.update(
                { "_id": ObjectId(records[w]._id) },
                { $set: set },
                function (err, result) {
                    //Result
                    if (err) {
                        console.log("err"); console.log(err);
                    }
                });
            console.log("Area Id : " + records[w].areaId)
            Userareamapping.app.models.Area.update({
                "_id": ObjectId("5c48e1362cd97a1a4c79d82e")//ObjectId(records[w].areaId)
            }, set, function (err, result) {
                console.log("Test Done : ", result)
            });
        }
        //console.log("after : ",returnData)
        return cb(false, returnData);
    };
    // End DCR Record
    // Update All Data for sync
    Userareamapping.remoteMethod(
        'updateAllRecord', {
        description: 'update and send response all data',
        accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );
    //-----------update latlong based on areaId----preeti arora----------
    Userareamapping.updateLocation = function (params, cb) {
        var self = this;
        var UserareamappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        for (let i = 0; i < params.length; i++) {
            var geoLocation = [params[i].latitude, params[i].longitude]
            let areaID = ObjectId(params[i].areaId);
            UserareamappingCollection.update({ areaId: areaID }, {
                $set: {
                    geoLocation: geoLocation
                }
            }, function (err, result) {
                // return cb(false,result);
            });
        }
    }
    Userareamapping.remoteMethod('updateLocation', {
        description: 'update location based on areaIds',
        accepts: [{
            arg: 'params',
            type: 'array'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    });
    //--------------------------------------
    Userareamapping.getUserMappedBlocks = function (companyId, userId, cb) {
        var self = this;
        var UserareamappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        var supervisiorId = [];
        supervisiorId[0] = userId;
        var param = {
            supervisorId: supervisiorId,
            companyId: ObjectId(companyId),
            status: true
        };
        var match = {};
        Userareamapping.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
            if (res.length > 0) {
                match = {
                    "companyId": ObjectId(companyId),
                    "userId": { $in: res[0].userId },
                    "status": true,
                    "appStatus": 'approved'
                }
            } else {
                match = {
                    "companyId": ObjectId(companyId),
                    "userId": ObjectId(userId),
                    "status": true,
                    "appStatus": 'approved'
                }
            }
            UserareamappingCollection.aggregate(
                // Stage 1
                {
                    $match: match
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "Area",
                        "localField": "areaId",
                        "foreignField": "_id",
                        "as": "areaInfo"
                    }
                }, {
                $lookup: {
                    "from": "State",
                    "localField": "stateId",
                    "foreignField": "_id",
                    "as": "states"
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "districtId",
                    "foreignField": "_id",
                    "as": "districts"
                }
            }, {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "users"
                }
            },
                // Stage 5
                {
                    $project: {
                        stateId: {
                            $arrayElemAt: ["$states._id", 0]
                        },
                        stateName: {
                            $arrayElemAt: ["$states.stateName", 0]
                        },
                        districtId: {
                            $arrayElemAt: ["$districts._id", 0]
                        },
                        districtName: {
                            $arrayElemAt: ["$districts.districtName", 0]
                        },
                        userName: {
                            $arrayElemAt: ["$users.name", 0]
                        },
                        areaName: {
                            $arrayElemAt: ["$areaInfo.areaName", 0]
                        },
                        noOfBeds: {
                            $arrayElemAt: ["$areaInfo.noOfBeds", 0]
                        },
                        areaCode: {
                            $arrayElemAt: ["$areaInfo.areaCode", 0]
                        },
                        areaId: {
                            $arrayElemAt: ["$areaInfo._id", 0]
                        },
                        type: {
                            $arrayElemAt: ["$areaInfo.type", 0]
                        },
                        userId: 1,
                        status: 1,
                        appStatus: 1,
                        delStatus: 1
                    }
                },
				////STEP 6
				{
				 $sort: {	
					"areaName":1
				}
				},
                function (err, result) {
                    if (err) {
                        return cb(err);
                    }
                    return cb(false, result);
                }
            );
        });
    }
    Userareamapping.remoteMethod('getUserMappedBlocks', {
        description: 'get mapped blocks of User',
        accepts: [{
            arg: 'companyId',
            type: 'string'
        }, {
            arg: 'userId',
            type: 'string'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    });
    //------------------------Praveen Kumar(24-01-2019 12:54PM)----------------------------------------------
    Userareamapping.getUserMappedBlocksForMobile = function (companyId, userId, cb) {
        var self = this;
        var UserareamappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        var supervisiorId = [];
        supervisiorId[0] = userId;
        var param = {
            supervisorId: supervisiorId,
            companyId: ObjectId(companyId),
            status: true
        };
        var match = {};
        Userareamapping.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
            if (res.length > 0) {
                var userIds = [ObjectId(userId)]; // For Manager Area 12-04-2019
                for (var n = 0; n < res[0].userId.length; n++) {
                    userIds.push(ObjectId(res[0].userId[n]))
                }
                match = {
                    "companyId": ObjectId(companyId),
                    "userId": { $in: userIds },
                    areaId: { $ne: "" }
                }
            } else {
                match = {
                    "companyId": ObjectId(companyId),
                    "userId": ObjectId(userId),
                    areaId: { $ne: "" }
                }
            }
            UserareamappingCollection.aggregate(
                // Stage 1
                {
                    $match: match
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "Area",
                        "localField": "areaId",
                        "foreignField": "_id",
                        "as": "areaInfo"
                    }
                }, {
                $lookup: {
                    "from": "State",
                    "localField": "stateId",
                    "foreignField": "_id",
                    "as": "states"
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "districtId",
                    "foreignField": "_id",
                    "as": "districts"
                }
            }, {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "users"
                }
            },
                // Stage 5
                {
                    $project: {
                        stateId: {
                            $arrayElemAt: ["$states._id", 0]
                        },
                        stateName: {
                            $arrayElemAt: ["$states.stateName", 0]
                        },
                        districtId: {
                            $arrayElemAt: ["$districts._id", 0]
                        },
                        districtName: {
                            $arrayElemAt: ["$districts.districtName", 0]
                        },
                        userName: {
                            $arrayElemAt: ["$users.name", 0]
                        },
                        areaName: {
                            $arrayElemAt: ["$areaInfo.areaName", 0]
                        },
                        noOfBeds: {
                            $arrayElemAt: ["$areaInfo.noOfBeds", 0]
                        },
                        areaCode: {
                            $arrayElemAt: ["$areaInfo.areaCode", 0]
                        },
                        areaId: {
                            $arrayElemAt: ["$areaInfo._id", 0]
                        },
                        type: {
                            $arrayElemAt: ["$areaInfo.type", 0]
                        },
                        userId: 1,
                        status: 1,
                        appStatus: 1,
                        delStatus: 1,
                        "mgrAppDate": 1,
                        "finalAppDate": 1,
                        "appByMgr": 1,
                        "finalAppBy": 1,
                        "delStatus": 1,
                        "mgrDelDate": 1,
                        "finalDelDate": 1,
                        "delByMgr": 1,
						  rejectAddStatus:1,
                        rejectDelStatus:1,
                        rejectionMessage:1,
                        "finalDelBy": 1,
                        "deleteReason": 1   //Added By PK 2019-08-26
                    }
                },
                function (err, result) {
                    if (err) {
                        return cb(err);
                    }
                    return cb(false, result);
                }
            );
        });
    }
    Userareamapping.remoteMethod('getUserMappedBlocksForMobile', {
        description: 'get mapped blocks of User',
        accepts: [{
            arg: 'companyId',
            type: 'string'
        }, {
            arg: 'userId',
            type: 'string'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    });
    //--------------------------------------------END---------------------------------------------------------
    //----------------------------------Praveen Kuumar(27-11-2018)-------------------------------------------
    Userareamapping.getAllUserIdsOnAreaBasis = function (companyId, areaIds, cb) {
        var self = this;
        var UserareamappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        let areaIdArr = [];
        for (var w in areaIds) {
            areaIdArr.push(ObjectId(areaIds[w]));
        }
        UserareamappingCollection.aggregate(
            // Stage 1
            {
                $match: {
                    areaId: {
                        $in: areaIdArr
                    },
                    "appStatus": 'approved',
                    status: true
                }
            },
            // Stage 2
            {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "userPersonalDetail"
                }
            },
            // Stage 3
            {
                $unwind: "$userPersonalDetail"
            },
            // Stage 4
            {
                $project: {
                    _id: 0,
                    userId: 1,
                    name: "$userPersonalDetail.name",
                    designation: 1,
                    joiningDate: "$promotedOn"
                }
            },
            // Stage 5
            {
                $group: {
                    _id: {
                    },
                    userIds: {
                        $addToSet: "$userId"
                    },
                    obj: {
                        $addToSet: {
                            userId: "$userId",
                            name: "$name",
                            designation: "$designation",
                            joiningDate: "$joiningDate"
                        }
                    }
                }
            },
            function (err, result) {
                // console.log(result);
                return cb(false, result)
            });
    };
    Userareamapping.remoteMethod(
        'getAllUserIdsOnAreaBasis', {
        description: 'get User Id On Area Basis',
        accepts: [{
            arg: 'companyId',
            type: 'string'
        }, {
            arg: 'areaIds',
            type: 'array'
        }
        ],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //-------------------------------------------END---------------------------------------------------------
    Userareamapping.deleteAreaAndMapping = function (params, cb) {
        var self = this;
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        Userareamapping.app.models.Area.deleteArea(params, function (result) {
            Userareamapping.find({
                where: {
                    companyId: ObjectId(params.companyId),
                    _id: params.hasOwnProperty("id") ? ObjectId(params.id) : undefined,
                    areaId: ObjectId(params.areaId),
                    //  status: true
                }
            }, function (err, response) {
                if (response.length > 0) {
                    let aSet = {};
                    if (params.rL === 1) {
                        if (params.validationRole > 0) {
                            aSet = {
                                delStatus: params.appStatus == 'pending' ? 'approved' : 'pending',
                                deleteReason: params.deleteReason,
                                mgrDelDate: new Date('1900-01-02T 00:00:00'),
                                finalDelDate: new Date('1900-01-02T 00:00:00'),
                                delByMgr: '',
                                finalDelBy: ''
                            };
                        } else {
                            aSet = {
                                status: false,
                                delStatus: 'approved',
                                deleteReason: params.deleteReason,
                                mgrDelDate: new Date(),
                                finalDelDate: new Date(),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: ObjectId(params.updatedBy)
                            };
                        }
                    } else if (params.rL === 2) {
                        if (params.validationRole == 0 || params.validationRole == 1) {
                            aSet = {
                                status: false,
                                delStatus: 'approved',
                                deleteReason: params.deleteReason,
                                mgrDelDate: new Date(),
                                finalDelDate: new Date(),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: ObjectId(params.updatedBy)
                            };
                        } else if (params.validationRole == 2) {
                            aSet = {
                                delStatus: 'InProcess',
                                deleteReason: params.deleteReason,
                                mgrDelDate: new Date(),
                                finalDelDate: new Date('1900-01-02T 00:00:00'),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: ''
                            };
                        }
                    } else if (params.rL === 0) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    }
                 
                    UserareamappingCollection.update({
                        companyId: ObjectId(params.companyId),
                        _id: params.hasOwnProperty("id") ? ObjectId(params.id) : undefined,
                        areaId: ObjectId(params.areaId)
                    }, {
                        $set: aSet
                    }, function (err, res) {
                        if (err) {
                            console.log(err);
                            return cb(err);
                        } else {
                            return cb(false, res)
                        }
                    })
                } else {
                    var err = new Error('Mapping cannot be deleted.');
                    err.statusCode = 409;
                    err.code = 'Validation failed';
                    return cb(err);
                }
            })
        });
    }
    Userareamapping.remoteMethod(
        'deleteAreaAndMapping', {
        description: 'Delete Deatils',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'get'
        }
    }
    )
    //by ravi for approval master on 29-12-2018
    Userareamapping.getUserMappedBlocksForApprovalDeletionRequest = function (params, cb) {
        var self = this;
        var UserareamappingCollection = self.getDataSource().connector.collection(Userareamapping.modelName);
        let dynamicMatch1 = {};
        let dynamicMatch2 = {};
        let userArr = [];
        for (let i = 0; i < params.userInfo.length; i++) {
            userArr.push(ObjectId(params.userInfo[i]));
        }
		
		console.log("params",params);
        if (params.requestType === 'approval') {
            dynamicMatch1 = {
                "companyId": ObjectId(params.companyId),
                "userId": { $in: userArr },
                "status": false,
                delStatus: '',
                "appStatus": 'pending'
            }
            dynamicMatch2 = {
                "areaInfo.status": false,
                "areaInfo.appStatus": "pending"
            }
            if (params.rL === 0) {
                if (params.approvalUpto === 2) {
                    dynamicMatch1.appStatus = 'InProcess';
                    dynamicMatch2 = {
                        "areaInfo.status": false,
                        "areaInfo.appStatus": "InProcess"
                    }
                }
            }
        } else if (params.requestType === 'delete') {
            dynamicMatch1 = {
                "companyId": ObjectId(params.companyId),
                "delStatus": "pending",
                status: true,
                "userId": { $in: userArr }
            }
            dynamicMatch2 = {
                "areaInfo.delStatus": "pending",
                status: true
            }
            if (params.rL === 0) {
                if (params.approvalUpto === 2) {
                    dynamicMatch2 = {
                        "areaInfo.delStatus": "InProcess",
                        status: true
                    }
                }
            }
        }
        UserareamappingCollection.aggregate(
            // Stage 1
            {
                $match: dynamicMatch1
            },
            // Stage 2
            {
                $lookup: {
                    "from": "Area",
                    "localField": "areaId",
                    "foreignField": "_id",
                    "as": "areaInfo"
                }
            },
            // Stage 2
            {
                $unwind: { path: "$areaInfo", preserveNullAndEmptyArrays: true }
            },
            // Stage 3
            {
                $match: dynamicMatch2
            }, {
            $lookup: {
                "from": "State",
                "localField": "stateId",
                "foreignField": "_id",
                "as": "states"
            }
        }, {
            $lookup: {
                "from": "District",
                "localField": "districtId",
                "foreignField": "_id",
                "as": "districts"
            }
        }, {
            $lookup: {
                "from": "UserLogin",
                "localField": "userId",
                "foreignField": "_id",
                "as": "users"
            }
        },
            // Stage 5
            {
                $project: {
                    stateId: {
                        $arrayElemAt: ["$states._id", 0]
                    },
                    stateName: {
                        $arrayElemAt: ["$states.stateName", 0]
                    },
                    districtId: {
                        $arrayElemAt: ["$districts._id", 0]
                    },
                    districtName: {
                        $arrayElemAt: ["$districts.districtName", 0]
                    },
                    userName: {
                        $arrayElemAt: ["$users.name", 0]
                    },
                    areaName: "$areaInfo.areaName",
                    areaCode: "$areaInfo.areaCode",
                    areaId: "$areaInfo._id",
                    type: "$areaInfo.type",
                    status: 1,
                    appStatus: 1
                }
            },
            function (err, result) {
                if (err) {
                    return cb(err);
                }
                return cb(false, result);
            }
        );
    }
    Userareamapping.remoteMethod('getUserMappedBlocksForApprovalDeletionRequest', {
        description: 'get mapped blocks of User',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    });
    Userareamapping.approveAreaAndMapping = function (params, cb) {
        var self = this;
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        let areaIds = [];
        for (let i = 0; i < params.approveDetails.length; i++) {
            areaIds.push(ObjectId(params.approveDetails[i].areaId));
        }
        params.areaIds = areaIds;
        Userareamapping.app.models.Area.approveArea(params, function (err, result) {
            if (result.status > 0) {
                let mSet = {
                };
                if (params.requestType === 'approve') {
                    if (params.rL > 1) {
                        if (params.validationRole === 1) {
                            mSet = {
                                status: true,
                                appStatus: 'approved',
                                mgrAppDate: new Date(),
                                finalAppDate: new Date(),
                                appByMgr: ObjectId(params.updatedBy),
                                finalAppBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                appStatus: 'InProcess',
                                mgrAppDate: new Date(),
                                finalAppDate: new Date('1900-01-02T 00:00:00'),
                                appByMgr: ObjectId(params.updatedBy),
                                finalAppBy: '',
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    } else {
                        if (params.validationRole === 1) {
                            mSet = {
                                status: true,
                                appStatus: 'approved',
                                mgrAppDate: new Date(),
                                finalAppDate: new Date(),
                                appByMgr: ObjectId(params.updatedBy),
                                finalAppBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                status: true,
                                appStatus: 'approved',
                                finalAppDate: new Date(),
                                finalAppBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    }
                } else if (params.requestType === 'delete') {
                    if (params.rL > 1) {
                        if (params.validationRole === 1) {
                            mSet = {
                                status: false,
                                delStatus: 'approved',
                                mgrDelDate: new Date(),
                                finalDelDate: new Date(),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                appStatus: 'InProcess',
                                mgrDelDate: new Date(),
                                finalDelDate: new Date('1900-01-02T 00:00:00'),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: '',
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    } else {
                        if (params.validationRole === 1) {
                            mSet = {
                                status: false,
                                delStatus: 'approved',
                                mgrDelDate: new Date(),
                                finalDelDate: new Date(),
                                delByMgr: ObjectId(params.updatedBy),
                                finalDelBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                status: false,
                                delStatus: 'approved',
                                finalDelDate: new Date(),
                                finalDelBy: ObjectId(params.updatedBy),
                                updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    }
                }  else if (params.requestType === 'rejectAddRequest') {
                    if (params.rL > 1) {
                        if (params.validationRole === 1) {
                            mSet = {
                        rejectionAddDate: new Date(),
                        rejectedBy: ObjectId(params.updatedBy),
                        rejectionMessage: params.rejectionMessage,
						rejectAddStatus:true,
						updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                appStatus: 'InProcess',
                                rejectionAddDate: new Date(),
                                rejectedBy: ObjectId(params.updatedBy),
                                rejectionMessage: params.rejectionMessage,
                                rejectAddStatus:true,
                                updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    } else {
                        if (params.validationRole === 1) {
                            mSet = {
                                rejectionAddDate: new Date(),
                        rejectedBy: ObjectId(params.updatedBy),
                        rejectionMessage: params.rejectionMessage,
						rejectAddStatus:true,
						updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                            };
                        } else if (params.validationRole === 2) {
                            mSet = {
                                appStatus: 'InProcess',
                                rejectionAddDate: new Date(),
                                rejectedBy: ObjectId(params.updatedBy),
                                rejectionMessage: params.rejectionMessage,
                                rejectAddStatus:true,
                                updatedAt : new Date(moment.utc().add('1270', 'minutes'))
                            };
                        }
                    }
                }
                UserareamappingCollection.update({
                    companyId: ObjectId(params.companyId),
                    areaId: { $in: areaIds }
                }, {
                    $set: mSet
                }, {
                    multi: true
                }, function (err, res) {
                    if (err) {
                        return cb(err);
                    }
                    let results = [];
                    results.push({
                        status: res.result.nModified
                    });
                    cb(false, results)
                });
            }
        });
    }
    Userareamapping.remoteMethod(
        'approveAreaAndMapping', {
        description: 'approveAreaAndMapping',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
    //End
    // by ravi on 30-12-2018
    Userareamapping.getProviderOnUserBasis = function (param, cb) {
        var self = this;
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        let userIds = [];
        let dynamicMatch = {};
        let dynamicMatch1 = {};
        for (let i = 0; i < param.userInfo.length; i++) {
            userIds.push(ObjectId(param.userInfo[i]));
        }
        dynamicMatch = {
            "companyId": ObjectId(param.companyId),
            "userId": { $in: userIds }
        };
        if (param.requestType === 'approval') {
            dynamicMatch1 = {
                "providers.providerType": { $in: param.providerType },
                "providers.status": false,
                "providers.delStatus": '',
                "providers.appStatus": 'pending',
                "providers.rejectAddStatus": { $ne: true }
            };
            if (param.rL === 0) {
                if (param.approvalUpto === 2) {
                    dynamicMatch1 = {
                        "providers.providerType": { $in: param.providerType },
                        "providers.status": false,
                        "providers.delStatus": '',
                        "providers.appStatus": 'InProcess',
                        "providers.rejectAddStatus": { $ne: true }
                    };
                }
            }
        } else if (param.requestType === 'delete') {
            dynamicMatch1 = {
                "providers.providerType": { $in: param.providerType },
                "providers.delStatus": 'pending',
                "providers.status": true,
                //"providers.rejectDelStatus": false,
                "block.status": true
            };
            if (param.rL === 0) {
                if (param.approvalUpto === 2) {
                    dynamicMatch1 = {
                        "providers.providerType": { $in: param.providerType },
                        "providers.delStatus": 'InProcess',
                        "providers.status": true,
                    //    "providers.rejectDelStatus": false,
                        "block.status": true,
                    };
                }
            }
        }

        console.log("dynamicMatch=>",dynamicMatch);
        UserareamappingCollection.aggregate( // Stage 1
            // Stage 1
            {
                $match: dynamicMatch
            },
            {
                $lookup: {
                    "from": "Providers",
                    "localField": "areaId",
                    "foreignField": "blockId",
                    "as": "providers"
                }
            },
            {
                $lookup: {
                    "from": "UserLogin",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "users"
                }
            }, {
            $lookup: {
                "from": "State",
                "localField": "stateId",
                "foreignField": "_id",
                "as": "states"
            }
        }, {
            $lookup: {
                "from": "District",
                "localField": "districtId",
                "foreignField": "_id",
                "as": "districts"
            }
        },
            // Stage 3
            {
                $unwind: { path: "$providers", preserveNullAndEmptyArrays: true }
            },
            {
                $lookup: {
                    "from": "Area",
                    "localField": "providers.blockId",
                    "foreignField": "_id",
                    "as": "block"
                }
            },
            // Stage 8
            {
                $unwind: { path: "$block", preserveNullAndEmptyArrays: true }
            },
            {
                $match: dynamicMatch1
            },
            // Stage 4
            {
                $project: {
                    providerName: "$providers.providerName",
                    providerType: "$providers.providerType",
                    providerCode: "$providers.providerCode",
                    providercategory: "$providers.category",
                    providerphone: "$providers.phone",
                    providerstateName: {
                        $arrayElemAt: ["$states.stateName", 0]
                    },
                    providerdistrictName: {
                        $arrayElemAt: ["$districts.districtName", 0]
                    },
                    providerblockName: "$block.areaName",
                    userName: {
                        $arrayElemAt: ["$users.name", 0]
                    },
                    providerId: "$providers._id",
                    deleteReason: "$providers.deleteReason"
                }
            },
            function (err, response) {
                if (response.length > 0) {
                    return cb(false, response);
                } else {
                    return cb(false, []);
                }
            })
    }
    Userareamapping.remoteMethod(
        'getProviderOnUserBasis', {
        description: 'Getting getProviderOnUserBasis List',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
    // Praveen Singh getting Provider list with user name 09-01-2019
    Userareamapping.getProviderList = function (params, cb) {
        console.log(params);
        var self = this;
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        let match = {};
        let stateIds = [];
        let districtIds = []
        let areaIds = [];
        let users = [];
        let userIds = [];
        async.parallel({
            match: function (cb) {
                if (params.type === 'State' || params.type === 'Headquarter' || params.type === 'Area') {
                    for (let i = 0; i < params.stateId.length; i++) {
                        stateIds.push(ObjectId(params.stateId[i]));
                    }
                    match = {
                        companyId: ObjectId(params.companyId),
                        stateId: {
                            $in: stateIds
                        },
                        status: true,
                        appStatus: "approved"
                    }
                    if (params.type === 'Headquarter' || params.type === 'Area') {
                        for (let i = 0; i < params.districtId.length; i++) {
                            districtIds.push(ObjectId(params.districtId[i]));
                        }
                        match = {
                            companyId: ObjectId(params.companyId),
                            stateId: {
                                $in: stateIds
                            },
                            districtId: {
                                $in: districtIds
                            },
                            status: true,
                            appStatus: "approved"
                        }
                        if (params.type === 'Area') {
                            for (let i = 0; i < params.areaId.length; i++) {
                                areaIds.push(ObjectId(params.areaId[i]));
                            }
                            match = {
                                companyId: ObjectId(params.companyId),
                                stateId: {
                                    $in: stateIds
                                },
                                districtId: {
                                    $in: districtIds
                                },
                                areaId: {
                                    $in: areaIds
                                },
                                status: true,
                                appStatus: "approved"
                            }
                        }
                    }
                    cb(false, match);
                } else if (params.type === 'Employee Wise') {
                    if (params.designation > 1) {
                        Userareamapping.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(params.companyId),
                                supervisorId: {
                                    inq: params.userId //Supervisior Ids
                                },
                                status: true
                            }
                        }, function (err, hrcyResult) {
                            for (let i = 0; i < hrcyResult.length; i++) {
                                userIds.push(hrcyResult[i].userId);
                            }
                            //-------Preetis add manager self id---------------
                            for (let i = 0; i < params.userId.length; i++) {
                                userIds.push(params.userId[i]);// add manger userid also ...
                            }
                            match = {
                                companyId: ObjectId(params.companyId),
                                userId: {
                                    $in: userIds
                                },
                                status: true,
                                appStatus: "approved"
                            }
                            cb(false, match);
                        });
                    } else {
                        for (let i = 0; i < params.userId.length; i++) {
                            users.push(ObjectId(params.userId[i]));
                        }
                        match = {
                            companyId: ObjectId(params.companyId),
                            userId: {
                                $in: users
                            },
                            status: true,
                            appStatus: "approved"
                        }
                        cb(false, match);
                    }
                }
            }
        }, function (err, result) {
            //------Praveen Kumar(18-04-2019)----------------
            let providerMatch = {};
            if (params.status[0] == true) {
                providerMatch = {
                    providerType: {
                        $in: params.provoderType
                    },
                    providerStatus: true,
                    providerAppStatus: "approved",
                    category: { $in: params.category }
                }
            } else if (params.status[0] == false) {
                providerMatch = {
                    providerType: {
                        $in: params.provoderType
                    },
                    providerStatus: false,
                    providerDelStatus: "approved",
                    category: { $in: params.category }
                }
            }
            //------END Praveen Kumar(18-04-2019)----------------
            // console.log("result match--",result.match);
            UserareamappingCollection.aggregate({
                $match: result.match
            }, {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "userId",
                    "foreignField": "userId",
                    "as": "users"
                }
            }, {
                $unwind: "$users"
            }, {
                $match: {
                    "users.status": true
                }
            }, {
                $unwind: {
                    path: "$users.divisionId",
                    preserveNullAndEmptyArrays: true
                }
            }, {
                $lookup: {
                    "from": "DivisionMaster",
                    "localField": "users.divisionId",
                    "foreignField": "_id",
                    "as": "division"
                }
            }, {
                $lookup: {
                    "from": "Providers",
                    "localField": "areaId",
                    "foreignField": "blockId",
                    "as": "provider"
                }
            }, {
                $lookup: {
                    "from": "State",
                    "localField": "stateId",
                    "foreignField": "_id",
                    "as": "state"
                }
            }, {
                $lookup: {
                    "from": "Area",
                    "localField": "areaId",
                    "foreignField": "_id",
                    "as": "area"
                }
            }, {
                $lookup: {
                    "from": "District",
                    "localField": "districtId",
                    "foreignField": "_id",
                    "as": "district"
                }
            }, {
                $unwind: {
                    "path": "$provider",
                    "preserveNullAndEmptyArrays": false
                }
            }, {
                $project: {
                    stateId: 1,
                    districtId: 1,
                    areaId: 1,
                    areaStatus: 1,
                    stateName: {
                        $arrayElemAt: ["$state.stateName", 0]
                    },
                    districtName: {
                        $arrayElemAt: ["$district.districtName", 0]
                    },
                    name: "$users.name",
                    divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                    providerType: "$provider.providerType",
                    providerName: "$provider.providerName",
                    providerId: "$provider._id",
                    providerCode: {
                        $cond: [{
                            $eq: ["$provider.providerCode", "null"]
                        }, '', "$provider.providerCode"]
                    },
                    degree: "$provider.degree",
                    specialization: "$provider.specialization",
                    category: "$provider.category",
                    address: "$provider.address",
                    phone: "$provider.phone",
                    frequencyVisit: "$provider.frequencyVisit",
                    dob: {
                        $cond: [{
                            $eq: ["$provider.dob", "1900-01-01"]
                        }, '', "$provider.dob"]
                    },
                    doa: {
                        $cond: [{
                            $eq: ["$provider.doa", "1900-01-01"]
                        }, '', "$provider.doa"]
                    },
                    providerStatus: "$provider.status",
                    providerAppStatus: "$provider.appStatus",
                    providerDelStatus: "$provider.delStatus",
                    areaName: {
                        $arrayElemAt: ["$area.areaName", 0]
                    }
                }
            }, {
                $match: providerMatch
            }, {
                $sort: {
                    divisionName: 1,
                    providerType: 1,
                    stateName: 1,
                    districtName: 1,
                    name: 1,
                    areaName: 1,
                    providerName: 1
                }
            }, {
                cursor: {
                    batchSize: 50
                },
                allowDiskUse: true
            }, function (err, result) {
                return cb(false, result);
            })
        });
    }
    Userareamapping.remoteMethod(
        'getProviderList', {
        description: 'Getting Provider list with users',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'get'
        }
    }
    )
    //END
    //ravi on 30-01-2019
    Userareamapping.getAreaRequestsEmployeewise = function (param, cb) {
        var self = this;
        var userCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        let aMatch = {};
        if (param.designationLevel == 0) {
            if (param.requsetType === 'addition') {
                if (param.approvalUpto == 1) {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        status: false,
                        delStatus: '',
                        appStatus: { $in: ["pending"] }
                    }
                } else if (param.approvalUpto == 2) {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        status: false,
                        delStatus: '',
                        appStatus: { $in: ["InProcess"] }
                    }
                }
            } else if (param.requsetType === 'deletion') {
                if (param.approvalUpto == 1) {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        delStatus: { $in: ["pending"] },
                        status: true
                    }
                } else if (param.approvalUpto == 2) {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        delStatus: { $in: ["InProcess"] },
                        status: true
                    }
                }
            }
            userCollection.aggregate(// Stage 1
                {
                    $match: aMatch
                },// Stage 2
                {
                    $lookup: {
                        "from": "UserLogin",
                        "localField": "userId",
                        "foreignField": "_id",
                        "as": "User"
                    }
                },
                // Stage 3
                {
                    $lookup: {
                        "from": "State",
                        "localField": "stateId",
                        "foreignField": "_id",
                        "as": "states"
                    }
                },
                // Stage 4
                {
                    $lookup: {
                        "from": "District",
                        "localField": "districtId",
                        "foreignField": "_id",
                        "as": "District"
                    }
                },
                // Stage 5
                {
                    $project: {
                        _id: 0,
                        userId: "$userId",
                        userName: {
                            $arrayElemAt: ["$User.name", 0]
                        },
                        stateName: {
                            $arrayElemAt: ["$states.stateName", 0]
                        },
                        District: {
                            $arrayElemAt: ["$District.districtName", 0]
                        }
                    }
                },
                // Stage 6
                {
                    $group: {
                        _id: {
                            userId: "$userId",
                            userName: "$userName",
                            stateName: "$stateName",
                            District: "$District"
                        },
                        total: { $sum: 1 }
                    }
                }, function (err, userAreaCount) {
                    console.log(userAreaCount);
                    if (err) {
                        console.log(err);
                        return cb(err);
                    }
                    if (!userAreaCount) {
                        var err = new Error('no records found');
                        err.statusCode = 404;
                        err.code = 'NOT FOUND';
                        return cb(err);
                    }
                    return cb(false, userAreaCount);
                });
        } else if (param.designationLevel > 1) {
            var param1 = {
                supervisorId: param.supervisorId,
                companyId: ObjectId(param.companyId),
                status: true
            };
            Userareamapping.app.models.Hierarchy.getManagerHierarchyInArray(param1, function (err, res) {
                var userIds = [];
                for (var n = 0; n < res[0].userId.length; n++) {
                    userIds.push(ObjectId(res[0].userId[n]))
                }
                if (param.requsetType == 'addition') {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        status: false,
                        delStatus: '',
                        appStatus: { $in: ["pending"] },
                        userId: { $in: userIds }
                    }
                } else if (param.requsetType == 'deletion') {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        delStatus: { $in: ["pending"] },
                        userId: { $in: userIds },
                        status: true
                    }
                }
                userCollection.aggregate(
                    // Stage 1
                    {
                        $match: aMatch
                    },// Stage 2
                    {
                        $lookup: {
                            "from": "UserLogin",
                            "localField": "userId",
                            "foreignField": "_id",
                            "as": "User"
                        }
                    },
                    // Stage 3
                    {
                        $lookup: {
                            "from": "State",
                            "localField": "stateId",
                            "foreignField": "_id",
                            "as": "states"
                        }
                    },
                    // Stage 4
                    {
                        $lookup: {
                            "from": "District",
                            "localField": "districtId",
                            "foreignField": "_id",
                            "as": "District"
                        }
                    },
                    // Stage 5
                    {
                        $project: {
                            _id: 0,
                            userId: "$userId",
                            userName: {
                                $arrayElemAt: ["$User.name", 0]
                            },
                            stateName: {
                                $arrayElemAt: ["$states.stateName", 0]
                            },
                            District: {
                                $arrayElemAt: ["$District.districtName", 0]
                            }
                        }
                    },
                    // Stage 6
                    {
                        $group: {
                            _id: {
                                userId: "$userId",
                                userName: "$userName",
                                stateName: "$stateName",
                                District: "$District"
                            },
                            total: { $sum: 1 }
                        }
                    },
                    function (err, result) {
                        if (err) {
                            return cb(err);
                        }
                        return cb(false, result);
                    }
                );
            });
        }
    }
    Userareamapping.remoteMethod(
        'getAreaRequestsEmployeewise', {
        description: 'This method is used to get the all area count of the paticular users',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
    //END	
    //----------------getting provider list- with tagged provider--------------------//
    Userareamapping.getProviderLocationTagged = function (params, cb) {
        var self = this;
        let users = [];
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        var providerCollection=this.getDataSource().connector.collection(Userareamapping.app.models.Providers.modelName);
        let match = {}
        if (params.designation > 1) {
            Userareamapping.app.models.Hierarchy.find({
                where: {
                    companyId: ObjectId(params.companyId),
                    supervisorId: {
                        inq: params.userId //Supervisior Ids
                    },
                    status: true
                }
            }, function (err, hrcyResult) {
                for (let i = 0; i < hrcyResult.length; i++) {
                    userIds.push(hrcyResult[i].userId);
                }
                //-------Preetis add manager self id---------------
                for (let i = 0; i < params.userId.length; i++) {
                    userIds.push(params.userId[i]);// add manger userid also ...
                }
                match = {
                    companyId: ObjectId(params.companyId),
                    userId: {
                        $in: userIds
                    },
                    status: true,
                    appStatus: "approved",
                    geoLocation:{$exists:true}
                }
            });
        } else {
            users.push(ObjectId(params.userId));
            match = {
                companyId: ObjectId(params.companyId),
                userId: {
                    $in: users
                },
                status: true,
                appStatus: "approved",
                geoLocation:{$exists:true}
            }
        }
        //-----------get provider list---------------------
        providerCollection.aggregate(
          // Stage 1
          {
            $match: match
          },
          // Stage 2
          {
            $lookup: {
              from: "UserInfo",
              localField: "userId",
              foreignField: "userId",
              as: "users"
            }
          },
          // Stage 3
          {
            $lookup: {
              from: "State",
              localField: "stateId",
              foreignField: "_id",
              as: "state"
            }
          },
          // Stage 4
          {
            $lookup: {
              from: "Area",
              localField: "blockId",
              foreignField: "_id",
              as: "area"
            }
          },
          // Stage 5
          {
            $lookup: {
              from: "District",
              localField: "districtId",
              foreignField: "_id",
              as: "district"
            }
          },
          // Stage 6
          {
            $project: {
              stateName: {
                $arrayElemAt: ["$state.stateName", 0]
              },
              districtName: {
                $arrayElemAt: ["$district.districtName", 0]
              },
              name: {
                $arrayElemAt: ["$users.name", 0]
              },
              providerType: 1,
              providerName: 1,
              companyId: 1,
              userId: 1,
              providerId: "$_id",
              providerCode: {
                $cond: [
                  {
                    $eq: ["$providerCode", "null"]
                  },
                  "",
                  "$providerCode"
                ]
              },
              degree: 1,
              specialization: 1,
              category: 1,
              address: 1,
              phone: 1,
              frequencyVisit: 1,
              providerStatus: 1,
              providerAppStatus: "$appStatus",
              providerDelStatus: "$delStatus",
              geoLocation: "$geoLocation",
              areaName: {
                $arrayElemAt: ["$area.areaName", 0]
              },
              dcrDetails: 1
            }
          },
          function(err, result) {
            if (err) {
              return cb(err, null);
            }
            let finalResult = [];
            if (result.length > 0) {
              result.forEach(element => {
                Userareamapping.app.models.DCRProviderVisitDetails.findOne(
                  {
                    where: {
                      providerId: element.providerId,
                      companyId: element.companyId
                    },
                    fields: { id: true, dcrDate: true },
                    order: "dcrDate DESC",
                    limit: 1
                  },
                  (err, dcr) => {
                    if (dcr) {
                      finalResult.push({
                        ...element,
                        dcrDate: dcr.dcrDate ? dcr.dcrDate : ""
                      });
                    } else {
                      finalResult.push({
                        ...element,
                        dcrDate: ""
                      });
                    }
                    if (finalResult.length === result.length) {
                      return cb(false, finalResult);
                    }
                  }
                );
              });
            } else {
              return cb(false, result);
            }
          }
        );
        //---------------------------------------------------
    }
    Userareamapping.remoteMethod(
        'getProviderLocationTagged', {
        description: 'This method is used to get the all providers that have tagged/untagged location',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
    //--------------------------------
    //----Preeti Arora----replica of get providers as rahul has changed the existing method, so transfer module is not working with its original-----
    // Praveen Singh getting Provider list with user name 09-01-2019
    Userareamapping.getProviderListReplica = function (params, cb) {
        var self = this;
        var UserareamappingCollection = this.getDataSource().connector.collection(Userareamapping.modelName);
        let match = {};
        let stateIds = [];
        let districtIds = []
        let areaIds = [];
        let users = [];
        let userIds = [];
        console.log("params=>",params);
        
        async.parallel(
          {
            match: function(cb) {
              if (
                params.type === "State" ||
                params.type === "Headquarter" ||
                params.type === "Area"
              ) {
                for (let i = 0; i < params.stateId.length; i++) {
                  stateIds.push(ObjectId(params.stateId[i]));
                }
                match = {
                  companyId: ObjectId(params.companyId),
                  stateId: {
                    $in: stateIds
                  },
                  status: true,
                  appStatus: "approved"
                };
                if (params.type === "Headquarter" || params.type === "Area") {
                  for (let i = 0; i < params.districtId.length; i++) {
                    districtIds.push(ObjectId(params.districtId[i]));
                  }
                  match = {
                    companyId: ObjectId(params.companyId),
                    stateId: {
                      $in: stateIds
                    },
                    districtId: {
                      $in: districtIds
                    },
                    status: true,
                    appStatus: "approved"
                  };
                  if (params.type === "Area") {
                    for (let i = 0; i < params.areaId.length; i++) {
                      areaIds.push(ObjectId(params.areaId[i]));
                    }
                    match = {
                      companyId: ObjectId(params.companyId),
                      stateId: {
                        $in: stateIds
                      },
                      districtId: {
                        $in: districtIds
                      },
                      areaId: {
                        $in: areaIds
                      },
                      status: true,
                      appStatus: "approved"
                    };
                  }
                }
                cb(false, match);
              } else if (params.type === "Employee Wise") {
                if (params.designation > 1) {
                  Userareamapping.app.models.Hierarchy.find(
                    {
                      where: {
                        companyId: ObjectId(params.companyId),
                        supervisorId: {
                          inq: params.userId //Supervisior Ids
                        },
                        status: true
                      }
                    },
                    function(err, hrcyResult) {
                      for (let i = 0; i < hrcyResult.length; i++) {
                        userIds.push(hrcyResult[i].userId);
                      }
                      //-------Preetis add manager self id---------------
                      for (let i = 0; i < params.userId.length; i++) {
                        userIds.push(params.userId[i]); // add manger userid also ...
                      }
                      match = {
                        companyId: ObjectId(params.companyId),
                        userId: {
                          $in: userIds
                        },
                        status: true,
                        appStatus: "approved"
                      };
                      cb(false, match);
                    }
                  );
                } else {
                  for (let i = 0; i < params.userId.length; i++) {
                    users.push(ObjectId(params.userId[i]));
                  }
                  match = {
                    companyId: ObjectId(params.companyId),
                    userId: {
                      $in: users
                    },
                    status: true,
                    appStatus: "approved"
                  };
                  cb(false, match);
                }
              }
            }
          },
          function(err, result) {
            //------Praveen Kumar(18-04-2019)----------------
            let providerMatch = {};
            if (params.status[0] == true) {
              providerMatch = {
                providerType: {
                  $in: params.provoderType
                },
                providerStatus: true,
                providerDelStatus: "",
                providerAppStatus: "approved"
                //category: { $in: params.category }
              };
            } else if (params.status[0] == false) {
              providerMatch = {
                providerType: {
                  $in: params.provoderType
                },
                providerStatus: false,
                providerDelStatus: "approved"
                //category: { $in: params.category }
              };
            }
            if (params.category.length > 0) {
              providerMatch.category = { $in: params.category };
            }
            console.log("providerMatch==>",providerMatch);
            
            //------END Praveen Kumar(18-04-2019)----------------
            UserareamappingCollection.aggregate(
              {
                $match: result.match
              },
              {
                $lookup: {
                  from: "UserInfo",
                  localField: "userId",
                  foreignField: "userId",
                  as: "users"
                }
              },
              {
                $unwind: "$users"
              },
              {
                $match: {
                  "users.status": true
                }
              },
              {
                $unwind: {
                  path: "$users.divisionId",
                  preserveNullAndEmptyArrays: true
                }
              },
              {
                $lookup: {
                  from: "DivisionMaster",
                  localField: "users.divisionId",
                  foreignField: "_id",
                  as: "division"
                }
              },
              {
                $lookup: {
                  from: "Providers",
                  localField: "areaId",
                  foreignField: "blockId",
                  as: "provider"
                }
              },
              {
                $lookup: {
                  from: "State",
                  localField: "stateId",
                  foreignField: "_id",
                  as: "state"
                }
              },
              {
                $lookup: {
                  from: "Area",
                  localField: "areaId",
                  foreignField: "_id",
                  as: "area"
                }
              },
              {
                $lookup: {
                  from: "District",
                  localField: "districtId",
                  foreignField: "_id",
                  as: "district"
                }
              },
              {
                $unwind: {
                  path: "$provider",
                  preserveNullAndEmptyArrays: false
                }
              },
              {
                $project: {
                  stateId: 1,
                  districtId: 1,
                  areaId: 1,
                  areaStatus: 1,
                  stateName: {
                    $arrayElemAt: ["$state.stateName", 0]
                  },
                  districtName: {
                    $arrayElemAt: ["$district.districtName", 0]
                  },
                  name: "$users.name",
                  divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                  providerType: "$provider.providerType",
                  providerName: "$provider.providerName",
                  providerId: "$provider._id",
                  socialMedia:"$provider.socialMediaTypes",
                  providerCode: {
                    $cond: [
                      {
                        $eq: ["$provider.providerCode", "null"]
                      },
                      "",
                      "$provider.providerCode"
                    ]
                  },
                  degree: "$provider.degree",
                  specialization: "$provider.specialization",
                  category: "$provider.category",
                  address: "$provider.address",
                  city: "$provider.city",
                  phone: "$provider.phone",
                  email: "$provider.email",
                  frequencyVisit: "$provider.frequencyVisit",
                  dob: {
                    $cond: [
                      {
                        $eq: ["$provider.dob", "1900-01-01"]
                      },
                      "",
                      "$provider.dob"
                    ]
                  },
                  doa: {
                    $cond: [
                      {
                        $eq: ["$provider.doa", "1900-01-01"]
                      },
                      "",
                      "$provider.doa"
                    ]
                  },
                  providerStatus: "$provider.status",
                  providerAppStatus: "$provider.appStatus",
                  providerDelStatus: "$provider.delStatus",
                  areaName: {
                    $arrayElemAt: ["$area.areaName", 0]
                  },
                  focusProduct: {
                    $cond: {
                      if: { $isArray: "$provider.focusProduct" },
                      then: {
                        $cond: {
                          if: { $gt: [{ $size: "$provider.focusProduct" }, 0] },
                          then: "$provider.focusProduct",
                          else: []
                        }
                      },
                      else: []
                    }
                  },
                  productAlreadyUsing: {
                    $cond: {
                      if: { $isArray: "$provider.productAlreadyUsing" },
                      then: {
                        $cond: {
                          if: {
                            $gt: [{ $size: "$provider.productAlreadyUsing" }, 0]
                          },
                          then: "$provider.productAlreadyUsing",
                          else: []
                        }
                      },
                      else: []
                    }
                  }
                }
              },
              {
                $match: providerMatch
              },
              {
                $sort: {
                  divisionName: 1,
                  providerType: 1,
                  stateName: 1,
                  districtName: 1,
                  name: 1,
                  areaName: 1,
                  providerName: 1
                }
              },
              {
                cursor: {
                  batchSize: 50
                },
                allowDiskUse: true
              },
              function(err, result) {
                  console.log("result=>>",result);
                  
                let finalResult = [];
                if (result.length == 0) {
                  return cb(false, []);
                } else {
                  result.forEach(element => {
                    async.series(
                      {
                        focusProduct: function(callback) {
                          if (element.hasOwnProperty("focusProduct") === true) {
                            if (element.focusProduct.length > 0) {
                              Userareamapping.app.models.Products.getFocusProducts(
                                {
                                  productIds: element.focusProduct
                                },
                                function(err, res) {
                                  if (res.length > 0) {
                                    callback(null, res[0].products);
                                  } else {
                                    callback(null, []);
                                  }
                                }
                              );
                            } else {
                              callback(null, []);
                            }
                          } else {
                            callback(null, []);
                          }
                        },
                        productAlreadyUsing: function(callback) {
                          if (
                            element.hasOwnProperty("productAlreadyUsing") ===
                            true
                          ) {
                            if (element.productAlreadyUsing.length > 0) {
                              Userareamapping.app.models.Products.getFocusProducts(
                                {
                                  productIds: element.productAlreadyUsing
                                },
                                function(err, res) {
                                  if (res.length > 0) {
                                    callback(null, res[0].products);
                                  } else {
                                    callback(null, []);
                                  }
                                }
                              );
                            } else {
                              callback(null, []);
                            }
                          } else {
                            callback(null, []);
                          }
                        }
                      },
                      function(err, results) {
                        finalResult.push({
                          ...element,
                          focus: results.focusProduct.toString(),
                          productAlready: results.productAlreadyUsing.toString()
                        });
                        if (result.length === finalResult.length) {
                          return cb(false, finalResult);
                        }
                      }
                    );
                  });
                }
              }
            );
          }
        );
    }
    Userareamapping.remoteMethod(
        'getProviderListReplica', {
        description: 'Getting Provider list with users',
        accepts: [{
            arg: 'params',
            type: 'object',
            http: {source: 'body'}
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    )
    //-------------------------------------------------------
};
