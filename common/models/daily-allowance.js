'use strict';

module.exports = function(Dailyallowance) {

//------updated by Rahul Choudhary-given by---Rahul Saini (14-04-2020) start -------------
  
  Dailyallowance.editDA = function (params, cb) {
    console.log("param vaalue",params);
    var self = this;
    let DABackup=[];  

      let where={
        _id: params.id,
        companyId: params.companyId
      }
      Dailyallowance.find({
        where: where
      }  , function (err, res) {

        if(err){
          sendMail.sendMail({ collectionName: 'Dailyallowance', errorObject: err, paramsObject: params, methodName: 'editDA' });
        }
        console.log("response",res);
          if (res.length > 0) {
            if (res[0].hasOwnProperty('DABackup')) {
              for (var i = 0; i < res[0].DABackup.length; i++) {
                DABackup.push(res[0].DABackup[i])
              }
              DABackup.push({
                "daHQ" : res[0].daHQ,
                "daEX" : res[0].daEX, 
                "daOUT" : res[0].daOUT, 
                "mobileAllowance" : res[0].mobileAllowance, 
                "netAllowance" : res[0].netAllowance, 
                "specialAllowance" : res[0].specialAllowance, 
                 updatedAt: new Date()
              })

            }
            else {
              DABackup.push({
                "daHQ" : res[0].daHQ,
                "daEX" : res[0].daEX, 
                "daOUT" : res[0].daOUT, 
                "mobileAllowance" : res[0].mobileAllowance, 
                "netAllowance" : res[0].netAllowance, 
                "specialAllowance" : res[0].specialAllowance, 
                 updatedAt: new Date()
             })
            }

          } else {
            return cb(false, [])
          }
          


          let where = {
             _id: params.id,
             companyId:  params.companyId
           }
           let updateObj = {
            "daHQ" : params.daHQ,
            "daEX" : params.daEX, 
            "daOUT" : params.daOUT, 
            "mobileAllowance" : params.mobileAllowance, 
            "netAllowance" : params.netAllowance, 
            "specialAllowance" : params.specialAllowance, 
             updatedAt: new Date(),
             DABackup: DABackup
           }
           console.log("where Data ",where);
           console.log("updateObjDADA",updateObj);
           Dailyallowance.update(where, updateObj, function (err, res) {
            return cb(false,res)
})

    })
}

Dailyallowance.remoteMethod(
  'editDA', {
  description: 'get Distance between two areas in KM',
  accepts: [{
    arg: 'params',
    type: 'object'
  }],
  returns: {
    root: true,
    type: 'array'
  },
  http: {
    verb: 'post'
  }

}
);
  //-------------------------Rahul Saini (14-04-2020) end -------------
  //------------------------end--------------------------------
};
