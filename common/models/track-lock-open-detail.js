'use strict';
var sendMail = require('../../utils/sendMail');
module.exports = function(Tracklockopendetail) {
  
    Tracklockopendetail.submitLockOpenDetails = function (params, cb) {
    var self = this;
    // console.log("params=",params)
    var TrackLockOpenDetailCollection = this.getDataSource().connector.collection(Tracklockopendetail.modelName);
           
   Tracklockopendetail.app.models.DCRMaster.getLockedDCR(params,function(err, result) {
     
	 if(result.length>0){
         TrackLockOpenDetailCollection.insert(result, function (err, res){
           if(err){
            sendMail.sendMail({ collectionName: 'Tracklockopendetail', errorObject: err, paramsObject: params, methodName: 'submitLockOpenDetails' });
           }
          // console.log("result=",res);
		  return cb(err,res)
         })
      }  
      
    })
     
  }
  Tracklockopendetail.remoteMethod(
    'submitLockOpenDetails', {
      description: 'Track Lock Open Details',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'object'
      },
      http: {
        verb: 'post'
      }
    }
  )
//-----------------------------------------------------------------//
Tracklockopendetail.getLockingDetail = function (params, cb) {
  var self = this;
  var TrackLockOpenDetailCollection = this.getDataSource().connector.collection(Tracklockopendetail.modelName);
  // console.log(params)
   
}
Tracklockopendetail.remoteMethod(
  'getLockingDetail', {
    description: 'Track Lock Open Details',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  }
)
//-----------------------------------------------------------------//
};
