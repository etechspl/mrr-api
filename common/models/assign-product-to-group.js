'use strict';
var ObjectId = require("mongodb").ObjectID;
var sendMail = require("../../utils/sendMail");

module.exports = function(Assignproducttogroup) {





  
  /*Assignproducttogroup.createAssignProductGroups = function (params, cb) {
let selectedStates = [];
let selectedProducts = [];
for (var i = 0; i < params.stateInfo.length; i++) {
  selectedStates.push(ObjectId(params.stateInfo[i]));
}

for (var i = 0; i < params.productInfo.length; i++) {
  selectedProducts.push(ObjectId(params.productInfo[i].id));
}

Assignproducttogroup.find(
  {
    where: {
      companyId: ObjectId(params.companyId),
      productGroupName: params.productGroupName,
    },
  }, function (err, response) {
    if (err) {
      sendMail.sendMail({
        collectionName: "Productgroups",
        errorObject: err,
        paramsObject: params,
        methodName: "createProductGroups",
      });
    }
    if (response.length > 0) {
      var err = new Error("Group Name is Already Exists");
      console.log("err ; ",err);
      err.statusCode = 409;
      err.code = "Validation failed";
      return cb(err);
    } else {
      let finalObj = [];
                params.stateInfo.forEach(state => {
                  let prodetails = [];
                  let GroupObj={};
                    params.productInfo.forEach(products => {
                        prodetails.push(ObjectId(products.id));
                    });
                    GroupObj = {
                        companyId: ObjectId(params.companyId),
                        productGroupName:params.productGroupName,
                        stateId :ObjectId(state),
                        productInfo : prodetails,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    }
                    finalObj.push(GroupObj);
               
                });
Assignproducttogroup.create(finalObj,function (err, res) {
  if (err) {
    sendMail.sendMail({
      collectionName: "Assignproducttogroup",
      errorObject: err,
      paramsObject: params,
      methodName: "assignProduct",
    });
    //console.log(err);
    return cb(err);
  }
  return cb(false, res);
}
);
    }
  }
);


  }*/
  Assignproducttogroup.createAssignProductGroups = function (params, cb) {
    console.log("params:  ",params);
let selectedStates = [];
let selectedProducts = [];
let selectedDivision = []; 
 

for (var i = 0; i < params.stateInfo.length; i++) {
  selectedStates.push(ObjectId(params.stateInfo[i]));
}

for (var i = 0; i < params.productInfo.length; i++) {
  selectedProducts.push(ObjectId(params.productInfo[i].id));
}

let match={}
  if(params.divisionId!=null){
    for (var i = 0; i < params.divisionId.length; i++) {
      selectedDivision.push(ObjectId(params.divisionId[i]));
    }
   match={
    companyId: ObjectId(params.companyId),
      productGroupName: params.productGroupName,
      divisionId:selectedDivision
    
  }}
  else{
    match={
      companyId: ObjectId(params.companyId),
      productGroupName: params.productGroupName,
    } 
  }  
   
Assignproducttogroup.find(
  {
    where: match
  }, function (err, response) {
    if (err) {
      sendMail.sendMail({
        collectionName: "Productgroups",
        errorObject: err,
        paramsObject: params,
        methodName: "createProductGroups",
      });
    }
    if (response.length > 0) {
      var err = new Error("Group Name is Already Exists");
      console.log("err ; ",err);
      err.statusCode = 409;
      err.code = "Validation failed";
      return cb(err);
    } else {
      let finalObj = [];
      if(params.divisionId!=null){
      params.divisionId.forEach(div => {
                params.stateInfo.forEach(state => {
                  let prodetails = [];
                  let GroupObj={};
                    params.productInfo.forEach(products => {
                        prodetails.push(ObjectId(products.id));
                    });
                    GroupObj = {
                        companyId: ObjectId(params.companyId),
                        productGroupName:params.productGroupName,
                        stateId :ObjectId(state),
                        productInfo : prodetails,
                        divisionId : ObjectId(div), 
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    }
                    finalObj.push(GroupObj);
               
                });

              });
            }else{
              params.stateInfo.forEach(state => {
                let prodetails = [];
                let GroupObj={};
                  params.productInfo.forEach(products => {
                      prodetails.push(ObjectId(products.id));
                  });
                  GroupObj = {
                      companyId: ObjectId(params.companyId),
                      productGroupName:params.productGroupName,
                      stateId :ObjectId(state),
                      productInfo : prodetails, 
                      createdAt: new Date(),
                      updatedAt: new Date(),
                  }
                  finalObj.push(GroupObj);
             
              });
            }
            console.log("finalObj : ",finalObj);
Assignproducttogroup.create(finalObj,function (err, res) {
  if (err) {
    sendMail.sendMail({
      collectionName: "Assignproducttogroup",
      errorObject: err,
      paramsObject: params,
      methodName: "assignProduct",
    }); 
    //console.log(err);
    return cb(err);
  }
  return cb(false, res);
}
); 
    }
  }
);


  }
  Assignproducttogroup.remoteMethod("createAssignProductGroups", {
    description: "Create Assign Product To Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });


    Assignproducttogroup.assignProduct = function (params, cb) {
      
        var self = this;
        var groupsCollection = this.getDataSource().connector.collection(
            Assignproducttogroup.modelName
        ); 
        Assignproducttogroup.find({
            where: {
              companyId: ObjectId(params.companyId),
              groupId:ObjectId(params.id),
              stateId:ObjectId(params.stateId)
            },
          },function (err, response) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Assignproducttogroup",
                errorObject: err,
                paramsObject: params,
                methodName: "assignProduct",
              });
            }
            if (response.length > 0) {
                var err = new Error("Group is Already Assigned");
                err.statusCode = 409;
                err.code = "Validation failed";
                return cb(err);
              } else {
               
                let finalObj = [];
                params.stateInfo.forEach(state => {
                  let prodetails = [];
                  let GroupObj={};
                    params.productInfo.forEach(products => {
                        let prodDetails={
                            productId: ObjectId(products.id),
                            prodName: products.productName,
                            ptw: products.ptw,
                            ptr: products.ptr,
                            mrp: products.mrp,  
                        }
                        prodetails.push(prodDetails);
                    });
                    GroupObj = {
                        companyId: ObjectId(params.companyId),
                        groupId:ObjectId(params.id),
                        stateId :ObjectId(state),
                        productInfo : prodetails,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    }
                    finalObj.push(GroupObj);
               
                });
                Assignproducttogroup.create(finalObj,function (err, res) {
                    if (err) {
                      sendMail.sendMail({
                        collectionName: "Assignproducttogroup",
                        errorObject: err,
                        paramsObject: params,
                        methodName: "assignProduct",
                      });
                      //console.log(err);
                      return cb(err);
                    }
                    return cb(false, res);
                  }
                );

              }
        })
      
      };
      Assignproducttogroup.remoteMethod("assignProduct", {
        description: "Assign Product To Group",
        accepts: [
          {
            arg: "params",
            type: "object",
          },
        ],
        returns: {
          root: true,
          type: "object",
        },
        http: {
          verb: "post",
        },
      });


      Assignproducttogroup.getProductGroupBasedOnStates = function (params, cb) {
        var self = this;
             var viewProductGroupCollection = this.getDataSource().connector.collection(Assignproducttogroup.modelName);
            let selectedStates = [];

            for (var i = 0; i < params.stateIds.length; i++) {
              selectedStates.push(ObjectId(params.stateIds[i]));
            }
            viewProductGroupCollection.aggregate(
        // Stage 1
    {
      $match: {
      companyId: ObjectId(params.companyId),
      stateId:{ $in: selectedStates }
      }
    },

    // Stage 2
    {
      $group: {
      _id: {//stateId : "$stateId",
      groupId : "$groupId",
      }
      }
    },

    // Stage 3
    {
      $lookup: {
          "from" : "ProductGroups",
          "localField" : "_id.groupId",
          "foreignField" : "_id",
          "as" : "groupDetails"
      }
    },

    // Stage 4
    {
      $project: {
      groupId : { $arrayElemAt: ["$groupDetails._id", 0] },
      groupName : { $arrayElemAt: ["$groupDetails.proGrpName", 0] },
      }
    },
  // Options
  {
    cursor: {
      batchSize: 50
    },

    allowDiskUse: true
  },function(err,result){
    if(err){
      return cb(err)
    }
    return cb(false, result)
  });
          }
          
          Assignproducttogroup.remoteMethod("getProductGroupBasedOnStates", {
            description: "Get Product Groups Based on States",
            accepts: [
              {
                arg: "params",
                type: "object",
              },
            ],
            returns: {
              root: true,
              type: "object",
            },
            http: {
              verb: "get",
            },
          });


          /*Assignproducttogroup.getAssignedProductDetails = function (params, cb) {
            var self = this;
                 var viewProductGroupCollection = this.getDataSource().connector.collection(Assignproducttogroup.modelName);
                let selectedStates = [];
                let selectedGroups = [];
                // for (var i = 0; i < params.stateInfo.length; i++) {
                //   selectedStates.push(ObjectId(params.stateInfo[i]));
                // }
                // for (var i = 0; i < params.groupInfo.length; i++) {
                //   selectedGroups.push(ObjectId(params.groupInfo[i].groupId));
                // }

                let match={
                  companyId:ObjectId(params.companyId)
				  
                }
                if(params.hasOwnProperty("stateId")){
                  match.stateId=ObjectId(params.stateId)
                } 
				
				console.log("match",match);
				
                viewProductGroupCollection.aggregate(
            // Stage 1
        {
          $match: match
        },
    
        // Stage 2
        {
          $group: {
            _id :{
            stateId : "$stateId",
            productGroupName : "$productGroupName",
            id:"$_id",
            productInfo :"$productInfo" }
            }
        },
    
        // Stage 3
    {
      $lookup: {
        "from" : "State",
        "localField" : "_id.stateId",
        "foreignField" : "_id",
        "as" : "stateDetails"
      }
    },
// Stage 4
{
  $unwind: "$_id.productInfo"
},
    // Stage 5
    {
      $lookup: {
          "from" : "Products",
          "localField" : "_id.productInfo",
          "foreignField" : "_id",
          "as" : "products"
      }
    },

    // Stage 6
    {
      $project: {
        id:"$_id.id",
        stateId:{ $arrayElemAt: ["$stateDetails._id", 0] },
      stateName:{ $arrayElemAt: ["$stateDetails.stateName", 0] },
      productGroupName:"$_id.productGroupName",
           productId : { $arrayElemAt: ["$products._id", 0] },
            productName : { $arrayElemAt: ["$products.productName", 0] },
            productCode : { $arrayElemAt: ["$products.productCode", 0] },
          productType: { $arrayElemAt: ["$products.productType", 0] },
            ptw :  { $arrayElemAt: ["$products.ptw", 0] },
            ptr: { $arrayElemAt: ["$products.ptr", 0] },
            mrp: { $arrayElemAt: ["$products.mrp", 0] },
      }
    },

    // Stage 7
    {
      $group: {
        _id:{
      stateId:"$stateId",
      stateName:"$stateName",
      groupId:"$id",
      productGroupName:"$productGroupName"
      },
      products : { $addToSet:  { id : "$productId",productName :"$productName",productCode:"$productCode", productType : "$productType", ptw:"$ptw", ptr:"$ptr", mrp:"$mrp"}
      }
      }
    },

    // Stage 8
    {
      $project: {
       _id : 0,
       groupId:"$_id.groupId",
            companyId:"$_id.companyId",
            stateId: "$_id.stateId",
            stateName :"$_id.stateName",
            productGroupName : "$_id.productGroupName",
            ProductInfo : "$products"
      }
    },
      // Options
      {
        cursor: {
          batchSize: 50
        },
    
        allowDiskUse: true
      },function(err,result){
       
	   console.log("result++++++",result);
        if(err){
          return cb(err)
        }
        return cb(false, result)
      });
              }*/
			  
			  Assignproducttogroup.getAssignedProductDetails = function (params, cb) {
            var self = this;
                 var viewProductGroupCollection = this.getDataSource().connector.collection(Assignproducttogroup.modelName);
                let selectedStates = [];
                let selectedGroups = [];
                // for (var i = 0; i < params.stateInfo.length; i++) {
                //   selectedStates.push(ObjectId(params.stateInfo[i]));
                // }
                // for (var i = 0; i < params.groupInfo.length; i++) {
                //   selectedGroups.push(ObjectId(params.groupInfo[i].groupId));
                // }

                let match={
                  companyId:ObjectId(params.companyId)
                }
                if(params.hasOwnProperty("stateId")){
                  match.stateId=ObjectId(params.stateId)
                } 
                viewProductGroupCollection.aggregate(
            // Stage 1
        {
          $match: match
        },
    
        // Stage 2
        {
          $group: {
            _id :{
            stateId : "$stateId",
            productGroupName : "$productGroupName",
            id:"$_id",
            divisionId:"$divisionId",
            productInfo :"$productInfo" }
            }
        },
    
        // Stage 3
    {
      $lookup: {
        "from" : "State",
        "localField" : "_id.stateId",
        "foreignField" : "_id",
        "as" : "stateDetails"
      }
    },
// Stage 4
{
  $unwind: "$_id.productInfo"
},
    // Stage 5
    {
      $lookup: {
          "from" : "Products",
          "localField" : "_id.productInfo",
          "foreignField" : "_id",
          "as" : "products"
      }
    },
    // Stage 6
    {
      $lookup: {
          "from" : "DivisionMaster",
          "localField" : "_id.divisionId",
          "foreignField" : "_id",
          "as" : "divisions"
      }
    },

    // Stage 7
    {
      $project: {
        id:"$_id.id",
        stateId:{ $arrayElemAt: ["$stateDetails._id", 0] },
      stateName:{ $arrayElemAt: ["$stateDetails.stateName", 0] },
      divisionName:{ $arrayElemAt: ["$divisions.divisionName", 0] },
      divisionId:{ $arrayElemAt: ["$divisions._id", 0] },
      productGroupName:"$_id.productGroupName",
           productId : { $arrayElemAt: ["$products._id", 0] },
            productName : { $arrayElemAt: ["$products.productName", 0] },
            productCode : { $arrayElemAt: ["$products.productCode", 0] },
            productType: { $arrayElemAt: ["$products.productType", 0] },
            ptw :  { $arrayElemAt: ["$products.ptw", 0] },
            ptr: { $arrayElemAt: ["$products.ptr", 0] },
            mrp: { $arrayElemAt: ["$products.mrp", 0] },
      }
    },

    // Stage 7
    {
      $group: {
        _id:{
      stateId:"$stateId",
      stateName:"$stateName",
      divisionName:"$divisionName",
      divisionId:"$divisionId",
      groupId:"$id",
      productGroupName:"$productGroupName"
      },
      products : { $addToSet:  { id : "$productId",productName :"$productName",productCode:"$productCode", productType : "$productType", ptw:"$ptw", ptr:"$ptr", mrp:"$mrp"}
      }
      }
    },

    // Stage 8
    {
      $project: {
       _id : 0,
       groupId:"$_id.groupId",
            companyId:"$_id.companyId",
            stateId: "$_id.stateId",
            stateName :"$_id.stateName",
            divisionName:"$_id.divisionName",
            divisionId:"$_id.divisionId",
            productGroupName : "$_id.productGroupName",
            ProductInfo : "$products"
      }
    },
      // Options
      {
        cursor: {
          batchSize: 50
        },
    
        allowDiskUse: true
      },function(err,result){
       
        if(err){
          return cb(err)
        }
        return cb(false, result)
      });
              }
              
              Assignproducttogroup.remoteMethod("getAssignedProductDetails", {
                description: "Get Product Groups Based on States",
                accepts: [
                  {
                    arg: "params",
                    type: "object",
                  },
                ],
                returns: {
                  root: true,
                  type: "object",
                },
                http: {
                  verb: "post",
                },
              });

              
              Assignproducttogroup.removeProductFromGroup = function (params, cb) {
      
                var self = this;
                var groupsCollection = this.getDataSource().connector.collection(
                    Assignproducttogroup.modelName
                ); 
                let findObj = {
                  where: {
                      companyId: ObjectId(params.companyId),
                      stateId: ObjectId(params.stateId),
                      productGroupName: params.productGroupName,
                  }
              }
              Assignproducttogroup.find(findObj, function (err, res) {
                if (err) {
                    console.log(err);
                }
                if (res.length > 0) {
if (res[0].productInfo != null) {
  res[0].productInfo.forEach((item, index) => {
if(item == params.productId){
  res[0].productInfo.splice(index,1);
let where ={
  companyId: ObjectId(params.companyId),
  stateId: ObjectId(params.stateId),
  productGroupName: params.productGroupName,
}
let update ={
  productInfo:res[0].productInfo
}
groupsCollection.update(where, {
  $set: update
}, function (err, res) {
  if (err) return cb(false);
  return cb(false, "updated")
})

}
  });
}else {
  return cb(false, [11])
}
                }
              })
              };
              Assignproducttogroup.remoteMethod("removeProductFromGroup", {
                description: "Assign Product To Group",
                accepts: [
                  {
                    arg: "params",
                    type: "object",
                  },
                ],
                returns: {
                  root: true,
                  type: "object",
                },
                http: {
                  verb: "post",
                },
              });

Assignproducttogroup.deleteProductGroup = function (params, cb) {
    
    var self = this;
    var ProductGroupCollection = this.getDataSource().connector.collection(
      Assignproducttogroup.modelName
    );
    Assignproducttogroup.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          _id: ObjectId(params.groupId),
        },
      },
      function (err, response) {
        Assignproducttogroup.destroyById(params.groupId, function (err) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Assignproducttogroup",
              errorObject: err,
              paramsObject: params,
              methodName: "deleteProductGroup",
            });
            // console.log(err);
          }
          return cb(false, "deleted");
        });
      }
    );
  };

  Assignproducttogroup.remoteMethod("deleteProductGroup", {
    description: "Delete Product Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });


  Assignproducttogroup.editProductGroup = function (params, cb) {
    var self = this;
    var groupsCollection = this.getDataSource().connector.collection(
      Assignproducttogroup.modelName
    );

    let selectedProducts=[];
    let ExistingProducts=[];
    for (var i = 0; i < params.selecteddata.length; i++) {
      selectedProducts.push(ObjectId(params.selecteddata[i].id));
    }
for (var i = 0; i < params.data.ProductInfo.length; i++) {
  ExistingProducts.push(ObjectId(params.data.ProductInfo[i].id));
}
let allProduct=selectedProducts.concat(ExistingProducts);
let unique = allProduct.filter((item, i, ar) => ar.indexOf(item) === i);

Assignproducttogroup.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          stateId:  ObjectId(params.stateId),
          id : ObjectId(params.data.groupId),
        },
      },
      function (err, response) {
       
        if(response.length>0){
        let where = {
          companyId: ObjectId(params.companyId),
          stateId:  ObjectId(params.stateId),
          _id : ObjectId(params.data.groupId),
        }
        let update={
          productInfo : unique,
          updatedAt : new Date()
        }
        groupsCollection.update(
    where,
    {
      $set: update,
    },
    function (err, res) {
      
      if (err) return cb(false);
      return cb(false, "updated");
    }
  )
  }
      }); 
   
  };
  Assignproducttogroup.remoteMethod("editProductGroup", {
    description: "Edit Assign Product To Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });
  
  
};
