﻿"use strict";
var sendMail = require("../../utils/sendMail");
var ObjectId = require("mongodb").ObjectID;
var moment = require("moment");
var async = require('async');

var NodeGeocoder = require("node-geocoder");
module.exports = function(Dcrprovidervisitdetails) {
  Dcrprovidervisitdetails.observe("before save", function logQuery(ctx, next) {
    // if(ctx.where){
    //   let params = JSON.parse(JSON.stringify(ctx.data));
    //   Dcrprovidervisitdetails.app.models.SampleGiftIssueDetail.checkSampleIssueBalance(
    //     [params],
    //     function(err, response) {
    //       if (err) {
    //         sendMail.sendMail({
    //           collectionName: "Dcrprovidervisitdetails",
    //           errorObject: err,
    //           paramsObject: "before save",
    //           methodName: "observe"
    //         });
    //       }
    //       //  // console.log("result-",response);
    //     }
    //   );
    // }
    if (ctx.instance) {
      let params = JSON.parse(JSON.stringify(ctx.instance));
      Dcrprovidervisitdetails.app.models.SampleGiftIssueDetail.checkSampleIssueBalance(
        [params],
        function(err, response) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: "before save",
              methodName: "observe"
            });
          }
          //  // console.log("result-",response);
        }
      );
      next();
    } else {
      next();
    }
  });
  Dcrprovidervisitdetails.observe("after save", function logQuery(ctx, next) {
    if (ctx.instance) {
      if (!ctx.instance.geoLocation) {
        next();
      } else if (ctx.instance.geoLocation.length > 0) {
        var param = {};
        param = {
          lat: ctx.instance.geoLocation[0].lat,
          long: ctx.instance.geoLocation[0].long
        };
        Dcrprovidervisitdetails.app.models.longLatInfo.getLatLong(
          param,
          function(err, instances) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Dcrprovidervisitdetails",
                errorObject: err,
                paramsObject: "after save",
                methodName: "observe"
              });
            }
            if (instances != undefined && instances.length != 0) {
              next();
            } else {
              var options = {
                provider: "google",
                // Optional depending on the providers
                httpAdapter: "https", // Default
                apiKey: "AIzaSyCKCxPLVooXnPEFe17z4a65p-7DpJmsA2o", // for Mapquest, OpenCage, Google Premier
                formatter: null // 'gpx', 'string', ...
              };
              var geocoder = NodeGeocoder(options);

              geocoder.reverse(
                {
                  lat: parseFloat(ctx.instance.geoLocation[0].lat),
                  lon: parseFloat(ctx.instance.geoLocation[0].long)
                },
                function(err, res) {
                  if (err) {
                    sendMail.sendMail({
                      collectionName: "Dcrprovidervisitdetails",
                      errorObject: err,
                      paramsObject: "after save",
                      methodName: "observe"
                    });
                  }
                  if (
                    res != undefined &&
                    res.length > 0 &&
                    res[0].longitude != undefined
                  ) {
                    var longLatObj = {};
                    longLatObj = {
                      country: res[0].country,
                      city: res[0].city,
                      address: res[0].formattedAddress,
                      loc: {
                        type: "Point",
                        coordinates: [res[0].latitude, res[0].longitude]
                      },
                      updatedBy:"Geocording"
                    };
                    Dcrprovidervisitdetails.app.models.longLatInfo.create(
                      longLatObj,
                      function(result) {
                        let count=1;
                        const obj={
                          count:count,
                          date:new Date(),
                          companyId:ObjectId(ctx.instance.companyId),
                          submitBy:ObjectId(ctx.instance.submitBy),
                          createdAt:new Date()
                        }
                        
                        Dcrprovidervisitdetails.app.models.trackGeoInformation.create(
                          obj,
                          function(result) {
                          }
                        );

                        //// console.log("result");// console.log(result);
                        // next();
                      }
                    );
                  }
                }
              );
              next();
            }
            // next();
          }
        );
      } else {
        next();
      }
    } else {
      next();
    }
  });

  // change this callback api to async function by preeti arora 28-09-2020-----
  
  Dcrprovidervisitdetails.updateAllRecord = function(records, cb) {
    var self = this;
    var response = [];
    var ids = [];
    var ProviderVisitDetailCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
      Dcrprovidervisitdetails.app.models.CompanyMaster.find({where:{_id:records[0].companyId}},function(err,companyRes) {
        if(err){
          var err = new Error("records is undefined or empty");
          err.statusCode = 404;
          err.code = "RECORDS ARE NOT FOUND";
          return cb(err);
        }else{
          if(companyRes[0].validation.sampleIssue){
            if(companyRes[0].validation.sampleIssue==true){
              Dcrprovidervisitdetails.app.models.SampleGiftIssueDetail.checkSampleIssueBalance(
                records,
                function (err, response) {
                  if (records.length === 0 || records === "") {
                    var err = new Error("records is undefined or empty");
                    err.statusCode = 404;
                    err.code = "RECORDS ARE NOT FOUND";
                    return cb(err);
                  }
                }
              );
            }

          }
        }
      })
    if (records.length === 0 || records === "") {
      var err = new Error("records is undefined or empty");
      err.statusCode = 404;
      err.code = "RECORDS ARE NOT FOUND";
      return cb(err);
    }
    let returnData = records;
    for (let w = 0; w < records.length; w++) {
      let set = {};
      set = records[w];
      if(records[w].dcrId.includes("-")){
        //skip thiss portion
        // console.log(records[w].dcrId);
      }else{
        set["dcrId"] = ObjectId(set["dcrId"]);
        set["providerId"] = ObjectId(set["providerId"]);
        set["stateId"] = ObjectId(set["stateId"]);
        set["districtId"] = ObjectId(set["districtId"]);
        set["blockId"] = ObjectId(set["blockId"]);
        set["submitBy"] = ObjectId(set["submitBy"]);
        set["companyId"] = ObjectId(set["companyId"]);
        set["dcrDate"] = new Date(set["dcrDate"]);
  
        ProviderVisitDetailCollection.update({
          _id: ObjectId(records[w].id)
        }, {
          $set: set
        },
          function (err, result) {
  
            if (err) {
              // sendMail.sendMail({
              //   collectionName: "Dcrprovidervisitdetails",
              //   errorObject: err,
              //   paramsObject: records,
              //   methodName: "updateAllRecord",
              // });
    
            }
          }
        );
      }
    }
    //// console.log("after : ",returnData)
    return cb(false, returnData);
  };
  // End DCR Record
  // Update All Data for sync
  Dcrprovidervisitdetails.remoteMethod("updateAllRecord", {
    description: "update and send response all data",
    accepts: [{ arg: "records", type: "array", http: { source: "body" } }],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "post"
    }
  });


  // Dcrprovidervisitdetails.updateAllRecord = async function (records) {
  //   var self = this;
  //   var response = [];
  //   var ids = [];
  //   var ProviderVisitDetailCollection = self
  //     .getDataSource()
  //     .connector.collection(Dcrprovidervisitdetails.modelName);
  //     try{
  //       const  companyRes= await Dcrprovidervisitdetails.app.models.CompanyMaster.find({where:{_id:records[0].companyId}});
  //       if(companyRes[0].validation.sampleIssue){
  //         if(companyRes[0].validation.sampleIssue==true){
  //           Dcrprovidervisitdetails.app.models.SampleGiftIssueDetail.checkSampleIssueBalance(
  //             records,
  //             function (err, response) {
  //               if (response.length === 0 || response === "") {
  //                 var err = new Error("records is undefined or empty");
  //                 err.statusCode = 404;
  //                 err.code = "RECORDS ARE NOT FOUND";
  //                 return err;
  //               }
  //             }
  //           );
  //         }
  //       }
  //     }catch(ex){
  //        console.log(ex);
  //     }
   
  //   if (records.length === 0 || records === "") {
  //     var err = new Error("records is undefined or empty");
  //     err.statusCode = 404;
  //     err.code = "RECORDS ARE NOT FOUND";
  //     return cb(err);
  //   }
  //   let returnData = records;
  //   for (let w = 0; w < records.length; w++) {
  //     let set = {};
  //     set = records[w];
  //     set["dcrId"] = ObjectId(set["dcrId"]);
  //     set["providerId"] = ObjectId(set["providerId"]);
  //     set["stateId"] = ObjectId(set["stateId"]);
  //     set["districtId"] = ObjectId(set["districtId"]);
  //     set["blockId"] = ObjectId(set["blockId"]);
  //     set["submitBy"] = ObjectId(set["submitBy"]);
  //     set["companyId"] = ObjectId(set["companyId"]);
  //     set["dcrDate"] = new Date(set["dcrDate"]);
  //     ProviderVisitDetailCollection.update({
  //       _id: ObjectId(records[w].id)
  //     }, {
  //       $set: set
  //     },
  //       function (err, result) {
  //         if (err) {
  //           sendMail.sendMail({
  //             collectionName: "Dcrprovidervisitdetails",
  //             errorObject: err,
  //             paramsObject: records,
  //             methodName: "updateAllRecord",
  //           });
  
  //         }
  //       }
  //     );
  //   }
  //   return  returnData;
  // };
  // // End DCR Record
  // // Update All Data for sync
  // Dcrprovidervisitdetails.remoteMethod("updateAllRecord", {
  //   description: "update and send response all data",
  //   accepts: [{
  //     arg: "records",
  //     type: "array",
  //     http: {
  //       source: "body"
  //     }
  //   }],
  //   returns: {
  //     root: true,
  //     type: "array",
  //   },
  //   http: {
  //     verb: "post",
  //   },
  // });





  Dcrprovidervisitdetails.getDCHeirarchyDCRVisitData = function(userIdArr, cb) {
    var self = this;
    var ProviderVisitDetailCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    ProviderVisitDetailCollection.aggregate(
      {
        $match: {
          submitBy: { $in: userIdArr }
        }
      },
      // Stage 2
      {
        $project: {
          id: "$_id",
          _id: 0,
          dcrId: 1,
          dcrDate: 1,
          providerType: 1,
          visitLocation: 1,
          detailingDone: 1,
          productDetails: 1,
          purchasedQty: 1,
          createdAt: 1,
          updatedAt: 1,
          submitBy: 1,
          ipAddress: 1,
          geoLocation: 1,
          windowDisplay: 1,
          isOutsideTp: 1,
          stateId: 1,
          districtId: 1,
          blockId: 1,
          providerId: 1,
          remarks: 1
        }
      },
      function(err, DCHeirarchyDCR) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: userIdArr,
            methodName: "getDCHeirarchyDCRVisitData"
          });
          // console.log(err);
          return cb(err);
        }
        if (!DCHeirarchyDCR) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, DCHeirarchyDCR);
      }
    );
  };
  Dcrprovidervisitdetails.remoteMethod("getDCHeirarchyDCRVisitData", {
    description: "getDCHeirarchyDCRVisitData for selected details",
    accepts: [
      {
        arg: "userId",
        type: "array"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------
  Dcrprovidervisitdetails.getUserJointworkDetails = function(
    userId,
    fromDate,
    toDate,
    cb
  ) {
    var self = this;
    var ProviderVisitDetailCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    ProviderVisitDetailCollection.aggregate(
      // Stage 1
      {
        $match: {
          submitBy: ObjectId(userId),
          dcrDate: {
            //$gte: new Date(moment.utc(fromDate).format()),
            //$lte: new Date(moment.utc(toDate).add(1, 'days').format())
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
          }
        }
      },
      // Stage 2
      {
        $unwind: {
          path: "$jointWorkWithId",
          preserveNullAndEmptyArrays: true
        }
      },
      // Stage 3
      {
        $group: {
          _id: {
            jointWorkWith: "$jointWorkWithId"
          },
          totalCalls: {
            $addToSet: "$providerId"
          },
          dcrDate: {
            $addToSet: "$dcrDate"
          }
        }
      },
      // Stage 4
      {
        $match: {
          "_id.jointWorkWith": { $ne: null }
        }
      },
      function(err, result) {
        // console.log("result=",result)
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: [userId, fromDate, toDate],
            methodName: "getUserJointworkDetails"
          });
          // console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Dcrprovidervisitdetails.remoteMethod("getUserJointworkDetails", {
    description: "user joint wise wise DCR details",
    accepts: [
      {
        arg: "userId",
        type: "string"
      },
      {
        arg: "fromDate",
        type: "string"
      },
      {
        arg: "toDate",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------end
  
  
  
  // Provider Visit Detail report
  //-------Updated by Rahul Saini .
  Dcrprovidervisitdetails.getReminderProviderVisitDetails = function (params, cb) {
	  
    var self = this;
    var DcrprovidervisitdetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    var aMatch = {};
    let categoryMatch = {
      
      "providerDetail.status": {
        $in: [true, false],
      },
      "providerDetail.appStatus": "approved",
    };
    if (!params.hasOwnProperty("category")) {

      categoryMatch["providerDetail.category"] = {
        $in: params.category,
      };
    }
   
    let includeUnlistedProviderMatch = {};
    // if(params.hasOwnProperty("phoneOrder")){
    //   includeUnlistedProviderMatch["phoneOrder"]=params.phoneOrder
    // }
    
    

    if (params.unlistedInclude == true) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
		reminder:params.submitted
        //   "providerStatus": "unlisted"
      };
    } else if (params.unlistedInclude == false) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
		
        providerStatus: { $ne: "unlisted" },
		reminder:params.submitted
      };
    }
	console.log("params++++++",);
	
	
    params.providerType.forEach(element => {
      if(typeof(element)=="boolean"){
        includeUnlistedProviderMatch["phoneOrder"]=element
        params.providerType.pop(element)
      }
    });
    includeUnlistedProviderMatch["providerType"]={$in:params.providerType}
    
    if (params.type == "State" || params.type == "Headquarter") {
      Dcrprovidervisitdetails.app.models.UserInfo.getAllUserIdsBasedOnType(
        params,
        function (err, userRecords) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getProviderVisitDetail",
            });
          }
          var aProject = {};
          if (params.providerType == "RMP") {
            aProject = {
              _id: 0,
              providerName: "$providerDetail.providerName",
              providerCode: "$providerDetail.providerCode",
              category: "$providerDetail.category",
              address: "$providerDetail.address",
              phone: "$providerDetail.phone",
              status: "$providerDetail.status",
              providerType: "$providerDetail.providerType",
              frequencyVisit: "$providerDetail.frequencyVisit",
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
              blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
              totalVisit: { $size: "$visitDates" },
              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              userName: "$userDetails.name",
			  specialization:"$providerDetail.specialization",
              mefAnswers: 1,
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              givenProducts: "$productDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          } else {
            aProject = {
              _id: 0,
              providerName: "$providerDetail.providerName",
              providerCode: "$providerDetail.providerCode",
              category: "$providerDetail.category",
              address: "$providerDetail.address",
              phone: "$providerDetail.phone",
              status: "$providerDetail.status",
              providerType: "$providerDetail.providerType",
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
              blockName: { $arrayElemAt: ["$blockDetail.blockName", 0] },
              totalVisit: { $size: "$visitDates" },
		      	  specialization:"$providerDetail.specialization",
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              mefAnswers: 1,
              userName: "$userDetails.name",
              givenProducts: "$productDetails",
              giftDetails:"$giftDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          }
          if (userRecords.length > 0) {
            includeUnlistedProviderMatch.submitBy = {
              $in: userRecords[0].userIds,
            };
    
 
            DcrprovidervisitdetailsCollection.aggregate(
              // Stage 1
              {
                $match: includeUnlistedProviderMatch,
              },
              {
                $unwind: {
                  path: "$productDetails",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $group: {
                  _id: {
                    userId: "$submitBy",
                    stateId: "$stateId",
                    districtId: "$districtId",
                    providerId: "$providerId",
                  },
                  totalVisit: {
                    $addToSet: {
                      dcrdate: "$dcrDate",
                      submitBy: "$submitBy",
                      productDetails: "$productDetails",
                    },
                  },
                  visitDates: {
                    $addToSet: {
                      $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
                    },
                  },
                  productDetails: {
                    $addToSet: "$productDetails",
                  },
                  giftDetails: {
                    $addToSet: "$giftDetails",
                  },
                  mefAnswers: {
                    $addToSet: "$mefAnswers",
                  },
                  providerRemarks: {
                    $push: "$remarks",
                  },
                  productPob: {
                    $addToSet: "$productPob",
                  }
                },
              },
              {
                $lookup: {
                  from: "State",
                  localField: "_id.stateId",
                  foreignField: "_id",
                  as: "stateInfo",
                },
              },
              {
                $lookup: {
                  from: "District",
                  localField: "_id.districtId",
                  foreignField: "_id",
                  as: "district",
                },
              },
              {
                $lookup: {
                  from: "Providers",
                  localField: "_id.providerId",
                  foreignField: "_id",
                  as: "providerDetail",
                },
              },

              {
                $unwind: {
                  path: "$providerDetail",
                  preserveNullAndEmptyArrays: true,
                },
              },
             
              {
                $lookup: {
                  from: "Area",
                  localField: "providerDetail.blockId",
                  foreignField: "_id",
                  as: "blockDetail",
                },
              },
              {
                $lookup: {
                  from: "UserInfo",
                  localField: "_id.userId",
                  foreignField: "userId",
                  as: "userDetails",
                },
              },
              {
                $unwind: "$userDetails",
              },
              {
                $match: {
                  "userDetails.status": true,
                },
              },
              {
                $unwind: {
                  path: "$userDetails.divisionId",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "DivisionMaster",
                  localField: "userDetails.divisionId",
                  foreignField: "_id",
                  as: "division",
                },
              },
              {
                $project: aProject,
              },
              function (err, result) {

                
                if (err) {
                  sendMail.sendMail({
                    collectionName: "Dcrprovidervisitdetails",
                    errorObject: err,
                    paramsObject: params,
                    methodName: "getProviderVisitDetail",
                  });
                  // console.log(err);
                  return cb(err);
                }
                if (!result) {
                  var err = new Error("no records found");
                  err.statusCode = 404;
                  err.code = "NOT FOUND";
                  return cb(err);
                }
                if (result.length > 0) {
                  result.forEach(async (element, index) => {
                    if (element.hasOwnProperty("mefAnswers")) {
                      if (element.mefAnswers.length > 0) {
                        if (
                          element.mefAnswers[0].hasOwnProperty(
                            "productDiscussed"
                          ) ||
                          element.mefAnswers[0].productDiscussed != undefined ||
                          element.mefAnswers[0].productDiscussed != null
                        ) {
                          const where = {
                            _id: {
                              inq: element.mefAnswers[0].productDiscussed,
                            },
                            companyId: element.companyId,
                          };
                          const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                            {
                              where: where,
                            }
                          );
                          const givenProducts = [];
                          if (mefProducts.length > 0 && mefProducts != null) {
                            mefProducts.forEach((prod) => {
                              givenProducts.push({
                                productQuantity: 0,
                                availableStock: 0,
                                pob: 0,
                                productId: prod.id,
                                productName: prod.productName,
                                productPrice: 0,
                              });
                            });
                          }
                          element["givenProducts"] = givenProducts;
                        }
                      }
                    }
                    if (index == result.length - 1) {
                      cb(false, result);
                    }
                  });
                } else {
                  cb(false, []);
                }
              }
            );
          } else {
            cb(false, []);
          }
        }
      );
    } else {      
      var userIds = [];
      for (var i = 0; i < params.userIds.length; i++) {
        userIds.push(ObjectId(params.userIds[i]));
      }
      includeUnlistedProviderMatch.submitBy = {
        $in: userIds,
      };
      
      
      
      DcrprovidervisitdetailsCollection.aggregate(
        // Stage 1
        {
          $match: includeUnlistedProviderMatch,
        },
        {
          $unwind: {
            path: "$productDetails",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: {
              userId: "$submitBy",
              stateId: "$stateId",
              districtId: "$districtId",
              providerId: "$providerId",
            },
            totalVisit: {
              $sum: 1,
            },
            visitDates: {
              $addToSet: {
                $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
              },
            },
            productDetails: {
              $addToSet: "$productDetails",
            },
            giftDetails: {
              $addToSet: "$giftDetails",
            },
            mefAnswers: {
              $addToSet: "$mefAnswers",
            },
            providerRemarks: {
              $push: "$remarks",
            },
            productPob: {
              $addToSet: "$productPob",
            },
            dcrId: {
              $addToSet: "$dcrId",
            },
          },
        },
        // Stage 2
        {
          $lookup: {
            from: "State",
            localField: "_id.stateId",
            foreignField: "_id",
            as: "stateInfo",
          },
        },
        // Stage 3
        {
          $lookup: {
            from: "District",
            localField: "_id.districtId",
            foreignField: "_id",
            as: "district",
          },
        },
        // Stage 4
        {
          $lookup: {
            from: "Providers",
            localField: "_id.providerId",
            foreignField: "_id",
            as: "providerDetail",
          },
        },
        {
          $unwind: {
            path: "$providerDetail",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $match: categoryMatch,
        },
        {
          $lookup: {
            from: "Area",
            localField: "providerDetail.blockId",
            foreignField: "_id",
            as: "blockDetail",
          },
        },
        {
          $lookup: {
            from: "UserInfo",
            localField: "_id.userId",
            foreignField: "userId",
            as: "userDetails",
          },
        },
        {
          $unwind: "$userDetails",
        },
        {
          $match: {
            "userDetails.status": true,
          },
        },
        {
          $unwind: {
            path: "$userDetails.divisionId",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "DivisionMaster",
            localField: "userDetails.divisionId",
            foreignField: "_id",
            as: "division",
          },
        },
        {
          $project: {
            providerName: "$providerDetail.providerName",
            providerCode: "$providerDetail.providerCode",
            category: "$providerDetail.category",
            address: "$providerDetail.address",
            phone: "$providerDetail.phone",
            status: "$providerDetail.status",
            frequencyVisit: "$providerDetail.frequencyVisit",
            stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
            districtName: { $arrayElemAt: ["$district.districtName", 0] },
            blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
            totalVisit: { $size: "$visitDates" },
            visitDates: 1,
            mefAnswers: 1,
            productPob:{ $arrayElemAt: ["$productPob", 0] },
            dcrId: {
              $arrayElemAt: ["$dcrId", 0]
            },

            //userName: { $arrayElemAt: ["$userDetails.name", 0] },
            userName: "$userDetails.name",
            givenProducts: "$productDetails",
            giftDetails:"$giftDetails",
            providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
            divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
          },
        },
        function (err, result) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getProviderVisitDetail",
            });
            // console.log(err);
            return cb(err);
          }
          if (!result) {
            var err = new Error("no records found");
            err.statusCode = 404;
            err.code = "NOT FOUND";
            return cb(err);
          }
          if (result.length > 0) {            
            result.forEach(async (element, index) => {
              if (element.hasOwnProperty("mefAnswers")) {
                if (element.mefAnswers.length > 0) {
                  if (
                    element.mefAnswers[0].hasOwnProperty("productDiscussed") ||
                    element.mefAnswers[0].productDiscussed != undefined ||
                    element.mefAnswers[0].productDiscussed != null
                  ) {
                    const where = {
                      _id: { inq: element.mefAnswers[0].productDiscussed },
                      companyId: element.companyId,
                    };
                    const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                      {
                        where: where,
                      }
                    );
                    const givenProducts = [];
                    if (mefProducts.length > 0 && mefProducts != null) {
                      mefProducts.forEach((prod) => {
                        givenProducts.push({
                          productQuantity: 0,
                          availableStock: 0,
                          pob: 0,
                          productId: prod.id,
                          productName: prod.productName,
                          productPrice: 0,
                        });
                      });
                    }
                    element["givenProducts"] = givenProducts;
                  }
                }
              }
              if (index == result.length - 1) {
                cb(false, result);
              }
            });
          } else {
            cb(false, []);
          }
        }
      );
    }
  };
  Dcrprovidervisitdetails.remoteMethod("getReminderProviderVisitDetails", {
    description: "Date wise DCR details",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  
  
  // Provider Visit Detail report
  //-------Updated by Rahul CHoudhary shared by Preeti at 12-06-2020 for vygon .
  Dcrprovidervisitdetails.getProviderVisitDetail = function (params, cb) {
	  
    var self = this;
    var DcrprovidervisitdetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    var aMatch = {};
    let categoryMatch = {
      
      "providerDetail.status": {
        $in: [true, false],
      },
      "providerDetail.appStatus": "approved",
    };
    if (!params.hasOwnProperty("category")) {

      categoryMatch["providerDetail.category"] = {
        $in: params.category,
      };
    }
   
    let includeUnlistedProviderMatch = {};
    // if(params.hasOwnProperty("phoneOrder")){
    //   includeUnlistedProviderMatch["phoneOrder"]=params.phoneOrder
    // }
    
    

    if (params.unlistedInclude == true) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
        //   "providerStatus": "unlisted"
      };
    } else if (params.unlistedInclude == false) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
		
        providerStatus: { $ne: "unlisted" },
      };
    }
    params.providerType.forEach(element => {
      if(typeof(element)=="boolean"){
        includeUnlistedProviderMatch["phoneOrder"]=element
        params.providerType.pop(element)
      }
    });
    includeUnlistedProviderMatch["providerType"]={$in:params.providerType}
    
    if (params.type == "State" || params.type == "Headquarter") {
      Dcrprovidervisitdetails.app.models.UserInfo.getAllUserIdsBasedOnType(
        params,
        function (err, userRecords) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getProviderVisitDetail",
            });
          }
          var aProject = {};
          if (params.providerType == "RMP") {
            aProject = {
              _id: 0,
              providerName: "$providerDetail.providerName",
              providerCode: "$providerDetail.providerCode",
              category: "$providerDetail.category",
              address: "$providerDetail.address",
              phone: "$providerDetail.phone",
              status: "$providerDetail.status",
              providerType: "$providerDetail.providerType",
              frequencyVisit: "$providerDetail.frequencyVisit",
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
              blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
              totalVisit: { $size: "$visitDates" },
              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              userName: "$userDetails.name",
			  specialization:"$providerDetail.specialization",
              mefAnswers: 1,
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              givenProducts: "$productDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          } else {
            aProject = {
              _id: 0,
              providerName: "$providerDetail.providerName",
              providerCode: "$providerDetail.providerCode",
              category: "$providerDetail.category",
              address: "$providerDetail.address",
              phone: "$providerDetail.phone",
              status: "$providerDetail.status",
              providerType: "$providerDetail.providerType",
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
              blockName: { $arrayElemAt: ["$blockDetail.blockName", 0] },
              totalVisit: { $size: "$visitDates" },
		      	  specialization:"$providerDetail.specialization",
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              mefAnswers: 1,
              userName: "$userDetails.name",
              givenProducts: "$productDetails",
              giftDetails:"$giftDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          }
          if (userRecords.length > 0) {
            includeUnlistedProviderMatch.submitBy = {
              $in: userRecords[0].userIds,
            };
    
 
            DcrprovidervisitdetailsCollection.aggregate(
              // Stage 1
              {
                $match: includeUnlistedProviderMatch,
              },
              {
                $unwind: {
                  path: "$productDetails",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $group: {
                  _id: {
                    userId: "$submitBy",
                    stateId: "$stateId",
                    districtId: "$districtId",
                    providerId: "$providerId",
                  },
                  totalVisit: {
                    $addToSet: {
                      dcrdate: "$dcrDate",
                      submitBy: "$submitBy",
                      productDetails: "$productDetails",
                    },
                  },
                  visitDates: {
                    $addToSet: {
                      $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
                    },
                  },
                  productDetails: {
                    $addToSet: "$productDetails",
                  },
                  giftDetails: {
                    $addToSet: "$giftDetails",
                  },
                  mefAnswers: {
                    $addToSet: "$mefAnswers",
                  },
                  providerRemarks: {
                    $push: "$remarks",
                  },
                  productPob: {
                    $addToSet: "$productPob",
                  }
                },
              },
              {
                $lookup: {
                  from: "State",
                  localField: "_id.stateId",
                  foreignField: "_id",
                  as: "stateInfo",
                },
              },
              {
                $lookup: {
                  from: "District",
                  localField: "_id.districtId",
                  foreignField: "_id",
                  as: "district",
                },
              },
              {
                $lookup: {
                  from: "Providers",
                  localField: "_id.providerId",
                  foreignField: "_id",
                  as: "providerDetail",
                },
              },

              {
                $unwind: {
                  path: "$providerDetail",
                  preserveNullAndEmptyArrays: true,
                },
              },
             
              {
                $lookup: {
                  from: "Area",
                  localField: "providerDetail.blockId",
                  foreignField: "_id",
                  as: "blockDetail",
                },
              },
              {
                $lookup: {
                  from: "UserInfo",
                  localField: "_id.userId",
                  foreignField: "userId",
                  as: "userDetails",
                },
              },
              {
                $unwind: "$userDetails",
              },
              {
                $match: {
                  "userDetails.status": true,
                },
              },
              {
                $unwind: {
                  path: "$userDetails.divisionId",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "DivisionMaster",
                  localField: "userDetails.divisionId",
                  foreignField: "_id",
                  as: "division",
                },
              },
              {
                $project: aProject,
              },
              function (err, result) {

                
                if (err) {
                  sendMail.sendMail({
                    collectionName: "Dcrprovidervisitdetails",
                    errorObject: err,
                    paramsObject: params,
                    methodName: "getProviderVisitDetail",
                  });
                  // console.log(err);
                  return cb(err);
                }
                if (!result) {
                  var err = new Error("no records found");
                  err.statusCode = 404;
                  err.code = "NOT FOUND";
                  return cb(err);
                }
                if (result.length > 0) {
                  result.forEach(async (element, index) => {
                    if (element.hasOwnProperty("mefAnswers")) {
                      if (element.mefAnswers.length > 0) {
                        if (
                          element.mefAnswers[0].hasOwnProperty(
                            "productDiscussed"
                          ) ||
                          element.mefAnswers[0].productDiscussed != undefined ||
                          element.mefAnswers[0].productDiscussed != null
                        ) {
                          const where = {
                            _id: {
                              inq: element.mefAnswers[0].productDiscussed,
                            },
                            companyId: element.companyId,
                          };
                          const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                            {
                              where: where,
                            }
                          );
                          const givenProducts = [];
                          if (mefProducts.length > 0 && mefProducts != null) {
                            mefProducts.forEach((prod) => {
                              givenProducts.push({
                                productQuantity: 0,
                                availableStock: 0,
                                pob: 0,
                                productId: prod.id,
                                productName: prod.productName,
                                productPrice: 0,
                              });
                            });
                          }
                          element["givenProducts"] = givenProducts;
                        }
                      }
                    }
                    if (index == result.length - 1) {
                      cb(false, result);
                    }
                  });
                } else {
                  cb(false, []);
                }
              }
            );
          } else {
            cb(false, []);
          }
        }
      );
    } else {      
      var userIds = [];
      for (var i = 0; i < params.userIds.length; i++) {
        userIds.push(ObjectId(params.userIds[i]));
      }
      includeUnlistedProviderMatch.submitBy = {
        $in: userIds,
      };
      
      
      
      DcrprovidervisitdetailsCollection.aggregate(
        // Stage 1
        {
          $match: includeUnlistedProviderMatch,
        },
        {
          $unwind: {
            path: "$productDetails",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: {
              userId: "$submitBy",
              stateId: "$stateId",
              districtId: "$districtId",
              providerId: "$providerId",
            },
            totalVisit: {
              $sum: 1,
            },
            visitDates: {
              $addToSet: {
                $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
              },
            },
            productDetails: {
              $addToSet: "$productDetails",
            },
            giftDetails: {
              $addToSet: "$giftDetails",
            },
            mefAnswers: {
              $addToSet: "$mefAnswers",
            },
            providerRemarks: {
              $push: "$remarks",
            },
            productPob: {
              $addToSet: "$productPob",
            },
            dcrId: {
              $addToSet: "$dcrId",
            },
          },
        },
        // Stage 2
        {
          $lookup: {
            from: "State",
            localField: "_id.stateId",
            foreignField: "_id",
            as: "stateInfo",
          },
        },
        // Stage 3
        {
          $lookup: {
            from: "District",
            localField: "_id.districtId",
            foreignField: "_id",
            as: "district",
          },
        },
        // Stage 4
        {
          $lookup: {
            from: "Providers",
            localField: "_id.providerId",
            foreignField: "_id",
            as: "providerDetail",
          },
        },
        {
          $unwind: {
            path: "$providerDetail",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $match: categoryMatch,
        },
        {
          $lookup: {
            from: "Area",
            localField: "providerDetail.blockId",
            foreignField: "_id",
            as: "blockDetail",
          },
        },
        {
          $lookup: {
            from: "UserInfo",
            localField: "_id.userId",
            foreignField: "userId",
            as: "userDetails",
          },
        },
        {
          $unwind: "$userDetails",
        },
        {
          $match: {
            "userDetails.status": true,
          },
        },
        {
          $unwind: {
            path: "$userDetails.divisionId",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "DivisionMaster",
            localField: "userDetails.divisionId",
            foreignField: "_id",
            as: "division",
          },
        },
        {
          $project: {
            providerName: "$providerDetail.providerName",
            providerCode: "$providerDetail.providerCode",
            category: "$providerDetail.category",
            address: "$providerDetail.address",
            phone: "$providerDetail.phone",
            status: "$providerDetail.status",
            frequencyVisit: "$providerDetail.frequencyVisit",
            stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
            districtName: { $arrayElemAt: ["$district.districtName", 0] },
            blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
            totalVisit: { $size: "$visitDates" },
            visitDates: 1,
            mefAnswers: 1,
            productPob:{ $arrayElemAt: ["$productPob", 0] },
            dcrId: {
              $arrayElemAt: ["$dcrId", 0]
            },

            //userName: { $arrayElemAt: ["$userDetails.name", 0] },
            userName: "$userDetails.name",
            givenProducts: "$productDetails",
            giftDetails:"$giftDetails",
            providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
            divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
          },
        },
        function (err, result) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getProviderVisitDetail",
            });
            // console.log(err);
            return cb(err);
          }
          if (!result) {
            var err = new Error("no records found");
            err.statusCode = 404;
            err.code = "NOT FOUND";
            return cb(err);
          }
          if (result.length > 0) {            
            result.forEach(async (element, index) => {
              if (element.hasOwnProperty("mefAnswers")) {
                if (element.mefAnswers.length > 0) {
                  if (
                    element.mefAnswers[0].hasOwnProperty("productDiscussed") ||
                    element.mefAnswers[0].productDiscussed != undefined ||
                    element.mefAnswers[0].productDiscussed != null
                  ) {
                    const where = {
                      _id: { inq: element.mefAnswers[0].productDiscussed },
                      companyId: element.companyId,
                    };
                    const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                      {
                        where: where,
                      }
                    );
                    const givenProducts = [];
                    if (mefProducts.length > 0 && mefProducts != null) {
                      mefProducts.forEach((prod) => {
                        givenProducts.push({
                          productQuantity: 0,
                          availableStock: 0,
                          pob: 0,
                          productId: prod.id,
                          productName: prod.productName,
                          productPrice: 0,
                        });
                      });
                    }
                    element["givenProducts"] = givenProducts;
                  }
                }
              }
              if (index == result.length - 1) {
                cb(false, result);
              }
            });
          } else {
            cb(false, []);
          }
        }
      );
    }
  };
  Dcrprovidervisitdetails.remoteMethod("getProviderVisitDetail", {
    description: "Date wise DCR details",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  
  
  
  
  
  /////RAHUL SAINI (26-03-2021)//////////////////////////////////
   Dcrprovidervisitdetails.getAreaVisitDetail = function (params, cb) {
	  
    var self = this;
    var DcrprovidervisitdetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    var aMatch = {};
    let categoryMatch = {
      
      "providerDetail.status": {
        $in: [true, false],
      },
      "providerDetail.appStatus": "approved",
    };
    if (!params.hasOwnProperty("category")) {

      categoryMatch["providerDetail.category"] = {
        $in: params.category,
      };
    }
   
    let includeUnlistedProviderMatch = {};
    // if(params.hasOwnProperty("phoneOrder")){
    //   includeUnlistedProviderMatch["phoneOrder"]=params.phoneOrder
    // }
    
    

    if (params.unlistedInclude == true) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
        //   "providerStatus": "unlisted"
      };
    } else if (params.unlistedInclude == false) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
		
        providerStatus: { $ne: "unlisted" },
      };
    }
    params.providerType.forEach(element => {
      if(typeof(element)=="boolean"){
        includeUnlistedProviderMatch["phoneOrder"]=element
        params.providerType.pop(element)
      }
    });
    includeUnlistedProviderMatch["providerType"]={$in:params.providerType}
    
    if (params.type == "State" || params.type == "Headquarter") {
      Dcrprovidervisitdetails.app.models.UserInfo.getAllUserIdsBasedOnType(
        params,
        function (err, userRecords) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getAreaVisitDetail",
            });
          }
          var aProject = {};
          if (params.providerType == "RMP") {
            aProject = {
              _id: 0,
            
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
              blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
              totalVisit: { $size: "$visitDates" },
              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              userName: "$userDetails.name",
			
              mefAnswers: 1,
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              givenProducts: "$productDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          } else {
            aProject = {
              _id: 0,
           
              stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
              districtName: { $arrayElemAt: ["$district.districtName", 0] },
           
			    blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
              totalVisit: { $size: "$visitDates" },
		      	  
              productPob:{ $arrayElemAt: ["$productPob", 0] },

              visitDates: 1,
              //userName: { $arrayElemAt: ["$userDetails.name", 0] },
              mefAnswers: 1,
              userName: "$userDetails.name",
              givenProducts: "$productDetails",
              giftDetails:"$giftDetails",
              providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
              divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
            };
          }
          if (userRecords.length > 0) {
            includeUnlistedProviderMatch.submitBy = {
              $in: userRecords[0].userIds,
            };
    
 
            DcrprovidervisitdetailsCollection.aggregate(
              // Stage 1
              {
                $match: includeUnlistedProviderMatch,
              },
              {
                $unwind: {
                  path: "$productDetails",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $group: {
                  _id: {
                    userId: "$submitBy",
                    stateId: "$stateId",
                    districtId: "$districtId",
                   blockId: "$blockId",
                  },
                  totalVisit: {
                    $addToSet: {
                      dcrdate: "$dcrDate",
                      submitBy: "$submitBy",
                      productDetails: "$productDetails",
                    },
                  },
                  visitDates: {
                    $addToSet: {
                      $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
                    },
                  },
                  productDetails: {
                    $addToSet: "$productDetails",
                  },
                  giftDetails: {
                    $addToSet: "$giftDetails",
                  },
                  mefAnswers: {
                    $addToSet: "$mefAnswers",
                  },
                  providerRemarks: {
                    $push: "$remarks",
                  },
                  productPob: {
                    $addToSet: "$productPob",
                  }
                },
              },
              {
                $lookup: {
                  from: "State",
                  localField: "_id.stateId",
                  foreignField: "_id",
                  as: "stateInfo",
                },
              },
              {
                $lookup: {
                  from: "District",
                  localField: "_id.districtId",
                  foreignField: "_id",
                  as: "district",
                },
              },
             
             
              {
                $lookup: {
                  from: "Area",
                  localField: "_id.blockId",
                  foreignField: "_id",
                  as: "blockDetail",
                },
              },
              {
                $lookup: {
                  from: "UserInfo",
                  localField: "_id.userId",
                  foreignField: "userId",
                  as: "userDetails",
                },
              },
              {
                $unwind: "$userDetails",
              },
              {
                $match: {
                  "userDetails.status": true,
                },
              },
              {
                $unwind: {
                  path: "$userDetails.divisionId",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "DivisionMaster",
                  localField: "userDetails.divisionId",
                  foreignField: "_id",
                  as: "division",
                },
              },
              {
                $project: aProject,
              },
              function (err, result) {

                
                if (err) {
                  sendMail.sendMail({
                    collectionName: "Dcrprovidervisitdetails",
                    errorObject: err,
                    paramsObject: params,
                    methodName: "getAreaVisitDetail",
                  });
                  // console.log(err);
                  return cb(err);
                }
                if (!result) {
                  var err = new Error("no records found");
                  err.statusCode = 404;
                  err.code = "NOT FOUND";
                  return cb(err);
                }
                if (result.length > 0) {
                  result.forEach(async (element, index) => {
                    if (element.hasOwnProperty("mefAnswers")) {
                      if (element.mefAnswers.length > 0) {
                        if (
                          element.mefAnswers[0].hasOwnProperty(
                            "productDiscussed"
                          ) ||
                          element.mefAnswers[0].productDiscussed != undefined ||
                          element.mefAnswers[0].productDiscussed != null
                        ) {
                          const where = {
                            _id: {
                              inq: element.mefAnswers[0].productDiscussed,
                            },
                            companyId: element.companyId,
                          };
                          const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                            {
                              where: where,
                            }
                          );
                          const givenProducts = [];
                          if (mefProducts.length > 0 && mefProducts != null) {
                            mefProducts.forEach((prod) => {
                              givenProducts.push({
                                productQuantity: 0,
                                availableStock: 0,
                                pob: 0,
                                productId: prod.id,
                                productName: prod.productName,
                                productPrice: 0,
                              });
                            });
                          }
                          element["givenProducts"] = givenProducts;
                        }
                      }
                    }
                    if (index == result.length - 1) {
                      cb(false, result);
                    }
                  });
                } else {
                  cb(false, []);
                }
              }
            );
          } else {
            cb(false, []);
          }
        }
      );
    } else {      
      var userIds = [];
      for (var i = 0; i < params.userIds.length; i++) {
        userIds.push(ObjectId(params.userIds[i]));
      }
      includeUnlistedProviderMatch.submitBy = {
        $in: userIds,
      };
      
      
      
      DcrprovidervisitdetailsCollection.aggregate(
        // Stage 1
        {
          $match: includeUnlistedProviderMatch,
        },
        {
          $unwind: {
            path: "$productDetails",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: {
              userId: "$submitBy",
              stateId: "$stateId",
              districtId: "$districtId",
              blockId: "$blockId",
            },
            totalVisit: {
              $sum: 1,
            },
            visitDates: {
              $addToSet: {
                $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" },
              },
            },
            productDetails: {
              $addToSet: "$productDetails",
            },
            giftDetails: {
              $addToSet: "$giftDetails",
            },
            mefAnswers: {
              $addToSet: "$mefAnswers",
            },
            providerRemarks: {
              $push: "$remarks",
            },
            productPob: {
              $addToSet: "$productPob",
            },
            dcrId: {
              $addToSet: "$dcrId",
            },
          },
        },
        // Stage 2
        {
          $lookup: {
            from: "State",
            localField: "_id.stateId",
            foreignField: "_id",
            as: "stateInfo",
          },
        },
        // Stage 3
        {
          $lookup: {
            from: "District",
            localField: "_id.districtId",
            foreignField: "_id",
            as: "district",
          },
        },
        // Stage 4
        
       
        
        {
          $lookup: {
            from: "Area",
            localField: "_id.blockId",
            foreignField: "_id",
            as: "blockDetail",
          },
        },
        {
          $lookup: {
            from: "UserInfo",
            localField: "_id.userId",
            foreignField: "userId",
            as: "userDetails",
          },
        },
        {
          $unwind: "$userDetails",
        },
        {
          $match: {
            "userDetails.status": true,
          },
        },
        {
          $unwind: {
            path: "$userDetails.divisionId",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "DivisionMaster",
            localField: "userDetails.divisionId",
            foreignField: "_id",
            as: "division",
          },
        },
        {
          $project: {
           
            stateName: { $arrayElemAt: ["$stateInfo.stateName", 0] },
            districtName: { $arrayElemAt: ["$district.districtName", 0] },
            blockName: { $arrayElemAt: ["$blockDetail.areaName", 0] },
            totalVisit: { $size: "$visitDates" },
            visitDates: 1,
            mefAnswers: 1,
            productPob:{ $arrayElemAt: ["$productPob", 0] },
            dcrId: {
              $arrayElemAt: ["$dcrId", 0]
            },

            //userName: { $arrayElemAt: ["$userDetails.name", 0] },
            userName: "$userDetails.name",
            givenProducts: "$productDetails",
            giftDetails:"$giftDetails",
            providerDiscussion: { $arrayElemAt: ["$providerRemarks", 0] },
            divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
          },
        },
        function (err, result) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getAreaVisitDetail",
            });
            // console.log(err);
            return cb(err);
          }
          if (!result) {
            var err = new Error("no records found");
            err.statusCode = 404;
            err.code = "NOT FOUND";
            return cb(err);
          }
          if (result.length > 0) {            
            result.forEach(async (element, index) => {
              if (element.hasOwnProperty("mefAnswers")) {
                if (element.mefAnswers.length > 0) {
                  if (
                    element.mefAnswers[0].hasOwnProperty("productDiscussed") ||
                    element.mefAnswers[0].productDiscussed != undefined ||
                    element.mefAnswers[0].productDiscussed != null
                  ) {
                    const where = {
                      _id: { inq: element.mefAnswers[0].productDiscussed },
                      companyId: element.companyId,
                    };
                    const mefProducts = await Dcrprovidervisitdetails.app.models.Products.find(
                      {
                        where: where,
                      }
                    );
                    const givenProducts = [];
                    if (mefProducts.length > 0 && mefProducts != null) {
                      mefProducts.forEach((prod) => {
                        givenProducts.push({
                          productQuantity: 0,
                          availableStock: 0,
                          pob: 0,
                          productId: prod.id,
                          productName: prod.productName,
                          productPrice: 0,
                        });
                      });
                    }
                    element["givenProducts"] = givenProducts;
                  }
                }
              }
              if (index == result.length - 1) {
                cb(false, result);
              }
            });
          } else {
            cb(false, []);
          }
        }
      );
    }
  };
  Dcrprovidervisitdetails.remoteMethod("getAreaVisitDetail", {
    description: "Date wise DCR details",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  //--------------------------------Praveen Kumar(25-01-2019)-----------------------------------
  Dcrprovidervisitdetails.getMonthOnMOnthProgression = function(params, cb) {
    let self = this;
    let dcrProviderVisitDetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    let providerCollection = self
      .getDataSource()
      .connector.collection(
        Dcrprovidervisitdetails.app.models.Providers.modelName
      );
    var DCRMasterCollection = self
      .getDataSource()
      .connector.collection(
        Dcrprovidervisitdetails.app.models.DCRMaster.modelName
      );
    let dynamicMatch = {
      companyId: ObjectId(params.companyId),
      dcrDate: {
        $gte: new Date(params.fromDate),
        $lte: new Date(params.toDate)
        //$gte: new Date("2019-06-01"),
        //$lte: new Date("2019-06-30")
      },
      workingStatus: "Working"
    };
    let dynamicLookup = {
      from: "DCRProviderVisitDetails",
      localField: "_id",
      foreignField: "dcrId",
      as: "providerDetails"
    };
    let dynamicUnwind = {
      path: "$providerDetails",
      preserveNullAndEmptyArrays: true
    };
    let dynamicGroup = {
      _id: {
        month: {
          $month: "$dcrDate"
        },
        year: {
          $year: "$dcrDate"
        }
      },
      workingDCR: {
        $addToSet: {
          submitBy: "$submitBy",
          dcrDate: "$dcrDate"
        }
      }
    };
    let dynamicProject = {
      _id: 0,
      month: "$_id.month",
      year: "$_id.year",
      totalWorkingDCR: {
        $size: "$workingDCR"
      },
      totalVisitedRMP: "$TotalDoctor",
      totalVisitedDrug: "$TotalVendor",
      totalVisitedStockist: "$TotalStockist",
      TotalVisit: {
        $add: ["$TotalDoctor", "$TotalVendor", "$TotalStockist"]
      },
      overAllAvg: {
        $divide: [
          { $add: ["$TotalDoctor", "$TotalVendor", "$TotalStockist"] },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      },
      totalUniqueVisitedRMP: {
        $size: "$TotalUniqueDoctor"
      },
      totalUniqueVisitedDrug: {
        $size: "$TotalUniqueVendor"
      },
      totalUniqueVisitedStockist: {
        $size: "$TotalUniqueStockist"
      },
      TotalUniqueVisit: {
        $add: [
          { $size: "$TotalUniqueDoctor" },
          { $size: "$TotalUniqueVendor" },
          { $size: "$TotalUniqueStockist" }
        ]
      }
    };
    //-----Dynamic Group and Project For Doctors--------------------
    if (
      params.unlistedValidations.unlistedDocCall == true &&
      params.unlistedValidations.unlistedDocAvg == true
    ) {
      dynamicGroup.TotalDoctor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueDoctor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.drCallAvg = {
        $divide: [
          "$TotalDoctor",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedDoctorCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueDoctor"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedDocCall == false &&
      params.unlistedValidations.unlistedDocAvg == false
    ) {
      dynamicGroup.TotalDoctor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueDoctor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.drCallAvg = {
        $divide: [
          "$TotalDoctor",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedDoctorCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueDoctor"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedDocCall == true &&
      params.unlistedValidations.unlistedDocAvg == false
    ) {
      dynamicGroup.TotalDoctor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.doctorCallForUnlistedAvgFalse = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueDoctor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedDoctorCallForUnlistedAvgFalse = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.drCallAvg = {
        $divide: [
          "$doctorCallForUnlistedAvgFalse",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedDoctorCallAvg = {
        $divide: [
          {
            $size: "$listedDoctorCallForUnlistedAvgFalse"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedDocCall == false &&
      params.unlistedValidations.unlistedDocAvg == true
    ) {
      dynamicGroup.TotalDoctor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.doctorCallForUnlistedAvgTrue = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueDoctor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                },
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedDoctorCallForUnlistedAvgTrue = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "RMP"]
                }
              ]
            },
            "$providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.drCallAvg = {
        $divide: [
          "$doctorCallForUnlistedAvgTrue",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedDoctorCallAvg = {
        $divide: [
          {
            $size: "$listedDoctorCallForUnlistedAvgTrue"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    }
    //-----Dynamic Group and Project For Vendor--------------------
    if (
      params.unlistedValidations.unlistedVenCall == true &&
      params.unlistedValidations.unlistedVenAvg == true
    ) {
      dynamicGroup.TotalVendor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueVendor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.venCallAvg = {
        $divide: [
          "$TotalVendor",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedVendorCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueVendor"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      //---------------Stockist--------------------------
      dynamicGroup.TotalStockist = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueStockist = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.stockistCallAvg = {
        $divide: [
          "$TotalStockist",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedStockistCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueStockist"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedVenCall == false &&
      params.unlistedValidations.unlistedVenAvg == false
    ) {
      dynamicGroup.TotalVendor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueVendor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.venCallAvg = {
        $divide: [
          "$TotalVendor",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedVendorCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueVendor"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      //---------------Stockist--------------------------
      dynamicGroup.TotalStockist = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueStockist = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.stockistCallAvg = {
        $divide: [
          "$TotalStockist",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedStockistCallAvg = {
        $divide: [
          {
            $size: "$TotalUniqueStockist"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedVenCall == true &&
      params.unlistedValidations.unlistedVenAvg == false
    ) {
      dynamicGroup.TotalVendor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.vendorCallForUnlistedAvgFalse = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueVendor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedVendorCallForUnlistedAvgFalse = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.venCallAvg = {
        $divide: [
          "$vendorCallForUnlistedAvgFalse",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedVendorCallAvg = {
        $divide: [
          {
            $size: "$listedVendorCallForUnlistedAvgFalse"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      //-----------------------------Stockist-----------------------------------------
      dynamicGroup.TotalStockist = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.stockistCallForUnlistedAvgFalse = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueStockist = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedStockistCallForUnlistedAvgFalse = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.stockistCallAvg = {
        $divide: [
          "$stockistCallForUnlistedAvgFalse",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedStockistCallAvg = {
        $divide: [
          {
            $size: "$listedStockistCallForUnlistedAvgFalse"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    } else if (
      params.unlistedValidations.unlistedVenCall == false &&
      params.unlistedValidations.unlistedVenAvg == true
    ) {
      dynamicGroup.TotalVendor = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.vendorCallForUnlistedAvgTrue = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueVendor = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedVendorCallForUnlistedAvgTrue = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Drug"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.venCallAvg = {
        $divide: [
          "$vendorCallForUnlistedAvgTrue",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedVendorCallAvg = {
        $divide: [
          {
            $size: "$listedVendorCallForUnlistedAvgTrue"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      //-------------------Stockist--------------------------------
      dynamicGroup.TotalStockist = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.stockistCallForUnlistedAvgTrue = {
        $sum: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            1,
            0
          ]
        }
      };
      dynamicGroup.TotalUniqueStockist = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $ne: ["$providerDetails.providerStatus", "unlisted"]
                },
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicGroup.listedStockistCallForUnlistedAvgTrue = {
        $addToSet: {
          $cond: [
            {
              $and: [
                {
                  $eq: ["$providerDetails.providerType", "Stockist"]
                }
              ]
            },
            "$providerDetails.providerId",
            "$abc"
          ]
        }
      };
      dynamicProject.stockistCallAvg = {
        $divide: [
          "$stockistCallForUnlistedAvgTrue",
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
      dynamicProject.listedStockistCallAvg = {
        $divide: [
          {
            $size: "$listedStockistCallForUnlistedAvgTrue"
          },
          {
            $cond: {
              if: {
                $gte: [{ $size: "$workingDCR" }, 1]
              },
              then: { $size: "$workingDCR" },
              else: 1 // used 1 for divided by zero
            }
          }
        ]
      };
    }
    if (params.designationLevel == 0) {
      DCRMasterCollection.aggregate(
        {
          $match: dynamicMatch
        },
        {
          $lookup: dynamicLookup
        },
        {
          $unwind: dynamicUnwind
        },
        {
          $group: dynamicGroup
        },
        {
          $project: dynamicProject
        },
        function(err, dcrResult) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getMonthOnMOnthProgression"
            });
            // console.log(err)
          }
          // // console.log("Wating for result :(");
          //// console.log(dcrResult)
          let startingMonth = 1;
          let monthArray = [];
          let yearArray = [];
          let monthNameArray = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
          ];
          let fromDateMonth = parseInt(params.fromDate.split("-")[1]);
          let fromDateYear = parseInt(params.fromDate.split("-")[0]);
          let finalResult = [];
          for (let i = 0; i < 6; i++) {
            if (fromDateMonth > 12) {
              monthArray[i] = startingMonth;
              startingMonth++;
              yearArray[i] = fromDateYear + 1;
            } else {
              monthArray[i] = fromDateMonth;
              yearArray[i] = fromDateYear;
              fromDateMonth++;
            }
          }
          for (let i = 0; i < monthArray.length; i++) {
            let dcrResultFound = dcrResult.filter(function(dcrObj) {
              return (
                dcrObj.month == monthArray[i] && dcrObj.year == yearArray[i]
              );
            });
            if (dcrResultFound.length > 0) {
              finalResult.push({
                month: dcrResultFound[0].month,
                year: dcrResultFound[0].year,
                monthNameWithYear:
                  monthNameArray[monthArray[i] - 1] +
                  "," +
                  dcrResultFound[0].year,
                totalWorkingDCR: dcrResultFound[0].totalWorkingDCR,
                totalDoctorCall: dcrResultFound[0].totalVisitedRMP,
                totalVendorCall: dcrResultFound[0].totalVisitedDrug,
                totalStockistCall: dcrResultFound[0].totalVisitedStockist,
                totalDocVenStockCall: dcrResultFound[0].TotalVisit,
                //drCallAvg: (dcrResultFound[0].totalVisitedRMP / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //venCallAvg: (dcrResultFound[0].totalVisitedDrug / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //stockCallAvg: (dcrResultFound[0].totalVisitedStockist / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //overAllAvg: (dcrResultFound[0].TotalVisit / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                drCallAvg: dcrResultFound[0].drCallAvg.toFixed(2),
                venCallAvg: dcrResultFound[0].venCallAvg.toFixed(2),
                stockCallAvg: dcrResultFound[0].stockistCallAvg.toFixed(2),
                overAllAvg: dcrResultFound[0].overAllAvg.toFixed(2),
                totalUniqueDoctorCall: dcrResultFound[0].totalUniqueVisitedRMP,
                totalUniqueVendorCall: dcrResultFound[0].totalUniqueVisitedDrug,
                totalUniqueStockistCall:
                  dcrResultFound[0].totalUniqueVisitedStockist,
                TotalUniqueDocVenStockCall: dcrResultFound[0].TotalUniqueVisit
              });
            } else {
              finalResult.push({
                month: monthArray[i],
                year: yearArray[i],
                monthNameWithYear:
                  monthNameArray[monthArray[i] - 1] + "," + yearArray[i],
                totalWorkingDCR: 0,
                totalDoctorCall: 0,
                totalVendorCall: 0,
                totalStockistCall: 0,
                totalDocVenStockCall: 0,
                drCallAvg: 0,
                venCallAvg: 0,
                stockCallAvg: 0,
                overAllAvg: 0,
                totalUniqueDoctorCall: 0,
                totalUniqueVendorCall: 0,
                totalUniqueStockistCall: 0,
                TotalUniqueDocVenStockCall: 0
              });
            }
          }
          //// console.log(finalResult[0])
          return cb(false, finalResult);
        }
      );
    } else if (params.designationLevel >= 2) {
      Dcrprovidervisitdetails.app.models.Hierarchy.find(
        {
          where: {
            companyId: params.companyId,
            supervisorId: params.userId,
            status: true
          }
        },
        function(err, hrcyResult) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getMonthOnMOnthProgression"
            });
            // console.log(err);
          }
          let passingUserIds = [];
          for (let i = 0; i < hrcyResult.length; i++) {
            passingUserIds.push(hrcyResult[i].userId);
          }
          dynamicMatch.submitBy = {
            $in: passingUserIds
          };
          DCRMasterCollection.aggregate(
            {
              $match: dynamicMatch
            },
            {
              $lookup: dynamicLookup
            },
            {
              $unwind: dynamicUnwind
            },
            {
              $group: dynamicGroup
            },
            {
              $project: dynamicProject
            },
            function(err, dcrResult) {
              if (err) {
                sendMail.sendMail({
                  collectionName: "Dcrprovidervisitdetails",
                  errorObject: err,
                  paramsObject: params,
                  methodName: "getMonthOnMOnthProgression"
                });
                // console.log(err)
              }
              // // console.log("Wating for result :(");
              //// console.log(dcrResult)
              let startingMonth = 1;
              let monthArray = [];
              let yearArray = [];
              let monthNameArray = [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
              ];
              let fromDateMonth = parseInt(params.fromDate.split("-")[1]);
              let fromDateYear = parseInt(params.fromDate.split("-")[0]);
              let finalResult = [];
              for (let i = 0; i < 6; i++) {
                if (fromDateMonth > 12) {
                  monthArray[i] = startingMonth;
                  startingMonth++;
                  yearArray[i] = fromDateYear + 1;
                } else {
                  monthArray[i] = fromDateMonth;
                  yearArray[i] = fromDateYear;
                  fromDateMonth++;
                }
              }
              for (let i = 0; i < monthArray.length; i++) {
                let dcrResultFound = dcrResult.filter(function(dcrObj) {
                  return (
                    dcrObj.month == monthArray[i] && dcrObj.year == yearArray[i]
                  );
                });
                if (dcrResultFound.length > 0) {
                  finalResult.push({
                    month: dcrResultFound[0].month,
                    year: dcrResultFound[0].year,
                    monthNameWithYear:
                      monthNameArray[monthArray[i] - 1] +
                      "," +
                      dcrResultFound[0].year,
                    totalWorkingDCR: dcrResultFound[0].totalWorkingDCR,
                    totalDoctorCall: dcrResultFound[0].totalVisitedRMP,
                    totalVendorCall: dcrResultFound[0].totalVisitedDrug,
                    totalStockistCall: dcrResultFound[0].totalVisitedStockist,
                    totalDocVenStockCall: dcrResultFound[0].TotalVisit,
                    //drCallAvg: (dcrResultFound[0].totalVisitedRMP / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                    //venCallAvg: (dcrResultFound[0].totalVisitedDrug / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                    //stockCallAvg: (dcrResultFound[0].totalVisitedStockist / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                    //overAllAvg: (dcrResultFound[0].TotalVisit / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                    drCallAvg: dcrResultFound[0].drCallAvg.toFixed(2),
                    venCallAvg: dcrResultFound[0].venCallAvg.toFixed(2),
                    stockCallAvg: dcrResultFound[0].stockistCallAvg.toFixed(2),
                    overAllAvg: dcrResultFound[0].overAllAvg.toFixed(2),
                    totalUniqueDoctorCall:
                      dcrResultFound[0].totalUniqueVisitedRMP,
                    totalUniqueVendorCall:
                      dcrResultFound[0].totalUniqueVisitedDrug,
                    totalUniqueStockistCall:
                      dcrResultFound[0].totalUniqueVisitedStockist,
                    TotalUniqueDocVenStockCall:
                      dcrResultFound[0].TotalUniqueVisit
                  });
                } else {
                  finalResult.push({
                    month: monthArray[i],
                    year: yearArray[i],
                    monthNameWithYear:
                      monthNameArray[monthArray[i] - 1] + "," + yearArray[i],
                    totalWorkingDCR: 0,
                    totalDoctorCall: 0,
                    totalVendorCall: 0,
                    totalStockistCall: 0,
                    totalDocVenStockCall: 0,
                    drCallAvg: 0,
                    venCallAvg: 0,
                    stockCallAvg: 0,
                    overAllAvg: 0,
                    totalUniqueDoctorCall: 0,
                    totalUniqueVendorCall: 0,
                    totalUniqueStockistCall: 0,
                    TotalUniqueDocVenStockCall: 0
                  });
                }
              }
              //// console.log(finalResult[0])
              return cb(false, finalResult);
            }
          );
        }
      );
    } else if (params.designationLevel == 1) {
      dynamicMatch.submitBy = ObjectId(params.userId);
      DCRMasterCollection.aggregate(
        {
          $match: dynamicMatch
        },
        {
          $lookup: dynamicLookup
        },
        {
          $unwind: dynamicUnwind
        },
        {
          $group: dynamicGroup
        },
        {
          $project: dynamicProject
        },
        function(err, dcrResult) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "getMonthOnMOnthProgression"
            });
            // console.log(err)
          }
          // // console.log("Wating for result :(");
          //// console.log(dcrResult)
          let startingMonth = 1;
          let monthArray = [];
          let yearArray = [];
          let monthNameArray = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
          ];
          let fromDateMonth = parseInt(params.fromDate.split("-")[1]);
          let fromDateYear = parseInt(params.fromDate.split("-")[0]);
          let finalResult = [];
          for (let i = 0; i < 6; i++) {
            if (fromDateMonth > 12) {
              monthArray[i] = startingMonth;
              startingMonth++;
              yearArray[i] = fromDateYear + 1;
            } else {
              monthArray[i] = fromDateMonth;
              yearArray[i] = fromDateYear;
              fromDateMonth++;
            }
          }
          for (let i = 0; i < monthArray.length; i++) {
            let dcrResultFound = dcrResult.filter(function(dcrObj) {
              return (
                dcrObj.month == monthArray[i] && dcrObj.year == yearArray[i]
              );
            });
            if (dcrResultFound.length > 0) {
              finalResult.push({
                month: dcrResultFound[0].month,
                year: dcrResultFound[0].year,
                monthNameWithYear:
                  monthNameArray[monthArray[i] - 1] +
                  "," +
                  dcrResultFound[0].year,
                totalWorkingDCR: dcrResultFound[0].totalWorkingDCR,
                totalDoctorCall: dcrResultFound[0].totalVisitedRMP,
                totalVendorCall: dcrResultFound[0].totalVisitedDrug,
                totalStockistCall: dcrResultFound[0].totalVisitedStockist,
                totalDocVenStockCall: dcrResultFound[0].TotalVisit,
                //drCallAvg: (dcrResultFound[0].totalVisitedRMP / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //venCallAvg: (dcrResultFound[0].totalVisitedDrug / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //stockCallAvg: (dcrResultFound[0].totalVisitedStockist / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                //overAllAvg: (dcrResultFound[0].TotalVisit / dcrResultFound[0].totalWorkingDCR).toFixed(2),
                drCallAvg: dcrResultFound[0].drCallAvg.toFixed(2),
                venCallAvg: dcrResultFound[0].venCallAvg.toFixed(2),
                stockCallAvg: dcrResultFound[0].stockistCallAvg.toFixed(2),
                overAllAvg: dcrResultFound[0].overAllAvg.toFixed(2),
                totalUniqueDoctorCall: dcrResultFound[0].totalUniqueVisitedRMP,
                totalUniqueVendorCall: dcrResultFound[0].totalUniqueVisitedDrug,
                totalUniqueStockistCall:
                  dcrResultFound[0].totalUniqueVisitedStockist,
                TotalUniqueDocVenStockCall: dcrResultFound[0].TotalUniqueVisit
              });
            } else {
              finalResult.push({
                month: monthArray[i],
                year: yearArray[i],
                monthNameWithYear:
                  monthNameArray[monthArray[i] - 1] + "," + yearArray[i],
                totalWorkingDCR: 0,
                totalDoctorCall: 0,
                totalVendorCall: 0,
                totalStockistCall: 0,
                totalDocVenStockCall: 0,
                drCallAvg: 0,
                venCallAvg: 0,
                stockCallAvg: 0,
                overAllAvg: 0,
                totalUniqueDoctorCall: 0,
                totalUniqueVendorCall: 0,
                totalUniqueStockistCall: 0,
                TotalUniqueDocVenStockCall: 0
              });
            }
          }
          //// console.log(finalResult[0])
          return cb(false, finalResult);
        }
      );
    }
  };
  Dcrprovidervisitdetails.remoteMethod("getMonthOnMOnthProgression", {
    description: "Getting month on month data",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //-----------------------------------------END------------------------------------------------
  // ravindra singh on 02-05-2019
  Dcrprovidervisitdetails.getProviderVisitDates = function(params, cb) {
    var self = this;
    var DcrprovidervisitdetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    var aMatch = {};
    var providerIds = [];
    for (var i = 0; i < params.providerId.length; i++) {
      providerIds.push(ObjectId(params.providerId[i]));
    }
    params.dcrDate = params.date;
    delete params["date"];
    DcrprovidervisitdetailsCollection.aggregate(
      // Stage 1
      {
        $match: params
      },
      {
        $group: {
          _id: {
            //"dcrDate": "$dcrDate",
            providerId: "$providerId"
          },
          visitDates: {
            $addToSet: {
              $dateToString: { format: "%d-%m-%Y", date: "$dcrDate" }
            }
          }
        }
      },
      function(err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: params,
            methodName: "getProviderVisitDates"
          });
          // console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Dcrprovidervisitdetails.remoteMethod("getProviderVisitDates", {
    description: "Date wise DCR details",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //END
  // Delete API for TP
  Dcrprovidervisitdetails.deleteMatchedRecord = function(params, cb) {
    // console.log("records====");
    var self = this;
    var DcrproviderCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    if (params.length === 0 || params === "") {
      var err = new Error("records is undefined or empty");
      err.statusCode = 404;
      err.code = "RECORDS ARE NOT FOUND";
      return cb(err);
    }
    DcrproviderCollection.remove(
      {
        dcrId: ObjectId(params.dcrId),
        providerType: { $in: params.providerType }
      },
      function(err, res) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: params,
            methodName: "deleteMatchedRecord"
          });
          // console.log(err);
        }
        //return cb(false,res)
      }
    );
    return cb(false, "Record has been Deleted Sucessfully");
  };
  Dcrprovidervisitdetails.remoteMethod("deleteMatchedRecord", {
    description: "delete data",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "get"
    }
  });
  // Map Reports- anjana(11.06.2019)
  Dcrprovidervisitdetails.getGeoCoordinates = function(params, cb) {
    var self = this;
    var DcrproviderCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    DcrproviderCollection.aggregate(
      // Stage 1
      {
        $match: {
          companyId: ObjectId(params.companyId),
          submitBy: ObjectId(params.submitBy),
          dcrDate: new Date(params.dcrDate)
          //geoLocation:{$ne:[]}
        }
      },
      // Stage 2
      {
        $lookup: {
          from: "Providers",
          localField: "providerId",
          foreignField: "_id",
          as: "proInfo"
        }
      },
      // Stage 3
      {
        $project: {
          punchDate: 1,
          providerId: 1,
          geoLocation: { $arrayElemAt: ["$geoLocation", 0] },
          providerName: { $arrayElemAt: ["$proInfo.providerName", 0] },
          typingAddress: { $arrayElemAt: ["$proInfo.address", 0] },
          providerType: { $arrayElemAt: ["$proInfo.providerType", 0] }
        }
      },
      function(err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: params,
            methodName: "getGeoCoordinates"
          });
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Dcrprovidervisitdetails.remoteMethod("getGeoCoordinates", {
    description: "Get LongLat data",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //---by preeti arora 16-10-20190----
  (Dcrprovidervisitdetails.deleteVisitedProvider = function(params, cb) {
    var self = this;
    var DcrproviderCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    let id = ObjectId(params.dcrId);
    DcrproviderCollection.remove({ dcrId: id });
    return cb(false, "done visit");
  }),
    Dcrprovidervisitdetails.remoteMethod("deleteVisitedProvider", {
      description: "delete visit provider",
      accepts: [
        {
          arg: "params",
          type: "object"
        }
      ],
      returns: {
        root: true,
        type: "array"
      },
      http: {
        verb: "get"
      }
    });
  //----------Get submit last POB on doctor--Preeti Arora-11-03-2020--------
  (Dcrprovidervisitdetails.getlastPOBOfProvider = function (params, cb) {    
    var self = this;
    var DcrproviderCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    let providerId = [];
    for (var i = 0; i < params.providerId.length; i++) {
      providerId.push(ObjectId(params.providerId[i]));
    }
    var match = {
      companyId: ObjectId(params.companyId),
      providerType: {
        $in: params.providerType
      },
      submitBy: ObjectId(params.submitBy),
      providerId: {
        $in: providerId
      },
    };    
    DcrproviderCollection.aggregate({
        $match: match,
      },
      // Stage 3
      {
        $group: {
          _id: {
            providerId: "$providerId",
          },
          lastPob: {
            $last: "$productPob",
          },
          dcrDate: {
            $last: "$dcrDate",
          },
          providerId: {
            $last: "$providerId",
          },
          productDetails: {
            $last: "$productDetails",
          },
        },
      },
      // Stage 4
      {
        $lookup: {
          from: "Providers",
          localField: "providerId",
          foreignField: "_id",
          as: "providerData",
        },
      }, {
        $unwind: {
          path: "$providerData",
          preserveNullAndEmptyArrays: true
        },
      }, {
        $lookup: {
          from: "Area",
          localField: "providerData.blockId",
          foreignField: "_id",
          as: "area",
        },
      },
      // Stage 5
      {
        $project: {
          _id: 0,
          providerName: "$providerData.providerName",
          providerType: "$providerData.providerType",
          area: {
            $arrayElemAt: ["$area.areaName", 0]
          }, // add area name
          providerId: "$_id.providerId",
          lastPob: 1,
          productDetails:1,
          dcrDate: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$dcrDate"
            }
          },
        },
      },
      function (err, result) {        
        let finalResult=[];
        if(result.length>0){
          result.forEach(element => {
          if(element.productDetails.length>0){
            let productDetail=[]
            element.productDetails.forEach( async prodRes => {
              await productDetail.push({productName:prodRes.productName,pob:prodRes.pob})
            });
            finalResult.push({
              lastPob:element.lastPob,
              productDetails:productDetail,
              providerId:element.providerId,
              dcrDate:element.dcrDate,
              providerName:element.providerName,
              providerType:element.providerType,
              areaName:element.area,

            })

          }
          else{
            finalResult.push({
              lastPob:element.lastPob,
              productDetails:[],
              providerId:element.providerId,
              dcrDate:element.dcrDate,
              providerName:element.providerName,
              providerType:element.providerType,
              areaName:element.area,

            })

          }
          
          });
          return cb(false, finalResult);
          
        }        
        if (err) {
          sendMail.sendMail({
            collectionName: "Dcrprovidervisitdetails",
            errorObject: err,
            paramsObject: params,
            methodName: "getlastPOBOfProvider",
          });
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
      }
    );
  }),
    Dcrprovidervisitdetails.remoteMethod("getlastPOBOfProvider", {
      description: "get last POB Of Provider ",
      accepts: [
        {
          arg: "params",
          type: "object"
        }
      ],
      returns: {
        root: true,
        type: "array"
      },
      http: {
        verb: "get"
      }
    });
  //---------------------------------By Preeti Arora---End--------------------------
   //----------------Preeti Arora 21-07-2020--------------
  // Provider Visit Detail report
  Dcrprovidervisitdetails.MEFTrackerReport = function (params, cb) {
    var self = this;
    var DcrprovidervisitdetailsCollection = self
      .getDataSource()
      .connector.collection(Dcrprovidervisitdetails.modelName);
    var aMatch = {};

    let includeUnlistedProviderMatch = {};
    if (params.unlistedInclude == true) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
        providerType: {
          $in: params.providerType
        },
        mefAnswers: {
          $exists: true
        },

        //   "providerStatus": "unlisted"
      };
    } else if (params.unlistedInclude == false) {
      includeUnlistedProviderMatch = {
        //"submitBy": { $in: userRecords[0].userIds },
        dcrDate: {
          $gte: new Date(params.fromDate),
          $lte: new Date(params.toDate),
        },
        providerType: {
          $in: params.providerType
        },
        providerStatus: {
          $ne: "unlisted"
        },

        mefAnswers: {
          $exists: true
        },
      };
      var categoryMatch = {
        category: "MEF"
      }
    }
    if (params.type == "State" || params.type == "Headquarter") {
      Dcrprovidervisitdetails.app.models.UserInfo.getAllUserIdsBasedOnType(
        params,
        function (err, userRecords) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Dcrprovidervisitdetails",
              errorObject: err,
              paramsObject: params,
              methodName: "MEFTrackerReport",
            });
          }

          if (userRecords.length > 0) {
            includeUnlistedProviderMatch.submitBy = {
              $in: userRecords[0].userIds,
            };

            DcrprovidervisitdetailsCollection.aggregate(
              // Stage 1
              {
                $match: includeUnlistedProviderMatch,
              },

              // Stage 2
              {
                $group: {
                  _id: {
                    userId: "$submitBy",
                    stateId: "$stateId",
                    districtId: "$districtId",
                    dcrDate: "$dcrDate",
                    providerId: "$providerId",
                  },

                  visitDates: {
                    $addToSet: {
                      $dateToString: {
                        format: "%d-%m-%Y",
                        date: "$dcrDate"
                      },
                    },
                  },
                  mefAnswers: {
                    $addToSet: "$mefAnswers",
                  },
                  providerRemarks: {
                    $push: "$remarks",
                  },
                },
              },

              // Stage 3
              {
                $lookup: {
                  from: "State",
                  localField: "_id.stateId",
                  foreignField: "_id",
                  as: "stateInfo",
                },
              },

              // Stage 4
              {
                $lookup: {
                  from: "District",
                  localField: "_id.districtId",
                  foreignField: "_id",
                  as: "district",
                },
              },

              // Stage 5
              {
                $lookup: {
                  from: "Providers",
                  localField: "_id.providerId",
                  foreignField: "_id",
                  as: "providerDetail",
                },
              },

              // Stage 6
              {
                $unwind: {
                  path: "$providerDetail",
                  preserveNullAndEmptyArrays: true,
                },
              },

              // Stage 7
              {
                $lookup: {
                  from: "Area",
                  localField: "providerDetail.blockId",
                  foreignField: "_id",
                  as: "blockDetail",
                },
              },

              // Stage 8
              {
                $lookup: {
                  from: "UserInfo",
                  localField: "_id.userId",
                  foreignField: "userId",
                  as: "userDetails",
                },
              },

              // Stage 9
              {
                $unwind: "$userDetails",
              },

              // Stage 10
              {
                $match: {},
              },

              // Stage 11
              {
                $unwind: {
                  path: "$userDetails.divisionId",
                  preserveNullAndEmptyArrays: true,
                },
              },

              // Stage 12
              {
                $lookup: {
                  from: "DivisionMaster",
                  localField: "userDetails.divisionId",
                  foreignField: "_id",
                  as: "division",
                },
              },

              // Stage 13
              {
                $project: {
                  _id: 0,
                  providerName: "$providerDetail.providerName",
                  category: "$providerDetail.category",
                  address: "$providerDetail.address",
                  phone: "$providerDetail.phone",
                  status: "$providerDetail.status",
                  userName: "$userDetails.name",
                  providerType: "$providerDetail.providerType",
                  stateName: {
                    $arrayElemAt: ["$stateInfo.stateName", 0]
                  },
                  districtName: {
                    $arrayElemAt: ["$district.districtName", 0]
                  },
                  blockName: {
                    $arrayElemAt: ["$blockDetail.areaName", 0]
                  },
                  focusProduct: "$providerDetail.focusProduct",
                  visitDates: 1,
                  productDiscussed: {
                    $arrayElemAt: ["$mefAnswers.productDiscussed", 0],
                  },
                  competitorProduct: {
                    $arrayElemAt: ["$mefAnswers.competitorProduct", 0],
                  },
                  drProductProblem: {
                    $arrayElemAt: ["$mefAnswers.drProductProblem", 0],
                  },
                  problemSolnToDr: {
                    $arrayElemAt: ["$mefAnswers.problemSolnToDr", 0],
                  },
                  doctorFeedback: {
                    $arrayElemAt: ["$mefAnswers.doctorFeedback", 0],
                  },
                  isQuerySolnGiven: {
                    $arrayElemAt: ["$mefAnswers.isQuerySolnGiven", 0],
                  },
                  querySoln: {
                    $arrayElemAt: ["$mefAnswers.querySoln", 0]
                  },
                  seniorHelp: {
                    $arrayElemAt: ["$mefAnswers.seniorHelp", 0]
                  },
                  conversionDone: {
                    $arrayElemAt: ["$mefAnswers.conversionDone", 0],
                  },
                  conversionNoReason: {
                    $arrayElemAt: ["$mefAnswers.conversionNoReason", 0],
                  },
                  quoteGiven: {
                    $arrayElemAt: ["$mefAnswers.quoteGiven", 0]
                  },
                  quoteStatus: {
                    $arrayElemAt: ["$mefAnswers.quoteStatus", 0]
                  },
                  quoteOthers: {
                    $arrayElemAt: ["$mefAnswers.quoteOthers", 0]
                  },
                  divisionName: {
                    $arrayElemAt: ["$division.divisionName", 0]
                  },
                },
              }, {
                $match: {
                  category: "MEF"
                },
              },
              // Stage 14
              {
                $sort: {
                  providerName: 1,
                },
              },
              function (err, res) {
                let finalResult = [];

                if (err) {
                  console.log(err);
                } else {
                  if (res.length > 0) {
                    res.forEach(element => {
                      async.series({
                          focusProduct: function (callback) {
                            if (element.hasOwnProperty("focusProduct") === true) {
                              if (element.focusProduct.length > 0) {
                                Dcrprovidervisitdetails.app.models.Products.getFocusProducts({
                                    productIds: element.focusProduct
                                  },
                                  function (err, res) {
                                    if (res.length > 0) {
                                      callback(null, res[0].products);
                                    } else {
                                      callback(null, []);
                                    }
                                  }
                                );
                              } else {
                                callback(null, []);
                              }
                            } else {
                              callback(null, []);
                            }
                          },
                          productDiscussed: function (callback) {
                            if ( element.hasOwnProperty("productDiscussed") ===  true) {
                              if (element.productDiscussed.length > 0) {
                                Dcrprovidervisitdetails.app.models.Products.find({
                                  where: {
                                    companyId: params.companyId,
                                    _id: {
                                      inq: element.productDiscussed
                                    },
                                  },
                                },
                                function (err, ProResponse) {
                                  if (err) {
                                    console.log(err);
                                  } else {
                                    if (ProResponse.length > 0) {
                                      let productName = [];
                                      ProResponse.forEach((productResult,index,arr) => {
                                        productName.push(productResult.productName);
                                        if (Object.is(arr.length - 1, index)) {
                                          callback(null, productName);                                        }
                                      });
                                      
                                      // element["productName"] = productName;
                                      
                                    }else{
                                      callback(null, []);
                                    }
                                  }

                                } );
                              } else {
                                callback(null, []);
                              }
                            } else {
                              callback(null, []);
                            }
                          }
                        },
                        function (err, results) {                         
                          finalResult.push({
                            ...element,
                            focusProduct: results.focusProduct.toString(),
                            productName: results.productDiscussed.toString()
                          });
                          if (res.length === finalResult.length) {
                            return cb(false, finalResult);
                          }
                        }
                      );
                    });


                    //////////////////========
                  } else {
                    return cb(false, []);
                  }
                }
              }
            );
          } else {
            cb(false, []);
          }
        }
      );
    } 
    else {
      var userIds = [];
      for (var i = 0; i < params.userIds.length; i++) {
        userIds.push(ObjectId(params.userIds[i]));
      }
      includeUnlistedProviderMatch.submitBy = {
        $in: userIds,
      };
      DcrprovidervisitdetailsCollection.aggregate(
        // Stage 1
        {
          $match: includeUnlistedProviderMatch,
        },

        // Stage 2
        {
          $group: {
            _id: {
              userId: "$submitBy",
              stateId: "$stateId",
              districtId: "$districtId",
              dcrDate: "$dcrDate",
              providerId: "$providerId",
            },

            visitDates: {
              $addToSet: {
                $dateToString: {
                  format: "%d-%m-%Y",
                  date: "$dcrDate"
                },
              },
            },
            mefAnswers: {
              $addToSet: "$mefAnswers",
            },
            providerRemarks: {
              $push: "$remarks",
            },
          },
        },

        // Stage 3
        {
          $lookup: {
            from: "State",
            localField: "_id.stateId",
            foreignField: "_id",
            as: "stateInfo",
          },
        },

        // Stage 4
        {
          $lookup: {
            from: "District",
            localField: "_id.districtId",
            foreignField: "_id",
            as: "district",
          },
        },

        // Stage 5
        {
          $lookup: {
            from: "Providers",
            localField: "_id.providerId",
            foreignField: "_id",
            as: "providerDetail",
          },
        },

        // Stage 6
        {
          $unwind: {
            path: "$providerDetail",
            preserveNullAndEmptyArrays: true,
          },
        },

        // Stage 7
        {
          $lookup: {
            from: "Area",
            localField: "providerDetail.blockId",
            foreignField: "_id",
            as: "blockDetail",
          },
        },

        // Stage 8
        {
          $lookup: {
            from: "UserInfo",
            localField: "_id.userId",
            foreignField: "userId",
            as: "userDetails",
          },
        },

        // Stage 9
        {
          $unwind: "$userDetails",
        },

        // Stage 10
        {
          $match: {},
        },

        // Stage 11
        {
          $unwind: {
            path: "$userDetails.divisionId",
            preserveNullAndEmptyArrays: true,
          },
        },

        // Stage 12
        {
          $lookup: {
            from: "DivisionMaster",
            localField: "userDetails.divisionId",
            foreignField: "_id",
            as: "division",
          },
        },

        // Stage 13
        {
          $project: {
            _id: 0,
            providerName: "$providerDetail.providerName",
            category: "$providerDetail.category",
            address: "$providerDetail.address",
            phone: "$providerDetail.phone",
            status: "$providerDetail.status",
            userName: "$userDetails.name",
            providerType: "$providerDetail.providerType",
            stateName: {
              $arrayElemAt: ["$stateInfo.stateName", 0]
            },
            districtName: {
              $arrayElemAt: ["$district.districtName", 0]
            },
            blockName: {
              $arrayElemAt: ["$blockDetail.areaName", 0]
            },
            visitDates: 1,
            productDiscussed: {
              $arrayElemAt: ["$mefAnswers.productDiscussed", 0],
            },
            competitorProduct: {
              $arrayElemAt: ["$mefAnswers.competitorProduct", 0],
            },
            drProductProblem: {
              $arrayElemAt: ["$mefAnswers.drProductProblem", 0],
            },
            problemSolnToDr: {
              $arrayElemAt: ["$mefAnswers.problemSolnToDr", 0],
            },
            doctorFeedback: {
              $arrayElemAt: ["$mefAnswers.doctorFeedback", 0],
            },
            isQuerySolnGiven: {
              $arrayElemAt: ["$mefAnswers.isQuerySolnGiven", 0],
            },
            querySoln: {
              $arrayElemAt: ["$mefAnswers.querySoln", 0]
            },
            seniorHelp: {
              $arrayElemAt: ["$mefAnswers.seniorHelp", 0]
            },
            conversionDone: {
              $arrayElemAt: ["$mefAnswers.conversionDone", 0],
            },
            conversionNoReason: {
              $arrayElemAt: ["$mefAnswers.conversionNoReason", 0],
            },
            quoteGiven: {
              $arrayElemAt: ["$mefAnswers.quoteGiven", 0]
            },
            quoteStatus: {
              $arrayElemAt: ["$mefAnswers.quoteStatus", 0]
            },
            quoteOthers: {
              $arrayElemAt: ["$mefAnswers.quoteOthers", 0]
            },
            divisionName: {
              $arrayElemAt: ["$division.divisionName", 0]
            },
          },
        }, {
          $match: {
            category: "MEF"
          },
        },

        // Stage 14
        {
          $sort: {
            providerName: 1,
          },
        },
        function (err, res) {
          let finalResult = [];

          if (err) {
            console.log(err);
          } else {
            if (res.length > 0) {
              res.forEach(element => {
                async.series({
                    focusProduct: function (callback) {
                      if (element.hasOwnProperty("focusProduct") === true) {
                        if (element.focusProduct.length > 0) {
                          Dcrprovidervisitdetails.app.models.Products.getFocusProducts({
                              productIds: element.focusProduct
                            },
                            function (err, res) {
                              if (res.length > 0) {
                                callback(null, res[0].products);
                              } else {
                                callback(null, []);
                              }
                            }
                          );
                        } else {
                          callback(null, []);
                        }
                      } else {
                        callback(null, []);
                      }
                    },
                    productDiscussed: function (callback) {
                      if ( element.hasOwnProperty("productDiscussed") ===  true) {
                        if (element.productDiscussed.length > 0) {
                          Dcrprovidervisitdetails.app.models.Products.find({
                            where: {
                              companyId: params.companyId,
                              _id: {
                                inq: element.productDiscussed
                              },
                            },
                          },
                          function (err, ProResponse) {
                            if (err) {
                              console.log(err);
                            } else {
                              if (ProResponse.length > 0) {
                                let productName = [];
                                ProResponse.forEach((productResult,index,arr) => {
                                  productName.push(productResult.productName);
                                  if (Object.is(arr.length - 1, index)) {
                                    callback(null, productName);                                        }
                                });
                                
                                // element["productName"] = productName;
                                
                              }else{
                                callback(null, []);
                              }
                            }

                          } );
                        } else {
                          callback(null, []);
                        }
                      } else {
                        callback(null, []);
                      }
                    }
                  },
                  function (err, results) {
                    finalResult.push({
                      ...element,
                      focusProduct: results.focusProduct.toString(),
                      productName: results.productDiscussed.toString()
                    });
                    if (res.length === finalResult.length) {
                      return cb(false, finalResult);
                    }
                  }
                );
              });


              //////////////////========
            } else {
              return cb(false, []);
            }
          }
        }
      );

    }
  };
  Dcrprovidervisitdetails.remoteMethod("MEFTrackerReport", {
    description: "Date wise DCR details",
    accepts: [{
      arg: "params",
      type: "object",
    }, ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  //------------------end--------------
//------------get expense details on the basis of block visited--Preeti Arora-15-09-2020------
Dcrprovidervisitdetails.getVisitedAreaAsPerProviderVisit = function (params, cb) {
var self = this;
var DcrproviderCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.modelName);
let where={
companyId:ObjectId(params.companyId),
submitBy:ObjectId(params.submitBy),
dcrId:ObjectId(params.dcrId)
}
DcrproviderCollection.aggregate(
// Stage 1
{
$match: where
},

// Stage 2
{
$group: {
_id: {
blockId: "$blockId",
submitBy: "$submitBy",
},
punchDate: {
$addToSet: "$punchDate"
},
dcrId: {
$addToSet: "$dcrId"
}

}
},

// Stage 3
{
$unwind: "$dcrId"
},

// Stage 4
{
$lookup: {
"from": "DCRMaster",
"localField": "dcrId",
"foreignField": "_id",
"as": "dcrData"
}
}, {
$lookup: {
"from" : "Area",
"localField" : "_id.blockId",
"foreignField" : "_id",
"as" : "area"
}
},

// Stage 5
{
$project: {
blockId: "$_id.blockId",
submitBy: "$_id.submitBy",
blockName:{$arrayElemAt:["$area.areaName",0]},

punchDate: { $arrayElemAt: ["$punchDate", 0] },
visitedBlock: { $arrayElemAt: ["$dcrData.visitedBlock", 0] },
jointWork: { $arrayElemAt: ["$dcrData.jointWork", 0] },
workingStatus: { $arrayElemAt: ["$dcrData.workingStatus", 0] },
dcrDate: { $arrayElemAt: ["$dcrData.dcrDate", 0] },
companyId: { $arrayElemAt: ["$dcrData.companyId", 0] },
remarks: { $arrayElemAt: ["$dcrData.remarks", 0] },
stateId: { $arrayElemAt: ["$dcrData.stateId", 0] },
districtId: { $arrayElemAt: ["$dcrData.districtId", 0] },

}
},

// Stage 6
{
$sort: {
punchDate: 1
}
},function(err,dcrObj){
let visitedBlock=[];
if(err){
console.log(err);
}
if(dcrObj.length>0){
if (dcrObj.length == 1) {
visitedBlock.push({
"fromArea": dcrObj[0].blockName,
"toArea": dcrObj[0].blockName
})
} else {
for (var i = 0; i < dcrObj.length - 1; i++) {
console.log();
if (i == dcrObj.length - 1) {
visitedBlock.push({
"fromArea": dcrObj[i - 1].blockName,
"toArea": dcrObj[i].blockName
})
} else {
visitedBlock.push({
"fromArea": dcrObj[i].blockName,
"toArea": dcrObj[i + 1].blockName
})
}
}
}
}else{
return cb(false,[])

}

return cb(false,visitedBlock)

}
);

},
Dcrprovidervisitdetails.remoteMethod("getVisitedAreaAsPerProviderVisit", {
description: "get Visited Area As Per ProviderVisit ",
accepts: [
{
arg: "params",
type: "object"
}
],
returns: {
root: true,
type: "array"
},
http: {
verb: "get"
}
});
//---------------------by preeti arora end----------------------
  
  //------------get expense details on the basis of block visited--Preeti Arora-15-09-2020------
  Dcrprovidervisitdetails.preCallAnalysisReport = function (params, cb) {
    var self = this;
    var DcrproviderCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.modelName);
    let providerId=[];
    params.providerId.forEach((item)=>{
      providerId.push(ObjectId(item))
    })
    let where={
      companyId:ObjectId(params.companyId),
      submitBy:ObjectId(params.submitBy),
      providerId:{$in:providerId},
      providerType:params.providerType
    }
    DcrproviderCollection.aggregate(

        // Stage 1
        {
          $match:where
        },
    
        // Stage 2
        {
          $group: {
          _id:{providerId:"$providerId",submitBy:"$submitBy"},
          providerId:{
          $last:"$providerId"
          },
          dcrDate:{
          $last:"$dcrDate"
          },
          providerType:{
          $last:"$providerType"
          },
          callObjective:{
          $last:"$callObjective"
          },
          callOutcomes:{
          $last:"$callOutcomes"
          },
          productDetails:{
          $last:"$productDetails"
          },
          giftDetails:{
          $last:"$giftDetails"
          },
          remarks:{
          $last:"$remarks"
          },
          productPob:{
          $last:"$productPob"
          },
          fileId:{
          $last:"$fileId"
          }
          
          
          
          }
        },
    
        // Stage 3
        {
          $lookup: {
              "from" : "Providers",
              "localField" : "providerId",
              "foreignField" : "_id",
              "as" : "providerData"
          }
        },
    
        // Stage 4
        {
          $unwind: "$providerData"
        },
    
        // Stage 5
        {
          $lookup: {
              "from" : "Area",
              "localField" : "providerData.blockId",
              "foreignField" : "_id",
              "as" : "areaData"
          }
        },
    
        // Stage 6
        {
          $project: {
          providerName:"$providerData.providerName",
          category:"$providerData.category",
          providerDegree:"$providerData.degree",
          dcrDate:1,
          providerArea:{$arrayElemAt:["$areaData.areaName",0]},
          providerType:1,
          callObjective:1,
          callOutcomes:1,
          productDetails:1,
          giftDetails:1,
          remarks:1,
          productPob:1,
          fileId:1,
          providerId:1,
          
          }
        },function(err,result){
          if(err){
            return cb(err)
          }
          if(result.length>0){
            return cb(false,result)
          }else{
            return cb(false,[])
          }
        }
    
    
    );
    
    
   
    

  },
  Dcrprovidervisitdetails.remoteMethod("preCallAnalysisReport", {
    description: "Pre Call Analysis Report",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //---------------------by preeti arora end----------------------
 
 
  Dcrprovidervisitdetails.getProductWisePOBDetails = function (params, cb) {
    var self = this;
	let finalpob = []; 
    var DcrprovidervisitdetailsCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.modelName);
     DcrprovidervisitdetailsCollection.aggregate(
      // Stage 1
      {
        $match: {
          "companyId" : ObjectId(params.companyId),
          "submitBy" : ObjectId(params.submitBy),
          "dcrDate": {
            $gte: new Date(params.fromDate),
            $lte: new Date(params.toDate),
          },
          "productDetails.productId" : {$exists : true} 
        },
      },
    // Stage 2
    {
      $unwind: {
                path: "$productDetails",
                preserveNullAndEmptyArrays: true,
              }
    },

    // Stage 3
    {
      $group: {
                _id: {
                  userId: "$submitBy",
                  productId : "$productDetails.productId",
                  productName : "$productDetails.productName",
                },
				qty :{
                  $sum: "$productDetails.productQuantity",
                }, 
                Totpob: {
                  $sum: "$productDetails.pob",
                }
              
            }
    },

    // Stage 4
    {
      $lookup: {
          "from" : "UserInfo",
          "localField" : "_id.userId",
          "foreignField" : "userId",
          "as" : "userDetails"
      }
    },

    // Stage 5
    {
      $unwind: "$userDetails"
    },

    // Stage 6
    {
      $project: {
        userName : "$userDetails.name",
      productId : "$_id.productId",
      productName : "$_id.productName",
	  productQty : "$qty",
      pob : "$Totpob"
      }
  },
  function (err, result) {
	if (err) {
        sendMail.sendMail({
        collectionName: "Dcrprovidervisitdetails",
        errorObject: err,
        paramsObject: params,
        methodName: "Dcrprovidervisitdetails.getProductWisePOBDetails"
       });
        return cb(err);
        }
		
	if (result.length > 0) {
		 Dcrprovidervisitdetails.app.models.Products.find({
                            where: {
                              companyId: params.companyId
                            },
                          },function (err, ProResponse) { 
                            for (let i = 0; i < ProResponse.length; i++) {
                              let matchedObj = result.filter(function (obj) {
                                let prodId1 = ProResponse[i].id.toString();
                                   let prodId =  obj._id.productId.toString();
                                   return prodId === prodId1;
                                 });
							
								 if (matchedObj.length > 0) {
									 finalpob.push({
										productId : matchedObj[0]._id.productId,
										productName : matchedObj[0].productName,
										productQty :matchedObj[0].productQty,
										productPrice : ProResponse[i].mrp,
										pob :matchedObj[0].pob,
										  
									 })
									 
								 }else{
									 finalpob.push({
										productId : ProResponse[i].id,
										productName : ProResponse[i].productName,
										productQty : 0,//result[i].productQty,
										productPrice : ProResponse[i].mrp,
										pob :0,
									 })
								 }
							}
              return cb(false, finalpob)
						  });
	}
     //return cb(false, result);
    })
    
  };

  Dcrprovidervisitdetails.remoteMethod("getProductWisePOBDetails", {
    description: "Get Product Wise POB Details",
    accepts: [{
      arg: "params",
      type: "object",
    },],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });

  Dcrprovidervisitdetails.getProviderSampleAndGiftDetails = function (params, cb) {
    var self = this;
    var DcrprovidervisitdetailsCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.modelName);
    let match={
      companyId:ObjectId(params.companyId),
      submitBy:ObjectId(params.submitBy),
      providerId:ObjectId(params.providerId),
      dcrDate:{$gte:new Date(params.fromDate),$lte:new Date(params.toDate)}
    }
    DcrprovidervisitdetailsCollection.aggregate(
        // Stage 1
        {
          $match: match
        },
    
        // Stage 2
        {
          $group: {
             _id: {
                        userId: "$submitBy",
                        stateId: "$stateId",
                        districtId: "$districtId",
                      },
                      totalVisit: {
                        $sum: 1,
                      },
                     
                      productDetails: {
                        $push: "$productDetails",
                      },
                      giftDetails: {
                        $push: "$giftDetails",
                      },
                       jointWorkWithId: {
                        $push: "$jointWorkWithId",
                      },
                     
          }
        },async function (err,res) {
          if(err){
            return cb(err)
          }
          if(res.length>0){
            let userIds=[];
            let data=[];
            res[0].jointWorkWithId.forEach(async(joint) => {
              if(joint.length>0){
                joint.forEach(async(jointId) => {
                  userIds.push(jointId.id)
                });
              }

            });
            let obj={
              where:{
                userId:{inq:userIds}
              }
            }
          let users=await  Dcrprovidervisitdetails.app.models.UserInfo.find(obj);
          res[0]["mgrData"]=users;
          // if(users.length>0){
          //   const group = _.groupBy(users, "designation");
          // }
          
            return cb(false,res)
          }else{
            return cb(false,[])
          }
        }
    
        
    );
    
    
  };

  Dcrprovidervisitdetails.remoteMethod("getProviderSampleAndGiftDetails", {
    description: "Get Provider Sample And GiftDetails",
    accepts: [{
      arg: "params",
      type: "object",
    },],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  //------------------get sample distirbuted, Issued and balance details by preeti arora 15-03-2021------------
  Dcrprovidervisitdetails.getSampleIssueDistributedBalanceDetails = function (params, cb) {
    var self = this;
    var DcrprovidervisitdetailsCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.modelName);
    var sampleCollection = self.getDataSource().connector.collection(Dcrprovidervisitdetails.app.models.SampleGiftIssueDetail.modelName);

    let userIds=[];
    params.userId.forEach(element => {
       userIds.push(ObjectId(element));
     });
    let match={
      companyId:ObjectId(params.companyId),
      dcrDate:{$gte:new Date(params.fromDate),$lte:new Date(params.toDate)},
      submitBy:{$in:userIds}
    }
    let matchForIssued={
      companyId:ObjectId(params.companyId),
      userId:{$in:userIds},
      type:params.type
    }
    let unwind="";
    let group={};
    if(params.type=="sample"){
        match["productDetails"]={$ne:[]};
        unwind="$productDetails";
        group={
          _id:{
          "productName":"$productDetails.productName",
          "productId":"$productDetails.productId",
          submitBy:"$submitBy"
          },
          sample: {
          $sum: "$productDetails.productQuantity"
          }
          }

    }else{
      match["giftDetails"]={$ne:[]};
      unwind="$giftDetails";
      group={
        _id:{
        "productName":"$giftDetails.giftName",
        "productId":"$giftDetails.giftId",
        submitBy:"$submitBy"
        },
        sample: {
        $sum: "$giftDetails.giftQty"
        }
        }
    }
    async.parallel({
      distributed:(cb)=>{
        DcrprovidervisitdetailsCollection.aggregate(
            // Stage 1
            {
              $match:match
            },
        
            // Stage 2
            {
              $unwind: unwind
            },
        
            // Stage 3
            {
              $group: group
            },
        
            // Stage 4
            {
              $project: {
              productId:"$_id.productId",
              productName:"$_id.productName",
              userId:"$_id.submitBy",
              distributed:"$sample"
              }
            },
            {
              $match: {
              distributed:{$ne:0}
              }
            },
            (err,res)=>{
              if (err) return cb(err)
              if (res.length > 0) return cb(false, res)
              else {
                  return cb(false, [])
              }
            }      
        
        );
        
      },
      issueAndBalance:(cb)=>{
        sampleCollection.aggregate(
            {
              $match: matchForIssued
            },
        
            // Stage 2
            {
              $sort: {
              issueDate: 1,
              }
            },
        
            // Stage 3
            {
              $group: {
                _id: {
                          userId: '$userId',
                          productId:"$productId"
                        },
                        balance: {
                          $last: '$balance',
                        },
                        receipt:{
                        $last: '$receipt',
                        },
                        opening:{
                          $last: '$opening',
                          },
                        rowId: {
                          $last: '$_id',
                        },
                        
              }
            },
        
            // Stage 4
            {
              $project: {
                      productId:"$_id.productId",
                      userId:"$_id.userId",
                         receipt:1,
                          balance:1,
                          opening:1,
                          rowId:1,
              }
            },
        
            // Stage 5
            {
              $lookup: {
                  "from" : "UserInfo",
                  "localField" : "userId",
                  "foreignField" : "userId",
                  "as" : "userDetails"
              }
            },
        
            // Stage 6
            {
              $unwind: "$userDetails"
            },
        
            // Stage 7
            {
              $lookup: {
                  "from" : "District",
                  "localField" : "userDetails.districtId",
                  "foreignField" : "_id",
                  "as" : "dis"
              }
            },
        
            // Stage 8
            {
              $project: {
              userId:1,
              productId:1,
              balance:1,
              opening:1,
              receipt:1,
              rowId:1,
              userName:"$userDetails.name",
              district:{$arrayElemAt:["$dis.districtName",0]}
              }
            } , (err,res)=>{
              if (err) return cb(err)
              if (res.length > 0) return cb(false, res)
              else {
                  return cb(false, [])
              }
            } 
         
              
        );
        
      }
    },(err,asyncRes)=>{
      if(err){
        return cb(err)
      }
      else{
        if(asyncRes.issueAndBalance.length>0){
          let finalRes=[];
          asyncRes.issueAndBalance.forEach(issueSample => {
            let matchedObj=asyncRes.distributed.filter(obj=>{
               return (issueSample.productId.toString() == obj.productId.toString()) && (issueSample.userId.toString() == obj.userId.toString())
            });
            if(matchedObj.length>0){
              finalRes.push({
                username:issueSample.userName,
                district:issueSample.district,
                distributed:matchedObj[0].distributed,
                receipt:issueSample.receipt + issueSample.opening,
                balance:issueSample.balance,
              })
            }else{
              finalRes.push({
                username:issueSample.userName,
                district:issueSample.district,
                distributed:0,
                receipt:issueSample.receipt + issueSample.opening,
                balance:issueSample.balance,
              })
            }
            
          });
          return cb(false,finalRes)
        }else{
          return cb(false,[])
        }
      }

    })
   
    
    
  };

  Dcrprovidervisitdetails.remoteMethod("getSampleIssueDistributedBalanceDetails", {
    description: "Get Sample Issue Distributed Balance Details",
    accepts: [{
      arg: "params",
      type: "object",
    },],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });

  //-------------end -------------------------------------------------------------------------------
 
 
};
