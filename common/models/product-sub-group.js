"use strict";
var ObjectId = require("mongodb").ObjectID;

module.exports = function (Productsubgroup) {
  Productsubgroup.createAssignProductGroups = function (params, cb) {
    let selectedStates = [];
    let selectedProducts = [];
    let selectedDivision = [];

    for (var i = 0; i < params.stateInfo.length; i++) {
      selectedStates.push(ObjectId(params.stateInfo[i]));
    }

    for (var i = 0; i < params.productInfo.length; i++) {
      selectedProducts.push(ObjectId(params.productInfo[i].id));
    }

    let match = {};
    if (params.divisionId != null) {
      for (var i = 0; i < params.divisionId.length; i++) {
        selectedDivision.push(ObjectId(params.divisionId[i]));
      }
      match = {
        companyId: ObjectId(params.companyId),
        productSubGroupName: params.productSubGroupName,
        stateId: { inq: selectedStates },
        divisionId: selectedDivision,
      };
    } else {
      match = {
        companyId: ObjectId(params.companyId),
        stateId: { inq: selectedStates },
        productSubGroupName: params.productSubGroupName,
      };
    }

    Productsubgroup.find(
      {
        where: match,
      },
      function (err, response) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Productsubgroup",
            errorObject: err,
            paramsObject: params,
            methodName: "createProductSubGroups",
          });
        }
        if (response.length > 0) {
          var err = new Error("Sub Group Name is Already Exists");
          console.log("err ; ", err);
          err.statusCode = 409;
          err.code = "Validation failed";
          return cb(err);
        } else {
          let finalObj = [];
          if (params.divisionId != null) {
            params.divisionId.forEach((div) => {
              params.stateInfo.forEach((state) => {
                let prodetails = [];
                let GroupObj = {};
                params.productInfo.forEach((products) => {
                  prodetails.push(ObjectId(products.id));
                });
                GroupObj = {
                  companyId: ObjectId(params.companyId),
                  productSubGroupName: params.productSubGroupName,
                  stateId: ObjectId(state),
                  groupId: ObjectId(params.groupInfo.groupId),
                  productInfo: prodetails,
                  divisionId: ObjectId(div),
                  createdAt: new Date(),
                  updatedAt: new Date(),
                };
                finalObj.push(GroupObj);
              });
            });
          } else {
            params.stateInfo.forEach((state) => {
              let prodetails = [];
              let GroupObj = {};
              params.productInfo.forEach((products) => {
                prodetails.push(ObjectId(products.id));
              });
              GroupObj = {
                companyId: ObjectId(params.companyId),
                productSubGroupName: params.productSubGroupName,
                stateId: ObjectId(state),
                groupId: ObjectId(params.groupInfo.groupId),
                productInfo: prodetails,
                createdAt: new Date(),
                updatedAt: new Date(),
              };
              finalObj.push(GroupObj);
            });
          }

          Productsubgroup.create(finalObj, function (err, res) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Productsubgroup",
                errorObject: err,
                paramsObject: params,
                methodName: "createAssignProductGroups",
              });
              //console.log(err);
              return cb(err);
            }
            return cb(false, res);
          });
        }
      }
    );
  };
  Productsubgroup.remoteMethod("createAssignProductGroups", {
    description: "Create Assign Product To Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });

  Productsubgroup.getProductSubGroupDetails = function (params, cb) {
    var self = this;
    var viewProductGroupCollection = this.getDataSource().connector.collection(
      Productsubgroup.modelName
    );
    let match = {
      companyId: ObjectId(params.companyId),
    };
    // if(params.hasOwnProperty("stateId")){
    //   match.stateId=ObjectId(params.stateId)
    // }
    viewProductGroupCollection.aggregate(
      // Stage 1
      {
        $match: match,
      },

      // Stage 2
      {
        $group: {
          _id: {
            stateId: "$stateId",
            id: "$_id",
            productSubGroupName: "$productSubGroupName",
            groupId:"$groupId",
            divisionId: "$divisionId",
            productInfo: "$productInfo",
          },
        },
      },

      // Stage 3
      {
        $lookup: {
          from: "State",
          localField: "_id.stateId",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      // Stage 4
      {
        $unwind: "$_id.productInfo",
      },
      // Stage 5
      {
        $lookup: {
          from: "Products",
          localField: "_id.productInfo",
          foreignField: "_id",
          as: "products",
        },
      },
      // Stage 6
      {
        $lookup: {
          from: "DivisionMaster",
          localField: "_id.divisionId",
          foreignField: "_id",
          as: "divisions",
        },
      },
      // Stage 6
      {
        $lookup: {
          from: "Assignproducttogroup",
          localField: "_id.groupId",
          foreignField: "_id",
          as: "grouopDetails",
        },
      },

      // Stage 7
      {
        $project: {
          id: "$_id.id",
          stateId: { $arrayElemAt: ["$stateDetails._id", 0] },
          stateName: { $arrayElemAt: ["$stateDetails.stateName", 0] },
          divisionName: { $arrayElemAt: ["$divisions.divisionName", 0] },
          divisionId: { $arrayElemAt: ["$divisions._id", 0] },
          prodGroupName: { $arrayElemAt: ["$grouopDetails.productGroupName", 0] },
          prodGroupId: { $arrayElemAt: ["$grouopDetails._id", 0] },
          productSubGroupName: "$_id.productSubGroupName",
          productId: { $arrayElemAt: ["$products._id", 0] },
          productName: { $arrayElemAt: ["$products.productName", 0] },
          productCode: { $arrayElemAt: ["$products.productCode", 0] },
          productType: { $arrayElemAt: ["$products.productType", 0] },
          ptw: { $arrayElemAt: ["$products.ptw", 0] },
          ptr: { $arrayElemAt: ["$products.ptr", 0] },
          mrp: { $arrayElemAt: ["$products.mrp", 0] },
        },
      }, 
 
      // Stage 7
      {
        $group: {
          _id: {
            stateId: "$stateId",
            stateName: "$stateName",
            divisionName: "$divisionName",
            divisionId: "$divisionId",
            groupId: "$id",
            prodGroupId:"$prodGroupId",
            prodGroupName : "$prodGroupName",
            productSubGroupName: "$productSubGroupName",
          },
          products: {
            $addToSet: {
              id: "$productId",
              productName: "$productName",
              productCode: "$productCode",
              productType: "$productType",
              ptw: "$ptw",
              ptr: "$ptr",
              mrp: "$mrp",
            },
          },
        },
      },

      // Stage 8
      {
        $project: {
          _id: 0,
          groupId: "$_id.groupId",
          companyId: "$_id.companyId",
          stateId: "$_id.stateId",
          stateName: "$_id.stateName",
          divisionName: "$_id.divisionName",
          divisionId: "$_id.divisionId",
          productGroupId:"$_id.prodGroupId",
          productGroupName:"$_id.prodGroupName",
          productSubGroupName: "$_id.productSubGroupName",
          ProductInfo: "$products",
        },
      },
      // Options
      {
        cursor: {
          batchSize: 50,
        },

        allowDiskUse: true,
      },
      function (err, result) {
        
        if (err) {
          return cb(err);
        }
        return cb(false, result);
      }
    );  
  };

  Productsubgroup.remoteMethod("getProductSubGroupDetails", { 
    description: "Get Product Groups Based on States",
    accepts: [ 
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });

  Productsubgroup.deleteProductSubGroup = function (params, cb) {
    var self = this;
    Productsubgroup.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          _id: ObjectId(params.groupId),
        },
      },
      function (err, response) {
        Productsubgroup.destroyById(params.groupId, function (err) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Productsubgroup",
              errorObject: err,
              paramsObject: params,
              methodName: "deleteProductSubGroup",
            });
            // console.log(err);
          }
          return cb(false, "deleted");
        });
      }
    );
  };

  Productsubgroup.remoteMethod("deleteProductSubGroup", {
    description: "Delete Product Sub Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });

  Productsubgroup.removeProductFromSubGroup = function (params, cb) {
   
    var self = this;
    var groupsCollection = this.getDataSource().connector.collection(
      Productsubgroup.modelName
    );
    let findObj = {
      where: {
        companyId: ObjectId(params.companyId),
        stateId: ObjectId(params.stateId),
        productSubGroupName: params.productSubGroupName,
      },
    };
    Productsubgroup.find(findObj, function (err, res) {
      if (err) {
        console.log(err);
      }
      if (res.length > 0) {
        if (res[0].productInfo != null) {
          res[0].productInfo.forEach((item, index) => {
            if (item == params.productId) {
              res[0].productInfo.splice(index, 1);
              let where = {
                companyId: ObjectId(params.companyId),
                stateId: ObjectId(params.stateId),
                productSubGroupName: params.productSubGroupName,
              };
              let update = {
                productInfo: res[0].productInfo,
              };
              groupsCollection.update(
                where,
                {
                  $set: update,
                },
                function (err, res) {
                  if (err) return cb(false);
                  return cb(false, "updated");
                }
              );
            }
          });
        } else {
          return cb(false, [11]);
        }
      }
    });
  };
  Productsubgroup.remoteMethod("removeProductFromSubGroup", {
    description: "Remove Product To sub Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    }, 
  });
 

  Productsubgroup.editProductSubGroup = function (params, cb) {
  console.log("params : ",params);
    var self = this;
    var groupsCollection = this.getDataSource().connector.collection(
      Productsubgroup.modelName
    );

    let selectedProducts=[]; 
    let ExistingProducts=[];
    for (var i = 0; i < params.selecteddata.length; i++) {
      selectedProducts.push(ObjectId(params.selecteddata[i].id));
    }
for (var i = 0; i < params.data.ProductInfo.length; i++) {
  ExistingProducts.push(ObjectId(params.data.ProductInfo[i].id));
}
let allProduct=selectedProducts.concat(ExistingProducts);
let unique = allProduct.filter((item, i, ar) => ar.indexOf(item) === i);

Productsubgroup.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          stateId:  ObjectId(params.stateId),
          id : ObjectId(params.data.groupId),
        },
      },
      function (err, response) {
       
        if(response.length>0){
        let where = {
          companyId: ObjectId(params.companyId),
          stateId:  ObjectId(params.stateId),
          _id : ObjectId(params.data.groupId),
        }
        let update={
          productInfo : unique,
          updatedAt : new Date()
        }
        groupsCollection.update(
    where,
    {
      $set: update,
    },
    function (err, res) {
      
      if (err) return cb(false);
      return cb(false, "updated");
    }
  ) 
  }
      }); 
   
  };
  Productsubgroup.remoteMethod("editProductSubGroup", {
    description: "Edit Assign Product To Sub Group",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "object",
    },
    http: {
      verb: "post",
    },
  });

};
