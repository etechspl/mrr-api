'use strict';
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail');
module.exports = function (Sampleissue) {
    
    Sampleissue.sampleInsert = function (params, cb) {
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Sampleissue.modelName);

        Sampleissue.app.models.UserInfo.find({ 
            "where": {
                "companyId": params.form1.companyId,
                "stateId": params.form1.stateInfo,
                "designationLevel": params.form1.designation,
                
            },
            "fields1": "userId",
            "fields2": "districtId"

        }, function (err, userIdResult) {
            if(err){
                sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'sampleInsert' });
            }
            let finalobj = [];
            for (let i = 0; i < userIdResult.length; i++) {
                for (let j = 0; j < params.form2.formarray.length; j++) {
                 if (params.form2.formarray[j].receipt === null || params.form2.formarray[j].receipt < 0 || params.form2.formarray[j].receipt === undefined || params.form2.formarray[j].receipt === '') {
                        
                 }else{
                     if(params.form1.districtId === ""||params.form1.districtId === null){
                        let tempData = {
                            companyId : ObjectId(params.form1.companyId),
                            stateId : ObjectId(params.form1.stateInfo), 
                            districtId : userIdResult[i].districtId,
                            designationLevel : params.form1.designation,
                            userId : userIdResult[i].userId,
                            productId : ObjectId(params.form2.formarray[j].id._id._id),
                            receipt:params.form2.formarray[j].receipt,
                            issueDate : new Date,
                            type : "sample",
                            month : new Date().getMonth()+1,
                            year  : new Date().getYear()+1900,
                        };

                        finalobj.push(tempData);

                     }else{
                    let tempData = {
                        companyId : ObjectId(params.form1.companyId),
                        stateId : ObjectId(params.form1.stateInfo), 
                        districtId : ObjectId(params.form1.districtId),
                        designationLevel : params.form1.designation,
                        userId : userIdResult[i].userId,
                        productId : ObjectId(params.form2.formarray[j].id._id._id),
                        receipt:params.form2.formarray[j].receipt,
                        issueDate : new Date,
                        type : "sample",
                        month : new Date().getMonth()+1,
                        year  : new Date().getYear()+1900,

                    };
                    finalobj.push(tempData);
                    
                }
                    
                } 
           }

        }
            
            Sampleissue.find({
                "where":{  
                "companyId": params.form1.companyId,
                "stateId": params.form1.stateInfo,
                "designationLevel": params.form1.designation
                 }
            }, function(err, response) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'sampleInsert' });
                }
                if (response.length > 0) {
                    var err = new Error('Sample Already Issued');
                    err.statusCode = 409;
                    err.code = 'Validation failed';
                    return cb(err);
                } else {
                    Sampleissue.create(finalobj, function(err, res) {
                        //// console.log("Done......");
                        if (err) {
                            sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'sampleInsert' });
                            // console.log(err);
                            return cb(err);
                        }
                        return cb(false, res)
                    })
            
                }
            
            }) 
        })
    }
    Sampleissue.remoteMethod(
        'sampleInsert', {
            description: 'Sample Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )
    
    //-------------------FOr GiftIssue--------------
    Sampleissue.giftInsert = function (params, cb) {
        // console.log(params.form1);
        // console.log(params.form2);
        var self = this;
        var SampleCollection = self.getDataSource().connector.collection(Sampleissue.modelName);

        Sampleissue.app.models.UserInfo.find({ 
            "where": {
                "companyId": params.form1.companyId,
                "stateId": params.form1.stateInfo,
                "designationLevel": params.form1.designation,
                
            },
            "fields1": "userId",
            "fields2": "districtId"

        }, function (err, userIdResult) {
            if(err){
                sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'giftInsert' });
            }
            let finalobj = [];
            for (let i = 0; i < userIdResult.length; i++) {

                
                for (let j = 0; j < params.form2.formarray.length; j++) {
                 if (params.form2.formarray[j].receipt === null || params.form2.formarray[j].receipt < 0 || params.form2.formarray[j].receipt === undefined || params.form2.formarray[j].receipt === '') {
                        
                 }else{
                     if(params.form1.districtId === ""||params.form1.districtId === null){
                        let tempData = {
                            companyId : ObjectId(params.form1.companyId),
                            stateId : ObjectId(params.form1.stateInfo), 
                            districtId : userIdResult[i].districtId,
                            designationLevel : params.form1.designation,
                            userId : userIdResult[i].userId,
                            productId : ObjectId(params.form2.formarray[j].id._id),
                            receipt:params.form2.formarray[j].receipt,
                            issueDate : new Date,
                            type : "gift",
                            month : new Date().getMonth()+1,
                            year  : new Date().getYear()+1900,
                        };

                        finalobj.push(tempData);

                     }else{
                    let tempData = {
                        companyId : ObjectId(params.form1.companyId),
                        stateId : ObjectId(params.form1.stateInfo), 
                        districtId : ObjectId(params.form1.districtId),
                        designationLevel : params.form1.designation,
                        userId : userIdResult[i].userId,
                        productId : ObjectId(params.form2.formarray[j].id._id),
                        receipt:params.form2.formarrayg[j].receipt,
                        issueDate : new Date,
                        type : "gift",
                        month : new Date().getMonth()+1,
                        year  : new Date().getYear()+1900,

                    };
                    finalobj.push(tempData);
                    
                }
                    
                } 
           }

        }
            
            Sampleissue.find({
                "where":{  
                "companyId": params.form1.companyId,
                "stateId": params.form1.stateInfo,
                "designationLevel": params.form1.designation
                 }
            }, function(err, response) {
                if(err){
                    sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'giftInsert' });
                }
                if (response.length > 0) {
                    var err = new Error('Gift Already Issued');
                    err.statusCode = 409;
                    err.code = 'Validation failed';
                    return cb(err);
                } else {
                    Sampleissue.create(finalobj, function(err, res) {
                        //// console.log("Done......");
                        if (err) {
                            sendMail.sendMail({ collectionName: 'Sampleissue', errorObject: err, paramsObject: params, methodName: 'giftInsert' });
                            // console.log(err);
                            return cb(err);
                        }
                        return cb(false, res)
                    })
            
                }
            
            }) 
        })
    }
    Sampleissue.remoteMethod(
        'giftInsert', {
            description: 'Gift Issued',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    )


};
