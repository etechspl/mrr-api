'use strict';
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../../utils/sendMail');
var async = require('async');
var moment = require('moment');
module.exports = function (Target) {
    
    Target.createTarget = function (param, cb) {
        var self = this;
        var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
        var aMatch = {};
        let insert = {};
        let quarterNumber = 0;
        let loopRunStart = 0;
        let loopRunEnd = 0;
        if (param.formInfo.targetSubmitType == 'productwise') {
            if (param.formInfo.yearlyType == 'Monthly') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 1;
                loopRunEnd = 12;
            } else if (param.formInfo.yearlyType == 'Quarterly') {
                quarterNumber = parseInt(param.formInfo.quarterType);
                if (param.formInfo.quarterType == '1') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 1;
                    loopRunEnd = 3;
                } else if (param.formInfo.quarterType == '2') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 4;
                    loopRunEnd = 6;
                } else if (param.formInfo.quarterType == '3') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 7;
                    loopRunEnd = 9;
                } else if (param.formInfo.quarterType == '4') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 10;
                    loopRunEnd = 12;
                }
            } else if (param.formInfo.yearlyType == 'Yearly') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 13;
                loopRunEnd = 13;
            }
            var empNameAlreadySubmitTraget = [];
            var hqNameAlreadySubmitTraget = [];
            var userIds = [];
            if (param.formInfo.reportType == 'Employeewise') {
                for (let i = 0; i < param.formInfo.userInfo.length; i++) {
                    userIds.push(ObjectId(param.formInfo.userInfo[i]));
                    aMatch.userId = ObjectId(param.formInfo.userInfo[i]);
                    //aMatch.targetSubmitType=param.formInfo.reportType;
                    //aMatch.durationType=param.formInfo.yearlyType;
                    //aMatch.targetType=param.formInfo.targetSubmitType;
                    TargetCollection.aggregate({
                        $match: aMatch
                    }, function (err, recordExistOrNot) {
                        if(err){
                            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                        }
                        if (recordExistOrNot.length > 0) {
                            //empNameAlreadySubmitTraget.push(ObjectId(param.formInfo.userInfo[i]));
                            // console.log('Alreay Exist...');
                        } else {
                            var targetInserArray = [];
                            for (let j = 0; j < param.targetInfo.length; j++) {
                                for (let k = loopRunStart; k <= loopRunEnd; k++) {
                                    insert = {
                                        companyId: ObjectId(param.companyId),
                                        targetSubmitType: param.formInfo.reportType,
                                        durationType: param.formInfo.yearlyType,
                                        targetType: param.formInfo.targetSubmitType,
                                        productName: param.targetInfo[j].productName,
                                        productId: ObjectId(param.targetInfo[j].productId),
                                        stateId: ObjectId(param.formInfo.stateInfo[0]),
                                        districtId: ObjectId(param.formInfo.districtInfo[0]),
                                        userId: ObjectId(param.formInfo.userInfo[i]),
                                        targetMonth: k,
                                        targetYear: param.formInfo.year,
                                        quarterNo: quarterNumber,
                                        group: param.formInfo.selectedGroupName,
                                        createdBy: ObjectId(param.userLoginId),
                                        createdAt: new Date(),
                                        updateAt: new Date()
                                    };
                                    if (k == 1) {
                                        insert.targetQuantity = param.targetInfo[j].Jan;
                                    } else if (k == 2) {
                                        insert.targetQuantity = param.targetInfo[j].Feb;
                                    } else if (k == 3) {
                                        insert.targetQuantity = param.targetInfo[j].March;
                                    } else if (k == 4) {
                                        insert.targetQuantity = param.targetInfo[j].April;
                                    } else if (k == 5) {
                                        insert.targetQuantity = param.targetInfo[j].May;
                                    } else if (k == 6) {
                                        insert.targetQuantity = param.targetInfo[j].June;
                                    } else if (k == 7) {
                                        insert.targetQuantity = param.targetInfo[j].July;
                                    } else if (k == 8) {
                                        insert.targetQuantity = param.targetInfo[j].Aug;
                                    } else if (k == 9) {
                                        insert.targetQuantity = param.targetInfo[j].Sep;
                                    } else if (k == 10) {
                                        insert.targetQuantity = param.targetInfo[j].Oct;
                                    } else if (k == 11) {
                                        insert.targetQuantity = param.targetInfo[j].Nov;
                                    } else if (k == 12) {
                                        insert.targetQuantity = param.targetInfo[j].Dec;
                                    } else if (k == 13) {
                                        insert.targetQuantity = param.targetInfo[j].quantity;
                                    }
                                    targetInserArray.push(insert);
                                }
                            }
                            Target.create(targetInserArray, function (err, result) {
                                if(err){
                                    sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                                }
                            });
                        }
                    });
                }
                alreadySubmitUserFun(userIds,param.companyId,param.formInfo.reportType,param.formInfo.year, empNameAlreadySubmitTraget);
            } else if (param.formInfo.reportType == 'Headquarterwise') {
                var hqIds=[];
                for (let i = 0; i < param.formInfo.districtInfo.length; i++) {
                    hqIds.push(ObjectId(param.formInfo.districtInfo[i]));
                    aMatch.districtId = ObjectId(param.formInfo.districtInfo[i]);
                    TargetCollection.aggregate({
                        $match: aMatch
                    }, function (err, recordExistOrNot) {
                        if(err){
                            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                        }
                        if (recordExistOrNot.length > 0) {
                            //hqNameAlreadySubmitTraget.push(ObjectId(param.formInfo.districtInfo[i]));
                            // console.log('Alreay Exist..@@.');
                        } else {
                            var targetInserArray = [];
                            for (let j = 0; j < param.targetInfo.length; j++) {
                                for (let k = loopRunStart; k <= loopRunEnd; k++) {
                                    insert = {
                                        companyId: ObjectId(param.companyId),
                                        targetSubmitType: param.formInfo.reportType,
                                        durationType: param.formInfo.yearlyType,
                                        targetType: param.formInfo.targetSubmitType,
                                        productName: param.targetInfo[j].productName,
                                        productId: ObjectId(param.targetInfo[j].productId),
                                        stateId: ObjectId(param.formInfo.stateInfo[0]),
                                        districtId: ObjectId(param.formInfo.districtInfo[i]),
                                        targetMonth: k,
                                        targetYear: param.formInfo.year,
                                        quarterNo: quarterNumber,
                                        group: param.formInfo.selectedGroupName,
                                        createdBy: ObjectId(param.userLoginId),
                                        createdAt: new Date(),
                                        updateAt: new Date()
                                    };
                                    if (k == 1) {
                                        insert.targetQuantity = param.targetInfo[j].Jan;
                                    } else if (k == 2) {
                                        insert.targetQuantity = param.targetInfo[j].Feb;
                                    } else if (k == 3) {
                                        insert.targetQuantity = param.targetInfo[j].March;
                                    } else if (k == 4) {
                                        insert.targetQuantity = param.targetInfo[j].April;
                                    } else if (k == 5) {
                                        insert.targetQuantity = param.targetInfo[j].May;
                                    } else if (k == 6) {
                                        insert.targetQuantity = param.targetInfo[j].June;
                                    } else if (k == 7) {
                                        insert.targetQuantity = param.targetInfo[j].July;
                                    } else if (k == 8) {
                                        insert.targetQuantity = param.targetInfo[j].Aug;
                                    } else if (k == 9) {
                                        insert.targetQuantity = param.targetInfo[j].Sep;
                                    } else if (k == 10) {
                                        insert.targetQuantity = param.targetInfo[j].Oct;
                                    } else if (k == 11) {
                                        insert.targetQuantity = param.targetInfo[j].Nov;
                                    } else if (k == 12) {
                                        insert.targetQuantity = param.targetInfo[j].Dec;
                                    } else if (k == 13) {
                                        insert.targetQuantity = param.targetInfo[j].quantity;
                                    }
                                    targetInserArray.push(insert);
                                }
                            }
                            Target.create(targetInserArray, function (err, result) {
                                if(err){
                                    sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                                }
                            });
                        }
                    });
                }
                alreadySubmitHQFun(hqIds,param.companyId,param.formInfo.reportType,param.formInfo.year, hqNameAlreadySubmitTraget);
            }
        } else if (param.formInfo.targetSubmitType == 'amountwise') {
            if (param.formInfo.yearlyType == 'Monthly') {
				
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 1;
                loopRunEnd = 12;
            } else if (param.formInfo.yearlyType == 'Quarterly') {
                quarterNumber = parseInt(param.formInfo.quarterType);
                if (param.formInfo.quarterType == '1') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 1;
                    loopRunEnd = 3;
                } else if (param.formInfo.quarterType == '2') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 4;
                    loopRunEnd = 6;
                } else if (param.formInfo.quarterType == '3') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 7;
                    loopRunEnd = 9;
                } else if (param.formInfo.quarterType == '4') {
                    aMatch.targetYear = param.formInfo.year;
                    loopRunStart = 10;
                    loopRunEnd = 12;
                }
            } else if (param.formInfo.yearlyType == 'Yearly') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 13;
                loopRunEnd = 13;
            }
            var empNameAlreadySubmitTraget = [];
            var hqNameAlreadySubmitTraget = [];
            var userIds = [];
            if (param.formInfo.reportType == 'Employeewise') {
                for (let i = 0; i < param.formInfo.userInfo.length; i++) {
                    userIds.push(ObjectId(param.formInfo.userInfo[i]));
                    aMatch.userId = ObjectId(param.formInfo.userInfo[i]);
                    //aMatch.targetSubmitType=param.formInfo.reportType;
                    //aMatch.durationType=param.formInfo.yearlyType;
                    //aMatch.targetType=param.formInfo.targetSubmitType;
                    TargetCollection.aggregate({
                        $match: aMatch
                    }, function (err, recordExistOrNot) {
                        if(err){
                            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                        }
                        if (recordExistOrNot.length > 0) {
                            //empNameAlreadySubmitTraget.push(ObjectId(param.formInfo.userInfo[i]));
                            // console.log('Alreay Exist...');
                        } else {
                            for (let j = 0; j < 1; j++) {
                                var targetInserArray = [];
                                for (let k = loopRunStart; k <= loopRunEnd; k++) {
                                    insert = {
                                        companyId: ObjectId(param.companyId),
                                        targetSubmitType: param.formInfo.reportType,
                                        durationType: param.formInfo.yearlyType,
                                        targetType: param.formInfo.targetSubmitType,
                                        stateId: ObjectId(param.formInfo.stateInfo[0]),
                                        districtId: ObjectId(param.formInfo.districtInfo[0]),
                                        userId: ObjectId(param.formInfo.userInfo[i]),
                                        targetMonth: k,
                                        targetYear: param.formInfo.year,
                                        quarterNo: quarterNumber,
                                        createdBy: ObjectId(param.userLoginId),
                                        createdAt: new Date(),
                                        updateAt: new Date()
                                    };
                                    if (k == 1) {
                                        insert.targetQuantity = param.targetInfo[j].Jan;
                                    } else if (k == 2) {
                                        insert.targetQuantity = param.targetInfo[j].Feb;
                                    } else if (k == 3) {
                                        insert.targetQuantity = param.targetInfo[j].March;
                                    } else if (k == 4) {
                                        insert.targetQuantity = param.targetInfo[j].April;
                                    } else if (k == 5) {
                                        insert.targetQuantity = param.targetInfo[j].May;
                                    } else if (k == 6) {
                                        insert.targetQuantity = param.targetInfo[j].June;
                                    } else if (k == 7) {
                                        insert.targetQuantity = param.targetInfo[j].July;
                                    } else if (k == 8) {
                                        insert.targetQuantity = param.targetInfo[j].Aug;
                                    } else if (k == 9) {
                                        insert.targetQuantity = param.targetInfo[j].Sep;
                                    } else if (k == 10) {
                                        insert.targetQuantity = param.targetInfo[j].Oct;
                                    } else if (k == 11) {
                                        insert.targetQuantity = param.targetInfo[j].Nov;
                                    } else if (k == 12) {
                                        insert.targetQuantity = param.targetInfo[j].Dec;
                                    } else if (k == 13) {
                                        insert.targetQuantity = param.targetInfo[j].quantity;
                                    }
                                    targetInserArray.push(insert);
                                }
                            }
                            Target.create(targetInserArray, function (err, result) {
                                if(err){
                                    sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                                }
                            });
                        }
                    });
                }
                alreadySubmitUserFun(userIds,param.companyId,param.formInfo.reportType,param.formInfo.year, empNameAlreadySubmitTraget);
            } else if (param.formInfo.reportType == 'Headquarterwise') {
                var hqIds=[];
                for (let i = 0; i < param.formInfo.districtInfo.length; i++) {
                    hqIds.push(ObjectId(param.formInfo.districtInfo[i]));
                    aMatch.districtId = ObjectId(param.formInfo.districtInfo[i]);
					aMatch.companyId = ObjectId(param.companyId);

                    TargetCollection.aggregate({
                        $match: aMatch
                    }, function (err, recordExistOrNot) {
                        if(err){
                            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                        }
                        if (recordExistOrNot.length > 0) {
                            //hqNameAlreadySubmitTraget.push(ObjectId(param.formInfo.districtInfo[i]));
                            // console.log('Alreay Exist...');
                        } else {
                            for (let j = 0; j < 1; j++) {
                                var targetInserArray = [];
                                for (let k = loopRunStart; k <= loopRunEnd; k++) {
                                    insert = {
                                        companyId: ObjectId(param.companyId),
                                        targetSubmitType: param.formInfo.reportType,
                                        durationType: param.formInfo.yearlyType,
                                        targetType: param.formInfo.targetSubmitType,
                                        stateId: ObjectId(param.formInfo.stateInfo[0]),
                                        districtId: ObjectId(param.formInfo.districtInfo[i]),
                                        targetMonth: k,
                                        targetYear: param.formInfo.year,
                                        quarterNo: quarterNumber,
                                        createdBy: ObjectId(param.userLoginId),
                                        createdAt: new Date(),
                                        updateAt: new Date()
                                    };
                                    if (k == 1) {
                                        insert.targetQuantity = param.targetInfo[j].Jan;
                                    } else if (k == 2) {
                                        insert.targetQuantity = param.targetInfo[j].Feb;
                                    } else if (k == 3) {
                                        insert.targetQuantity = param.targetInfo[j].March;
                                    } else if (k == 4) {
                                        insert.targetQuantity = param.targetInfo[j].April;
                                    } else if (k == 5) {
                                        insert.targetQuantity = param.targetInfo[j].May;
                                    } else if (k == 6) {
                                        insert.targetQuantity = param.targetInfo[j].June;
                                    } else if (k == 7) {
                                        insert.targetQuantity = param.targetInfo[j].July;
                                    } else if (k == 8) {
                                        insert.targetQuantity = param.targetInfo[j].Aug;
                                    } else if (k == 9) {
                                        insert.targetQuantity = param.targetInfo[j].Sep;
                                    } else if (k == 10) {
                                        insert.targetQuantity = param.targetInfo[j].Oct;
                                    } else if (k == 11) {
                                        insert.targetQuantity = param.targetInfo[j].Nov;
                                    } else if (k == 12) {
                                        insert.targetQuantity = param.targetInfo[j].Dec;
                                    } else if (k == 13) {
                                        insert.targetQuantity = param.targetInfo[j].quantity;
                                    }
                                    targetInserArray.push(insert);
                                }
                            }
                            Target.create(targetInserArray, function (err, result) {
                            });
                        }
                    });
                }
                alreadySubmitHQFun(hqIds,param.companyId,param.formInfo.reportType,param.formInfo.year, hqNameAlreadySubmitTraget);
            }
        }

        function alreadySubmitUserFun(userIdArr,companyId,reportType,year ,empNameAlreadySubmitTraget) {
            TargetCollection.aggregate({
                $match: {
                    companyId: ObjectId(companyId),
                    userId: { $in: userIdArr },
                    targetSubmitType:reportType,
                    targetYear:parseInt(year)
                }
            }, {
                    $group: {
                        _id: {},
                        userIds: { $addToSet: "$userId" }
                    }
                }, function (err, alreadyExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                    }
                    if (alreadyExistOrNot.length > 0) {
                        let userIdss = [];
                        for (let m = 0; m < userIdArr.length; m++) {
                            let userId = alreadyExistOrNot[0].userIds.filter(function (obj) {
                                return obj == userIdArr[m].toString();
                            });
                            if (userId.length > 0) {
                                userIdss.push(userId[0]);
                            }
                        }
                        Target.app.models.UserLogin.find({
                            where: {
                                companyId: companyId,
                                _id: { inq: userIdss },
                            },
                            fields: {
                                name: 1
                            }
                        }, function (err, result) {
                            if(err){
                                sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                            }
                            var userName='';
                            for (let mm = 0; mm < result.length; mm++) {
                                if (mm == 0) {
                                    userName = result[mm].name;
                                } else {
                                    userName = userName+' , ' + result[mm].name;
                                }
                            }
                            empNameAlreadySubmitTraget.push({
                                "alreadySubmitUser": userName
                            });
                            // console.log(empNameAlreadySubmitTraget);
                            return cb(false, empNameAlreadySubmitTraget);
                        });
                    } else {
                        return cb(false, empNameAlreadySubmitTraget);
                    }
                });
        }

        function alreadySubmitHQFun(hqIdsArr,companyId,reportType ,year,hqNameAlreadySubmitTraget) {
            TargetCollection.aggregate({
                $match: {
                    companyId: ObjectId(companyId),
                    districtId: { $in: hqIdsArr },
                    targetSubmitType:reportType,
                    targetYear:parseInt(year)
                }
            }, {
                    $group: {
                        _id: {},
                        districtIds: { $addToSet: "$districtId" }
                    }
                }, function (err, alreadyExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                    }
                    if (alreadyExistOrNot.length > 0) {
                        let districtIdss = [];
                        for (let m = 0; m < hqIdsArr.length; m++) {
                            let districtId = alreadyExistOrNot[0].districtIds.filter(function (obj) {
                                return obj == hqIdsArr[m].toString();
                            });
                            if (districtId.length > 0) {
                                districtIdss.push(districtId[0]);
                            }
                        }
                        Target.app.models.District.find({
                            where: {
                               // companyId: companyId,
                                _id: { inq: districtIdss },
                            },
                            fields: {
                                districtName: 1
                            }
                        }, function (err, result) {
                            if(err){
                                sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'createTarget' });
                            }
                            var districtName='';
                            for (let mm = 0; mm < result.length; mm++) {
                                if (mm == 0) {
                                    districtName = result[mm].districtName;
                                } else {
                                    districtName = districtName+' , ' + result[mm].districtName;
                                }
                            }
                            hqNameAlreadySubmitTraget.push({
                                "alreadySubmitDistrict": districtName
                            });
                            return cb(false, hqNameAlreadySubmitTraget);
                        });
                    } else {
                        return cb(false, hqNameAlreadySubmitTraget);
                    }
                });
        }

    };

    Target.remoteMethod(
        'createTarget', {
            description: 'createTarget',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Target.getTarget = function (param, cb) {
        var self = this;
        var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
        let aMatch = {};
        let aGroup = {};
        aMatch.companyId = ObjectId(param.companyId);
        let stateIds = [];
        let userIds = [];
        let districtIds = [];
        let quarterNumber = 0;
        let loopRunStart = 0;
        let loopRunEnd = 0;

        if (param.formInfo.yearlyType == 'Monthly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 1;
            loopRunEnd = 12;
        } else if (param.formInfo.yearlyType == 'Quarterly') {
            quarterNumber = parseInt(param.formInfo.quarterType);
            if (param.formInfo.quarterType == '1') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 1;
                loopRunEnd = 3;
            } else if (param.formInfo.quarterType == '2') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 4;
                loopRunEnd = 6;
            } else if (param.formInfo.quarterType == '3') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 7;
                loopRunEnd = 9;
            } else if (param.formInfo.quarterType == '4') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 10;
                loopRunEnd = 12;
            }
        } else if (param.formInfo.yearlyType == 'Yearly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 13;
            loopRunEnd = 13;
        }
        if (param.formInfo.reportType == 'Employeewise') {
            for (var i = 0; i < param.formInfo.userInfo.length; i++) {
                userIds.push(ObjectId(param.formInfo.userInfo[i]));
            }
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.userId = { $in: userIds }
            if (param.formInfo.targetSubmitType == 'productwise') {
                aGroup = {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        stateId: "$stateId",
                        districtId: "$districtId",
                        userId: "$userId",
                        group: "$group"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                getTargetEmployeeWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            } else {
                aGroup = {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId",
                        userId: "$userId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                getTargetEmployeeWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            }
        } else if (param.formInfo.reportType == 'Headquarterwise') {
            for (var ii = 0; ii < param.formInfo.districtInfo.length; ii++) {
                districtIds.push(ObjectId(param.formInfo.districtInfo[ii]));
            }
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.districtId = { $in: districtIds }
            if (param.formInfo.targetSubmitType == 'productwise') {
                aGroup = {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        stateId: "$stateId",
                        districtId: "$districtId",
                        group: "$group"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                getTargetHQWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            } else {
                aGroup = {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                getTargetHQWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            }

        } else if (param.formInfo.reportType == 'Statewise') {
            for (var iii = 0; iii < param.formInfo.stateInfo.length; iii++) {
                stateIds.push(ObjectId(param.formInfo.stateInfo[iii]));
            }
            // aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.stateId = { $in: stateIds }
            if (param.formInfo.targetSubmitType == 'productwise') {
                aGroup = {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        stateId: "$stateId",
                        group: "$group"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                getTargetStateWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            } else {
                aGroup = {
                      // _id: {
                    //     stateId: "$stateId"
                    // },
                    // targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                    _id: { stateId: '$stateId', month : "$targetMonth", year : "$targetYear" },
                    targetQty:{$sum:"$targetQuantity"}
                }
                getTargetStateWiseDetail(aMatch, aGroup, loopRunStart, loopRunEnd);
            }

        }
        function getTargetEmployeeWiseDetail(Match, Group, loopRunStart, loopRunEnd) {
            TargetCollection.aggregate({
                $match: Match
            },
                // Stage 2
                {
                    $group: Group
                },
                // Stage 3
                {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "stateData"
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "districtData"
                    }
                },
                // Stage 5
                {
                    $lookup: {
                        "from": "UserLogin",
                        "localField": "_id.userId",
                        "foreignField": "_id",
                        "as": "userData"
                    }
                },

                // Stage 6
                {
                    $project: {
                        _id: 0,
                        stateId: { $arrayElemAt: ["$stateData._id", 0] },
                        stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                        districtId: { $arrayElemAt: ["$districtData._id", 0] },
                        districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                        userName: { $arrayElemAt: ["$userData.name", 0] },
                        userId: { $arrayElemAt: ["$userData._id", 0] },
                        targetQty: 1,
                        productId: "$_id.productId",
                        productName: "$_id.productName",
                        group:"$_id.group"
                    }
                }, function (err, recordExistOrNot) {
					console.log("err-",err)
					console.log("recordExistOrNot:-",recordExistOrNot)
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTarget' });
                    }
                    var finalResultArray = [];
                    var finalObj = {};
                    var totalJan = 0, totalFeb = 0, totalMar = 0, totalApr = 0, totalMay = 0, totalJune = 0, totalJuly = 0,
                        totalAug = 0, totalSep = 0, totalOct = 0, totalNov = 0, totalDec = 0, yearlyQty = 0;
                    if (recordExistOrNot.length > 0) {
                        for (var k = 0; k < recordExistOrNot.length; k++) {
                            var monthData = [];
                            var totalQty = 0;
                            for (let m = loopRunStart; m <= loopRunEnd; m++) {
                                let monthFound = recordExistOrNot[k].targetQty.filter(function (obj) {
                                    return obj.targetMonth == m;
                                });
                                if (monthFound.length > 0) {
                                    if (m == 1) {
                                        totalJan = totalJan + parseInt(monthFound[0].targetQty);
                                    } else if (m == 2) {
                                        totalFeb = totalFeb + parseInt(monthFound[0].targetQty);
                                    } else if (m == 3) {
                                        totalMar = totalMar + parseInt(monthFound[0].targetQty);
                                    } else if (m == 4) {
                                        totalApr = totalApr + parseInt(monthFound[0].targetQty);
                                    } else if (m == 5) {
                                        totalMay = totalMay + parseInt(monthFound[0].targetQty);
                                    } else if (m == 6) {
                                        totalJune = totalJune + parseInt(monthFound[0].targetQty);
                                    } else if (m == 7) {
                                        totalJuly = totalJuly + parseInt(monthFound[0].targetQty);
                                    } else if (m == 8) {
                                        totalAug = totalAug + parseInt(monthFound[0].targetQty);
                                    } else if (m == 9) {
                                        totalSep = totalSep + parseInt(monthFound[0].targetQty);
                                    } else if (m == 10) {
                                        totalOct = totalOct + parseInt(monthFound[0].targetQty);
                                    } else if (m == 11) {
                                        totalNov = totalNov + parseInt(monthFound[0].targetQty);
                                    } else if (m == 12) {
                                        totalDec = totalDec + parseInt(monthFound[0].targetQty);
                                    } else if (m == 13) {
                                        yearlyQty = yearlyQty + parseInt(monthFound[0].targetQty);
                                    }
                                    monthData.push(parseInt(monthFound[0].targetQty));
                                    totalQty = totalQty + parseInt(monthFound[0].targetQty);
                                } else {
                                    monthData.push(0);
                                }
                            }
                            if (Match.targetType == 'amountwise') {
                                finalResultArray.push({
                                    "stateName": recordExistOrNot[k].stateName,
                                    "districtName": recordExistOrNot[k].districtName,
                                    "empName": recordExistOrNot[k].userName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            } else {
                                finalResultArray.push({
                                    "productId": recordExistOrNot[k].productId,
                                    "productName": recordExistOrNot[k].productName,
                                    "group":recordExistOrNot[k].group,
                                    "stateName": recordExistOrNot[k].stateName,
                                    "districtName": recordExistOrNot[k].districtName,
                                    "empName": recordExistOrNot[k].userName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            }

                        }
                        finalObj.obj1 = finalResultArray;
                        finalObj.totalJan = totalJan;
                        finalObj.totalFeb = totalFeb;
                        finalObj.totalMar = totalMar;
                        finalObj.totalApr = totalApr;
                        finalObj.totalMay = totalMay;
                        finalObj.totalJune = totalJune;
                        finalObj.totalJuly = totalJuly;
                        finalObj.totalAug = totalAug;
                        finalObj.totalSep = totalSep;
                        finalObj.totalOct = totalOct;
                        finalObj.totalNov = totalNov;
                        finalObj.totalDec = totalDec;
                        finalObj.yearlyQty = yearlyQty;
                        finalObj.status = 1;
                        return cb(false, finalObj);
                    } else {
                        finalObj.status = 0;
                        return cb(false, finalObj);
                    }
                });
        }

        function getTargetHQWiseDetail(Match, Group, loopRunStart, loopRunEnd) {
            TargetCollection.aggregate({
                $match: Match
            },

                // Stage 2
                {
                    $group: Group
                },

                // Stage 3
                {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "stateData"
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "districtData"
                    }
                },

                // Stage 6
                {
                    $project: {
                        _id: 0,
                        stateId: { $arrayElemAt: ["$stateData._id", 0] },
                        stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                        districtId: { $arrayElemAt: ["$districtData._id", 0] },
                        districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                        targetQty: 1,
                        productId: "$_id.productId",
                        productName: "$_id.productName",
                        group:"$_id.group"
                    }
                }, function (err, recordExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTarget' });
                    }
                    var finalResultArray = [];
                    var finalObj = {};
                    var totalJan = 0, totalFeb = 0, totalMar = 0, totalApr = 0, totalMay = 0, totalJune = 0, totalJuly = 0,
                        totalAug = 0, totalSep = 0, totalOct = 0, totalNov = 0, totalDec = 0, yearlyQty = 0;
                    if (recordExistOrNot.length > 0) {
                        for (var k = 0; k < recordExistOrNot.length; k++) {
                            var monthData = [];
                            var totalQty = 0;
                            for (let m = loopRunStart; m <= loopRunEnd; m++) {
                                let monthFound = recordExistOrNot[k].targetQty.filter(function (obj) {
                                    return obj.targetMonth == m;
                                });
                                if (monthFound.length > 0) {
                                    if (m == 1) {
                                        totalJan = totalJan + parseInt(monthFound[0].targetQty);
                                    } else if (m == 2) {
                                        totalFeb = totalFeb + parseInt(monthFound[0].targetQty);
                                    } else if (m == 3) {
                                        totalMar = totalMar + parseInt(monthFound[0].targetQty);
                                    } else if (m == 4) {
                                        totalApr = totalApr + parseInt(monthFound[0].targetQty);
                                    } else if (m == 5) {
                                        totalMay = totalMay + parseInt(monthFound[0].targetQty);
                                    } else if (m == 6) {
                                        totalJune = totalJune + parseInt(monthFound[0].targetQty);
                                    } else if (m == 7) {
                                        totalJuly = totalJuly + parseInt(monthFound[0].targetQty);
                                    } else if (m == 8) {
                                        totalAug = totalAug + parseInt(monthFound[0].targetQty);
                                    } else if (m == 9) {
                                        totalSep = totalSep + parseInt(monthFound[0].targetQty);
                                    } else if (m == 10) {
                                        totalOct = totalOct + parseInt(monthFound[0].targetQty);
                                    } else if (m == 11) {
                                        totalNov = totalNov + parseInt(monthFound[0].targetQty);
                                    } else if (m == 12) {
                                        totalDec = totalDec + parseInt(monthFound[0].targetQty);
                                    } else if (m == 13) {
                                        yearlyQty = yearlyQty + parseInt(monthFound[0].targetQty);
                                    }
                                    monthData.push(parseInt(monthFound[0].targetQty));
                                    totalQty = totalQty + parseInt(monthFound[0].targetQty);
                                } else {
                                    monthData.push(0);
                                }
                            }
                            if (Match.targetType == 'amountwise') {
                                finalResultArray.push({
                                    "stateName": recordExistOrNot[k].stateName,
                                    "districtName": recordExistOrNot[k].districtName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            } else {
                                finalResultArray.push({
                                    "productId": recordExistOrNot[k].productId,
                                    "productName": recordExistOrNot[k].productName,
                                    "group":recordExistOrNot[k].group,
                                    "stateName": recordExistOrNot[k].stateName,
                                    "districtName": recordExistOrNot[k].districtName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            }

                        }
                        finalObj.obj1 = finalResultArray;
                        finalObj.totalJan = totalJan;
                        finalObj.totalFeb = totalFeb;
                        finalObj.totalMar = totalMar;
                        finalObj.totalApr = totalApr;
                        finalObj.totalMay = totalMay;
                        finalObj.totalJune = totalJune;
                        finalObj.totalJuly = totalJuly;
                        finalObj.totalAug = totalAug;
                        finalObj.totalSep = totalSep;
                        finalObj.totalOct = totalOct;
                        finalObj.totalNov = totalNov;
                        finalObj.totalDec = totalDec;
                        finalObj.yearlyQty = yearlyQty;
                        finalObj.status = 1;
                        return cb(false, finalObj);
                    } else {
                        finalObj.status = 0;
                        return cb(false, finalObj);
                    }
                });
        }

        function getTargetStateWiseDetail(Match, Group, loopRunStart, loopRunEnd) {
            TargetCollection.aggregate({
                $match: Match
            }, {
                $group: Group
            },{
                $project:{
                _id : 0,
                stateId : "$_id.stateId",
                month : "$_id.month",
                year : "$_id.year",
                target : "$targetQty"
                
                }
            },{
                $sort:{
                    month : 1,
                    year : 1
                    
            }
           },
            {
                $group:{
                    _id : {
                        stateId : "$stateId"
                        },
                        data :{
                        $addToSet :{
                        targetMonth : "$month",
                        targetYear : "$year",
                        targetQty : "$target"
                        }
                        }
                }
            },

            // Stage 3
            {
                $lookup: {
                    "from": "State",
                    "localField": "_id.stateId",
                    "foreignField": "_id",
                    "as": "stateData"
                }
            },

            // Stage 6
            {
                $project: {
                    _id: 1,
                    stateId: { $arrayElemAt: ["$stateData._id", 0] },
                    stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                    targetQty: "$data",
                    productId: "$_id.productId",
                    productName: "$_id.productName",
                    group:"$_id.group"
                }
            }, 

              function (err, recordExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTarget' });
                    }
                    var finalResultArray = [];
                    var finalObj = {};
                    var totalJan = 0, totalFeb = 0, totalMar = 0, totalApr = 0, totalMay = 0, totalJune = 0, totalJuly = 0,
                        totalAug = 0, totalSep = 0, totalOct = 0, totalNov = 0, totalDec = 0, yearlyQty = 0;
                    if (recordExistOrNot.length > 0) {
                        for (var k = 0; k < recordExistOrNot.length; k++) {
                            var monthData = [];
                            var totalQty = 0;
                            for (let m = loopRunStart; m <= loopRunEnd; m++) {
                                let monthFound = recordExistOrNot[k].targetQty.filter(function (obj) {
                                    return obj.targetMonth == m;
                                });
                                if (monthFound.length > 0) {
                                    if (m == 1) {
                                        totalJan = totalJan + parseInt(monthFound[0].targetQty);
                                    } else if (m == 2) {
                                        totalFeb = totalFeb + parseInt(monthFound[0].targetQty);
                                    } else if (m == 3) {
                                        totalMar = totalMar + parseInt(monthFound[0].targetQty);
                                    } else if (m == 4) {
                                        totalApr = totalApr + parseInt(monthFound[0].targetQty);
                                    } else if (m == 5) {
                                        totalMay = totalMay + parseInt(monthFound[0].targetQty);
                                    } else if (m == 6) {
                                        totalJune = totalJune + parseInt(monthFound[0].targetQty);
                                    } else if (m == 7) {
                                        totalJuly = totalJuly + parseInt(monthFound[0].targetQty);
                                    } else if (m == 8) {
                                        totalAug = totalAug + parseInt(monthFound[0].targetQty);
                                    } else if (m == 9) {
                                        totalSep = totalSep + parseInt(monthFound[0].targetQty);
                                    } else if (m == 10) {
                                        totalOct = totalOct + parseInt(monthFound[0].targetQty);
                                    } else if (m == 11) {
                                        totalNov = totalNov + parseInt(monthFound[0].targetQty);
                                    } else if (m == 12) {
                                        totalDec = totalDec + parseInt(monthFound[0].targetQty);
                                    } else if (m == 13) {
                                        yearlyQty = yearlyQty + parseInt(monthFound[0].targetQty);
                                    }
                                    monthData.push(parseInt(monthFound[0].targetQty));
                                    totalQty = totalQty + parseInt(monthFound[0].targetQty);
                                } else {
                                    monthData.push(0);
                                }
                            }
                            if (Match.targetType == 'amountwise') {
                                finalResultArray.push({
                                    "stateName": recordExistOrNot[k].stateName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            } else {
                                finalResultArray.push({
                                    "productId": recordExistOrNot[k].productId,
                                    "productName": recordExistOrNot[k].productName,
                                    "group":recordExistOrNot[k].group,
                                    "stateName": recordExistOrNot[k].stateName,
                                    "targetQty": monthData,
                                    "totalQty": totalQty
                                })
                            }

                        }
                        finalObj.obj1 = finalResultArray;
                        finalObj.totalJan = totalJan;
                        finalObj.totalFeb = totalFeb;
                        finalObj.totalMar = totalMar;
                        finalObj.totalApr = totalApr;
                        finalObj.totalMay = totalMay;
                        finalObj.totalJune = totalJune;
                        finalObj.totalJuly = totalJuly;
                        finalObj.totalAug = totalAug;
                        finalObj.totalSep = totalSep;
                        finalObj.totalOct = totalOct;
                        finalObj.totalNov = totalNov;
                        finalObj.totalDec = totalDec;
                        finalObj.yearlyQty = yearlyQty;
                        finalObj.status = 1;
                        return cb(false, finalObj);
                    } else {
                        finalObj.status = 0;
                        return cb(false, finalObj);
                    }
                });
        }

    };

    Target.remoteMethod(
        'getTarget', {
            description: 'getTarget',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Target.getTargetSubmittedDetail = function (param, cb) {
        var self = this;
        var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
        let aMatch = {};
        let aGroup = {};
        let aProject = {};
        aMatch.companyId = ObjectId(param.companyId);
        let districtIds = [];
        let quarterNumber = 0;
        let loopRunStart = 0;
        let loopRunEnd = 0;

        if (param.formInfo.yearlyType == 'Monthly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 1;
            loopRunEnd = 12;
        } else if (param.formInfo.yearlyType == 'Quarterly') {
            quarterNumber = parseInt(param.formInfo.quarterType);
            if (param.formInfo.quarterType == '1') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 1;
                loopRunEnd = 3;
            } else if (param.formInfo.quarterType == '2') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 4;
                loopRunEnd = 6;
            } else if (param.formInfo.quarterType == '3') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 7;
                loopRunEnd = 9;
            } else if (param.formInfo.quarterType == '4') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 10;
                loopRunEnd = 12;
            }
        } else if (param.formInfo.yearlyType == 'Yearly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 13;
            loopRunEnd = 13;
        }
        if (param.formInfo.reportType == 'Employeewise') {
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.userId = ObjectId(param.formInfo.userInfo)
            if (param.formInfo.targetSubmitType == 'productwise') {
                aGroup = {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        stateId: "$stateId",
                        districtId: "$districtId",
                        userId: "$userId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                aProject = {
                    _id: 0,
                    productId: "$_id.productId",
                    productName: "$_id.productName",
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    userId: "$_id.userId",
                    targetQty: 1,
                }
                getTargetEmployeeWiseDetail(aMatch, aGroup, aProject, loopRunStart, loopRunEnd);
            } else {
                aGroup = {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId",
                        userId: "$userId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                aProject = {
                    _id: 0,
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    userId: "$_id.userId",
                    targetQty: 1,
                }
                getTargetEmployeeWiseDetail(aMatch, aGroup, aProject, loopRunStart, loopRunEnd);
            }
        } else if (param.formInfo.reportType == 'Headquarterwise') {
            for (var ii = 0; ii < param.formInfo.districtInfo.length; ii++) {
                districtIds.push(ObjectId(param.formInfo.districtInfo[ii]));
            }
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.districtId = { $in: districtIds }
            if (param.formInfo.targetSubmitType == 'productwise') {
                aGroup = {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        stateId: "$stateId",
                        districtId: "$districtId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                aProject = {
                    _id: 0,
                    productId: "$_id.productId",
                    productName: "$_id.productName",
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    targetQty: 1,
                }
                getTargetHQWiseDetail(aMatch, aGroup, aProject, loopRunStart, loopRunEnd);
            } else {
                aGroup = {
                    _id: {
                        stateId: "$stateId",
                        districtId: "$districtId"
                    },
                    targetQty: { $push: { targetMonth: "$targetMonth", targetQty: "$targetQuantity" } }
                }
                aProject = {
                    _id: 0,
                    stateId: "$_id.stateId",
                    districtId: "$_id.districtId",
                    targetQty: 1,
                }
                getTargetHQWiseDetail(aMatch, aGroup, aProject, loopRunStart, loopRunEnd);
            }

        }

        function getTargetEmployeeWiseDetail(Match, Group, Project, loopRunStart, loopRunEnd) {
            TargetCollection.aggregate({
                $match: Match
            },
                // Stage 2
                {
                    $group: Group
                },

                // Stage 6
                {
                    $project: Project
                }, function (err, recordExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTargetSubmittedDetail' });
                    }
                    var finalResultArray = [];
                    if (recordExistOrNot.length > 0) {
                        for (var k = 0; k < recordExistOrNot.length; k++) {
                            var finalObj = {};
                            for (let m = loopRunStart; m <= loopRunEnd; m++) {
                                let monthFound = recordExistOrNot[k].targetQty.filter(function (obj) {
                                    return obj.targetMonth == m;
                                });
                                if (monthFound.length > 0) {
                                    if (m == 1) {
                                        finalObj.Jan = parseInt(monthFound[0].targetQty);
                                    } else if (m == 2) {
                                        finalObj.Feb = parseInt(monthFound[0].targetQty);
                                    } else if (m == 3) {
                                        finalObj.March = parseInt(monthFound[0].targetQty);
                                    } else if (m == 4) {
                                        finalObj.April = parseInt(monthFound[0].targetQty);
                                    } else if (m == 5) {
                                        finalObj.May = parseInt(monthFound[0].targetQty);
                                    } else if (m == 6) {
                                        finalObj.June = parseInt(monthFound[0].targetQty);
                                    } else if (m == 7) {
                                        finalObj.July = parseInt(monthFound[0].targetQty);
                                    } else if (m == 8) {
                                        finalObj.Aug = parseInt(monthFound[0].targetQty);
                                    } else if (m == 9) {
                                        finalObj.Sep = parseInt(monthFound[0].targetQty);
                                    } else if (m == 10) {
                                        finalObj.Oct = parseInt(monthFound[0].targetQty);
                                    } else if (m == 11) {
                                        finalObj.Nov = parseInt(monthFound[0].targetQty);
                                    } else if (m == 12) {
                                        finalObj.Dec = parseInt(monthFound[0].targetQty);
                                    } else if (m == 13) {
                                        finalObj.quantity = parseInt(monthFound[0].targetQty);
                                    }
                                } else {
                                    monthData.push(0);
                                }
                            }
                            if (Match.targetType == 'amountwise') {
                                finalObj.stateId = recordExistOrNot[k].stateId;
                                finalObj.districtId = recordExistOrNot[k].districtId;
                                finalObj.userId = recordExistOrNot[k].userId;
                                finalObj.status = 1;
                                finalResultArray.push(finalObj)
                            } else {
                                finalObj.productId = recordExistOrNot[k].productId;
                                finalObj.productName = recordExistOrNot[k].productName;
                                finalObj.stateId = recordExistOrNot[k].stateId;
                                finalObj.districtId = recordExistOrNot[k].districtId;
                                finalObj.userId = recordExistOrNot[k].userId;
                                finalResultArray.push(finalObj)
                            }

                        }
                        return cb(false, finalResultArray);
                    } else {
                        return cb(false, finalResultArray);
                    }
                });
        }

        function getTargetHQWiseDetail(Match, Group, Project, loopRunStart, loopRunEnd) {
            TargetCollection.aggregate({
                $match: Match
            },

                // Stage 2
                {
                    $group: Group
                },

                // Stage 6
                {
                    $project: Project
                }, function (err, recordExistOrNot) {
                    if(err){
                        sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTargetSubmittedDetail' });
                    }
                    var finalResultArray = [];
                    var finalObj = {};
                    if (recordExistOrNot.length > 0) {
                        for (var k = 0; k < recordExistOrNot.length; k++) {
                            var finalObj = {};
                            for (let m = loopRunStart; m <= loopRunEnd; m++) {
                                let monthFound = recordExistOrNot[k].targetQty.filter(function (obj) {
                                    return obj.targetMonth == m;
                                });
                                if (monthFound.length > 0) {
                                    if (m == 1) {
                                        finalObj.Jan = parseInt(monthFound[0].targetQty);
                                    } else if (m == 2) {
                                        finalObj.Feb = parseInt(monthFound[0].targetQty);
                                    } else if (m == 3) {
                                        finalObj.March = parseInt(monthFound[0].targetQty);
                                    } else if (m == 4) {
                                        finalObj.April = parseInt(monthFound[0].targetQty);
                                    } else if (m == 5) {
                                        finalObj.May = parseInt(monthFound[0].targetQty);
                                    } else if (m == 6) {
                                        finalObj.June = parseInt(monthFound[0].targetQty);
                                    } else if (m == 7) {
                                        finalObj.July = parseInt(monthFound[0].targetQty);
                                    } else if (m == 8) {
                                        finalObj.Aug = parseInt(monthFound[0].targetQty);
                                    } else if (m == 9) {
                                        finalObj.Sep = parseInt(monthFound[0].targetQty);
                                    } else if (m == 10) {
                                        finalObj.Oct = parseInt(monthFound[0].targetQty);
                                    } else if (m == 11) {
                                        finalObj.Nov = parseInt(monthFound[0].targetQty);
                                    } else if (m == 12) {
                                        finalObj.Dec = parseInt(monthFound[0].targetQty);
                                    } else if (m == 13) {
                                        finalObj.quantity = parseInt(monthFound[0].targetQty);
                                    }
                                } else {
                                    monthData.push(0);
                                }
                            }
                            if (Match.targetType == 'amountwise') {
                                finalObj.stateId = recordExistOrNot[k].stateId;
                                finalObj.districtId = recordExistOrNot[k].districtId;
                                finalResultArray.push(finalObj)
                            } else {
                                finalObj.productId = recordExistOrNot[k].productId;
                                finalObj.productName = recordExistOrNot[k].productName;
                                finalObj.stateId = recordExistOrNot[k].stateId;
                                finalObj.districtId = recordExistOrNot[k].districtId;
                                finalResultArray.push(finalObj);
                            }

                        }
                        return cb(false, finalResultArray);
                    } else {
                        return cb(false, finalResultArray);
                    }
                });
        }

    };

    Target.remoteMethod(
        'getTargetSubmittedDetail', {
            description: 'getTargetSubmittedDetail',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    
    Target.modifyTarget = function (param, cb) {
        var self = this;
        var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
        let aMatch = {};
        aMatch.companyId = ObjectId(param.companyId);
        let districtIds = [];
        let quarterNumber = 0;
        let loopRunStart = 0;
        let loopRunEnd = 0;

        if (param.formInfo.yearlyType == 'Monthly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 1;
            loopRunEnd = 12;
        } else if (param.formInfo.yearlyType == 'Quarterly') {
            quarterNumber = parseInt(param.formInfo.quarterType);
            if (param.formInfo.quarterType == '1') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 1;
                loopRunEnd = 3;
            } else if (param.formInfo.quarterType == '2') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 4;
                loopRunEnd = 6;
            } else if (param.formInfo.quarterType == '3') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 7;
                loopRunEnd = 9;
            } else if (param.formInfo.quarterType == '4') {
                aMatch.targetYear = param.formInfo.year;
                loopRunStart = 10;
                loopRunEnd = 12;
            }
        } else if (param.formInfo.yearlyType == 'Yearly') {
            aMatch.targetYear = param.formInfo.year;
            loopRunStart = 13;
            loopRunEnd = 13;
        }
        if (param.formInfo.reportType == 'Employeewise') {
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.userId = ObjectId(param.formInfo.userInfo)
            if (param.formInfo.targetSubmitType == 'productwise') {
                modifyTargetProductwise(aMatch, loopRunStart, loopRunEnd, param.resultValue);
            } else {
                modifyTargetAmountwise(aMatch, loopRunStart, loopRunEnd, param.resultValue);
            }
        } else if (param.formInfo.reportType == 'Headquarterwise') {
            for (var ii = 0; ii < param.formInfo.districtInfo.length; ii++) {
                districtIds.push(ObjectId(param.formInfo.districtInfo[ii]));
            }
            aMatch.targetSubmitType = param.formInfo.reportType;
            aMatch.durationType = param.formInfo.yearlyType;
            aMatch.targetType = param.formInfo.targetSubmitType;
            aMatch.districtId = { $in: districtIds }
            if (param.formInfo.targetSubmitType == 'productwise') {
                modifyTargetProductwise(aMatch, loopRunStart, loopRunEnd, param.resultValue);
            } else {
                modifyTargetAmountwise(aMatch, loopRunStart, loopRunEnd, param.resultValue);
            }

        }

        function modifyTargetProductwise(Match, loopRunStart, loopRunEnd, resultValue) {
            for (var k = 0; k < resultValue.length; k++) {
                Match.productId = ObjectId(resultValue[k].productId);
                for (let m = loopRunStart; m <= loopRunEnd; m++) {
                    let aSet = {};
                    Match.targetMonth = m;
                    if (m == 1) {
                        aSet.targetQuantity = parseInt(resultValue[k].Jan);
                    } else if (m == 2) {
                        aSet.targetQuantity = parseInt(resultValue[k].Feb);
                    } else if (m == 3) {
                        aSet.targetQuantity = parseInt(resultValue[k].March);
                    } else if (m == 4) {
                        aSet.targetQuantity = parseInt(resultValue[k].April);
                    } else if (m == 5) {
                        aSet.targetQuantity = parseInt(resultValue[k].May);
                    } else if (m == 6) {
                        aSet.targetQuantity = parseInt(resultValue[k].June);
                    } else if (m == 7) {
                        aSet.targetQuantity = parseInt(resultValue[k].July);
                    } else if (m == 8) {
                        aSet.targetQuantity = parseInt(resultValue[k].Aug);
                    } else if (m == 9) {
                        aSet.targetQuantity = parseInt(resultValue[k].Sep);
                    } else if (m == 10) {
                        aSet.targetQuantity = parseInt(resultValue[k].Oct);
                    } else if (m == 11) {
                        aSet.targetQuantity = parseInt(resultValue[k].Nov);
                    } else if (m == 12) {
                        aSet.targetQuantity = parseInt(resultValue[k].Dec);
                    } else if (m == 13) {
                        aSet.targetQuantity = parseInt(resultValue[k].quantity);
                    }
                    aSet.updateAt = new Date();
                    TargetCollection.update(Match, { $set: aSet }, { multi: false },
                        function (err, res) {
                            if (err) {
                                sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'modifyTarget' });
                                return cb(false, err); 
                            }
                            if (res) {
                                var lastResult = [];
                                lastResult.push({
                                    "count": res.result.nModified
                                });
                            }
                        });
                }
            }
            let finalResultArray = [];
            finalResultArray.push({
                "status": 1
            });
            return cb(false, finalResultArray);
        }

        function modifyTargetAmountwise(Match, loopRunStart, loopRunEnd, resultValue) {
            for (var k = 0; k < resultValue.length; k++) {
                for (let m = loopRunStart; m <= loopRunEnd; m++) {
                    let aSet = {};
                    Match.targetMonth = m;
                    if (m == 1) {
                        aSet.targetQuantity = parseInt(resultValue[k].Jan);
                    } else if (m == 2) {
                        aSet.targetQuantity = parseInt(resultValue[k].Feb);
                    } else if (m == 3) {
                        aSet.targetQuantity = parseInt(resultValue[k].March);
                    } else if (m == 4) {
                        aSet.targetQuantity = parseInt(resultValue[k].April);
                    } else if (m == 5) {
                        aSet.targetQuantity = parseInt(resultValue[k].May);
                    } else if (m == 6) {
                        aSet.targetQuantity = parseInt(resultValue[k].June);
                    } else if (m == 7) {
                        aSet.targetQuantity = parseInt(resultValue[k].July);
                    } else if (m == 8) {
                        aSet.targetQuantity = parseInt(resultValue[k].Aug);
                    } else if (m == 9) {
                        aSet.targetQuantity = parseInt(resultValue[k].Sep);
                    } else if (m == 10) {
                        aSet.targetQuantity = parseInt(resultValue[k].Oct);
                    } else if (m == 11) {
                        aSet.targetQuantity = parseInt(resultValue[k].Nov);
                    } else if (m == 12) {
                        aSet.targetQuantity = parseInt(resultValue[k].Dec);
                    } else if (m == 13) {
                        aSet.targetQuantity = parseInt(resultValue[k].quantity);
                    }
                    aSet.updateAt = new Date();
                    TargetCollection.update(Match, { $set: aSet }, { multi: false },
                        function (err, res) {
                            if (err) { 
                                sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'modifyTarget' });
                                return cb(false, err); 
                            }
                            if (res) {
                                var lastResult = [];
                                lastResult.push({
                                    "count": res.result.nModified
                                });
                            }
                        });
                }
            }
            let finalResultArray = [];
            finalResultArray.push({
                "status": 1
            });
            return cb(false, finalResultArray);
        }

    };

    Target.remoteMethod(
        'modifyTarget', {
            description: 'modifyTarget',
            accepts: [{
                arg: 'param',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
	
	//------------------
	Target.getTargetDetails = function(param, cb) {
    var self = this;
    var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
    let match = {};
    let group = {};
    let project = {
      _id: 0
    };
    let lookupState = {};
    let lookupDistrict = {};
    let lookupUser = {};
    let sort = {};
    let targetSubmitType = param["targetSubmitType"];
    let durationType = param["durationType"];
    let targetType = param["targetType"];
    let userIds = [];
    let districtIds = [];
    let stateIds = [];
    //setting match object
    match.companyId = ObjectId(param.companyId);
    match.targetSubmitType = param.targetSubmitType;
    match.durationType = param.durationType;
    match.targetType = param.targetType;
    match.targetYear = {
      $in: param.targetYear
    };


    // Duration Type Monthly , Quarterly or Yearly

    if (durationType === 'Monthly' || durationType === 'Quarterly') {
      match.targetMonth = {
        $in: param.targetMonth
      };
      if (durationType === 'Monthly') {
        group.targetMonth = "$targetMonth";


        project.targetMonth = "$_id.targetMonth";
      } else if (durationType === 'Quarterly') {
        group.quarterNo = "$quarterNo";

        project.quarterNo = "$_id.quarterNo";

      }
      group.targetYear = "$targetYear";

      project.targetYear = "$_id.targetYear";
    } else {
      group.targetYear = "$targetYear";

      project.targetYear = "$_id.targetYear";
    }
    // target type amountwise or productwise
    if (targetType === 'amountwise') {

      if (targetSubmitType === 'Employeewise') {
        project.userId = "$_id.userId";
      } else if (targetSubmitType === 'Headquarterwise') {
        project.stateId = "$_id.stateId";
        project.districtId = "$_id.districtId";
      }

      project.totalTarget = "$totalTarget";

    } else if (targetType === 'productwise') {


      group.productId = "$productId";
      group.productName = "$productName";

      project.productId = "$_id.productId";
      project.productName = "$_id.productName";
      project.userId = "$_id.userId";
      project.totalTarget = "$totalTarget";
    }else{
        if (targetSubmitType === 'Employeewise') {
            project.userId = "$_id.userId";
          } else if (targetSubmitType === 'Headquarterwise') {
            project.stateId = "$_id.stateId";
            project.districtId = "$_id.districtId";
          }
    
          project.totalTarget = "$totalTarget";
    }

    //Employeewise monthly productwise
    if (targetSubmitType === 'Employeewise') {
      //converting UserIds into ObjectId
      for (let userId of param.userId) {
        userIds.push(ObjectId(userId))
      }
      match.userId = {
        $in: userIds
      };

      group.userId = "$userId";

      lookupUser = {
        "from": "UserInfo",
        "localField": "userId",
        "foreignField": "userId",
        "as": "users"
      }
	  // console.log("match=",match)
      TargetCollection.aggregate({
        $match: match,
      }, {
        $group: {
          _id: group,
          "totalTarget": {
            "$sum": "$targetQuantity"
          }
        }
      }, {
        $project: project
      }, {
        $lookup: lookupUser,
      }, {
        $project: {
          "totalTarget": 1,
          "targetMonth": 1,
          "targetYear": 1,
          productName:1,
          productId:1,
          "userId": 1,
          "name": {
            $arrayElemAt: ["$users.name", 0]
          }
        }
      }, function(err, result) {
          console.log(err);
          
          console.log(result);
          
        if (err) {
            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTargetDetails' });
            return cb(false, err);
        }
        return cb(false, result)
      })


    } else if (targetSubmitType === 'Headquarterwise') {
      //converting stateId into ObjectId
      for (let state of param.stateId) {
        stateIds.push(ObjectId(state));
      }
      match.stateId = {
        $in: stateIds
      };

      //converting DistrictId into ObjectId
      for (let district of param.districtIds) {
        districtIds.push(ObjectId(district))
      }
      match.districtId = {
        $in: districtIds
      };

      group.stateId = "$stateId";
      group.districtId = "$districtId";

      project.stateId = "$_id.stateId";
      project.districtId = "$_id.districtId";

      lookupState = {
        "from": "State",
        "localField": "stateId",
        "foreignField": "_id",
        "as": "states"
      };
      lookupDistrict = {
        "from": "District",
        "localField": "districtId",
        "foreignField": "_id",
        "as": "districts"
      };
      TargetCollection.aggregate({
        $match: match,
      }, {
        $group: {
          _id: group,
          "totalTarget": {
            "$sum": "$targetQuantity"
          }
        }
      }, {
        $project: project
      }, {
        $lookup: lookupState,
      }, {
        $lookup: lookupDistrict
      }, {
        $project: {
          "totalTarget": 1,
          "targetMonth": 1,
          "targetYear": 1,
          "stateId": 1,
          "districtId": 1,
          "stateName": {
            $arrayElemAt: ["$states.stateName", 0]
          },
          "districtName": {
            $arrayElemAt: ["$districts.districtName", 0]
          }
        }
      }, function(err, result) {
        if (err) {
            sendMail.sendMail({ collectionName: 'Target', errorObject: err, paramsObject: param, methodName: 'getTargetDetails' });
            return cb(false, err);
        }
        return cb(false, result)
      })
    }
  };

  Target.remoteMethod(
    'getTargetDetails', {
      description: 'getTargetDetails',
      accepts: [{
        arg: 'param',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }
    }
  );
	//------------------------------
    function monthNameAndYearBTWTwoDate(fromDate, toDate) {
        let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let arr = [];
        let datFrom = new Date(fromDate);
        let datTo = new Date(toDate);
        let fromYear = datFrom.getFullYear();
        let toYear = datTo.getFullYear();
        let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
        for (let i = datFrom.getMonth(); i <= diffYear; i++) {
            arr.push({
                monthName: monthNames[i % 12],
                month: (i % 12) + 1,
                year: Math.floor(fromYear + (i / 12)),
                endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
            });
        }
        return arr;
    }


    Target.getTargetFinancialYearWise = function (params, cb) {
        var self = this;
        var TargetCollection = self.getDataSource().connector.collection(Target.modelName);
        let match = {};
        let stateIds = [];
        let districtIds = []
        let users = [];
        let userIds = [];
        let fromdate=params.year[0]+"-04-01";
        let toDate=params.year[1]+"-03-31";
        let group={};
        let lookup={};
        let project={};
        let group2={};
        let sort={};
        let project2={};
        let group3={};
        async.parallel(
            {
                match: function (cb) {
                    if (
                        params.type === "State" ||
                        params.type === "Headquarter"
                    ) {
                        for (let i = 0; i < params.stateId.length; i++) {
                            stateIds.push(ObjectId(params.stateId[i]));
                        }
                        match = {
                            companyId: ObjectId(params.companyId),
                            stateId: {
                                $in: stateIds
                            },
                            fromDate: {$gte:new Date(fromdate)},
                            toDate: {$lte:new Date(toDate)}     
                                        };
                       
                        if (params.type === "Headquarter") {
                            for (let i = 0; i < params.districtId.length; i++) {
                                districtIds.push(ObjectId(params.districtId[i]));
                            }
                            match = {
                                companyId: ObjectId(params.companyId),
                                stateId: {
                                    $in: stateIds
                                },
                                districtId: {
                                    $in: districtIds
                                },
                                fromDate: {$gte:new Date(fromdate)},
                                toDate: {$lte:new Date(toDate)}     

                            };
                        }
                        cb(false, match);
                    } if (params.type === "Employee Wise") {
                       
                        if (params.designation > 1) {
                            let where = {
                                supervisorId: params.userId[0],
                                companyId: params.companyId,
                                type: "lower",
                            };
                              // adding manger's self id also ...
                              for (let i = 0; i < params.userId.length; i++) {
                                userIds.push(params.userId[i]); // adding manger's self id also ...
                                }
                            Target.app.models.Hierarchy.getManagerHierarchy(
                                where,
                               async function (err, hierarchy) {
                                         //-------------for the pool concepts------
                            if(hierarchy.length>0){
                                for (let i = 0; i < hierarchy.length; i++) {
                                    let obj={
                                        where:{
                                                companyId:params.companyId,
                                                status:true,
                                                mappedDistrictId:hierarchy[i].districtId
                                            }
                                    }
                              let mappedData=await Target.app.models.PoolHeadquarterLinking.find(obj);
                              if (mappedData.length>0) {
                                hierarchy[i]["mappedWithPoolHQ"]=mappedData[0].districtId
                               }
                            }
                            let PoolUser=[];
                            let groupPoolUsers=[];
                             hierarchy.forEach(element => {
                                    if(element.hasOwnProperty("mappedWithPoolHQ")){
                                        PoolUser.push({userId:element.userId,poolDistrictId:element.mappedWithPoolHQ,disName:element.districtName,name:element.name})
                                    }else{
                                        userIds.push(ObjectId(element.userId));
                                    }
                                 });
                                 //----------------grouping the pool IDS-------------------------
                                 PoolUser.forEach(item=>{
                                    const index = groupPoolUsers.findIndex(x=>(x.poolDistrictId).toString() == (item.poolDistrictId)).toString() 
                                    if(index == -1){
                                        groupPoolUsers.push(item);
                                    }
                                    })
                                    console.log("before",userIds.length);
                                    groupPoolUsers.forEach(element => {
                                        userIds.push(ObjectId(element.userId));
                                    });
                                 //---------------------------------------
                                   console.log("after",userIds.length);
                                 //---------------------------------------

                            }
                            //----------------------
                            



                                    
                                    // for (let i = 0; i < hierarchy.length; i++) {
                                    //     userIds.push(hierarchy[i].userId);
                                    // }
                                    
                                  
                                    match = {
                                        companyId: ObjectId(params.companyId),
                                        userId: {
                                            $in: userIds
                                        },
                                        fromDate: {$gte:new Date(fromdate)},
                                          toDate: {$lte:new Date(toDate)}     
                                    };
                                    cb(false, match);
                                }
                            );
                        } else {
                            for (let i = 0; i < params.userId.length; i++) {
                                users.push(ObjectId(params.userId[i]));
                            }
                            
                            match = {
                                companyId: ObjectId(params.companyId),
                                userId: {
                                    $in: users
                                },
                                fromDate: {$gte:new Date(fromdate)},
                                toDate: {$lte:new Date(toDate)}     
                            };
                            cb(false, match);
                        }
                    }
                }
            },
            function (err, result) {
                if (err) {
                    return cb(err)
                }
                if (result != undefined) {
                  
                    if(params.type=="State"){
                        group={
                            _id: {
                                productId: "$productId",
                                stateId: "$stateId",
                                targetMonth: "$targetMonth",
                                targetYear: "$targetYear"
                            },
                            targetQty: {
                                $sum: "$targetQuantity"
                            }
                        }
                        lookup={
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "stateData"
                        };
                        project={
                            stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                            stateId: "$_id.stateId",
                            productName: { $arrayElemAt: ["$prod.productName", 0] },
                            productId: "$_id.productId",
                            creditValue: { $arrayElemAt: ["$prod.creditValue", 0] },
                            month: "$_id.targetMonth",
                            year: "$_id.targetYear",
                            targetQty: 1,
                        }
                        group2={
                            _id: {
                                stateId: "$stateId",
                                productId: "$productId",
                                stateName: "$stateName",
                                productName: "$productName",
                                month: "$month",
                                year: "$year"
                            },
                            data: {
                                $push: {
                                    month: "$month",
                                    year: "$year",
                                    targetQty: "$targetQty",
                                    creditValue:"$creditValue"
                                }
                            },
                            totalTargetQty: {
                                $sum: "$targetQty"
                            }
                        }
                        project2={

                            stateId: "$_id.stateId",
                            stateName: "$_id.stateName",
                            productName: "$_id.productName",
                            month: "$_id.month",
                            year: "$_id.year",
                            data: "$data",
                            totalTargetQty: 1
                        }
                        group3={
                            _id: {
                                stateId: "$stateId",
                                stateName: "$stateName",
                                productName: "$productName"
                            },
                            data: { $push: { month: "$month", year: "$year", targetQty: { $arrayElemAt: ["$data.targetQty", 0] },creditValue: { $arrayElemAt: ["$data.creditValue", 0] } } },
                            totalTargetQty: {
                                $sum: "$totalTargetQty"
                            },
                        };
                       sort= {
                            "year":1,
                             month:1,
                             stateName:1
                            }
                    }
                    if(params.type=="Headquarter"){
                        group={
                            _id:{
                                productId:"$productId",
                                districtId:"$districtId",
                                targetMonth:"$targetMonth",
                                targetYear:"$targetYear"
                              },
                              targetQty:{
                               $sum:"$targetQuantity"
                              }
                        }
                        lookup={
                            "from" : "District",
                           "localField" : "_id.districtId",
                             "foreignField" : "_id",
                             "as" : "disData"
                        };
                        project={
                            districtName:{$arrayElemAt: ["$disData.districtName", 0] },
                            districtId:"$_id.districtId",
                            productName:{ $arrayElemAt: ["$prod.productName", 0] },
                            productId:"$_id.productId",
                            creditValue:{$arrayElemAt: ["$prod.creditValue", 0] },
                            month:"$_id.targetMonth",
                            year:"$_id.targetYear",
                            targetQty:1,
                        }
                        group2={
                            _id: {
                                districtId: "$districtId",
                                productId: "$productId",
                                districtName:"$districtName",
                                productName: "$productName",
                                month: "$month",
                                year: "$year"
                            },
                            data: {
                                $push: {
                                    month: "$month",
                                    year: "$year",
                                    targetQty: "$targetQty",
                                    creditValue:"$creditValue"
                                }
                            },
                            totalTargetQty: {
                                $sum: "$targetQty"
                            }
                        }
                        project2={

                            districtId:"$_id.districtId",
                            districtName: "$_id.districtName",
                            productName: "$_id.productName",
                            month: "$_id.month",
                            year: "$_id.year",
                            data: "$data",
                            totalTargetQty: 1
                        }
                        group3={
                            _id: {
                                districtId:"$districtId",
                                districtName: "$districtName",
                                productName: "$productName"
                            },
                            data: { $push: { month: "$month", year: "$year", targetQty: { $arrayElemAt: ["$data.targetQty", 0] },creditValue: { $arrayElemAt: ["$data.creditValue", 0] } } },
                            totalTargetQty: {
                                $sum: "$totalTargetQty"
                            },
                        }
                        sort= {
                            "year":1,
                             month:1,
                             districtName:1
                            }
                    }
                    if(params.type=="Employee Wise"){
                        group={
                            _id:{
                                productId:"$productId",
                                targetMonth:"$targetMonth",
                                targetYear:"$targetYear"
                              },
                              targetQty:{
                               $sum:"$targetQuantity"
                              }
                        }
                        if(params.designation==1){
                            group._id["userId"]="$userId";
                        }
                        lookup={
                            "from" : "UserInfo",
                           "localField" : "_id.userId",
                             "foreignField" : "userId",
                             "as" : "userData"
                        };
                        project={
                            userName:{$arrayElemAt: ["$userData.name", 0] },
                            userId:"$_id.userId",
                            productName:{ $arrayElemAt: ["$prod.productName", 0] },
                            productId:"$_id.productId",
                            creditValue:{$arrayElemAt: ["$prod.creditValue", 0] },
                            month:"$_id.targetMonth",
                            year:"$_id.targetYear",
                            targetQty:1,
                        }
                        group2={
                            _id: {
                                userId: "$userId",
                                productId: "$productId",
                                userName:"$userName",
                                productName: "$productName",
                                month: "$month",
                                year: "$year"
                            },
                            data: {
                                $push: {
                                    month: "$month",
                                    year: "$year",
                                    targetQty: "$targetQty",
                                    creditValue:"$creditValue"
                                }
                            },
                            totalTargetQty: {
                                $sum: "$targetQty"
                            }
                        }
                        project2={

                            userId:"$_id.userId",
                            userName: "$_id.userName",
                            productName: "$_id.productName",
                            month: "$_id.month",
                            year: "$_id.year",
                            data: "$data",
                            totalTargetQty: 1
                        }
                        group3={
                            _id: {
                                userId:"$userId",
                                userName: "$userName",
                                productName: "$productName"
                            },
                            data: { $push: { month: "$month", year: "$year", targetQty: { $arrayElemAt: ["$data.targetQty", 0] },creditValue: { $arrayElemAt: ["$data.creditValue", 0] } } },
                            totalTargetQty: {
                                $sum: "$totalTargetQty"
                            },
                        };
                        sort= {
                            "year":1,
                             month:1,
                             userName:1
                            }
                    }
                    TargetCollection.aggregate(
                            // Stage 1
                            {
                                $match: result.match
                            },
                            // Stage 2
                            {
                                $group: group
                            },

                            // Stage 3
                            {
                                $lookup: lookup
                            },

                            // Stage 4
                            {
                                $lookup: {
                                    "from": "Products",
                                    "localField": "_id.productId",
                                    "foreignField": "_id",
                                    "as": "prod"
                                }
                            },

                            // Stage 5
                            {
                                $project: project
                            },

                            // Stage 6
                            {
                                $group:group2
                            },

                            // Stage 7
                            {
                                $project: project2
                            },
                            {
                                $sort:sort
                                       
                            },
                            {
                                $group: group3
                            },
                            function(err,result) {
                                if(err){
                                    return cb(err)
                                }
                                if(result.length>0){
                                    let finalObject = [];
                                    const monthYearArray = monthNameAndYearBTWTwoDate(fromdate,toDate);
                                    result.forEach(targetResult => {
                                        let details = [];
                                        let amount=0
                                        for (const month of monthYearArray) {
                                            const matchedObject = targetResult.data.filter(function (obj) {
                                                return obj.month == month.month 
                                            });
                                            if (matchedObject.length > 0) {
                                                let qty=params.viewType=="unit"? parseInt(matchedObject[0].targetQty):parseInt(matchedObject[0].targetQty)*parseInt(matchedObject[0].creditValue)
                                                 amount += qty;
                                                details.push({month:matchedObject[0].month,year:matchedObject[0].year,targetQty:qty,creditValue:matchedObject[0].creditValue})
                                            } else {
                                                details.push({month:month.month,year:month.year,targetQty:0})
                                            }
                                        }
                                        let key="";
                                        let value="";
                                        if(params.type=="State"){
                                            key="stateName";
                                            value=targetResult._id.stateName;
                                        }if(params.type=="Headquarter"){
                                            key="districtName";
                                            value=targetResult._id.districtName;
                                        }if(params.type=="Employee Wise"){
                                            key="userName";
                                            value=targetResult._id.userName;
                                        }
                                        let totalTargetAmt=params.viewType=="unit"? parseInt(targetResult.totalTargetQty):amount
                                        finalObject.push({
                                             [key]: value,
                                             productName: targetResult._id.productName,
                                             data: details,
                                             totalTarget:totalTargetAmt
                                        });
                                    });
                                    return cb(false, finalObject)
                                }else{
                                    return cb(false, [])
                                }
                            }

                      

                    );


                } else {
                    return cb(false, [])
                }

            }
        );
    }
    Target.remoteMethod(
        'getTargetFinancialYearWise', {
        description: 'Get Target Financial Year Wise',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

};
