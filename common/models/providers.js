﻿var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var NodeGeocoder = require('node-geocoder');
var status = require('../../utils/statusMessage');
'use strict';
module.exports = function (Providers) {
        Providers.observe('after save', function logQuery(ctx, next) {
            if ((ctx.instance && ctx.instance.onLocation == true) && ctx.instance.geoLocation.length > 0) {
                var param = {};
                param = {
                    lat: ctx.instance.geoLocation[0].lat,
                    long: ctx.instance.geoLocation[0].long
                };
                Providers.app.models.longLatInfo.getLatLong(param,
                    function (err, instances) {
                        if (err) {
                            //  next();
                        }
                        if (instances != undefined && instances.length != 0) {
                            next();
                        } else {
                            var options = {
                                provider: 'google',
                                // Optional depending on the providers
                                httpAdapter: 'https', // Default
                                apiKey: 'AIzaSyCKCxPLVooXnPEFe17z4a65p-7DpJmsA2o', // for Mapquest, OpenCage, Google Premier
                                formatter: null // 'gpx', 'string', ...
                            };
                            var geocoder = NodeGeocoder(options);
                            geocoder.reverse({
                                lat: ctx.instance.geoLocation[0].lat,
                                lon: ctx.instance.geoLocation[0].long
                            }, function (err, res) {
                                if (err) {
                                    // next();
                                }
                                if (res != undefined && res.length > 0 && res[0].longitude != undefined) {
                                    var longLatObj = {};
                                    longLatObj = {
                                        country: res[0].country,
                                        city: res[0].city,
                                        address: res[0].formattedAddress,
                                        loc: {
                                            "type": "Point",
                                            "coordinates": [
                                                res[0].latitude,
                                                res[0].longitude
                                            ]
                                        }
                                    }
                                    Providers.app.models.longLatInfo.create(longLatObj,
                                        function (result) {
                                            //console.log("result");console.log(result);
                                            // next();
                                        });
                                }
                            })
                            next();
                        }
                        // next();
                    });
            } else {
                next();
            }
        })
        Providers.updateAllRecord = function (records, cb) {
            var self = this;
            var response = [];
            var ids = [];
            var ProviderCollection = self.getDataSource().connector.collection(Providers.modelName);
            if (records.length === 0 || records === "") {
                var err = new Error('records is undefined or empty');
                err.statusCode = 404;
                err.code = 'RECORDS ARE NOT FOUND';
                return cb(err);
            }
            let returnData = records;
            for (var w = 0; w < records.length; w++) {
                let set = {};
                set = records[w];
                if (set.hasOwnProperty("stateId") === true) {
                    set["stateId"] = ObjectId(set["stateId"]);
                }
                if (set.hasOwnProperty("districtId") === true) {
                    set["districtId"] = ObjectId(set["districtId"]);
                }
                if (set.hasOwnProperty("blockId") === true) {
                    set["blockId"] = ObjectId(set["blockId"]);
                }
                if (set.hasOwnProperty("companyId") === true) {
                    set["companyId"] = ObjectId(set["companyId"]);
                }
                if (set.hasOwnProperty("userId") === true) {
                    set["userId"] = ObjectId(set["userId"]);
                }
                if (set.hasOwnProperty("doa") === true) {
                    set["doa"] = new Date(set["doa"]);
                }
                if (set.hasOwnProperty("dob") === true) {
                    set["dob"] = new Date(set["dob"]);
                }
                if (set.hasOwnProperty("mgrAppDate") === true) {
                    set["mgrAppDate"] = new Date(set["mgrAppDate"]);
                }
                if (set.hasOwnProperty("finalAppDate") === true) {
                    set["finalAppDate"] = new Date(set["finalAppDate"]);
                }
                if (set.hasOwnProperty("appByMgr") === true && set["delByMgr"] != "") {
                    set["appByMgr"] = ObjectId(set["appByMgr"]);
                }
                if (set.hasOwnProperty("finalAppBy") === true && set["delByMgr"] != "") {
                    set["finalAppBy"] = ObjectId(set["finalAppBy"]);
                }
                if (set.hasOwnProperty("mgrDelDate") === true) {
                    set["mgrDelDate"] = new Date(set["mgrDelDate"]);
                }
                if (set.hasOwnProperty("finalDelDate") === true) {
                    set["finalDelDate"] = new Date(set["finalDelDate"]);
                }
                if (set.hasOwnProperty("delByMgr") === true && set["delByMgr"] != "") {
                    set["delByMgr"] = ObjectId(set["delByMgr"]);
                }
                if (set.hasOwnProperty("finalDelBy") === true && set["finalDelBy"] != "") {
                    set["finalDelBy"] = ObjectId(set["finalDelBy"]);
                }
                set["createdAt"] = new Date(set["createdAt"]);
                set["updatedAt"] = new Date(moment.utc().add('1070', 'minutes'));;
                ProviderCollection.update({
                        "_id": ObjectId(records[w].id)
                    }, {
                        $set: set
                    },
                    function (err, result) {
                        //console.log("res");console.log(result);
                        if (err) {
                            console.log("err");
                            console.log(err);
                        }
                    })
            }
            return cb(false, returnData);
        };
        // End DCR Record
        // Update All Data for sync
        Providers.remoteMethod(
            'updateAllRecord', {
                description: 'update and send response all data',
                accepts: [{
                    arg: 'records',
                    type: 'array',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'post'
                }
            }
        );
        Providers.getProviderToBeMapped = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let districtIds = [];
            for (let i = 0; i < param.districts.length; i++) {
                districtIds.push(ObjectId(param.districts[i]));
            }
            ProviderCollection.aggregate( // Stage 1
                {
                    $match: {
                        companyId: ObjectId(param.companyId),
                        districtId: {
                            $in: districtIds
                        },
                        providerType: param.providerType,
                        status: true
                    }
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "District",
                        "localField": "districtId",
                        "foreignField": "_id",
                        "as": "DistrictData"
                    }
                },
                // Stage 3
                {
                    $unwind: "$DistrictData"
                },
                // Stage 4
                {
                    $project: {
                        districtName: "$DistrictData.districtName",
                        stockiestName: "$providerName",
                        districtId: "$districtId",
                        blockId: "$blockId"
                    }
                }, {
                    $sort: {
                        districtName: 1
                    }
                },
                function (err, response) {
                    if (response.length > 0) {
                        return cb(false, response);
                    } else {
                        return cb(false, []);
                    }
                })
        }
        Providers.remoteMethod(
            'getProviderToBeMapped', {
                description: 'Getting ProviderToBeMapped List',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //-----------------------PRaveen Kumar(17-12-2018)-------------------------------------
        //    Providers.providerVisitAnalysis = function (param, cb) {
        // 	   console.log("param : ",param);
        //         var self = this;
        //         var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
        //         var DCRMasterCollection = this.getDataSource().connector.collection(Providers.app.models.DCRMaster.modelName);
        //         var userAreaMappingCollection = this.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
        //         let fromDate = "";
        //         let toDate = "";
        //         if (param.fiscalYear == "yearly") {
        //             fromDate = param.year + "-01-01";
        //             toDate = param.year + "-12-31";
        //         } else if (param.fiscalYear == "financialYearly") {
        //             fromDate = param.year[0] + "-04-01";
        //             toDate = param.year[1] + "-03-31";
        //         }
        //         if (param.designationLevel == 1) {
        //             userAreaMappingCollection.aggregate({
        //                 $match: {
        //                     companyId: ObjectId(param.companyId),
        //                     userId: ObjectId(param.userId)
        //                 }
        //             }, {
        //                     $group: {
        //                         _id: {},
        //                         areaIds: {
        //                             $addToSet: "$areaId"
        //                         }
        //                     }
        //                 }, function (err, areaMappingResult) {
        //                     if (err) {
        //                         console.log(err)
        //                     }
        // 						let providerMatch={
        // 			               blockId: {
        //                                 $in: areaMappingResult[0].areaIds
        //                             },
        //                             providerType: {$in:param.type},
        //                             appStatus: "approved",
        //                             status: {
        //                                 $in: [true, false]
        //                             },
        //                             finalAppDate: {
        //                                 $lte: new Date(toDate)
        //                             },
        //                             $or: [{
        //                                 finalDelDate: {
        //                                     $gte: new Date(fromDate),
        //                                     $lte: new Date(toDate),
        //                                 }
        //                             }, {
        //                                 finalDelDate: { $eq: new Date("1900-01-01T00:00:00.000Z") }
        //                             }]
        // 		}
        // 		if(param.category.length>0){
        // 			providerMatch.category={
        // 								$in :param.category
        // 							}		
        // 		}
        //                     ProviderCollection.aggregate({
        //                         $match: providerMatch
        //                     }, {
        //                             $lookup: {
        //                                 "from": "State",
        //                                 "localField": "stateId",
        //                                 "foreignField": "_id",
        //                                 "as": "stateDetails"
        //                             }
        //                         }, {
        //                             $lookup: {
        //                                 "from": "District",
        //                                 "localField": "districtId",
        //                                 "foreignField": "_id",
        //                                 "as": "districtDetails"
        //                             }
        //                         }, {
        //                             $lookup: {
        //                                 "from": "Area",
        //                                 "localField": "blockId",
        //                                 "foreignField": "_id",
        //                                 "as": "areaDetails"
        //                             }
        //                         }, {
        //                             $lookup: {
        //                                 "from": "UserAreaMapping",
        //                                 "localField": "blockId",
        //                                 "foreignField": "areaId",
        //                                 "as": "userAreaDetails"
        //                             }
        //                         }, {
        //                             $unwind: "$userAreaDetails"
        //                         }, {
        //                             $match: {
        //                                 "userAreaDetails.appStatus": {
        //                                     $ne: "pending"
        //                                 }
        //                             }
        //                         }, {
        //                             $group: {
        //                                 _id: {},
        //                                 unlistedDoctor: {
        //                                     $addToSet: {
        //                                         $cond: [{
        //                                             $and: [{
        //                                                 $eq: ["$appStatus", "unlisted"]
        //                                             },
        //                                             {
        //                                                 $eq: ["$submitBy", ObjectId(param.userId)]
        //                                             }
        //                                             ]
        //                                         }, "$$ROOT", "$abc"]
        //                                     }
        //                                 },
        //                                 allDoctor: {
        //                                     $addToSet: {
        //                                         $cond: [{
        //                                             $and: [{
        //                                                 $ne: ["$appStatus", "unlisted"]
        //                                             }]
        //                                         }, "$$ROOT", "$abc"]
        //                                     }
        //                                 }
        //                             }
        //                         }, {
        //                             $project: {
        //                                 _id: 0,
        //                                 totalDoctors: { $concatArrays: ["$unlistedDoctor", "$allDoctor"] }
        //                             }
        //                         }, {
        //                             $unwind: "$totalDoctors"
        //                         },
        //                         function (err, providerResult) {
        //                             if (err) {
        //                                 console.log(err);
        //                             }
        //                             for (let i = 0; i < providerResult.length; i++) {
        //                                 providerResult[i] = providerResult[i].totalDoctors;
        //                             }
        //                             DCRMasterCollection.aggregate({
        //                                 $match: {
        //                                     "companyId": ObjectId(param.companyId),
        //                                     "submitBy": ObjectId(param.userId),
        //                                     "dcrDate": {
        //                                         $gte: new Date(fromDate),
        //                                         $lte: new Date(toDate)
        //                                     }
        //                                 }
        //                             }, {
        //                                     $lookup: {
        //                                         "from": "DCRProviderVisitDetails",
        //                                         "localField": "_id",
        //                                         "foreignField": "dcrId",
        //                                         "as": "dcrVisitDetails"
        //                                     }
        //                                 }, {
        //                                     $unwind: "$dcrVisitDetails"
        //                                 }, {
        //                                     $match: {
        //                                         "dcrVisitDetails.providerType": {$in:param.type}
        //                                     }
        //                                 }, {
        //                                     $project: {
        //                                         "providerId": "$dcrVisitDetails.providerId",
        //                                         "month": {
        //                                             $month: "$dcrVisitDetails.dcrDate"
        //                                         },
        //                                         "year": {
        //                                             $year: "$dcrVisitDetails.dcrDate"
        //                                         },
        //                                          "day": {
        //                                                     day: "$dcrVisitDetails.dcrDate"
        //                                                 }
        //                                     }
        //                                 }, {
        //                                     $group: {
        //                                         _id: {
        //                                             providerId: "$providerId",
        //                                             month: "$month",
        //                                             year: "$year"
        //                                         },
        //                                         totalVisit: {
        //                                              $addToSet : "$day"
        //                                         }
        //                                     }
        //                                 }, function (err, visitedProviderDetails) {
        //                                     if (err) {
        //                                         console.log(err);
        //                                     }
        //                                     let finalObject = [];
        //                                     for (let i = 0; i < providerResult.length; i++) {
        //                                         let currentProviderStatus = "";
        //                                         if (providerResult[i].appStatus == "unlisted") {
        //                                             currentProviderStatus = "Unlisted";
        //                                         } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
        //                                             currentProviderStatus = "Active";
        //                                         } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
        //                                             currentProviderStatus = "InActive";
        //                                         }
        //                                         let currentAreaStatus = "";
        //                                         if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
        //                                             currentAreaStatus = "Active";
        //                                         } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
        //                                             currentAreaStatus = "InActive";
        //                                         }
        //                                         let matchedObj = visitedProviderDetails.filter(function (obj) {
        //                                             let providerId = JSON.stringify(providerResult[i]._id);
        //                                             let visitedProviderId = JSON.stringify(obj._id.providerId);
        //                                             return visitedProviderId == providerId;
        //                                         });
        //                                         if (matchedObj.length > 0) {
        //                                             let providerVisitData ="";
        //                                             if (param.fiscalYear == "yearly") {
        //                                                 for (let i = 1; i <= 12; i++) {
        //                                                     let matchedObject = matchedObj.filter(function (obj) {
        //                                                         return i == obj._id.month;
        //                                                     });
        //                                                     if (matchedObject.length > 0) {
        //                                                         //console.log(matchedObject)
        //                                                         providerVisitData = matchedObject[0].totalVisit.join();
        //                                                     } else {
        //                                                         providerVisitData="-----";
        //                                                     }
        //                                                 }
        //                                             } else if (param.fiscalYear == "financialYearly") {
        //                                                 for (let i = 4; i <= 15; i++) {
        //                                                     let month = i;
        //                                                     if (i == 13) {
        //                                                         month = 1;
        //                                                     } else if (i == 14) {
        //                                                         month = 2;
        //                                                     } else if (i == 15) {
        //                                                         month = 3;
        //                                                     }
        //                                                     let matchedObject = matchedObj.filter(function (obj) {
        //                                                         //console.log(obj._id)
        //                                                         //console.log(i + " ---- " + obj._id.month)
        //                                                         return month == obj._id.month;
        //                                                     });
        //                                                     if (matchedObject.length > 0) {
        //                                                         //console.log(matchedObject)
        //                                                         providerVisitData= matchedObject[0].totalVisit.join();
        //                                                     } else {
        //                                                         providerVisitData = "-----";
        //                                                     }
        //                                                 }
        //                                             }
        //                                             finalObject.push({
        //                                                 stateName: providerResult[i].stateDetails[0].stateName,
        //                                                 districtName: providerResult[i].districtDetails[0].districtName,
        //                                                 areaName: providerResult[i].areaDetails[0].areaName,
        //                                                 providerName: providerResult[i].providerName,
        //                                                 providerCode: providerResult[i].providerCode,
        //                                                 category: providerResult[i].category,
        //                                                 specialization: providerResult[i].specialization,
        //                                                 degree: providerResult[i].degree,
        //                                                 providerStatus: currentProviderStatus,
        //                                                 areaStatus: currentAreaStatus,
        //                                                 providerType:providerResult[i].providerType,
        //                                                 data: providerVisitData
        //                                             });
        //                                             //console.log(matchedObj)
        //                                         } else {
        //                                             finalObject.push({
        //                                                 stateName: providerResult[i].stateDetails[0].stateName,
        //                                                 districtName: providerResult[i].districtDetails[0].districtName,
        //                                                 areaName: providerResult[i].areaDetails[0].areaName,
        //                                                 providerName: providerResult[i].providerName,
        //                                                 providerCode: providerResult[i].providerCode,
        //                                                 category: providerResult[i].category,
        //                                                 specialization: providerResult[i].specialization,
        //                                                 degree: providerResult[i].degree,
        //                                                 providerStatus: currentProviderStatus,
        //                                                 areaStatus: currentAreaStatus,
        // 											    providerType:providerResult[i].providerType,
        //                                                 data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
        //                                             });
        //                                         }
        //                                     }
        //                                     cb(false, finalObject);
        //                                     //  console.log(finalObject)
        //                                 });
        //                         })
        //                 });
        //         } 
        //          else if (param.designationLevel >= 2) {
        //         let passinObjectForHierarchy = {
        //         supervisorId: param.userId,
        //         companyId: param.companyId,
        //         type: 'lower'
        //         };
        //         Providers.app.models.Hierarchy.getManagerHierarchy(
        //         passinObjectForHierarchy,
        //         function (err, hierarchyResult) {
        //         if (err) {
        //         console.log(err);
        //         }
        //         let passingUserIds = [];
        //         for (let i = 0; i < hierarchyResult.length; i++) {
        //         passingUserIds.push(hierarchyResult[i].userId);
        //         }
        //         //console.log("Provider Visit Analysis Hierarchy len : " + hierarchyResult.length)
        //         //let passingUserIds = [ObjectId("5b33ca57f87a57439085de2a"), ObjectId("5bcd0b949ef1c86b7d025cc5"), ObjectId("5bce1cc8d0a43a0b1e4e23c4")]
        //         userAreaMappingCollection.aggregate({
        //         $match: {
        //         companyId: ObjectId(param.companyId),
        //         userId: { $in: passingUserIds },
        //         appStatus: {
        //         $ne: "pending"
        //         },
        //         status: {
        //         $in: [true, false]
        //         }
        //         }
        //         }, {
        //         $group: {
        //         _id: {},
        //         areaIds: {
        //         $addToSet: "$areaId"
        //         }
        //         }
        //         }, function (err, areaMappingResult) {
        //         if (err) {
        //         console.log(err)
        //         }
        //         //console.log("Visit Analysis Area Length : " + areaMappingResult[0].areaIds.length)
        //         let providerMatch={
        //         blockId: {
        //         $in: areaMappingResult[0].areaIds
        //         },
        //         providerType: param.type,
        //         appStatus: "approved",
        //         status: {
        //         $in: [true, false]
        //         },
        //         finalAppDate: {
        //         $lte: new Date(toDate)
        //         },
        //         $or: [{
        //         finalDelDate: {
        //         $gte: new Date(fromDate),
        //         $lte: new Date(toDate),
        //         }
        //         }, {
        //         finalDelDate: { $eq: new Date("1900-01-01T00:00:00.000Z") }
        //         }]
        //         }
        //         if(param.category.length>0){
        //         providerMatch.category={
        //         $in :param.category
        //         }
        //         }
        //         ProviderCollection.aggregate({
        //         $match: providerMatch
        //         }, {
        //         $lookup: {
        //         "from": "State",
        //         "localField": "stateId",
        //         "foreignField": "_id",
        //         "as": "stateDetails"
        //         }
        //         }, {
        //         $lookup: {
        //         "from": "District",
        //         "localField": "districtId",
        //         "foreignField": "_id",
        //         "as": "districtDetails"
        //         }
        //         }, {
        //         $lookup: {
        //         "from": "Area",
        //         "localField": "blockId",
        //         "foreignField": "_id",
        //         "as": "areaDetails"
        //         }
        //         }, {
        //         $lookup: {
        //         "from": "UserAreaMapping",
        //         "localField": "blockId",
        //         "foreignField": "areaId",
        //         "as": "userAreaDetails"
        //         }
        //         }, {
        //         $unwind: "$userAreaDetails"
        //         }, {
        //         $match: {
        //         "userAreaDetails.appStatus": {
        //         $ne: "pending"
        //         }
        //         }
        //         }, {
        //         $group: {
        //         _id: {},
        //         unlistedDoctor: {
        //         $addToSet: {
        //         $cond: [{
        //         $and: [{
        //         $eq: ["$appStatus", "unlisted"]
        //         },
        //         {
        //         $eq: ["$submitBy", ObjectId(param.userId)]
        //         }
        //         ]
        //         }, "$$ROOT", "$abc"]
        //         }
        //         },
        //         allDoctor: {
        //         $addToSet: {
        //         $cond: [{
        //         $and: [{
        //         $ne: ["$appStatus", "unlisted"]
        //         }]
        //         }, "$$ROOT", "$abc"]
        //         }
        //         }
        //         }
        //         }, {
        //         $project: {
        //         _id: 0,
        //         totalDoctors: { $concatArrays: ["$unlistedDoctor", "$allDoctor"] }
        //         }
        //         }, {
        //         $unwind: "$totalDoctors"
        //         },
        //         function (err, providerResult) {
        //         if (err) {
        //         console.log(err);
        //         }
        //         //console.log("Provider Length : " + providerResult.length)
        //         for (let i = 0; i < providerResult.length; i++) {
        //         providerResult[i] = providerResult[i].totalDoctors;
        //         }
        //         DCRMasterCollection.aggregate({
        //         $match: {
        //         "companyId": ObjectId(param.companyId),
        //         "submitBy": ObjectId(param.userId),
        //         "dcrDate": {
        //         $gte: new Date(fromDate),
        //         $lte: new Date(toDate)
        //         }
        //         }
        //         }, {
        //         $lookup: {
        //         "from": "DCRProviderVisitDetails",
        //         "localField": "_id",
        //         "foreignField": "dcrId",
        //         "as": "dcrVisitDetails"
        //         }
        //         }, {
        //         $unwind: "$dcrVisitDetails"
        //         }, {
        //         $match: {
        //         "dcrVisitDetails.providerType":{$in : param.type}
        //         }
        //         }, {
        //         $project: {
        //         "providerId": "$dcrVisitDetails.providerId",
        //         "month": {
        //         $month: "$dcrVisitDetails.dcrDate"
        //         },
        //         "year": {
        //         $year: "$dcrVisitDetails.dcrDate"
        //         },
        //         "day": {
        //         day: "$dcrVisitDetails.dcrDate"
        //         }
        //         }
        //         }, {
        //         $group: {
        //         _id: {
        //         providerId: "$providerId",
        //         month: "$month",
        //         year: "$year"
        //         },
        //         totalVisit: {
        //         $addToSet : "$day"
        //         }
        //         }
        //         }, function (err, visitedProviderDetails) {
        //         if (err) {
        //         console.log(err);
        //         }
        //         let finalObject = [];
        //         for (let i = 0; i < providerResult.length; i++) {
        //         let currentProviderStatus = "";
        //         if (providerResult[i].appStatus == "unlisted") {
        //         currentProviderStatus = "Unlisted";
        //         } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
        //         currentProviderStatus = "Active";
        //         } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
        //         currentProviderStatus = "InActive";
        //         }
        //         let currentAreaStatus = "";
        //         if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
        //         currentAreaStatus = "Active";
        //         } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
        //         currentAreaStatus = "InActive";
        //         }
        //         let matchedObj = visitedProviderDetails.filter(function (obj) {
        //         let providerId = JSON.stringify(providerResult[i]._id);
        //         let visitedProviderId = JSON.stringify(obj._id.providerId);
        //         return visitedProviderId == providerId;
        //         });
        //         if (matchedObj.length > 0) {
        //         let providerVisitData = [];
        //         let VisitedDate = "";
        //         if (param.fiscalYear == "yearly") {
        //         for (let i = 1; i <= 12; i++) {
        //         let matchedObject = matchedObj.filter(function (obj) {
        //         return i == obj._id.month;
        //         });
        //         if (matchedObject.length > 0) {
        //         for(let i=0;i<matchedObject[0].dates.length;i++){
        //         if(i==0){
        //         VisitedDate=moment(matchedObject[0].dates[i].day).format("DD-MMM-YYYY").toString();
        //         }else{
        //         VisitedDate=VisitedDate+","+moment(matchedObject[0].dates[i].day).format("DD-MMM-YYYY").toString();
        //         }
        //         }
        //         providerVisitData.push(VisitedDate)
        //         } else {
        //         VisitedDate="-----";
        //         providerVisitData.push(VisitedDate)
        //         }
        //         }
        //         } else if (param.fiscalYear == "financialYearly") {
        //         for (let i = 4; i <= 15; i++) {
        //         let month = i;
        //         if (i == 13) {
        //         month = 1;
        //         } else if (i == 14) {
        //         month = 2;
        //         } else if (i == 15) {
        //         month = 3;
        //         }
        //         let matchedObject = matchedObj.filter(function (obj) {
        //         //console.log(obj._id)
        //         //console.log(i + " ---- " + obj._id.month)
        //         return month == obj._id.month;
        //         });
        //         if (matchedObject.length > 0) {
        //         for(let i=0;i<matchedObject[0].dates.length;i++){
        //         if(i==0){
        //         VisitedDate=moment(matchedObject[0].dates[i].day).format("DD-MMM-YYYY").toString();
        //         }else{
        //         VisitedDate=VisitedDate+","+moment(matchedObject[0].dates[i].day).format("DD-MMM-YYYY").toString();
        //         }
        //         }
        //         providerVisitData.push(VisitedDate)
        //         } else {
        //         VisitedDate="-----";
        //         providerVisitData.push(VisitedDate)
        //         }
        //         }
        //         }
        //         finalObject.push({
        //         stateName: providerResult[i].stateDetails[0].stateName,
        //         districtName: providerResult[i].districtDetails[0].districtName,
        //         areaName: providerResult[i].areaDetails[0].areaName,
        //         providerName: providerResult[i].providerName,
        //         providerCode: providerResult[i].providerCode,
        //         providerType:providerResult[i].providerType,
        //         category: providerResult[i].category,
        //         specialization: providerResult[i].specialization,
        //         degree: providerResult[i].degree,
        //         status: currentProviderStatus,
        //         providerStatus: currentProviderStatus,
        //         areaStatus: currentAreaStatus,
        //         data: providerVisitData
        //         });
        //         //console.log(matchedObj)
        //         } else {
        //         finalObject.push({
        //         stateName: providerResult[i].stateDetails[0].stateName,
        //         districtName: providerResult[i].districtDetails[0].districtName,
        //         areaName: providerResult[i].areaDetails[0].areaName,
        //         providerName: providerResult[i].providerName,
        //         providerCode: providerResult[i].providerCode,
        //         category: providerResult[i].category,
        //         providerType:providerResult[i].providerType,
        //         specialization: providerResult[i].specialization,
        //         degree: providerResult[i].degree,
        //         status: currentProviderStatus,
        //         providerStatus: currentProviderStatus,
        //         areaStatus: currentAreaStatus,
        //         data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
        //         });
        //         }
        //         }
        //         cb(false, finalObject);
        //         // console.log(finalObject)
        //         });
        //         })
        //         });
        //         });
        //         }


        //         // else if (param.designationLevel >= 2) {
        //         //     let passinObjectForHierarchy = {
        //         //         supervisorId: param.userId,
        //         //         companyId: param.companyId,
        //         //         type: 'lower'
        //         //     };
        //         //     Providers.app.models.Hierarchy.getManagerHierarchy(
        //         //         passinObjectForHierarchy,
        //         //         function (err, hierarchyResult) {
        //         //             if (err) {
        //         //                 console.log(err);
        //         //             }
        //         //             let passingUserIds = [];
        //         //             for (let i = 0; i < hierarchyResult.length; i++) {
        //         //                 passingUserIds.push(hierarchyResult[i].userId);
        //         //             }
        //         //             //console.log("Provider Visit Analysis Hierarchy len : " + hierarchyResult.length)
        //         //             //let passingUserIds = [ObjectId("5b33ca57f87a57439085de2a"), ObjectId("5bcd0b949ef1c86b7d025cc5"), ObjectId("5bce1cc8d0a43a0b1e4e23c4")]
        //         //             userAreaMappingCollection.aggregate({
        //         //                 $match: {
        //         //                     companyId: ObjectId(param.companyId),
        //         //                     userId: { $in: passingUserIds },
        //         //                     appStatus: {
        //         //                         $ne: "pending"
        //         //                     },
        //         //                     status: {
        //         //                         $in: [true, false]
        //         //                     }
        //         //                 }
        //         //             }, {
        //         //                     $group: {
        //         //                         _id: {},
        //         //                         areaIds: {
        //         //                             $addToSet: "$areaId"
        //         //                         }
        //         //                     }
        //         //                 }, function (err, areaMappingResult) {
        //         //                     if (err) {
        //         //                         console.log(err)
        //         //                     }
        //         //                     //console.log("Visit Analysis Area Length : " + areaMappingResult[0].areaIds.length)
        //         //                    let providerMatch={
        //         //                             blockId: {
        //         //                                 $in: areaMappingResult[0].areaIds
        //         //                             },
        //         //                             providerType: param.type,
        //         //                             appStatus: "approved",
        //         //                             status: {
        //         //                                 $in: [true, false]
        //         //                             },
        //         //                             finalAppDate: {
        //         //                                 $lte: new Date(toDate)
        //         //                             },
        //         //                             $or: [{
        //         //                                 finalDelDate: {
        //         //                                     $gte: new Date(fromDate),
        //         //                                     $lte: new Date(toDate),
        //         //                                 }
        //         //                             }, {
        //         //                                 finalDelDate: { $eq: new Date("1900-01-01T00:00:00.000Z") }
        //         //                             }]
        //         //                         }
        // 		// 						if(param.category.length>0){
        // 		// 	                        providerMatch.category={
        // 		// 						   $in :param.category
        // 		// 					}		
        // 		//                              }
        // 		// 				   ProviderCollection.aggregate({
        //         //                         $match: providerMatch
        //         //                     }, {
        //         //                             $lookup: {
        //         //                                 "from": "State",
        //         //                                 "localField": "stateId",
        //         //                                 "foreignField": "_id",
        //         //                                 "as": "stateDetails"
        //         //                             }
        //         //                         }, {
        //         //                             $lookup: {
        //         //                                 "from": "District",
        //         //                                 "localField": "districtId",
        //         //                                 "foreignField": "_id",
        //         //                                 "as": "districtDetails"
        //         //                             }
        //         //                         }, {
        //         //                             $lookup: {
        //         //                                 "from": "Area",
        //         //                                 "localField": "blockId",
        //         //                                 "foreignField": "_id",
        //         //                                 "as": "areaDetails"
        //         //                             }
        //         //                         }, {
        //         //                             $lookup: {
        //         //                                 "from": "UserAreaMapping",
        //         //                                 "localField": "blockId",
        //         //                                 "foreignField": "areaId",
        //         //                                 "as": "userAreaDetails"
        //         //                             }
        //         //                         }, {
        //         //                             $unwind: "$userAreaDetails"
        //         //                         }, {
        //         //                             $match: {
        //         //                                 "userAreaDetails.appStatus": {
        //         //                                     $ne: "pending"
        //         //                                 }
        //         //                             }
        //         //                         }, {
        //         //                             $group: {
        //         //                                 _id: {},
        //         //                                 unlistedDoctor: {
        //         //                                     $addToSet: {
        //         //                                         $cond: [{
        //         //                                             $and: [{
        //         //                                                 $eq: ["$appStatus", "unlisted"]
        //         //                                             },
        //         //                                             {
        //         //                                                 $eq: ["$submitBy", ObjectId(param.userId)]
        //         //                                             }
        //         //                                             ]
        //         //                                         }, "$$ROOT", "$abc"]
        //         //                                     }
        //         //                                 },
        //         //                                 allDoctor: {
        //         //                                     $addToSet: {
        //         //                                         $cond: [{
        //         //                                             $and: [{
        //         //                                                 $ne: ["$appStatus", "unlisted"]
        //         //                                             }]
        //         //                                         }, "$$ROOT", "$abc"]
        //         //                                     }
        //         //                                 }
        //         //                             }
        //         //                         }, {
        //         //                             $project: {
        //         //                                 _id: 0,
        //         //                                 totalDoctors: { $concatArrays: ["$unlistedDoctor", "$allDoctor"] }
        //         //                             }
        //         //                         }, {
        //         //                             $unwind: "$totalDoctors"
        //         //                         },
        //         //                         function (err, providerResult) {
        //         //                             if (err) {
        //         //                                 console.log(err);
        //         //                             }
        //         //                             //console.log("Provider Length : " + providerResult.length)
        //         //                             for (let i = 0; i < providerResult.length; i++) {
        //         //                                 providerResult[i] = providerResult[i].totalDoctors;
        //         //                             }
        //         //                             DCRMasterCollection.aggregate({
        //         //                                 $match: {
        //         //                                     "companyId": ObjectId(param.companyId),
        //         //                                     "submitBy": ObjectId(param.userId),
        //         //                                     "dcrDate": {
        //         //                                         $gte: new Date(fromDate),
        //         //                                         $lte: new Date(toDate)
        //         //                                     }
        //         //                                 }
        //         //                             }, {
        //         //                                     $lookup: {
        //         //                                         "from": "DCRProviderVisitDetails",
        //         //                                         "localField": "_id",
        //         //                                         "foreignField": "dcrId",
        //         //                                         "as": "dcrVisitDetails"
        //         //                                     }
        //         //                                 }, {
        //         //                                     $unwind: "$dcrVisitDetails"
        //         //                                 }, {
        //         //                                     $match: {
        //         //                                         "dcrVisitDetails.providerType": param.type
        //         //                                     }
        //         //                                 }, {
        //         //                                     $project: {
        //         //                                         "providerId": "$dcrVisitDetails.providerId",
        //         //                                         "month": {
        //         //                                             $month: "$dcrVisitDetails.dcrDate"
        //         //                                         },
        //         //                                         "year": {
        //         //                                             $year: "$dcrVisitDetails.dcrDate"
        //         //                                         },
        //         //                                         "day": {
        //         //                                             day: "$dcrVisitDetails.dcrDate"
        //         //                                         }
        //         //                                     }
        //         //                                 }, {
        //         //                                     $group: {
        //         //                                         _id: {
        //         //                                             providerId: "$providerId",
        //         //                                             month: "$month",
        //         //                                             year: "$year"
        //         //                                         },
        //         //                                         totalVisit: {
        //         //                                              $addToSet : "$day"
        //         //                                         }
        //         //                                     }
        //         //                                 }, function (err, visitedProviderDetails) {
        //         //                                     if (err) {
        //         //                                         console.log(err);
        //         //                                     }
        //         //                                     let finalObject = [];
        //         //                                     for (let i = 0; i < providerResult.length; i++) {
        //         //                                         let currentProviderStatus = "";
        //         //                                         if (providerResult[i].appStatus == "unlisted") {
        //         //                                             currentProviderStatus = "Unlisted";
        //         //                                         } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
        //         //                                             currentProviderStatus = "Active";
        //         //                                         } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
        //         //                                             currentProviderStatus = "InActive";
        //         //                                         }
        //         //                                         let currentAreaStatus = "";
        //         //                                         if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
        //         //                                             currentAreaStatus = "Active";
        //         //                                         } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
        //         //                                             currentAreaStatus = "InActive";
        //         //                                         }
        //         //                                         let matchedObj = visitedProviderDetails.filter(function (obj) {
        //         //                                             let providerId = JSON.stringify(providerResult[i]._id);
        //         //                                             let visitedProviderId = JSON.stringify(obj._id.providerId);
        //         //                                             return visitedProviderId == providerId;
        //         //                                         });
        //         //                                         if (matchedObj.length > 0) {
        //         //                                             let providerVisitData = "";
        //         //                                             if (param.fiscalYear == "yearly") {
        //         //                                                 for (let i = 1; i <= 12; i++) {
        //         //                                                     let matchedObject = matchedObj.filter(function (obj) {
        //         //                                                         return i == obj._id.month;
        //         //                                                     });
        //         //                                                     if (matchedObject.length > 0) {
        //         //                                                         //console.log(matchedObject)
        //         //                                                         providerVisitData = matchedObject[0].totalVisit.join();
        //         //                                                     } else {
        //         //                                                         providerVisitData = "-----" ;
        //         //                                                     }
        //         //                                                 }
        //         //                                             } else if (param.fiscalYear == "financialYearly") {
        //         //                                                 for (let i = 4; i <= 15; i++) {
        //         //                                                     let month = i;
        //         //                                                     if (i == 13) {
        //         //                                                         month = 1;
        //         //                                                     } else if (i == 14) {
        //         //                                                         month = 2;
        //         //                                                     } else if (i == 15) {
        //         //                                                         month = 3;
        //         //                                                     }
        //         //                                                     let matchedObject = matchedObj.filter(function (obj) {
        //         //                                                         //console.log(obj._id)
        //         //                                                         //console.log(i + " ---- " + obj._id.month)
        //         //                                                         return month == obj._id.month;
        //         //                                                     });
        //         //                                                     if (matchedObject.length > 0) {
        //         //                                                         //console.log(matchedObject)
        //         //                                                         providerVisitData= matchedObject[0].totalVisit.join();
        //         //                                                     } else {
        //         //                                                         providerVisitData ="-----" ;
        //         //                                                     }
        //         //                                                 }
        //         //                                             }
        //         //                                             finalObject.push({
        //         //                                                 stateName: providerResult[i].stateDetails[0].stateName,
        //         //                                                 districtName: providerResult[i].districtDetails[0].districtName,
        //         //                                                 areaName: providerResult[i].areaDetails[0].areaName,
        //         //                                                 providerName: providerResult[i].providerName,
        //         //                                                 providerCode: providerResult[i].providerCode,
        // 		// 												providerType:providerResult[i].providerType,
        //         //                                                 category: providerResult[i].category,
        //         //                                                 specialization: providerResult[i].specialization,
        //         //                                                 degree: providerResult[i].degree,
        //         //                                                 status: currentProviderStatus,
        //         //                                                 providerStatus: currentProviderStatus,
        //         //                                                 areaStatus: currentAreaStatus,
        //         //                                                 data: providerVisitData
        //         //                                             });
        //         //                                             //console.log(matchedObj)
        //         //                                         } else {
        //         //                                             finalObject.push({
        //         //                                                 stateName: providerResult[i].stateDetails[0].stateName,
        //         //                                                 districtName: providerResult[i].districtDetails[0].districtName,
        //         //                                                 areaName: providerResult[i].areaDetails[0].areaName,
        //         //                                                 providerName: providerResult[i].providerName,
        //         //                                                 providerCode: providerResult[i].providerCode,
        //         //                                                 category: providerResult[i].category,
        // 		// 												providerType:providerResult[i].providerType,
        //         //                                                 specialization: providerResult[i].specialization,
        //         //                                                 degree: providerResult[i].degree,
        //         //                                                 status: currentProviderStatus,
        //         //                                                 providerStatus: currentProviderStatus,
        //         //                                                 areaStatus: currentAreaStatus,
        //         //                                                 data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
        //         //                                             });
        //         //                                         }
        //         //                                     }
        //         //                                     cb(false, finalObject);
        //         //                                     //  console.log(finalObject)
        //         //                                 });
        //         //                         })
        //         //                 });
        //         //         });
        //         // }
        //     }
        //-----------------------PRaveen Kumar(17-12-2018)-------------------------------------
        Providers.providerVisitAnalysis = function (param, cb) {
            var self = this;
            //  console.log("param-",param);
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var DCRMasterCollection = this.getDataSource().connector.collection(Providers.app.models.DCRMaster.modelName);
            var userAreaMappingCollection = this.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            let fromDate = "";
            let toDate = "";
            if (param.fiscalYear == "yearly") {
                fromDate = param.year + "-01-01";
                toDate = param.year + "-12-31";
            } else if (param.fiscalYear == "financialYearly") {
                fromDate = param.year[0] + "-04-01";
                toDate = param.year[1] + "-03-31";
            }
            if (param.designationLevel == 1) {
                userAreaMappingCollection.aggregate({
                    $match: {
                        companyId: ObjectId(param.companyId),
                        userId: ObjectId(param.userId)
                    }
                }, {
                    $group: {
                        _id: {},
                        areaIds: {
                            $addToSet: "$areaId"
                        }
                    }
                }, function (err, areaMappingResult) {
                    if (err) {
                        console.log(err)
                    }
                    let providerMatch = {
                        blockId: {
                            $in: areaMappingResult[0].areaIds
                        },
                        providerType: {
                            $in: param.type
                        },
                        appStatus: "approved",
                        status: {
                            $in: [true, false]
                        },
                        finalAppDate: {
                            $lte: new Date(toDate)
                        },
                        $or: [{
                            finalDelDate: {
                                $gte: new Date(fromDate),
                                $lte: new Date(toDate),
                            }
                        }, {
                            finalDelDate: {
                                $eq: new Date("1900-01-01T00:00:00.000Z")
                            }
                        }]
                    }
                    if (param.category.length > 0) {
                        providerMatch.category = {
                            $in: param.category
                        }
                    }
                    ProviderCollection.aggregate({
                            $match: providerMatch
                        }, {
                            $lookup: {
                                "from": "State",
                                "localField": "stateId",
                                "foreignField": "_id",
                                "as": "stateDetails"
                            }
                        }, {
                            $lookup: {
                                "from": "District",
                                "localField": "districtId",
                                "foreignField": "_id",
                                "as": "districtDetails"
                            }
                        }, {
                            $lookup: {
                                "from": "Area",
                                "localField": "blockId",
                                "foreignField": "_id",
                                "as": "areaDetails"
                            }
                        }, {
                            $lookup: {
                                "from": "UserAreaMapping",
                                "localField": "blockId",
                                "foreignField": "areaId",
                                "as": "userAreaDetails"
                            }
                        }, {
                            $unwind: "$userAreaDetails"
                        }, {
                            $match: {
                                "userAreaDetails.appStatus": {
                                    $ne: "pending"
                                }
                            }
                        }, {
                            $group: {
                                _id: {},
                                unlistedDoctor: {
                                    $addToSet: {
                                        $cond: [{
                                            $and: [{
                                                    $eq: ["$appStatus", "unlisted"]
                                                },
                                                {
                                                    $eq: ["$submitBy", ObjectId(param.userId)]
                                                }
                                            ]
                                        }, "$$ROOT", "$abc"]
                                    }
                                },
                                allDoctor: {
                                    $addToSet: {
                                        $cond: [{
                                            $and: [{
                                                $ne: ["$appStatus", "unlisted"]
                                            }]
                                        }, "$$ROOT", "$abc"]
                                    }
                                }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                totalDoctors: {
                                    $concatArrays: ["$unlistedDoctor", "$allDoctor"]
                                }
                            }
                        }, {
                            $unwind: "$totalDoctors"
                        },
                        function (err, providerResult) {
                            if (err) {
                                console.log(err);
                            }
                            for (let i = 0; i < providerResult.length; i++) {
                                providerResult[i] = providerResult[i].totalDoctors;
                            }
                            DCRMasterCollection.aggregate({
                                $match: {
                                    "companyId": ObjectId(param.companyId),
                                    "submitBy": ObjectId(param.userId),
                                    "dcrDate": {
                                        $gte: new Date(fromDate),
                                        $lte: new Date(toDate)
                                    }
                                }
                            }, {
                                $lookup: {
                                    "from": "DCRProviderVisitDetails",
                                    "localField": "_id",
                                    "foreignField": "dcrId",
                                    "as": "dcrVisitDetails"
                                }
                            }, {
                                $unwind: "$dcrVisitDetails"
                            }, {
                                $match: {
                                    "dcrVisitDetails.providerType": {
                                        $in: param.type
                                    }
                                }
                            }, {
                                $project: {
                                    "providerId": "$dcrVisitDetails.providerId",
                                    "month": {
                                        $month: "$dcrVisitDetails.dcrDate"
                                    },
                                    "year": {
                                        $year: "$dcrVisitDetails.dcrDate"
                                    },
                                    "day": {
                                        day: "$dcrVisitDetails.dcrDate"
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {
                                        providerId: "$providerId",
                                        month: "$month",
                                        year: "$year"
                                    },
                                    dates: {
                                        $addToSet: "$day"
                                    }

                                }
                            }, function (err, visitedProviderDetails) {

                                if (err) {
                                    console.log(err);
                                }
                                let finalObject = [];
                                for (let i = 0; i < providerResult.length; i++) {
                                    let currentProviderStatus = "";
                                    if (providerResult[i].appStatus == "unlisted") {
                                        currentProviderStatus = "Unlisted";
                                    } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
                                        currentProviderStatus = "Active";
                                    } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
                                        currentProviderStatus = "InActive";
                                    }
                                    let currentAreaStatus = "";
                                    if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
                                        currentAreaStatus = "Active";
                                    } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
                                        currentAreaStatus = "InActive";
                                    }
                                    let matchedObj = visitedProviderDetails.filter(function (obj) {
                                        let providerId = JSON.stringify(providerResult[i]._id);
                                        let visitedProviderId = JSON.stringify(obj._id.providerId);
                                        return visitedProviderId == providerId;
                                    });
                                    if (matchedObj.length > 0) {
                                        let providerVisitData = [];
                                        let VisitedDate = "";
                                        if (param.fiscalYear == "yearly") {
                                            for (let i = 1; i <= 12; i++) {
                                                let matchedObject = matchedObj.filter(function (obj) {
                                                    return i == obj._id.month;
                                                });
                                                if (matchedObject.length > 0) {
                                                    for (let i = 0; i < matchedObject[0].dates.length; i++) {
                                                        if (i == 0) {
                                                            VisitedDate = moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                        } else {
                                                            VisitedDate = VisitedDate + "," + moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                        }
                                                    }
                                                    providerVisitData.push(VisitedDate)
                                                } else {
                                                    VisitedDate = "-----";
                                                    providerVisitData.push(VisitedDate)
                                                }
                                            }
                                        } else if (param.fiscalYear == "financialYearly") {
                                            for (let i = 4; i <= 15; i++) {
                                                let month = i;
                                                if (i == 13) {
                                                    month = 1;
                                                } else if (i == 14) {
                                                    month = 2;
                                                } else if (i == 15) {
                                                    month = 3;
                                                }
                                                let matchedObject = matchedObj.filter(function (obj) {
                                                    //console.log(obj._id)
                                                    //console.log(i + " ---- " + obj._id.month)
                                                    return month == obj._id.month;
                                                });
                                                if (matchedObject.length > 0) {
                                                    for (let i = 0; i < matchedObject[0].dates.length; i++) {
                                                        if (i == 0) {
                                                            VisitedDate = moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                        } else {
                                                            VisitedDate = VisitedDate + "," + moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                        }
                                                    }
                                                    providerVisitData.push(VisitedDate)
                                                } else {
                                                    VisitedDate = "-----";
                                                    providerVisitData.push(VisitedDate)
                                                }
                                            }
                                        }
                                        finalObject.push({
                                            stateName: providerResult[i].stateDetails[0].stateName,
                                            districtName: providerResult[i].districtDetails[0].districtName,
                                            areaName: providerResult[i].areaDetails[0].areaName,
                                            providerName: providerResult[i].providerName,
                                            providerCode: providerResult[i].providerCode,
                                            category: providerResult[i].category,
                                            specialization: providerResult[i].specialization,
                                            degree: providerResult[i].degree,
                                            providerStatus: currentProviderStatus,
                                            areaStatus: currentAreaStatus,
                                            providerType: providerResult[i].providerType,
                                            data: providerVisitData
                                        });
                                        //console.log(matchedObj)
                                    } else {
                                        finalObject.push({
                                            stateName: providerResult[i].stateDetails[0].stateName,
                                            districtName: providerResult[i].districtDetails[0].districtName,
                                            areaName: providerResult[i].areaDetails[0].areaName,
                                            providerName: providerResult[i].providerName,
                                            providerCode: providerResult[i].providerCode,
                                            category: providerResult[i].category,
                                            specialization: providerResult[i].specialization,
                                            degree: providerResult[i].degree,
                                            providerStatus: currentProviderStatus,
                                            areaStatus: currentAreaStatus,
                                            providerType: providerResult[i].providerType,
                                            data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
                                        });
                                    }
                                }
                                cb(false, finalObject);
                                //  console.log(finalObject)
                            });
                        })
                });
            } else if (param.designationLevel >= 2) {
                let passingUserIds = [ObjectId(param.userId)];
                if (param.hasOwnProperty("team") && param.team != "") {
                    param.team.forEach(teamId => {
                        passingUserIds.push(ObjectId(teamId))
                    })
                

                //---------edited by preeti arora-------------
                userAreaMappingCollection.aggregate({
                    $match: {
                        companyId: ObjectId(param.companyId),
                        userId: {
                            $in: passingUserIds
                        },
                        appStatus: {
                            $ne: "pending"
                        },
                        status: {
                            $in: [true, false]
                        }
                    }
                }, {
                    $group: {
                        _id: {},
                        areaIds: {
                            $addToSet: "$areaId"
                        }
                    }
                }, function (err, areaMappingResult) {
                    if (err) {
                        console.log(err)
                    }
                    if (areaMappingResult.length > 0) {
                        let providerMatch = {
                            blockId: {
                                $in: areaMappingResult[0].areaIds
                            },
                            providerType: {
                                $in: param.type
                            },
                            appStatus: "approved",
                            status: {
                                $in: [true, false]
                            },
                            finalAppDate: {
                                $lte: new Date(toDate)
                            },
                            $or: [{
                                finalDelDate: {
                                    $gte: new Date(fromDate),
                                    $lte: new Date(toDate),
                                }
                            }, {
                                finalDelDate: {
                                    $eq: new Date("1900-01-01T00:00:00.000Z")
                                }
                            }]
                        }
                        if (param.category.length > 0) {
                            providerMatch.category = {
                                $in: param.category
                            }
                        }

                        ProviderCollection.aggregate({
                                $match: providerMatch
                            }, {
                                $lookup: {
                                    "from": "State",
                                    "localField": "stateId",
                                    "foreignField": "_id",
                                    "as": "stateDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "District",
                                    "localField": "districtId",
                                    "foreignField": "_id",
                                    "as": "districtDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "Area",
                                    "localField": "blockId",
                                    "foreignField": "_id",
                                    "as": "areaDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "UserAreaMapping",
                                    "localField": "blockId",
                                    "foreignField": "areaId",
                                    "as": "userAreaDetails"
                                }
                            }, {
                                $unwind: "$userAreaDetails"
                            }, {
                                $match: {
                                    "userAreaDetails.appStatus": {
                                        $ne: "pending"
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {},
                                    unlistedDoctor: {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$appStatus", "unlisted"]
                                                    },
                                                    {
                                                        $eq: ["$submitBy", ObjectId(param.userId)]
                                                    }
                                                ]
                                            }, "$$ROOT", "$abc"]
                                        }
                                    },
                                    allDoctor: {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $ne: ["$appStatus", "unlisted"]
                                                }]
                                            }, "$$ROOT", "$abc"]
                                        }
                                    }
                                }
                            }, {
                                $project: {
                                    _id: 0,
                                    totalDoctors: {
                                        $concatArrays: ["$unlistedDoctor", "$allDoctor"]
                                    }
                                }
                            }, {
                                $unwind: "$totalDoctors"
                            },
                            function (err, providerResult) {
                                if (err) {
                                    console.log(err);
                                }
                                // console.log("Provider Length : " + providerResult.length)
                                if (providerResult.length > 0) {
                                    for (let i = 0; i < providerResult.length; i++) {
                                        providerResult[i] = providerResult[i].totalDoctors;
                                    }
                                    DCRMasterCollection.aggregate({
                                        $match: {
                                            "companyId": ObjectId(param.companyId),
                                            "submitBy": ObjectId(param.userId),
                                            "dcrDate": {
                                                $gte: new Date(fromDate),
                                                $lte: new Date(toDate)
                                            }
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "DCRProviderVisitDetails",
                                            "localField": "_id",
                                            "foreignField": "dcrId",
                                            "as": "dcrVisitDetails"
                                        }
                                    }, {
                                        $unwind: "$dcrVisitDetails"
                                    }, {
                                        $match: {
                                            "dcrVisitDetails.providerType": param.type
                                        }
                                    }, {
                                        $project: {
                                            "providerId": "$dcrVisitDetails.providerId",
                                            "month": {
                                                $month: "$dcrVisitDetails.dcrDate"
                                            },
                                            "year": {
                                                $year: "$dcrVisitDetails.dcrDate"
                                            }
                                        }
                                    }, {
                                        $group: {
                                            _id: {
                                                providerId: "$providerId",
                                                month: "$month",
                                                year: "$year"
                                            },
                                            totalVisit: {
                                                $sum: 1
                                            }
                                        }
                                    }, function (err, visitedProviderDetails) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        let finalObject = [];
                                        for (let i = 0; i < providerResult.length; i++) {
                                            let currentProviderStatus = "";
                                            if (providerResult[i].appStatus == "unlisted") {
                                                currentProviderStatus = "Unlisted";
                                            } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
                                                currentProviderStatus = "Active";
                                            } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
                                                currentProviderStatus = "InActive";
                                            }
                                            let currentAreaStatus = "";
                                            if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
                                                currentAreaStatus = "Active";
                                            } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
                                                currentAreaStatus = "InActive";
                                            }
                                            let matchedObj = visitedProviderDetails.filter(function (obj) {
                                                let providerId = JSON.stringify(providerResult[i]._id);
                                                let visitedProviderId = JSON.stringify(obj._id.providerId);
                                                return visitedProviderId == providerId;
                                            });
                                            if (matchedObj.length > 0) {
                                                let providerVisitData = [];
                                                if (param.fiscalYear == "yearly") {
                                                    for (let i = 1; i <= 12; i++) {
                                                        let matchedObject = matchedObj.filter(function (obj) {
                                                            return i == obj._id.month;
                                                        });
                                                        if (matchedObject.length > 0) {
                                                            //console.log(matchedObject)
                                                            providerVisitData.push(matchedObject[0].totalVisit)
                                                        } else {
                                                            providerVisitData.push("-----");
                                                        }
                                                    }
                                                } else if (param.fiscalYear == "financialYearly") {
                                                    for (let i = 4; i <= 15; i++) {
                                                        let month = i;
                                                        if (i == 13) {
                                                            month = 1;
                                                        } else if (i == 14) {
                                                            month = 2;
                                                        } else if (i == 15) {
                                                            month = 3;
                                                        }
                                                        let matchedObject = matchedObj.filter(function (obj) {
                                                            //console.log(obj._id)
                                                            //console.log(i + " ---- " + obj._id.month)
                                                            return month == obj._id.month;
                                                        });
                                                        if (matchedObject.length > 0) {
                                                            //console.log(matchedObject)
                                                            providerVisitData.push(matchedObject[0].totalVisit)
                                                        } else {
                                                            providerVisitData.push("-----");
                                                        }
                                                    }
                                                }
                                                finalObject.push({
                                                    stateName: providerResult[i].stateDetails[0].stateName,
                                                    districtName: providerResult[i].districtDetails[0].districtName,
                                                    areaName: providerResult[i].areaDetails[0].areaName,
                                                    providerName: providerResult[i].providerName,
                                                    providerCode: providerResult[i].providerCode,
                                                    providerType: providerResult[i].providerType,
                                                    category: providerResult[i].category,
                                                    specialization: providerResult[i].specialization,
                                                    degree: providerResult[i].degree,
                                                    status: currentProviderStatus,
                                                    providerStatus: currentProviderStatus,
                                                    areaStatus: currentAreaStatus,
                                                    data: providerVisitData
                                                });
                                                //console.log(matchedObj)
                                            } else {
                                                finalObject.push({
                                                    stateName: providerResult[i].stateDetails[0].stateName,
                                                    districtName: providerResult[i].districtDetails[0].districtName,
                                                    areaName: providerResult[i].areaDetails[0].areaName,
                                                    providerName: providerResult[i].providerName,
                                                    providerCode: providerResult[i].providerCode,
                                                    category: providerResult[i].category,
                                                    providerType: providerResult[i].providerType,
                                                    specialization: providerResult[i].specialization,
                                                    degree: providerResult[i].degree,
                                                    status: currentProviderStatus,
                                                    providerStatus: currentProviderStatus,
                                                    areaStatus: currentAreaStatus,
                                                    data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
                                                });
                                            }
                                        }
                                        cb(false, finalObject);
                                        //  console.log(finalObject)
                                    });
                                } else {
                                    cb(false, [])
                                }

                            })
                    } else {
                        cb(false, [])
                    }
                });
                //--------------end---------------
			} 
			
			else{
				
				
                let passingUserIds = [];
				
				   let passinObjectForHierarchy = {
                    supervisorId: ObjectId(param.userId),
                    companyId: param.companyId,
                    type: 'lower'
                };
              
				
				Providers.app.models.Hierarchy.getManagerHierarchy(
passinObjectForHierarchy,
                    function (err, hierarchyResult) {
                        if (err) {
                            console.log(err);
                        }
						if(hierarchyResult.length>0){
for (let ii = 0; ii < hierarchyResult.length; ii++) {
passingUserIds.push(ObjectId(hierarchyResult[ii].userId));
}

}
else{
passingUserIds.push(ObjectId(param.userId));

}

console.log("passingUserIds+++++++++++++++++",passingUserIds);
						
                //---------edited by preeti arora-------------
                userAreaMappingCollection.aggregate({
                    $match: {
                        companyId: ObjectId(param.companyId),
                        userId: {
                            $in: passingUserIds
                        },
                        appStatus: {
                            $ne: "pending"
                        },
                        status: {
                            $in: [true, false]
                        }
                    }
                }, {
                    $group: {
                        _id: {},
                        areaIds: {
                            $addToSet: "$areaId"
                        }
                    }
                }, function (err, areaMappingResult) {
                    if (err) {
                        console.log(err)
                    }
                    if (areaMappingResult.length > 0) {
                        let providerMatch = {
                            blockId: {
                                $in: areaMappingResult[0].areaIds
                            },
                            providerType: {
                                $in: param.type
                            },
                            appStatus: "approved",
                            status: {
                                $in: [true, false]
                            },
                            finalAppDate: {
                                $lte: new Date(toDate)
                            },
                            $or: [{
                                finalDelDate: {
                                    $gte: new Date(fromDate),
                                    $lte: new Date(toDate),
                                }
                            }, {
                                finalDelDate: {
                                    $eq: new Date("1900-01-01T00:00:00.000Z")
                                }
                            }]
                        }
                        if (param.category.length > 0) {
                            providerMatch.category = {
                                $in: param.category
                            }
                        }

                        ProviderCollection.aggregate({
                                $match: providerMatch
                            }, {
                                $lookup: {
                                    "from": "State",
                                    "localField": "stateId",
                                    "foreignField": "_id",
                                    "as": "stateDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "District",
                                    "localField": "districtId",
                                    "foreignField": "_id",
                                    "as": "districtDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "Area",
                                    "localField": "blockId",
                                    "foreignField": "_id",
                                    "as": "areaDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "UserAreaMapping",
                                    "localField": "blockId",
                                    "foreignField": "areaId",
                                    "as": "userAreaDetails"
                                }
                            }, {
                                $unwind: "$userAreaDetails"
                            }, {
                                $match: {
                                    "userAreaDetails.appStatus": {
                                        $ne: "pending"
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {},
                                    unlistedDoctor: {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$appStatus", "unlisted"]
                                                    },
                                                    {
                                                        $eq: ["$submitBy", ObjectId(param.userId)]
                                                    }
                                                ]
                                            }, "$$ROOT", "$abc"]
                                        }
                                    },
                                    allDoctor: {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $ne: ["$appStatus", "unlisted"]
                                                }]
                                            }, "$$ROOT", "$abc"]
                                        }
                                    }
                                }
                            }, {
                                $project: {
                                    _id: 0,
                                    totalDoctors: {
                                        $concatArrays: ["$unlistedDoctor", "$allDoctor"]
                                    }
                                }
                            }, {
                                $unwind: "$totalDoctors"
                            },
                            function (err, providerResult) {
                                if (err) {
                                    console.log(err);
                                }
                                // console.log("Provider Length : " + providerResult.length)
                                if (providerResult.length > 0) {
                                    for (let i = 0; i < providerResult.length; i++) {
                                        providerResult[i] = providerResult[i].totalDoctors;
                                    }
                                    DCRMasterCollection.aggregate({
                                        $match: {
                                            "companyId": ObjectId(param.companyId),
                                            "submitBy": ObjectId(param.userId),
                                            "dcrDate": {
                                                $gte: new Date(fromDate),
                                                $lte: new Date(toDate)
                                            }
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "DCRProviderVisitDetails",
                                            "localField": "_id",
                                            "foreignField": "dcrId",
                                            "as": "dcrVisitDetails"
                                        }
                                    }, {
                                        $unwind: "$dcrVisitDetails"
                                    }, {
                                        $match: {
                                            "dcrVisitDetails.providerType": param.type
                                        }
                                    }, {
                                        $project: {
                                            "providerId": "$dcrVisitDetails.providerId",
                                            "month": {
                                                $month: "$dcrVisitDetails.dcrDate"
                                            },
                                            "year": {
                                                $year: "$dcrVisitDetails.dcrDate"
                                            }
                                        }
                                    }, {
                                        $group: {
                                            _id: {
                                                providerId: "$providerId",
                                                month: "$month",
                                                year: "$year"
                                            },
                                            totalVisit: {
                                                $sum: 1
                                            }
                                        }
                                    }, function (err, visitedProviderDetails) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        let finalObject = [];
                                        for (let i = 0; i < providerResult.length; i++) {
                                            let currentProviderStatus = "";
                                            if (providerResult[i].appStatus == "unlisted") {
                                                currentProviderStatus = "Unlisted";
                                            } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
                                                currentProviderStatus = "Active";
                                            } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
                                                currentProviderStatus = "InActive";
                                            }
                                            let currentAreaStatus = "";
                                            if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
                                                currentAreaStatus = "Active";
                                            } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
                                                currentAreaStatus = "InActive";
                                            }
                                            let matchedObj = visitedProviderDetails.filter(function (obj) {
                                                let providerId = JSON.stringify(providerResult[i]._id);
                                                let visitedProviderId = JSON.stringify(obj._id.providerId);
                                                return visitedProviderId == providerId;
                                            });
                                            if (matchedObj.length > 0) {
                                                let providerVisitData = [];
                                                if (param.fiscalYear == "yearly") {
                                                    for (let i = 1; i <= 12; i++) {
                                                        let matchedObject = matchedObj.filter(function (obj) {
                                                            return i == obj._id.month;
                                                        });
                                                        if (matchedObject.length > 0) {
                                                            //console.log(matchedObject)
                                                            providerVisitData.push(matchedObject[0].totalVisit)
                                                        } else {
                                                            providerVisitData.push("-----");
                                                        }
                                                    }
                                                } else if (param.fiscalYear == "financialYearly") {
                                                    for (let i = 4; i <= 15; i++) {
                                                        let month = i;
                                                        if (i == 13) {
                                                            month = 1;
                                                        } else if (i == 14) {
                                                            month = 2;
                                                        } else if (i == 15) {
                                                            month = 3;
                                                        }
                                                        let matchedObject = matchedObj.filter(function (obj) {
                                                            //console.log(obj._id)
                                                            //console.log(i + " ---- " + obj._id.month)
                                                            return month == obj._id.month;
                                                        });
                                                        if (matchedObject.length > 0) {
                                                            //console.log(matchedObject)
                                                            providerVisitData.push(matchedObject[0].totalVisit)
                                                        } else {
                                                            providerVisitData.push("-----");
                                                        }
                                                    }
                                                }
                                                finalObject.push({
                                                    stateName: providerResult[i].stateDetails[0].stateName,
                                                    districtName: providerResult[i].districtDetails[0].districtName,
                                                    areaName: providerResult[i].areaDetails[0].areaName,
                                                    providerName: providerResult[i].providerName,
                                                    providerCode: providerResult[i].providerCode,
                                                    providerType: providerResult[i].providerType,
                                                    category: providerResult[i].category,
                                                    specialization: providerResult[i].specialization,
                                                    degree: providerResult[i].degree,
                                                    status: currentProviderStatus,
                                                    providerStatus: currentProviderStatus,
                                                    areaStatus: currentAreaStatus,
                                                    data: providerVisitData
                                                });
                                                //console.log(matchedObj)
                                            } else {
                                                finalObject.push({
                                                    stateName: providerResult[i].stateDetails[0].stateName,
                                                    districtName: providerResult[i].districtDetails[0].districtName,
                                                    areaName: providerResult[i].areaDetails[0].areaName,
                                                    providerName: providerResult[i].providerName,
                                                    providerCode: providerResult[i].providerCode,
                                                    category: providerResult[i].category,
                                                    providerType: providerResult[i].providerType,
                                                    specialization: providerResult[i].specialization,
                                                    degree: providerResult[i].degree,
                                                    status: currentProviderStatus,
                                                    providerStatus: currentProviderStatus,
                                                    areaStatus: currentAreaStatus,
                                                    data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
                                                });
                                            }
                                        }
                                        cb(false, finalObject);
                                        //  console.log(finalObject)
                                    });
                                } else {
                                    cb(false, [])
                                }

                            })
                    } else {
                        cb(false, [])
                    }
                });
				
			});
                //--------------end---------------
            
				
			}
			
			}
            //   else if (param.designationLevel >= 2) {


            //  let passinObjectForHierarchy = {
            //      supervisorId: param.userId,
            //      companyId: param.companyId,
            //      type: 'lower'
            //  };
            //  Providers.app.models.Hierarchy.getManagerHierarchy(
            //      passinObjectForHierarchy,
            //      function (err, hierarchyResult) {
            //          if (err) {
            //              console.log(err);
            //          }
            //          let passingUserIds = [];
            //          for (let i = 0; i < hierarchyResult.length; i++) {
            //              passingUserIds.push(hierarchyResult[i].userId);
            //          }
            //          //console.log("Provider Visit Analysis Hierarchy len : " + hierarchyResult.length)
            //          //let passingUserIds = [ObjectId("5b33ca57f87a57439085de2a"), ObjectId("5bcd0b949ef1c86b7d025cc5"), ObjectId("5bce1cc8d0a43a0b1e4e23c4")]
            //          userAreaMappingCollection.aggregate({
            //              $match: {
            //                  companyId: ObjectId(param.companyId),
            //                  userId: { $in: passingUserIds },
            //                  appStatus: {
            //                      $ne: "pending"
            //                  },
            //                  status: {
            //                      $in: [true, false]
            //                  }
            //              }
            //          }, {
            //                  $group: {
            //                      _id: {},
            //                      areaIds: {
            //                          $addToSet: "$areaId"
            //                      }
            //                  }
            //              }, function (err, areaMappingResult) {
            //                  if (err) {
            //                      console.log(err)
            //                  }

            //                 let providerMatch={
            //                          blockId: {
            //                              $in: areaMappingResult[0].areaIds
            //                          },
            //                          providerType: {$in : param.type},
            //                          appStatus: "approved",
            //                          status: {
            //                              $in: [true, false]
            //                          },
            //                          finalAppDate: {
            //                              $lte: new Date(toDate)
            //                          },
            //                          $or: [{
            //                              finalDelDate: {
            //                                  $gte: new Date(fromDate),
            //                                  $lte: new Date(toDate),
            //                              }
            //                          }, {
            //                              finalDelDate: { $eq: new Date("1900-01-01T00:00:00.000Z") }
            //                          }]
            //                      }
            //                      if(param.category.length>0){
            //                          providerMatch.category={
            //                         $in :param.category
            //                  }		
            //                           }
            //                 ProviderCollection.aggregate({
            //                      $match: providerMatch
            //                  }, {
            //                          $lookup: {
            //                              "from": "State",
            //                              "localField": "stateId",
            //                              "foreignField": "_id",
            //                              "as": "stateDetails"
            //                          }
            //                      }, {
            //                          $lookup: {
            //                              "from": "District",
            //                              "localField": "districtId",
            //                              "foreignField": "_id",
            //                              "as": "districtDetails"
            //                          }
            //                      }, {
            //                          $lookup: {
            //                              "from": "Area",
            //                              "localField": "blockId",
            //                              "foreignField": "_id",
            //                              "as": "areaDetails"
            //                          }
            //                      }, {
            //                          $lookup: {
            //                              "from": "UserAreaMapping",
            //                              "localField": "blockId",
            //                              "foreignField": "areaId",
            //                              "as": "userAreaDetails"
            //                          }
            //                      }, {
            //                          $unwind: "$userAreaDetails"
            //                      }, {
            //                          $match: {
            //                              "userAreaDetails.appStatus": {
            //                                  $ne: "pending"
            //                              }
            //                          }
            //                      }, {
            //                          $group: {
            //                              _id: {},
            //                              unlistedDoctor: {
            //                                  $addToSet: {
            //                                      $cond: [{
            //                                          $and: [{
            //                                              $eq: ["$appStatus", "unlisted"]
            //                                          },
            //                                          {
            //                                              $eq: ["$submitBy", ObjectId(param.userId)]
            //                                          }
            //                                          ]
            //                                      }, "$$ROOT", "$abc"]
            //                                  }
            //                              },
            //                              allDoctor: {
            //                                  $addToSet: {
            //                                      $cond: [{
            //                                          $and: [{
            //                                              $ne: ["$appStatus", "unlisted"]
            //                                          }]
            //                                      }, "$$ROOT", "$abc"]
            //                                  }
            //                              }
            //                          }
            //                      }, {
            //                          $project: {
            //                              _id: 0,
            //                              totalDoctors: { $concatArrays: ["$unlistedDoctor", "$allDoctor"] }
            //                          }
            //                      }, {
            //                          $unwind: "$totalDoctors"
            //                      },
            //                      function (err, providerResult) {
            //                          if (err) {
            //                              console.log(err);
            //                          }
            //                          for (let i = 0; i < providerResult.length; i++) {
            //                              providerResult[i] = providerResult[i].totalDoctors;
            //                          }
            //                          DCRMasterCollection.aggregate({
            //                              $match: {
            //                                  "companyId": ObjectId(param.companyId),
            //                                  "submitBy": ObjectId(param.userId),
            //                                  "dcrDate": {
            //                                      $gte: new Date(fromDate),
            //                                      $lte: new Date(toDate)
            //                                  }
            //                              }
            //                          }, {
            //                                  $lookup: {
            //                                      "from": "DCRProviderVisitDetails",
            //                                      "localField": "_id",
            //                                      "foreignField": "dcrId",
            //                                      "as": "dcrVisitDetails"
            //                                  }
            //                              }, {
            //                                  $unwind: "$dcrVisitDetails"
            //                              }, {
            //                                  $match: {
            //                                      "dcrVisitDetails.providerType":{$in : param.type}
            //                                  }
            //                              }, {
            //                                  $project: {
            //                                      "providerId": "$dcrVisitDetails.providerId",
            //                                      "month": {
            //                                          $month: "$dcrVisitDetails.dcrDate"
            //                                      },
            //                                      "year": {
            //                                          $year: "$dcrVisitDetails.dcrDate"
            //                                      },
            //                                      "day": {
            //                                          day: "$dcrVisitDetails.dcrDate"
            //                                      }
            //                                  }
            //                              }, {
            //                                  $group: {
            //                                      _id: {
            //                                          providerId: "$providerId",
            //                                          month: "$month",
            //                                          year: "$year"
            //                                      },
            //                                      dates: {
            //                                          $addToSet : "$day"
            //                                      }
            //                                  }
            //                              }, function (err, visitedProviderDetails) {
            //                                  if (err) {
            //                                      console.log(err);
            //                                  }
            //                                  let finalObject = [];
            //                                  for (let i = 0; i < providerResult.length; i++) {
            //                                      let currentProviderStatus = "";
            //                                      if (providerResult[i].appStatus == "unlisted") {
            //                                          currentProviderStatus = "Unlisted";
            //                                      } else if (providerResult[i].status == true && providerResult[i].appStatus == "approved") {
            //                                          currentProviderStatus = "Active";
            //                                      } else if (providerResult[i].status == false && providerResult[i].delStatus == "approved") {
            //                                          currentProviderStatus = "InActive";
            //                                      }
            //                                      let currentAreaStatus = "";
            //                                      if (providerResult[i].userAreaDetails.status == true && providerResult[i].userAreaDetails.appStatus == "approved") {
            //                                          currentAreaStatus = "Active";
            //                                      } else if (providerResult[i].userAreaDetails.status == false && providerResult[i].userAreaDetails.delStatus == "approved") {
            //                                          currentAreaStatus = "InActive";
            //                                      }
            //                                      let matchedObj = visitedProviderDetails.filter(function (obj) {
            //                                          let providerId = JSON.stringify(providerResult[i]._id);
            //                                          let visitedProviderId = JSON.stringify(obj._id.providerId);
            //                                          return visitedProviderId == providerId;
            //                                      });
            //                                      if (matchedObj.length > 0) {
            //                                          let providerVisitData = [];
            //                                          let VisitedDate = "";
            //                                          if (param.fiscalYear == "yearly") {
            //                                              for (let i = 1; i <= 12; i++) {
            //                                                  let matchedObject = matchedObj.filter(function (obj) {
            //                                                      return i == obj._id.month;
            //                                                  });
            //                                                  if (matchedObject.length > 0) {
            //                                                      for(let i=0;i<matchedObject[0].dates.length;i++){
            //                                                          if(i==0){
            //                                                              VisitedDate=moment(matchedObject[0].dates[i].day).format("DD-MMM").toString();
            //                                                              }else{
            //                                                              VisitedDate=VisitedDate+","+moment(matchedObject[0].dates[i].day).format("DD").toString();
            //                                                          }
            //                                                      }
            //                                                      providerVisitData.push(VisitedDate)
            //                                                  } else {
            //                                                      VisitedDate="-----";
            //                                                      providerVisitData.push(VisitedDate)
            //                                                  }
            //                                              }
            //                                          } else if (param.fiscalYear == "financialYearly") {
            //                                              for (let i = 4; i <= 15; i++) {
            //                                                  let month = i;
            //                                                  if (i == 13) {
            //                                                      month = 1;
            //                                                  } else if (i == 14) {
            //                                                      month = 2;
            //                                                  } else if (i == 15) {
            //                                                      month = 3;
            //                                                  }
            //                                                  let matchedObject = matchedObj.filter(function (obj) {
            //                                                      //console.log(obj._id)
            //                                                      //console.log(i + " ---- " + obj._id.month)
            //                                                      return month == obj._id.month;
            //                                                  });
            //                                                  if (matchedObject.length > 0) {
            //                                                      for(let i=0;i<matchedObject[0].dates.length;i++){
            //                                                          if(i==0){
            //                                                              VisitedDate=moment(matchedObject[0].dates[i].day).format("DD").toString();
            //                                                              }else{
            //                                                              VisitedDate=VisitedDate+","+moment(matchedObject[0].dates[i].day).format("DD").toString();
            //                                                          }
            //                                                      }
            //                                                      providerVisitData.push(VisitedDate)
            //                                                  } else {
            //                                                      VisitedDate="-----";
            //                                                      providerVisitData.push(VisitedDate)
            //                                                  }
            //                                              }
            //                                          }
            //                                          finalObject.push({
            //                                              stateName: providerResult[i].stateDetails[0].stateName,
            //                                              districtName: providerResult[i].districtDetails[0].districtName,
            //                                              areaName: providerResult[i].areaDetails[0].areaName,
            //                                              providerName: providerResult[i].providerName,
            //                                              providerCode: providerResult[i].providerCode,
            //                                              providerType:providerResult[i].providerType,
            //                                              category: providerResult[i].category,
            //                                              specialization: providerResult[i].specialization,
            //                                              degree: providerResult[i].degree,
            //                                              status: currentProviderStatus,
            //                                              providerStatus: currentProviderStatus,
            //                                              areaStatus: currentAreaStatus,
            //                                              data: providerVisitData
            //                                          });
            //                                          //console.log(matchedObj)
            //                                      } else {
            //                                          finalObject.push({
            //                                              stateName: providerResult[i].stateDetails[0].stateName,
            //                                              districtName: providerResult[i].districtDetails[0].districtName,
            //                                              areaName: providerResult[i].areaDetails[0].areaName,
            //                                              providerName: providerResult[i].providerName,
            //                                              providerCode: providerResult[i].providerCode,
            //                                              category: providerResult[i].category,
            //                                              providerType:providerResult[i].providerType,
            //                                              specialization: providerResult[i].specialization,
            //                                              degree: providerResult[i].degree,
            //                                              status: currentProviderStatus,
            //                                              providerStatus: currentProviderStatus,
            //                                              areaStatus: currentAreaStatus,
            //                                              data: ['-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----', '-----']
            //                                          });
            //                                      }
            //                                  }
            //                                  cb(false, finalObject);
            //                                  //  console.log(finalObject)
            //                              });
            //                      })
            //              });
            //      });
            // }
        }

        Providers.remoteMethod(
            'providerVisitAnalysis', {
                description: 'Getting ProviderToBeMapped List',
                accepts: [{
                    arg: 'param',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'post'
                }
            }
        )
        //---------------------------------END-------------------------------------------------
        //------------------------------------------replica-----------------
        Providers.approveAndDeleteProviderRequestReplica = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let providerIds = [];
            for (let i = 0; i < params.approveDetails.length; i++) {
                providerIds.push(ObjectId(params.approveDetails[i].providerId));
            }
            let aSet = {};
            if (params.requestType === 'approve') {
                if (params.rL > 1) {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date('1900-01-02T 00:00:00'),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ''
                        };
                    }
                } else {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            finalAppDate: new Date(),
                            finalAppBy: ObjectId(params.updatedBy)
                        };
                    }
                }
            } else if (params.requestType === 'delete') {
                if (params.rL > 1) {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date('1900-01-02T 00:00:00'),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ''
                        };
                    }
                } else {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            finalDelDate: new Date(),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    }
                }
            }
            ProviderCollection.update({
                companyId: ObjectId(params.companyId),
                _id: {
                    $in: providerIds
                }
            }, {
                $set: aSet
            }, {
                multi: true
            }, function (err, res) {
                if (err) {
                    return cb(err);
                }
                let results = [];
                results.push({
                    status: res.result.nModified
                });
                cb(false, results)
            });
        }
        Providers.remoteMethod(
            'approveAndDeleteProviderRequestReplica', {
                description: 'approveAndDeleteProviderRequestReplica',
                accepts: [{
                    arg: 'params',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'post'
                }
            }
        )
        //End
        //-----------------------
        //by ravi 30-12-2018
        Providers.approveAndDeleteProviderRequest = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let providerIds = [];
            for (let i = 0; i < params.approveDetails.length; i++) {
                providerIds.push(ObjectId(params.approveDetails[i].providerId));
            }
            let aSet = {};
            if (params.requestType === 'approve') {
                if (params.rL > 1) {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date('1900-01-02T 00:00:00'),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: '',
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                } else {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            finalAppDate: new Date(),
                            finalAppBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                }
            } else if (params.requestType === 'delete') {
                if (params.rL > 1) {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date('1900-01-02T 00:00:00'),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: '',
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                } else {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            finalDelDate: new Date(),
                            finalDelBy: ObjectId(params.updatedBy),
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                }
            }
            ProviderCollection.update({
                companyId: ObjectId(params.companyId),
                _id: {
                    $in: providerIds
                }
            }, {
                $set: aSet
            }, {
                multi: true
            }, function (err, res) {
                if (err) {
                    return cb(err);
                }
                let results = [];
                results.push({
                    status: res.result.nModified
                });
                cb(false, results)
            });
        }
        Providers.remoteMethod(
            'approveAndDeleteProviderRequest', {
                description: 'approveAndDeleteProviderRequest',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //End
        // by ravi on 16-1-2019
        Providers.apprAndDelAreaDocVenCount = function (param, cb) {
            var self = this;
            var ProviderCollection = self.getDataSource().connector.collection(Providers.modelName);
            var UserAreaMappCollection = self.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            const lookup = {
                $lookup: {
                    "from": "UserAreaMapping",
                    "localField": "blockId",
                    "foreignField": "areaId",
                    "as": "UserArea"
                }
            };
            const unwind = {
                $unwind: "$UserArea"
            };
            const matchArea = {
                $match: {
                    "UserArea.status": true
                }
            }
            const group = {
                $group: {
                    _id: null,
                    count: {
                        $sum: 1
                    }
                }
            }
            const options = {
                cursor: {
                    batchSize: 50
                },
                allowDiskUse: true
            }
            async.parallel({
                doctorApproveReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        providerType: 'RMP',
                        status: false,
                        delStatus: '',
                        rejectAddStatus: false
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.appStatus = "pending";
                        } else if (param.validationRole == 2) {
                            match.appStatus = {
                                $in: ["pending", "InProcess"]
                            }
                        }
                        ProviderCollection.aggregate({
                                $match: match
                            },
                            lookup, unwind, matchArea, group, options, (error, result) => {
                                if (error) {
                                    return cb(error)
                                }
                                return cb(null, result)
                            })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            };
                            match.appStatus = "pending";
                            ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                lookup, unwind, {
                                    $match: {
                                        "UserArea.status": true,
                                        "UserArea.userId": {
                                            $in: userIds
                                        }
                                    }
                                },
                                group, options,
                                function (error, result) {
                                    if (error) {
                                        console.log(error);
                                        return cb(null)
                                    }
                                    return cb(false, result);
                                }
                            )
                        })
                    }
                },
                doctorDeletionReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        providerType: 'RMP',
                        status: true,
                        // rejectDelStatus:false
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.delStatus = "pending";
                        } else if (param.validationRole == 2) {
                            match.delStatus = "InProcess";
                        }
                        // console.log("match=>",match);
                        ProviderCollection.aggregate({
                            $match: match
                        }, lookup, unwind, matchArea, group, options, (error, result) => {
                            console.log("deletion request->", result);
                            if (error) {
                                return cb(error)
                            }
                            return cb(null, result)
                        })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            };
                            match.delStatus = "pending";
                            ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                lookup, unwind, {
                                    $match: {
                                        "UserArea.status": true,
                                        "UserArea.userId": {
                                            $in: userIds
                                        }
                                    }
                                },
                                group, options,
                                function (error, result) {
                                    if (error) {
                                        console.log(error);
                                        return cb(null)
                                    }
                                    return cb(false, result);
                                }
                            )
                        })
                    }
                },
                vendorApproveReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        providerType: {
                            $in: ["Drug", "Chemist", "Stockist", "purchaseManager", "hospitalManagement", "Kiryana"]
                        },
                        status: false,
                        delStatus: '',
                        rejectAddStatus: false
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.appStatus = {
                                $in: ["pending"]
                            }
                        } else if (param.validationRole == 2) {
                            match.appStatus = {
                                $in: ["pending", "InProcess"]
                            }
                        }
                        ProviderCollection.aggregate({
                            $match: match
                        }, lookup, unwind, matchArea, group, options, (error, result) => {
                            if (error) {
                                return cb(error)
                            }
                            return cb(null, result)
                        })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            }
                            match.appStatus = "pending";
                            ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                // Stage 2
                                lookup, unwind,
                                // Stage 4
                                {
                                    $match: {
                                        "UserArea.status": true,
                                        "UserArea.userId": {
                                            $in: userIds
                                        }
                                    }
                                },
                                group, options,
                                function (err, result) {
                                    return cb(false, result);
                                });
                        })
                    }
                },
                vendorDeletionReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        providerType: {
                            $in: ["Drug", "Chemist", "Stockist", "purchaseManager", "hospitalManagement", "Kiryana"]
                        },
                        status: true,
                        rejectDelStatus: false
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.delStatus = "pending";
                        } else if (param.validationRole == 2) {
                            match.delStatus = "InProcess";
                        }
                        ProviderCollection.aggregate({
                            $match: match
                        }, lookup, unwind, matchArea, group, options, (error, result) => {
                            if (error) {
                                return cb(error)
                            }
                            return cb(null, result)
                        })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            }
                            match.delStatus = "pending";
                            ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                // Stage 2
                                lookup, unwind,
                                // Stage 4
                                {
                                    $match: {
                                        "UserArea.status": true,
                                        "UserArea.userId": {
                                            $in: userIds
                                        }
                                    }
                                },
                                group, options,
                                function (err, result) {
                                    return cb(false, result);
                                });
                        })
                    }
                },
                areaApproveReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        status: false,
                        delStatus: ''
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.appStatus = "pending";
                        } else if (param.validationRole == 2) {
                            match.appStatus = "InProcess";
                        }
                        UserAreaMappCollection.aggregate({
                            $match: match
                        }, {
                            $group: {
                                _id: null,
                                count: {
                                    $sum: 1
                                }
                            }
                        }, {
                            cursor: {
                                batchSize: 50
                            },
                            allowDiskUse: true
                        }, (error, result) => {
                            if (error) {
                                return cb(error)
                            }
                            return cb(null, result)
                        })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            }
                            match.userId = {
                                $in: userIds
                            };
                            match.appStatus = "pending";
                            UserAreaMappCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                group, options,
                                function (err, areaResult) {
                                    return cb(false, areaResult);
                                });
                        });
                    }
                },
                areaDeletionReq: (cb) => {
                    let match = {
                        companyId: ObjectId(param.companyId),
                        status: true
                    }
                    if (param.designationLevel == 0) {
                        if (param.validationRole == 1) {
                            match.delStatus = "pending";
                        } else if (param.validationRole == 2) {
                            match.delStatus = "InProcess";
                        }
                        UserAreaMappCollection.aggregate({
                            $match: match
                        }, {
                            $group: {
                                _id: null,
                                count: {
                                    $sum: 1
                                }
                            }
                        }, {
                            cursor: {
                                batchSize: 50
                            },
                            allowDiskUse: true
                        }, (error, result) => {
                            if (error) {
                                return cb(error)
                            }
                            return cb(null, result)
                        })
                    } else if (param.designationLevel > 1) {
                        Providers.app.models.Hierarchy.getManagerHierarchyInArray(param, function (err, res) {
                            var userIds = [];
                            for (var n = 0; n < res[0].userId.length; n++) {
                                userIds.push(ObjectId(res[0].userId[n]))
                            }
                            match.userId = {
                                $in: userIds
                            };
                            match.delStatus = "pending";
                            UserAreaMappCollection.aggregate(
                                // Stage 1
                                {
                                    $match: match
                                },
                                group, options,
                                function (err, result) {
                                    return cb(false, result);
                                });
                        });
                    }
                }
            }, function (err, results) {
                var returnArray = [];
                let obj = {};
                obj.areaApproveReq = results.areaApproveReq.length > 0 ? results.areaApproveReq[0].count : 0;
                obj.doctorApproveReq = results.doctorApproveReq.length > 0 ? results.doctorApproveReq[0].count : 0;
                obj.vendorApproveReq = results.vendorApproveReq.length > 0 ? results.vendorApproveReq[0].count : 0;
                obj.doctorDeletionReq = results.doctorDeletionReq.length > 0 ? results.doctorDeletionReq[0].count : 0;
                obj.vendorDeletionReq = results.vendorDeletionReq.length > 0 ? results.vendorDeletionReq[0].count : 0;
                obj.areaDeletionReq = results.areaDeletionReq.length > 0 ? results.areaDeletionReq[0].count : 0;
                returnArray.push(obj);
                return cb(false, returnArray);
            });
        };
        Providers.remoteMethod(
            'apprAndDelAreaDocVenCount', {
                description: 'get apprAndDelAreaDocVenCount',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
        Providers.getDocVenRequestsEmployeewise = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let aMatch = {};
            if (param.designationLevel == 0) {
                if (param.requsetType === 'addition') {
                    if (param.approvalUpto == 1) {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            status: false,
                            delStatus: '',
                            rejectAddStatus: false,
                            appStatus: {
                                $in: ["pending"]
                            },
                            providerType: {
                                $in: param.providerType
                            }
                        }
                    } else if (param.approvalUpto == 2) {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            status: false,
                            delStatus: '',
                            rejectAddStatus: false,

                            appStatus: {
                                $in: ["InProcess"]
                            },
                            providerType: {
                                $in: param.providerType
                            }
                        }
                    }
                } else if (param.requsetType === 'deletion') {
                    if (param.approvalUpto == 1) {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            status: true,
                            // rejectDelStatus:false,
                            delStatus: {
                                $in: ["pending"]
                            },
                            providerType: {
                                $in: param.providerType
                            }
                        }
                    } else if (param.approvalUpto == 2) {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            status: true,
                            rejectDelStatus: false,
                            delStatus: {
                                $in: ["InProcess"]
                            },
                            providerType: {
                                $in: param.providerType
                            }
                        }
                    }
                }
                ProviderCollection.aggregate( // Stage 1
                    {
                        $match: aMatch
                    }, // Stage 2
                    {
                        $lookup: {
                            "from": "UserAreaMapping",
                            "localField": "blockId",
                            "foreignField": "areaId",
                            "as": "UserArea"
                        }
                    },
                    // Stage 3
                    {
                        $unwind: "$UserArea"
                    },
                    // Stage 4
                    {
                        $match: {
                            "UserArea.status": true,
                            //"UserArea.userId": { $in: [ObjectId("5c246262b5156b14f8103847")] },
                        }
                    },
                    // Stage 5
                    {
                        $lookup: {
                            "from": "UserLogin",
                            "localField": "UserArea.userId",
                            "foreignField": "_id",
                            "as": "User"
                        }
                    },
                    // Stage 6
                    {
                        $lookup: {
                            "from": "State",
                            "localField": "stateId",
                            "foreignField": "_id",
                            "as": "states"
                        }
                    },
                    // Stage 7
                    {
                        $lookup: {
                            "from": "District",
                            "localField": "districtId",
                            "foreignField": "_id",
                            "as": "District"
                        }
                    },
                    // Stage 8
                    {
                        $project: {
                            _id: 0,
                            userId: "$UserArea.userId",
                            userName: {
                                $arrayElemAt: ["$User.name", 0]
                            },
                            stateName: {
                                $arrayElemAt: ["$states.stateName", 0]
                            },
                            District: {
                                $arrayElemAt: ["$District.districtName", 0]
                            }
                        }
                    },
                    // Stage 9
                    {
                        $group: {
                            _id: {
                                userId: "$userId",
                                userName: "$userName",
                                stateName: "$stateName",
                                District: "$District"
                            },
                            total: {
                                $sum: 1
                            }
                        }
                    },
                    function (err, providerAreaCount) {
                        if (err) {
                            console.log(err);
                            return cb(err);
                        }
                        if (!providerAreaCount) {
                            var err = new Error('no records found');
                            err.statusCode = 404;
                            err.code = 'NOT FOUND';
                            return cb(err);
                        }
                        return cb(false, providerAreaCount);
                    });
            } else if (param.designationLevel > 1) {
                var param1 = {
                    supervisorId: param.supervisorId,
                    companyId: ObjectId(param.companyId),
                    status: true
                };
                Providers.app.models.Hierarchy.getManagerHierarchyInArray(param1, function (err, res) {
                    var userIds = [];
                    for (var n = 0; n < res[0].userId.length; n++) {
                        userIds.push(ObjectId(res[0].userId[n]))
                    }
                    if (param.requsetType === 'addition') {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            status: false,
                            delStatus: '',
                            appStatus: {
                                $in: ["pending"]
                            },
                            //userId: { $in: userIds },
                            rejectAddStatus: false,

                            providerType: {
                                $in: param.providerType
                            }
                        }
                    } else if (param.requsetType === 'deletion') {
                        aMatch = {
                            companyId: ObjectId(param.companyId),
                            delStatus: {
                                $in: ["pending"]
                            },
                            status: true,
                            rejectDelStatus: false,
                            providerType: {
                                $in: param.providerType
                            }
                        }
                    }
                    ProviderCollection.aggregate(
                        // Stage 1
                        {
                            $match: aMatch
                        }, // Stage 2
                        {
                            $lookup: {
                                "from": "UserAreaMapping",
                                "localField": "blockId",
                                "foreignField": "areaId",
                                "as": "UserArea"
                            }
                        },
                        // Stage 3
                        {
                            $unwind: "$UserArea"
                        },
                        // Stage 4
                        {
                            $match: {
                                "UserArea.status": true,
                                "UserArea.userId": {
                                    $in: userIds
                                },
                            }
                        },
                        // Stage 5
                        {
                            $lookup: {
                                "from": "UserLogin",
                                "localField": "UserArea.userId",
                                "foreignField": "_id",
                                "as": "User"
                            }
                        },
                        // Stage 6
                        {
                            $lookup: {
                                "from": "State",
                                "localField": "stateId",
                                "foreignField": "_id",
                                "as": "states"
                            }
                        },
                        // Stage 7
                        {
                            $lookup: {
                                "from": "District",
                                "localField": "districtId",
                                "foreignField": "_id",
                                "as": "District"
                            }
                        },
                        // Stage 8
                        {
                            $project: {
                                _id: 0,
                                userId: "$UserArea.userId",
                                userName: {
                                    $arrayElemAt: ["$User.name", 0]
                                },
                                stateName: {
                                    $arrayElemAt: ["$states.stateName", 0]
                                },
                                District: {
                                    $arrayElemAt: ["$District.districtName", 0]
                                }
                            }
                        },
                        // Stage 9
                        {
                            $group: {
                                _id: {
                                    userId: "$userId",
                                    userName: "$userName",
                                    stateName: "$stateName",
                                    District: "$District"
                                },
                                total: {
                                    $sum: 1
                                }
                            }
                        },
                        function (err, result) {
                            if (err) {
                                return cb(err);
                            }
                            return cb(false, result);
                        }
                    );
                });
            }
        }
        Providers.remoteMethod(
            'getDocVenRequestsEmployeewise', {
                description: 'This method is used to get the all area count of the paticular users',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //end
        // by ravi on 02-05-2019
        Providers.getFocusProduct = function (providerId, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let providerIds = [];
            for (let i = 0; i < providerId.length; i++) {
                providerIds.push(ObjectId(providerId[i]));
            }
            ProviderCollection.aggregate( // Stage 1
                {
                    $match: {
                        _id: {
                            $in: providerIds
                        }
                    }
                },
                // Stage 2
                {
                    $unwind: "$focusProduct"
                },
                // Stage 3
                {
                    $lookup: {
                        "from": "Products",
                        "localField": "focusProduct",
                        "foreignField": "_id",
                        "as": "focusProd"
                    }
                },
                // Stage 4
                {
                    $project: {
                        _id: 0,
                        providerId: "$_id",
                        focusProduct: {
                            $arrayElemAt: ["$focusProd.productName", 0]
                        },
                    }
                },
                // Stage 5
                {
                    $group: {
                        _id: {
                            providerId: "$providerId"
                        },
                        focusProduct: {
                            $addToSet: "$focusProduct"
                        }
                    }
                },
                function (err, response) {
                    if (response.length > 0) {
                        return cb(false, response);
                    } else {
                        return cb(false, []);
                    }
                })
        }
        Providers.remoteMethod(
            'getFocusProduct', {
                description: 'getFocusProduct',
                accepts: [{
                    arg: 'providerId',
                    type: 'array'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //end
        Providers.getProviderListBasedOnAreaInDCR = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var blockId = [];
            for (var n = 0; n < param.blockId.length; n++) {
                blockId.push(ObjectId(param.blockId[n]));
            }
            let dynamicMatch = {
                "companyId": ObjectId(param.companyId),
                "blockId": {
                    $in: blockId
                },
                "providerType": {
                    $in: param.providerType
                },
                "appStatus": "approved",
                "status": true
            }
            /* removed after discussion with Chaman -- 12-06-2019
     if (param.companyId == "5bea8423e7e95f19bc9bc417") {
             dynamicMatch.category = { $ne: "MEF" }
         }*/
            ProviderCollection.aggregate({
                    $match: dynamicMatch
                }, {
                    $project: {
                        id: "$_id",
                        providerName: 1,
                        category: 1,
                        foucsProduct: 1,
                        providerInfo: {
                            id: "$id",
                            blockId: "$blockId"
                        }
                    }
                },
                function (err, result) {
                    if (err) {
                        console.log(err);
                        return cb(err);
                    }
                    return cb(false, result);
                });
        }
        Providers.remoteMethod(
            'getProviderListBasedOnAreaInDCR', {
                description: 'This method is used to get the all provider of selected areas',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //-----------------------Anjana  CRM Doctor 's  ROI Details(1-06-2019)-------------------------------------
        Providers.getSponserDoctorsRoi = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var userAreaMappingCollection = this.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            let fromDate = "";
            let toDate = "";
            if (param.fiscalYear == "yearly") {
                fromDate = param.year + "-01-01";
                toDate = param.year + "-12-31";
            } else if (param.fiscalYear == "financialYearly") {
                fromDate = param.year[0] + "-04-01";
                toDate = param.year[1] + "-03-31";
            }
            if (param.designationLevel == 1) {
                userAreaMappingCollection.aggregate({
                    $match: {
                        companyId: ObjectId(param.companyId),
                        userId: ObjectId(param.userId)
                    }
                }, {
                    $group: {
                        _id: {},
                        areaIds: {
                            $addToSet: "$areaId"
                        }
                    }
                }, function (err, areaMappingResult) {
                    if (err) {
                        console.log(err)
                    }
                    var dynamicMatch = {};
                    if (areaMappingResult.length == 0) {
                        dynamicMatch = {
                            blockId: {
                                $in: []
                            },
                            appStatus: "approved",
                            status: {
                                $in: [true, false]
                            },
                            companyId: ObjectId("5b2e22083225df01ba7d6059"),
                            crmStatus: true,
                            finalAppDate: {
                                $lte: new Date(toDate)
                            },
                            $or: [{
                                finalDelDate: {
                                    $gte: new Date(fromDate),
                                    $lte: new Date(toDate),
                                }
                            }, {
                                finalDelDate: {
                                    $eq: new Date("1900-01-01T00:00:00.000Z")
                                }
                            }]
                        }
                    } else {
                        dynamicMatch = {
                            blockId: {
                                $in: areaMappingResult[0].areaIds
                            },
                            appStatus: "approved",
                            status: {
                                $in: [true, false]
                            },
                            companyId: ObjectId("5b2e22083225df01ba7d6059"),
                            crmStatus: true,
                            finalAppDate: {
                                $lte: new Date(toDate)
                            },
                            $or: [{
                                finalDelDate: {
                                    $gte: new Date(fromDate),
                                    $lte: new Date(toDate),
                                }
                            }, {
                                finalDelDate: {
                                    $eq: new Date("1900-01-01T00:00:00.000Z")
                                }
                            }]
                        }
                    }
                    ProviderCollection.aggregate({
                        $match: dynamicMatch
                    }, {
                        $lookup: {
                            "from": "RXActivity",
                            "localField": "_id",
                            "foreignField": "providerId",
                            "as": "roiInfo"
                        }
                    }, {
                        $unwind: {
                            path: "$roiInfo",
                            preserveNullAndEmptyArrays: true
                        }
                    }, {
                        $project: {
                            "providerName": 1,
                            "stateId": 1,
                            "districtId": 1,
                            "blockId": 1,
                            "category": 1,
                            "status": 1,
                            "month": {
                                $ifNull: ["$roiInfo.month", 0]
                            },
                            "year": {
                                $ifNull: ["$roiInfo.year", 0]
                            },
                            "rxValue": {
                                $ifNull: ["$roiInfo.rxValue", 0]
                            },
                            "submitBy": {
                                $ifNull: ["$roiInfo.submitBy", ""]
                            }
                        }
                    }, {
                        $group: {
                            _id: {
                                "stateId": "$stateId",
                                "districtId": "$districtId",
                                "areaId": "$blockId",
                                "category": "$category",
                                "status": "$status",
                                "providerName": "$providerName",
                                "providerId": "$_id",
                                "month": "$month",
                                "year": "$year",
                                "submitBy": "$submitBy"
                            },
                            tot: {
                                $sum: "$rxValue"
                            }
                        }
                    }, {
                        $group: {
                            _id: {
                                "stateId": "$_id.stateId",
                                "districtId": "$_id.districtId",
                                "areaId": "$_id.areaId",
                                "category": "$_id.category",
                                "status": "$_id.status",
                                providerId: "$_id.providerId",
                                "providerName": "$_id.providerName"
                            },
                            tot: {
                                $sum: "$tot"
                            },
                            jandata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 1]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            febdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 2]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            mardata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 3]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            aprdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 4]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            maydata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 5]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            jundata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 6]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            juldata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 7]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            augdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 8]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            sepdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 9]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            octdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 10]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            novdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 11]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            },
                            decdata: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                                $eq: ["$_id.month", 12]
                                            },
                                            {
                                                $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                            },
                                        ]
                                    }, "$tot", "$abc"]
                                }
                            }
                        }
                    }, {
                        $lookup: {
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "stateDetails"
                        }
                    }, {
                        $lookup: {
                            "from": "District",
                            "localField": "_id.districtId",
                            "foreignField": "_id",
                            "as": "districtDetails"
                        }
                    }, {
                        $lookup: {
                            "from": "Area",
                            "localField": "_id.areaId",
                            "foreignField": "_id",
                            "as": "areaDetails"
                        }
                    }, {
                        $project: {
                            "_id": 0,
                            "stateName": {
                                $arrayElemAt: ["$stateDetails.stateName", 0]
                            },
                            "districtName": {
                                $arrayElemAt: ["$districtDetails.districtName", 0]
                            },
                            "areaName": {
                                $arrayElemAt: ["$areaDetails.areaName", 0]
                            },
                            "category": "$_id.category",
                            "status": "$_id.status",
                            "providerId": "$_id.providerId",
                            "providerName": "$_id.providerName",
                            "tot": "$tot",
                            "janData": "$jandata",
                            "febData": "$febdata",
                            "marData": "$mardata",
                            "aprData": "$aprdata",
                            "mayData": "$maydata",
                            "junData": "$jundata",
                            "julData": "$juldata",
                            "augData": "$augdata",
                            "septData": "$sepdata",
                            "octData": "$octdata",
                            "novData": "$novdata",
                            "decData": "$decdata",
                        }
                    }, function (err, providerResult) {
                        if (err) {
                            console.log(err);
                        }
                        cb(false, providerResult);
                    })
                });
            } else if (param.designationLevel >= 2) {
                let passinObjectForHierarchy = {
                    supervisorId: param.userId,
                    companyId: param.companyId,
                    type: 'lower'
                };
                Providers.app.models.Hierarchy.getManagerHierarchy(
                    passinObjectForHierarchy,
                    function (err, hierarchyResult) {
                        if (err) {
                            console.log(err);
                        }
                        let passingUserIds = [];
                        for (let i = 0; i < hierarchyResult.length; i++) {
                            passingUserIds.push(hierarchyResult[i].userId);
                        }
                        userAreaMappingCollection.aggregate({
                            $match: {
                                companyId: ObjectId(param.companyId),
                                userId: {
                                    $in: passingUserIds
                                },
                                appStatus: {
                                    $ne: "pending"
                                },
                                status: {
                                    $in: [true, false]
                                }
                            }
                        }, {
                            $group: {
                                _id: {},
                                areaIds: {
                                    $addToSet: "$areaId"
                                }
                            }
                        }, function (err, areaMappingResult) {
                            if (err) {
                                console.log(err)
                            }
                            if (areaMappingResult.length == 0) {
                                dynamicMatch = {
                                    blockId: {
                                        $in: []
                                    },
                                    appStatus: "approved",
                                    status: {
                                        $in: [true, false]
                                    },
                                    companyId: ObjectId("5b2e22083225df01ba7d6059"),
                                    crmStatus: true,
                                    finalAppDate: {
                                        $lte: new Date(toDate)
                                    },
                                    $or: [{
                                        finalDelDate: {
                                            $gte: new Date(fromDate),
                                            $lte: new Date(toDate),
                                        }
                                    }, {
                                        finalDelDate: {
                                            $eq: new Date("1900-01-01T00:00:00.000Z")
                                        }
                                    }]
                                }
                            } else {
                                dynamicMatch = {
                                    blockId: {
                                        $in: areaMappingResult[0].areaIds
                                    },
                                    appStatus: "approved",
                                    status: {
                                        $in: [true, false]
                                    },
                                    companyId: ObjectId("5b2e22083225df01ba7d6059"),
                                    crmStatus: true,
                                    finalAppDate: {
                                        $lte: new Date(toDate)
                                    },
                                    $or: [{
                                        finalDelDate: {
                                            $gte: new Date(fromDate),
                                            $lte: new Date(toDate),
                                        }
                                    }, {
                                        finalDelDate: {
                                            $eq: new Date("1900-01-01T00:00:00.000Z")
                                        }
                                    }]
                                }
                            }
                            ProviderCollection.aggregate({
                                $match: dynamicMatch
                            }, {
                                $lookup: {
                                    "from": "RXActivity",
                                    "localField": "_id",
                                    "foreignField": "providerId",
                                    "as": "roiInfo"
                                }
                            }, {
                                $unwind: {
                                    path: "$roiInfo",
                                    preserveNullAndEmptyArrays: true
                                }
                            }, {
                                $project: {
                                    "providerName": 1,
                                    "stateId": 1,
                                    "districtId": 1,
                                    "blockId": 1,
                                    "category": 1,
                                    "status": 1,
                                    "month": {
                                        $ifNull: ["$roiInfo.month", 0]
                                    },
                                    "year": {
                                        $ifNull: ["$roiInfo.year", 0]
                                    },
                                    "rxValue": {
                                        $ifNull: ["$roiInfo.rxValue", 0]
                                    },
                                    "submitBy": {
                                        $ifNull: ["$roiInfo.submitBy", ""]
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {
                                        "stateId": "$stateId",
                                        "districtId": "$districtId",
                                        "areaId": "$blockId",
                                        "category": "$category",
                                        "status": "$status",
                                        "providerName": "$providerName",
                                        "providerId": "$_id",
                                        "month": "$month",
                                        "year": "$year",
                                        "submitBy": "$submitBy"
                                    },
                                    tot: {
                                        $sum: "$rxValue"
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {
                                        "stateId": "$_id.stateId",
                                        "districtId": "$_id.districtId",
                                        "areaId": "$_id.areaId",
                                        "category": "$_id.category",
                                        "status": "$_id.status",
                                        providerId: "$_id.providerId",
                                        "providerName": "$_id.providerName"
                                    },
                                    tot: {
                                        $sum: "$tot"
                                    },
                                    jandata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 1]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    febdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 2]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    mardata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 3]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    aprdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 4]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    maydata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 5]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    jundata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 6]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    juldata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 7]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    augdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 8]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    sepdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 9]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    octdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 10]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    novdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 11]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    },
                                    decdata: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$_id.month", 12]
                                                    },
                                                    {
                                                        $eq: ["$_id.submitBy", ObjectId(param.userId)]
                                                    },
                                                ]
                                            }, "$tot", "$abc"]
                                        }
                                    }
                                }
                            }, {
                                $lookup: {
                                    "from": "State",
                                    "localField": "_id.stateId",
                                    "foreignField": "_id",
                                    "as": "stateDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "District",
                                    "localField": "_id.districtId",
                                    "foreignField": "_id",
                                    "as": "districtDetails"
                                }
                            }, {
                                $lookup: {
                                    "from": "Area",
                                    "localField": "_id.areaId",
                                    "foreignField": "_id",
                                    "as": "areaDetails"
                                }
                            }, {
                                $project: {
                                    "_id": 0,
                                    "stateName	": {
                                        $arrayElemAt: ["$stateDetails.stateName", 0]
                                    },
                                    "districtName": {
                                        $arrayElemAt: ["$districtDetails.districtName", 0]
                                    },
                                    "areaName": {
                                        $arrayElemAt: ["$areaDetails.areaName", 0]
                                    },
                                    "category": "$_id.category",
                                    "status": "$_id.status",
                                    "providerId": "$_id.providerId",
                                    "providerName": "$_id.providerName",
                                    "tot": "$tot",
                                    "janData": "$jandata",
                                    "febData": "$febdata",
                                    "marData": "$mardata",
                                    "aprData": "$aprdata",
                                    "mayData": "$maydata",
                                    "junData": "$jundata",
                                    "julData": "$juldata",
                                    "augData": "$augdata",
                                    "septData": "$sepdata",
                                    "octData": "$octdata",
                                    "novData": "$novdata",
                                    "decData": "$decdata",
                                }
                            }, function (err, providerResult) {
                                if (err) {
                                    console.log(err);
                                }
                                cb(false, providerResult);
                            })
                        });
                    });
            }
        }
        Providers.remoteMethod(
            'getSponserDoctorsRoi', {
                description: 'Getting CRM List',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //---------------------------------END-------------------------------------------------
        // by ravi for provider add-modify-view-delete methods on 15-07-2019		
        Providers.providerCreation = function (param, cb) {
            var self = this;
            var ProviderCollection = self.getDataSource().connector.collection(Providers.modelName);
            focusProductss = [];
            for (var i = 0; i < param.length; i++) {
                if (param[i].providerType == 'RMP' && param[i].focusProduct != null) {
                    for (var j = 0; j < param[i].focusProduct.length; j++) {
                        focusProductss.push(ObjectId(param[i].focusProduct[j]));
                    }
                }
            }
            delete param.focusProduct;
            param.focusProduct = focusProductss;
            Providers.create(param, function (err, result) {
                console.log("error=>",err);
                return cb(false, result);
            });
        };
        Providers.remoteMethod(
            'providerCreation', {
                description: 'providerCreation',
                accepts: [{
                    arg: 'param',
                    type: 'array'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
        Providers.getProviderDetails = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let blockIds = [];
            for (let i = 0; i < param.blockId.length; i++) {
                blockIds.push(ObjectId(param.blockId[i]));
            }
            param.companyId = ObjectId(param.companyId);
            param.providerType = {
                $in: param.providerType
            }
            param.blockId = {
                $in: blockIds
            }
            ProviderCollection.aggregate( // Stage 1
                {
                    $match: param
                },
                // Stage 3
                {
                    $lookup: {
                        "from": "Area",
                        "localField": "blockId",
                        "foreignField": "_id",
                        "as": "area"
                    }
                },
                // Stage 4
                {
                    $project: {
                        blockName: {
                            $arrayElemAt: ["$area.areaName", 0]
                        },
                        providerName: 1,
                        providerCode: 1,
                        degree: 1,
                        specialization: 1,
                        category: 1,
						dob:1,
						doa:1
                    }
                },
                function (err, response) {
                    if (response.length > 0) {
                        return cb(false, response);
                    } else {
                        return cb(false, []);
                    }
                })
        }
        Providers.remoteMethod(
            'getProviderDetails', {
                description: 'getProviderDetails',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
        //approve and delete provider For MR and MGR Himself
        Providers.approveAndDeleteProviderForMrMgrHimsef = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let providerIds1 = [];
            for (let i = 0; i < params.approveDetails.length; i++) {
                providerIds1.push(ObjectId(params.approveDetails[i].providerId));
            }
            let aSet = {};
            if (params.requestType === 'approve') {
                if (params.rL == 1) {
                    if (params.validationRole === 0) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole > 0) {
                        aSet = {
                            appStatus: 'pending',
                            mgrAppDate: new Date('1900-01-02T 00:00:00'),
                            finalAppDate: new Date('1900-01-02T 00:00:00'),
                            appByMgr: '',
                            finalAppBy: ''
                        };
                    }
                } else if (params.rL == 2) {
                    if (params.validationRole == 0 || params.validationRole == 1) {
                        aSet = {
                            status: true,
                            appStatus: 'approved',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date(),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrAppDate: new Date(),
                            finalAppDate: new Date('1900-01-02T 00:00:00'),
                            appByMgr: ObjectId(params.updatedBy),
                            finalAppBy: ''
                        };
                    }
                } else if (params.rL === 0) {
                    aSet = {
                        status: true,
                        appStatus: 'approved',
                        mgrAppDate: new Date(),
                        finalAppDate: new Date(),
                        appByMgr: ObjectId(params.updatedBy),
                        finalAppBy: ObjectId(params.updatedBy)
                    };
                }
            } else if (params.requestType === 'delete') {
                if (params.rL == 1) {
                    if (params.validationRole == 0) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            deleteReason: params.deleteReason,
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole > 0) {
                        aSet = {
                            delStatus: 'pending',
                            deleteReason: params.deleteReason,
                            mgrDelDate: new Date('1900-01-02T 00:00:00'),
                            finalDelDate: new Date('1900-01-02T 00:00:00'),
                            delByMgr: '',
                            finalDelBy: ''
                        };
                    }
                } else if (params.rL == 2) {
                    if (params.validationRole == 0 || params.validationRole == 1) {
                        aSet = {
                            status: false,
                            delStatus: 'approved',
                            deleteReason: params.deleteReason,
                            mgrDelDate: new Date(),
                            finalDelDate: new Date(),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ObjectId(params.updatedBy)
                        };
                    } else if (params.validationRole == 2) {
                        aSet = {
                            delStatus: 'InProcess',
                            deleteReason: params.deleteReason,
                            mgrDelDate: new Date(),
                            finalDelDate: new Date('1900-01-02T 00:00:00'),
                            delByMgr: ObjectId(params.updatedBy),
                            finalDelBy: ''
                        };
                    }
                } else if (params.rL === 0) {
                    aSet = {
                        status: false,
                        delStatus: 'approved',
                        mgrDelDate: new Date(),
                        finalDelDate: new Date(),
                        delByMgr: ObjectId(params.updatedBy),
                        finalDelBy: ObjectId(params.updatedBy)
                    };
                }
            }
            ProviderCollection.update({
                companyId: ObjectId(params.companyId),
                _id: {
                    $in: providerIds1
                }
            }, {
                $set: aSet
            }, {
                multi: true
            }, function (err, res) {
                if (err) {
                    return cb(err);
                }
                let results = [];
                results.push({
                    status: res.result.nModified
                });
                cb(false, results)
            });
        }
        Providers.remoteMethod(
            'approveAndDeleteProviderForMrMgrHimsef', {
                description: 'approveAndDeleteProviderForMrMgrHimsef',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        Providers.modifyProvider = function (param, cb) {
            var self = this;
            var ProviderCollection = self.getDataSource().connector.collection(Providers.modelName);
            if (param.length == 0 || param == "") {
                var err = new Error('records is undefined or empty');
                err.statusCode = 404;
                err.code = 'RECORDS ARE NOT FOUND';
                return cb(err);
            }
            let focusProductIds = [];
            for (let i = 0; i < param.length; i++) {
                if (param[i].providerType == 'RMP') {
                    for (let j = 0; j < param[i].focusProduct.length; j++) {
                        focusProductIds.push(ObjectId(param[i].focusProduct[j]));
                    }
                    param[i].focusProduct = focusProductIds;
                }
            }
            var whereId = ObjectId(param[0].id);
            delete param[0].id;
            param[0].updatedAt = new Date();
            Providers.update({
                "_id": whereId
            }, param[0], function (err, result) {
                return cb(false, result);
            });
        };
        Providers.remoteMethod(
            'modifyProvider', {
                description: 'modifyProvider',
                accepts: [{
                    arg: 'param',
                    type: 'array'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
        // end by ravi on 15-07-2019
        //------------Reject approve and delete provider request- by preeti arora-----------
        Providers.rejectApproveAndDeleteProviderRequest = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let providerIds = [];
            for (let i = 0; i < params.approveDetails.length; i++) {
                providerIds.push(ObjectId(params.approveDetails[i].providerId));
            }
            let aSet = {};
            if (params.requestType === 'rejectAddRequest') {
                if (params.rL > 1 || params.rL == 0) {
                    if (params.validationRole === 1) {
                        aSet = {
                            rejectionAddDate: new Date(),
                            rejectedBy: ObjectId(params.updatedBy),
                            rejectionMessage: params.rejectionMessage,
                            rejectAddStatus: true,
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            rejectionAddDate: new Date(),
                            rejectedBy: ObjectId(params.updatedBy),
                            rejectionMessage: params.rejectionMessage,
                            rejectAddStatus: true,
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                }
            } else if (params.requestType === 'rejectDelRequest') {
                if (params.rL > 1 || params.rL == 0) {
                    if (params.validationRole === 1) {
                        aSet = {
                            status: true,
                            appStatus: "approved",
                            delStatus: "",
                            rejectionDelDate: new Date(),
                            rejectedBy: ObjectId(params.updatedBy),
                            rejectionMessage: params.rejectionMessage,
                            rejectDelStatus: true,
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    } else if (params.validationRole === 2) {
                        aSet = {
                            appStatus: 'InProcess',
                            mgrDelDate: new Date(),
                            rejectionDelDate: new Date(),
                            rejectedBy: ObjectId(params.updatedBy),
                            rejectionMessage: params.rejectionMessage,
                            rejectDelStatus: true,
                            updatedAt: new Date(moment.utc().add('1270', 'minutes'))
                        };
                    }
                }
            }
            ProviderCollection.update({
                companyId: ObjectId(params.companyId),
                _id: {
                    $in: providerIds
                }
            }, {
                $set: aSet
            }, {
                multi: true
            }, function (err, res) {
                if (err) {
                    return cb(err);
                }
                let results = [];
                results.push({
                    status: res.result.nModified
                });
                cb(false, results)
            });
        }
        Providers.remoteMethod(
            'rejectApproveAndDeleteProviderRequest', {
                description: 'rejectApproveAndDeleteProviderRequest',
                accepts: [{
                    arg: 'params',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'post'
                }
            }
        )
        //--------------------end ------------------------------------
        // get Provider Count on Type basis i.e Specialization, Category
        Providers.getProviderCountOnTypeBasis = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let stateIds = [];
            let districtIds = [];
            let userId = [];
            let where = {
                companyId: ObjectId(param.companyId),
                status: true,
                appStatus: "approved",
                providerType: "RMP",
                delStatus: ""
            }
            if (param.stateInfo != null) {
                for (let i = 0; i < param.stateInfo.length; i++) {
                    stateIds.push(ObjectId(param.stateInfo[i]));
                }
                where.stateId = {
                    $in: stateIds
                }
            }
            if (param.districtInfo != null) {
                for (let i = 0; i < param.districtInfo.length; i++) {
                    districtIds.push(ObjectId(param.districtInfo[i]));
                }
                where.districtId = {
                    $in: districtIds
                }
            }
            if (param.userId != null) {
                for (let i = 0; i < param.userId.length; i++) {
                    userId.push(ObjectId(param.userId[i]));
                }
                where.userId = {
                    $in: userId
                }
            }
            let customProject = {};
            if (param.providerType == "specialization") {
                customProject = {
                    "type": "$specialization",
                    "blockInfo": {
                        $arrayElemAt: ["$blockInfo", 0]
                    }
                }
            } else if (param.providerType == "Category") {
                customProject = {
                    "type": "$category",
                    "blockInfo": {
                        $arrayElemAt: ["$blockInfo", 0]
                    }
                }
            } else if (param.providerType == "Degree") {
                customProject = {
                    "type": "$degree",
                    "blockInfo": {
                        $arrayElemAt: ["$blockInfo", 0]
                    }
                }
            }
            ProviderCollection.aggregate( // Stage 1
                {
                    $match: where
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "UserAreaMapping",
                        "localField": "blockId",
                        "foreignField": "areaId",
                        "as": "blockInfo"
                    }
                },
                // Stage 3
                {
                    $project: customProject
                },
                // Stage 4
                {
                    $lookup: {
                        "from": "UserInfo",
                        "localField": "blockInfo.userId",
                        "foreignField": "userId",
                        "as": "userInfo"
                    }
                },
                // // Stage 5
                {
                    $lookup: {
                        "from": "State",
                        "localField": "blockInfo.stateId",
                        "foreignField": "_id",
                        "as": "state"
                    }
                },
                // //Stage 6
                {
                    $lookup: {
                        "from": "District",
                        "localField": "blockInfo.districtId",
                        "foreignField": "_id",
                        "as": "district"
                    }
                },
                //Stage 7
                {
                    $project: {
                        "_id": 0,
                        "type": "$type",
                        "userId": {
                            $arrayElemAt: ["$userInfo.userId", 0]
                        },
                        "user": {
                            $arrayElemAt: ["$userInfo.name", 0]
                        },
                        "state": {
                            $arrayElemAt: ["$state.stateName", 0]
                        },
                        "district": {
                            $arrayElemAt: ["$district.districtName", 0]
                        }
                    }
                },
                // Stage 8
                {
                    $group: {
                        _id: {
                            "type": "$type",
                            "userId": "$userId",
                            "user": "$user",
                            "state": "$state",
                            "district": "$district"
                        },
                        tot: {
                            $sum: 1
                        }
                    }
                },
                // Stage 9
                {
                    $group: {
                        _id: {
                            "userId": "$_id.userId",
                            "user": "$_id.user",
                            "state": "$_id.state",
                            "district": "$_id.district"
                        },
                        data: {
                            $push: {
                                specialization: "$_id.type",
                                tot: "$tot"
                            }
                        }
                    }
                },
                // Stage 10
                {
                    $project: {
                        _id: 0,
                        "userId": "$_id.userId",
                        "user": "$_id.user",
                        "state": "$_id.state",
                        "district": "$_id.district",
                        "data": 1
                    }
                },
                function (err, response) {
                    if (response.length > 0) {
                        return cb(false, response);
                    } else {
                        return cb(false, []);
                    }
                })
        }
        Providers.remoteMethod(
            'getProviderCountOnTypeBasis', {
                description: 'Providers Count of selected Type in Employee ',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
        ///-------------hierarchyWise Rahul Choudhary-07-12-2019-----------------------------------------
        Providers.getdoctorDisparity = function (param, cb) {
            console.log("param : ------", param);
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var DCRMasterCollection = this.getDataSource().connector.collection(Providers.app.models.DCRMaster.modelName);
            var userAreaMappingCollection = this.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            if (param.fiscalYear == "hierarchyWise") {
                let passinObjectForHierarchy = {
                    supervisorId: param.userId,
                    companyId: param.companyId,
                    type: 'lower'
                };
                let areas = [];
                let passingUserIds = [];
                let finalObject = [];
                Providers.app.models.Hierarchy.getManagerHierarchy(
                    passinObjectForHierarchy,
                    function (err, hierarchyResult) {
                        if (err) {
                            console.log(err);
                        }
                        for (let ii = 0; ii < hierarchyResult.length; ii++) {
                            passingUserIds.push(ObjectId(hierarchyResult[ii].userId));
                        }
                        userAreaMappingCollection.aggregate({
                            $match: {
                                companyId: ObjectId(param.companyId),
                                userId: {
                                    $in: passingUserIds
                                },
                                appStatus: {
                                    $ne: "pending"
                                },
                                status: {
                                    $in: [true, false]
                                }
                            }
                        }, {
                            $group: {
                                _id: {},
                                areaIds: {
                                    $addToSet: "$areaId"
                                }
                            }
                        }, function (err, areaMappingResult) {

                            if (areaMappingResult.length > 0) {
                                for (let i = 0; i < areaMappingResult[0].areaIds.length; i++) {
                                    areas.push(ObjectId(areaMappingResult[0].areaIds[i]));
                                }
                                let providerMatch = {
                                    blockId: {
                                        $in: areas
                                    },
                                    providerType: {
                                        $in: param.type
                                    },
                                    appStatus: "approved",
                                    status: {
                                        $in: [true, false]
                                    },
                                    finalAppDate: {
                                        $lte: new Date(param.toDate)
                                    },
                                    $or: [{
                                        finalDelDate: {
                                            $gte: new Date(param.fromDate),
                                            $lte: new Date(param.toDate),
                                        }
                                    }, {
                                        finalDelDate: {
                                            $eq: new Date("1900-01-01T00:00:00.000Z")
                                        }
                                    }]
                                }
                                if (param.category.length > 0) {
                                    providerMatch.category = {
                                        $in: param.category
                                    }
                                }
                                ProviderCollection.aggregate({
                                        $match: providerMatch
                                    }, {
                                        $lookup: {
                                            "from": "State",
                                            "localField": "stateId",
                                            "foreignField": "_id",
                                            "as": "stateDetails"
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "District",
                                            "localField": "districtId",
                                            "foreignField": "_id",
                                            "as": "districtDetails"
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "Area",
                                            "localField": "blockId",
                                            "foreignField": "_id",
                                            "as": "areaDetails"
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "UserInfo",
                                            "localField": "userId",
                                            "foreignField": "userId",
                                            "as": "userdetails"
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "UserAreaMapping",
                                            "localField": "blockId",
                                            "foreignField": "areaId",
                                            "as": "userAreaDetails"
                                        }
                                    }, {
                                        $unwind: "$userAreaDetails"
                                    }, {
                                        $match: {
                                            "userAreaDetails.appStatus": {
                                                $ne: "pending"
                                            }
                                        }
                                    }, {
                                        $group: {
                                            _id: {},
                                            unlistedDoctor: {
                                                $addToSet: {
                                                    $cond: [{
                                                        $and: [{
                                                                $eq: ["$appStatus", "unlisted"]
                                                            },
                                                            {
                                                                $eq: ["$submitBy", ObjectId(param.userId)]
                                                            }
                                                        ]
                                                    }, "$$ROOT", "$abc"]
                                                }
                                            },
                                            allDoctor: {
                                                $addToSet: {
                                                    $cond: [{
                                                        $and: [{
                                                            $ne: ["$appStatus", "unlisted"]
                                                        }]
                                                    }, "$$ROOT", "$abc"]
                                                }
                                            }
                                        }
                                    }, {
                                        $project: {
                                            _id: 0,
                                            totalDoctors: {
                                                $concatArrays: ["$unlistedDoctor", "$allDoctor"]
                                            }
                                        }
                                    }, {
                                        $unwind: "$totalDoctors"
                                    },
                                    function (err, providerResult) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (providerResult.length > 0) {
                                            DCRMasterCollection.aggregate({
                                                $match: {
                                                    "companyId": ObjectId(param.companyId),
                                                    "submitBy": {
                                                        $in: passingUserIds
                                                    },
                                                    "dcrDate": {
                                                        $gte: new Date(param.fromDate),
                                                        $lte: new Date(param.toDate)
                                                    }
                                                }
                                            }, {
                                                $lookup: {
                                                    "from": "DCRProviderVisitDetails",
                                                    "localField": "_id",
                                                    "foreignField": "dcrId",
                                                    "as": "dcrVisitDetails"
                                                }
                                            }, {
                                                $unwind: "$dcrVisitDetails"
                                            }, {
                                                $match: {
                                                    "dcrVisitDetails.providerType": {
                                                        $in: param.type
                                                    }
                                                }
                                            }, {
                                                $lookup: {
                                                    "from": "Providers",
                                                    "localField": "dcrVisitDetails.providerId",
                                                    "foreignField": "_id",
                                                    "as": "providerDetails"
                                                }
                                            }, {
                                                $unwind: "$providerDetails"
                                            }, {
                                                $project: {
                                                    "providerId": "$dcrVisitDetails.providerId",
                                                    "providerName": "$providerDetails.providerName",
                                                    "stateId": "$providerDetails.stateId",
                                                    "districtId": "$providerDetails.districtId",
                                                    "visiteddate": "$dcrVisitDetails.dcrDate",
                                                    "month": {
                                                        $month: "$dcrVisitDetails.dcrDate"
                                                    },
                                                    "year": {
                                                        $year: "$dcrVisitDetails.dcrDate"
                                                    },
                                                    "day": {
                                                        day: "$dcrVisitDetails.dcrDate"
                                                    }
                                                }
                                            }, {
                                                $group: {
                                                    _id: {
                                                        providerId: "$providerId",
                                                        month: "$month",
                                                        year: "$year"
                                                    },
                                                    dates: {
                                                        $addToSet: "$day"
                                                    }
                                                }
                                            }, function (err, visitedProviderDetails) {
                                                console.log("visitedProviderDetails : ", visitedProviderDetails);
                                                if (visitedProviderDetails.length > 0) {
                                                    for (let i = 0; i < providerResult.length; i++) {
                                                        let currentProviderStatus = "";
                                                        if (providerResult[i].totalDoctors.appStatus == "unlisted") {
                                                            currentProviderStatus = "Unlisted";
                                                        } else if (providerResult[i].totalDoctors.status == true && providerResult[i].totalDoctors.appStatus == "approved") {
                                                            currentProviderStatus = "Active";
                                                        } else if (providerResult[i].totalDoctors.status == false && providerResult[i].totalDoctors.delStatus == "approved") {
                                                            currentProviderStatus = "InActive";
                                                        }
                                                        let currentAreaStatus = "";
                                                        if (providerResult[i].totalDoctors.userAreaDetails.status == true && providerResult[i].totalDoctors.userAreaDetails.appStatus == "approved") {
                                                            currentAreaStatus = "Active";
                                                        } else if (providerResult[i].totalDoctors.userAreaDetails.status == false && providerResult[i].totalDoctors.userAreaDetails.delStatus == "approved") {
                                                            currentAreaStatus = "InActive";
                                                        }
                                                        let matchedObj = visitedProviderDetails.filter(function (obj) {
                                                            let providerId = JSON.stringify(providerResult[i].totalDoctors._id);
                                                            let visitedProviderId = JSON.stringify(obj._id.providerId);
                                                            return visitedProviderId == providerId;
                                                        });
                                                        if (matchedObj.length > 0) {
                                                            let providerVisitData = [];
                                                            let VisitedDate = "";
                                                            const monthArray = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate);
                                                            for (const month of monthArray) {
                                                                let matchedObject = matchedObj.filter(function (obj) {
                                                                    //console.log(obj._id)
                                                                    //console.log(i + " ---- " + obj._id.month)
                                                                    return month.month == obj._id.month && month.year == obj._id.year;
                                                                });
                                                                if (matchedObject.length > 0) {
                                                                    for (let i = 0; i < matchedObject[0].dates.length; i++) {
                                                                        if (i == 0) {
                                                                            VisitedDate = moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                                        } else {
                                                                            VisitedDate = VisitedDate + "," + moment(matchedObject[0].dates[i].day).format("DD").toString();
                                                                        }
                                                                    }
                                                                    providerVisitData.push(VisitedDate)
                                                                } else {
                                                                    VisitedDate = "-----";
                                                                    providerVisitData.push(VisitedDate)
                                                                }
                                                            }
                                                            finalObject.push({
                                                                userName: providerResult[i].totalDoctors.userdetails[0].name,
                                                                stateName: providerResult[i].totalDoctors.stateDetails[0].stateName,
                                                                districtName: providerResult[i].totalDoctors.districtDetails[0].districtName,
                                                                areaName: providerResult[i].totalDoctors.areaDetails[0].areaName,
                                                                providerName: providerResult[i].totalDoctors.providerName,
                                                                providerCode: providerResult[i].totalDoctors.providerCode,
                                                                providerType: providerResult[i].totalDoctors.providerType,
                                                                category: providerResult[i].totalDoctors.category,
                                                                specialization: providerResult[i].totalDoctors.specialization,
                                                                degree: providerResult[i].totalDoctors.degree,
                                                                status: currentProviderStatus,
                                                                providerStatus: currentProviderStatus,
                                                                areaStatus: currentAreaStatus,
                                                                data: providerVisitData
                                                            });
                                                            //console.log(matchedObj)
                                                        } else {
                                                            const monthArray = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate);
                                                            let dataNotFOund = []
                                                            for (const month of monthArray) {
                                                                dataNotFOund.push("-----");
                                                            }
                                                            finalObject.push({
                                                                userName: providerResult[i].totalDoctors.userdetails[0].name,
                                                                stateName: providerResult[i].totalDoctors.stateDetails[0].stateName,
                                                                districtName: providerResult[i].totalDoctors.districtDetails[0].districtName,
                                                                areaName: providerResult[i].totalDoctors.areaDetails[0].areaName,
                                                                providerName: providerResult[i].totalDoctors.providerName,
                                                                providerType: providerResult[i].totalDoctors.providerType,
                                                                providerCode: providerResult[i].totalDoctors.providerCode,
                                                                category: providerResult[i].totalDoctors.category,
                                                                specialization: providerResult[i].totalDoctors.specialization,
                                                                degree: providerResult[i].totalDoctors.degree,
                                                                status: currentProviderStatus,
                                                                providerStatus: currentProviderStatus,
                                                                areaStatus: currentAreaStatus,
                                                                data: dataNotFOund
                                                            });
                                                            //console.log("Final Data : ",finalObject);
                                                        }
                                                    }
                                                } else {
                                                    for (let i = 0; i < providerResult.length; i++) {
                                                        let currentProviderStatus = "";
                                                        if (providerResult[i].totalDoctors.appStatus == "unlisted") {
                                                            currentProviderStatus = "Unlisted";
                                                        } else if (providerResult[i].totalDoctors.status == true && providerResult[i].totalDoctors.appStatus == "approved") {
                                                            currentProviderStatus = "Active";
                                                        } else if (providerResult[i].totalDoctors.status == false && providerResult[i].totalDoctors.delStatus == "approved") {
                                                            currentProviderStatus = "InActive";
                                                        }
                                                        let currentAreaStatus = "";
                                                        if (providerResult[i].totalDoctors.userAreaDetails.status == true && providerResult[i].totalDoctors.userAreaDetails.appStatus == "approved") {
                                                            currentAreaStatus = "Active";
                                                        } else if (providerResult[i].totalDoctors.userAreaDetails.status == false && providerResult[i].totalDoctors.userAreaDetails.delStatus == "approved") {
                                                            currentAreaStatus = "InActive";
                                                        }
                                                        const monthArray = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate);
                                                        let dataNotFOund = []
                                                        for (const month of monthArray) {
                                                            dataNotFOund.push("-----");
                                                        }
                                                        finalObject.push({
                                                            userName: providerResult[i].totalDoctors.userdetails[0].name,
                                                            stateName: providerResult[i].totalDoctors.stateDetails[0].stateName,
                                                            districtName: providerResult[i].totalDoctors.districtDetails[0].districtName,
                                                            areaName: providerResult[i].totalDoctors.areaDetails[0].areaName,
                                                            providerName: providerResult[i].totalDoctors.providerName,
                                                            providerCode: providerResult[i].totalDoctors.providerCode,
                                                            providerType: providerResult[i].totalDoctors.providerType,
                                                            category: providerResult[i].totalDoctors.category,
                                                            specialization: providerResult[i].totalDoctors.specialization,
                                                            degree: providerResult[i].totalDoctors.degree,
                                                            status: currentProviderStatus,
                                                            providerStatus: currentProviderStatus,
                                                            areaStatus: currentAreaStatus,
                                                            data: dataNotFOund
                                                        });
                                                    }
                                                }
                                                cb(false, finalObject);
                                            });
                                        } else {
                                            //Excluding MR or MGR nam, because he doesnt have even 1 Provider.
                                        }
                                    });

                            } else {
                                return cb(false, [])
                            }
                        });
                    });
            }
        }
        Providers.remoteMethod(
            'getdoctorDisparity', {
                description: 'Getting Doctor Disparity List',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //-----------By preeti arora- 3-02-2020------------------
        Providers.getFieldForceStatus = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var UserAreaCollection = this.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            let stateId = [];
            let districtId = [];
            let divisionId = [];
            let userId = [];
            let where = {
                companyId: ObjectId(params.companyId),
            };
            if (params.type == "Employee Wise") {
                for (var i = 0; i < params.userIds.length; i++) {
                    userId.push(ObjectId(params.userIds[i]))
                }
                where.userId = {
                    $in: userId
                }
                async.parallel({
                    providerResult: function (cb) {
                        ProviderCollection.aggregate(
                            // Stage 1
                            {
                                $match: where
                            },
                            // Stage 2
                            {
                                $lookup: {
                                    "from": "UserInfo",
                                    "localField": "userId",
                                    "foreignField": "userId",
                                    "as": "userData"
                                }
                            },
                            // Stage 3
                            {
                                $unwind: "$userData"
                            },
                            // Stage 4
                            {
                                $project: {
                                    divisionId: {
                                        $arrayElemAt: ["$userData.divisionId", 0]
                                    },
                                    stateId: 1,
                                    districtId: 1,
                                    userId: 1,
                                    blockId: 1,
                                    providerType: 1,
                                    companyId: 1,
                                    appStatus: 1,
                                    delStatus: 1,
                                    status: 1,
                                    userName: "$userData.name"
                                }
                            },
                            // Stage 5
                            {
                                $lookup: {
                                    "from": "DivisionMaster",
                                    "localField": "divisionId",
                                    "foreignField": "_id",
                                    "as": "divData"
                                }
                            },
                            // Stage 6
                            {
                                $lookup: {
                                    "from": "State",
                                    "localField": "stateId",
                                    "foreignField": "_id",
                                    "as": "stateData"
                                }
                            },
                            // Stage 7
                            {
                                $unwind: {
                                    path: "$stateData",
                                    preserveNullAndEmptyArrays: true
                                }
                            },
                            // Stage 8
                            {
                                $lookup: {
                                    "from": "District",
                                    "localField": "districtId",
                                    "foreignField": "_id",
                                    "as": "disData"
                                }
                            },
                            // Stage 9
                            {
                                $unwind: {
                                    path: "$disData",
                                    preserveNullAndEmptyArrays: true
                                }
                            },
                            // Stage 10
                            {
                                $unwind: {
                                    path: "$divData",
                                    preserveNullAndEmptyArrays: true
                                }
                            },
                            // Stage 11
                            {
                                $group: {
                                    _id: {
                                        userId: "$userId",
                                    },
                                    totalActiveRMP: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$appStatus", "approved"]
                                                    },
                                                    {
                                                        $eq: ["$providerType", "RMP"]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
                                    totalInActiveRMP: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$delStatus", "approved"]
                                                }, {
                                                    $eq: ["$providerType", "RMP"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedRMP: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$appStatus", "unlisted"]
                                                }, {
                                                    $eq: ["$providerType", "RMP"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    totalActiveDrug: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$appStatus", "approved"]
                                                    },
                                                    {
                                                        $eq: ["$providerType", "Drug"]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
                                    totalInActiveDrug: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$delStatus", "approved"]
                                                }, {
                                                    $eq: ["$providerType", "Drug"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedDrug: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$appStatus", "unlisted"]
                                                }, {
                                                    $eq: ["$providerType", "Drug"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    totalActiveStockist: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $eq: ["$appStatus", "approved"]
                                                    },
                                                    {
                                                        $eq: ["$providerType", "Stockist"]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
                                    totalInActiveStockist: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$delStatus", "approved"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedStockist: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$appStatus", "unlisted"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    userName: {
                                        $addToSet: "$userName"
                                    },
                                    state: {
                                        $addToSet: "$stateData"
                                    },
                                    district: {
                                        $addToSet: "$disData"
                                    },
                                    division: {
                                        $addToSet: "$divData"
                                    }
                                }
                            },
                            // Stage 12
                            {
                                $project: {
                                    userName: {
                                        $arrayElemAt: ["$userName", 0]
                                    },
                                    state: {
                                        $arrayElemAt: ["$state.stateName", 0]
                                    },
                                    district: {
                                        $arrayElemAt: ["$district.districtName", 0]
                                    },
                                    totalActiveRMP: 1,
                                    totalInActiveRMP: 1,
                                    unlistedRMP: 1,
                                    unlistedDrug: 1,
                                    totalActiveDrug: 1,
                                    totalInActiveDrug: 1,
                                    totalActiveStockist: 1,
                                    totalInActiveStockist: 1,
                                    unlistedStockist: 1,
                                    divisionName: {
                                        $arrayElemAt: ["$division.divisionName", 0]
                                    },
                                }
                            },
                            function (err, providerResult) {
                                if (providerResult.length > 0) {
                                    cb(false, providerResult)
                                }
                            }
                        );
                    },
                    areaResult: function (cb) {
                        UserAreaCollection.aggregate(
                            // Stage 1
                            {
                                $match: where
                            },
                            // Stage 2
                            {
                                $group: {
                                    _id: {
                                        userId: "$userId"
                                    },
                                    activeArea: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$status", true]
                                                }, ]
                                            }, 1, 0]
                                        }
                                    },
                                    InactiveArea: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$status", false]
                                                }]
                                            }, 1, 0]
                                        }
                                    }
                                }
                            },
                            // Stage 3
                            {
                                $project: {
                                    activeArea: 1,
                                    InactiveArea: 1
                                }
                            },
                            function (err, areaRes) {
                                if (areaRes.length > 0) {
                                    cb(false, areaRes)
                                }
                            }
                        );
                    }
                }, function (err, asynRes) {
                    let finalObj = [];
                    let inactiveArea = "";
                    let activeArea = "";
                    if (asynRes.providerResult.length > 0) {
                        for (const result of asynRes.providerResult) {
                            const matchedObject = asynRes.areaResult.filter(function (obj) {
                                return (obj._id.userId).toString() == (result._id.userId).toString()
                            });
                            if (matchedObject.length > 0) {
                                activeArea = matchedObject[0].activeArea,
                                    inactiveArea = matchedObject[0].InactiveArea
                            } else {
                                activeArea = matchedObject[0].activeArea,
                                    inactiveArea = matchedObject[0].InactiveArea
                            }
                            finalObj.push({
                                totalActiveRMP: result.totalActiveRMP,
                                totalInActiveRMP: result.totalInActiveRMP,
                                unlistedRMP: result.unlistedRMP,
                                totalActiveDrug: result.totalActiveDrug,
                                totalInActiveDrug: result.totalInActiveDrug,
                                totalActiveStockist: result.totalActiveStockist,
                                totalInActiveStockist: result.totalInActiveStockist,
                                unlistedStockist: result.unlistedStockist,
                                userName: result.userName,
                                state: result.state,
                                district: result.district,
                                activeArea: activeArea,
                                InactiveArea: inactiveArea,
                                unlistedDrug: result.unlistedDrug,
                                divisionName: result.divisionName,
                            })
                        }
                        return cb(false, finalObj)
                    } else {
                        return cb(false, [])
                    }
                })
            }
            if (params.type == "State" || params.type == "Headquarter") {
                Providers.app.models.UserInfo.getAllUserIdsBasedOnType(params,
                    function (err, userRecords) {
                        let userIds = [];
                        for (var i = 0; i < userRecords[0].userIds.length; i++) {
                            userIds.push(ObjectId(userRecords[0].userIds[i]))
                        }
                        where.userId = {
                            $in: userIds
                        }
                        async.parallel({
                            providerResult: function (cb) {
                                ProviderCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: where
                                    },
                                    // Stage 2
                                    {
                                        $lookup: {
                                            "from": "UserInfo",
                                            "localField": "userId",
                                            "foreignField": "userId",
                                            "as": "userData"
                                        }
                                    },
                                    // Stage 3
                                    {
                                        $unwind: "$userData"
                                    },
                                    // Stage 4
                                    {
                                        $project: {
                                            divisionId: {
                                                $arrayElemAt: ["$userData.divisionId", 0]
                                            },
                                            stateId: 1,
                                            districtId: 1,
                                            userId: 1,
                                            blockId: 1,
                                            providerType: 1,
                                            companyId: 1,
                                            appStatus: 1,
                                            delStatus: 1,
                                            status: 1,
                                            userName: "$userData.name"
                                        }
                                    },
                                    // Stage 5
                                    {
                                        $lookup: {
                                            "from": "DivisionMaster",
                                            "localField": "divisionId",
                                            "foreignField": "_id",
                                            "as": "divData"
                                        }
                                    },
                                    // Stage 6
                                    {
                                        $lookup: {
                                            "from": "State",
                                            "localField": "stateId",
                                            "foreignField": "_id",
                                            "as": "stateData"
                                        }
                                    },
                                    // Stage 7
                                    {
                                        $unwind: {
                                            path: "$stateData",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },
                                    // Stage 8
                                    {
                                        $lookup: {
                                            "from": "District",
                                            "localField": "districtId",
                                            "foreignField": "_id",
                                            "as": "disData"
                                        }
                                    },
                                    // Stage 9
                                    {
                                        $unwind: {
                                            path: "$disData",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },
                                    // Stage 10
                                    {
                                        $unwind: {
                                            path: "$divData",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },
                                    // Stage 11
                                    {
                                        $group: {
                                            _id: {
                                                userId: "$userId",
                                            },
                                            totalActiveRMP: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                                $eq: ["$appStatus", "approved"]
                                                            },
                                                            {
                                                                $eq: ["$providerType", "RMP"]
                                                            }
                                                        ]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalInActiveRMP: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$delStatus", "approved"]
                                                        }, {
                                                            $eq: ["$providerType", "RMP"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            unlistedRMP: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$appStatus", "unlisted"]
                                                        }, {
                                                            $eq: ["$providerType", "RMP"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalActiveDrug: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                                $eq: ["$appStatus", "approved"]
                                                            },
                                                            {
                                                                $eq: ["$providerType", "Drug"]
                                                            }
                                                        ]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalInActiveDrug: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$delStatus", "approved"]
                                                        }, {
                                                            $eq: ["$providerType", "Drug"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            unlistedDrug: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$appStatus", "unlisted"]
                                                        }, {
                                                            $eq: ["$providerType", "Drug"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalActiveStockist: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                                $eq: ["$appStatus", "approved"]
                                                            },
                                                            {
                                                                $eq: ["$providerType", "Stockist"]
                                                            }
                                                        ]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalInActiveStockist: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$delStatus", "approved"]
                                                        }, {
                                                            $eq: ["$providerType", "Stockist"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            unlistedStockist: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$appStatus", "unlisted"]
                                                        }, {
                                                            $eq: ["$providerType", "Stockist"]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            },
                                            userName: {
                                                $addToSet: "$userName"
                                            },
                                            state: {
                                                $addToSet: "$stateData"
                                            },
                                            district: {
                                                $addToSet: "$disData"
                                            },
                                            division: {
                                                $addToSet: "$divData"
                                            }
                                        }
                                    },
                                    // Stage 12
                                    {
                                        $project: {
                                            userName: {
                                                $arrayElemAt: ["$userName", 0]
                                            },
                                            state: {
                                                $arrayElemAt: ["$state.stateName", 0]
                                            },
                                            district: {
                                                $arrayElemAt: ["$district.districtName", 0]
                                            },
                                            totalActiveRMP: 1,
                                            totalInActiveRMP: 1,
                                            unlistedRMP: 1,
                                            unlistedDrug: 1,
                                            totalActiveDrug: 1,
                                            totalInActiveDrug: 1,
                                            totalActiveStockist: 1,
                                            totalInActiveStockist: 1,
                                            unlistedStockist: 1,
                                            divisionName: {
                                                $arrayElemAt: ["$division.divisionName", 0]
                                            },
                                        }
                                    },
                                    function (err, providerResult) {
                                        if (providerResult.length > 0) {
                                            cb(false, providerResult)
                                        } else {
                                            cb(false, [])
                                        }
                                    }
                                );
                            },
                            areaResult: function (cb) {
                                UserAreaCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: where
                                    },
                                    // Stage 2
                                    {
                                        $group: {
                                            _id: {
                                                userId: "$userId"
                                            },
                                            activeArea: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$status", true]
                                                        }, ]
                                                    }, 1, 0]
                                                }
                                            },
                                            InactiveArea: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [{
                                                            $eq: ["$status", false]
                                                        }]
                                                    }, 1, 0]
                                                }
                                            }
                                        }
                                    },
                                    // Stage 3
                                    {
                                        $project: {
                                            activeArea: 1,
                                            InactiveArea: 1
                                        }
                                    },
                                    function (err, areaRes) {
                                        if (areaRes.length > 0) {
                                            cb(false, areaRes)
                                        }
                                    }
                                );
                            }
                        }, function (err, asynRes) {
                            let finalObj = [];
                            let inactiveArea = "";
                            let activeArea = "";
                            if (asynRes.providerResult.length > 0) {
                                for (const result of asynRes.providerResult) {
                                    const matchedObject = asynRes.areaResult.filter(function (obj) {
                                        return (obj._id.userId).toString() == (result._id.userId).toString()
                                    });
                                    if (matchedObject.length > 0) {
                                        activeArea = matchedObject[0].activeArea,
                                            inactiveArea = matchedObject[0].InactiveArea
                                    } else {
                                        activeArea = matchedObject[0].activeArea,
                                            inactiveArea = matchedObject[0].InactiveArea
                                    }
                                    finalObj.push({
                                        totalActiveRMP: result.totalActiveRMP,
                                        totalInActiveRMP: result.totalInActiveRMP,
                                        unlistedRMP: result.unlistedRMP,
                                        totalActiveDrug: result.totalActiveDrug,
                                        totalInActiveDrug: result.totalInActiveDrug,
                                        totalActiveStockist: result.totalActiveStockist,
                                        totalInActiveStockist: result.totalInActiveStockist,
                                        unlistedStockist: result.unlistedStockist,
                                        userName: result.userName,
                                        state: result.state,
                                        district: result.district,
                                        activeArea: activeArea,
                                        InactiveArea: inactiveArea,
                                        unlistedDrug: result.unlistedDrug,
                                        divisionName: result.divisionName,
                                    })
                                }
                                return cb(false, finalObj)
                            } else {
                                return cb(false, [])
                            }
                        })
                    });
            }
        }
        Providers.remoteMethod(
            'getFieldForceStatus', {
                description: 'Getting field force status',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //---------------------------
        //YYYY-MM-DD Date Format
        function monthNameAndYearBTWTwoDate(fromDate, toDate) {
            let monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            let arr = [];
            let datFrom = new Date(fromDate);
            let datTo = new Date(toDate);
            let fromYear = datFrom.getFullYear();
            let toYear = datTo.getFullYear();
            let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
            for (let i = datFrom.getMonth(); i <= diffYear; i++) {
                arr.push({
                    monthName: monthNames[i % 12],
                    month: (i % 12) + 1,
                    year: Math.floor(fromYear + (i / 12)),
                    endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
                });
            }
            return arr;
        }
        /**
         * This API is getting the all providers based on blockIds, We already have the find API on providers but we at manager level we have many areas of his
         * the hierarchy, so the exisiting API is not executing.
         *
         **/
        Providers.getProvidersBasedOnBlockIds = function (param, cb) {
            var self = this;
            let where = {
                "companyId": param.companyId,
                "blockId": {
                    "inq": param.blockId
                }
            };
            if (param.updatedAt !== undefined) {
                where.updatedAt = {
                    gt: param.updatedAt
                }
            }
            self.find({
                where: where,
                skip: param.skip,
                limit: param.limit,
            }, function (err, result) {
                //console.log(result);
                if (err) return cb(err);
                return cb(false, result)
            })
        }
        Providers.remoteMethod('getProvidersBasedOnBlockIds', {
            description: 'getProvidersOfEmployeesUnderManager',
            accepts: [{
                arg: 'param',
                type: 'object',
                http: {
                    source: 'body'
                }
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        });
        /**
         * 
         * Get Providers count
         *
         **/
        Providers.getProvidersCount = function (param, cb) {
            var self = this;
            self.find({
                where: {
                    "companyId": param.companyId,
                    "blockId": {
                        "inq": param.blockId
                    }
                }
            }, function (err, result) {
                //console.log(result);
                if (err) return cb(err);
                return cb(false, {
                    count: result.length
                })
            })
        }
        Providers.remoteMethod('getProvidersCount', {
            description: 'get Providers count based on block ids',
            accepts: [{
                arg: 'param',
                type: 'object',
                http: {
                    source: 'body'
                }
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'post'
            }
        });
        //------Rahul Choudhary maintan logs of Provider untagged.
        Providers.untagedProviderLogs = function (params, cb) {
            var self = this;
            self.find({
                where: {
                    "_id": ObjectId(params.id)
                }
            }, function (err, result) {
                var untagproviderObj = {};
                untagproviderObj = {
                    "stateId": result[0].stateId,
                    "districtId": result[0].districtId,
                    "providerId": result[0].id,
                    "companyId": result[0].companyId,
                    "geoLocation": result[0].geoLocation,
                    "userId": result[0].userId,
                    "blockId": result[0].blockId,
                    "createdAt": new Date(),
                    "updatedAt": new Date()
                }

                Providers.app.models.ProvidersUntagLogs.create(untagproviderObj,
                    function (Insertresult) {
                        return cb(false, Insertresult)
                    });
            })
        }
        Providers.remoteMethod('untagedProviderLogs', {
            description: '',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        });

        //------Preeti Arora getting master data 10-08-2020--------------------
        Providers.getMasterData = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var UserAreaCollection = self.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            let where = {
                status: true,
                appStatus: "approved",
                companyId: ObjectId(params.companyId),
            };
            async.parallel({
                userResult: function (cb) {
                    if (params.type == "State" || params.type == "Headquarter") {
                        Providers.app.models.UserInfo.getAllUserIdsBasedOnType(
                            params,
                            function (err, userRecords) {
                                cb(false, userRecords);
                            });
                    } else if (params.type == "Employee Wise") {
                        Providers.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            params.companyId,
                            params.status,
                            params.userIds,
                            function (err, userRecords) {
                                if (err) {
                                    sendMail.sendMail({
                                        collectionName: 'Tourprogram',
                                        errorObject: err,
                                        paramsObject: params,
                                        methodName: 'getTPStatus'
                                    });
                                }
                                cb(false, userRecords);
                            });
                    }
                }
            }, function (err, asyncResult) {
                if (err) {
                    sendMail.sendMail({
                        collectionName: "Providers",
                        errorObject: err,
                        paramsObject: params,
                        methodName: "Providers.getMasterData",
                    });
                }
                var where = {
                    companyId: ObjectId(params.companyId),
                    userId: {
                        $in: asyncResult.userResult[0].userIds
                    },
                    status: params.status,
                }
                if (asyncResult.userResult.length > 0) {
                    async.parallel({
                        ProvidersCount: function (cb) {
                            ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: where
                                },
                                // Stage 2
                                {
                                    $group: {
                                        _id: {
                                            userId: "$userId"
                                        },
                                        totalRMP: {
                                            $sum: {
                                                $cond: [{
                                                    $eq: ["$providerType", "RMP"]
                                                }, 1, 0]
                                            }
                                        },
                                        totalStockist: {
                                            $sum: {
                                                $cond: [{
                                                    $eq: ["$providerType", "Stockist"]
                                                }, 1, 0]
                                            }
                                        },
                                        totalDrug: {
                                            $sum: {
                                                $cond: [{
                                                    $eq: ["$providerType", "Drug"]
                                                }, 1, 0]
                                            }
                                        },
                                    }
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "UserInfo",
                                        "localField": "_id.userId",
                                        "foreignField": "userId",
                                        "as": "userDetails"
                                    }
                                },

                                // Stage 4
                                {
                                    $unwind: "$userDetails"
                                },

                                // Stage 5
                                {
                                    $lookup: {
                                        "from": "State",
                                        "localField": "userDetails.stateId",
                                        "foreignField": "_id",
                                        "as": "state"
                                    }
                                },

                                // Stage 6
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "userDetails.districtId",
                                        "foreignField": "_id",
                                        "as": "dis"
                                    }
                                },

                                // Stage 7
                                {
                                    $project: {
                                        state: {
                                            $arrayElemAt: ["$state.stateName", 0]
                                        },
                                        district: {
                                            $arrayElemAt: ["$dis.districtName", 0]
                                        },
                                        user: "$userDetails.name",
                                        totalDrug: 1,
                                        totalStockist: 1,
                                        totalRMP: 1

                                    }
                                },
                                function (err, res) {
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getMasterData",
                                        });
                                    }
                                    cb(false, res);

                                }
                            );
                        },
                        areaCount: function (cb) {
                            UserAreaCollection.aggregate(
                                // Stage 1
                                {
                                    $match: where
                                },

                                // Stage 2
                                {
                                    $group: {
                                        _id: {
                                            userId: "$userId"
                                        },
                                        totalArea: {
                                            $addToSet: "$areaId"
                                        },
                                    }
                                }, {
                                    $lookup: {
                                        "from": "UserInfo",
                                        "localField": "_id.userId",
                                        "foreignField": "userId",
                                        "as": "userDetails"
                                    }
                                },

                                // Stage 4
                                {
                                    $unwind: "$userDetails"
                                },

                                // Stage 5
                                {
                                    $lookup: {
                                        "from": "State",
                                        "localField": "userDetails.stateId",
                                        "foreignField": "_id",
                                        "as": "state"
                                    }
                                },

                                // Stage 6
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "userDetails.districtId",
                                        "foreignField": "_id",
                                        "as": "dis"
                                    }
                                }, {
                                    $project: {
                                        userId: "$_id.userId",
                                        totalArea: {
                                            $size: "$totalArea"
                                        },
                                        state: {
                                            $arrayElemAt: ["$state.stateName", 0]
                                        },
                                        district: {
                                            $arrayElemAt: ["$dis.districtName", 0]
                                        },
                                        user: "$userDetails.name",
                                    }
                                },
                                function (err, res) {
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getMasterData",
                                        });
                                    }
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getMasterData",
                                        });
                                    }
                                    cb(false, res);
                                }

                            );

                        }
                    }, function (err, providerCount) {
                        if (err) {
                            sendMail.sendMail({
                                collectionName: "Providers",
                                errorObject: err,
                                paramsObject: params,
                                methodName: "Providers.getMasterData",
                            });
                        }
                        const finalData = [];
                        if (providerCount.areaCount.length > 0) {
                            for (let i = 0; i < asyncResult.userResult[0].obj.length; i++) {
                                let filterObj = providerCount.areaCount.filter(function (obj) {
                                    return obj.userId.toString() == asyncResult.userResult[0].obj[i].userId.toString()
                                });
                                if (filterObj.length > 0) {


                                    // get master data

                                    let filteProviderObj = providerCount.ProvidersCount.filter(function (obj) {
                                        return obj._id.userId.toString() == filterObj[0].userId.toString()
                                    });
                                    if (filteProviderObj.length > 0) {
                                        // provider found
                                        finalData.push({
                                            state: filteProviderObj[0].state,
                                            district: filteProviderObj[0].district,
                                            user: filteProviderObj[0].user,
                                            totalDrug: filteProviderObj[0].totalDrug,
                                            totalStockist: filteProviderObj[0].totalStockist,
                                            totalRMP: filteProviderObj[0].totalRMP,
                                            totalArea: filterObj[0].totalArea
                                        });
                                    } else {
                                        // only area found
                                        finalData.push({
                                            state: filterObj.state,
                                            district: filterObj.district,
                                            user: filterObj.user,
                                            totalDrug: 0,
                                            totalStockist: 0,
                                            totalRMP: 0,
                                            totalArea: filterObj[0].totalArea
                                        });
                                    }
                                } else {
                                    // for those whose data is not submitted yet.
                                    finalData.push({
                                        user: asyncResult.userResult[0].obj[i].name,
                                        state: asyncResult.userResult[0].obj[i].stateName,
                                        district: asyncResult.userResult[0].obj[i].districtName,
                                        totalDrug: 0,
                                        totalStockist: 0,
                                        totalRMP: 0,
                                        totalArea: 0
                                    });
                                }
                            }
                            return cb(false, finalData);

                        } else {
                            return cb(false, []);
                        }
                    })
                } else {
                    return cb(false, [])
                }

            })




        }
        Providers.remoteMethod('getMasterData', {
            description: 'Get Total Master Data',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        });
        //------------end ------------------





        //------Rahul Saini getting Station details 24-0-2020--------------------
        Providers.getStationData = function (params, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var UserAreaCollection = self.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
            let where = {
                status: true,
                appStatus: "approved",
                companyId: ObjectId(params.companyId),
            };
            async.parallel({
                userResult: function (cb) {
                    if (params.type == "State" || params.type == "Headquarter") {
                        Providers.app.models.UserInfo.getAllUserIdsBasedOnType(
                            params,
                            function (err, userRecords) {
                                cb(false, userRecords);
                            });
                    } else if (params.type == "Employee Wise") {
                        Providers.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            params.companyId,
                            params.status,
                            params.userIds,
                            function (err, userRecords) {
                                if (err) {
                                    sendMail.sendMail({
                                        collectionName: 'Tourprogram',
                                        errorObject: err,
                                        paramsObject: params,
                                        methodName: 'getTPStatus'
                                    });
                                }
                                cb(false, userRecords);
                            });
                    }
                }
            }, function (err, asyncResult) {
                if (err) {
                    sendMail.sendMail({
                        collectionName: "Providers",
                        errorObject: err,
                        paramsObject: params,
                        methodName: "Providers.getStationData",
                    });
                }
                var where = {
                    companyId: ObjectId(params.companyId),
                    userId: {
                        $in: asyncResult.userResult[0].userIds
                    },
                    status: params.status,
                }
                if (asyncResult.userResult.length > 0) {
                    async.parallel({

                        ProvidersCount: function (cb) {
                            UserAreaCollection.aggregate(
                                // Stage 1
                                {
                                    $match: where
                                },
                                // Stage 2
                                {
                                    $lookup: {
                                        "from": "Providers",
                                        "localField": "areaId",
                                        "foreignField": "blockId",
                                        "as": "providerData"

                                    }

                                },

                                // Stage 3

                                {

                                    $unwind: {
                                        'path': '$providerData',
                                        'preserveNullAndEmptyArrays': true
                                    }

                                },
                                // Stage 3

                                {
                                    $group: {

                                        '_id': {
                                            areaId: "$areaId"
                                        },

                                        totalRMP: {
                                            $sum: {
                                                $cond: [{
                                                        $and: [{
                                                                $eq: ["$providerData.status", true]
                                                            },
                                                            {
                                                                $eq: ["$providerData.providerType", "RMP"]
                                                            }
                                                        ]
                                                    },
                                                    1,
                                                    0
                                                ]
                                            }
                                        },
                                        totalStockist: {
                                            $sum: {
                                                $cond: [{
                                                        $and: [{
                                                                $eq: ["$providerData.status", true]
                                                            },
                                                            {
                                                                $eq: ["$providerData.providerType", "Stockist"]
                                                            }
                                                        ]
                                                    },
                                                    1,
                                                    0
                                                ]
                                            }
                                        },

                                        totalDrug: {
                                            $sum: {
                                                $cond: [{
                                                        $and: [{
                                                                $eq: ["$providerData.status", true]
                                                            },
                                                            {
                                                                $eq: ["$providerData.providerType", "Drug"]
                                                            }
                                                        ]
                                                    },
                                                    1,
                                                    0
                                                ]
                                            }
                                        }





                                    }

                                },

                                // Stage 3

                                {
                                    $lookup: {
                                        "from": "Area",
                                        "localField": "_id.areaId",
                                        "foreignField": "_id",
                                        "as": "areaDetails"

                                    }

                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "UserAreaMapping",
                                        "localField": "_id.areaId",
                                        "foreignField": "areaId",
                                        "as": "UserAreaData"
                                    }
                                },



                                // Stage 4
                                {
                                    $unwind: "$UserAreaData"
                                },

                                // Stage 5
                                {
                                    $lookup: {
                                        "from": "UserInfo",
                                        "localField": "UserAreaData.userId",
                                        "foreignField": "userId",
                                        "as": "userDetails"
                                    }
                                },

                                // Stage 6
                                {
                                    $lookup: {
                                        "from": "State",
                                        "localField": "UserAreaData.stateId",
                                        "foreignField": "_id",
                                        "as": "state"
                                    }
                                },

                                // Stage 7
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "UserAreaData.districtId",
                                        "foreignField": "_id",
                                        "as": "dis"
                                    }
                                },

                                // Stage 8
                                {
                                    $project: {

                                        district: {
                                            $arrayElemAt: ["$dis.districtName", 0]
                                        },
                                        state: {
                                            $arrayElemAt: ["$state.stateName", 0]
                                        },
                                        userId: {
                                            $arrayElemAt: ["$userDetails.userId", 0]
                                        },

                                        user: {
                                            $arrayElemAt: ["$userDetails.name", 0]
                                        },

                                        Area: {
                                            $arrayElemAt: ["$areaDetails.areaName", 0]
                                        },
                                        totalDrug: 1,
                                        totalStockist: 1,
                                        totalRMP: 1

                                    }
                                },
                                function (err, res) {
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getMasterData",
                                        });
                                    }
                                    cb(false, res);

                                }
                            );
                        },

                        areaCount: function (cb) {
                            UserAreaCollection.aggregate(
                                // Stage 1
                                {
                                    $match: where
                                },

                                // Stage 2
                                {
                                    $lookup: {
                                        "from": "Area",
                                        "localField": "areaId",
                                        "foreignField": "_id",
                                        "as": "areaDetails"
                                    }
                                },

                                // Stage 4

                                {
                                    $unwind: "$areaDetails"
                                },

                                // Stage 4

                                {
                                    $lookup: {
                                        "from": "UserInfo",
                                        "localField": "userId",
                                        "foreignField": "userId",
                                        "as": "userDetails"
                                    }
                                },

                                // Stage 4
                                {
                                    $unwind: "$userDetails"
                                },

                                // Stage 5
                                {
                                    $lookup: {
                                        "from": "State",
                                        "localField": "stateId",
                                        "foreignField": "_id",
                                        "as": "state"
                                    }
                                },

                                // Stage 5

                                {
                                    $unwind: "$state"
                                },

                                // Stage 6
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "districtId",
                                        "foreignField": "_id",
                                        "as": "dis"
                                    }
                                },

                                // Stage 5

                                {
                                    $unwind: "$dis"
                                },

                                {
                                    $project: {
                                        userId: "$userDetails.userId",
                                        Area: "$areaDetails.areaName",
                                        state: "$state.stateName",
                                        district: "$dis.districtName",
                                        user: "$userDetails.name",
                                    }
                                },
                                function (err, res) {
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getStationData",
                                        });
                                    }
                                    if (err) {
                                        sendMail.sendMail({
                                            collectionName: "Providers",
                                            errorObject: err,
                                            paramsObject: params,
                                            methodName: "Providers.getStationData",
                                        });
                                    }
                                    cb(false, res);
                                }

                            );

                        }
                    }, function (err, providerCount) {
                        if (err) {
                            sendMail.sendMail({
                                collectionName: "Providers",
                                errorObject: err,
                                paramsObject: params,
                                methodName: "Providers.getStationData",
                            });
                        }
                        const finalData = [];
                        if (providerCount.areaCount.length > 0) {



                            for (let i = 0; i < asyncResult.userResult[0].obj.length; i++) {
                                let filterObj = providerCount.areaCount.filter(function (obj) {


                                    return obj.userId.toString() == asyncResult.userResult[0].obj[i].userId.toString()
                                });
                                if (filterObj.length > 0) {


                                    // get master data


                                    let filteProviderObj = providerCount.ProvidersCount.filter(function (obj) {
                                        return obj.userId.toString() == filterObj[0].userId.toString()
                                    });
                                    if (filteProviderObj.length > 0) {


                                        for (let i = 0; i < filteProviderObj.length; i++) {

                                            finalData.push({
                                                state: filteProviderObj[i].state,
                                                district: filteProviderObj[i].district,
                                                user: filteProviderObj[i].user,
                                                Area: filteProviderObj[i].Area,

                                                totalDrug: filteProviderObj[i].totalDrug,
                                                totalStockist: filteProviderObj[i].totalStockist,
                                                totalRMP: filteProviderObj[i].totalRMP

                                            });
                                        }
                                        // provider found

                                    } else {
                                        // only area found
                                        finalData.push({
                                            state: filterObj.state,
                                            district: filterObj.district,
                                            user: filterObj.user,
                                            Area: filterObj.Area,
                                            totalDrug: 0,
                                            totalStockist: 0,
                                            totalRMP: 0,

                                        });
                                    }
                                } else {
                                    // for those whose data is not submitted yet.
                                    finalData.push({
                                        user: asyncResult.userResult[0].obj[i].name,
                                        state: asyncResult.userResult[0].obj[i].stateName,
                                        district: asyncResult.userResult[0].obj[i].districtName,
                                        Area: asyncResult.userResult[0].obj[i].Area,
                                        totalDrug: 0,
                                        totalStockist: 0,
                                        totalRMP: 0,
                                        totalArea: 0
                                    });
                                }
                            }
                            return cb(false, finalData);

                        } else {
                            return cb(false, []);
                        }
                    })
                } else {
                    return cb(false, [])
                }

            })




        }
        Providers.remoteMethod('getStationData', {
            description: 'Get Total Master Data',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        });
        //------------end ------------------



        //--------------------API for Birthdays And Anniversaries-----------------//
        Providers.getBirthdaysAndAnniversariesFromProviders = function (param) {
            let matchObj = {
                status: true,
                companyId: ObjectId(param.companyId)
            }
            if (param.userId) {
                matchObj['userId'] = param.userId;
            }

            return new Promise((resolve, reject) => {
                try {
                    var self = this;
                    var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
                    ProviderCollection.aggregate({
                            "$match": matchObj
                        }, {
                            "$lookup": {
                                "from": "State",
                                "localField": "stateId",
                                "foreignField": "_id",
                                "as": "stateInfo"
                            }
                        }, {
                            "$unwind": {
                                "path": "$stateInfo",
                                "preserveNullAndEmptyArrays": true
                            }
                        }, {
                            "$match": {
                                "dob": {
                                    $exists: true
                                },
                                "doa": {
                                    $exists: true
                                }
                            }
                        }, {
                            "$project": {
                                "name": "$providerName",
                                "type": "$providerType",
                                "doa": 1.0,
                                "dob": 1.0,
                                "state": "$stateInfo.stateName",
                                "DOBmonth": {
                                    "$month": "$dob"
                                },
                                "DOBdate": {
                                    "$dayOfMonth": "$dob"
                                },
                                "DOAmonth": {
                                    "$month": "$doa"
                                },
                                "DOAdate": {
                                    "$dayOfMonth": "$doa"
                                }
                            }
                        }, {
                            "$match": {
                                "$or": [{
                                        "$and": [{
                                                "DOBmonth": param.month
                                            },
                                            {
                                                "DOBdate": param.date
                                            }
                                        ]
                                    },
                                    {
                                        "$and": [{
                                                "DOAmonth": param.month
                                            },
                                            {
                                                "DOAdate": param.date
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        function (err, providerRes) {
                            if (err) reject(err);
                            return resolve(providerRes);
                        }
                    );
                } catch (err) {
                    reject(err);
                }
            })

        };

        Providers.remoteMethod(
            'getBirthdaysAndAnniversariesFromProviders', {
                description: 'This method is used to get the Birthdays And Anniversaries from Providers',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )



        //------------------untagged providers by preeti arora 3-11-2020-------------------
        Providers.unTaggedProvider = function (param, cb) {
            console.log("param :", param);
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            let findObj = {
                where: {
                    companyId: param.companyId,
                    id: param.id
                }
            }
            Providers.find(findObj, function (err, res) {
                if (err) {
                    console.log(err);
                }
                if (res.length > 0) {
                    if (res[0].geoLocation) {
                        if (res[0].geoLocation != null) {
                            res[0].geoLocation.forEach((item, index) => {

                                if (item.lat == param.workingAddress.latLong.lat && item.long == param.workingAddress.latLong.long.toString()) {
                                    res[0].geoLocation.splice(index);
                                    let where = {
                                        companyId: ObjectId(param.companyId),
                                        _id: ObjectId(param.id)
                                    };
                                    let update = {
                                        geoLocation: res[0].geoLocation
                                    }
                                    ProviderCollection.update(where, {
                                        $set: update
                                    }, function (err, res) {
                                        if (err) return cb(false);
                                        return cb(false, "updated")
                                    })

                                }

                            })
                        }
                    } else {
                        return cb(false, [])
                    }

                }

            })


        };

        Providers.remoteMethod(
            'unTaggedProvider', {
                description: 'unTagged Provider',
                accepts: [{
                    arg: 'param',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        )
        //===============================end================================================
        //------------------UnTagged Providers by Preeti Arora 18-11-2020-------------------
        // Providers.NotificationCount = function (param, cb) {
        //     var self = this;
        //     var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
        //     var inboxCollection = self.getDataSource().connector.collection(Providers.app.models.Inbox.modelName);
        //     var userCollection = self.getDataSource().connector.collection(Providers.app.models.UserLogin.modelName);
        //     let date = moment().format("YYYY-MM-DD");
        //     let month = moment().format("MM");
        //     let day = moment().format("DD");
        //     let year = moment().format("YYYY");

        //     const matchForprovider = {
        //         companyId: ObjectId(param.companyId),
        //         userId: ObjectId(param.userId),
        //         appStatus: "approved",
        //         status: true,
        //         dob: {
        //             $ne: null
        //         },
        //         doa: {
        //             $ne: null
        //         },
        //     }
        //     const matchForInbox = {
        //         companyId: ObjectId(param.companyId),
        //         from: ObjectId(param.userId),
        //         readStatus: null
        //     }
        //     const matchForUser = {
        //         companyId: ObjectId(param.companyId),
        //         userId: ObjectId(param.userId),
        //         dateOfAnniversary: {
        //             $ne: null
        //         },
        //         dateOfBirth: {
        //             $ne: null
        //         },
        //     }

        //     async.parallel({
        //         totalDOAAndDOB: (cb) => {
        //             ProviderCollection.aggregate(
        //                 // Stage 1
        //                 {
        //                     $match: matchForprovider
        //                 }, {
        //                     $project: {
        //                         monthDOA: {
        //                             $month: "$doa"
        //                         },
        //                         dateDOA: {
        //                             $dayOfMonth: "$doa"
        //                         },
        //                         yearDOA: {
        //                             $year: "$doa"
        //                         },
        //                         monthDOB: {
        //                             $month: "$dob"
        //                         },
        //                         dateDOB: {
        //                             $dayOfMonth: "$dob"
        //                         },
        //                         yearDOB: {
        //                             $year: "$dob"
        //                         },
        //                         providerName: 1,
        //                         userId: 1,
        //                         doa: 1,
        //                         dob: 1
        //                     }
        //                 }, {
        //                     $match: {
        //                         "monthDOA": parseInt(month),
        //                         "yearDOA": parseInt(year),
        //                         "monthDOB": parseInt(month),
        //                         "yearDOB": parseInt(year),
        //                     }
        //                 },
        //                 // Stage 2
        //                 {
        //                     $group: {
        //                         _id: {
        //                             userId: "$userId"
        //                         },
        //                         totaldoa: {
        //                             $addToSet: "$doa"
        //                         },
        //                         totaldob: {
        //                             $addToSet: "$dob"
        //                         },
        //                         month: {
        //                             $push: "$monthDOB"
        //                         },
        //                         year: {
        //                             $push: "$yearDOB"
        //                         },
        //                         dateDOA: {
        //                             $push: "$dateDOA"
        //                         },
        //                         dateDOB: {
        //                             $push: "$dateDOB"
        //                         }
        //                     }
        //                 },
        //                 // Stage 3
        //                 {
        //                     $project: {
        //                         _id: 0,
        //                         userId: "$_id.userId",
        //                         totalDoa: {
        //                             $size: "$totaldoa"
        //                         },
        //                         totalDob: {
        //                             $size: "$totaldob"
        //                         },
        //                         month: {
        //                             $arrayElemAt: ["$month", 0]
        //                         },
        //                         year: {
        //                             $arrayElemAt: ["$year", 0]
        //                         },
        //                         dateDOA: {
        //                             $arrayElemAt: ["$dateDOA", 0]
        //                         },
        //                         dateDOB: {
        //                             $arrayElemAt: ["$dateDOB", 0]
        //                         },
        //                     }
        //                 },
        //                 function (err, res) {
        //                     if (err) cb(err)
        //                     if (res.length > 0) {
        //                         cb(null, res)
        //                     }else{
        //                         cb(null, res)
        //                     }
        //                 }
        //             );

        //         },
        //         totalMailsCount: (cb) => {
        //             inboxCollection.aggregate(
        //                 // Stage 1
        //                 {
        //                     $match: matchForInbox
        //                 },
        //                 // Stage 2
        //                 {
        //                     $group: {
        //                         _id: {
        //                             from: "$from"
        //                         },
        //                         total: {
        //                             $push: "$from"
        //                         }
        //                     }
        //                 },
        //                 // Stage 3
        //                 {
        //                     $project: {
        //                         userId: "$_id.from",
        //                         totalMails: {
        //                             $size: "$total"
        //                         }
        //                     }
        //                 },
        //                 function (err, res) {
        //                     if (err) {
        //                         cb(err)
        //                     } else {
        //                         cb(null, res)
        //                     }
        //                 }
        //             );

        //         },
        //         totalUserCount: (cb) => {
        //             userCollection.aggregate(
        //                 // Stage 1
        //                 {
        //                     $match: matchForUser
        //                 },
        //                 // Stage 2
        //                 {
        //                     $project: {
        //                         monthDOA: {
        //                             $month: "$dateOfAnniversary"
        //                         },
        //                         dateDOA: {
        //                             $dayOfMonth: "$dateOfAnniversary"
        //                         },
        //                         yearDOA: {
        //                             $year: "$dateOfAnniversary"
        //                         },
        //                         monthDOB: {
        //                             $month: "$dateOfBirth"
        //                         },
        //                         dateDOB: {
        //                             $dayOfMonth: "$dateOfBirth"
        //                         },
        //                         yearDOB: {
        //                             $year: "$dateOfBirth"
        //                         },
        //                         _id: 1,
        //                         dateOfAnniversary: 1,
        //                         dateOfBirth: 1
        //                     }
        //                 },
        //                 // Stage 3
        //                 {
        //                     $match: {
        //                         "monthDOA": month,
        //                         "yearDOA": year,
        //                         "monthDOB": month,
        //                         "yearDOB": year,
        //                     }
        //                 },
        //                 // Stage 4
        //                 {
        //                     $group: {
        //                         _id: {
        //                             userId: "$_id"
        //                         },
        //                         month: {
        //                             $push: "$monthDOB"
        //                         },
        //                         year: {
        //                             $push: "$yearDOB"
        //                         },
        //                         dateDOA: {
        //                             $push: "$dateDOA"
        //                         },
        //                         dateDOB: {
        //                             $push: "$dateDOB"
        //                         }
        //                     }
        //                 },
        //                 // Stage 5
        //                 {
        //                     $project: {
        //                         _id: 0,
        //                         userId: "$_id.userId",
        //                         month: {
        //                             $arrayElemAt: ["$month", 0]
        //                         },
        //                         year: {
        //                             $arrayElemAt: ["$year", 0]
        //                         },
        //                         dateDOA: {
        //                             $arrayElemAt: ["$dateDOA", 0]
        //                         },
        //                         dateDOB: {
        //                             $arrayElemAt: ["$dateDOB", 0]
        //                         },
        //                     }
        //                 },
        //                 function (err, res) {
        //                     if (err) {
        //                         cb(err)
        //                     } else {
        //                         cb(null, res)
        //                     }

        //                 }
        //             );
        //         }
        //     }, function (err, response) {

        //         console.log("response, err=>",response,"--",err);
        //         if (err) {
        //             return cb(err)
        //         }
        //         var count = 0;
        //         if (response.hasOwnProperty("totalMailsCount") && response.totalMailsCount.length>0 ) {
        //             count += response.totalMailsCount[0].totalMails

        //         }
        //         if (response.hasOwnProperty("totalUserCount") && response.totalUserCount.length>0) {
        //             response.totalUserCount.forEach((item, index) => {
        //                 if (item.dateDOB == parseInt(day)) {
        //                     count += 1
        //                 }
        //                 if (item.dateDOA == parseInt(day)) {
        //                     count += 1
        //                 }

        //             })
        //         }
        //         if (response.hasOwnProperty("totalDOAAndDOB") && response.totalDOAAndDOB.length>0) {
        //             response.totalDOAAndDOB.forEach((item, index) => {
        //                 if (item.dateDOB == parseInt(day)) {
        //                     count += 1
        //                 }
        //                 if (item.dateDOA == parseInt(day)) {
        //                     count += 1
        //                 }

        //             })
        //         }
        //         console.log("count==>", count);
        //         return cb(false, count)
        //     })

        // };
        Providers.NotificationCount = function (param, cb) {
            var self = this;
            var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
            var inboxCollection = self.getDataSource().connector.collection(Providers.app.models.Inbox.modelName);
            var userCollection = self.getDataSource().connector.collection(Providers.app.models.UserLogin.modelName);
            let date = moment().format("YYYY-MM-DD");
            let month = moment().format("MM");
            let day = moment().format("DD");
            let year = moment().format("YYYY");
            const matchForprovider = {
                companyId: ObjectId(param.companyId),
                appStatus: "approved",
                status: true,
                dob: {
                    $type: "date"
                },
                doa: {
                    $type: "date"
                }
            }
            const matchForInbox = {
                companyId: ObjectId(param.companyId),
                $or: [{
                    readStatus: false
                }, ]
            }
            const matchForUser = {
                companyId: ObjectId(param.companyId),
                status: true,
                dateOfAnniversary: {
                    $type: "date"
                },
                dateOfBirth: {
                    $type: "date"
                }

            }
            if (param.rL > 0) {
                matchForprovider["userId"] = ObjectId(param.userId);
                matchForInbox["to"] = ObjectId(param.userId);
                matchForUser["_id"] = ObjectId(param.userId);
            }

            async.parallel({
                    totalDOAAndDOB: (cb) => {
                        ProviderCollection.aggregate(
                                // Stage 1
                                {
                                    $match: matchForprovider
                                }, {
                                    $lookup: {
                                        "from": "Area",
                                        "localField": "blockId",
                                        "foreignField": "_id",
                                        "as": "area"
                                    }
                                },
                                    {
                                        $project: {
                                            monthDOA: {
                                                $month: "$doa"
                                            },
                                            dateDOA: {
                                                $dayOfMonth: "$doa"
                                            },
                                            yearDOA: {
                                                $year: "$doa"
                                            },
                                            monthDOB: {
                                                $month: "$dob"
                                            },
                                            dateDOB: {
                                                $dayOfMonth: "$dob"
                                            },
                                            yearDOB: {
                                                $year: "$dob"
                                            },
                                            providerName: 1,
                                            providerType: 1,
                                            userId: 1,
                                            phone:1,
                                            doa: 1,
                                            dob: 1,
                                            area: {
                                                $arrayElemAt: ["$area.areaName", 0]
                                            }
                                        }
                                    },
                                    {
                                        $match: {
                                            $or: [{
                                                    monthDOB: parseInt(month)
                                                },
                                                {
                                                    monthDOA: parseInt(month)
                                                },
                                            ],
                                        }
                                    },
                                    function (err, res) {
                                        if (err) cb(err)
                                        if (res.length > 0) {
                                            cb(null, res)
                                        } else {
                                            cb(null, res)
                                        }
                                    }


                                );


                            },
                            totalMailsCount: (cb) => {
                                inboxCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: matchForInbox
                                    }, {
                                        $project: {
                                            userId: "$to",
                                            subject: 1,
                                            mesage: 1,
                                            mailDate: 1,
                                        }
                                    },
                                    function (err, res) {
                                        if (err) {
                                            cb(err)
                                        } else {
                                            cb(null, res)
                                        }
                                    }
                                );
                            },
                            totalUserCount: (cb) => {

                                userCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: matchForUser
                                    },
                                    // Stage 2
                                    {
                                        $lookup: {
                                            "from": "UserInfo",
                                            "localField": "_id",
                                            "foreignField": "userId",
                                            "as": "userData"
                                        }
                                    }, {
                                        $unwind: "$userData"
                                    }, {
                                        $lookup: {
                                            "from": "State",
                                            "localField": "userData.stateId",
                                            "foreignField": "_id",
                                            "as": "state"
                                        }
                                    },{
                                        $lookup: {
                                            "from": "District",
                                            "localField": "userData.districtId",
                                            "foreignField": "_id",
                                            "as": "dis"
                                        }
                                    }, {
                                        $project: {
                                            monthDOA: {
                                                $month: "$dateOfAnniversary"
                                            },
                                            dateDOA: {
                                                $dayOfMonth: "$dateOfAnniversary"
                                            },
                                            yearDOA: {
                                                $year: "$dateOfAnniversary"
                                            },
                                            monthDOB: {
                                                $month: "$dateOfBirth"
                                            },
                                            dateDOB: {
                                                $dayOfMonth: "$dateOfBirth"
                                            },
                                            yearDOB: {
                                                $year: "$dateOfBirth"
                                            },
                                            _id: 1,
                                            name: 1,
                                            mobile:1,
                                            dateOfAnniversary: 1,
                                            dateOfBirth: 1,
                                            state: {
                                                $arrayElemAt: ["$state.stateName", 0]
                                            },
                                            dis: {
                                                $arrayElemAt: ["$dis.districtName", 0]
                                            }
                                        }
                                    },

                                    // Stage 3
                                    {
                                        $match: {
                                            $or: [{
                                                    monthDOB: parseInt(month)
                                                },
                                                {
                                                    monthDOA: parseInt(month)
                                                },
                                            ],
                                        }
                                    },
                                    function (err, res) {
                                        if (err) {
                                            cb(err)
                                        } else {
                                            cb(null, res)
                                        }

                                    }


                                );

                            }

                    },
                    function (err, response) {
                        if (err) {
                            return cb(err)
                        }
                        var count = 0;
                        const data = [];
                        const ReturnRes = {};

                        if (response.hasOwnProperty("totalMailsCount") && response.totalMailsCount.length > 0) {
                            count += response.totalMailsCount.length
                            response.totalMailsCount.forEach(element => {
                                var obj = {};
                                obj["title"] = element.subject;
                                obj["type"] = "Mail";
                                obj["description"] = "";
                                data.push(obj)
                            });

                        }
                        if (response.hasOwnProperty("totalUserCount") && response.totalUserCount.length > 0) {
                            response.totalUserCount.forEach((item, index) => {
                            
                                if (item.dateDOB == parseInt(day) && item.monthDOB == parseInt(month)) {
                                    var obj = {};
                                    count += 1
                                    obj["title"] = item.name;
                                    obj["state"] = item.state;
                                    obj["hq"] = item.dis;
                                    obj["mobile"] = item.mobile;
                                    obj["type"] = "User";
                                    obj["description"] = "Happy Birthday";
                                    data.push(obj)
                                }
                                if (item.dateDOA == parseInt(day) && item.monthDOB == parseInt(month)) {
                                    var obj = {};

                                    count += 1
                                    obj["title"] = item.name;
                                    obj["type"] = "User";
                                    obj["state"] = item.state;
                                    obj["hq"] = item.dis;
                                    obj["mobile"] = item.mobile;
                                    obj["description"] = "Happy Anniversary";
                                    data.push(obj)
                                }

                            })

                        }
                        if (response.hasOwnProperty("totalDOAAndDOB") && response.totalDOAAndDOB.length > 0) {
                            response.totalDOAAndDOB.forEach((item, index) => {
                                if (item.dateDOB == parseInt(day)) {
                                    var obj = {};
                                    count += 1
                                    obj["title"] = item.providerName;
                                    obj["type"] = item.providerType;
                                    obj["area"] = item.area;
                                    obj["mobile"] = item.phone;
                                    obj["description"] = "Happy Birthday";
                                    data.push(obj)

                                }
                                if (item.dateDOA == parseInt(day)) {
                                    count += 1
                                    var obj = {};
                                    obj["title"] = item.providerName;
                                    obj["type"] = item.providerType;
                                    obj["area"] = item.area;
                                    obj["mobile"] = item.phone;
                                    obj["description"] = "Happy Anniversary";
                                    data.push(obj)

                                }
                            })

                        }
                        ReturnRes["count"] = count;
                        ReturnRes["data"] = data;

                        return cb(false, ReturnRes)
                    })



            };

            Providers.remoteMethod(
                'NotificationCount', {
                    description: 'Notification Count',
                    accepts: [{
                        arg: 'param',
                        type: 'object'
                    }],
                    returns: {
                        root: true,
                        type: 'array'
                    },
                    http: {
                        verb: 'get'
                    }
                }
            )
            //===============================end================================================

            Providers.unTaggedMultipleProviders = function (param, cb) {
                var self = this;
                var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);

                let providerIds = [];
                for (var i = 0; i < param.id.length; i++) {
                    providerIds.push(ObjectId(param.id[i].providerId))
                }

                let where = {
                    companyId: ObjectId(param.companyId),
                    _id: {
                        $in: providerIds
                    }
                };
                let update = {
                    geoLocation: []
                }
                ProviderCollection.update(where, {
                    $set: update
                }, function (err, res) {
                    console.log("res : ", res);
                    if (err) return cb(false);
                    return cb(false, "updated")
                })

            };

            Providers.remoteMethod(
                'unTaggedMultipleProviders', {
                    description: 'Multiple Providers unTagged',
                    accepts: [{
                        arg: 'param',
                        type: 'object'
                    }],
                    returns: {
                        root: true,
                        type: 'array'
                    },
                    http: {
                        verb: 'get'
                    }
                }
            )

            //-------------------Preeti Arora 26-01-2021-------------------
            
            Providers.getProvidersListWithPatchName = function (param, cb) {
                var self = this;
                var ProviderCollection = this.getDataSource().connector.collection(Providers.modelName);
                var UserAreaCollection = self.getDataSource().connector.collection(Providers.app.models.UserAreaMapping.modelName);
                
                UserAreaCollection.aggregate(
                    {
                      $match: {companyId:ObjectId(param.companyId),userId:ObjectId(param.userId[0]),status: true,
                        appStatus: "approved"}
                    },
                    {
                      $lookup: {
                        from: "UserInfo",
                        localField: "userId",
                        foreignField: "userId",
                        as: "users"
                      }
                    },
                    {
                      $unwind: "$users"
                    },
                    {
                      $match: {
                        "users.status": true
                      }
                    },
                    {
                      $unwind: {
                        path: "$users.divisionId",
                        preserveNullAndEmptyArrays: true
                      }
                    },
                    {
                      $lookup: {
                        from: "DivisionMaster",
                        localField: "users.divisionId",
                        foreignField: "_id",
                        as: "division"
                      }
                    },
                    {
                      $lookup: {
                        from: "Providers",
                        localField: "areaId",
                        foreignField: "blockId",
                        as: "provider"
                      }
                    },
                    {
                      $lookup: {
                        from: "State",
                        localField: "stateId",
                        foreignField: "_id",
                        as: "state"
                      }
                    },
                    {
                      $lookup: {
                        from: "Area",
                        localField: "areaId",
                        foreignField: "_id",
                        as: "area"
                      }
                    },
                    {
                      $lookup: {
                        from: "District",
                        localField: "districtId",
                        foreignField: "_id",
                        as: "district"
                      }
                    },  {
                        $lookup: {
                          from: "PatchMaster",
                          localField: "userId",
                          foreignField: "userId",
                          as: "patch"
                        }
                      },
                    {
                      $unwind: {
                        path: "$provider",
                        preserveNullAndEmptyArrays: false
                      }
                    },
                    {
                      $project: {
                          _id:1,
                          providerId:"$provider._id",
                        stateId: 1,
                        districtId: 1,
                        areaId: 1,
                        areaStatus: 1,
                        stateName: {
                          $arrayElemAt: ["$state.stateName", 0]
                        },
                        districtName: {
                          $arrayElemAt: ["$district.districtName", 0]
                        },
                        name: "$users.name",
                        divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                        providerType: "$provider.providerType",
                        providerName: "$provider.providerName",
                        providerId: "$provider._id",
                        socialMedia:"$provider.socialMediaTypes",
                        providerCode: {
                          $cond: [
                            {
                              $eq: ["$provider.providerCode", "null"]
                            },
                            "",
                            "$provider.providerCode"
                          ]
                        },
                        degree: "$provider.degree",
                        specialization: "$provider.specialization",
                        category: "$provider.category",
                        address: "$provider.address",
                        city: "$provider.city",
                        phone: "$provider.phone",
                        email: "$provider.email",
                        patchDetails:"$patch",
                        frequencyVisit: "$provider.frequencyVisit",
                        dob: {
                          $cond: [
                            {
                              $eq: ["$provider.dob", "1900-01-01"]
                            },
                            "",
                            "$provider.dob"
                          ]
                        },
                        doa: {
                          $cond: [
                            {
                              $eq: ["$provider.doa", "1900-01-01"]
                            },
                            "",
                            "$provider.doa"
                          ]
                        },
                        providerStatus: "$provider.status",
                        providerAppStatus: "$provider.appStatus",
                        providerDelStatus: "$provider.delStatus",
                        areaName: {
                          $arrayElemAt: ["$area.areaName", 0]
                        }
                    }
                    },
                    {
                      $sort: {
                        divisionName: 1,
                        providerType: 1,
                        stateName: 1,
                        districtName: 1,
                        name: 1,
                        areaName: 1,
                        providerName: 1
                      }
                    },
                    {
                      cursor: {
                        batchSize: 50
                      },
                      allowDiskUse: true
                    },
                    function(err, result) {
        
                      let finalResult = [];
                      if (result.length == 0) {
                        return cb(false, []);
                      } else {
                          result.forEach((element,i,arr) => {
                            let AsignePatch=[];
        
                              if(element.hasOwnProperty("patchDetails")){
        
                                element.patchDetails.forEach(patch => {
                                    let ProviderObj = patch.providerId.filter(function (obj) {
                                        return obj.toString() == element.providerId.toString()
                                    });
                                    if(ProviderObj.length>0){
                                        AsignePatch.push(patch.patchName)
                                    }
                                });
                                element.patch=AsignePatch
                                
                              }
                              
                          });
                          
                          return cb(false,result)
                      }
                      
                    }
                  );
            }
            Providers.remoteMethod('getProvidersListWithPatchName', {
                description: 'get Providers count based on block ids',
                accepts: [{
                    arg: 'param',
                    type: 'object',
                    http: { source: 'body' }
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'post'
                }
            });
            //-----------------------

// ERP MASTER APIs Work Start By Ravi on 04/28/2021

    Providers.create_or_update = function (params, cb) {
        if (params) {
            if (params.hasOwnProperty('companyCode') && params.hasOwnProperty('stateCode') && params.hasOwnProperty('hqCode') && params.hasOwnProperty('partyCode') && params.hasOwnProperty('partyName')) {
                Providers.app.models.CompanyMaster.findOne({ "where": { erpCode: parseInt(params.companyCode) } }, function (err1, companyId) {
                    if (err1) { return cb(false, status.getStatusMessage(2)) }
                    Providers.app.models.State.findOne({ "where": { erpCode: parseInt(params.stateCode) } }, function (err2, stateId) {
                        if (err2) { return cb(false, status.getStatusMessage(2)) }
                        Providers.app.models.District.findOne({ "where": { erpCode: parseInt(params.hqCode) } }, function (err3, districtId) {
                            if (err3) { return cb(false, status.getStatusMessage(2)) }
                            if (companyId && stateId && districtId) {
                                Providers.findOne({ where: { erpCode: parseInt(params['partyCode']) } }, function (err4, isPartyExits) {
                                    if (err4) { return cb(false, status.getStatusMessage(2)) }
                                    if (isPartyExits) {
                                        isPartyExits.stateId = ObjectId(isPartyExits.stateId);
                                        isPartyExits.districtId = ObjectId(isPartyExits.districtId);
                                        isPartyExits.companyId = ObjectId(isPartyExits.companyId);
                                        isPartyExits.providerName = params.partyName;
                                        isPartyExits.category = params.hasOwnProperty('category') ? params.category : isProductExits.category;
                                        isPartyExits.mobileNum = params.hasOwnProperty('mobileNum') ? params.mobileNum : isProductExits.mobileNum;
                                        isPartyExits.email = params.hasOwnProperty('email') ? params.email : isProductExits.email;
                                        isPartyExits.address = params.hasOwnProperty('address') ? params.address : isProductExits.address;
                                        isPartyExits.contactPerson = params.hasOwnProperty('contactPerson') ? params.contactPerson : isProductExits.contactPerson;
                                        Providers.replaceOrCreate(isPartyExits, function (err, replace) {
                                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                            if (replace) {
                                                console.log();
                                                return cb(false, status.getStatusMessage(9))
                                            }
                                        })
                                    } else {
                                        const providerObj = {
                                            "stateId": ObjectId(stateId.id),
                                            "districtId": ObjectId(districtId.id),
                                            "blockId": ObjectId(districtId.id),
                                            "providerType": "Stockist",
                                            "providerName": params.partyName,
                                            "erpCode": params.partyCode,
                                            "status": true,
                                            "providerCode": "",
                                            "category": params.hasOwnProperty('category') ? params.category : "Dealer",
                                            "mobileNum": params.hasOwnProperty('mobileNum') ? params.mobileNum : "9999999999",
                                            "addressType": "",
                                            "email": params.hasOwnProperty('email') ? params.email : "NA",
                                            "city": "",
                                            "blockName": "",
                                            "submitBy": "",
                                            "address": params.hasOwnProperty('address') ? params.address : "NA",
                                            "contactPerson": params.hasOwnProperty('contactPerson') ? params.contactPerson : "NA",
                                            "status": true,
                                            "createdAt": new Date(),
                                            "updatedAt": new Date(),
                                            "companyId": ObjectId(companyId.id),
                                            "oldInfo": {},
                                            "businessPotential": 0,
                                            "appStatus": "",
                                            "mgrAppDate": new Date(),
                                            "finalAppDate": new Date(),
                                            "appByMgr": "",
                                            "finalAppBy": "",
                                            "delStatus": "",
                                            "mgrDelDate": new Date(),
                                            "finalDelDate": new Date(),
                                            "delByMgr": "",
                                            "finalDelBy": "",
                                            "specialization": "",
                                            "userId": ObjectId(districtId.id),
                                            "onLocation": false,
                                            "crmStatus": false,
                                            "geoLocation": [],
                                            "focusProduct": []
                                        }
                                        Providers.replaceOrCreate(providerObj, function (err, create) {
                                            if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                                            if (create) {
                                                return cb(false, status.getStatusMessage(8))
                                            }
                                        })
                                    }
                                })
                            } else {
                                return cb(false, status.getStatusMessage(7));
                            }
                        });
                    });
                });
            } else {
                return cb(false, status.getStatusMessage(1));
            }
        } else {
            return cb(false, status.getStatusMessage(0))
        }
    }

    Providers.remoteMethod(
        'create_or_update', {
        description: 'Add and Update Stockist Master',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post'
        }
    });

    //END

        };