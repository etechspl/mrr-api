"use strict";
var ObjectId = require("mongodb").ObjectID;
module.exports = function (Crmsponsorship) {
  Crmsponsorship.getCrmRequestUserwise = function (params, cb) {
    let self = this;
    let CrmSpomsorshipAggregate = self
      .getDataSource()
      .connector.collection(Crmsponsorship.modelName);
    var userIdArr = [];
    params.userIds.forEach((element) => {
      userIdArr.push(ObjectId(element));
    });
    let match={
      companyId: ObjectId(params.companyId),
       userId: {
         $in: userIdArr,
       },
     }
     if(params.appAndDisAppBy=="manager"){
        match["approvalStatusByMgr"]="pending"
     }
     
     
    CrmSpomsorshipAggregate.aggregate(
      {
        $match:match
      },
      {
        $lookup: {
          from: "UserInfo",
          localField: "userId",
          foreignField: "userId",
          as: "UserData",
        },
      },
      {
        $unwind: "$UserData",
      },
      {
        $lookup: {
          from: "District",
          localField: "UserData.districtId",
          foreignField: "_id",
          as: "DistrictData",
        },
      },
      {
        $unwind: { path: "$DistrictData", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          userId: 1.0,
          name: "$UserData.name",
          district: "$DistrictData.districtName",
        },
      },
      {
        $group: {
          _id: {
            userId: "$userId",
            name: "$name",
            district: "$district",
          },
          requestCount: {
            $sum: 1.0,
          },
        },
      },
      function (err, result) {
        if (err) {
          cb(false, err);
        }
        cb(false, result);
      }
    );
  };

  Crmsponsorship.remoteMethod("getCrmRequestUserwise", {
    description: "getCrmRequestUserwise",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });

  Crmsponsorship.getApproveAndDisapproveCrmRequest = function (params, cb) {
    let self = this;
    let CrmSpomsorshipAggregate = self
      .getDataSource()
      .connector.collection(Crmsponsorship.modelName);

    if (
      params.appAndDisAppBy == "manager" ||
      params.appAndDisAppBy == "adminDeny"
    ) {
      let requestIdArr = [];
      params.requestIds.forEach((element) => {
        requestIdArr.push(ObjectId(element));
      });
      let where = { _id: { $in: requestIdArr } };
      let setData = params.setData;
      CrmSpomsorshipAggregate.updateMany(
        where,
        {
          $set: setData,
        },
        function (err, CrmRequestResult) {
          if (err) {
            console.log(err);
          }
          cb(false, CrmRequestResult.result);
        }
      );
    } else if (params.appAndDisAppBy == "admin") {
      let setData = params.setData;

      params.data.forEach((item, index, arr) => {
        const where = {};
        where._id = ObjectId(item.id);
        setData.remarksAdmin = item.remarks;
        setData.investmentAmount = item.investmentAmount;
        setData.paidTo = item.paidTo;
        setData.billNum = item.billNum;
        setData.kindOfCRM = item.kindOfCRM;
        setData.modeOfPayment = item.modeOfPayment;

        CrmSpomsorshipAggregate.update(
          where,
          {
            $set: setData,
          },
          function (err, CrmRequestResult) {
            if (err) {
              console.log(err);
            }
            if (Object.is(arr.length - 1, index)) {
              let result = {};
              result.response = `${index + 1} items updated successfully !!`;
              cb(false, result);
            }
          }
        );
      });
    }
  };

  Crmsponsorship.remoteMethod("getApproveAndDisapproveCrmRequest", {
    description: "getApproveAndDisapproveCrmRequest",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
};