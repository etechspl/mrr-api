var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
var asyncLoop = require('node-async-loop');
var status = require('../../utils/statusMessage');
'use strict';

module.exports = function (District) {
  District.getDistrictToBeMapped = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(District.modelName);
    DistrictCollection.aggregate({
      $match: {
        stateId: ObjectId(params.stateId),
        assignedTo: ObjectId(params.companyId),
        status: true
      }
    }, {
      $group: {
        _id: null,
        districtId: {
          $addToSet: "$_id"
        }
      }
    }, function (err, result) {
      if (err) {
        sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'getDistrictToBeMapped' });
      }
      DistrictCollection.aggregate({
        $match: {
          "stateId": ObjectId(params.stateId),
          _id: {
            //$nin: result[0].districtId
            $nin: (result.length > 0) ? result[0].districtId : []
          }
        }
      }, {
        $lookup: {
          "from": "State",
          "localField": "stateId",
          "foreignField": "_id",
          "as": "states"
        }
      }, {
        $project: {
          stateId: {
            $arrayElemAt: ["$states._id", 0]
          },
          stateName: {
            $arrayElemAt: ["$states.stateName", 0]
          },
          id: "$_id",
          _id: 0,
          districtName: "$districtName"

        }
      }, {
        $sort: {
          districtName: 1
        }
      }, function (err, response) {
        if (err) {
          sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'getDistrictToBeMapped' });
        }
        if (response.length > 0) {
          return cb(false, response);
        } else {
          return cb(false, []);
        }
      })
    })
  }
  District.remoteMethod(
    'getDistrictToBeMapped', {
    description: 'Getting District List which are not mapped to company',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  )

  District.mapDistricts = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(
      District.modelName
    );
    var districtIds = [];
    for (let district of params.selectedDistricts) {
      districtIds.push(ObjectId(district.id));
    }
    this.find(
      {
        where: {
          _id: {
            inq: districtIds,
          },
        },
      },
      function (err, response) {
        if (err) {
          sendMail.sendMail({
            collectionName: "District",
            errorObject: err,
            paramsObject: params,
            methodName: "mapDistricts",
          });
        }
        asyncLoop(
          response,
          function (item, next) {
            var companyId = ObjectId(params.companyId);
            if (item.assignedTo) {
              if (item.assignedTo.indexOf(companyId) === -1) {
                item.assignedTo.push(companyId);
                District.updateAll(
                  {
                    _id: item.id,
                  },
                  {
                    assignedTo: item.assignedTo,
                    status: true,
                  },
                  function (err, result) {
                    if (err) {
                      sendMail.sendMail({
                        collectionName: "District",
                        errorObject: err,
                        paramsObject: params,
                        methodName: "mapDistricts",
                      });
                    }
                    next();
                  }
                );
              }
            } else {
              var assignedTo = [];
              assignedTo.push(companyId);
              District.updateAll(
                {
                  _id: item.id,
                },
                {
                  assignedTo: assignedTo,
                  status: true,
                },
                function (err, result) {
                  if (err) {
                    sendMail.sendMail({
                      collectionName: "District",
                      errorObject: err,
                      paramsObject: params,
                      methodName: "mapDistricts",
                    });
                  }
                  next();
                }
              );
            }
          },
          function (err) {
            if (err) {
              sendMail.sendMail({
                collectionName: "District",
                errorObject: err,
                paramsObject: params,
                methodName: "mapDistricts",
              });
            }
            // console.log("Updated Successfully");
            return cb(false, [
              {
                status: "Done",
              },
            ]);
          }
        );
      }
    );
  };
  District.remoteMethod("mapDistricts", {
    description: "Mapped the Districts to the Selected Company",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });
  District.removeMappedDistricts = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(
      District.modelName
    );
    var districtIds = [];
    for (let district of params.selectedDistricts) {
      districtIds.push(ObjectId(district.id));
    }
    this.find(
      {
        where: {
          _id: {
            inq: districtIds,
          },
        },
      },
      function (err, response) {
        if (err) {
          // sendMail.sendMail({collectionName: "District", errorObject: err, paramsObject: params, methodName: 'removeMappedDistricts'});
        }
        asyncLoop(
          response,
          function (item, next) {
            var companyId = ObjectId(params.companyId);
            if (item.assignedTo) {
              let arr = [];
              item.assignedTo.forEach((compId) => {
                if (compId.toString() !== companyId.toString()) {
                  arr.push(compId)
                }
              })

              District.updateAll({
                "_id": item.id
              }, {
                assignedTo: arr,
                status: true
              }, function (err, result) {
                if (err) {
                  sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'removeMappedDistricts' });
                }
                next();
              })
              //   }
            }
          },
          function (err) {
            if (err) {
              sendMail.sendMail({
                collectionName: "District",
                errorObject: err,
                paramsObject: params,
                methodName: "mapDistricts",
              });
            }
            return cb(false, [
              {
                status: "Done",
              },
            ]);
          }
        );
      }
    );
  };
  District.remoteMethod("removeMappedDistricts", {
    description: "Remove the Mapped Districts For the Selected Company",
    accepts: [
      {
        arg: "params",
        type: "object",
      },
    ],
    returns: {
      root: true,
      type: "array",
    },
    http: {
      verb: "get",
    },
  });

  District.createDistrict = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(District.modelName);

    District.find({
      where: {
        stateId: ObjectId(params.stateInfo),
        districtName: params.districtName.toUpperCase()
      }
    }, function (err, response) {
      if (err) {
        sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'createDistrict' });
        // console.log(err);
        return cb(err);
      }
      // console.log(response);
      if (response.length > 0) {
        var err = new Error('The request could not be completed due to a conflict with the current district name');
        err.statusCode = 409;
        err.code = 'Validation failed';
        return cb(err);
      }
      // console.log("Value not exits");
      var assignedTo = [];
      assignedTo.push(ObjectId(params.companyId));
      var obj = {
        stateId: ObjectId(params.stateInfo),
        districtName: params.districtName.toUpperCase(),
        status: true,
        assignedTo: assignedTo,
        createdAt: new Date(),
        updatedAt: new Date()
      }
      DistrictCollection.insertOne(obj, function (err, res) {
        if (err) {
          sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'createDistrict' });
          // console.log(err);
          return cb(err);
        }
        // console.log(res)
        return cb(false, res)
      })
    })

  }
  District.remoteMethod(
    'createDistrict', {
    description: 'Created Districts',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  }
  )

  //--------------------------------Praveen Kumar(07-12-2018)------------------------------------
  District.getDistrictList = function (parameters, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(District.modelName);
    var userAreaMappingCollection = this.getDataSource().connector.collection(District.app.models.UserAreaMapping.modelName);

    if (parameters.designationLevel == 0) {
      self.find({
        "where": {
          "assignedTo": parameters.companyId,
          "stateId": {
            "inq": parameters.stateIds,
          },
          "status": true
        },
        "order": "districtName ASC",
        "include": ["state"]
      }, function (err, assignedDistrictResult) {
        if (err) {
          sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: parameters, methodName: 'getDistrictList' });
          // console.log(err)
        }
        let passingResult = []

        for (let i = 0; i < assignedDistrictResult.length; i++) {
          let jsonAssignedDistrictResult = JSON.parse(JSON.stringify(assignedDistrictResult[i]));
          passingResult.push({
            stateId: jsonAssignedDistrictResult.state.id,
            stateName: jsonAssignedDistrictResult.state.stateName,
            id: jsonAssignedDistrictResult.id,
            districtName: jsonAssignedDistrictResult.districtName
          });
        }
        cb(false, passingResult);

      })
    } else if (parameters.designationLevel >= 2) {
      District.app.models.Hierarchy.find({
        where: {
          companyId: parameters.companyId,
          supervisorId: parameters.supervisorId,
          userDesignation: "MR",
          status: true
        }
      }, function (err, hrcyResult) {
        if (err) {
          sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: parameters, methodName: 'getDistrictList' });
          // console.log(err);
        }
        let passingUserIds = [];
        for (let i = 0; i < hrcyResult.length; i++) {
          passingUserIds.push(hrcyResult[i].userId);
        }

        let passingStateIds = [];
        for (let i = 0; i < parameters.stateIds.length; i++) {
          passingStateIds.push(ObjectId(parameters.stateIds[i]));
        }
        userAreaMappingCollection.aggregate({
          $match: {
            stateId: {
              $in: passingStateIds
            },
            userId: {
              $in: passingUserIds
            },
            status: true
          }
        }, {
          $group: {
            _id: {
              stateId: "$stateId",
              districtId: "$districtId"
            }
          }
        }, {
          $lookup: {
            "from": "State",
            "localField": "_id.stateId",
            "foreignField": "_id",
            "as": "stateDetails"
          }
        }, {
          $lookup: {
            "from": "District",
            "localField": "_id.districtId",
            "foreignField": "_id",
            "as": "districtDetails"
          }
        }, {
          $project: {
            _id: 0,
            stateId: {
              $arrayElemAt: ["$stateDetails._id", 0]
            },
            stateName: {
              $arrayElemAt: ["$stateDetails.stateName", 0]
            },
            id: {
              $arrayElemAt: ["$districtDetails._id", 0]
            },
            districtName: {
              $arrayElemAt: ["$districtDetails.districtName", 0]
            }

          }
        }, {
          $sort: {
            "stateName": 1,
            "districtName": 1
          }
        },
          function (err, result) {
            if (err) {
              sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: parameters, methodName: 'getDistrictList' });
            }
            cb(false, result);
          })


      });
    }
  };
  District.remoteMethod(
    'getDistrictList', {
    description: 'Getting District List',
    accepts: [{
      arg: 'parameters',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  );
  //------------------------------------END------------------------------------------
  District.getDistrict = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(District.modelName);
    const UserInfoCollection = this.getDataSource().connector.collection(District.app.models.UserInfo.modelName);

    let passingStateIds = [];
    for (let i = 0; i < params.stateId.length; i++) {
      passingStateIds.push(ObjectId(params.stateId[i]));
    }
    let dynamicMatch = {};
    if (params.isDivisionExist == true) {
      let divisionIds = [];
      for (let i = 0; i < params.division.length; i++) {
        divisionIds.push(ObjectId(params.division[i]));
      }
      dynamicMatch = {
        companyId: ObjectId(params.assignedTo),
        stateId: { $in: passingStateIds },
        divisionId: {
          $in: divisionIds
        }
      }
      UserInfoCollection.aggregate({
        $match: dynamicMatch
      }, {
        $group: {
          _id: {},
          districtId: {
            $addToSet: "$districtId"
          }
        }
      }, function (err, userInfoResult) {
        if (err) {
          sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'getDistrict' });
        }
        // console.log("Test : ", userInfoResult);
        if (userInfoResult.length > 0) {
          DistrictCollection.aggregate({
            $match: {
              _id: {
                $in: userInfoResult[0].districtId
              }
            }
          }, {
            $sort: {
              districtName: 1
            }
          }, {
            $group: {
              _id: {},
              districtObj: {
                $addToSet: {
                  id: "$_id",
                  districtName: "$districtName"
                }
              },
              districtIds: {
                $addToSet: "$_id"
              }
            }
          },
            function (err, result) {
              if (err) {
                sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'getDistrict' });
                return cb(false, err)
              }
              return cb(false, result)
            });
        } else {
          return cb(false, [])
        }
      });


    } else if (params.isDivisionExist == false || params.isDivisionExist == undefined) {
      dynamicMatch = {
        "assignedTo": ObjectId(params.assignedTo),
        "stateId": { $in: passingStateIds }
      }
      DistrictCollection.aggregate({
        $match: dynamicMatch
      }, {
        $sort: {
          districtName: -1
        }
      }, {
        $group: {
          _id: {},
          districtObj: {
            $addToSet: {
              id: "$_id",
              districtName: "$districtName",
              erpHqCode: "$erpHqCode",
            }
          },
          districtIds: {
            $addToSet: "$_id"
          }
        }
      },
        function (err, result) {
          if (err) {
            sendMail.sendMail({ collectionName: "District", errorObject: err, paramsObject: params, methodName: 'getDistrict' });
            return cb(false, err)
          }
          return cb(false, result)
        });
    }
  }
  District.remoteMethod(
    'getDistrict', {
    description: 'Getting district list',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'get'
    }
  }
  )
  //END

  // ERP MASTER APIs Work Start By Ravi on 04/27/2021

  District.getDistricts = function (params, cb) {
    var self = this;
    var DistrictCollection = this.getDataSource().connector.collection(District.modelName);
    const UserInfoCollection = this.getDataSource().connector.collection(District.app.models.UserInfo.modelName);

    let passingStateIds = [];
    for (let i = 0; i < params.stateId.length; i++) {
      passingStateIds.push(ObjectId(params.stateId[i]));
    }
    let dynamicMatch = {};
    if (params.isDivisionExist == false || params.isDivisionExist == undefined) {
      dynamicMatch = {
        "assignedTo": ObjectId(params.assignedTo),
        "stateId": { $in: passingStateIds },
        "erpHqCode": { $nin: [null,""] }
      }
      DistrictCollection.aggregate({
        $match: dynamicMatch
      }, {
        $sort: {
          districtName: 1
        }
      },
        function (err, result) {
          if (err) {
            return cb(false, err)
          }
          return cb(false, result)
        });
    }
  }
  District.remoteMethod(
    'getDistricts', {
    description: 'Getting district list',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'get'
    }
  }
  )



  District.create_or_update = function (params, cb) {
    if (params) {
      if (params.hasOwnProperty('stateCode') && params.hasOwnProperty('hqName') && params.hasOwnProperty('hqCode')) {
        District.app.models.State.findOne({ "where": { erpCode: params.stateCode } }, function (err, stateId) {
          if (err) { return cb(false, status.getStatusMessage(2)) }
          if (stateId) {
            District.findOne({ where: { erpCode: parseInt(params['hqCode']) } }, function (err, isHqExits) {
              if (err) { return cb(false, status.getStatusMessage(2)) }
              if (isHqExits) {
                isHqExits.stateId = ObjectId(stateId.id);
                isHqExits.districtName = params.hqName;
                District.replaceOrCreate(isHqExits, function (err, replace) {
                  if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                  if (replace) {
                    return cb(false, status.getStatusMessage(6))
                  }
                })
              } else {
                const hqObj = {
                  "stateId": ObjectId(stateId.id),
                  "districtName": params.hqName,
                  "erpCode": params.hqCode,
                  "assignedTo": ["erp"],
                  "status": true,
                  "assignedDivision": ["erp"],
                  "createdAt": new Date(),
                  "updatedAt": new Date()
                }
                District.replaceOrCreate(hqObj, function (err, create) {
                  if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                  if (create) {
                    return cb(false, status.getStatusMessage(5))
                  }
                })
              }
            })
          } else {
            return cb(false, status.getStatusMessage(7));
          }
        });
      } else {
        return cb(false, status.getStatusMessage(1));
      }
    } else {
      return cb(false, status.getStatusMessage(0))
    }
  }

  District.remoteMethod(
    'create_or_update', {
    description: 'Add and Update District Master',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  });

  //END
};