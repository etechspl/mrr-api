'use strict';
var sendMail = require('../../utils/sendMail');
module.exports = function(Forms) {
   
  Forms.getAssignedForms=function(companyId ,cb){
	  
	var self = this;
    var FormsCollection = self.getDataSource().connector.collection(Forms.modelName);  
	FormsCollection.aggregate(	
    // Stage 1
     {
      $unwind: "$company"
	 },
    // Stage 2
  
	 {
      $match: {
        "company.id":companyId,
	     	"company.enable":true
      }
    },
	// Stage 3
    {
      $project: {
        _id:1,
        formName:1,
        formType:1,
        displayOrder:1,
        isAdd:"$company.isAdd",
        formLabel:"$company.formLabel",
		    shortLabel:"$company.shortLabel"
      }
    },function(err, records){
		if (err) {
      sendMail.sendMail({collectionName: "Forms", errorObject: err, paramsObject: companyId, methodName: 'getAssignedForms'});
          return cb(err);
        }
        return cb(false, records);
	});

  }
  Forms.remoteMethod(
    'getAssignedForms', {
      description: 'get assigned form to selected company',
      accepts: [{
        arg: 'companyId',
        type: 'string'
      }],
      returns: { root: true, type: 'array' },
      http: { verb: 'get' }
    }
  );
};
