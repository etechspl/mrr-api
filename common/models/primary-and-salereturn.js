var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var asyncLoop = require('node-async-loop');
'use strict';

module.exports = function (Primaryandsalereturn) {
    Primaryandsalereturn.createPrimaryInfoWithValidationCheck = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        for (let i = 0; i < param.length; i++) {
            let obj1 = {};
            if (param[i].data[0].type === 'primarySale') {
                obj1 = {
                    "type": param[i].data[0].type,
                    "invoiceNo": param[i].data[0].invoiceNo,
                    "invoiceDate": new Date(param[i].data[0].invoiceDate),
                    "userId": param[i].data[0].userId,
                    "netRate": parseInt(param[i].data[0].netRate),
                    "splRate": parseInt(param[i].data[0].splRate),
                    "qty": parseInt(param[i].data[0].qty),
                    "amount": parseInt(param[i].data[0].amount),
                    "createdAt": new Date()
                }
            } else if (param[i].data[0].type === 'returnSale') {
                obj1 = {
                    "type": param[i].data[0].type,
                    "invoiceNo": param[i].data[0].invoiceNo,
                    "invoiceDate": new Date(param[i].data[0].invoiceDate),
                    "userId": param[i].data[0].userId,
                    "netRate": parseInt(param[i].data[0].netRate),
                    "splRate": parseInt(param[i].data[0].splRate),
                    "qty": parseInt(param[i].data[0].qty),
                    "breakage": parseInt(param[i].data[0].breakage),
                    "saleable": parseInt(param[i].data[0].saleable),
                    "other": parseInt(param[i].data[0].other),
                    "amount": parseInt(param[i].data[0].amount),
                    "createdAt": new Date()
                }
            }
            PrimaryInfoCollection.aggregate(
                // Stage 1
                {
                    $match: {
                        companyId: ObjectId(param[i].companyId),
                        productId: ObjectId(param[i].productId),
                        month: parseInt(param[i].month),
                        year: parseInt(param[i].year)
                    }
                },
                function (err, response) {
                    if (response.length > 0) {
                        var responses = [];
                        var primaryQty = 0;
                        var primaryAmt = 0;
                        var returnQty = 0;
                        var returnAmt = 0;
                        if (param[i].data[0].type === 'primarySale') {
                            primaryQty = primaryQty + param[i].data[0].qty;
                            primaryAmt = primaryAmt + param[i].data[0].amount;
                        } else if (param[i].data[0].type === 'returnSale') {
                            returnQty = returnQty + param[i].data[0].qty;
                            returnAmt = returnAmt + param[i].data[0].amount;
                        }
                        for (let j = 0; j < response[0].data.length; j++) {

                            responses.push(response[0].data[j]);
                            if (response[0].data[j].type === 'primarySale') {
                                primaryQty = primaryQty + response[0].data[j].qty;
                                primaryAmt = primaryAmt + response[0].data[j].amount;
                            } else if (response[0].data[j].type === 'returnSale') {
                                returnQty = returnQty + response[0].data[j].qty;
                                returnAmt = returnAmt + response[0].data[j].amount;
                            }
                        }
                        responses.push(obj1);
                        PrimaryInfoCollection.update({
                            companyId: ObjectId(param[i].companyId),
                            productId: ObjectId(param[i].productId),
                            month: parseInt(param[i].month),
                            year: parseInt(param[i].year),
                            status: true
                        }, {
                            $set: {
                                "data": responses,
                                "totalPrimaryQty": primaryQty,
                                "totalPrimaryAmt": primaryAmt,
                                "totalSaleReturnQty": returnQty,
                                "totalSaleReturnAmt": returnAmt,
                                "updatedAt": new Date()
                            }
                        }, {
                            multi: false
                        },
                            function (err, res) {
                                if (err) {
                                    return cb(false, err);
                                }
                                if (res) {
                                    var lastResult = [];
                                    lastResult.push({
                                        "count": res.result.nModified
                                    });
                                }
                            });
                    } else {
                        Primaryandsalereturn.create(param[i], function (err, resp) { });
                    }
                });
        }
        let response = [];
        response.push({ "status": "OK" });
        return cb(false, response)
    };
    Primaryandsalereturn.remoteMethod(
        'createPrimaryInfoWithValidationCheck', {
        description: 'createPrimaryInfoWithValidationCheck',
        accepts: [{
            arg: 'param',
            type: 'array'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.deleteSubmittedProducts = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);

        var record = [];
        if (param.productInfo.data.length === 1) {
            PrimaryInfoCollection.remove({
                _id: ObjectId(param.productInfo.id)
            }, function (err, result) {
                if (err) {
                    return res.status(500).json({ 'error': 'error in deleting address' });
                }
                record.push({
                    result: result.result
                });
                return cb(false, record);
            });
        } else if (param.productInfo.data.length > 1) {
            PrimaryInfoCollection.update({ _id: ObjectId(param.productInfo.id) }, {
                $pull: {
                    data: { type: param.formInfo.saleType, userId: param.formInfo.userId }
                }
            }, function (err, data) {
                if (err) {
                    return res.status(500).json({ 'error': 'error in deleting address' });
                }
                record.push({
                    result: data.result
                });
                return cb(false, record);
            });
        }

    };

    Primaryandsalereturn.remoteMethod(
        'deleteSubmittedProducts', {
        description: 'deleteSubmittedProducts',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.getProductInfoAsUnwindUserData = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);

        PrimaryInfoCollection.aggregate(
            // Stage 1
            {
                $match: {
                    companyId: ObjectId(param.companyId),
                    data: {
                        $elemMatch: {
                            type: param.formInfo.saleType,
                            userId: param.formInfo.userId
                        }
                    },
                    month: parseInt(param.formInfo.month),
                    year: parseInt(param.formInfo.year)
                }
            },

            // Stage 2
            {
                $unwind: "$data"
            },

            // Stage 3
            {
                $match: {
                    companyId: ObjectId(param.companyId),
                    "data.userId": param.formInfo.userId,
                    "data.type": param.formInfo.saleType,
                    month: param.formInfo.month,
                    year: param.formInfo.year
                }
            },

            // Stage 3
            {
                $project: {
                    _id: 0,
                    productName: "$productName",
                    netRate: "$data.netRate",
                    splRate: "$data.splRate",
                    opening: "$opening",
                    primarySale: "$data.qty",
                    returnSale: "$data.qty",
                    breakage: "$data.breakage",
                    saleable: "$data.saleable",
                    other: "$data.other",
                    productId: "$productId",
                    invoiceNo: "$data.invoiceNo",
                    invoiceDate: "$data.invoiceDate",
                }
            },
            function (err, response) {
                return cb(false, response);
            });

    };
    Primaryandsalereturn.remoteMethod(
        'getProductInfoAsUnwindUserData', {
        description: 'getProductInfoAsUnwindUserData',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.editSubmittedSSSDetail = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        for (let i = 0; i < param.sssInfo.length; i++) {
            var qty = 0;
            var amt = 0;
            var opening = 0;
            let obj1 = {};
            opening = parseInt(param.sssInfo[i].opening);
            if (param.formInfo.saleType === 'primarySale') {
                qty = param.sssInfo[i].primarySale;
                if (param.sssInfo[i].splRate == undefined || param.sssInfo[i].splRate == null || param.sssInfo[i].splRate == '') {
                    amt = parseInt(qty * param.sssInfo[i].netRate)
                } else {
                    amt = parseInt(qty * param.sssInfo[i].splRate)
                }
                obj1 = {
                    "type": param.formInfo.saleType,
                    "invoiceNo": param.sssInfo[i].invoiceNo,
                    "invoiceDate": new Date(param.sssInfo[i].invoiceDate),
                    "userId": param.formInfo.userId,
                    "netRate": parseInt(param.sssInfo[i].netRate),
                    "splRate": param.sssInfo[i].splRate,
                    "qty": parseInt(qty),
                    "amount": parseInt(amt),
                    "createdAt": new Date()
                }
            } else if (param.formInfo.saleType === 'returnSale') {
                qty = (param.sssInfo[i].breakage + param.sssInfo[i].saleable + param.sssInfo[i].other);
                if (param.sssInfo[i].splRate == undefined || param.sssInfo[i].splRate == null || param.sssInfo[i].splRate == '') {
                    amt = parseInt(qty * param.sssInfo[i].netRate)
                } else {
                    amt = parseInt(qty * param.sssInfo[i].splRate)
                }
                obj1 = {
                    "type": param.formInfo.saleType,
                    "invoiceNo": param.sssInfo[i].invoiceNo,
                    "invoiceDate": new Date(param.sssInfo[i].invoiceDate),
                    "userId": param.formInfo.userId,
                    "netRate": parseInt(param.sssInfo[i].netRate),
                    "splRate": param.sssInfo[i].splRate,
                    "qty": parseInt(qty),
                    "breakage": parseInt(param.sssInfo[i].breakage),
                    "saleable": parseInt(param.sssInfo[i].saleable),
                    "other": parseInt(param.sssInfo[i].other),
                    "amount": parseInt(amt),
                    "createdAt": new Date()
                }
            }

            PrimaryInfoCollection.update({ productId: ObjectId(param.sssInfo[i].productId), month: parseInt(param.formInfo.month), year: parseInt(param.formInfo.year) }, {
                $pull: {
                    data: { type: param.formInfo.saleType, userId: param.formInfo.userId }
                }
            }, function (err, data) {
                if (err) {
                    return res.status(500).json({ 'error': 'error in deleting address' });
                }
                var record = [];
                record.push({
                    result: data.result.nModified
                });
                if (data.result.nModified === 1) {
                    PrimaryInfoCollection.aggregate({
                        $match: {
                            companyId: ObjectId(param.companyId),
                            productId: ObjectId(param.sssInfo[i].productId),
                            //data: { $elemMatch: { userId: param.formInfo.userId } },
                            //data: { $elemMatch: { type: param.formInfo.saleType } },
                            partyId: ObjectId(param.formInfo.stockistId.stkId),
                            month: parseInt(param.formInfo.month),
                            year: parseInt(param.formInfo.year)
                        }
                    },
                        function (err, response) {
                            if (response.length > 0) {
                                var primaryQty = 0;
                                var primaryAmt = 0;
                                var returnQty = 0;
                                var returnAmt = 0;
                                if (param.formInfo.saleType === 'primarySale') {
                                    if (param.sssInfo[i].splRate == undefined || param.sssInfo[i].splRate == null || param.sssInfo[i].splRate == '') {
                                        amtValue = parseInt(param.sssInfo[i].primarySale * param.sssInfo[i].netRate)
                                    } else {
                                        amtValue = parseInt(param.sssInfo[i].primarySale * param.sssInfo[i].splRate)
                                    }
                                    primaryQty = primaryQty + param.sssInfo[i].primarySale;
                                    primaryAmt = primaryAmt + amtValue;
                                } else if (param.formInfo.saleType === 'returnSale') {
                                    if (param.sssInfo[i].splRate == undefined || param.sssInfo[i].splRate == null || param.sssInfo[i].splRate == '') {
                                        amtValue = parseInt(param.sssInfo[i].breakage + param.sssInfo[i].saleable + param.sssInfo[i].other) * (param.sssInfo[i].netRate);
                                    } else {
                                        amtValue = parseInt(param.sssInfo[i].breakage + param.sssInfo[i].saleable + param.sssInfo[i].other) * (param.sssInfo[i].splRate);
                                    }
                                    returnQty = returnQty + param.sssInfo[i].breakage + param.sssInfo[i].saleable + param.sssInfo[i].other;
                                    returnAmt = returnAmt + amtValue;
                                }
                                var responses = [];
                                if (response[0].data.length > 0) {
                                    for (let j = 0; j < response[0].data.length; j++) {
                                        responses.push(response[0].data[j]);
                                        if (response[0].data[j].type === 'primarySale') {
                                            primaryQty = primaryQty + response[0].data[j].qty;
                                            primaryAmt = primaryAmt + response[0].data[j].amount;
                                        } else if (response[0].data[j].type === 'returnSale') {
                                            returnQty = returnQty + response[0].data[j].qty;
                                            returnAmt = returnAmt + response[0].data[j].amount;
                                        }
                                    }
                                }

                                responses.push(obj1);
                                PrimaryInfoCollection.update({
                                    companyId: ObjectId(param.companyId),
                                    partyId: ObjectId(param.formInfo.stockistId.stkId),
                                    productId: ObjectId(param.sssInfo[i].productId),
                                    month: parseInt(param.formInfo.month),
                                    year: parseInt(param.formInfo.year)
                                }, {
                                    $set: {
                                        "data": responses,
                                        "opening": opening,
                                        "totalPrimaryQty": primaryQty,
                                        "totalPrimaryAmt": primaryAmt,
                                        "totalSaleReturnQty": returnQty,
                                        "totalSaleReturnAmt": returnAmt,
                                        "updatedAt": new Date()
                                    }
                                }, {
                                    multi: false
                                },
                                    function (err, res) {
                                        if (err) {
                                            //return cb(false, err);
                                        }
                                        if (res) {
                                            var lastResult = [];
                                            lastResult.push({
                                                "count": res.result.nModified
                                            });
                                        }
                                    });
                            } else {
                                //console.log('Hello ELSE  Product not found');
                            }
                        });
                }
            });

        }
        var res = [];
        res.push({
            "status": 'Ok'
        });
        return cb(false, res);

    };

    Primaryandsalereturn.remoteMethod(
        'editSubmittedSSSDetail', {
        description: 'editSubmittedSSSDetail',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.getSubmittedSSSDetailForSSSClosing = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);

        PrimaryInfoCollection.aggregate({
            $match: {
                companyId: ObjectId(param.companyId),
                partyId: ObjectId(param.sssClosingInfo.stockistId.stkId),
                month: parseInt(param.sssClosingInfo.month),
                year: parseInt(param.sssClosingInfo.year)
            }
        },


            // Stage 2
            {
                $unwind: { path: "$data", preserveNullAndEmptyArrays: true }
            },

            // Stage 3
            {
                $group: {
                    _id: {
                        productId: "$productId",
                        productName: "$productName",
                        closing: "$closing"
                    },
                    "primarySaleQty": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "primarySale"]
                            }, "$data.qty", 0]
                        }
                    },
                    "primarySaleAmt": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "primarySale"]
                            }, "$data.amount", 0]
                        }
                    },
                    "primarySaleTotalUser": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "primarySale"]
                            }, 1, 0]
                        }
                    },
                    "primarySaleData": {
                        $addToSet: {
                            $cond: [{
                                $eq: ["$data.type", "primarySale"]
                            }, "$data", 0]
                        }
                    },
                    "returnSaleQty": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "returnSale"]
                            }, "$data.qty", 0]
                        }
                    },
                    "returnSaleAmt": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "returnSale"]
                            }, "$data.amount", 0]
                        }
                    },
                    "returnSaleTotalUser": {
                        $sum: {
                            $cond: [{
                                $eq: ["$data.type", "returnSale"]
                            }, 1, 0]
                        }
                    },
                    "returnSaleData": {
                        $addToSet: {
                            $cond: [{
                                $eq: ["$data.type", "returnSale"]
                            }, "$data", 0]
                        }
                    }


                }
            },
            function (err, sssResponse) {
                let stateId = [];
                stateId.push(param.sssClosingInfo.stateId);
                Primaryandsalereturn.app.models.Products.find({
                    where: {
                        companyId: param.companyId,
                        assignedStates: { inq: stateId },
                        status: true
                    }
                }, function (err, response) {
                    let data = [];
                    for (let i = 0; i < response.length; i++) {
                        var filterObj1 = sssResponse.filter(function (obj) {
                            return obj._id.productId == response[i].id.toString();
                        });
                        if (filterObj1.length > 0) {
                            data.push({
                                productName: filterObj1[0]._id.productName,
                                productId: filterObj1[0]._id.productId,
                                totalPrimarySubmitUser: filterObj1[0].primarySaleTotalUser,
                                totalPrimarySale: filterObj1[0].primarySaleQty,
                                totalRetuenSubmitUser: filterObj1[0].returnSaleTotalUser,
                                totalRetuenSale: filterObj1[0].returnSaleQty,
                                primarySaleData: filterObj1[0].primarySaleData,
                                returnSaleData: filterObj1[0].returnSaleData,
                                closing: filterObj1[0]._id.closing,
                            });
                        } else {
                            data.push({
                                productName: response[i].productName,
                                productId: response[i].id,
                                totalPrimarySubmitUser: 0,
                                totalPrimarySale: 0,
                                totalRetuenSubmitUser: 0,
                                totalRetuenSale: 0,
                                primarySaleData: [],
                                returnSaleData: [],
                                closing: 0,
                            });
                        }
                    }
                    return cb(false, data);
                });
            });

    };

    Primaryandsalereturn.remoteMethod(
        'getSubmittedSSSDetailForSSSClosing', {
        description: 'getSubmittedSSSDetailForSSSClosing',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.submitClosingDetail = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        for (let i = 0; i < param.sssClosingInfo.length; i++) {
            PrimaryInfoCollection.aggregate({
                $match: {
                    companyId: ObjectId(param.companyId),
                    productId: ObjectId(param.sssClosingInfo[i].productId),
                    partyId: ObjectId(param.formInfo.stockistId.stkId),
                    month: parseInt(param.formInfo.month),
                    year: parseInt(param.formInfo.year)
                }
            },
                function (err, checkProductExist) {
                    if (checkProductExist.length > 0) {
                        PrimaryInfoCollection.update({
                            companyId: ObjectId(param.companyId),
                            partyId: ObjectId(param.formInfo.stockistId.stkId),
                            productId: ObjectId(param.sssClosingInfo[i].productId),
                            month: parseInt(param.formInfo.month),
                            year: parseInt(param.formInfo.year)
                        }, {
                            $set: {
                                "closing": param.sssClosingInfo[i].closing,
                                "userId": ObjectId(param.formInfo.userId),
                                "createClosingAt": new Date()
                            }
                        }, {
                            multi: false
                        },
                            function (err, res) {
                                if (err) {
                                    //return cb(false, err);
                                }
                                if (res) {
                                    var lastResult = [];
                                    lastResult.push({
                                        "count": res.result.nModified
                                    });
                                }
                            });
                    } else {
                        var newObj = {
                            "companyId": ObjectId(param.companyId),
                            "productId": ObjectId(param.sssClosingInfo[i].productId),
                            "productName": param.sssClosingInfo[i].productName,
                            "productCode": param.sssClosingInfo[i].productName,
                            "partyName": param.formInfo.stockistId.stkName,
                            "partyCode": param.formInfo.stockistId.stkName,
                            "districtId": ObjectId(param.formInfo.districtId),
                            "blockId": ObjectId(param.formInfo.districtId),
                            "data": [],
                            "month": parseInt(param.formInfo.month),
                            "year": parseInt(param.formInfo.year),
                            "totalPrimaryQty": 0,
                            "totalPrimaryAmt": 0,
                            "totalSaleReturnQty": 0,
                            "totalSaleReturnAmt": 0,
                            "createdAt": new Date(),
                            "partyId": ObjectId(param.formInfo.stockistId.stkId),
                            "opening": 0,
                            "createClosingAt": new Date(),
                            "userId": ObjectId(param.formInfo.userId),
                            "closing": param.sssClosingInfo[i].closing
                        }
                        PrimaryInfoCollection.insertOne(newObj, function (err, result) { });
                    }
                });

        }
        var res = [];
        res.push({
            "status": 'Ok'
        });
        return cb(false, res);
    };

    Primaryandsalereturn.remoteMethod(
        'submitClosingDetail', {
        description: 'submitClosingDetail',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    Primaryandsalereturn.checkClosingSubmittedOrNot = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        PrimaryInfoCollection.aggregate({
            $match: {
                companyId: ObjectId(param.companyId),
                partyId: ObjectId(param.partyId),
                month: parseInt(param.month),
                year: parseInt(param.year)
            }
        },
            // Stage 1
            {
                $group: {
                    _id: {
                        companyId: "$companyId",
                        partyId: "$partyId",
                        month: "$month",
                        year: "$year"
                    },
                    closing: {
                        $sum: "$closing"
                    }
                }
            },
            function (err, checkClosingExist) {
                return cb(false, checkClosingExist);
            });
    };

    Primaryandsalereturn.remoteMethod(
        'checkClosingSubmittedOrNot', {
        description: 'checkClosingSubmittedOrNot',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );


    Primaryandsalereturn.getSubmittedSaleStateAndHQwise = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let stateArr = [];
        let districtArr = [];
        let userIdArr = [];
        let partyIdArr = [];
        let dynamicMatch = {};
        //Praveen Kumar 04-02-2019 06:64PM
        //As earlier, the state was an array but as per anjana mam instructions, 
        //the state would be a single at a time.
        if (param.sssInfo.stateInfo != null && typeof (param.sssInfo.stateInfo) == "object") {
            for (let i = 0; i < param.sssInfo.stateInfo.length; i++) {
                stateArr.push(ObjectId(param.sssInfo.stateInfo[i]));
            }
        }
        if (param.sssInfo.stateInfo != null && typeof (param.sssInfo.stateInfo) == "string") {
            stateArr.push(ObjectId(param.sssInfo.stateInfo));
        }

        if (param.sssInfo.districtInfo != null) {
            if (param.sssInfo.districtInfo.length > 0) {
                for (let j = 0; j < param.sssInfo.districtInfo.length; j++) {
                    districtArr.push(ObjectId(param.sssInfo.districtInfo[j]));
                }
            }
        }
        if (param.sssInfo.userInfo != null) {
            if (param.sssInfo.userInfo.length > 0) {
                for (let k = 0; k < param.sssInfo.userInfo.length; k++) {
                    userIdArr.push(ObjectId(param.sssInfo.userInfo[k]));
                }
            }
        }
        if (param.sssInfo.stokistInfo != null) {
            if (param.sssInfo.stokistInfo.length > 0) {
                for (let m = 0; m < param.sssInfo.stokistInfo.length; m++) {
                    partyIdArr.push(ObjectId(param.sssInfo.stokistInfo[m].stkId));
                }
            }
        }

        if (param.sssInfo.type === 'State') {
            dynamicMatch = {
                companyId: ObjectId(param.companyId),
                stateId: { $in: stateArr },
                month: parseInt(param.sssInfo.month),
                year: parseInt(param.sssInfo.year)
            }

        } else if (param.sssInfo.type === 'Headquarter') {
            dynamicMatch = {
                companyId: ObjectId(param.companyId),
                districtId: { $in: districtArr },
                month: parseInt(param.sssInfo.month),
                year: parseInt(param.sssInfo.year)
            }
        } else if (param.sssInfo.reportType === 'Employeewise') {
            if (param.sssType == 0) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    userId: { $in: userIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            } else if (param.sssType == 1) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    // districtId: { $in: districtArr },
                    data: { $elemMatch: { userId: { $in: param.sssInfo.userInfo } } },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            }
        } else if (param.sssInfo.reportType === 'Stokistwise') {
            if (param.sssType == 0) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    userId: { $in: userIdArr },
                    partyId: { $in: partyIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            } else if (param.sssType == 1) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    data: { $elemMatch: { userId: { $in: param.sssInfo.userInfo } } },
                    partyId: { $in: partyIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            }
        }

        if (param.sssType === 0) {
            PrimaryInfoCollection.aggregate(

                {
                    $match: dynamicMatch
                },
                {
                    $lookup: {
                        "from": "DoctorChemist",
                        "localField": "partyId",
                        "foreignField": "providerLinkedId",
                        "as": "data"
                    }
                },

                // Stage 3
                {
                    $unwind: {
                        path: "$data",
                        preserveNullAndEmptyArrays: true
                    }
                },

                // Stage 4
                {
                    $group: {
                        _id: {
                            "productId": "$productId",
                            "productName": "$productName",
                            "netRate": "$netRate",
                        },
                        partyData: {
                            $addToSet: "$data.providerLinkedName"
                        },
                        partyDataDoctor: {
                            $addToSet: "$data.providerName"
                        },
                        totalSecondrySaleAmt: {
                            $sum: "$totalSecondrySaleAmt"
                        },
                        totalPrimaryQty: {
                            $sum: "$totalPrimaryQty"
                        },
                        opening: {
                            $sum: "$opening"
                        },
                        totalPrimaryAmt: {
                            $sum: "$totalPrimaryAmt"
                        },
                        totalSaleReturnQty: {
                            $sum: "$totalSaleReturnQty"
                        },
                        totalSaleReturnAmt: {
                            $sum: "$totalSaleReturnAmt"
                        },
                        totalSecondrySaleQty: {
                            $sum: "$totalSecondrySaleQty"
                        },
                        totalStock: {
                            $sum: "$totalStock"
                        },
                        totalStockValue: {
                            $sum: "$totalStockValue"
                        },
                        closing: {
                            $sum: "$closing"
                        },
                        closingAmt: {
                            $sum: "$closingAmt"
                        },
                        breakage: {
                            $sum: "$breakage"
                        },
                        breakageQty: {
                            $sum: "$breakageQty"
                        },
                        expiry: {
                            $sum: "$expiry"
                        },
                        expiryQty: {
                            $sum: "$expiryQty"
                        },
                        batchRecall: {
                            $sum: "$batchRecall"
                        },
                        batchRecallQty: {
                            $sum: "$batchRecallQty"
                        },
                    }
                },

                // Stage 5
                {
                    $project: {
                        productName: "$_id.productName",
                        productRate: "$_id.netRate",
                        partyName: { $arrayElemAt: ["$partyData", 0] },
                        partyNameDoctor: { $arrayElemAt: ["$partyDataDoctor", 0] },
                        productId: "$_id.productId",
                        opening: "$opening",
                        totalPrimaryQty: "$totalPrimaryQty",
                        totalPrimaryAmt: "$totalPrimaryAmt",
                        totalSaleReturnQty: "$totalSaleReturnQty",
                        totalSaleReturnAmt: "$totalSaleReturnAmt",
                        secondarySaleQty: "$totalSecondrySaleQty",
                        secondarySaleValue: "$totalSecondrySaleAmt",
                        breakage: "$breakage",
                        totalStockValue: { $multiply: [{ $subtract: [{ $add: ["$opening", "$totalPrimaryQty"] }, "$totalSaleReturnQty"] }, "$_id.netRate"] }, totalStock: {
                            $subtract: [{ $add: ["$opening", "$totalPrimaryQty"] }, "$totalSaleReturnQty"]
                        },
                        closing: "$closing",
                        closingvalue: "$closingAmt",
                        breakageQty: "$breakageQty",
                        expiry: "$expiry",
                        expiryQty: "$expiryQty",
                        batchRecall: "$batchRecall",
                        batchRecallQty: "$batchRecallQty"
                    }
                }, function (err, data) {
                    if (err) {
                        return cb(err)
                    }
                    let
                        totalSaleReturnQtySum = 0,
                        totalSaleReturnAmtSum = 0,
                        expiryQtySum = 0,
                        expirySum = 0,
                        openingQtySum = 0,
                        openingSum = 0,
                        breakageQtySum = 0,
                        breakageSum = 0,
                        batchRecallQtySum = 0,
                        batchRecallSum = 0,
                        totalPrimaryQtySum = 0,
                        totalPrimaryAmtSum = 0,
                        closingSum = 0,
                        closingvalueSum = 0,
                        secondarySaleQtySum = 0,
                        secondarySaleValueSum = 0;
                    let total = [];
                    data.forEach(item => {
                        totalSaleReturnQtySum += item["totalSaleReturnQty"];
                        totalSaleReturnAmtSum += item["totalSaleReturnAmt"];
                        expiryQtySum += item["expiryQty"];
                        item["expiry"] = item.expiryQty * item.productRate;
                        expirySum += item["expiry"];
                        item["openingQty"] = item.opening;
                        openingQtySum += item["openingQty"]
                        item["opening"] = item.openingQty * item.productRate
                        openingSum += item["opening"];
                        breakageQtySum += item["breakageQty"];
                        item["breakage"] = item.breakageQty * item.productRate;
                        breakageSum += item["breakage"];
                        batchRecallQtySum += item["batchRecallQty"]
                        item["batchRecall"] = item.batchRecallQty * item.productRate;
                        batchRecallSum += item["batchRecall"];
                        totalPrimaryQtySum += item["totalPrimaryQty"]
                        totalPrimaryAmtSum += item["totalPrimaryAmt"]
                        closingSum += item["closing"];
                        closingvalueSum += item["closingvalue"]
                        item["secondarySaleQty"] = item.totalPrimaryQty + item.openingQty - (item.totalSaleReturnQty + item.expiryQty + item.breakageQty + item.batchRecallQty + item.closing);
                        secondarySaleQtySum += item["secondarySaleQty"]
                        item["secondarySaleValue"] = item.secondarySaleQty * item.productRate;
                        secondarySaleValueSum += item["secondarySaleValue"];

                    });
                    total.push({
                        "totalSaleReturnQtySum": totalSaleReturnQtySum,
                        "totalSaleReturnAmtSum": totalSaleReturnAmtSum,
                        "expiryQtySum": expiryQtySum,
                        "expirySum": expirySum,
                        "openingQtySum": openingQtySum,
                        "openingSum": openingSum,
                        "breakageQtySum": breakageQtySum,
                        "breakageSum": breakageSum,
                        "batchRecallQtySum": batchRecallQtySum,
                        "batchRecallSum": batchRecallSum,
                        "totalPrimaryQtySum": totalPrimaryQtySum,
                        "totalPrimaryAmtSum": totalPrimaryAmtSum,
                        "closingSum": closingSum,
                        "closingvalueSum": closingvalueSum,
                        "secondarySaleQtySum": secondarySaleQtySum,
                        "secondarySaleValueSum": secondarySaleValueSum
                    });
                    // data.push(total);  

                    return cb(false, data)


                    // if(data.length>0){
                    //     let newData = data.map(item=>{
                    //         item["closingvalue"]=item.closingvalue.toFixed(2);
                    //         item["secondarySaleValue"]=item.secondarySaleValue.toFixed(2);
                    //         item["totalSaleReturnAmt":item.totalSaleReturnAmt.toFixed(2);
                    //         return item;
                    //     })
                    //     return cb(false, newData)
                    // }else{
                    //     return cb(false, [])
                    // }

                }
            );
            /*Primaryandsalereturn.app.models.Products.find({
                where: {
                    companyId: param.companyId,
                    assignedStates: { inq: param.sssInfo.stateInfo },
                }
            }, function (err, response) {
                let data = [];
                for (let i = 0; i < response.length; i++) {
                    var filterObj1 = salesData.filter(function (obj) {
                        return obj.productId.toString() == response[i].id.toString();
                    });
                    if (filterObj1.length > 0) {
                        var rate1;
                        if (filterObj1[0].splRate == '' || filterObj1[0].splRate == undefined || filterObj1[0].splRate == null) {
                            rate1 = filterObj1[0].netRate
                        } else {
                            rate1 = filterObj1[0].splRate
                        }
                        data.push({
                            productName: filterObj1[0].productName,
                            productRate: rate1,
                            productId: filterObj1[0].productId,
                            opening: filterObj1[0].opening,
                            totalPrimaryQty: filterObj1[0].totalPrimaryQty,
                            totalPrimaryAmt: filterObj1[0].totalPrimaryAmt,
                            totalSaleReturnQty: filterObj1[0].totalSaleReturnQty,
                            totalSaleReturnAmt: filterObj1[0].totalSaleReturnAmt,
                            secondarySaleQty: filterObj1[0].totalSecondrySaleQty,
                            secondarySaleValue: filterObj1[0].totalSecondrySaleAmt,
                            totalStock: (filterObj1[0].opening + filterObj1[0].totalPrimaryQty - filterObj1[0].totalSaleReturnQty),
                            totalStockValue: (filterObj1[0].opening + filterObj1[0].totalPrimaryQty - filterObj1[0].totalSaleReturnQty) * (rate1),
                            closing: filterObj1[0].closing,
                            closingvalue: filterObj1[0].closingAmt
                        });
                    } else {
                        if (param.stkAmtCal == undefined) {
                            data.push({
                                productName: response[i].productName,
                                productRate: response[i].ptw,
                                productId: response[i].id,
                                opening: 0,
                                totalPrimaryQty: 0,
                                totalPrimaryAmt: 0,
                                totalSaleReturnQty: 0,
                                totalSaleReturnAmt: 0,
                                secondarySaleQty: 0,
                                secondarySaleValue: 0,
                                totalStock: 0,
                                totalStockValue: 0,
                                closing: 0,
                                closingvalue: 0
                            });
                        } else if (param.stkAmtCal == "mrp") {
                            data.push({
                                productName: response[i].productName,
                                productRate: response[i].mrp,
                                productId: response[i].id,
                                opening: 0,
                                totalPrimaryQty: 0,
                                totalPrimaryAmt: 0,
                                totalSaleReturnQty: 0,
                                totalSaleReturnAmt: 0,
                                secondarySaleQty: 0,
                                secondarySaleValue: 0,
                                totalStock: 0,
                                totalStockValue: 0,
                                closing: 0,
                                closingvalue: 0
                            });

                        } else if (param.stkAmtCal == "ptr") {
                            data.push({
                                productName: response[i].productName,
                                productRate: response[i].ptr,
                                productId: response[i].id,
                                opening: 0,
                                totalPrimaryQty: 0,
                                totalPrimaryAmt: 0,
                                totalSaleReturnQty: 0,
                                totalSaleReturnAmt: 0,
                                secondarySaleQty: 0,
                                secondarySaleValue: 0,
                                totalStock: 0,
                                totalStockValue: 0,
                                closing: 0,
                                closingvalue: 0
                            });
                        } else if (param.stkAmtCal == "pts") {
                            data.push({
                                productName: response[i].productName,
                                productRate: response[i].pts,
                                productId: response[i].id,
                                opening: 0,
                                totalPrimaryQty: 0,
                                totalPrimaryAmt: 0,
                                totalSaleReturnQty: 0,
                                totalSaleReturnAmt: 0,
                                secondarySaleQty: 0,
                                secondarySaleValue: 0,
                                totalStock: 0,
                                totalStockValue: 0,
                                closing: 0,
                                closingvalue: 0
                            });
                        } else {
                            data.push({
                                productName: response[i].productName,
                                productRate: response[i].ptw,
                                productId: response[i].id,
                                opening: 0,
                                totalPrimaryQty: 0,
                                totalPrimaryAmt: 0,
                                totalSaleReturnQty: 0,
                                totalSaleReturnAmt: 0,
                                secondarySaleQty: 0,
                                secondarySaleValue: 0,
                                totalStock: 0,
                                totalStockValue: 0,
                                closing: 0,
                                closingvalue: 0
                            });
                        }
                    }
                }
                console.log("data=",data);
                return cb(false, data);
            });*/
            // return cb(false, salesData);

        } else if (param.sssType === 1) {

            PrimaryInfoCollection.aggregate(
                {
                    $match: dynamicMatch
                },



                // Stage 2
                {
                    $unwind: { path: "$data", preserveNullAndEmptyArrays: true }
                },

                // Stage 3
                {
                    $group: {
                        _id: {
                            productId: "$productId",
                            productName: "$productName",
                            opening: "$opening",
                            closing: "$closing"
                        },
                        "primarySaleQty": {
                            $sum: {
                                $cond: [{
                                    $eq: ["$data.type", "primarySale"]
                                }, "$data.qty", 0]
                            }
                        },
                        "primarySaleAmt": {
                            $sum: {
                                $cond: [{
                                    $eq: ["$data.type", "primarySale"]
                                }, "$data.amount", 0]
                            }
                        },
                        "returnSaleQty": {
                            $sum: {
                                $cond: [{
                                    $eq: ["$data.type", "returnSale"]
                                }, "$data.qty", 0]
                            }
                        },
                        "returnSaleAmt": {
                            $sum: {
                                $cond: [{
                                    $eq: ["$data.type", "returnSale"]
                                }, "$data.amount", 0]
                            }
                        }
                    }
                },
                function (err, sssResponse) {
                    Primaryandsalereturn.app.models.Products.find({
                        where: {
                            companyId: param.companyId,
                            assignedStates: { inq: stateArr },
                            status: true
                        }
                    }, function (err, response) {
                        let data = [];
                        for (let i = 0; i < response.length; i++) {
                            var filterObj1 = sssResponse.filter(function (obj) {
                                return obj._id.productId.toString() == response[i].id.toString();
                            });
                            if (filterObj1.length > 0) {
                                data.push({
                                    productName: filterObj1[0]._id.productName,
                                    productRate: response[i].mrp,
                                    productId: filterObj1[0]._id.productId,
                                    opening: filterObj1[0]._id.opening,
                                    totalPrimaryQty: filterObj1[0].primarySaleQty,
                                    totalPrimaryAmt: (filterObj1[0].primarySaleAmt).toFixed(2),
                                    totalSaleReturnQty: filterObj1[0].returnSaleQty,
                                    totalSaleReturnAmt: filterObj1[0].returnSaleAmt,
                                    secondarySaleQty: (filterObj1[0]._id.opening + filterObj1[0].primarySaleQty - filterObj1[0]._id.closing - filterObj1[0].returnSaleQty),
                                    secondarySaleValue: (filterObj1[0]._id.opening + filterObj1[0].primarySaleQty - filterObj1[0]._id.closing - filterObj1[0].returnSaleQty) * (response[i].mrp),
                                    totalStock: (filterObj1[0]._id.opening + filterObj1[0].primarySaleQty - filterObj1[0].returnSaleQty),
                                    totalStockValue: ((filterObj1[0]._id.opening + filterObj1[0].primarySaleQty - filterObj1[0].returnSaleQty) * (response[i].mrp)).toFixed(2),
                                    closing: filterObj1[0]._id.closing,
                                    closingvalue: (filterObj1[0]._id.closing) * (response[i].mrp)
                                });
                            } else {
                                data.push({
                                    productName: response[i].productName,
                                    productRate: response[i].mrp,
                                    productId: response[i].id,
                                    opening: 0,
                                    totalPrimaryQty: 0,
                                    totalPrimaryAmt: 0,
                                    totalSaleReturnQty: 0,
                                    totalSaleReturnAmt: 0,
                                    secondarySaleQty: 0,
                                    secondarySaleValue: 0,
                                    totalStock: 0,
                                    totalStockValue: 0,
                                    closing: 0,
                                    closingvalue: 0
                                });
                            }
                        }

                        return cb(false, data);
                    });
                });
        }
    }
    Primaryandsalereturn.remoteMethod(
        'getSubmittedSaleStateAndHQwise', {
        description: 'getSubmittedSaleStateAndHQwise',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );




    // Updated by Aakash on (01-05-2021) for McW
    Primaryandsalereturn.getSubmittedSSSSalePerformanceWise = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let stateArr = [];
        let districtArr = [];
        let userIdArr = [];
        let partyIdArr = [];
        let dynamicMatch = {};
        //Praveen Kumar 04-02-2019 06:64PM
        //As earlier, the state was an array but as per anjana mam instructions, 
        //the state would be a single at a time.
        if (param.sssInfo.stateInfo != null && typeof (param.sssInfo.stateInfo) == "object") {
            for (let i = 0; i < param.sssInfo.stateInfo.length; i++) {
                stateArr.push(ObjectId(param.sssInfo.stateInfo[i]));
            }
        }
        if (param.sssInfo.stateInfo != null && typeof (param.sssInfo.stateInfo) == "string") {
            stateArr.push(ObjectId(param.sssInfo.stateInfo));
        }

        if (param.sssInfo.districtInfo != null) {
            if (param.sssInfo.districtInfo.length > 0) {
                for (let j = 0; j < param.sssInfo.districtInfo.length; j++) {
                    districtArr.push(ObjectId(param.sssInfo.districtInfo[j]));
                }
            }
        }
        if (param.sssInfo.userInfo != null) {
            if (param.sssInfo.userInfo.length > 0) {
                for (let k = 0; k < param.sssInfo.userInfo.length; k++) {
                    userIdArr.push(ObjectId(param.sssInfo.userInfo[k]));
                }
            }
        }
        if (param.sssInfo.stokistInfo != null) {
            if (param.sssInfo.stokistInfo.length > 0) {
                for (let m = 0; m < param.sssInfo.stokistInfo.length; m++) {
                    partyIdArr.push(ObjectId(param.sssInfo.stokistInfo[m].stkId));
                }
            }
        }

        if (param.sssInfo.type === 'State') {
            dynamicMatch = {
                companyId: ObjectId(param.companyId),
                stateId: { $in: stateArr },
                month: parseInt(param.sssInfo.month),
                year: parseInt(param.sssInfo.year)
            }

        } else if (param.sssInfo.type === 'Headquarter') {
            dynamicMatch = {
                companyId: ObjectId(param.companyId),
                districtId: { $in: districtArr },
                month: parseInt(param.sssInfo.month),
                year: parseInt(param.sssInfo.year)
            }
        } else if (param.sssInfo.reportType === 'Employeewise') {
            if (param.sssType == 0) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    userId: { $in: userIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            } else if (param.sssType == 1) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    // districtId: { $in: districtArr },
                    data: { $elemMatch: { userId: { $in: param.sssInfo.userInfo } } },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            }
        } else if (param.sssInfo.reportType === 'Stokistwise') {
            if (param.sssType == 0) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    userId: { $in: userIdArr },
                    partyId: { $in: partyIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            } else if (param.sssType == 1) {
                dynamicMatch = {
                    companyId: ObjectId(param.companyId),
                    //  districtId: { $in: districtArr },
                    data: { $elemMatch: { userId: { $in: param.sssInfo.userInfo } } },
                    partyId: { $in: partyIdArr },
                    month: parseInt(param.sssInfo.month),
                    year: parseInt(param.sssInfo.year)
                }
            }
        }

        if (param.sssType === 0) {
            PrimaryInfoCollection.aggregate(

                {
                    $match: dynamicMatch
                }, {
                $lookup: {
                    "from": "DoctorChemist",
                    "localField": "partyId",
                    "foreignField": "providerLinkedId",
                    "as": "data"
                }
            },

                // Stage 3
                {
                    $unwind: {
                        path: "$data",
                        preserveNullAndEmptyArrays: true
                    }
                },

                // Stage 4
                {
                    $group: {
                        _id: {
                            "productId": "$productId",
                            "productName": "$productName",
                            "netRate": "$netRate",
                        },
                        partyData: {
                            $addToSet: "$data.providerLinkedName"
                        },
                        partyDataDoctor: {
                            $addToSet: "$data.providerName"
                        },
                        totalSecondrySaleAmt: {
                            $sum: "$totalSecondrySaleAmt"
                        },
                        totalPrimaryQty: {
                            $sum: "$totalPrimaryQty"
                        },
                        opening: {
                            $sum: "$opening"
                        },
                        totalPrimaryAmt: {
                            $sum: "$totalPrimaryAmt"
                        },
                        totalSaleReturnQty: {
                            $sum: "$totalSaleReturnQty"
                        },
                        totalSaleReturnAmt: {
                            $sum: "$totalSaleReturnAmt"
                        },
                        totalSecondrySaleQty: {
                            $sum: "$totalSecondrySaleQty"
                        },
                        totalStock: {
                            $sum: "$totalStock"
                        },
                        totalStockValue: {
                            $sum: "$totalStockValue"
                        },
                        closing: {
                            $sum: "$closing"
                        },
                        closingAmt: {
                            $sum: "$closingAmt"
                        },
                        breakage: {
                            $sum: "$breakage"
                        },
                        breakageQty: {
                            $sum: "$breakageQty"
                        },
                        expiry: {
                            $sum: "$expiry"
                        },
                        expiryQty: {
                            $sum: "$expiryQty"
                        },
                        batchRecall: {
                            $sum: "$batchRecall"
                        },
                        batchRecallQty: {
                            $sum: "$batchRecallQty"
                        },

                    }
                },

                // Stage 5
                {
                    $project: {
                        productName: "$_id.productName",
                        productRate: "$_id.netRate",
                        partyName: { $arrayElemAt: ["$partyData", 0] },
                        partyNameDoctor: { $arrayElemAt: ["$partyDataDoctor", 0] },
                        productId: "$_id.productId",
                        opening: "$opening",
                        totalPrimaryQty: "$totalPrimaryQty",
                        totalPrimaryAmt: "$totalPrimaryAmt",
                        totalSaleReturnQty: "$totalSaleReturnQty",
                        totalSaleReturnAmt: "$totalSaleReturnAmt",
                        secondarySaleQty: "$totalSecondrySaleQty",
                        secondarySaleValue: "$totalSecondrySaleAmt",
                        totalStockValue: { $multiply: [{ $subtract: [{ $add: ["$opening", "$totalPrimaryQty"] }, "$totalSaleReturnQty"] }, "$_id.netRate"] },
                        totalStock: {
                            $subtract: [{ $add: ["$opening", "$totalPrimaryQty"] }, "$totalSaleReturnQty"]
                        },
                        closing: "$closing",
                        closingvalue: "$closingAmt",
                        breakage: "$breakage",
                        breakageQty: "$breakageQty",
                        expiry: "$expiry",
                        expiryQty: "$expiryQty",
                        batchRecall: "$batchRecall",
                        batchRecallQty: "$batchRecallQty"
                    }
                },
                function (err, data) {
                    const SSSViewFormula = param.SSSViewFormula;
                    const total = [];

                    if (err) {
                        return cb(err)
                    }
                    let
                        totalSaleReturnQtySum = 0,
                        totalSaleReturnAmtSum = 0,
                        expiryQtySum = 0,
                        expirySum = 0,
                        openingQtySum = 0,
                        openingSum = 0,
                        breakageQtySum = 0,
                        breakageSum = 0,
                        batchRecallQtySum = 0,
                        batchRecallSum = 0,
                        totalPrimaryQtySum = 0,
                        totalPrimaryAmtSum = 0,
                        closingSum = 0,
                        closingvalueSum = 0,
                        secondarySaleQtySum = 0,
                        secondarySaleValueSum = 0;
                    data.forEach(item => {
                        item["totalSaleReturnQty"] = item.totalSaleReturnQty * SSSViewFormula.salesReturn;
                        totalSaleReturnQtySum += item["totalSaleReturnQty"];
                        item["totalSaleReturnAmt"] = item.totalSaleReturnQty * item.productRate;
                        totalSaleReturnAmtSum += item["totalSaleReturnAmt"];
                        item["expiryQty"] = item.expiryQty * SSSViewFormula.expiry;
                        expiryQtySum += item["expiryQty"];
                        item["expiry"] = item.expiryQty * item.productRate;
                        expirySum += item["expiry"];
                        item["openingQty"] = item.opening;
                        openingQtySum += item["openingQty"]
                        item["opening"] = item.openingQty * item.productRate;
                        openingSum += item["opening"];
                        item["breakageQty"] = item.breakageQty * SSSViewFormula.breakage;
                        breakageQtySum += item["breakageQty"];
                        item["breakage"] = item.breakageQty * item.productRate;
                        breakageSum += item["breakage"];
                        item["batchRecallQty"] = item.batchRecallQty * SSSViewFormula.batchRecall;
                        batchRecallQtySum += item["batchRecallQty"]
                        item["batchRecall"] = item.batchRecallQty * item.productRate;
                        batchRecallSum += item["batchRecall"];
                        item["totalPrimaryQty"] = item.totalPrimaryQty;
                        totalPrimaryQtySum += item["totalPrimaryQty"]
                        item["totalPrimaryAmt"] = item.totalPrimaryQty * item.productRate;
                        totalPrimaryAmtSum += item["totalPrimaryAmt"]
                        item["closing"] = item.closing;
                        closingSum += item["closing"];
                        item["closingvalue"] = item.closing * item.productRate;
                        closingvalueSum += item["closingvalue"]
                        item["secondarySaleQty"] = item.totalPrimaryQty + item.openingQty - (item.totalSaleReturnQty + item.expiryQty + item.breakageQty + item.batchRecallQty + item.closing);
                        secondarySaleQtySum += item["secondarySaleQty"]
                        item["secondarySaleValue"] = item.secondarySaleQty * item.productRate;
                        secondarySaleValueSum += item["secondarySaleValue"];
                    })
                    total.push({
                        "totalSaleReturnQtySum": totalSaleReturnQtySum,
                        "totalSaleReturnAmtSum": totalSaleReturnAmtSum,
                        "expiryQtySum": expiryQtySum,
                        "expirySum": expirySum,
                        "openingQtySum": openingQtySum,
                        "openingSum": openingSum,
                        "breakageQtySum": breakageQtySum,
                        "breakageSum": breakageSum,
                        "batchRecallQtySum": batchRecallQtySum,
                        "batchRecallSum": batchRecallSum,
                        "totalPrimaryQtySum": totalPrimaryQtySum,
                        "totalPrimaryAmtSum": totalPrimaryAmtSum,
                        "closingSum": closingSum,
                        "closingvalueSum": closingvalueSum,
                        "secondarySaleQtySum": secondarySaleQtySum,
                        "secondarySaleValueSum": secondarySaleValueSum
                    });
                    // data.push(total);                    

                    return cb(false, data)
                }
            );
        }
    }
    Primaryandsalereturn.remoteMethod(
        'getSubmittedSSSSalePerformanceWise', {
        description: 'getSubmittedSSSSalePerformanceWise',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: 'body' }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );




    //updated by Rahul Choudhary 06-05-2020
    Primaryandsalereturn.getProductAndSSSDetailForSingleUser = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        var match2 = {
            companyId: ObjectId(param.companyId),
            // userId: ObjectId(param.sssClosingInfo.userId),
            partyId: ObjectId(param.sssClosingInfo.stockistId.stkId)


        }
        if (param.sssClosingInfo.month.monthValue == 1) {
            match2.month = 12;
            match2.year = parseInt(param.sssClosingInfo.year) - 1
        } else {
            match2.month = parseInt(param.sssClosingInfo.month),
                match2.year = parseInt(param.sssClosingInfo.year)
        }
        match = {
            companyId: ObjectId(param.companyId),
            //userId: ObjectId(param.sssClosingInfo.userId),
            partyId: ObjectId(param.sssClosingInfo.stockistId.stkId),
            month: parseInt(param.sssClosingInfo.month),
            year: parseInt(param.sssClosingInfo.year)

        }


        PrimaryInfoCollection.aggregate({
            $match: match
        }, function (err, singleUserSSS) {
            if (singleUserSSS.length != 0) {
                PrimaryInfoCollection.aggregate({
                    $match: match
                },
                    function (err, previousSSResult) {
                        let stateId = [];
                        if (param.sssClosingInfo.stateId == undefined) {
                            stateId.push(param.stateId);
                        } else {
                            stateId.push(param.sssClosingInfo.stateId);
                        }
                        Primaryandsalereturn.app.models.Products.find({
                            where: {
                                companyId: param.companyId,
                                assignedStates: { inq: stateId },
                                status: true
                            }
                        }, function (err, response) {
                            let data = [];
                            for (let i = 0; i < response.length; i++) {
                                var filterObj1 = previousSSResult.filter(function (obj) {
                                    return obj.productId == response[i].id.toString();
                                });

                                if (filterObj1.length > 0) {
                                    data.push({
                                        productName: filterObj1[0].productName,
                                        productType: response[i].productType,
                                        productId: filterObj1[0].productId,
                                        netRate: filterObj1[0].netRate,
                                        splRate: filterObj1[0].splRate,
                                        opening: filterObj1[0].opening,
                                        primarySale: filterObj1[0].totalPrimaryQty,
                                        returnSale: filterObj1[0].totalSaleReturnQty,
                                        breakage: filterObj1[0].breakage,
                                        saleable: filterObj1[0].saleable,
                                        other: filterObj1[0].other,
                                        closing: filterObj1[0].closing,
                                    });
                                } else {
                                    if (param.stkAmtCal == undefined) {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else if (param.stkAmtCal == "mrp") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].mrp,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });

                                    } else if (param.stkAmtCal == "ptr") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptr,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else if (param.stkAmtCal == "ptw") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    }
                                    // console.log(data)

                                }
                            }
                            return cb(false, data);
                        })

                    });

                // return cb(false,[]);
            } else {

                PrimaryInfoCollection.aggregate({
                    $match: match2
                },
                    function (err, previousSSResult) {
                        let stateId = [];
                        if (param.sssClosingInfo.stateId == undefined) {
                            stateId.push(param.stateId);
                        } else {
                            stateId.push(param.sssClosingInfo.stateId);
                        }

                        Primaryandsalereturn.app.models.Products.find({
                            where: {
                                companyId: param.companyId,
                                assignedStates: { inq: stateId },
                                status: true
                            }
                        }, function (err, response) {
                            let data = [];
                            for (let i = 0; i < response.length; i++) {
                                var filterObj1 = previousSSResult.filter(function (obj) {
                                    return obj.productId == response[i].id.toString();
                                });

                                if (filterObj1.length > 0) {
                                    data.push({
                                        productName: filterObj1[0].productName,
                                        productType: response[i].productType,
                                        productId: filterObj1[0].productId,
                                        netRate: filterObj1[0].netRate,
                                        splRate: 0,
                                        opening: filterObj1[0].closing,
                                        primarySale: 0,
                                        returnSale: 0,
                                        breakage: 0,
                                        saleable: 0,
                                        other: 0,
                                        closing: 0
                                    });
                                } else {

                                    if (param.stkAmtCal == undefined) {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else if (param.stkAmtCal == "mrp") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].mrp,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });

                                    } else if (param.stkAmtCal == "ptr") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptr,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else if (param.stkAmtCal == "ptw") {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    } else {
                                        data.push({
                                            productName: response[i].productName,
                                            productType: response[i].productType,
                                            productId: response[i].id,
                                            netRate: response[i].ptw,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            returnSale: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            closing: 0
                                        });
                                    }
                                    // console.log(data)

                                }
                            }
                            return cb(false, data);
                        })

                    });
            }

        });



    };

    Primaryandsalereturn.remoteMethod(
        'getProductAndSSSDetailForSingleUser', {
        description: 'getProductAndSSSDetailForSingleUser',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    /*Primaryandsalereturn.submitSingleUserSSSDetail = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        //console.log("param=",param)

       for (let i = 0; i < param.sssClosingInfo.length; i++) {
            let rate = 0;
            if (param.sssClosingInfo[i].splRate == '' || param.sssClosingInfo[i].splRate == undefined || param.sssClosingInfo[i].splRate == null) {
                rate = param.sssClosingInfo[i].netRate
            } else {
                rate = param.sssClosingInfo[i].splRate
            }
            PrimaryInfoCollection.aggregate({
                $match: {
                    companyId: ObjectId(param.companyId),
                    productId: ObjectId(param.sssClosingInfo[i].productId),
                    userId: ObjectId(param.formInfo.userId),
                    partyId: ObjectId(param.formInfo.stockistId.stkId),
                    month: parseInt(param.formInfo.month.monthValue),
                    year: parseInt(param.formInfo.year)
                }
            },
                function (err, checkProductExist) {
                    if (checkProductExist.length > 0) {
                        var openingPlusPrimary = param.sssClosingInfo[i].opening + param.sssClosingInfo[i].primarySale;
                        var saleReturn = param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other;
                        var closing = param.sssClosingInfo[i].closing;
                        var finalSecondarySaleQty =  Math.round(openingPlusPrimary - saleReturn - closing);
                        PrimaryInfoCollection.update({
                            companyId: ObjectId(param.companyId),
                            userId: ObjectId(param.formInfo.userId),
                            partyId: ObjectId(param.formInfo.stockistId.stkId),
                            productId: ObjectId(param.sssClosingInfo[i].productId),
                            month: parseInt(param.formInfo.month.monthValue),
                            year: parseInt(param.formInfo.year)
                        }, {
                                $set: {
                                    "splRate": param.sssClosingInfo[i].splRate,
                                    "opening": param.sssClosingInfo[i].opening,
                                    "totalPrimaryQty": param.sssClosingInfo[i].primarySale,
                                    "totalPrimaryAmt": (param.sssClosingInfo[i].primarySale * rate),
                                    "totalSaleReturnQty": (param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other),
                                    "totalSaleReturnAmt": (param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other) * (rate),
                                    "totalSecondrySaleQty": finalSecondarySaleQty,
                                    "totalSecondrySaleAmt": finalSecondarySaleQty * (rate),
                                    "breakage": param.sssClosingInfo[i].breakage,
                                    "saleable": param.sssClosingInfo[i].saleable,
                                    "other": param.sssClosingInfo[i].other,
                                    "closing": param.sssClosingInfo[i].closing,
                                    "closingAmt": (param.sssClosingInfo[i].closing) * (rate),
                                    "userId": ObjectId(param.formInfo.userId),
                                    "updatedAt": new Date()
                                }
                            }, {
                                multi: false
                            },
                            function (err, res) {
                                if (err) {
                                    //return cb(false, err);
                                }
                                if (res) {
                                    console.log("updated result=",res)
                                    var lastResult = [];
                                    lastResult.push({
                                        "count": res.result.nModified
                                    });
                                }
                            });
                    } else {
                        var openingPlusPrimary = param.sssClosingInfo[i].opening + param.sssClosingInfo[i].primarySale;
                        var saleReturn = param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other;
                        var closing = param.sssClosingInfo[i].closing;
                        var finalSecondarySaleQty =  Math.round(openingPlusPrimary - saleReturn - closing);
                        var newObjforSingleUser = {
                            "companyId": ObjectId(param.companyId),
                            "productId": ObjectId(param.sssClosingInfo[i].productId),
                            "productName": param.sssClosingInfo[i].productName,
                            "netRate": param.sssClosingInfo[i].netRate,
                            "splRate": param.sssClosingInfo[i].splRate,
                            "productCode": param.sssClosingInfo[i].productName,
                            "partyName": param.formInfo.stockistId.stkName,
                            "partyId": ObjectId(param.formInfo.stockistId.stkId),
                            "partyCode": param.formInfo.stockistId.stkName,
                            "stateId": ObjectId(param.formInfo.stateId),
                            "districtId": ObjectId(param.formInfo.districtId),
                            "data": [],
                            "month": parseInt(param.formInfo.month.monthValue),
                            "year": parseInt(param.formInfo.year),
                            "opening": param.sssClosingInfo[i].opening,
                            "totalPrimaryQty": param.sssClosingInfo[i].primarySale,
                            "totalPrimaryAmt": (param.sssClosingInfo[i].primarySale * rate),
                            "totalSaleReturnQty": (param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other),
                            "totalSaleReturnAmt": (param.sssClosingInfo[i].breakage + param.sssClosingInfo[i].saleable + param.sssClosingInfo[i].other) * (rate),
                            "totalSecondrySaleQty": finalSecondarySaleQty,
                            "totalSecondrySaleAmt": finalSecondarySaleQty * (rate),
                            "breakage": param.sssClosingInfo[i].breakage,
                            "saleable": param.sssClosingInfo[i].saleable,
                            "other": param.sssClosingInfo[i].other,
                            "closing": param.sssClosingInfo[i].closing,
                            "closingAmt": (param.sssClosingInfo[i].closing) * (rate),
                            "userId": ObjectId(param.formInfo.userId),
                            "createdAt": new Date()
                        }
                        console.log(newObjforSingleUser);
                        PrimaryInfoCollection.insertOne(newObjforSingleUser, function (err, result) {
                          console.log("inserted result=",result);
                        });
                    }
                });

        }
        var res = [];
        res.push({
            "status": 'Ok'
        });
        return cb(false, res);
    };

    Primaryandsalereturn.remoteMethod(
        'submitSingleUserSSSDetail', {
            description: 'submitSingleUserSSSDetail',
            accepts: [{
                arg: 'param',
                type: 'object',
                http: {source: 'body'}
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );*/

    Primaryandsalereturn.submitSingleUserSSSDetail = function (param, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        /* let primaryCount = 0

        for (let i = 0; i < param.sssClosingInfo.length; i++) {
            primaryCount = primaryCount + param.sssClosingInfo[i].primarySale;
        }
        if (primaryCount == 0) {
            var res = [];
            res.push({
                "status": 'Zero'
            });

            return cb(false, res)

        } */
        for (let i = 0; i < param.sssClosingInfo.length; i++) {
            let rate = 0;
            if (param.sssClosingInfo[i].splRate == "0" || param.sssClosingInfo[i].splRate == '' || param.sssClosingInfo[i].splRate == undefined || param.sssClosingInfo[i].splRate == null) {
                rate = param.sssClosingInfo[i].netRate
            } else {
                rate = param.sssClosingInfo[i].splRate
            }
            PrimaryInfoCollection.aggregate({
                $match: {
                    companyId: ObjectId(param.companyId),
                    productId: ObjectId(param.sssClosingInfo[i].productId),
                    //userId: ObjectId(param.formInfo.userId),
                    partyId: ObjectId(param.formInfo.stockistId.stkId),
                    districtId: ObjectId(param.districtId),
                    month: parseInt(param.formInfo.month),
                    year: parseInt(param.formInfo.year)
                }
            },
                function (err, checkProductExist) {
                    if (checkProductExist.length > 0) {

                        var openingPlusPrimary = param.sssClosingInfo[i].opening + param.sssClosingInfo[i].primarySale + param.sssClosingInfo[i].other;
                        var saleReturn = param.sssClosingInfo[i].breakageQty + param.sssClosingInfo[i].returnSale + param.sssClosingInfo[i].expiryQty + param.sssClosingInfo[i].batchRecallQty;
                        var closing = param.sssClosingInfo[i].closing;
                        var finalSecondarySaleQty = Math.round(openingPlusPrimary - saleReturn - closing)

                        PrimaryInfoCollection.update({
                            companyId: ObjectId(param.companyId),
                            userId: ObjectId(param.userId),
                            partyId: ObjectId(param.formInfo.stockistId.stkId),
                            productId: ObjectId(param.sssClosingInfo[i].productId),
                            month: parseInt(param.formInfo.month),
                            year: parseInt(param.formInfo.year)
                        }, {
                            $set: {
                                "opening": param.sssClosingInfo[i].opening,
                                "breakageQty": param.sssClosingInfo[i].breakageQty,
                                "expiryQty": param.sssClosingInfo[i].expiryQty,
                                "batchRecallQty": param.sssClosingInfo[i].batchRecallQty,
                                "totalPrimaryQty": param.sssClosingInfo[i].primarySale,
                                "totalPrimaryAmt": (param.sssClosingInfo[i].primarySale * rate),
                                "totalSaleReturnQty": param.sssClosingInfo[i].returnSale,
                                "totalSaleReturnAmt": (param.sssClosingInfo[i].returnSale) * (rate),
                                "totalSecondrySaleQty": finalSecondarySaleQty,
                                "totalSecondrySaleAmt": finalSecondarySaleQty * (rate),
                                //"breakage": param.sssClosingInfo[i].breakage,
                                "saleable": param.sssClosingInfo[i].saleable,
                                "other": param.sssClosingInfo[i].other,
                                "closing": param.sssClosingInfo[i].closing,
                                "closingAmt": (param.sssClosingInfo[i].closing) * (rate),
                            }
                        }, {
                            multi: false
                        },
                            function (err, res) {
                                if (err) {
                                    //return cb(false, err);
                                }
                                if (res) {
                                    var lastResult = [];
                                    lastResult.push({
                                        "count": res.result.nModified
                                    });
                                }
                            });
                    } else {
                        var openingPlusPrimary = param.sssClosingInfo[i].opening + param.sssClosingInfo[i].primarySale + param.sssClosingInfo[i].other;
                        var saleReturn = param.sssClosingInfo[i].breakageQty + param.sssClosingInfo[i].returnSale + param.sssClosingInfo[i].expiryQty + param.sssClosingInfo[i].batchRecallQty;
                        var closing = param.sssClosingInfo[i].closing;
                        var finalSecondarySaleQty = Math.round(openingPlusPrimary - saleReturn - closing);

                        var newObjforSingleUser = {
                            "companyId": ObjectId(param.companyId),
                            "productId": ObjectId(param.sssClosingInfo[i].productId),
                            "productName": param.sssClosingInfo[i].productName,
                            "netRate": param.sssClosingInfo[i].netRate,
                            "splRate": param.sssClosingInfo[i].splRate,
                            "productCode": param.sssClosingInfo[i].productName,
                            "partyName": param.formInfo.stockistId.stkName,
                            "partyId": ObjectId(param.formInfo.stockistId.stkId),
                            "partyCode": param.formInfo.stockistId.stkName,
                            "stateId": ObjectId(param.stateId),
                            "districtId": ObjectId(param.districtId),
                            "data": [],
                            "month": parseInt(param.formInfo.month),
                            "year": parseInt(param.formInfo.year),
                            "opening": (param.sssClosingInfo[i].opening + param.sssClosingInfo[i].other),
                            "breakageQty": param.sssClosingInfo[i].breakageQty,
                            "expiryQty": param.sssClosingInfo[i].expiryQty,
                            "batchRecallQty": param.sssClosingInfo[i].batchRecallQty,
                            "totalPrimaryQty": param.sssClosingInfo[i].primarySale,
                            "totalPrimaryAmt": (param.sssClosingInfo[i].primarySale * rate),
                            "totalSaleReturnQty": param.sssClosingInfo[i].returnSale,
                            "totalSaleReturnAmt": (param.sssClosingInfo[i].returnSale) * (rate),
                            "totalSecondrySaleQty": finalSecondarySaleQty,
                            "totalSecondrySaleAmt": finalSecondarySaleQty * (rate),
                            //"breakage": param.sssClosingInfo[i].breakage,
                            "saleable": param.sssClosingInfo[i].saleable,
                            "other": param.sssClosingInfo[i].other,
                            "closing": param.sssClosingInfo[i].closing,
                            "closingAmt": (param.sssClosingInfo[i].closing) * (rate),
                            "userId": ObjectId(param.userId),
                            "createdAt": new Date()
                        }

                        PrimaryInfoCollection.insertOne(newObjforSingleUser, function (err, result) {

                        });
                    }
                });

        }

        var res = [];
        res.push({
            "status": 'Ok'
        });

        return cb(false, res);

    }

    Primaryandsalereturn.remoteMethod(
        'submitSingleUserSSSDetail', {
        description: 'submitSingleUserSSSDetail',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: 'body' }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );

    /*Primaryandsalereturn.getProductAndSSSDetailForSingleUser = function (param, cb) {
       var self = this;
       var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
       var match2={
               companyId: ObjectId(param.companyId),
               //userId: ObjectId(param.sssClosingInfo.userId),
               partyId: ObjectId(param.sssClosingInfo.stockistId.stkId),
               districtId: ObjectId(param.sssClosingInfo.stockistId.districtId),
               month: parseInt(param.sssClosingInfo.month.monthValue)-1,
               year: parseInt(param.sssClosingInfo.year) 
        }
        match={
               companyId: ObjectId(param.companyId),
              // userId: ObjectId(param.sssClosingInfo.userId),
               partyId: ObjectId(param.sssClosingInfo.stockistId.stkId),
               month: parseInt(param.sssClosingInfo.month.monthValue),
               year: parseInt(param.sssClosingInfo.year),
               districtId: ObjectId(param.sssClosingInfo.stockistId.districtId),

        }
        
       PrimaryInfoCollection.aggregate({
           $match: match
       },function (err, singleUserSSS) {
            
              if(singleUserSSS.length==0){
                  PrimaryInfoCollection.aggregate({
                       $match: match
                   },
                       function (err, previousSSResult) {
                       if(previousSSResult.length ==0){
                         let stateId = [];
                         let divisionId=[];
                         stateId.push(param.sssClosingInfo.stateId);
                         let where={
                           companyId: param.companyId,
                           assignedStates: { inq: stateId },
                       }
                         if(param.sssClosingInfo.hasOwnProperty("divisionId")){
                           divisionId.push(param.sssClosingInfo.divisionId);
                           //where.assignedDivision={ inq: divisionId }
                           // where.assignedDivision= param.sssClosingInfo.divisionId
                          }
                         console.log("=================================")
                         console.log(where)
                         console.log("=================================")
                         Primaryandsalereturn.app.models.Products.find({
                             where: where
                         }, function (err, response) {
                         let data = [];
                         for (let i = 0; i < response.length; i++) {
                             var filterObj1 = previousSSResult.filter(function (obj) {
                                 return obj.productId == response[i].id.toString();
                             });
     
                             if (filterObj1.length > 0) {
                                 data.push({
                                     productName: filterObj1[0].productName,
                                     productType: response[i].productType,
                                     productId: filterObj1[0].productId,
                                     netRate: filterObj1[0].netRate,
                                     splRate: filterObj1[0].splRate,
                                     opening: filterObj1[0].opening,
                                     primarySale: filterObj1[0].totalPrimaryQty,
                                     returnSale: filterObj1[0].totalSaleReturnQty,
                                     breakage: filterObj1[0].breakage,
                                     saleable: filterObj1[0].saleable,
                                     other: filterObj1[0].other,
                                     closing: filterObj1[0].closing,
                                 });
                             } else {
                                 if (param.stkAmtCal == undefined) {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else if (param.stkAmtCal == "mrp") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].mrp,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
     
                                 } else if (param.stkAmtCal == "ptr") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptr,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else if (param.stkAmtCal == "ptw") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 }
                                // console.log(data)
     
                             }
                         }
                         return cb(false, data);
                       })
                   }
                   else{
                       return cb(false, []);
                   }
                 /////----------
               });
                  
                  // return cb(false,[]);
              }else{
                  
                   PrimaryInfoCollection.aggregate({
                       $match: match2
                   },
                       function (err, previousSSResult) {
                         let stateId = [];
                         let divisionId=[];
                         stateId.push(param.sssClosingInfo.stateId);

                         let where={
                           companyId: param.companyId,
                           assignedStates: { inq: stateId },
                         }
                         
                         if(param.sssClosingInfo.hasOwnProperty("divisionId")){
                           divisionId.push(param.sssClosingInfo.divisionId);
                           //where.assignedDivision={ inq: divisionId }
                           where.assignedDivision=param.sssClosingInfo.divisionId;//{ inq: divisionId }
                          }
                           console.log("===========ELSE======================")
                         console.log(where)
                         console.log("=================================")
                        
                         Primaryandsalereturn.app.models.Products.find({
                             where: where
                         }, function (err, response) {
                             
                         let data = [];
                         for (let i = 0; i < response.length; i++) {
                             var filterObj1 = previousSSResult.filter(function (obj) {
                                 return obj.productId == response[i].id.toString();
                             });
     
                             if (filterObj1.length > 0) {
                                 data.push({
                                     productName: filterObj1[0].productName,
                                     productType: response[i].productType,
                                     productId: filterObj1[0].productId,
                                     netRate: filterObj1[0].netRate,
                                     splRate: 0,
                                     opening: filterObj1[0].closing,
                                     primarySale: 0,
                                     returnSale: 0,
                                     breakage: 0,
                                     saleable: 0,
                                     other: 0,
                                     closing: 0
                                 });
                             } else {
     
                                 if (param.stkAmtCal == undefined) {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else if (param.stkAmtCal == "mrp") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].mrp,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
     
                                 } else if (param.stkAmtCal == "ptr") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptr,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else if (param.stkAmtCal == "ptw") {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 } else {
                                     data.push({
                                         productName: response[i].productName,
                                         productType: response[i].productType,
                                         productId: response[i].id,
                                         netRate: response[i].ptw,
                                         splRate: 0,
                                         opening: 0,
                                         primarySale: 0,
                                         returnSale: 0,
                                         breakage: 0,
                                         saleable: 0,
                                         other: 0,
                                         closing: 0
                                     });
                                 }
                                // console.log(data)
     
                             }
                         }
                         return cb(false, data);
                       })
                 
               });
               } // end of else ---goes no record found---- there
              
           });

           

   };

   Primaryandsalereturn.remoteMethod(
       'getProductAndSSSDetailForSingleUser', {
           description: 'getProductAndSSSDetailForSingleUser',
           accepts: [{
               arg: 'param',
               type: 'object'
           }],
           returns: {
               root: true,
               type: 'array'
           },
           http: {
               verb: 'get'
           }
       }
   );*/


    //-----------------------Praveen Kumar(12-03-2019)------------------------------------------
    Primaryandsalereturn.targetVsAchievementGraph = function (param, cb) {

        var self = this;
        let SateCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.State.modelName);

        let DistrictCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.District.modelName);

        let PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);

        let TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);

        if (param.type == "state") {

            let matchForState = {};

            if (param.designationLevel == 0) {
                if (param.lookingFor == "all") {
                    matchForState = {
                        assignedTo: ObjectId(param.companyId)
                    }
                } else if (param.lookingFor == "selected") {
                    stateIds = [];
                    for (let i = 0; i < param.stateIds.length; i++) {
                        stateIds[i] = ObjectId(param.stateIds[i]);
                    }
                    matchForState = {
                        assignedTo: ObjectId(param.companyId),
                        _id: {
                            $in: stateIds
                        }
                    }
                }
                SateCollection.aggregate({
                    $match: matchForState
                }, {
                    $sort: {
                        stateName: 1
                    }
                }, {
                    $group: {
                        _id: {},
                        stateIds: { $push: "$_id" },
                        stateInfo: {
                            $push: {
                                "id": "$_id",
                                "stateName": "$stateName"
                            }
                        }
                    }
                }, function (err, stateResult) {
                    if (err) {
                        console.log(err);
                    }
                    // console.log({
                    //     companyId: ObjectId(param.companyId),
                    //     month: param.month,
                    //     year: param.year,
                    //     stateId: {
                    //         $in: stateResult[0].stateIds
                    //     }
                    // });

                    async.parallel({
                        primaryAndSecondory: function (cb) {
                            PrimaryandsalereturnCollection.aggregate({
                                $match: {
                                    companyId: ObjectId(param.companyId),
                                    month: param.month,
                                    year: param.year,
                                    stateId: {
                                        $in: stateResult[0].stateIds
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {
                                        stateId: "$stateId"
                                    },
                                    totalPrimary: {
                                        $sum: "$totalPrimaryAmt"
                                    },
                                    totalSecondarySale: {
                                        $sum: "$totalSecondrySaleAmt"
                                    }
                                }
                            }, {
                                $project: {
                                    _id: 0,
                                    stateId: "$_id.stateId",
                                    totalPrimarySale: "$totalPrimary",
                                    totalSecondarySale: "$totalSecondarySale" //{ $subtract: ["$totalPrimary", "$totalSaleReturn"] }
                                }
                            }, function (err, result) {
                                if (err) {
                                    console.log(err)
                                }
                                //console.log(result)
                                cb(false, result);
                            });

                        },
                        target: function (cb) {

                            TargetCollection.aggregate({
                                $match: {
                                    companyId: ObjectId(param.companyId),
                                    targetYear: param.year
                                }
                            }, {
                                $group: {
                                    _id: {
                                        durationType: "$durationType"
                                    }
                                }
                            }, function (err, result) {
                                //console.log("Target Result",result)

                                if (err) {
                                    console.log(err);
                                }
                                if (result.length > 0) {
                                    let matchForTarget = {};
                                    let groupForTarget = {};
                                    let projectForTarget = {};
                                    let foundDurationType_Yearly = result.filter(function (obj) {
                                        return obj._id.durationType == "Yearly";
                                    });
                                    if (foundDurationType_Yearly.length > 0) {
                                        matchForTarget = {
                                            companyId: ObjectId(param.companyId),
                                            targetYear: param.year,

                                        };
                                        groupForTarget = {
                                            _id: {
                                                stateId: "$stateId"
                                            },
                                            total: {
                                                $sum: "$targetQuantity"
                                            }
                                        };
                                        projectForTarget = {
                                            _id: 0,
                                            stateId: "$_id.stateId",
                                            tagetForMonth: {
                                                $divide: ["$total", 12]
                                            }
                                        };
                                    } else {
                                        let foundDurationType_MonthlyOrQuarterly = result.filter(function (obj) {
                                            return obj._id.durationType == "Monthly" || obj._id.durationType == "Quarterly";
                                        });

                                        if (foundDurationType_MonthlyOrQuarterly.length > 0) {
                                            matchForTarget = {
                                                companyId: ObjectId(param.companyId),
                                                targetYear: param.year,
                                                targetMonth: param.month

                                            };
                                            groupForTarget = {
                                                _id: {
                                                    stateId: "$stateId"
                                                },
                                                total: {
                                                    $sum: "$targetQuantity"
                                                }
                                            };
                                            projectForTarget = {
                                                _id: 0,
                                                stateId: "$_id.stateId",
                                                tagetForMonth: "$total"
                                            };
                                        } else {

                                        }
                                    }
                                    TargetCollection.aggregate({
                                        $match: matchForTarget
                                    }, {
                                        $group: groupForTarget
                                    }, {
                                        $project: projectForTarget
                                    }, function (err, targetResult) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        cb(false, targetResult)
                                    });


                                } else {
                                    cb(false, result);

                                }

                            });




                        }
                    }, function (err, asyncResult) {
                        if (err) {
                            console.log(err)
                        }
                        let finalResult = [];
                        //console.log("PKK",asyncResult);
                        for (let i = 0; i < stateResult[0].stateInfo.length; i++) {
                            // console.log(stateResult[0].stateInfo[i].stateName);
                            let tempData = {
                                stateId: stateResult[0].stateInfo[i].id,
                                stateName: stateResult[0].stateInfo[i].stateName,
                                target: 0,
                                primary: 0,
                                secondary: 0
                            };
                            let foundPrimaryAndSecondory = asyncResult.primaryAndSecondory.filter(function (obj) {
                                return obj.stateId.toString() == stateResult[0].stateInfo[i].id.toString();
                            });
                            if (foundPrimaryAndSecondory.length > 0) {
                                tempData.secondary = foundPrimaryAndSecondory[0].totalPrimarySale;
                                tempData.primary = foundPrimaryAndSecondory[0].totalSecondarySale;
                            }

                            let foundTarget = asyncResult.target.filter(function (obj) {
                                return obj.stateId.toString() == stateResult[0].stateInfo[i].id.toString();
                            });
                            if (foundTarget.length > 0) {
                                tempData.target = Math.round(foundTarget[0].tagetForMonth).toFixed(2);
                            }

                            finalResult.push(tempData);
                        }

                        cb(false, finalResult)
                    });

                });

            } else if (param.designationLevel > 1) {
                let passingObj = {
                    companyId: param.companyId,
                    supervisorId: param.userId,
                    isDivisionExist: param.isDivisionExist,
                    division: [] //will be update
                }
                Primaryandsalereturn.app.models.UserInfo.getUniqueStateOnManager(passingObj, function (err, result) {
                    if (err) {
                        return false;
                    }
                    if (result.length > 0) {

                        SateCollection.aggregate({
                            $match: {
                                assignedTo: ObjectId(param.companyId),
                                _id: {
                                    $in: result[0].stateIds
                                }
                            }
                        }, {
                            $sort: {
                                stateName: 1
                            }
                        }, {
                            $group: {
                                _id: {},
                                stateIds: { $push: "$_id" },
                                stateInfo: {
                                    $push: {
                                        "id": "$_id",
                                        "stateName": "$stateName"
                                    }
                                }
                            }
                        }, function (err, stateResult) {
                            if (err) {
                                console.log(err);
                            }
                            // console.log({
                            //     companyId: ObjectId(param.companyId),
                            //     month: param.month,
                            //     year: param.year,
                            //     stateId: {
                            //         $in: stateResult[0].stateIds
                            //     }
                            // });

                            async.parallel({
                                primaryAndSecondory: function (cb) {
                                    PrimaryandsalereturnCollection.aggregate({
                                        $match: {
                                            companyId: ObjectId(param.companyId),
                                            month: param.month,
                                            year: param.year,
                                            stateId: {
                                                $in: stateResult[0].stateIds
                                            }
                                        }
                                    }, {
                                        $group: {
                                            _id: {
                                                stateId: "$stateId"
                                            },
                                            totalPrimary: {
                                                $sum: "$totalPrimaryAmt"
                                            },
                                            totalSaleReturn: {
                                                $sum: "$totalSaleReturnAmt"
                                            }
                                        }
                                    }, {
                                        $project: {
                                            _id: 0,
                                            stateId: "$_id.stateId",
                                            totalPrimarySale: "$totalPrimary",
                                            totalSecondarySale: { $subtract: ["$totalPrimary", "$totalSaleReturn"] }
                                        }
                                    }, function (err, result) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        //console.log(result)
                                        cb(false, result);
                                    });

                                },
                                target: function (cb) {

                                    TargetCollection.aggregate({
                                        $match: {
                                            companyId: ObjectId(param.companyId),
                                            targetYear: param.year
                                        }
                                    }, {
                                        $group: {
                                            _id: {
                                                durationType: "$durationType"
                                            }
                                        }
                                    }, function (err, result) {
                                        //console.log("Target Result",result)

                                        if (err) {
                                            console.log(err);
                                        }
                                        if (result.length > 0) {
                                            let matchForTarget = {};
                                            let groupForTarget = {};
                                            let projectForTarget = {};
                                            let foundDurationType_Yearly = result.filter(function (obj) {
                                                return obj._id.durationType == "Yearly";
                                            });
                                            if (foundDurationType_Yearly.length > 0) {
                                                matchForTarget = {
                                                    companyId: ObjectId(param.companyId),
                                                    targetYear: param.year,

                                                };
                                                groupForTarget = {
                                                    _id: {
                                                        stateId: "$stateId"
                                                    },
                                                    total: {
                                                        $sum: "$targetQuantity"
                                                    }
                                                };
                                                projectForTarget = {
                                                    _id: 0,
                                                    stateId: "$_id.stateId",
                                                    tagetForMonth: {
                                                        $divide: ["$total", 12]
                                                    }
                                                };
                                            } else {
                                                let foundDurationType_MonthlyOrQuarterly = result.filter(function (obj) {
                                                    return obj._id.durationType == "Monthly" || obj._id.durationType == "Quarterly";
                                                });

                                                if (foundDurationType_MonthlyOrQuarterly.length > 0) {
                                                    matchForTarget = {
                                                        companyId: ObjectId(param.companyId),
                                                        targetYear: param.year,
                                                        targetMonth: param.month

                                                    };
                                                    groupForTarget = {
                                                        _id: {
                                                            stateId: "$stateId"
                                                        },
                                                        total: {
                                                            $sum: "$targetQuantity"
                                                        }
                                                    };
                                                    projectForTarget = {
                                                        _id: 0,
                                                        stateId: "$_id.stateId",
                                                        tagetForMonth: "$total"
                                                    };
                                                } else {

                                                }
                                            }
                                            TargetCollection.aggregate({
                                                $match: matchForTarget
                                            }, {
                                                $group: groupForTarget
                                            }, {
                                                $project: projectForTarget
                                            }, function (err, targetResult) {
                                                if (err) {
                                                    console.log(err)
                                                }
                                                cb(false, targetResult)
                                            });


                                        } else {
                                            cb(false, result);

                                        }

                                    });




                                }
                            }, function (err, asyncResult) {
                                if (err) {
                                    console.log(err)
                                }

                                let finalResult = [];
                                //console.log("PKK",asyncResult);
                                for (let i = 0; i < stateResult[0].stateInfo.length; i++) {
                                    // console.log(stateResult[0].stateInfo[i].stateName);
                                    let tempData = {
                                        stateId: stateResult[0].stateInfo[i].id,
                                        stateName: stateResult[0].stateInfo[i].stateName,
                                        target: 0,
                                        primary: 0,
                                        secondary: 0
                                    };
                                    let foundPrimaryAndSecondory = asyncResult.primaryAndSecondory.filter(function (obj) {
                                        return obj.stateId.toString() == stateResult[0].stateInfo[i].id.toString();
                                    });
                                    if (foundPrimaryAndSecondory.length > 0) {
                                        tempData.secondary = foundPrimaryAndSecondory[0].totalPrimarySale;
                                        tempData.primary = foundPrimaryAndSecondory[0].totalSecondarySale;
                                    }

                                    let foundTarget = asyncResult.target.filter(function (obj) {
                                        return obj.stateId.toString() == stateResult[0].stateInfo[i].id.toString();
                                    });
                                    if (foundTarget.length > 0) {
                                        tempData.target = Math.round(foundTarget[0].tagetForMonth).toFixed(2);
                                    }

                                    finalResult.push(tempData);
                                }
                                cb(false, finalResult)
                            });

                        });
                    } else {
                        cb(false, [])
                    }




                });
            }






        } else if (param.type == "headquarter") {
            let stateIds = [];
            for (let i = 0; i < param.stateIds.length; i++) {
                stateIds[i] = ObjectId(param.stateIds[i]);
            }
            let matchForDistrict = {};
            if (param.lookingFor == "all") {
                matchForDistrict = {
                    assignedTo: ObjectId(param.companyId),
                    stateId: {
                        $in: stateIds
                    }
                }
            } else if (param.lookingFor == "selected") {
                districtIds = [];
                for (let i = 0; i < param.districtIds.length; i++) {
                    districtIds[i] = ObjectId(param.districtIds[i]);
                }
                matchForDistrict = {
                    assignedTo: ObjectId(param.companyId),
                    _id: {
                        $in: districtIds
                    }
                }
            }
            DistrictCollection.aggregate({
                $match: matchForDistrict
            }, {
                $sort: {
                    districtName: 1
                }
            }, {
                $group: {
                    _id: {},
                    districtIds: { $push: "$_id" },
                    districtInfo: {
                        $push: {
                            "id": "$_id",
                            "districtName": "$districtName"
                        }
                    }
                }
            }, function (err, districtResult) {
                if (err) {
                    console.log(err);
                }
                var aMatch = {};
                if (districtResult.length > 0 && districtResult[0].districtIds != undefined) {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        month: param.month,
                        year: param.year,
                        districtId: {
                            $in: districtResult[0].districtIds
                        }
                    }
                } else {
                    aMatch = {
                        companyId: ObjectId(param.companyId),
                        month: param.month,
                        year: param.year,
                        districtId: {
                            $in: []
                        }
                    }
                }
                async.parallel({
                    primaryAndSecondory: function (cb) {
                        PrimaryandsalereturnCollection.aggregate({
                            $match: aMatch
                        }, {
                            $group: {
                                _id: {
                                    districtId: "$districtId"
                                },
                                totalPrimary: {
                                    $sum: "$totalPrimaryAmt"
                                },
                                totalSaleReturn: {
                                    $sum: "$totalSaleReturnAmt"
                                }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                districtId: "$_id.districtId",
                                totalPrimarySale: "$totalPrimary",
                                totalSecondarySale: { $subtract: ["$totalPrimary", "$totalSaleReturn"] }
                            }
                        }, function (err, result) {
                            if (err) {
                                console.log(err)
                            }
                            cb(false, result);
                        });

                    },
                    target: function (cb) {

                        TargetCollection.aggregate({
                            $match: {
                                companyId: ObjectId(param.companyId),
                                targetYear: param.year
                            }
                        }, {
                            $group: {
                                _id: {
                                    durationType: "$durationType"
                                }
                            }
                        }, function (err, result) {
                            // console.log(result)

                            if (err) {
                                console.log(err);
                            }
                            if (result.length > 0) {
                                let matchForTarget = {};
                                let groupForTarget = {};
                                let projectForTarget = {};
                                let foundDurationType_Yearly = result.filter(function (obj) {
                                    return obj._id.durationType == "Yearly";
                                });
                                if (foundDurationType_Yearly.length > 0) {
                                    matchForTarget = {
                                        companyId: ObjectId(param.companyId),
                                        targetYear: param.year,

                                    };
                                    groupForTarget = {
                                        _id: {
                                            districtId: "$districtId"
                                        },
                                        total: {
                                            $sum: "$targetQuantity"
                                        }
                                    };
                                    projectForTarget = {
                                        _id: 0,
                                        districtId: "$_id.districtId",
                                        tagetForMonth: {
                                            $divide: ["$total", 12]
                                        }
                                    };
                                } else {
                                    let foundDurationType_MonthlyOrQuarterly = result.filter(function (obj) {
                                        return obj._id.durationType == "Monthly" || obj._id.durationType == "Quarterly";
                                    });

                                    if (foundDurationType_MonthlyOrQuarterly.length > 0) {
                                        matchForTarget = {
                                            companyId: ObjectId(param.companyId),
                                            targetYear: param.year,
                                            targetMonth: param.month

                                        };
                                        groupForTarget = {
                                            _id: {
                                                districtId: "$districtId"
                                            },
                                            total: {
                                                $sum: "$targetQuantity"
                                            }
                                        };
                                        projectForTarget = {
                                            _id: 0,
                                            districtId: "$_id.districtId",
                                            tagetForMonth: "$total"
                                        };
                                    } else {

                                    }
                                }
                                TargetCollection.aggregate({
                                    $match: matchForTarget
                                }, {
                                    $group: groupForTarget
                                }, {
                                    $project: projectForTarget
                                }, function (err, targetResult) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    cb(false, targetResult)
                                });


                            } else {
                                cb(false, result);

                            }

                        });




                    }
                }, function (err, asyncResult) {
                    if (err) {
                        console.log(err)
                    }
                    let finalResult = [];
                    //console.log(stateResult[0].stateInfo[0].id);
                    if (districtResult.length > 0) {
                        for (let i = 0; i < districtResult[0].districtInfo.length; i++) {
                            // console.log(stateResult[0].stateInfo[i].stateName);
                            let tempData = {
                                districtId: districtResult[0].districtInfo[i].id,
                                districtName: districtResult[0].districtInfo[i].districtName,
                                target: 0,
                                primary: 0,
                                secondary: 0
                            };
                            let foundPrimaryAndSecondory = asyncResult.primaryAndSecondory.filter(function (obj) {
                                // console.log(obj.districtId+ "  ----  "+districtResult[0].districtInfo[i].id);
                                return obj.districtId.toString() == districtResult[0].districtInfo[i].id.toString();
                            });
                            if (foundPrimaryAndSecondory.length > 0) {
                                tempData.secondary = foundPrimaryAndSecondory[0].totalPrimarySale;
                                tempData.primary = foundPrimaryAndSecondory[0].totalSecondarySale;
                            }

                            let foundTarget = asyncResult.target.filter(function (obj) {
                                return obj.districtId.toString() == districtResult[0].districtInfo[i].id.toString();
                            });
                            if (foundTarget.length > 0) {
                                tempData.target = Math.round(foundTarget[0].tagetForMonth).toFixed(2);
                            }

                            finalResult.push(tempData);
                        }
                        cb(false, finalResult)
                    } else {
                        cb(false, finalResult)
                    }
                });

                //console.log(districtResult)
            });
        }
    };

    Primaryandsalereturn.remoteMethod(
        'targetVsAchievementGraph', {
        description: 'Target Vs Achievement Graph API',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //--------------------------------END-------------------------------------------------------
    //---------------HQ wise secondary sale report by preeti 18-12-2019-------------------------
    Primaryandsalereturn.HqWiseSecondaryReport = function (params, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let filter = {};
        let match = {}
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        let divisionArr = [];


        if (params.type == "State") {
            for (var i = 0; i < params.stateIds.length; i++) {
                stateIdsArr.push(ObjectId(params.stateIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { $in: stateIdsArr },
            }
        } else if (params.type == "Headquarter") {
            for (var i = 0; i < params.districtIds.length; i++) {
                hqIdArray.push(ObjectId(params.districtIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }

        }
        else if (params.type == "Employee Wise") {
            for (var i = 0; i < params.userIds.length; i++) {
                empArray.push(ObjectId(params.userIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { $in: empArray }
            }

        }
        const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
        let month = [];
        let year = [];

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);

        }
        filter.month = { $in: month }
        filter.year = { $in: year }

        PrimaryInfoCollection.aggregate(

            // Stage 1
            {
                $match: filter
            },

            // Stage 2
            {
                $group: {
                    _id: {
                        "stateId": "$stateId",
                        "districtId": "$districtId",
                        "month": "$month",
                        "year": "$year"
                    },
                    total: {
                        $sum: "$totalPrimaryAmt"

                    }
                }
            },

            // Stage 3
            {
                $lookup: {
                    "from": "State",
                    "localField": "_id.stateId",
                    "foreignField": "_id",
                    "as": "state"
                }
            },

            // Stage 4
            {
                $lookup: {
                    "from": "District",
                    "localField": "_id.districtId",
                    "foreignField": "_id",
                    "as": "district"
                }
            },

            // Stage 5
            {
                $project: {
                    month: "$_id.month",
                    year: "$_id.year",
                    districtName: { $arrayElemAt: ["$district.districtName", 0] },
                    stateName: { $arrayElemAt: ["$state.stateName", 0] },
                    districtId: { $arrayElemAt: ["$district._id", 0] },
                    stateId: { $arrayElemAt: ["$state._id", 0] },

                    total: 1
                }
            },
            {
                $group: {
                    _id: {
                        stateId: "$districtId",
                        districtId: "$stateId",
                        districtName: "$districtName",
                        stateName: "$stateName",
                        month: "$month",
                        year: "$year"

                    },
                    data: {
                        $push: {
                            month: "$month",
                            year: "$year",
                            total: "$total"

                        }
                    },
                    totalAmt: {
                        $sum: "$total"
                    }
                }
            },
            {
                $project: {
                    stateName: "$_id.stateName",
                    districtName: "$_id.districtName",
                    month: "$_id.month",
                    year: "$_id.year",
                    data: "$data",
                    totalAmt: 1
                }

            },
            {
                $group: {
                    _id: {
                        stateName: "$stateName",
                        districtName: "$districtName",
                    },
                    data: { $push: { month: "$month", year: "$year", total: { $arrayElemAt: ["$data.total", 0] } } },
                    totalAmt: {
                        $sum: "$totalAmt"
                    },
                }
            },
            function (err, SSSRes) {


                if (err) {
                    // console.log(err);
                    return cb(err);
                }
                if (SSSRes.length > 0) {
                    finalObject = [];
                    for (let i = 0; i < SSSRes.length; i++) {
                        let details = [];
                        for (const month of monthYearArray) {
                            const matchedObject = SSSRes[i].data.filter(function (obj) {
                                return obj.month == month.month && obj.year == month.year;
                            });

                            if (matchedObject.length > 0) {
                                details.push((matchedObject[0].total).toFixed(2))
                            } else {
                                details.push(0)
                            }
                        }
                        finalObject.push({
                            hqName: SSSRes[i]._id.districtName,
                            stateName: SSSRes[i]._id.stateName,
                            data: details,
                            totalAmt: (SSSRes[i].totalAmt).toFixed(2)
                        });
                    }

                    return cb(false, finalObject);

                } else {
                    return cb(false, [])
                }

            }

        )



    }
    Primaryandsalereturn.remoteMethod(
        'HqWiseSecondaryReport', {
        description: 'Target Vs Achievement Graph API',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );


    Primaryandsalereturn.getSecondarySalesStatus = function (param, cb) {

        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let amatch = {};
        let stateArr = [];
        let districtArr = [];
        let userIdArr = [];

        if (param.type == "State") {
            for (let i = 0; i < param.stateId.length; i++) {
                stateArr.push(ObjectId(param.stateId[i]));
            }
            amatch = {
                "companyId": ObjectId(param.companyId),
                "stateId": { $in: stateArr },
                "month": param.month,
                "year": param.year
            }

        } else if (param.type == "Headquarter") {
            for (let i = 0; i < param.stateId.length; i++) {
                stateArr.push(ObjectId(param.stateId[i]));
            }

            if (param.districtId != null) {
                if (param.districtId.length > 0) {
                    for (let j = 0; j < param.districtId.length; j++) {
                        districtArr.push(ObjectId(param.districtId[j]));
                    }
                }
            }
            amatch = {
                "companyId": ObjectId(param.companyId),
                "stateId": { $in: stateArr },
                "districtId": { $in: districtArr },
                "month": param.month,
                "year": param.year
            }
        } else if (param.type == "Employee Wise") {


            if (param.userId != null || param.userId != undefined) {
                if (param.userId.length > 0) {
                    for (let k = 0; k < param.userId.length; k++) {
                        userIdArr.push(ObjectId(param.userId[k]));
                    }
                }
            }
            amatch = {
                "companyId": ObjectId(param.companyId),
                "userId": { $in: userIdArr },
                "month": param.month,
                "year": param.year
            }
        }

        PrimaryInfoCollection.aggregate({
            $match: amatch
        },
            // Stage 2
            {
                $group: {
                    _id: {
                        userId: "$userId",
                        partyId: "$partyId",
                    },
                    createdAt: {
                        $addToSet: "$createdAt"
                    }
                }
            },

            // Stage 3
            {
                $lookup: {
                    "from": "Providers",
                    "localField": "_id.partyId",
                    "foreignField": "_id",
                    "as": "stockiestDetails"
                }
            },

            // Stage 4
            {
                $unwind: "$stockiestDetails"
            },

            // Stage 5
            {
                $lookup: {
                    "from": "UserInfo",
                    "localField": "stockiestDetails.userId",
                    "foreignField": "userId",
                    "as": "UserDetails"
                }
            },

            // Stage 6
            {
                $unwind: "$UserDetails"
            },
            {
                $project: {
                    _id: 1,
                    createdAt: 1,
                    stockiestDetails: 1,
                    UserDetails: 1,
                    divisionId: "$UserDetails.divisionId"
                }
            },

            // Stage 7
            {
                $unwind: {
                    path: "$divisionId",
                    preserveNullAndEmptyArrays: true
                }
            },

            // Stage 8
            {
                $lookup: {
                    "from": "DivisionMaster",
                    "localField": "divisionId",
                    "foreignField": "_id",
                    "as": "division"
                }
            },

            // Stage 9
            {
                $project: {
                    _id: 0,
                    userId: "$_id.userId",
                    divisionName: {
                        $arrayElemAt: ["$division.divisionName", 0]
                    },
                    divisionId: {
                        $arrayElemAt: ["$division._id", 0]
                    },
                    userName: "$UserDetails.name",
                    partyId: "$stockiestDetails._id",
                    partyName: "$stockiestDetails.providerName",
                    createdAt: {
                        $arrayElemAt: ["$createdAt", 0]
                    },
                    status: { $literal: "Yes" }


                }
            },
            async function (err, salesStatusResult) {
                if (err) {
                    console.log(err);
                    return cb(err);
                } else {
                    let match = {};
                    let finalObj = [];
                    if (param.type == "State") {
                        /*match = {
                            where:{
                                "companyId": param.companyId,
                                "stateId": { inq: stateArr },
                                 status:true,
                                 providerType:"Stockist"
                            },"include": [{
                                "relation": "user"
                              }]
                        }*/
                        match = {
                            "companyId": ObjectId(param.companyId),
                            "stateId": { $in: stateArr },
                            status: true,
                            providerType: "Stockist"
                        }

                    } else if (param.type == "Headquarter") {
                        /*match = {
                            where:{

                                "companyId": param.companyId,
                                "stateId": { inq: stateArr },
                                "districtId": { in: districtArr },
                                 status:true,
                                 providerType:"Stockist"
                            },"include": [{
                                "relation": "user"
                              }]*/
                        match = {
                            "companyId": ObjectId(param.companyId),
                            "stateId": { $in: stateArr },
                            "districtId": { $in: districtArr },
                            status: true,
                            providerType: "Stockist"
                        }
                        console.log("match ==> ", match)
                    } else if (param.type == "Employee Wise") {
                        /*match = {
                            where:{
                                "companyId": param.companyId,
                                "userId": { inq: userIdArr },
                                status:true,
                                providerType:"Stockist"
                            },
                            "include": [{
                                "relation": "user"
                            }]
                        }*/

                        match = {
                            "companyId": ObjectId(param.companyId),
                            "userId": { $in: userIdArr.map(user => ObjectId(user)) },
                            status: true,
                            providerType: "Stockist"
                        }
                    }

                    //let partiesLeft=await Primaryandsalereturn.app.models.Providers.find(match);

                    const ProviderCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Providers.modelName);

                    const providerData = ProviderCollection.aggregate([{
                        $match: match
                    }, {
                        $lookup: {
                            "from": "UserInfo",
                            "localField": "userId",
                            "foreignField": "userId",
                            "as": "user"
                        }
                    }, {
                        $unwind: "$user"
                    }, {
                        $project: {
                            _id: 1,
                            user: 1,
                            providerName: 1,
                            divisionId: "$user.divisionId"
                        }
                    }, {
                        $unwind: {
                            path: "$divisionId",
                            preserveNullAndEmptyArrays: true
                        }
                    }, {
                        $lookup: {
                            "from": "DivisionMaster",
                            "localField": "divisionId",
                            "foreignField": "_id",
                            "as": "division"
                        }
                    }, {
                        $unwind: {
                            path: "$division",
                            preserveNullAndEmptyArrays: true
                        }
                    }]);
                    const partiesLeft = await providerData.get();
                    if (partiesLeft !== undefined) {

                        partiesLeft.forEach(element => {
                            element = JSON.parse(JSON.stringify(element));
                            const matchedObj = salesStatusResult.filter(res => {
                                return (res.partyId).toString() == (element._id).toString()
                            })

                            let elementUser = JSON.parse(JSON.stringify(element));


                            if (matchedObj.length > 0) {
                                //console.log("matchedObj :=====> ",matchedObj)
                                finalObj.push({
                                    userId: matchedObj[0].userId,
                                    divisionName: matchedObj[0].divisionName,
                                    divisionId: matchedObj[0].divisionId,
                                    userName: matchedObj[0].userName,
                                    partyId: matchedObj[0].partyId,
                                    partyName: matchedObj[0].partyName,
                                    createdAt: matchedObj[0].createdAt,
                                    status: "Yes"

                                })

                            } else {
                                finalObj.push({
                                    userId: element.user._id,
                                    divisionName: element.division.divisionName || "-----",
                                    divisionId: element.division._id || "-----",
                                    userName: element.user.name,
                                    partyId: element._id,
                                    partyName: element.providerName,
                                    createdAt: "-----",
                                    status: "No"

                                })
                            }
                        });

                    }

                    cb(false, finalObj)
                }
            });






    };

    Primaryandsalereturn.remoteMethod(
        'getSecondarySalesStatus', {
        description: 'Secondary Sales Status',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //-----------------------------product wise sale report- by preeti 20-19-2019 ----------------------------
    Primaryandsalereturn.singleProductSale = function (params, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let ProductIds = []
        for (var i = 0; i < params.productId.length; i++) {
            ProductIds.push(ObjectId(params.productId[i]))
        }

        let objOfHierarchy = {
            companyId: params.companyId,
            status: params.status,
            type: params.type,
            supervisorId: params.userId
        }

        if (params.isDivisionExist) {
            objOfHierarchy.isDivisionExist = params.isDivisionExist
            objOfHierarchy.division = params.division
        }

        let filter = {
            companyId: ObjectId(params.companyId),
            productId: { $in: ProductIds }
        }

        Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchy(objOfHierarchy, function (err, res) {

            if (res.length > 0) {
                let userId = [];
                for (var i = 0; i < res.length; i++) {
                    userId.push(ObjectId(res[i].userId))
                }
                filter.userId = { $in: userId }
            } else {
                filter.userId = ObjectId(params.userId)

            }

            const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
            let month = [];
            let year = [];

            for (var i = 0; i < monthYearArray.length; i++) {
                month.push(monthYearArray[i].month);
                year.push(monthYearArray[i].year);

            }
            filter.month = { $in: month }
            filter.year = { $in: year }
            PrimaryInfoCollection.aggregate(

                // Stage 1
                {
                    $match: filter
                },

                // Stage 2
                {
                    $group: {
                        _id: {
                            "stateId": "$stateId",
                            "districtId": "$districtId",
                            "month": "$month",
                            "year": "$year",
                            productId: "$productId",
                            productName: "$productName"
                        },
                        total: {
                            $sum: "$totalSecondrySaleAmt"

                        }
                    }
                },

                // Stage 3
                {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "state"
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "district"
                    }
                },

                // Stage 5
                {
                    $project: {
                        month: "$_id.month",
                        year: "$_id.year",
                        districtName: { $arrayElemAt: ["$district.districtName", 0] },
                        stateName: { $arrayElemAt: ["$state.stateName", 0] },
                        districtId: { $arrayElemAt: ["$district._id", 0] },
                        stateId: { $arrayElemAt: ["$state._id", 0] },
                        total: 1,
                        productId: "$_id.productId",
                        productName: "$_id.productName"
                    }
                },
                {
                    $group: {
                        _id: {
                            stateId: "$districtId",
                            districtId: "$stateId",
                            districtName: "$districtName",
                            stateName: "$stateName",
                            month: "$month",
                            year: "$year"
                        },
                        data: {
                            $push: {
                                productId: "$productId",
                                productName: "$productName",
                                month: "$month",
                                year: "$year",
                                total: "$total"
                            }
                        },
                        totalAmt: {
                            $sum: "$total"
                        }
                    }
                },
                {
                    $project: {
                        stateName: "$_id.stateName",
                        districtName: "$_id.districtName",
                        month: "$_id.month",
                        year: "$_id.year",
                        data: "$data",
                        totalAmt: 1
                    }

                },
                {

                    $group: {
                        _id: {
                            stateName: "$stateName",
                            districtName: "$districtName",
                            productId: "$productId"
                        },
                        data: {
                            $push: {
                                month: "$month", year: "$year",
                                total: "$data.total",
                                productName: "$data.productName",
                                productId: "$data.productId"
                            }
                        },
                        totalAmt: {
                            $sum: "$totalAmt"
                        }
                    }
                },
                function (err, SSSRes) {

                    if (err) {
                        // console.log(err);
                        return cb(err);
                    }
                    if (SSSRes.length > 0) {
                        finalObject = [];

                        for (let i = 0; i < SSSRes.length; i++) {
                            let details = [];
                            let product = [];
                            // for(var j=0;j<SSSRes[i].data.length;i++){

                            // }

                            for (const month of monthYearArray) {
                                const matchedObject = SSSRes[i].data.filter(function (obj) {
                                    return obj.month == month.month && obj.year == month.year;
                                });
                                if (matchedObject.length > 0) {
                                    details.push(matchedObject[0].total)
                                    product.push(matchedObject[0].productName)
                                } else {
                                    details.push(0)
                                }
                            }
                        }

                        return cb(false, SSSRes);
                    } else {
                        return cb(false, []);

                    }

                }

            )

        })  //---------end of res--------------------

    }
    Primaryandsalereturn.remoteMethod(
        'singleProductSale', {
        description: 'Single Product Sale',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //-----------------------------------end ---------------
    //---------------Stockist Month wise sale report by preeti 18-12-2019-------------------------
    Primaryandsalereturn.stockistMonthWiseSale = function (params, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        //console.log('params.division',params.division[0][0] );
        let filter = {
            companyId: ObjectId(params.companyId),
            status: params.status,
            type: params.type,
            supervisorId: ObjectId(params.supervisorId),
        }

        if (params.isDivisionExist) {
            filter.isDivisionExist = params.isDivisionExist
            filter.division = ObjectId(params.division[0][0])
        }

        //console.log('filter ',filter );

        Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchy(filter, function (err, res) {
            let userId = [];

            let filter = {
                companyId: ObjectId(params.companyId),
            }
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    userId.push(ObjectId(res[i].userId))
                }
                filter.userId = { $in: userId };
            } else {
                filter.userId = ObjectId(params.supervisorId);
            }

            const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
            let month = [];
            let year = [];

            for (var i = 0; i < monthYearArray.length; i++) {
                month.push(monthYearArray[i].month);
                year.push(monthYearArray[i].year);
            }
            filter.month = { $in: month }
            filter.year = { $in: year }
            //console.log('filter ',filter );
            PrimaryInfoCollection.aggregate(

                // Stage 1
                {
                    $match: filter
                },

                // Stage 2
                {
                    $group: {
                        _id: {
                            "stateId": "$stateId",
                            "districtId": "$districtId",
                            "month": "$month",
                            "year": "$year",
                            partyId: "$partyId",
                            partyName: "$partyName"
                        },
                        total: {
                            $sum: "$totalSecondrySaleAmt"

                        }
                    }
                },

                // Stage 3
                {
                    $lookup: {
                        "from": "State",
                        "localField": "_id.stateId",
                        "foreignField": "_id",
                        "as": "state"
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "district"
                    }
                }, {
                $lookup: {
                    "from": "Providers",
                    "localField": "_id.partyId",
                    "foreignField": "_id",
                    "as": "providers"
                }
            },
                // Stage 5
                {
                    $project: {
                        month: "$_id.month",
                        year: "$_id.year",
                        districtName: { $arrayElemAt: ["$district.districtName", 0] },
                        stateName: { $arrayElemAt: ["$state.stateName", 0] },
                        districtId: { $arrayElemAt: ["$district._id", 0] },
                        stateId: { $arrayElemAt: ["$state._id", 0] },
                        //stockistName: { $arrayElemAt: ["$providers.providerName", 0] },
                        stockistName: "$_id.partyName",
                        //stkId: { $arrayElemAt: ["$providers._id", 0] },
                        stkId: "$_id.partyId",

                        total: 1
                    }
                }, {
                $group: {
                    _id: {
                        stateId: "$districtId",
                        districtId: "$stateId",
                        districtName: "$districtName",
                        stateName: "$stateName",
                        stockistName: "$stockistName",
                        month: "$month",
                        year: "$year"
                    },
                    data: {
                        $push: {
                            month: "$month",
                            year: "$year",
                            total: "$total"
                        }
                    },
                    totalAmt: {
                        $sum: "$total"
                    }
                }
            }, {
                $project: {
                    stateName: "$_id.stateName",
                    districtName: "$_id.districtName",
                    stockistName: "$_id.stockistName",
                    month: "$_id.month",
                    year: "$_id.year",
                    data: "$data",
                    totalAmt: 1
                }

            }, {
                $group: {
                    _id: {
                        stateName: "$stateName",
                        districtName: "$districtName",
                        stockistName: "$stockistName"
                    },
                    data: { $push: { month: "$month", year: "$year", total: { $arrayElemAt: ["$data.total", 0] } } },
                    totalAmt: {
                        $sum: "$totalAmt"
                    },
                }
            },
                function (err, SSSRes) {

                    if (err) {
                        // console.log(err);
                        return cb(err);
                    }
                    if (SSSRes.length > 0) {
                        finalObject = [];
                        for (let i = 0; i < SSSRes.length; i++) {
                            let details = [];
                            for (const month of monthYearArray) {
                                const matchedObject = SSSRes[i].data.filter(function (obj) {
                                    return obj.month == month.month && obj.year == month.year;
                                });

                                if (matchedObject.length > 0) {
                                    details.push((matchedObject[0].total).toFixed(2))
                                } else {
                                    details.push(0)
                                }
                            }
                            finalObject.push({
                                hqName: SSSRes[i]._id.districtName,
                                stateName: SSSRes[i]._id.stateName,
                                stockistName: SSSRes[i]._id.stockistName,
                                totalAmt: (SSSRes[i].totalAmt).toFixed(2),
                                data: details,
                            });
                        }
                        return cb(false, finalObject);
                    }
                    else {

                        return cb(false, [])
                    }
                }
            )
            // add if case in it-->

        })
    }
    Primaryandsalereturn.remoteMethod(
        'stockistMonthWiseSale', {
        description: 'Target Vs Achievement Graph API',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //----------------------end-------------------------------------------------

    //Product - State wise (Sale) Praveen Singh 
    Primaryandsalereturn.getProductStatewiseSale = function (params, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let filter = {};
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];

        if (params.type == "State") {
            // for (var i = 0; i < params.stateIds.length; i++) {
            //     stateIdsArr.push(ObjectId(params.stateIds[i]));
            // }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { $in: [ObjectId(params.stateIds)] },
            }
        } else if (params.type == "Headquarter") {
            for (var i = 0; i < params.districtIds.length; i++) {
                hqIdArray.push(ObjectId(params.districtIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }

        }
        else if (params.type == "Employee Wise") {
            for (var i = 0; i < params.userIds.length; i++) {
                empArray.push(ObjectId(params.userIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { $in: empArray }
            }

        }
        const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
        let month = [];
        let year = [];

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);

        }
        filter.month = { $in: month }
        filter.year = { $in: year }

        PrimaryInfoCollection.aggregate(
            // Stage 1
            {
                $match: filter
            },

            //Stage 2
            {
                $lookup: {
                    "from": "State",
                    "localField": "stateId",
                    "foreignField": "_id",
                    "as": "state"
                }
            },

            // Stage 3
            {
                $project: {
                    productId: 1,
                    productName: 1,
                    stateId: 1,
                    stateName: { "$arrayElemAt": ["$state.stateName", 0] },
                    month: 1,
                    year: 1,
                    productCode: 1,
                    totalSecondrySaleQty: 1,
                    totalSecondrySaleAmt: 1
                }
            }, {

            $sort: {
                stateName: 1,
                month: 1,
                year: 1,
                productName: 1
            }
        },

            // Stage 4
            {
                $group: {
                    _id: {
                        "productId": "$productId",
                        "productName": "$productName",
                        "stateId": "$stateId",
                        "stateName": "$stateName",
                        "month": "$month",
                        "year": "$year"
                    },
                    totalSecondrySaleQty: {
                        "$sum": "$totalSecondrySaleQty"
                    },
                    totalSecondrySaleAmt: {
                        $sum: "$totalSecondrySaleAmt"
                    },
                    productCode: {
                        $addToSet: "$productCode"
                    }
                }
            },

            // Stage 5
            {
                $project: {
                    _id: 0,
                    "productId": "$_id.productId",
                    "productName": "$_id.productName",
                    "stateId": "$_id.stateId",
                    "stateName": "$_id.stateName",
                    "month": "$_id.month",
                    "year": "$_id.year",
                    "totalSecondrySaleQty": 1,
                    "totalSecondrySaleAmt": 1,
                    productCode: { $arrayElemAt: ["$productCode", 0] },
                }
            }, {
            $sort: {
                month: 1,
                year: 1
            }
        }, {
            $group: {
                _id: {
                    productId: "$productId"
                },
                stateName: {
                    $addToSet: "$stateName"
                },
                productName: {
                    $addToSet: "$productName"
                },
                productCode: {
                    $addToSet: "$productCode"
                },
                data: {
                    $push: {
                        "month": "$month",
                        "year": "$year",
                        "totalSecondrySaleQty": "$totalSecondrySaleQty",
                        "totalSecondrySaleAmt": "$totalSecondrySaleAmt"
                    }
                },
                totalAmt: {
                    $sum: "$totalSecondrySaleAmt"
                },
                totalQty: {
                    $sum: "$totalSecondrySaleQty"
                }
            }
        }, {
            $project: {
                _id: 0,
                stateName: { $arrayElemAt: ["$stateName", 0] },
                productName: { $arrayElemAt: ["$productName", 0] },
                productCode: { $arrayElemAt: ["$productCode", 0] },
                totalAmt: 1,
                totalQty: 1,
                data: 1
            }
        },
            function (err, result) {
                const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                const newResult = result.map(result2 => {
                    //Product wise
                    const newDataArray = result2.data.map(data => {
                        //montth wise
                        const monthwisedata = monthYearArray.map(month => {
                            if (month.month === data.month && month.year === data.year) {
                                return data
                            } else {
                                const data = { month: month.month, year: month.year, totalSecondrySaleQty: 0, totalSecondrySaleAmt: 0 };
                                return data;
                            }

                        });
                        return monthwisedata;

                    });
                    result2.data = newDataArray[0]
                    return result2;
                });

                return cb(false, newResult)
            }
        );

    }

    Primaryandsalereturn.remoteMethod(
        'getProductStatewiseSale', {
        description: 'Get Product State wise sales',
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    // Anjana Rana
    Primaryandsalereturn.getProductCategroryOrBrandWiseSale = function (params, cb) {
        var self = this;
        var PrimaryInfoCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        let begin = new Date(params.dateRange.begin);
        let end = new Date(params.dateRange.end);
        var startDate = moment(begin, "YYYY-M-DD");
        var endDate = moment(end, "YYYY-M-DD").endOf("month");
        let filter = {
            companyId: ObjectId(params.companyId),
        }
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];

        const monthYearArray = monthNameAndYearBTWTwoDate(startDate, endDate);
        let month = [];
        let year = [];
        if (params.type == "State") {
            for (var i = 0; i < params.stateIds.length; i++) {
                stateIdsArr.push(ObjectId(params.stateIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                //stateId: { $in: [ObjectId(params.stateIds)] }
                stateId: { $in: stateIdsArr }
            }
        } else if (params.type == "Headquarter") {
            for (var i = 0; i < params.districtIds.length; i++) {
                hqIdArray.push(ObjectId(params.districtIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }

        }
        else if (params.type == "Employee Wise") {
            for (var i = 0; i < params.employeeIds.length; i++) {
                empArray.push(ObjectId(params.employeeIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { $in: empArray }
            }

        }

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);

        }
        filter.month = { $in: month }
        filter.year = { $in: year }


        PrimaryInfoCollection.aggregate(

            // Stage 1
            {
                $match: filter
            },

            // Stage 2
            {
                $lookup: {
                    "from": "Products",
                    "localField": "productId",
                    "foreignField": "_id",
                    "as": "productInfo"
                }
            },

            // Stage 3
            {
                $project: {
                    _id: 0,
                    productId: "$productId",
                    productName: { $arrayElemAt: ["$productInfo.productName", 0] },
                    productPackageSize: { $arrayElemAt: ["$productInfo.productPackageSize", 0] },
                    productType: { $arrayElemAt: ["$productInfo.productType", 0] },
                    productPackageSize: { $arrayElemAt: ["$productInfo.productPackageSize", 0] },
                    qty: "$totalSecondrySaleQty",
                    value: "$totalSecondrySaleAmt",
                    month: "$month",
                    year: "$year"
                }
            },

            // Stage 4
            {
                $group: {
                    _id: { productType: "$productType", month: "$month", year: "$year" },
                    qty: {
                        $sum: "$qty"
                    },
                    value: {
                        $sum: "$value"
                    },
                    data: {
                        $push: {
                            productId: "$productId",
                            productName: "$productName",
                            productPackageSize: "$productPackageSize",
                            qty: "$qty",
                            value: "$value"
                        }
                    }
                }
            },

            // Stage 5
            {
                $unwind: "$data"
            },

            // Stage 6
            {
                $group: {
                    _id: {
                        productType: "$_id.productType",
                        productId: "$data.productId",
                        productName: "$data.productName",
                        productPackageSize: "$data.productPackageSize"

                    },
                    datac: {
                        $push: {
                            qty: "$data.qty",
                            value: "$data.value",
                            month: "$_id.month",
                            year: "$_id.year"
                        }
                    },
                    tot: {
                        $addToSet: {

                            qty: "$qty",
                            value: "$value",
                            month: "$_id.month",
                            year: "$_id.year"
                        }
                    }
                }
            },

            // Stage 7
            {
                $project: {
                    "_id": 0,
                    "productType": "$_id.productType",
                    "productName": "$_id.productName",
                    "productPackageSize": "$_id.productPackageSize",
                    "productId": "$_id.productId",
                    "data": "$datac",
                    "tot": 1

                }
            },

            // Stage 8
            {
                $sort: {
                    productType: 1,
                    productName: 1
                }
            }, function (err, SSSRes) {
                if (err) {
                    return cb(err);
                }
                if (SSSRes.length > 0) {
                    finalObject = [];
                    let productType = [];
                    let finalqty = []; finalvalue = [];
                    for (let i = 0; i < SSSRes.length; i++) {
                        let finalqty = []; finalvalue = [];
                        let qty = 0; value = 0;
                        let details = [];
                        let totDetails = [];
                        for (const month of monthYearArray) {

                            const matchedObject = SSSRes[i].data.filter(function (obj) {
                                return obj.month == month.month && obj.year == month.year;
                            });
                            const matchedTotObject = SSSRes[i].tot.filter(function (obj) {
                                return obj.month == month.month && obj.year == month.year;
                            });
                            if (matchedObject.length > 0) {
                                totDetails.push({
                                    "qty": (matchedTotObject[0].qty).toFixed(2),
                                    "value": (matchedTotObject[0].value).toFixed(2)
                                })

                                details.push({
                                    "qty": (matchedObject[0].qty).toFixed(2),
                                    "value": (matchedObject[0].value).toFixed(2),
                                })
                            } else {
                                totDetails.push({
                                    "qty": 0,
                                    "value": 0
                                })
                                details.push({
                                    "qty": 0,
                                    "value": 0
                                })
                            }
                        }
                        if ((productType.indexOf(SSSRes[i].productType) == -1)) {

                            finalObject.push({
                                productType: "Total",
                                productName: "",
                                productPackageSize: "",
                                data: totDetails
                                //tot:totDetails
                            });

                        }
                        productType.push(SSSRes[i].productType)
                        finalObject.push({
                            productType: SSSRes[i].productType,
                            productName: SSSRes[i].productName,
                            productPackageSize: SSSRes[i].productPackageSize,
                            data: details
                            //tot:[]
                        });
                    }
                    return cb(false, finalObject);
                } else {
                    //No Data Found
                    return cb(false, []);
                }
            }
        );

    }

    Primaryandsalereturn.remoteMethod(
        'getProductCategroryOrBrandWiseSale', {
        description: 'Get Product State wise sales',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );

    // done by ravi on 20-12-2019
    Primaryandsalereturn.stockistwiseSales = function (param, cb) {
        const monthYearArray = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate);
        let month = [];
        let year = [];
        let filter = {
            companyId: ObjectId(param.companyId),
            userId: ObjectId(param.userId),
        }

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);
        }
        filter.month = { $in: month }
        filter.year = { $in: year }


        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        PrimaryandsalereturnCollection.aggregate(
            // Stage 1
            {
                $match: filter
            },

            // Stage 2
            {
                $group: {
                    _id: {
                        partyId: "$partyId",
                        partyName: "$partyName",
                        productId: "$productId",
                        productName: "$productName",
                        month: "$month",
                        year: "$year"
                    },
                    totalSecondrySaleQty: {
                        $sum: "$totalSecondrySaleQty"
                    },
                    totalSecondrySaleAmt: {
                        $sum: "$totalSecondrySaleAmt"
                    },
                    stockistArr: { $addToSet: "$partyName" },
                }
            },

            // Stage 3
            {
                $project: {
                    month: "$_id.month",
                    year: "$_id.year",
                    partyId: "$_id.partyId",
                    partyName: "$_id.partyName",
                    productId: "$_id.productId",
                    productName: "$_id.productName",
                    totalSecondrySaleQty: 1,
                    totalSecondrySaleAmt: 1,
                    stockistArr: 1,
                }
            }, {
            $group: {
                _id: {
                    productId: "$productId",
                    productName: "$productName",
                    partyId: "$partyId",
                    partyName: "$partyName"
                },
                data: {
                    $push: {
                        totalSecondrySaleQty: "$totalSecondrySaleQty",
                        totalSecondrySaleAmt: "$totalSecondrySaleAmt",
                        month: "$month",
                        year: "$year",
                        partyName: "$partyName"

                    }
                }
            }
        }, function (err, response) {
            if (response.length > 0) {
                let finalReturnObject = [];
                const monthARRAY = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate)
                for (const sr of response) {
                    let data = [];
                    for (const mnth of monthARRAY) {
                        const saleDataFound = sr.data.filter(function (obj) {
                            return mnth.month == obj.month && mnth.year == obj.year
                        });
                        if (saleDataFound.length > 0) {
                            data.push(saleDataFound[0])
                        } else {
                            data.push({
                                month: mnth.month,
                                year: mnth.year,
                                totalSecondrySaleQty: 0,
                                totalSecondrySaleAmt: 0,
                            })
                        }
                    }
                    finalReturnObject.push({
                        partyId: sr._id.partyId,
                        partyName: sr._id.partyName,
                        productId: sr._id.productId,
                        productName: sr._id.productName,
                        data: data
                    });
                }

                return cb(null, finalReturnObject)
            } else {
                return cb(null, [])
            }
        });

    };
    Primaryandsalereturn.remoteMethod(
        'stockistwiseSales', {
        description: 'stockistwiseSales',
        accepts: [{
            arg: 'param',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );


    //-----------------------Praveen Kumar(20-12-2019)------------------------------------------
    Primaryandsalereturn.MRQtySecondarySale = function (param, cb) {
        /* param = {
             companyId: "5d15fb9903459316a8376bbf",
             type: "lower",
             supervisorId: ["5d4ae8f90d1d7c0ec64c63dc", "5d5ae02b1ab20f28e57489ed"]
                 //supervisorId: ["5d4ae8f90d1d7c0ec64c63dc"]

         }*/
        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        const HierarchyCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Hierarchy.modelName);
        console.log('param', param);
        let HierarchyResult = [];
        asyncLoop(param.supervisorId, function (item, next) {
            // const passingObj = {
            //     companyId: "5d15fb9903459316a8376bbf",
            //     supervisorId: item,
            //     type: "lower"
            // }
            const passingObj = {
                companyId: param.companyId,
                supervisorId: item,
                type: "lower"
            }
            Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchy(passingObj, function (err, managerResult) {
                if (err) console.log(err);
                if (managerResult.length > 0) {
                    const lowerHierarchy = managerResult.map(res => ObjectId(res.userId));
                    HierarchyResult.push({
                        managerId: ObjectId(item),
                        lowerHierarchy
                    });
                }
                next();
            });
            return HierarchyResult
        }, function (err, result) {
            finalReturnObject = [];
            if (HierarchyResult.length > 0) {
                asyncLoop(HierarchyResult, function (item, next) {
                    let managerTotalQty = [0];
                    let managerTotalSale = [0];
                    Primaryandsalereturn.app.models.UserInfo.findOne({ where: { userId: item.managerId } }, function (err, manageResult) {
                        console.log(manageResult);
                        PrimaryandsalereturnCollection.aggregate({
                            $match: {
                                companyId: ObjectId(param.companyId), //ObjectId("5d15fb9903459316a8376bbf"),
                                userId: { $in: item.lowerHierarchy } //[ObjectId("5d694ecac2d0de130b074bc9"), ObjectId("5d5ae08e1ab20f28e57489f8")] }
                            }
                        }, {
                            $group: {
                                _id: {
                                    userId: "$userId",
                                    partyId: "$partyId",
                                    partyName: "$partyName",
                                    month: "$month",
                                    year: "$year"
                                },
                                totalSecondrySaleQty: {
                                    $sum: "$totalSecondrySaleQty"
                                },
                                totalSecondrySaleAmt: {
                                    $sum: "$totalSecondrySaleAmt"
                                },
                            }
                        }, {
                            $lookup: {
                                "from": "UserInfo",
                                "localField": "_id.userId",
                                "foreignField": "userId",
                                "as": "userData"
                            }
                        }, {
                            $unwind: "$userData"
                        }, {
                            $lookup: {
                                "from": "District",
                                "localField": "userData.districtId",
                                "foreignField": "_id",
                                "as": "disrictData"
                            }
                        }, {
                            $project: {
                                _id: 0,
                                userId: "$_id.userId",
                                partyId: "$_id.partyId",
                                partyName: "$_id.partyName",
                                month: "$_id.month",
                                year: "$_id.year",
                                totalSecondrySaleQty: 1,
                                totalSecondrySaleAmt: 1,
                                userDesignation: "$userData.designation",
                                userName: "$userData.name",
                                userDistrict: {
                                    $arrayElemAt: ["$disrictData.districtName", 0]
                                }
                            }
                        }, {
                            $group: {
                                _id: {
                                    userId: "$userId",
                                    userName: "$userName",
                                    partyId: "$partyId",
                                    partyName: "$partyName",
                                    userDesignation: "$userDesignation",
                                    userDistrict: "$userDistrict"
                                },
                                data: {
                                    $addToSet: {
                                        totalSecondrySaleQty: "$totalSecondrySaleQty",
                                        totalSecondrySaleAmt: "$totalSecondrySaleAmt",
                                        month: "$month",
                                        year: "$year"
                                    }
                                }
                            }
                        }, function (err, saleResult) {
                            //console.log(saleResult);


                            if (saleResult.length > 0) {
                                const monthARRAY = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate)
                                let userResultObject = [];
                                let userTotalQty = [0];
                                let userTotalSale = [0];
                                //console.log(monthARRAY);

                                for (const sr of saleResult) {
                                    let monthData = [];
                                    let cnt = 0;

                                    for (const mnth of monthARRAY) {

                                        const saleDataFound = sr.data.filter(function (obj) {
                                            return mnth.month == obj.month && mnth.year == obj.year
                                        });

                                        if (saleDataFound.length > 0) {
                                            monthData.push({
                                                totalSecondrySaleQty: (saleDataFound[0].totalSecondrySaleQty).toFixed(2),
                                                totalSecondrySaleAmt: (saleDataFound[0].totalSecondrySaleAmt).toFixed(2)
                                            });
                                            //console.log(cnt, "IfData : ", isNaN(userTotalQty[cnt]) ? 0 : userTotalQty[cnt], ": ", saleDataFound[0].totalSecondrySaleQty);

                                            managerTotalQty[cnt] = (isNaN(managerTotalQty[cnt]) ? 0 : managerTotalQty[cnt]) + saleDataFound[0].totalSecondrySaleQty;
                                            managerTotalSale[cnt] = (isNaN(managerTotalSale[cnt]) ? 0 : managerTotalSale[cnt]) + saleDataFound[0].totalSecondrySaleAmt;

                                            userTotalQty[cnt] = (isNaN(userTotalQty[cnt]) ? 0 : userTotalQty[cnt]) + saleDataFound[0].totalSecondrySaleQty;
                                            userTotalSale[cnt] = (isNaN(userTotalSale[cnt]) ? 0 : userTotalSale[cnt]) + saleDataFound[0].totalSecondrySaleAmt;

                                        } else {
                                            //console.log(cnt, "ELSEData : ", (isNaN(managerTotalQty[cnt]) ? 0 : userTotalQty[cnt] + 0));

                                            managerTotalQty[cnt] = (isNaN(managerTotalQty[cnt]) ? 0 : managerTotalQty[cnt] + 0);
                                            managerTotalSale[cnt] = (isNaN(managerTotalSale[cnt]) ? 0 : managerTotalSale[cnt] + 0);

                                            userTotalQty[cnt] = (isNaN(userTotalQty[cnt]) ? 0 : userTotalQty[cnt] + 0);
                                            userTotalSale[cnt] = (isNaN(userTotalSale[cnt]) ? 0 : userTotalSale[cnt] + 0);

                                            monthData.push({
                                                totalSecondrySaleQty: 0,
                                                totalSecondrySaleAmt: 0
                                            });
                                        }
                                        cnt++;
                                    }
                                    userResultObject.push({
                                        username: sr._id.userName,
                                        partyName: sr._id.partyName,
                                        designation: sr._id.userDesignation,
                                        userDistrict: sr._id.userDistrict,
                                        monthData
                                    });

                                }

                                finalReturnObject.push({
                                    managerName: manageResult.name,
                                    managerTotalQty: managerTotalQty,
                                    managerTotalSale: managerTotalSale,
                                    userTotalQty: userTotalQty,
                                    userTotalSale: userTotalSale,
                                    userStkData: userResultObject
                                });
                            }
                            next();
                        });


                    });

                    return finalReturnObject;
                }, function (err, finalResult) {
                    //console.log("Line 2397 : ", finalReturnObject);
                    return cb(false, finalReturnObject)
                });
            }
        });

    };
    Primaryandsalereturn.remoteMethod(
        'MRQtySecondarySale', {
        description: 'MR Qty Value wise Secondary Sale',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //-----------------------END Praveen Kumar(20-12-2019)------------------------------------------

    //-------------ravi on 202-12-2019-----------------------------------------------//
    Primaryandsalereturn.managerwiseSSS = function (param, cb) {
        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        const HierarchyCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Hierarchy.modelName);

        let HierarchyResult = [];
        let companyId = param.companyId
        asyncLoop(param.supervisorId, function (item, next) {
            const passingObj = {
                companyId: companyId,
                supervisorId: item,
                type: "lower"
            }
            Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchy(passingObj, function (err, managerResult) {
                if (err) console.log(err);
                if (managerResult.length > 0) {
                    const lowerHierarchy = managerResult.map(res => ObjectId(res.userId));
                    HierarchyResult.push({
                        managerId: ObjectId(item),
                        lowerHierarchy
                    });
                }
                next();
            });
            return HierarchyResult
        }, function (err, result) {
            //console.log(HierarchyResult);
            finalReturnObject = [];
            if (HierarchyResult.length > 0) {
                asyncLoop(HierarchyResult, function (item, next) {
                    Primaryandsalereturn.app.models.UserInfo.findOne({ where: { userId: item.managerId }, "include": { "relation": "district" } }, function (err, manageResult) {
                        PrimaryandsalereturnCollection.aggregate({
                            $match: {
                                companyId: ObjectId(companyId),
                                userId: { $in: item.lowerHierarchy }
                            }
                        }, {
                            $group: {
                                _id: {
                                    month: "$month",
                                    year: "$year"
                                },
                                opening: {
                                    $sum: "$opening"
                                },
                                closing: {
                                    $sum: "$closing"
                                },
                                closingAmt: {
                                    $sum: "$closingAmt"
                                },
                                totalSaleReturnQty: {
                                    $sum: "$totalSaleReturnQty"
                                },
                                totalSaleReturnAmt: {
                                    $sum: "$totalSaleReturnAmt"
                                },
                                totalPrimaryQty: {
                                    $sum: "$totalPrimaryQty"
                                },
                                totalPrimaryAmt: {
                                    $sum: "$totalPrimaryAmt"
                                },
                                totalSecondrySaleQty: {
                                    $sum: "$totalSecondrySaleQty"
                                },
                                totalSecondrySaleAmt: {
                                    $sum: "$totalSecondrySaleAmt"
                                }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                month: "$_id.month",
                                year: "$_id.year",
                                opening: 1,
                                closing: 1,
                                closingAmt: 1,
                                totalSaleReturnQty: 1,
                                totalSaleReturnAmt: 1,
                                totalPrimaryQty: 1,
                                totalPrimaryAmt: 1,
                                totalSecondrySaleQty: 1,
                                totalSecondrySaleAmt: 1
                            }
                        }, {
                            $group: {
                                _id: {},
                                data: {
                                    $addToSet: {
                                        opening: "$opening",
                                        closing: "$closing",
                                        closingAmt: "$closingAmt",
                                        totalSaleReturnQty: "$totalSaleReturnQty",
                                        totalSaleReturnAmt: "$totalSaleReturnAmt",
                                        totalPrimaryQty: "$totalPrimaryQty",
                                        totalPrimaryAmt: "$totalPrimaryAmt",
                                        totalSecondrySaleQty: "$totalSecondrySaleQty",
                                        totalSecondrySaleAmt: "$totalSecondrySaleAmt",
                                        month: "$month",
                                        year: "$year"
                                    }
                                }
                            }
                        }, function (err, saleResult) {
                            //console.log(saleResult[0].data);
                            if (saleResult.length > 0) {
                                const monthARRAY = monthNameAndYearBTWTwoDate(param.fromDate, param.toDate)
                                for (const sr of saleResult) {
                                    let data = [];
                                    for (const mnth of monthARRAY) {
                                        const saleDataFound = sr.data.filter(function (obj) {
                                            return mnth.month == obj.month && mnth.year == obj.year
                                        });
                                        if (saleDataFound.length > 0) {
                                            data.push(saleDataFound[0])
                                        } else {
                                            data.push({
                                                opening: 0,
                                                closing: 0,
                                                closingAmt: 0,
                                                totalSaleReturnQty: 0,
                                                totalSaleReturnAmt: 0,
                                                totalPrimaryQty: 0,
                                                totalPrimaryAmt: 0,
                                                totalSecondrySaleQty: 0,
                                                totalSecondrySaleAmt: 0,
                                                month: mnth.month,
                                                year: mnth.year
                                            })
                                        }
                                    }
                                    let district = JSON.parse(JSON.stringify(manageResult));
                                    finalReturnObject.push({
                                        managerName: manageResult.name,
                                        designation: manageResult.designation,
                                        districtName: district.district.districtName,
                                        data: data
                                    });
                                }
                                next();
                            } else {
                                return finalReturnObject
                            }
                        });
                    });
                    return finalReturnObject
                }, function (err, result) {
                    cb(false, finalReturnObject);
                });
            }
        });

    };
    Primaryandsalereturn.remoteMethod(
        'managerwiseSSS', {
        description: 'managerwise SSS',
        accepts: [{
            arg: 'param',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //END


    Primaryandsalereturn.getTargetVsAchievementData = function (params, cb) {
        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);

        let filter = {};
        let filterTarget = {};
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];

        if (params.type == "State") {
            for (var i = 0; i < params.stateIds.length; i++) {
                stateIdsArr.push(ObjectId(params.stateIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { $in: stateIdsArr },
            }
            filterTarget = {
                companyId: ObjectId(params.companyId),
                stateId: { $in: stateIdsArr },
            }
        } else if (params.type == "Headquarter") {
            for (var i = 0; i < params.districtIds.length; i++) {
                hqIdArray.push(ObjectId(params.districtIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }
            filterTarget = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }
        }
        else if (params.type == "Employee Wise") {
            for (var i = 0; i < params.userIds.length; i++) {
                empArray.push(ObjectId(params.userIds[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { $in: empArray }
            }
            filterTarget = {
                companyId: ObjectId(params.companyId),
                userId: { $in: empArray }
            }

        }
        const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
        let month = [];
        let year = [];

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);

        }
        filter.month = { $in: month }
        filter.year = { $in: year }
        filterTarget.targetMonth = { $in: month }
        filterTarget.targetYear = { $in: year }
        async.parallel({
            primaryAndSecondory: function (cb) {
                PrimaryandsalereturnCollection.aggregate({
                    $match: filter
                }, {
                    $group: {
                        _id: {
                            "stateId": "$stateId",
                            "districtId": "$districtId",
                            "month": "$month",
                            "year": "$year"
                        },
                        total: {
                            $sum: "$totalPrimaryAmt"

                        },
                        userId: { $push: "$userId" }
                    }
                },
                    {
                        $unwind: "$userId"
                    },

                    // Stage 3
                    {
                        $lookup: {
                            "from": "State",
                            "localField": "_id.stateId",
                            "foreignField": "_id",
                            "as": "state"
                        }
                    },
                    {
                        $lookup: {
                            "from": "UserLogin",
                            "localField": "userId",
                            "foreignField": "userId",
                            "as": "userData"
                        }
                    },

                    // Stage 4
                    {
                        $lookup: {
                            "from": "District",
                            "localField": "_id.districtId",
                            "foreignField": "_id",
                            "as": "district"
                        }
                    },

                    // Stage 5
                    {
                        $project: {
                            month: "$_id.month",
                            year: "$_id.year",
                            districtName: { $arrayElemAt: ["$district.districtName", 0] },
                            stateName: { $arrayElemAt: ["$state.stateName", 0] },
                            districtId: { $arrayElemAt: ["$district._id", 0] },
                            stateId: { $arrayElemAt: ["$state._id", 0] },
                            userName: { $arrayElemAt: ["$userData.name", 0] },
                            total: 1
                        }
                    },
                    {
                        $group: {
                            _id: {
                                stateId: "$districtId",
                                districtId: "$stateId",
                                districtName: "$districtName",
                                stateName: "$stateName",
                                userName: "$userName",
                                month: "$month",
                                year: "$year"

                            },
                            data: {
                                $push: {
                                    month: "$month",
                                    year: "$year",
                                    total: "$total"

                                }
                            },
                            totalAmt: {
                                $sum: "$total"
                            }
                        }
                    },
                    {
                        $project: {
                            stateName: "$_id.stateName",
                            districtName: "$_id.districtName",
                            userName: "$_id.userName",
                            month: "$_id.month",
                            year: "$_id.year",
                            data: "$data",
                            totalAmt: 1
                        }

                    },
                    {
                        $group: {
                            _id: {
                                stateName: "$stateName",
                                districtName: "$districtName",
                                userName: "$userName"
                            },
                            data: { $push: { month: "$month", year: "$year", total: { $arrayElemAt: ["$data.total", 0] } } },
                            totalAmt: {
                                $sum: "$totalAmt"
                            },
                        }
                    }, function (err, primResult) {
                        if (err) {
                            console.log(err)
                        }
                        cb(false, primResult);
                    });

            },
            target: function (cb) {

                TargetCollection.aggregate({
                    $match: filterTarget
                }, {
                    $group: {
                        _id: {
                            durationType: "$durationType"
                        }
                    }
                }, function (err, result) {

                    if (err) {
                        console.log(err);
                    }

                    if (result.length > 0) {
                        let matchForTarget = {};
                        let groupForTarget = {};
                        let projectForTarget = {};
                        let foundDurationType_Yearly = result.filter(function (obj) {
                            return obj._id.durationType == "Yearly";
                        });
                        if (foundDurationType_Yearly.length > 0) {
                            matchForTarget = {
                                companyId: ObjectId(params.companyId),
                                targetMonth: { $in: month }
                            };
                            groupForTarget = {
                                _id: {
                                    stateId: "$stateId",
                                    "districtId": "$districtId",
                                    "month": "$targetMonth",
                                    "year": "$targetYear",
                                    "userId": "$userId",
                                    productName: '$_id.productName',

                                },
                                total: {
                                    $sum: "$targetQuantity"
                                }
                            };
                            projectForTarget = {
                                _id: 0,
                                stateId: "$_id.stateId",
                                "districtId": "$districtId",
                                userName: { $arrayElemAt: ["$userData.name", 0] },
                                "month": "$targetMonth",
                                "year": "$targetYear",
                                productName: '$productName',
                                tagetForMonth: {
                                    $divide: ["$total", 12]
                                }
                            };
                        } else {
                            let foundDurationType_MonthlyOrQuarterly = result.filter(function (obj) {
                                return obj._id.durationType == "Monthly" || obj._id.durationType == "Quarterly";
                            });

                            if (foundDurationType_MonthlyOrQuarterly.length > 0) {
                                matchForTarget = filterTarget
                                groupForTarget = {
                                    _id: {
                                        stateId: "$stateId",
                                        "districtId": "$districtId",
                                        "month": "$targetMonth",
                                        "year": "$targetYear",
                                        "userId": "$userId",
                                        productName: '$productName',

                                    },
                                    total: {
                                        $sum: "$targetQuantity"
                                    }
                                };
                                projectForTarget = {
                                    month: "$_id.month",
                                    year: "$_id.year",
                                    userName: { $arrayElemAt: ["$userData.name", 0] },
                                    districtName: { $arrayElemAt: ["$district.districtName", 0] },
                                    stateName: { $arrayElemAt: ["$state.stateName", 0] },
                                    districtId: { $arrayElemAt: ["$district._id", 0] },
                                    stateId: { $arrayElemAt: ["$state._id", 0] },
                                    productName: '$_id.productName',

                                    total: 1
                                };
                            } else {

                            }
                        }

                        TargetCollection.aggregate({
                            $match: matchForTarget
                        },
                            {
                                $group: groupForTarget
                            },
                            // Stage 3
                            {
                                $lookup: {
                                    "from": "State",
                                    "localField": "_id.stateId",
                                    "foreignField": "_id",
                                    "as": "state"
                                }
                            },
                            {
                                $lookup: {
                                    "from": "UserLogin",
                                    "localField": "_id.userId",
                                    "foreignField": "_id",
                                    "as": "userData"
                                }
                            },

                            // Stage 4
                            {
                                $lookup: {
                                    "from": "District",
                                    "localField": "_id.districtId",
                                    "foreignField": "_id",
                                    "as": "district"
                                }
                            },
                            {
                                $project: projectForTarget
                            },
                            {
                                $group: {
                                    _id: {
                                        stateId: "$districtId",
                                        districtId: "$stateId",
                                        districtName: "$districtName",
                                        stateName: "$stateName",
                                        month: "$month",
                                        year: "$year",
                                        userName: "$userName",
                                        productName: "$productName",

                                    },
                                    data: {
                                        $push: {
                                            month: "$month",
                                            year: "$year",
                                            total: "$total",
                                            productName: "$productName",

                                        }
                                    },
                                    totalAmt: {
                                        $sum: "$total"
                                    }
                                }

                            }
                            , {
                                $project: {
                                    stateName: "$_id.stateName",
                                    districtName: "$_id.districtName",
                                    userName: "$_id.userName",
                                    month: "$_id.month",
                                    year: "$_id.year",
                                    data: "$data",
                                    totalAmt: 1,
                                    productName: "$_id.productName",
                                }
                            }, {
                            $group: {
                                _id: {
                                    stateName: "$stateName",
                                    districtName: "$districtName",
                                    userName: "$userName"
                                },
                                data: { $push: { month: "$month", year: "$year", productName: "$productName", total: { $arrayElemAt: ["$data.total", 0] } } },
                                totalAmt: {
                                    $sum: "$totalAmt"
                                },
                            }
                        },
                            function (err, targetResult) {

                                if (err) {
                                    console.log(err)
                                }
                                cb(false, targetResult)
                            });
                    } else {
                        cb(false, result);

                    }

                });
            }
        }, function (err, asyncResult) {
            finalObject = [];
            if (asyncResult.target.length > 0 && asyncResult.primaryAndSecondory.length > 0) {


                for (let i = 0; i < asyncResult.target.length; i++) {
                    let details = [];
                    let targetData = 0;
                    let primaryData = 0;
                    let productName = [];


                    for (const month of monthYearArray) {
                        const matchedObjectTarget = asyncResult.target[i].data.filter(function (obj) {
                            return obj.month == month.month && obj.year == month.year;
                        });

                        if (matchedObjectTarget.length > 0) {
                            matchedObjectTarget.forEach(element => {
                                targetData = + element.total
                                if (element.hasOwnProperty("productName")) {
                                    productName.push(
                                        element.productName
                                    )
                                }

                            });
                            // targetData=matchedObjectTarget[0].total;
                            // productName=matchedObjectTarget[0].hasOwnProperty("productName")? matchedObjectTarget[0].productName :"";
                        } else {
                            targetData = 0;
                        }
                        if (asyncResult.primaryAndSecondory[i] != undefined) {
                            const matchedObjectPrimary = asyncResult.primaryAndSecondory[i].data.filter(function (obj) {
                                return obj.month == month.month && obj.year == month.year;
                            });
                            if (matchedObjectPrimary.length > 0) {
                                primaryData = matchedObjectPrimary[0].total
                            } else {
                                primaryData = 0;
                            }
                        } else {
                            primaryData = 0;
                        }


                        if (params.targetType == "amountwise") {
                            details.push({
                                targetData: targetData,
                                primaryData: primaryData
                            })
                        } else {
                            details.push({
                                targetData: targetData,
                                productName: productName,
                                primaryData: primaryData
                            })
                        }
                    }
                    finalObject.push({
                        hqName: asyncResult.target[i]._id.districtName,
                        stateName: asyncResult.target[i]._id.stateName,
                        userName: asyncResult.target[i]._id.userName,
                        details: details,
                        totalAmtTarget: asyncResult.target[i].totalAmt,
                        totalAmtPrimary: asyncResult.primaryAndSecondory[i] == undefined ? 0 : asyncResult.primaryAndSecondory[i].totalAmt

                    });
                }

                return cb(false, finalObject);
            }
            else {
                let finalObject = [];
                if (asyncResult.target.length > 0) {
                    for (let i = 0; i < asyncResult.target.length; i++) {
                        let details = [];
                        let targetData = 0;
                        let primaryData = 0;
                        for (const month of monthYearArray) {
                            const matchedObjectTarget = asyncResult.target[i].data.filter(function (obj) {
                                return obj.month == month.month && obj.year == month.year;
                            });
                            if (params.targetType == "productwise") {
                                if (matchedObjectTarget.length > 0) {
                                    matchedObjectTarget.forEach((item) => {
                                        details.push({
                                            targetData: item.total,
                                            primaryData: primaryData,
                                            productName: item.productName
                                        })
                                    })
                                } else {
                                    targetData = 0;
                                }
                            } else {
                                if (matchedObjectTarget.length > 0) {
                                    targetData = matchedObjectTarget[0].total;
                                    productName = matchedObjectTarget[0].hasOwnProperty("productName") ? matchedObjectTarget[0].productName : "";
                                } else {
                                    targetData = 0;
                                }
                                details.push({
                                    targetData: targetData,
                                    primaryData: primaryData
                                })
                            }
                        }
                        finalObject.push({
                            hqName: asyncResult.target[i]._id.districtName,
                            stateName: asyncResult.target[i]._id.stateName,
                            userName: asyncResult.target[i]._id.userName,
                            details: details,
                            totalAmtTarget: asyncResult.target[i].totalAmt,
                            totalAmtPrimary: 0

                        });
                    }

                    return cb(false, finalObject);
                }
                else {
                    return cb(false, []);
                }

            }

        })


    }
    Primaryandsalereturn.remoteMethod(
        'getTargetVsAchievementData', {
        description: 'getTargetVsAchievementData',
        accepts: [{
            arg: 'params',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    ////----------end --------------
    //YYYY-MM-DD
    function monthNameAndYearBTWTwoDate(fromDate, toDate) {
        let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let arr = [];
        let datFrom = new Date(fromDate);
        let datTo = new Date(toDate);
        let fromYear = datFrom.getFullYear();
        let toYear = datTo.getFullYear();
        let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
        for (let i = datFrom.getMonth(); i <= diffYear; i++) {
            arr.push({
                monthName: monthNames[i % 12],
                month: (i % 12) + 1,
                year: Math.floor(fromYear + (i / 12)),
                endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
            });
        }
        return arr;
    }

    //----------Preeti Arora get product wise target vs achievment--- 27-03-2021----------------------

    Primaryandsalereturn.getTargetVsAchievementVsPrimaryVsClosing = function (params, cb) {
        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);

        let filterPrimary = {};
        let filterTarget = {};
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        let groupTarget = {};
        let lookupTarget = {};
        let projectTarget = {
            _id: 0,
            month: "$_id.month",
            year: "$_id.year",
            ProductName: { $arrayElemAt: ["$productName", 0] },
            productId: "$_id.productId",
            targetQty: "$totalTarget",
            creditValue: "$prodData.creditValue",
            productCode: "$prodData.productCode"
        };
        let sort = {};
        // const fromDate = moment(params.fromDate,'DD-MM-YYYY').format('YYYY-MM-DD');
        // const toDate = moment(params.toDate,'DD-MM-YYYY').format('YYYY-MM-DD');

        const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
        let month = [];
        let year = [];
        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);
        }
        let filterSSS = { companyId: ObjectId(params.companyId) }
        filterPrimary.fromDate = params.fromPrimDate;
        filterPrimary.toDate = params.toPrimDate;
        filterPrimary.type = "Primary";
        filterPrimary.APIEndPoint = params.APIEndPoint;
        filterTarget.targetMonth = { $in: month }
        filterTarget.targetYear = { $in: year }
        filterSSS.month = { $in: month }
        filterSSS.year = { $in: year }

        if (params.type == "State") {
            let code = []
            params.stateName.forEach(async (data) => {
                code.push(data)
            });
            filterPrimary = {
                type: "Primary",
                userId: params.userId,
                APIEndPoint: params.APIEndPoint
            }
            groupTarget = {
                _id: {
                    productId: '$productId',
                    "month": "$targetMonth",
                    "year": "$targetYear",
                    stateId: "$stateId"
                },
                totalTarget: {
                    $sum: "$targetQuantity"
                },
                productName: { $addToSet: "$productName" },
            }
            lookupTarget = {
                "from": "State",
                "localField": "_id.stateId",
                "foreignField": "_id",
                "as": "stateData"
            }
            projectTarget["stateName"] = { $arrayElemAt: ["$stateData.stateName", 0] };
            sort = {
                stateName: 1
            }
            Primaryandsalereturn.app.models.State.find({ where: { stateName: { inq: code } } }, function (err, res) {
                if (err) {
                    return cb(err)
                }
                if (res.length > 0) {
                    res.forEach(data => {
                        stateIdsArr.push(ObjectId(data.id));
                    })

                    filterTarget = {
                        companyId: ObjectId(params.companyId),
                        stateId: { $in: stateIdsArr },
                    }
                    filterSSS.stateId = { $in: empArray };
                    filterTarget.targetMonth = { $in: month }
                    filterTarget.targetYear = { $in: year }
                    async.parallel({
                        primary: function (cb) {
                            filterPrimary.stateCode = params.stateCode;
                            filterPrimary.fromDate = params.fromPrimDate;
                            filterPrimary.toDate = params.toPrimDate;
                            Primaryandsalereturn.app.models.State.getStateWiseProductDetails(filterPrimary,
                                function (err, result) {
                                    if (err) {
                                        cb(false, err)
                                    } else {
                                        let amount = 0;
                                        if (result != undefined) {
                                            result.forEach(primData => {
                                                amount = (primData.qty * primData.creditvalue).toFixed(2);
                                                primData["amount"] = amount;
                                            });
                                            cb(null, result);
                                        } else {
                                            cb(null, []);
                                        }


                                    }

                                })
                        },
                        target: function (cb) {
                            TargetCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterTarget
                                },

                                // Stage 2
                                {
                                    $group: groupTarget
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "Products",
                                        "localField": "_id.productId",
                                        "foreignField": "_id",
                                        "as": "prodData"
                                    }
                                },

                                // Stage 4
                                {
                                    $unwind: {
                                        path: "$prodData",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },

                                // Stage 5
                                {
                                    $lookup: lookupTarget
                                },

                                // Stage 6
                                {
                                    $project: projectTarget
                                },
                                {
                                    $sort: sort
                                },
                                function (err, result) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    if (result.length > 0) {
                                        let amount = 0;
                                        result.forEach(targetData => {
                                            amount = (targetData.targetQty * targetData.creditValue).toFixed(2);
                                            targetData["amount"] = amount;
                                        });
                                        return cb(null, result);
                                    } else {
                                        return cb(null, []);
                                    }

                                });

                        },
                        SSS: function (cb) {
                            PrimaryandsalereturnCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterSSS
                                },

                                // Stage 2
                                {
                                    $group: {
                                        _id: { productId: "$productId" },
                                        productName: { $addToSet: "$productName" },
                                        totalPrimaryAmt: { $addToSet: "$totalPrimaryAmt" },
                                        totalSaleReturnAmt: { $addToSet: "$totalSaleReturnAmt" },
                                        totalSecondrySaleAmt: { $addToSet: "$totalSecondrySaleAmt" },
                                        closingAmt: { $addToSet: "$closingAmt" }
                                    }
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "Products",
                                        "localField": "_id.productId",
                                        "foreignField": "_id",
                                        "as": "prod"
                                    }
                                },

                                // Stage 4
                                {
                                    $project: {
                                        ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                        ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                        totalPrimaryAmt: { $arrayElemAt: ["$totalPrimaryAmt", 0] },
                                        totalSaleReturnAmt: { $arrayElemAt: ["$totalSaleReturnAmt", 0] },
                                        totalSecondrySaleAmt: { $arrayElemAt: ["$totalSecondrySaleAmt", 0] },
                                        closingAmt: { $arrayElemAt: ["$closingAmt", 0] },
                                    }
                                }, function (err, res) {
                                    console.log("res->", res);
                                    if (err) {
                                        cb(false, [])
                                    }
                                    if (res.length > 0) {
                                        cb(false, res)
                                    } else {
                                        cb(false, [])

                                    }
                                }

                            );

                        }
                    },
                        function (err, asyncResult) {
                            if (err) {
                                return cb(err);
                            }
                            finalObject = [];
                            let returnData = {};
                            let targetAmt = 0, primaryAmt = 0;
                            let totalAmtTarget = 0;
                            let totalAmtPrimary = 0;
                            let finalReturnData = [];
                            let totalClosingAmt = 0, totalSecondaryAmt = 0;
                            let PerAchAmt = 0;

                            if (asyncResult.target.length > 0) {
                                asyncResult.target.forEach(target => {
                                    let matchedObj = asyncResult.primary.filter(primary => {
                                        return primary.ProductCode == target.productCode && primary.StateName == target.stateName
                                    })
                                    if (params.hasOwnProperty('viewType')) {
                                        targetAmt = params.viewType == "unit" ? target.targetQty : target.amount
                                    } else {
                                        targetAmt = parseInt(target.amount)
                                    }
                                    if (matchedObj.length > 0) {
                                        if (params.hasOwnProperty('viewType')) {
                                            primaryAmt = params.viewType == "unit" ? parseInt(matchedObj[0].qty) : parseInt(matchedObj[0].amount)
                                        } else {
                                            primaryAmt = parseInt(matchedObj[0].amount)
                                        }

                                        if (target.amount != 0) {
                                            PerAchAmt = ((primaryAmt / targetAmt) * 100).toFixed(2)
                                        } else {
                                            PerAchAmt = 0
                                        }
                                        totalAmtTarget += parseInt(targetAmt);
                                        totalAmtPrimary += parseInt(primaryAmt);
                                        finalObject.push({
                                            stateName: target.stateName,
                                            productName: target.ProductName,
                                            targetAmount: targetAmt,
                                            primaryAmount: primaryAmt,
                                            achievementAmount: parseInt(PerAchAmt),
                                            closingAmount: 0,
                                            secondaryAmount: 0,
                                        });
                                    } else {
                                        totalAmtTarget += parseInt(targetAmt);
                                        finalObject.push({
                                            stateName: target.stateName,
                                            productName: target.ProductName,
                                            targetAmount: targetAmt,
                                            primaryAmount: 0,
                                            achievementAmount: 0,
                                            closingAmount: 0,
                                            secondaryAmount: 0,
                                        });
                                    }
                                    // asyncResult.primary.forEach(primary => {
                                    //     if(primary.ProductCode==target.productCode){
                                    //            let index= finalObject.findIndex(res=>{
                                    //                       return target.ProductName == res.productName
                                    //                     })
                                    //                     if(index >-1){
                                    //                         totalAmtTarget += parseInt(target.amount);
                                    //                         totalAmtPrimary += parseInt(primary.amount);
                                    //                         finalObject[index].targetAmount=parseInt(finalObject[index]["targetAmount"]) + parseInt(target.amount)
                                    //                         finalObject[index].primaryAmount=parseInt(finalObject[index]["primaryAmount"]) + parseInt(primary.amount)
                                    //                       }else{
                                    //                         totalAmtTarget += parseInt(target.amount);
                                    //                         totalAmtPrimary += parseInt(primary.amount)
                                    //                         finalObject.push({
                                    //                           productName:target.ProductName,
                                    //                           targetAmount:target.amount,
                                    //                           primaryAmount:primary.amount,
                                    //                           closingAmount:0,
                                    //                           secondaryAmount:0,
                                    //                         });
                                    //                       }
                                    //     }

                                    //   });

                                });



                            }
                            returnData = {
                                details: finalObject,
                                totalAmtTarget: totalAmtTarget,
                                totalAmtPrimary: totalAmtPrimary,
                                totalPerAchAmt: ((totalAmtPrimary / totalAmtTarget) * 100).toFixed(2),
                                totalClosingAmt,
                                totalSecondaryAmt
                            }
                            finalReturnData.push(returnData)



                            return cb(false, finalReturnData)


                        })

                } else {
                    return cb(false, [])
                }
            })


        } else if (params.type == "Headquarter") {
            let code = [];
            params.headquarterName.forEach(async (data) => {
                code.push(data)
            })
            filterPrimary = {
                APIEndPoint: params.APIEndPoint
            }
            groupTarget = {
                _id: {
                    productId: '$productId',
                    "month": "$targetMonth",
                    "year": "$targetYear",
                    districtId: "$districtId"
                },
                totalTarget: {
                    $sum: "$targetQuantity"
                },
                productName: { $addToSet: "$productName" },
            }
            lookupTarget = {
                "from": "District",
                "localField": "_id.districtId",
                "foreignField": "_id",
                "as": "disData"
            }
            projectTarget["districtName"] = { $arrayElemAt: ["$disData.districtName", 0] };
            projectTarget["districtId"] = "$_id.districtId";
            sort = {
                districtName: 1
            }
            Primaryandsalereturn.app.models.District.find({ where: { districtName: { inq: code } } }, async function (err, res) {
                if (err) {
                    return cb(err)
                }
                if (res.length > 0) {
                    for (let i = 0; i < res.length; i++) {
                        let obj = {
                            where: {
                                companyId: params.companyId,
                                status: true,
                                mappedDistrictId: res[i].id
                            }
                        }
                        let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);
                        if (mappedData.length > 0) {
                            res[i]["mappedWithPoolHQ"] = mappedData[0].districtId;
                        }



                    }
                    let PoolHQId = [];
                    let hqIds = [];
                    let hqCodes = [];
                    let groupPoolUsers = [];
                    res.forEach(element => {
                        if (element.hasOwnProperty("mappedWithPoolHQ")) {
                            PoolHQId.push({ poolDistrictId: element.mappedWithPoolHQ, disName: element.districtName, districtId: element.id, erpHqCode: element.erpHqCode ? element.erpHqCode : 0 })
                        } else {
                            hqIdArray.push(ObjectId(element.id));
                            if (element.hasOwnProperty("erpHqCode")) {
                                hqCodes.push(element.erpHqCode);
                            }
                        }
                        hqIds.push(ObjectId(element.id));
                    });
                    //-------------------------------------grouping the pool IDS------------------------
                    PoolHQId.forEach(item => {
                        const index = groupPoolUsers.findIndex(x => (x.poolDistrictId).toString() == (item.poolDistrictId)).toString()
                        if (index == -1) {
                            groupPoolUsers.push(item);
                        }
                    })
                    groupPoolUsers.forEach(element => {
                        hqIdArray.push(ObjectId(element.districtId));
                        hqCodes.push(element.erpHqCode)
                    });
                    //------------------------------------------------------------------------------------
                    filterTarget = {
                        companyId: ObjectId(params.companyId),
                        districtId: { $in: hqIdArray }
                    }
                    async.parallel({
                        primary: function (cb) {
                            filterPrimary.hqCode = params.hqCode.toString();
                            filterPrimary.flag = "PS";
                            filterPrimary.fromDate = params.fromPrimDate;
                            filterPrimary.toDate = params.toPrimDate;
                            Primaryandsalereturn.app.models.State.getNewHQWiseProductDetails(filterPrimary,
                                async function (err, result) {
                                    if (err) {
                                        cb(false, err)
                                    } else {
                                        let amount = 0;
                                        let total = 0;
                                        let qty = 0;
                                        let finalRes = [];
                                        if (result != undefined) {
                                            for (let i = 0; i <= result.length; i++) {
                                                let primData = result[i];
                                                if (primData != undefined) {
                                                    let DisObj = {
                                                        where: {
                                                            erpHqCode: (primData.HQCode).toString()
                                                        }
                                                    }
                                                    let hqIds = await Primaryandsalereturn.app.models.District.find(DisObj);
                                                    if (hqIds.length > 0) {
                                                        primData["districtId"] = hqIds[0].id;
                                                    }
                                                    amount = (primData.primarysaleqty * primData.itemrate).toFixed(2);
                                                    primData["amount"] = amount;
                                                    primData["districtName"] = primData.HQName;
                                                    total = parseInt(total) + parseInt(amount);
                                                    qty = parseInt(qty) + parseInt(primData.primarysaleqty);



                                                }


                                            };
                                            for (let i = 0; i < result.length; i++) {
                                                let obj = {
                                                    where: {
                                                        companyId: params.companyId,
                                                        status: true,
                                                        mappedDistrictId: result[i].districtId
                                                    }
                                                }
                                                let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);
                                                if (mappedData.length > 0) {
                                                    result[i]["mappedDistrictId"] = mappedData[0].districtId;
                                                } else {
                                                    result[i]["mappedDistrictId"] = result[i].districtId;
                                                }
                                            }

                                            for (let i = 0; i < result.length; i++) {
                                                const item = result[i]
                                                if (item.hasOwnProperty("mappedDistrictId")) {
                                                    const index = finalRes.findIndex(x => ((x.mappedDistrictId).toString() == (item.mappedDistrictId).toString() && x.productcode == item.productcode))
                                                    if (index != -1) {
                                                        finalRes[index].primarysaleqty += item.primarysaleqty;
                                                        finalRes[index].primarysaleamount += item.primarysaleamount;
                                                        finalRes[index].salereturnqty += item.salereturnqty;
                                                        finalRes[index].salereturnamount += item.salereturnamount;
                                                        finalRes[index].expiryqty += item.expiryqty;
                                                        finalRes[index].expiryamount += item.expiryamount;
                                                        finalRes[index].breakageqty += item.breakageqty;
                                                        finalRes[index].breakageamount += item.breakageamount;
                                                        finalRes[index].amount += item.amount;
                                                    } else {
                                                        finalRes.push({
                                                            StateCode: item.StateCode,
                                                            StateName: item.StateName,
                                                            HQCode: item.HQCode,
                                                            HQName: item.HQName,
                                                            productcode: item.productcode,
                                                            productname: item.productname,
                                                            primarysaleqty: item.primarysaleqty,
                                                            primarysaleamount: item.primarysaleamount,
                                                            salereturnqty: item.salereturnqty,
                                                            salereturnamount: item.salereturnamount,
                                                            expiryqty: item.expiryqty,
                                                            expiryamount: item.expiryamount,
                                                            breakageqty: item.breakageqty,
                                                            breakageamount: item.breakageamount,
                                                            itemrate: item.itemrate,
                                                            districtId: item.districtId,
                                                            amount: item.amount,
                                                            districtName: item.districtName,
                                                            mappedDistrictId: item.mappedDistrictId,
                                                        });
                                                    }
                                                } else {
                                                    finalRes.push({
                                                        StateCode: item.StateCode,
                                                        StateName: item.StateName,
                                                        HQCode: item.HQCode,
                                                        HQName: item.HQName,
                                                        productcode: item.productcode,
                                                        productname: item.productname,
                                                        primarysaleqty: item.primarysaleqty,
                                                        primarysaleamount: item.primarysaleamount,
                                                        salereturnqty: item.salereturnqty,
                                                        salereturnamount: item.salereturnamount,
                                                        expiryqty: item.expiryqty,
                                                        expiryamount: item.expiryamount,
                                                        breakageqty: item.breakageqty,
                                                        breakageamount: item.breakageamount,
                                                        itemrate: item.itemrate,
                                                        districtId: item.districtId,
                                                        amount: item.amount,
                                                        districtName: item.districtName,
                                                        mappedDistrictId: item.mappedDistrictId,
                                                    });
                                                }
                                            }


                                            finalRes["totalPrimary"] = total;
                                            finalRes["totalPrimaryQty"] = qty;

                                            cb(false, finalRes);
                                        } else {
                                            cb(false, []);
                                        }
                                    }

                                })
                        },
                        target: function (cb) {
                            filterTarget.targetMonth = { $in: month }
                            filterTarget.targetYear = { $in: year }

                            TargetCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterTarget
                                },

                                // Stage 2
                                {
                                    $group: groupTarget
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "Products",
                                        "localField": "_id.productId",
                                        "foreignField": "_id",
                                        "as": "prodData"
                                    }
                                },

                                // Stage 4
                                {
                                    $unwind: {
                                        path: "$prodData",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },

                                // Stage 5
                                {
                                    $lookup: lookupTarget
                                },

                                // Stage 6
                                {
                                    $project: projectTarget
                                },
                                {
                                    $sort: sort
                                },
                                async function (err, result) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    if (result.length > 0) {
                                        let amount = 0;
                                        let totalTarget = 0;
                                        let totalTargetQty = 0;
                                        for (let i = 0; i < result.length; i++) {
                                            amount = (result[i].targetQty * (result[i].creditValue ? result[i].creditValue : 1)).toFixed(2);
                                            result[i]["amount"] = amount;
                                            totalTarget += parseInt(amount);
                                            totalTargetQty += parseInt(result[i].targetQty);
                                            let obj = {
                                                where: {
                                                    companyId: params.companyId,
                                                    status: true,
                                                    mappedDistrictId: result[i].districtId
                                                }
                                            }
                                            let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);
                                            if (mappedData.length > 0) {
                                                result[i]["mappedDistrictId"] = mappedData[0].districtId;
                                            } else {
                                                result[i]["mappedDistrictId"] = result[i].districtId;
                                            }
                                            let DisObj = {
                                                where: {
                                                    id: result[i].mappedDistrictId
                                                }
                                            }
                                            let regionHQName = await Primaryandsalereturn.app.models.District.find(DisObj);
                                            if (regionHQName != undefined) {
                                                result[i].districtName = regionHQName[0].districtName;
                                            }
                                        }
                                        result["totalTarget"] = totalTarget;
                                        result["totalTargetQty"] = totalTargetQty;
                                        cb(false, result);
                                    } else {
                                        cb(false, [])
                                    }

                                });

                        },
                        SSS: function (cb) {
                            filterSSS["districtId"] = { $in: hqIds };
                            PrimaryandsalereturnCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterSSS
                                },

                                // Stage 2
                                {
                                    $group: {
                                        _id: {
                                            "productId": '$productId',
                                            "month": "$targetMonth",
                                            "year": "$targetYear",
                                            districtId: "$districtId"
                                        }
                                        ,
                                        productName: { $addToSet: "$productName" },
                                        totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
                                        primaryQty: { $sum: "$totalPrimaryQty" },
                                        totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
                                        totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
                                        closingAmt: { $sum: "$closingAmt" },
                                        closingQty: { $sum: "$closing" },
                                        secondaryQty: { $sum: "$totalSecondrySaleQty" },
                                    }
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "Products",
                                        "localField": "_id.productId",
                                        "foreignField": "_id",
                                        "as": "prod"
                                    }
                                },
                                // Stage 4
                                {
                                    $project: {
                                        ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                        productId: { $arrayElemAt: ["$prod._id", 0] },
                                        ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                        totalPrimaryAmt: 1,
                                        primaryQty: 1,
                                        totalSaleReturnAmt: 1,
                                        totalSecondrySaleAmt: 1,
                                        districtId: "$_id.distirctId",
                                        closingAmt: 1,
                                        primaryFreeQty: 1,
                                        closingQty: 1,
                                        secondaryQty: 1,
                                    }
                                }, async function (err, res) {
                                    if (err) {
                                        cb(false, [])
                                    }
                                    var totalClosing = 0, closingQty = 0, primaryFreeQty = 0;
                                    var totalSecondary = 0, secondaryQty = 0;
                                    let finalRes = []
                                    if (res.length > 0) {
                                        for (let i = 0; i < res.length; i++) {
                                            let obj = {
                                                where: {
                                                    companyId: params.companyId,
                                                    status: true,
                                                    mappedDistrictId: res[i]._id.districtId
                                                }
                                            }
                                            let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);
                                            if (mappedData.length > 0) {
                                                res[i]["mappedDistrictId"] = mappedData[0].districtId;
                                            }
                                        }



                                        for (let i = 0; i < res.length; i++) {
                                            totalClosing = parseInt(totalClosing) + parseInt(res[i].closingAmt);
                                            totalSecondary = parseInt(totalSecondary) + parseInt(res[i].totalSecondrySaleAmt);
                                            closingQty = parseInt(closingQty) + parseInt(res[i].closingQty);
                                            secondaryQty = parseInt(secondaryQty) + parseInt(res[i].secondaryQty);
                                            primaryFreeQty = parseInt(primaryFreeQty) + parseInt(res[i].primaryFreeQty ? res[i].primaryFreeQty : 0);

                                            const item = res[i]
                                            if (item.hasOwnProperty("mappedDistrictId")) {
                                                const index = finalRes.findIndex(x => ((x.mappedDistrictId).toString() == (item.mappedDistrictId).toString() && x.ProductCode == item.ProductCode))
                                                if (index != -1) {
                                                    finalRes[index].totalPrimaryAmt += item.totalPrimaryAmt;
                                                    finalRes[index].primaryQty += item.primaryQty;
                                                    finalRes[index].totalSaleReturnAmt += item.totalSaleReturnAmt;
                                                    finalRes[index].totalSecondrySaleAmt += item.totalSecondrySaleAmt;
                                                    finalRes[index].closingAmt += item.closingAmt;
                                                    finalRes[index].closingQty += item.closingQty;
                                                    finalRes[index].secondaryQty += item.secondaryQty;
                                                    finalRes[index].secondaryQty += item.secondaryQty;
                                                } else {
                                                    finalRes.push({
                                                        totalPrimaryAmt: item.totalPrimaryAmt,
                                                        primaryQty: item.primaryQty,
                                                        totalSaleReturnAmt: item.totalSaleReturnAmt,
                                                        totalSecondrySaleAmt: item.totalSecondrySaleAmt,
                                                        closingAmt: item.closingAmt,
                                                        closingQty: item.closingQty,
                                                        secondaryQty: item.secondaryQty,
                                                        ProductName: item.ProductName,
                                                        productId: item.productId,
                                                        ProductCode: item.ProductCode,
                                                        mappedDistrictId: item.mappedDistrictId,
                                                        districtId: item._id.districtId
                                                    });
                                                }
                                            } else {
                                                finalRes.push({
                                                    totalPrimaryAmt: item.totalPrimaryAmt,
                                                    primaryQty: item.primaryQty,
                                                    totalSaleReturnAmt: item.totalSaleReturnAmt,
                                                    totalSecondrySaleAmt: item.totalSecondrySaleAmt,
                                                    closingAmt: item.closingAmt,
                                                    closingQty: item.closingQty,
                                                    secondaryQty: item.secondaryQty,
                                                    ProductName: item.ProductName,
                                                    productId: item.productId,
                                                    ProductCode: item.ProductCode,
                                                    districtId: item._id.districtId,
                                                    mappedDistrictId: item._id.districtId,
                                                });
                                            }
                                        }
                                        finalRes["totalClosing"] = totalClosing;
                                        finalRes["totalSecondary"] = totalSecondary;
                                        finalRes["closingQty"] = closingQty;
                                        finalRes["secondaryQty"] = secondaryQty;
                                        finalRes["primaryFreeQty"] = primaryFreeQty;

                                        cb(false, finalRes)
                                    } else {
                                        res["totalClosing"] = totalClosing;
                                        res["totalSecondary"] = totalSecondary;
                                        res["closingQty"] = closingQty;
                                        res["secondaryQty"] = secondaryQty;
                                        res["primaryFreeQty"] = primaryFreeQty
                                        cb(false, res)

                                    }
                                }

                            );

                        }
                    }, function (err, asyncResult) {
                        if (err) { return cb(err) }
                        finalObject = [];
                        let returnData = {};
                        let targetAmt = 0, primaryAmt = 0;
                        let targetQty = 0, primaryQty = 0;
                        let finalReturnData = [];
                        let PerAchAmt = 0, perAchQty = 0;

                        if (asyncResult.target.length > 0) {
                            asyncResult.target.forEach(async (target, i) => {

                                let matchedObj = asyncResult.primary.filter(primary => {
                                    return primary.productcode == target.productCode && (primary.mappedDistrictId).toString() == (target.mappedDistrictId).toString()
                                })
                                let matchedSecondaryObj = asyncResult.SSS.filter(sss => {
                                    return sss.ProductCode == target.productCode && (sss.mappedDistrictId).toString() == (target.mappedDistrictId).toString()
                                })
                                if (matchedObj.length > 0) {
                                    primaryQty = parseInt(matchedObj[0].primarysaleqty)
                                    primaryAmt = parseInt(matchedObj[0].amount)
                                    if (target.amount != 0) {
                                        targetAmt = target.amount;
                                        targetQty = target.targetQty;
                                        PerAchAmt = ((primaryAmt / targetAmt) * 100).toFixed(2);
                                        perAchQty = ((primaryQty / targetQty) * 100).toFixed(2);
                                    } else {
                                        PerAchAmt = 0;
                                        perAchQty = 0;
                                    }
                                    finalObject.push({
                                        districtName: target.districtName,
                                        productName: target.ProductName,
                                        targetAmount: parseInt(target.amount),
                                        targetQty: parseInt(target.targetQty),
                                        primaryAmount: parseInt(matchedObj[0].amount),
                                        primaryQty: parseInt(matchedObj[0].primarysaleqty),
                                        achievementAmount: PerAchAmt,
                                        achievementQty: perAchQty,
                                        closingAmount: 0,
                                        totalClosingQty: 0,
                                        secondaryAmount: 0,
                                        secondaryQty: 0,
                                        mappedDistrictId: matchedObj[0].mappedDistrictId
                                    });
                                } else {
                                    finalObject.push({
                                        districtName: target.districtName,
                                        productName: target.ProductName,
                                        targetAmount: parseInt(target.amount),
                                        targetQty: parseInt(target.targetQty),
                                        primaryAmount: 0,
                                        primaryQty: 0,
                                        achievementQty: 0,
                                        achievementAmount: 0,
                                        closingAmount: 0,
                                        totalClosingQty: 0,
                                        secondaryAmount: 0,
                                        secondaryQty: 0,
                                        mappedDistrictId: matchedObj[0].target

                                    });
                                }
                                if (matchedSecondaryObj.length > 0) {
                                    const index = finalObject.findIndex(res => (matchedSecondaryObj[0].ProductName == res.productName) && ((res.mappedDistrictId).toString() == (matchedSecondaryObj[0].mappedDistrictId).toString()))
                                    if (index != -1) {
                                        finalObject[index]["closingAmount"] = matchedSecondaryObj[0].closingAmt;
                                        finalObject[index]["secondaryAmount"] = matchedSecondaryObj[0].totalSecondrySaleAmt;
                                        finalObject[index]["totalClosingQty"] = matchedSecondaryObj[0].closingQty;
                                        finalObject[index]["secondaryQty"] = matchedSecondaryObj[0].secondaryQty;
                                    }
                                }
                            })

                        } else if (asyncResult.primary.length > 0) {
                            asyncResult.primary.forEach(prim => {
                                totalAmtPrimary += parseInt(prim.amount);
                                finalObject.push({
                                    districtName: prim.districtName,
                                    productName: prim.ProductName,
                                    targetAmount: 0,
                                    targetQty: 0,
                                    primaryAmount: parseInt(prim.amount),
                                    primaryQty: parseInt(prim.primarysaleqty),
                                    achievementAmount: 0,
                                    achievementQty: 0,
                                    closingAmount: 0,
                                    closingQty: 0,
                                    secondaryAmount: 0,
                                    secondaryQty: 0
                                });
                            });
                        } else if (asyncResult.SSS.length > 0) {
                            asyncResult.SSS.forEach(prim => {
                                totalSecondaryAmt += parseInt(prim.amount);
                                finalObject.push({
                                    districtName: prim.districtName,
                                    productName: prim.ProductName,
                                    targetAmount: 0,
                                    primaryAmount: parseInt(prim.totalPrimaryAmt),
                                    primaryQty: parseInt(prim.primaryQty),
                                    achievementAmount: 0,
                                    achievementQty: 0,
                                    closingAmount: parseInt(prim.closingAmt),
                                    closingQty: parseInt(prim.closingQty),
                                    secondaryAmount: parseInt(prim.totalSecondrySaleAmt),
                                    secondaryQty: parseInt(prim.secondaryQty),
                                });
                            });
                        }

                        returnData = {
                            details: finalObject,
                            totalAmtTarget: (asyncResult.target.totalTarget || 0),
                            totalAmtTargetQty: (asyncResult.target.totalTargetQty || 0),
                            totalAmtPrimary: (asyncResult.primary.totalPrimary || 0),
                            totalPrimaryQty: (asyncResult.primary.totalPrimaryQty || 0),
                            totalPerAchAmt: (((asyncResult.primary.totalPrimary || 0) / asyncResult.target.totalTarget) * 100).toFixed(2),
                            totalPerAchAmtQty: ((asyncResult.primary.totalPrimaryQty / asyncResult.target.totalTargetQty) * 100).toFixed(2),
                            totalClosingAmt: (asyncResult.SSS.totalClosing || 0),
                            totalClosingQty: (asyncResult.SSS.closingQty || 0),
                            totalSecondaryAmt: (asyncResult.SSS.totalSecondary || 0),
                            totalSecondaryQty: (asyncResult.SSS.secondaryQty || 0),
                        }
                        finalReturnData.push(returnData);

                        return cb(false, finalReturnData)


                    })

                } else {
                    return cb(false, [])
                }
            })

        } else if (params.type == "Employee Wise") {
            let districtIdsMrr = [];
            let districtIdsMrrForTarget = [];
            let districtIdsErp = [];
            let empArray = [];
            let where = {}
            let month = [];
            let year = [];
            if (params.hasOwnProperty("userIds")) {
                params.userIds.forEach(element => {
                    empArray.push(ObjectId(element))
                });
                where["companyId"] = ObjectId(params.companyId);
                where["userId"] = { inq: empArray }
            }
            Primaryandsalereturn.app.models.UserInfo.find({ where: where }, function (err, userInfo) {
                if (err) { return cb(null, err) }
                //console.log('userInfo', userInfo);
                let filter;
                if (userInfo.length > 0 && userInfo[0].designationLevel > 1) {
                    filter = { companyId: ObjectId(params.companyId), supervisorId: { '$in': empArray } };
                } else {
                    filter = { companyId: ObjectId(params.companyId), userId: { '$in': empArray } };
                }
                Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchyWithUniqueHQs(filter, async function (err, hierarchy) {
                    if (err) { return cb(err) }
                    //console.log('heirarchy', hierarchy);
                    let selectedHqDetails = hierarchy[0]['uniqueDistricts'];
                    let HeadquarterDetails = [];
                    for (let index = 0; index < selectedHqDetails.length; index++) {
                        const element = selectedHqDetails[index];
                        if (element.hasOwnProperty("erpCodeForMainHQ") && element.erpCodeForMainHQ) {
                            let duplicateIndex = selectedHqDetails.findIndex(code => code.erpHqCode == element.erpCodeForMainHQ);
                            if (duplicateIndex == -1) {
                                districtIdsErp.push(parseInt(element.erpCodeForMainHQ));
                                selectedHqDetails.push({
                                    "districtId": element.mappedWithPoolDistrictId,
                                    "erpHqCode": element.erpCodeForMainHQ
                                })
                                let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.mappedWithPoolDistrictId), status: true } });
                                if (hqMappedWithPoolDetails.length > 0) {
                                    const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                                    districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                                    hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                                        hqMappedWithPool = hqMappedWithPool.toJSON();
                                        districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                                        HeadquarterDetails.push({
                                            "districtId": hqMappedWithPool.mappedDistrictId,
                                            "erpHqCode": ""
                                        })
                                    });
                                }
                            }
                        } else {
                            if (element.erpHqCode) {
                                districtIdsErp.push(parseInt(element.erpHqCode));
                                HeadquarterDetails.push({
                                    "districtId": element.districtId,
                                    "erpHqCode": element.erpHqCode
                                })
                            }
                            if (element.hasOwnProperty("isPoolDistrict") && element.isPoolDistrict[params.companyId]) {
                                let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.districtId), status: true } });
                                if (hqMappedWithPoolDetails.length > 0) {
                                    const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                                    districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                                    hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                                        hqMappedWithPool = hqMappedWithPool.toJSON();
                                        districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                                        HeadquarterDetails.push({
                                            "districtId": hqMappedWithPool.mappedDistrictId,
                                            "erpHqCode": ""
                                        })
                                    });
                                }
                            } else {
                                districtIdsMrr.push(ObjectId(element.districtId));
                                districtIdsMrrForTarget.push(ObjectId(element.districtId));
                            }
                        }
                    }

                    //Async Start 
                    const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                    for (var i = 0; i < monthYearArray.length; i++) {
                        month.push(monthYearArray[i].month);
                        year.push(monthYearArray[i].year);
                    }
                    let filterPrimary = {};
                    filterPrimary.hqCode = districtIdsErp.toString();
                    filterPrimary.flag = "PS";
                    filterPrimary.fromDate = params.fromPrimDate;
                    filterPrimary.toDate = params.toPrimDate;
                    filterPrimary.APIEndPoint = params.APIEndPoint;
                    let filterTarget = {
                        companyId: ObjectId(params.companyId),
                        districtId: { $in: districtIdsMrrForTarget },
                        // targetMonth: { $in: month },
                        // targetYear: { $in: year }
                    }
                    filterTarget.fromDate = { $gte: new Date(params.fromDate) };
                    filterTarget.toDate = { $lte: new Date(params.toDate) };
                    let filterSSS = {
                        companyId: ObjectId(params.companyId),
                        districtId: { $in: districtIdsMrr },
                        month: { $in: month },
                        year: { $in: year }
                    }
                    let projectTarget = {
                        _id: 0,
                        //month: "$_id.month",
                        //year: "$_id.year",
                        //ProductName: { $arrayElemAt: ["$productName", 0] },
                        productId: "$_id.productId",
                        //districtId: "$_id.districtId",
                        targetQty: "$totalTarget",
                        //creditValue: "$prodData.creditValue",
                        //productCode: "$prodData.productCode"

                    };
                    groupTarget = {
                        _id: {
                            productId: '$productId',
                            //"month": "$targetMonth",
                            // "year": "$targetYear",
                            //districtId: "$districtId"
                        },
                        totalTarget: {
                            $sum: "$targetQuantity"
                        }
                    }
                    Primaryandsalereturn.app.models.Products.find({ where: { companyId: params.companyId, status: true, assignedDivision: ObjectId(params.divisionId) }, order: "productName ASC", limit: 1 }, async function (err, productDetailsFromMRRMaster) {
                        async.parallel({
                            primary: function (cb) {
                                Primaryandsalereturn.app.models.State.getNewHQWiseProductDetails(filterPrimary,
                                    function (err, result) {
                                        if (err) {
                                            cb(null, err)
                                        }
                                        cb(null, result)
                                    })
                            },
                            target: function (cb) {
                                TargetCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: filterTarget
                                    },

                                    // Stage 2
                                    {
                                        $group: groupTarget
                                    },

                                    // Stage 6
                                    {
                                        $project: projectTarget
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        cb(null, result)
                                    });

                            },
                            SSS: function (cb) {
                                PrimaryandsalereturnCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: filterSSS
                                    },

                                    // Stage 2
                                    {
                                        $group: {
                                            _id: {
                                                "productId": '$productId',
                                                //"netRate": "$netRate",
                                                //  "month": "$month",
                                                //  "year": "$year",
                                                // "districtId": "$districtId"
                                            },
                                            //productName: { $addToSet: "$productName" },
                                            openingQty: { $sum: "$opening" },
                                            totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
                                            primaryQty: { $sum: "$totalPrimaryQty" },
                                            totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
                                            totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
                                            closingAmt: { $sum: "$closingAmt" },
                                            closingQty: { $sum: "$closing" },
                                            primaryFreeQty: { $sum: "$primaryFreeQty" },
                                            returnFreeSale: { $sum: "$returnFreeSale" },
                                            secondaryQty: { $sum: "$totalSecondrySaleQty" },
                                            expiryQty: { $sum: "$expiryQty" },
                                            breakageQty: { $sum: "$breakageQty" },
                                            batchRecallQty: { $sum: "$batchRecallQty" },
                                        }
                                    },

                                    // Stage 4
                                    {
                                        $project: {
                                            //ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                            productId: "$_id.productId",
                                            // districtId: "$_id.districtId",
                                            //netRate: { $arrayElemAt: ["$prod.netRate", 0] },
                                            //ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                            openingQty: 1,
                                            totalPrimaryAmt: 1,
                                            primaryQty: 1,
                                            totalSaleReturnAmt: 1,
                                            totalSecondrySaleAmt: 1,
                                            closingAmt: 1,
                                            primaryFreeQty: 1,
                                            returnFreeSale: 1,
                                            closingQty: 1,
                                            secondaryQty: 1,
                                            expiryQty: 1,
                                            breakageQty: 1,
                                            batchRecallQty: 1
                                        }
                                    }, function (err, res) {
                                        if (err) {
                                            cb(null, [])
                                        }
                                        cb(null, res)
                                    }

                                );

                            }
                        }, async function (err, asyncResult) {
                            if (err) { return cb(null, err) }

                            let finalArrayOfObject = [];
                            let totalTargetQty = 0, totalTargetAmt = 0.0, totalOpeningQty = 0, totalOpeningAmt = 0, totalPrimaryQty = 0,
                                totalPrimaryAmt = 0.0, totalPrimaryFreeQty = 0, totalPrimaryFreeAmt = 0, totalSecondaryQty = 0, totalSecondaryAmt = 0.0,
                                totalSalereturnqty = 0, totalSalereturnamount = 0, totalReturnFreeSale = 0, totalReturnFreeAmt = 0, totalExpiryqty = 0, totalExpiryamount = 0,
                                totalBreakageqty = 0, totalBreakageamount = 0, totalClosingQty = 0, totalClosingAmt = 0.0;
                            let i = 0;
                            if (productDetailsFromMRRMaster.length > 0) {
                                productDetailsFromMRRMaster.forEach(productMasterFromMrr => {
                                    finalObject = {};
                                    finalObject['productName'] = productMasterFromMrr.productName;
                                    finalObject['productCode'] = productMasterFromMrr.productCode;
                                    finalObject['productType'] = productMasterFromMrr.productType;
                                    finalObject['productRate'] = productMasterFromMrr.creditValue;
                                    let priQty = 0, priAmt = 0.0, saleReturnQty = 0, saleReturnAmt = 0.0, expiryQty = 0, expiryAmt = 0.0, breakageQty = 0, breakageAmt = 0.0,
                                        targetQty = 0, targetAmt = 0.0, secondaryQty = 0, secondaryAmt = 0.0, closingQty = 0; closingAmt = 0.0, openingQty = 0, openingAmt = 0.0, primaryFreeQty = 0,
                                            primaryFreeAmt = 0.0, returnFreeSale = 0, returnFreeAmt = 0.0;
                                    //HeadquarterDetails.forEach(hqMasterFromSelection => {
                                    //finalObject['districtName'] = hqMasterFromSelection.districtName;
                                    //finalObject['districtCode'] = hqMasterFromSelection.districtCode;
                                    if (asyncResult.primary != undefined && asyncResult.primary.length > 0) {
                                        let foundPrimaryProductrray = asyncResult.primary.filter(primary => {
                                            return parseInt(primary.productcode) == parseInt(productMasterFromMrr.productCode)
                                        })
                                        if (foundPrimaryProductrray && foundPrimaryProductrray.length > 0) {
                                            foundPrimaryProductrray.forEach(foundPrimary => {
                                                priQty = parseInt(priQty) + parseInt(foundPrimary.primarysaleqty);
                                                priAmt = parseFloat(priAmt) + ((foundPrimary.primarysaleqty) * (finalObject['productRate']))//foundPrimary[0].primarysaleamount;
                                                saleReturnQty = parseInt(saleReturnQty) + parseInt(foundPrimary.salereturnqty);
                                                saleReturnAmt = parseFloat(saleReturnAmt) + ((foundPrimary.salereturnqty) * (finalObject['productRate']))//foundPrimary[0].salereturnamount;
                                                expiryQty = parseInt(expiryQty) + parseInt(foundPrimary.expiryqty);
                                                expiryAmt = parseFloat(expiryAmt) + ((foundPrimary.expiryqty) * (finalObject['productRate']))//foundPrimary[0].expiryamount;
                                                breakageQty = parseInt(breakageQty) + parseInt(foundPrimary.breakageqty);
                                                breakageAmt = parseFloat(breakageAmt) + ((foundPrimary.breakageqty) * (finalObject['productRate']))//foundPrimary[0].breakageamount;
                                            });
                                        }
                                    } else {
                                        priQty += 0;
                                        priAmt += 0.0;
                                        saleReturnQty += 0;
                                        saleReturnAmt += 0.0;
                                        expiryQty += 0;
                                        expiryAmt += 0.0;
                                        breakageQty += 0;
                                        breakageAmt += 0.0;
                                    }

                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        let foundTarget = asyncResult.target.filter(target => {
                                            return (target.productId).toString() == (productMasterFromMrr.id).toString()
                                        })
                                        if (foundTarget.length > 0) {
                                            targetAmt = parseFloat(targetAmt) + parseFloat((foundTarget[0].targetQty * (finalObject['productRate'] ? finalObject['productRate'] : 1)).toFixed(2));
                                            targetQty = parseInt(targetQty) + parseInt(foundTarget[0].targetQty);
                                            //achObj['PerAchAmt'] = (((primaryObj['primaryAmt'] == 0 ? 0 : primaryObj['primaryAmt']) / (targetObj['targetAmt'] == 0 ? 1 : targetObj['targetAmt'])) * 100).toFixed(2);
                                            //achObj['perAchQty'] = (((primaryObj['primaryQty'] == 0 ? 0 : primaryObj['primaryQty']) / (targetObj['targetQty'] == 0 ? 1 : targetObj['targetQty'])) * 100).toFixed(2);
                                        }
                                    } else {
                                        targetAmt += 0.0;
                                        targetQty += 0;
                                        //achObj['PerAchAmt'] = 0.0;
                                        //achObj['perAchQty'] = 0;
                                    }

                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        let foundSSS = asyncResult.SSS.filter(sss => {

                                            return (sss.productId).toString() == (productMasterFromMrr.id).toString()
                                        })
                                        if (foundSSS.length > 0) {
                                            openingQty = parseInt(openingQty) + parseInt(foundSSS[0].openingQty);
                                            openingAmt = parseFloat(openingAmt) + parseFloat((foundSSS[0].openingQty * finalObject['productRate']));
                                            secondaryQty = parseInt(secondaryQty) + parseInt(foundSSS[0].secondaryQty ? foundSSS[0].secondaryQty : 0);
                                            secondaryAmt = parseFloat(secondaryAmt) + parseFloat(foundSSS[0].totalSecondrySaleAmt ? (foundSSS[0].totalSecondrySaleAmt).toFixed(2) : 0.0);
                                            closingQty = parseInt(closingQty) + parseInt(foundSSS[0].closingQty);
                                            closingAmt = parseFloat(closingAmt) + parseFloat((foundSSS[0].closingAmt).toFixed(2));
                                            primaryFreeQty = parseInt(primaryFreeQty) + foundSSS[0].primaryFreeQty ? parseInt(foundSSS[0].primaryFreeQty) : 0;
                                            primaryFreeAmt = parseFloat(primaryFreeAmt) + parseFloat(foundSSS[0].primaryFreeQty ? (foundSSS[0].primaryFreeQty * finalObject['productRate']) : 0.0);
                                            returnFreeSale = parseInt(returnFreeSale) + parseInt(foundSSS[0].returnFreeSale ? foundSSS[0].returnFreeSale : 0);
                                            returnFreeAmt = parseFloat(returnFreeAmt) + parseFloat(foundSSS[0].returnFreeSale ? (foundSSS[0].returnFreeSale * finalObject['productRate']) : 0.0);
                                        }

                                    } else {
                                        openingQty = openingQty + 0;
                                        openingAmt = openingAmt + 0.0;
                                        secondaryQty = secondaryQty + 0;
                                        secondaryAmt = secondaryAmt + 0.0;
                                        closingQty = closingQty + 0;
                                        closingAmt = closingAmt + 0.0;
                                        primaryFreeQty = primaryFreeQty + 0;
                                        primaryFreeAmt = primaryFreeAmt + 0.0;
                                        returnFreeSale = returnFreeSale + 0;
                                        returnFreeAmt = returnFreeAmt + 0.0;
                                    }

                                    //});
                                    if (params.viewType == "unit") {
                                        finalArrayOfObject.push({
                                            //hqName: finalObject['districtName'],
                                            //hqCode: finalObject['districtCode'],
                                            userName: "----",
                                            productName: finalObject['productName'],
                                            //productcode: finalObject['productCode'],
                                            //productrate: finalObject['productRate'],
                                            targetAmount: targetQty,
                                            //targetAmt: parseFloat((targetAmt).toFixed(2)),
                                            primaryAmount: priQty,
                                            //primaryAmt: parseFloat((priAmt).toFixed(2)),
                                            achievementAmount: parseFloat(((priQty / targetQty) * 100).toFixed(2)),
                                            //achievementAmt: parseFloat(((priAmt / targetAmt) * 100).toFixed(2)),
                                            secondaryAmount: secondaryQty,
                                            //secondaryAmt: parseFloat((secondaryAmt).toFixed(2)),
                                            closingAmount: closingQty,
                                            //closingAmt: parseFloat((closingAmt).toFixed(2)),
                                            //returnQty: saleReturnQty,
                                            //returnAmt: parseFloat((saleReturnAmt).toFixed(2)),
                                            //openingQty: openingQty,
                                            //openingAmt: parseFloat((openingAmt).toFixed(2)),
                                            //primaryFreeQty: primaryFreeQty,
                                            //primaryFreeAmt: parseFloat((primaryFreeAmt).toFixed(2)),
                                            //returnFreeSale: returnFreeSale,
                                            //returnFreeAmt: parseFloat((returnFreeAmt).toFixed(2))
                                        });
                                        totalTargetQty = totalTargetQty + targetQty;
                                        totalPrimaryQty = totalPrimaryQty + priQty;
                                        //totalPerAchAmtQty = totalPerAchAmtQty + 0.0;
                                        totalSecondaryQty = totalSecondaryQty + secondaryQty;
                                        totalClosingQty = totalClosingQty + closingQty;

                                    } else {
                                        finalArrayOfObject.push({
                                            //hqName: finalObject['districtName'],
                                            //hqCode: finalObject['districtCode'],
                                            userName: "----",
                                            productName: finalObject['productName'],
                                            //productcode: finalObject['productCode'],
                                            //productrate: finalObject['productRate'],
                                            //targetQty: targetQty,
                                            targetAmount: parseFloat((targetAmt).toFixed(2)),
                                            //primaryQty: priQty,
                                            primaryAmount: parseFloat((priAmt).toFixed(2)),
                                            //achievementQty: parseFloat(((priQty / targetQty) * 100).toFixed(2)),
                                            achievementAmount: parseFloat(((priAmt / targetAmt) * 100).toFixed(2)),
                                            //secondaryQty: secondaryQty,
                                            secondaryAmount: parseFloat((secondaryAmt).toFixed(2)),
                                            //closingQty: closingQty,
                                            closingAmount: parseFloat((closingAmt).toFixed(2)),
                                            //returnQty: saleReturnQty,
                                            //returnAmt: parseFloat((saleReturnAmt).toFixed(2)),
                                            //openingQty: openingQty,
                                            //openingAmt: parseFloat((openingAmt).toFixed(2)),
                                            //primaryFreeQty: primaryFreeQty,
                                            //primaryFreeAmt: parseFloat((primaryFreeAmt).toFixed(2)),
                                            //returnFreeSale: returnFreeSale,
                                            //returnFreeAmt: parseFloat((returnFreeAmt).toFixed(2))
                                        });
                                        totalTargetAmt = totalTargetAmt + targetAmt;
                                        totalPrimaryAmt = totalPrimaryAmt + priAmt;
                                        //totalPerAchAmt = totalPerAchAmt + 0.0;
                                        totalSecondaryAmt = totalSecondaryAmt + secondaryAmt;
                                        totalClosingAmt = totalClosingAmt + closingAmt;
                                    }
                                });

                            }
                            let total = {};
                            let data;
                            if (params.viewType == "unit") {
                                data = [{
                                    details: finalArrayOfObject, "totalAmtTarget": totalTargetQty,
                                    "totalAmtPrimary": totalPrimaryQty,
                                    "totalPerAchAmt": parseFloat(((totalPrimaryQty / totalTargetQty) * 100).toFixed(2)),
                                    "totalSecondaryAmt": totalSecondaryQty,
                                    "totalClosingAmt": totalClosingQty,
                                }]
                            } else {
                                data = [{
                                    details: finalArrayOfObject, "totalAmtTarget": totalTargetAmt,
                                    "totalAmtPrimary": totalPrimaryAmt,
                                    "totalPerAchAmt": parseFloat(((totalPrimaryAmt / totalTargetAmt) * 100).toFixed(2)),
                                    "totalSecondaryAmt": totalSecondaryAmt,
                                    "totalClosingAmt": totalClosingAmt
                                }]

                            }
                            return cb(null, data)
                        })
                    });
                    //END
                })
            })
        } else {
            return cb(false, [])
        }
    }
    Primaryandsalereturn.remoteMethod(
        'getTargetVsAchievementVsPrimaryVsClosing', {
        description: 'get Target Vs Achievement Vs Primary Vs Closing',
        accepts: [{
            arg: 'params',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );
    //=----------------end-------------------------------------------------------

    //---------------------------get Product List from ERP for SSS submission-------------------
    // Primaryandsalereturn.getProductListForSSS = function (params, cb) {
    //     var self = this;
    //     const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
    //     let json = { userID: params.employeeCode, PartyCode: params.PartyCode }
    //     json['FromDate'] = params.fromDate;
    //     json['ToDate'] = params.toDate;
    //     let url = params.APIEndPoint + "ProductDetails";
    //     const request = require('request');
    //     request({
    //         url: url, // Online url
    //         method: "POST",
    //         headers: {
    //             // header info - in case of authentication enabled   
    //             "Content-Type": "application/json",
    //         }, //json        
    //         // body goes here 
    //         json: json
    //     }, function (err, res, body) {
    //         if (!err) {
    //             let prodData = await Primaryandsalereturn.app.models.Products.find({
    //                 where: {
    //                     companyId: params.companyId,
    //                     productCode: parseInt(products.productcode),
    //                     status: true
    //                 }
    //             })

    //             let finalObj = []
    //             if (body.item.length > 0) {
    //                 body.item.forEach(async (products, i, arr) => {
    //                     let prodData = await Primaryandsalereturn.app.models.Products.find({
    //                         where: {
    //                             companyId: params.companyId,
    //                             productCode: parseInt(products.productcode),
    //                             status: true
    //                         }
    //                     })
    //                     finalObj.push({
    //                         productName: prodData[0].productName,
    //                         productType: prodData[0].productType,
    //                         productId: prodData[0].id,
    //                         netRate: products.productrate,
    //                         splRate: 0,
    //                         opening: 0,
    //                         primarySale: products.primarysaleqty,
    //                         primaryFreeQty: products.primaryfreeqty,
    //                         returnSale: products.salereturnqty,
    //                         returnFreeSale: products.salereturnfreeqty,
    //                         breakage: 0,
    //                         saleable: 0,
    //                         other: 0,
    //                         scheme: 0,
    //                         closing: 0,
    //                     })
    //                     if (finalObj.length == body.item.length) {
    //                         return cb(false, finalObj)
    //                     }

    //                 });
    //             } else {
    //                 return cb(false, [])
    //             }

    //         } else {
    //             sendMail.sendMail({ collectionName: 'State', errorObject: err, paramsObject: param, methodName: 'getManagerData' });
    //         }

    //     });


    // }
    /* Primaryandsalereturn.getProductListForSSS = function (params, cb) {
         var self = this;
         let json = { userID: params.employeeCode, PartyCode: params.PartyCode };
         let fromDate=moment(params.fromDate,"DD-MM-YYYY").format("YYYY-DD-MM");
         let toDate=moment(params.toDate,"DD-MM-YYYY").format("YYYY-DD-MM");
         json['FromDate'] = fromDate;
         json['ToDate'] = toDate;
         let url = params.APIEndPoint + "ProductDetails";
         const request = require('request');
         request({
             url: url, // Online url
             method: "POST",
             headers: {
                 // header info - in case of authentication enabled   
                 "Content-Type": "application/json",
             }, //json        
             // body goes here 
             json: json
         }, async function (err, res, body) {
             if (!err) {
                 let finalObj = [];
                 // getting product details from the MRR data base
                 let prodData = await Primaryandsalereturn.app.models.Products.find({
                     where: {
                         companyId: "5fd9d874c5461d19808d83db",
                         status: true,
                         "assignedDivision" :{ inq:["6017270e7d19e108d4aa28ea"]}
                     },order: "productName ASC"
                 });
                 //------get the previous month of opening--------
                 let openingMonth=params.month-1;
                 if(prodData.length>0){
                     prodData.forEach(products => {
                         // compare and get the data like primary and return from ERP
                         if(body.item != undefined){
                            let matchedObj= body.item.filter(erpProd=>{
                                return parseInt(erpProd.productcode) === parseInt(products.productCode)
                             })
                             console.log("matchedObj:  ",matchedObj);
                             if(matchedObj.length>0){
                             finalObj.push({
                             productName: matchedObj[0].productname,
                             productType: products.productType,
                             productId: products.id,
                             netRate: matchedObj[0].productrate,
                             splRate: 0,
                             opening: 0,
                             primarySale: matchedObj[0].primarysaleqty,
                             primaryFreeQty: matchedObj[0].primaryfreeqty,
                             returnSale: 0,
                             returnFreeSale:0,
                             breakageQty: 0,
                             expiryQty:matchedObj[0].salereturnqty,
                             batchRecallQty:0,
                             breakage: 0,
                             saleable: 0,
                             other: 0,
                             scheme: 0,
                             closing: 0,
                         })
                         }else{
                          finalObj.push({
                             productName: products.productName,
                             productType: products.productType,
                             productId: products.id,
                             netRate: products.creditValue,
                             splRate: 0,
                             opening: 0,
                             primarySale: 0,
                             primaryFreeQty: 0,
                             returnSale: 0,
                             returnFreeSale: 0,
                             breakageQty: 0,
                             expiryQty:0,
                             batchRecallQty:0,
                             breakage: 0,
                             saleable: 0,
                             other: 0,
                             scheme: 0,
                             closing: 0,
                         })
                         }
                         }
                          else{
                             finalObj.push({
                                 productName: products.productName,
                                 productType: products.productType,
                                 productId: products.id,
                                 netRate: products.creditValue,
                                 splRate: 0,
                                 opening: 0,
                                 primarySale: 0,
                                 primaryFreeQty: 0,
                                 returnSale: 0,
                                 returnFreeSale: 0,
                                 breakageQty: 0,
                                 expiryQty:0,
                                 batchRecallQty:0,
                                 breakage: 0,
                                 saleable: 0,
                                 other: 0,
                                 scheme: 0,
                                 closing: 0,
                             })
                         }                    
                     });
                   
                     // let prodId = await Primaryandsalereturn.app.models.Providers.find({
                     //     where: {
                     //         companyId: params.companyId,
                     //         providerCode:params.PartyCode,
                     //         status: true
                     //     }
                     // })
 
                     //getting previous month of opening data-------
                     let openingData=await Primaryandsalereturn.find({
                         where: {
                             companyId: params.companyId,
                             userId:params.userId,
                             month:openingMonth,
                             partyId:params.partyId,
                             year:params.year
                         }
                     });
                     if(openingData.length>0){
                         finalObj.forEach((element,index) => {
                             let matchedObj= openingData.filter(erpProd=>{
                                 return (erpProd.productId).toString() == (element.productId).toString()
                              })
                              if(matchedObj.length>0){
                                 finalObj[index].opening=matchedObj[0].closing
                              }
                         });
                         return cb(false,finalObj) 
                     }else{
                         return cb(false,finalObj) 
                     }
                    
                 }else{
                     return cb(false, []) 
                 }
             } else {
                 return cb(false,err) 
             }
 
         });
 
 
     }
    Primaryandsalereturn.remoteMethod(
         'getProductListForSSS', {
         description: 'get Product List For SSS',
         accepts: [{
             arg: 'params',
             type: 'object',
             http: { source: 'body' }
         }],
         returns: {
             root: true,
             type: 'array'
         },
         http: {
             verb: 'post'
         }
     }
     );*/




    //-----------------------------------------
    Primaryandsalereturn.getProductListForSSS = function (params, cb) {
        console.log("params : ", params);
        console.log("params.rL : ", params.rL);
        var self = this;
        Primaryandsalereturn.find(
            {
                where: {
                    companyId: params.companyId,
                    userId: params.userId,
                    partyId: params.partyId,
                    month: params.month,
                    year: params.year,
                },
            },
            function (err, response) {

                let finalObj = [];
                if (response.length > 0) {
                    if (params.hasOwnProperty("rL") && params.rL == 0) {
                        response.forEach((products) => {
                            finalObj.push({
                                productName: products.productName,
                                productType: products.productCode,
                                productId: products.productId,
                                netRate: products.netRate,
                                splRate: products.splRate,
                                opening: products.opening,
                                primarySale: products.totalPrimaryQty,
                                primaryFreeQty: products.primaryfreeqty,
                                returnSale: products.totalSecondrySaleQty,
                                returnFreeSale: products.salereturnfreeqty,
                                breakage: products.breakage,
                                saleable: products.saleable,
                                other: products.other,
                                scheme: products.scheme,
                                closing: products.closing,
                            });
                        });
                        return cb(false, finalObj);
                    } else {
                        const err = new Error("Oops!!");
                        err.statusCode = 401;
                        err.message = "Data Is already submitted..";
                        console.log("err : -------------------", err);
                        return cb(err);
                    }
                } else {

                    let json = { userID: params.employeeCode, PartyCode: params.PartyCode };
                    let fromDate = moment(params.fromDate, "DD-MM-YYYY").format("YYYY-DD-MM");
                    let toDate = moment(params.toDate, "DD-MM-YYYY").format("YYYY-DD-MM");
                    json['FromDate'] = fromDate;
                    json['ToDate'] = toDate;
                    let url = params.APIEndPoint + "ProductDetails";
                    const request = require('request');
                    request({
                        url: url, // Online url
                        method: "POST",
                        headers: {
                            // header info - in case of authentication enabled   
                            "Content-Type": "application/json",
                        }, //json        
                        // body goes here 
                        json: json
                    }, async function (err, res, body) {
                        if (!err) {
                            let finalObj = [];
                            // getting product details from the MRR data base
                            let prodData = await Primaryandsalereturn.app.models.Products.find({
                                where: {
                                    companyId: "5fd9d874c5461d19808d83db",
                                    status: true,
                                    "assignedDivision": { inq: ["6017270e7d19e108d4aa28ea"] }
                                }, order: "productName ASC"
                            });
                            //------get the previous month of opening--------
                            let openingMonth = params.month - 1;

                            if (prodData.length > 0) {
                                prodData.forEach(products => {
                                    // compare and get the data like primary and return from ERP
                                    if (body.item != undefined) {
                                        let matchedObj = body.item.filter(erpProd => {
                                            return parseInt(erpProd.productcode) === parseInt(products.productCode)
                                        })
                                        console.log("matchedObj:  ", matchedObj);
                                        if (matchedObj.length > 0) {
                                            finalObj.push({
                                                productName: matchedObj[0].productname,
                                                productType: products.productType,
                                                productId: products.id,
                                                netRate: matchedObj[0].productrate,
                                                splRate: 0,
                                                opening: 0,
                                                primarySale: matchedObj[0].primarysaleqty,
                                                primaryFreeQty: matchedObj[0].primaryfreeqty,
                                                returnSale: 0,
                                                returnFreeSale: 0,
                                                breakageQty: 0,
                                                expiryQty: matchedObj[0].salereturnqty,
                                                batchRecallQty: 0,
                                                breakage: 0,
                                                saleable: 0,
                                                other: 0,
                                                scheme: 0,
                                                closing: 0,
                                            })
                                        } else {
                                            finalObj.push({
                                                productName: products.productName,
                                                productType: products.productType,
                                                productId: products.id,
                                                netRate: products.creditValue,
                                                splRate: 0,
                                                opening: 0,
                                                primarySale: 0,
                                                primaryFreeQty: 0,
                                                returnSale: 0,
                                                returnFreeSale: 0,
                                                breakageQty: 0,
                                                expiryQty: 0,
                                                batchRecallQty: 0,
                                                breakage: 0,
                                                saleable: 0,
                                                other: 0,
                                                scheme: 0,
                                                closing: 0,
                                            })
                                        }
                                    }
                                    else {
                                        finalObj.push({
                                            productName: products.productName,
                                            productType: products.productType,
                                            productId: products.id,
                                            netRate: products.creditValue,
                                            splRate: 0,
                                            opening: 0,
                                            primarySale: 0,
                                            primaryFreeQty: 0,
                                            returnSale: 0,
                                            returnFreeSale: 0,
                                            breakageQty: 0,
                                            expiryQty: 0,
                                            batchRecallQty: 0,
                                            breakage: 0,
                                            saleable: 0,
                                            other: 0,
                                            scheme: 0,
                                            closing: 0,
                                        })
                                    }
                                });

                                // let prodId = await Primaryandsalereturn.app.models.Providers.find({
                                //     where: {
                                //         companyId: params.companyId,
                                //         providerCode:params.PartyCode,
                                //         status: true
                                //     }
                                // })

                                //getting previous month of opening data-------
                                let openingData = await Primaryandsalereturn.find({
                                    where: {
                                        companyId: params.companyId,
                                        userId: params.userId,
                                        month: openingMonth,
                                        partyId: params.partyId,
                                        year: params.year
                                    }
                                });
                                if (openingData.length > 0) {
                                    finalObj.forEach((element, index) => {
                                        let matchedObj = openingData.filter(erpProd => {
                                            return (erpProd.productId).toString() == (element.productId).toString()
                                        })
                                        if (matchedObj.length > 0) {
                                            finalObj[index].opening = matchedObj[0].closing
                                        }
                                    });
                                    return cb(false, finalObj)
                                } else {
                                    return cb(false, finalObj)
                                }

                            } else {
                                return cb(false, [])
                            }
                        } else {
                            return cb(false, err)
                        }

                    });
                }
            }
        );

    };
    Primaryandsalereturn.remoteMethod("getProductListForSSS", {
        description: "get Product List For SSS",
        accepts: [
            {
                arg: "params",
                type: "object",
                http: { source: "body" },
            },
        ],
        returns: {
            root: true,
            type: "array",
        },
        http: {
            verb: "post",
        },
    });

    //-----------------------------------------
    //----------------getTargetVsAchPrimaryERPdata------ MCW company-02-05-2021-----Rahul CHoudhary-------------

    Primaryandsalereturn.getTargetVsAchPrimaryERPdata = function (param, cb) {
        var self = this;
        let PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);

        let TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        let filterPrimary = {};
        let filterTarget = {};
        let stateIdsArr = [];
        let hqIdArray = [];
        let groupTarget = {};
        let lookupTarget = {};
        let projectTarget = {};
        let groupProductTarget = {};
        let lookupProductTarget = {};
        let projectProdustTarget = {};
        let empID;

        let sort = {};
        // let fromDate = new moment.utc(param.year + "-" + param.month + "-01").startOf('month').format("YYYY-MM-DD");
        // let toDate = new moment.utc(param.year + "-" + param.month + "-01").endOf("month").format("YYYY-MM-DD");

        let fromDate = new moment.utc(param.year + "-" + param.month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
        let toDate = new moment.utc(param.year + "-" + param.month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");


        const monthYearArray = monthNameAndYearBTWTwoDate(fromDate, toDate);
        let month = [];
        let year = [];

        for (var i = 0; i < monthYearArray.length; i++) {
            month.push(monthYearArray[i].month);
            year.push(monthYearArray[i].year);
        }

        for (let j = 0; j < param.stateIds.length; j++) {
            stateIdsArr.push(ObjectId(param.stateIds[j]));
        }
        let filterSSS = {
            companyId: ObjectId(param.companyId),
            stateId: { $in: stateIdsArr },
        }
        filterPrimary.fromDate = fromDate;
        filterPrimary.toDate = toDate;
        filterPrimary.type = "Primary";
        filterPrimary.APIEndPoint = param.APIEndPoint;
        // filterTarget = {
        //     companyId: ObjectId(param.companyId),
        //     stateId: { $in:stateIdsArr},
        //     targetMonth: { $in: month },
        //     targetYear: { $in: year }
        // }
        if (param.type == "State") {

        } else if (param.type == "headquarter") {
            Primaryandsalereturn.app.models.District.find({
                where: { assignedTo: ObjectId(param.companyId), stateId: { inq: stateIdsArr } }

            }, async function (err, res) {

                if (err) {
                    console.log("err ", err);
                    return cb(err)
                }
                if (res.length > 0) {
                    for (let i = 0; i < res.length; i++) {
                        let obj = {
                            where: {
                                companyId: param.companyId,
                                status: true,
                                mappedDistrictId: res[i].id
                            }
                        }

                        let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);

                        if (mappedData.length > 0) {
                            res[i]["mappedWithPoolHQ"] = mappedData[0].districtId
                        }
                    }

                    let PoolHQId = [];
                    let groupPoolUsers = [];
                    res.forEach(element => {
                        if (element.hasOwnProperty("mappedWithPoolHQ")) {
                            PoolHQId.push({ poolDistrictId: element.mappedWithPoolHQ, disName: element.districtName, districtId: element.id })
                        } else {
                            hqIdArray.push(ObjectId(element.id));
                        }
                    });
                    //-------------------------------------grouping the pool IDS------------------------

                    PoolHQId.forEach(item => {
                        const index = groupPoolUsers.findIndex(x => (x.poolDistrictId).toString() == (item.poolDistrictId)).toString()

                        if (index == -1) {
                            groupPoolUsers.push(item);
                        }
                    })
                    groupPoolUsers.forEach(element => {
                        hqIdArray.push(ObjectId(element.districtId));
                    });


                    //------------------------------------------------------------------------------------
                    filterTarget = {
                        companyId: ObjectId(param.companyId),
                        districtId: { $in: hqIdArray }
                    }
                    if (param.userId == undefined || param.userId == null) {
                        empID = "";
                    } else {
                        empID = param.userId;
                    }

                    filterPrimary = {
                        stateCode: param.stateCode.toString(),
                        type: "Primary",
                        userId: empID,
                        APIEndPoint: param.APIEndPoint
                    }

                    groupProductTarget = {
                        _id: {
                            "month": "$targetMonth",
                            "year": "$targetYear",
                            "productId": "$productId",
                            "districtId": "$districtId",
                        },
                        totalTarget: {
                            $sum: "$targetQuantity"
                        }
                    }
                    lookupTarget = {
                        "from": "Products",
                        "localField": "_id.productId",
                        "foreignField": "_id",
                        "as": "prodData"
                    }

                    lookupProductTarget = {
                        "from": "District",
                        "localField": "_id.districtId",
                        "foreignField": "_id",
                        "as": "disData"
                    }

                    projectProdustTarget = {


                        month: '$_id.month',
                        year: '$_id.year',
                        productId: "$_id.productId",
                        targetQty: "$totalTarget",
                        creditValue: "$prodData.creditValue",
                        productCode: "$prodData.productCode",
                        finaltargetvalue: { $multiply: ["$totalTarget", "$prodData.creditValue"] },
                        districtName: { '$arrayElemAt': ['$disData.districtName', 0] },
                        districtId: { '$arrayElemAt': ['$disData._id', 0] },
                        erpHqCode: { '$arrayElemAt': ['$disData.erpHqCode', 0] }
                    };

                    groupTarget = {
                        _id: {
                            "month": "$month",
                            "year": "$year",
                            "districtId": "$districtId",
                            "districtName": "$districtName",
                            "erpHqCode": "$erpHqCode"
                        },
                        finalTarget: {
                            $sum: "$finaltargetvalue"
                        }
                    }

                    projectTarget = {
                        month: "$_id.month",
                        year: "$_id.year",
                        targetQty: "$totalTarget",
                        targetValue: "$finalTarget",
                        districtId: "$_id.districtId",
                        districtName: "$_id.districtName",
                        erpHqCode: "$_id.erpHqCode"
                    };

                    sort = {
                        districtName: 1
                    }


                    async.parallel({
                        primary: function (cb) {
                            filterPrimary.stateIds = param.stateCode;
                            filterPrimary.fromDate = new moment.utc(fromDate).format("DD-MM-YYYY");
                            filterPrimary.toDate = new moment.utc(toDate).format("DD-MM-YYYY");
                            Primaryandsalereturn.app.models.State.getHQWiseDetails(filterPrimary,
                                function (err, Primaryresult) {
                                    if (err) {
                                        cb(false, err)
                                    } else {
                                        cb(false, Primaryresult);
                                    }

                                })
                        },

                        target: function (cb) {

                            filterTarget.targetMonth = { $in: month }
                            filterTarget.targetYear = { $in: year }

                            TargetCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterTarget
                                },

                                // Stage 2
                                {
                                    $group: groupProductTarget
                                },
                                // Stage 5
                                {
                                    $lookup: lookupTarget
                                },
                                // Stage 5
                                {
                                    $lookup: lookupProductTarget
                                },
                                // Stage 5
                                {
                                    $unwind: { path: "$prodData", preserveNullAndEmptyArrays: true }
                                },

                                // Stage 6
                                {
                                    $project: projectProdustTarget
                                },
                                // Stage 6
                                {
                                    $group: groupTarget
                                },
                                // Stage 6
                                {
                                    $project: projectTarget
                                },
                                {
                                    $sort: sort
                                },
                                async function (err, Targetresult) {
                                    if (err) {
                                        return cb(false, err)
                                    }
                                    for (let i = 0; i < Targetresult.length; i++) {

                                        let obj = {
                                            where: {
                                                companyId: param.companyId,
                                                status: true,
                                                mappedDistrictId: Targetresult[i].districtId
                                            }
                                        }
                                        let mappedData = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find(obj);

                                        if (mappedData.length > 0) {
                                            let obj = {
                                                where: {
                                                    id: mappedData[0].districtId
                                                }
                                            }
                                            let regionHQName = await Primaryandsalereturn.app.models.District.find(obj);

                                            if (regionHQName != undefined) {
                                                Targetresult[i].districtName = regionHQName[0].districtName;
                                            }
                                        }

                                    }
                                    cb(false, Targetresult)

                                });

                        },
                        SSS: function (cb) {
                            PrimaryandsalereturnCollection.aggregate(
                                // Stage 1
                                {
                                    $match: filterSSS
                                },

                                // Stage 2
                                {
                                    $group: {
                                        _id: { districtId: "$districtId" },
                                        totalPrimaryAmt: { $addToSet: "$totalPrimaryAmt" },
                                        totalSaleReturnAmt: { $addToSet: "$totalSaleReturnAmt" },
                                        totalSecondrySaleAmt: { $addToSet: "$totalSecondrySaleAmt" },
                                        closingAmt: { $addToSet: "$closingAmt" }
                                    }
                                },
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "_id.districtId",
                                        "foreignField": "_id",
                                        "as": "disData"
                                    }
                                },
                                // Stage 4
                                {
                                    $project: {
                                        districtName: { $arrayElemAt: ["$disData.districtName", 0] },
                                        erpHqCode: { $arrayElemAt: ["$disData.erpHqCode", 0] },
                                        totalPrimaryAmt: { $arrayElemAt: ["$totalPrimaryAmt", 0] },
                                        totalSaleReturnAmt: { $arrayElemAt: ["$totalSaleReturnAmt", 0] },
                                        totalSecondrySaleAmt: { $arrayElemAt: ["$totalSecondrySaleAmt", 0] },
                                        closingAmt: { $arrayElemAt: ["$closingAmt", 0] },
                                    }
                                }, function (err, SSSresult) {
                                    if (err) {
                                        cb(false, [])
                                    }
                                    if (SSSresult.length > 0) {
                                        cb(false, SSSresult)
                                    } else {
                                        cb(false, [])

                                    }
                                }

                            );

                        }
                    }, function (err, asyncResult) {
                        if (err) {
                            return cb(err);
                        } else {
                            let finalObject = [];
                            let sssVal = 0;
                            let PerAchAmt = 0;
                            let clossingAmount = 0;
                            if (asyncResult.target.length > 0) {

                                asyncResult.target.forEach(target => {
                                    let matchedObj = asyncResult.primary.filter(primary => {
                                        return primary.HeadquaterCode == target.erpHqCode
                                    })

                                    let matchedObjsss = asyncResult.SSS.filter(SSS => {
                                        return SSS.erpHqCode == target.erpHqCode
                                    })

                                    if (matchedObjsss.length > 0) {
                                        if (matchedObjsss[0].totalSecondrySaleAmt == undefined || matchedObjsss[0].totalSecondrySaleAmt == 0) {
                                            sssVal = 0;
                                        } else {
                                            sssVal = matchedObjsss[0].totalSecondrySaleAmt;
                                        }
                                        if (matchedObjsss[0].closingAmt == undefined || matchedObjsss[0].closingAmt == 0) {
                                            clossingAmount = 0;
                                        } else {
                                            clossingAmount = matchedObjsss[0].closingAmt;
                                        }
                                    } else {
                                        sssVal = 0;
                                        clossingAmount = 0;
                                    }

                                    if (matchedObj.length > 0) {
                                        if (target.targetValue != 0) {
                                            PerAchAmt = ((parseInt(matchedObj[0].primSales) / target.targetValue) * 100).toFixed(2)
                                        } else {
                                            PerAchAmt = 0
                                        }

                                        finalObject.push({
                                            districtName: target.districtName,
                                            districtCode: target.erpHqCode,
                                            target: (target.targetValue).toFixed(2),
                                            primary: parseInt(matchedObj[0].primSales),
                                            perAchAmt: PerAchAmt,
                                            secondary: (sssVal).toFixed(2),
                                            clossing: (clossingAmount).toFixed(2)
                                        });
                                    } else {
                                        finalObject.push({
                                            districtName: target.districtName,
                                            districtCode: target.erpHqCode,
                                            target: 0,
                                            primary: 0,
                                            perAchAmt: 0,
                                            secondary: 0,
                                            clossing: 0
                                        });
                                    }

                                });
                            }
                            return cb(null, finalObject)
                        }

                    });
                }

            })

        }

    };

    Primaryandsalereturn.remoteMethod(
        'getTargetVsAchPrimaryERPdata', {
        description: 'Target Vs Achievement',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    );
    //--------------------------------END-------------------------------------------------------


    //----------Ravindra Singh get product wise target vs achievment HqWise--- 02-06-2021----------------------

    Primaryandsalereturn.getTargetVsAchievementVsPrimaryVsClosingHqWise = function (params, cb) {
        var self = this;
        const PrimaryandsalereturnCollection = self.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        if (params.type == "Employee Wise") {
            let districtIdsMrr = [];
            let districtIdsMrrForTarget = [];
            let districtIdsErp = [];
            let empArray = [];
            let where = {}
            let month = [];
            let year = [];
            if (params.hasOwnProperty("userIds")) {
                params.userIds.forEach(element => {
                    empArray.push(ObjectId(element))
                });
                where["companyId"] = ObjectId(params.companyId);
                where["userId"] = { inq: empArray }
            }
            Primaryandsalereturn.app.models.UserInfo.find({ where: where }, function (err, userInfo) {
                if (err) { return cb(null, err) }
                //console.log('userInfo', userInfo);
                let filter;
                if (userInfo.length > 0 && userInfo[0].designationLevel > 1) {
                    filter = { companyId: ObjectId(params.companyId), supervisorId: { '$in': empArray } };
                } else {
                    filter = { companyId: ObjectId(params.companyId), userId: { '$in': empArray } };
                }
                Primaryandsalereturn.app.models.Hierarchy.getManagerHierarchyWithUniqueHQs(filter, async function (err, hierarchy) {
                    if (err) { return cb(err) }
                    //console.log('heirarchy', hierarchy);
                    let selectedHqDetails = hierarchy[0]['uniqueDistricts'];
                    let HeadquarterDetails = [];
                    for (let index = 0; index < selectedHqDetails.length; index++) {
                        const element = selectedHqDetails[index];
                        if (element.hasOwnProperty("erpCodeForMainHQ") && element.erpCodeForMainHQ) {
                            let duplicateIndex = selectedHqDetails.findIndex(code => code.erpHqCode == element.erpCodeForMainHQ);
                            if (duplicateIndex == -1) {
                                districtIdsErp.push(parseInt(element.erpCodeForMainHQ));
                                selectedHqDetails.push({
                                    "districtId": element.mappedWithPoolDistrictId,
                                    "erpHqCode": element.erpCodeForMainHQ
                                })
                                let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.mappedWithPoolDistrictId), status: true } });
                                if (hqMappedWithPoolDetails.length > 0) {
                                    const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                                    districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                                    hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                                        hqMappedWithPool = hqMappedWithPool.toJSON();
                                        districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                                        HeadquarterDetails.push({
                                            "districtId": hqMappedWithPool.mappedDistrictId,
                                            "erpHqCode": ""
                                        })
                                    });
                                }
                            }
                        } else {
                            if (element.erpHqCode) {
                                districtIdsErp.push(parseInt(element.erpHqCode));
                                HeadquarterDetails.push({
                                    "districtId": element.districtId,
                                    "erpHqCode": element.erpHqCode
                                })
                            }
                            if (element.hasOwnProperty("isPoolDistrict") && element.isPoolDistrict[params.companyId]) {
                                districtIdsMrr.push(ObjectId(element.districtId));
                                //districtIdsMrrForTarget.push(ObjectId(element.districtId));
                                let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.districtId), status: true } });
                                if (hqMappedWithPoolDetails.length > 0) {
                                    const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                                    districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                                    hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                                        hqMappedWithPool = hqMappedWithPool.toJSON();
                                        districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                                        HeadquarterDetails.push({
                                            "districtId": hqMappedWithPool.mappedDistrictId,
                                            "erpHqCode": ""
                                        })
                                    });
                                }
                            } else {
                                districtIdsMrr.push(ObjectId(element.districtId));
                                districtIdsMrrForTarget.push(ObjectId(element.districtId));
                            }
                        }
                    }

                    //Async Start 
                    const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                    for (var i = 0; i < monthYearArray.length; i++) {
                        month.push(monthYearArray[i].month);
                        year.push(monthYearArray[i].year);
                    }
                    let filterPrimary = {};
                    filterPrimary.hqCode = districtIdsErp.toString();
                    filterPrimary.flag = "PS";
                    filterPrimary.fromDate = params.fromPrimDate;
                    filterPrimary.toDate = params.toPrimDate;
                    filterPrimary.APIEndPoint = params.APIEndPoint;
                    let filterTarget = {
                        companyId: ObjectId(params.companyId),
                        districtId: { $in: districtIdsMrrForTarget },
                        fromDate: { $gte: new Date(params.fromDate) },
                        toDate: { $lte: new Date(params.toDate) }
                        // targetMonth: { $in: month },
                        // targetYear: { $in: year }
                    }
                    //filterTarget.fromDate = { $gte: new Date(params.fromDate) };
                    //filterTarget.toDate = { $lte: new Date(params.toDate) };
                    let filterSSS = {
                        companyId: ObjectId(params.companyId),
                        districtId: { $in: districtIdsMrr },
                        fromDate: { $gte: new Date(params.fromDate) },
                        toDate: { $lte: new Date(params.toDate) }
                        //month: { $in: month },
                        //year: { $in: year }
                    }
                    //filterSSS.fromDate = { $gte: new Date(params.fromDate) };
                    //filterSSS.toDate = { $lte: new Date(params.toDate) };
                    let projectTarget = {
                        _id: 0,
                        //month: "$_id.month",
                        //year: "$_id.year",
                        //ProductName: { $arrayElemAt: ["$productName", 0] },
                        productId: "$_id.productId",
                        //districtId: "$_id.districtId",
                        targetQty: "$totalTarget",
                        //creditValue: "$prodData.creditValue",
                        //productCode: "$prodData.productCode"

                    };
                    groupTarget = {
                        _id: {
                            productId: '$productId',
                            //"month": "$targetMonth",
                            // "year": "$targetYear",
                            //districtId: "$districtId"
                        },
                        totalTarget: {
                            $sum: "$targetQuantity"
                        }
                    }
                    Primaryandsalereturn.app.models.Products.find({ where: { companyId: params.companyId, status: true, assignedDivision: ObjectId(params.divisionId) }, order: "productName ASC" }, async function (err, productDetailsFromMRRMaster) {
                        async.parallel({
                            primary: function (cb) {
                                Primaryandsalereturn.app.models.State.getNewHQWiseProductDetails(filterPrimary,
                                    function (err, result) {
                                        if (err) {
                                            cb(null, err)
                                        }
                                        cb(null, result)
                                    })
                            },
                            target: function (cb) {
                                TargetCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: filterTarget
                                    },

                                    // Stage 2
                                    {
                                        $group: groupTarget
                                    },

                                    // Stage 6
                                    {
                                        $project: projectTarget
                                    },
                                    function (err, result) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        cb(null, result)
                                    });

                            },
                            SSS: function (cb) {
                                PrimaryandsalereturnCollection.aggregate(
                                    // Stage 1
                                    {
                                        $match: filterSSS
                                    },

                                    // Stage 2
                                    {
                                        $group: {
                                            _id: {
                                                "productId": '$productId',
                                                //"netRate": "$netRate",
                                                //  "month": "$month",
                                                //  "year": "$year",
                                                // "districtId": "$districtId"
                                            },
                                            //productName: { $addToSet: "$productName" },
                                            openingQty: { $sum: "$opening" },
                                            totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
                                            primaryQty: { $sum: "$totalPrimaryQty" },
                                            totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
                                            totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
                                            closingAmt: { $sum: "$closingAmt" },
                                            closingQty: { $sum: "$closing" },
                                            primaryFreeQty: { $sum: "$primaryFreeQty" },
                                            returnFreeSale: { $sum: "$returnFreeSale" },
                                            secondaryQty: { $sum: "$totalSecondrySaleQty" },
                                            expiryQty: { $sum: "$expiryQty" },
                                            breakageQty: { $sum: "$breakageQty" },
                                            batchRecallQty: { $sum: "$batchRecallQty" },
                                        }
                                    },

                                    // Stage 4
                                    {
                                        $project: {
                                            //ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                            productId: "$_id.productId",
                                            // districtId: "$_id.districtId",
                                            //netRate: { $arrayElemAt: ["$prod.netRate", 0] },
                                            //ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                            openingQty: 1,
                                            totalPrimaryAmt: 1,
                                            primaryQty: 1,
                                            totalSaleReturnAmt: 1,
                                            totalSecondrySaleAmt: 1,
                                            closingAmt: 1,
                                            primaryFreeQty: 1,
                                            returnFreeSale: 1,
                                            closingQty: 1,
                                            secondaryQty: 1,
                                            expiryQty: 1,
                                            breakageQty: 1,
                                            batchRecallQty: 1
                                        }
                                    }, function (err, res) {
                                        if (err) {
                                            cb(null, [])
                                        }
                                        cb(null, res)
                                    }

                                );

                            }
                        }, async function (err, asyncResult) {
                            if (err) { return cb(null, err) }

                            let finalArrayOfObject = [];
                            let totalTargetQty = 0, totalTargetAmt = 0.0, totalOpeningQty = 0, totalOpeningAmt = 0, totalPrimaryQty = 0,
                                totalPrimaryAmt = 0.0, totalPrimaryFreeQty = 0, totalPrimaryFreeAmt = 0, totalSecondaryQty = 0, totalSecondaryAmt = 0.0,
                                totalSalereturnqty = 0, totalSalereturnamount = 0, totalReturnFreeSale = 0, totalReturnFreeAmt = 0, totalExpiryqty = 0, totalExpiryamount = 0,
                                totalBreakageqty = 0, totalBreakageamount = 0, totalClosingQty = 0, totalClosingAmt = 0.0;
                            let i = 0;
                            if (productDetailsFromMRRMaster.length > 0) {
                                productDetailsFromMRRMaster.forEach(productMasterFromMrr => {
                                    finalObject = {};
                                    finalObject['productName'] = productMasterFromMrr.productName;
                                    finalObject['productCode'] = productMasterFromMrr.productCode;
                                    finalObject['productType'] = productMasterFromMrr.productType;
                                    finalObject['productRate'] = productMasterFromMrr.creditValue;
                                    let priQty = 0, priAmt = 0.0, saleReturnQty = 0, saleReturnAmt = 0.0, expiryQty = 0, expiryAmt = 0.0, breakageQty = 0, breakageAmt = 0.0,
                                        targetQty = 0, targetAmt = 0.0, secondaryQty = 0, secondaryAmt = 0.0, closingQty = 0; closingAmt = 0.0, openingQty = 0, openingAmt = 0.0, primaryFreeQty = 0,
                                            primaryFreeAmt = 0.0, returnFreeSale = 0, returnFreeAmt = 0.0;
                                    //HeadquarterDetails.forEach(hqMasterFromSelection => {
                                    //finalObject['districtName'] = hqMasterFromSelection.districtName;
                                    //finalObject['districtCode'] = hqMasterFromSelection.districtCode;
                                    if (asyncResult.primary != undefined && asyncResult.primary.length > 0) {
                                        let foundPrimaryProductrray = asyncResult.primary.filter(primary => {
                                            return parseInt(primary.productcode) == parseInt(productMasterFromMrr.productCode)
                                        })
                                        if (foundPrimaryProductrray && foundPrimaryProductrray.length > 0) {
                                            foundPrimaryProductrray.forEach(foundPrimary => {
                                                priQty = parseInt(priQty) + parseInt(foundPrimary.primarysaleqty);
                                                priAmt = parseFloat(priAmt) + ((foundPrimary.primarysaleqty) * (finalObject['productRate']))//foundPrimary[0].primarysaleamount;
                                                saleReturnQty = parseInt(saleReturnQty) + parseInt(foundPrimary.salereturnqty);
                                                saleReturnAmt = parseFloat(saleReturnAmt) + ((foundPrimary.salereturnqty) * (finalObject['productRate']))//foundPrimary[0].salereturnamount;
                                                expiryQty = parseInt(expiryQty) + parseInt(foundPrimary.expiryqty);
                                                expiryAmt = parseFloat(expiryAmt) + ((foundPrimary.expiryqty) * (finalObject['productRate']))//foundPrimary[0].expiryamount;
                                                breakageQty = parseInt(breakageQty) + parseInt(foundPrimary.breakageqty);
                                                breakageAmt = parseFloat(breakageAmt) + ((foundPrimary.breakageqty) * (finalObject['productRate']))//foundPrimary[0].breakageamount;
                                            });
                                            //console.log('Name : ',productMasterFromMrr.productName,' Qty : ',priQty)
                                        }
                                    } else {
                                        priQty += 0;
                                        priAmt += 0.0;
                                        saleReturnQty += 0;
                                        saleReturnAmt += 0.0;
                                        expiryQty += 0;
                                        expiryAmt += 0.0;
                                        breakageQty += 0;
                                        breakageAmt += 0.0;
                                    }

                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        let foundTarget = asyncResult.target.filter(target => {
                                            return (target.productId).toString() == (productMasterFromMrr.id).toString()
                                        })
                                        if (foundTarget.length > 0) {
                                            targetAmt = parseFloat(targetAmt) + parseFloat((foundTarget[0].targetQty * (finalObject['productRate'] ? finalObject['productRate'] : 1)).toFixed(2));
                                            targetQty = parseInt(targetQty) + parseInt(foundTarget[0].targetQty);
                                            //achObj['PerAchAmt'] = (((primaryObj['primaryAmt'] == 0 ? 0 : primaryObj['primaryAmt']) / (targetObj['targetAmt'] == 0 ? 1 : targetObj['targetAmt'])) * 100).toFixed(2);
                                            //achObj['perAchQty'] = (((primaryObj['primaryQty'] == 0 ? 0 : primaryObj['primaryQty']) / (targetObj['targetQty'] == 0 ? 1 : targetObj['targetQty'])) * 100).toFixed(2);
                                        }
                                    } else {
                                        targetAmt += 0.0;
                                        targetQty += 0;
                                        //achObj['PerAchAmt'] = 0.0;
                                        //achObj['perAchQty'] = 0;
                                    }

                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        let foundSSS = asyncResult.SSS.filter(sss => {

                                            return (sss.productId).toString() == (productMasterFromMrr.id).toString()
                                        })
                                        if (foundSSS.length > 0) {
                                            openingQty = parseInt(openingQty) + parseInt(foundSSS[0].openingQty);
                                            openingAmt = parseFloat(openingAmt) + parseFloat((foundSSS[0].openingQty * finalObject['productRate']));
                                            secondaryQty = parseInt(secondaryQty) + parseInt(foundSSS[0].secondaryQty ? foundSSS[0].secondaryQty : 0);
                                            secondaryAmt = parseFloat(secondaryAmt) + parseFloat(foundSSS[0].totalSecondrySaleAmt ? (foundSSS[0].totalSecondrySaleAmt).toFixed(2) : 0.0);
                                            closingQty = parseInt(closingQty) + parseInt(foundSSS[0].closingQty);
                                            closingAmt = parseFloat(closingAmt) + parseFloat((foundSSS[0].closingAmt).toFixed(2));
                                            primaryFreeQty = parseInt(primaryFreeQty) + foundSSS[0].primaryFreeQty ? parseInt(foundSSS[0].primaryFreeQty) : 0;
                                            primaryFreeAmt = parseFloat(primaryFreeAmt) + parseFloat(foundSSS[0].primaryFreeQty ? (foundSSS[0].primaryFreeQty * finalObject['productRate']) : 0.0);
                                            returnFreeSale = parseInt(returnFreeSale) + parseInt(foundSSS[0].returnFreeSale ? foundSSS[0].returnFreeSale : 0);
                                            returnFreeAmt = parseFloat(returnFreeAmt) + parseFloat(foundSSS[0].returnFreeSale ? (foundSSS[0].returnFreeSale * finalObject['productRate']) : 0.0);
                                        }

                                    } else {
                                        openingQty = openingQty + 0;
                                        openingAmt = openingAmt + 0.0;
                                        secondaryQty = secondaryQty + 0;
                                        secondaryAmt = secondaryAmt + 0.0;
                                        closingQty = closingQty + 0;
                                        closingAmt = closingAmt + 0.0;
                                        primaryFreeQty = primaryFreeQty + 0;
                                        primaryFreeAmt = primaryFreeAmt + 0.0;
                                        returnFreeSale = returnFreeSale + 0;
                                        returnFreeAmt = returnFreeAmt + 0.0;
                                    }

                                    //});
                                    if (params.viewType == "unit") {
                                        finalArrayOfObject.push({
                                            //hqName: finalObject['districtName'],
                                            //hqCode: finalObject['districtCode'],
                                            userName: "----",
                                            productName: finalObject['productName'],
                                            //productcode: finalObject['productCode'],
                                            //productrate: finalObject['productRate'],
                                            targetAmount: targetQty,
                                            //targetAmt: parseFloat((targetAmt).toFixed(2)),
                                            primaryAmount: priQty,
                                            //primaryAmt: parseFloat((priAmt).toFixed(2)),
                                            achievementAmount: parseFloat(((priQty / targetQty) * 100).toFixed(2)),
                                            //achievementAmt: parseFloat(((priAmt / targetAmt) * 100).toFixed(2)),
                                            secondaryAmount: secondaryQty,
                                            //secondaryAmt: parseFloat((secondaryAmt).toFixed(2)),
                                            closingAmount: closingQty,
                                            //closingAmt: parseFloat((closingAmt).toFixed(2)),
                                            //returnQty: saleReturnQty,
                                            //returnAmt: parseFloat((saleReturnAmt).toFixed(2)),
                                            //openingQty: openingQty,
                                            //openingAmt: parseFloat((openingAmt).toFixed(2)),
                                            //primaryFreeQty: primaryFreeQty,
                                            //primaryFreeAmt: parseFloat((primaryFreeAmt).toFixed(2)),
                                            //returnFreeSale: returnFreeSale,
                                            //returnFreeAmt: parseFloat((returnFreeAmt).toFixed(2))
                                        });
                                        totalTargetQty = totalTargetQty + targetQty;
                                        totalPrimaryQty = totalPrimaryQty + priQty;
                                        //totalPerAchAmtQty = totalPerAchAmtQty + 0.0;
                                        totalSecondaryQty = totalSecondaryQty + secondaryQty;
                                        totalClosingQty = totalClosingQty + closingQty;

                                    } else {
                                        finalArrayOfObject.push({
                                            //hqName: finalObject['districtName'],
                                            //hqCode: finalObject['districtCode'],
                                            userName: "----",
                                            productName: finalObject['productName'],
                                            //productcode: finalObject['productCode'],
                                            //productrate: finalObject['productRate'],
                                            //targetQty: targetQty,
                                            targetAmount: parseFloat((targetAmt).toFixed(2)),
                                            //primaryQty: priQty,
                                            primaryAmount: parseFloat((priAmt).toFixed(2)),
                                            //achievementQty: parseFloat(((priQty / targetQty) * 100).toFixed(2)),
                                            achievementAmount: parseFloat(((priAmt / targetAmt) * 100).toFixed(2)),
                                            //secondaryQty: secondaryQty,
                                            secondaryAmount: parseFloat((secondaryAmt).toFixed(2)),
                                            //closingQty: closingQty,
                                            closingAmount: parseFloat((closingAmt).toFixed(2)),
                                            //returnQty: saleReturnQty,
                                            //returnAmt: parseFloat((saleReturnAmt).toFixed(2)),
                                            //openingQty: openingQty,
                                            //openingAmt: parseFloat((openingAmt).toFixed(2)),
                                            //primaryFreeQty: primaryFreeQty,
                                            //primaryFreeAmt: parseFloat((primaryFreeAmt).toFixed(2)),
                                            //returnFreeSale: returnFreeSale,
                                            //returnFreeAmt: parseFloat((returnFreeAmt).toFixed(2))
                                        });
                                        totalTargetAmt = totalTargetAmt + targetAmt;
                                        totalPrimaryAmt = totalPrimaryAmt + priAmt;
                                        //totalPerAchAmt = totalPerAchAmt + 0.0;
                                        totalSecondaryAmt = totalSecondaryAmt + secondaryAmt;
                                        totalClosingAmt = totalClosingAmt + closingAmt;
                                    }
                                });

                            }
                            let total = {};
                            let data;
                            if (params.viewType == "unit") {
                                data = [{
                                    details: finalArrayOfObject, "totalAmtTarget": totalTargetQty,
                                    "totalAmtPrimary": totalPrimaryQty,
                                    "totalPerAchAmt": parseFloat(((totalPrimaryQty / totalTargetQty) * 100).toFixed(2)),
                                    "totalSecondaryAmt": totalSecondaryQty,
                                    "totalClosingAmt": totalClosingQty,
                                }]
                            } else {
                                data = [{
                                    details: finalArrayOfObject, "totalAmtTarget": totalTargetAmt,
                                    "totalAmtPrimary": totalPrimaryAmt,
                                    "totalPerAchAmt": parseFloat(((totalPrimaryAmt / totalTargetAmt) * 100).toFixed(2)),
                                    "totalSecondaryAmt": totalSecondaryAmt,
                                    "totalClosingAmt": totalClosingAmt
                                }]

                            }
                            return cb(null, data)
                        })
                    });
                    //END
                })
            })
        } else {
            return cb(false, [])
        }
    }
    Primaryandsalereturn.remoteMethod(
        'getTargetVsAchievementVsPrimaryVsClosingHqWise', {
        description: 'get Target Vs Achievement Vs Primary Vs Closing Hq Wise',
        accepts: [{
            arg: 'params',
            type: 'object',
            http: { source: "body" }
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );
    //----------------End By Ravi-------------------------------------------------------

    //Done by Ravi 
    // Primaryandsalereturn.getPrimaryAndSceondaryHQWiseDump = function (params, cb) {
    //     var self = this;
    //     var PrimaryandsalereturnCollection = this.getDataSource().connector.collection(Primaryandsalereturn.modelName);
    //     const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
    //     let filterPrimary = {};
    //     let filterTarget = {};
    //     let districtIdsMrr = [];
    //     let districtIdsMrrForTarget = [];
    //     let districIdMappedWithPoolForSSS = [];
    //     let districIdMappedWithPoolForTarget = [];
    //     let districtIdsErp = [];
    //     let districtNamesMrr = [];
    //     let groupTarget = {};
    //     let month = [];
    //     let year = [];
    //     let divisionIds = [];
    //     params.divisionId.forEach(element => {
    //         divisionIds.push(ObjectId(element))
    //     });
    //     //console.log('Oarams',params);
    //     Primaryandsalereturn.app.models.Products.find({ where: { companyId: params.companyId, status: true, assignedDivision: { inq: divisionIds } }, order: "productName ASC" }, async function (err, productDetailsFromMRRMaster) {
    //         //params.selectedHqDetails.forEach((element, index) => {
    //         for (let index = 0; index < params.selectedHqDetails.length; index++) {
    //             const element = params.selectedHqDetails[index];
    //             if (element.hasOwnProperty("isPoolDistrict") && element.isPoolDistrict[params.companyId]) {
    //                 districtIdsErp.push(parseInt(element.erpHqCode));
    //                 let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.districtId), status: true } });
    //                 if (hqMappedWithPoolDetails.length > 0) {
    //                     districIdMappedWithPoolForSSS.length = 0;
    //                     districIdMappedWithPoolForTarget.length = 0;
    //                     const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
    //                     districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
    //                     districIdMappedWithPoolForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
    //                     hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
    //                         hqMappedWithPool = hqMappedWithPool.toJSON();
    //                         districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
    //                         districIdMappedWithPoolForSSS.push(ObjectId(hqMappedWithPool.mappedDistrictId));
    //                     });
    //                     params.selectedHqDetails[index]['districtIdsMappedWithPoolForSSS'] = [...districIdMappedWithPoolForSSS];
    //                     params.selectedHqDetails[index]['districtIdsMappedWithPoolForTarget'] = [...districIdMappedWithPoolForTarget];
    //                 }
    //             } else {
    //                 districtIdsMrr.push(ObjectId(element.districtId));
    //                 districtIdsMrrForTarget.push(ObjectId(element.districtId));
    //                 districtIdsErp.push(parseInt(element.erpHqCode));
    //                 districtNamesMrr.push(element.districtName);
    //             }
    //         }
    //         const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
    //         for (var i = 0; i < monthYearArray.length; i++) {
    //             month.push(monthYearArray[i].month);
    //             year.push(monthYearArray[i].year);
    //         }
    //         filterTarget = {
    //             companyId: ObjectId(params.companyId),
    //             districtId: { $in: districtIdsMrrForTarget },
    //             targetMonth: { $in: month },
    //             targetYear: { $in: year }
    //         }
    //         let filterSSS = {
    //             companyId: ObjectId(params.companyId),
    //             districtId: { $in: districtIdsMrr },
    //             month: { $in: month },
    //             year: { $in: year }
    //         }
    //         filterPrimary.hqCode = districtIdsErp.toString();
    //         filterPrimary.flag = "PS";
    //         filterPrimary.fromDate = params.fromPrimDate;
    //         filterPrimary.toDate = params.toPrimDate;
    //         filterPrimary.APIEndPoint = params.APIEndPoint;
    //         let projectTarget = {
    //             _id: 0,
    //             month: "$_id.month",
    //             year: "$_id.year",
    //             //ProductName: { $arrayElemAt: ["$productName", 0] },
    //             productId: "$_id.productId",
    //             districtId: "$_id.districtId",
    //             targetQty: "$totalTarget",
    //             //creditValue: "$prodData.creditValue",
    //             //productCode: "$prodData.productCode"

    //         };
    //         groupTarget = {
    //             _id: {
    //                 productId: '$productId',
    //                 "month": "$targetMonth",
    //                 "year": "$targetYear",
    //                 districtId: "$districtId"
    //             },
    //             totalTarget: {
    //                 $sum: "$targetQuantity"
    //             }
    //         }
    //         async.parallel({
    //             primary: function (cb) {
    //                 Primaryandsalereturn.app.models.State.getNewHQWiseProductDetails(filterPrimary,
    //                     function (err, result) {
    //                         if (err) {
    //                             cb(null, err)
    //                         }
    //                         cb(null, result)
    //                     })
    //             },
    //             target: function (cb) {
    //                 TargetCollection.aggregate(
    //                     // Stage 1
    //                     {
    //                         $match: filterTarget
    //                     },

    //                     // Stage 2
    //                     {
    //                         $group: groupTarget
    //                     },

    //                     // Stage 6
    //                     {
    //                         $project: projectTarget
    //                     },
    //                     function (err, result) {
    //                         if (err) {
    //                             console.log(err);
    //                         }
    //                         cb(null, result)
    //                     });

    //             },
    //             SSS: function (cb) {
    //                 PrimaryandsalereturnCollection.aggregate(
    //                     // Stage 1
    //                     {
    //                         $match: filterSSS
    //                     },

    //                     // Stage 2
    //                     {
    //                         $group: {
    //                             _id: {
    //                                 "productId": '$productId',
    //                                 //"netRate": "$netRate",
    //                                 "month": "$month",
    //                                 "year": "$year",
    //                                 "districtId": "$districtId"
    //                             },
    //                             //productName: { $addToSet: "$productName" },
    //                             openingQty: { $sum: "$opening" },
    //                             totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
    //                             primaryQty: { $sum: "$totalPrimaryQty" },
    //                             totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
    //                             totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
    //                             closingAmt: { $sum: "$closingAmt" },
    //                             closingQty: { $sum: "$closing" },
    //                             primaryFreeQty: { $sum: "$primaryFreeQty" },
    //                             returnFreeSale: { $sum: "$returnFreeSale" },
    //                             secondaryQty: { $sum: "$totalSecondrySaleQty" },
    //                             expiryQty: { $sum: "$expiryQty" },
    //                             breakageQty: { $sum: "$breakageQty" },
    //                             batchRecallQty: { $sum: "$batchRecallQty" },
    //                         }
    //                     },

    //                     // Stage 4
    //                     {
    //                         $project: {
    //                             //ProductName: { $arrayElemAt: ["$prod.productName", 0] },
    //                             productId: "$_id.productId",
    //                             districtId: "$_id.districtId",
    //                             //netRate: { $arrayElemAt: ["$prod.netRate", 0] },
    //                             //ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
    //                             openingQty: 1,
    //                             totalPrimaryAmt: 1,
    //                             primaryQty: 1,
    //                             totalSaleReturnAmt: 1,
    //                             totalSecondrySaleAmt: 1,
    //                             closingAmt: 1,
    //                             primaryFreeQty: 1,
    //                             returnFreeSale: 1,
    //                             closingQty: 1,
    //                             secondaryQty: 1,
    //                             expiryQty: 1,
    //                             breakageQty: 1,
    //                             batchRecallQty: 1
    //                         }
    //                     }, function (err, res) {
    //                         if (err) {
    //                             cb(null, [])
    //                         }
    //                         cb(null, res)
    //                     }

    //                 );

    //             }
    //         }, async function (err, asyncResult) {
    //             if (err) { return cb(err) }
    //             let districtWiseData = [];
    //             //let productDetailsFromMRRMaster = await Primaryandsalereturn.app.models.Products.find({ where: { companyId: params.companyId, status: true, productCode: { nin: [0] } }, order: "productName ASC" });
    //             if (productDetailsFromMRRMaster.length > 0) {
    //                 params.selectedHqDetails.forEach(hqMasterFromSelection => {
    //                     finalArrayOfObject = [];
    //                     let totalTargetQty = 0, totalTargetAmt = 0, totalOpeningQty = 0, totalOpeningAmt = 0, totalPrimaryQty = 0,
    //                         totalPrimaryAmt = 0, totalPrimaryFreeQty = 0, totalPrimaryFreeAmt = 0, totalSecondaryQty = 0, grandtotalSotalSecondrySaleAmt = 0,
    //                         totalSalereturnqty = 0, totalSalereturnamount = 0, totalReturnFreeSale = 0, totalReturnFreeAmt = 0, totalExpiryqty = 0, totalExpiryamount = 0,
    //                         totalBreakageqty = 0, totalBreakageamount = 0, totalClosingQty = 0, totalClosingAmt = 0;
    //                     let districtName, productName, productCode, productType, productRate;
    //                     productDetailsFromMRRMaster.forEach(productMasterFromMrr => {
    //                         let primaryQty = 0, primaryAmt = 0.0, salereturnqty = 0, salereturnamount = 0.0, expiryqty = 0, expiryamount = 0.0, breakageqty = 0, breakageamount = 0.0,
    //                             targetQty = 0, targetAmt = 0.0, PerAchAmt = 0, perAchQty = 0.0, secondaryQty = 0, totalSecondrySaleAmt = 0.0, closingQty = 0; closingAmt = 0.0, openingQty = 0, openingAmt = 0.0, primaryFreeQty = 0,
    //                                 primaryFreeAmt = 0.0, returnFreeSale = 0, returnFreeAmt = 0.0;
    //                         districtName = hqMasterFromSelection.districtName;
    //                         productName = productMasterFromMrr.productName;
    //                         productCode = productMasterFromMrr.productCode;
    //                         productType = productMasterFromMrr.productType;
    //                         productRate = productMasterFromMrr.creditValue;
    //                         if (asyncResult.primary != undefined && asyncResult.primary.length > 0) {
    //                             let foundPrimary = asyncResult.primary.filter(primary => {
    //                                 return parseInt(primary.productcode) == parseInt(productMasterFromMrr.productCode) && parseInt(primary.HQCode) == parseInt(hqMasterFromSelection.erpHqCode)
    //                             })

    //                             if (foundPrimary.length > 0) {
    //                                 primaryQty = primaryQty + foundPrimary[0].primarysaleqty;
    //                                 primaryAmt = primaryAmt + (foundPrimary[0].primarysaleqty) * (productRate)//foundPrimary[0].primarysaleamount;
    //                                 salereturnqty = salereturnqty + foundPrimary[0].salereturnqty;
    //                                 salereturnamount = salereturnamount + (foundPrimary[0].salereturnqty) * (productRate)//foundPrimary[0].salereturnamount;
    //                                 expiryqty = expiryqty + foundPrimary[0].expiryqty;
    //                                 expiryamount = expiryamount + (foundPrimary[0].expiryqty) * (productRate)//foundPrimary[0].expiryamount;
    //                                 breakageqty = breakageqty + foundPrimary[0].breakageqty;
    //                                 breakageamount = breakageamount + (foundPrimary[0].breakageqty) * (productRate)//foundPrimary[0].breakageamount;
    //                             }
    //                         } else {
    //                             primaryQty += 0;
    //                             primaryAmt += 0.0;
    //                             salereturnqty += 0;
    //                             salereturnamount += 0.0
    //                             expiryqty += 0;
    //                             expiryamount += 0.0;
    //                             breakageqty += 0;
    //                             breakageamount += 0.0;
    //                         }

    //                         if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
    //                             if (asyncResult.target != undefined && asyncResult.target.length > 0) {
    //                                 let poolDistrictId = hqMasterFromSelection.districtIdsMappedWithPoolForTarget[0];
    //                                 if (poolDistrictId) {
    //                                     let foundTargetPoolHq = asyncResult.target.filter(target => {
    //                                         return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (poolDistrictId).toString()
    //                                     })
    //                                     if (foundTargetPoolHq.length > 0) {
    //                                         targetAmt = (foundTargetPoolHq[0].targetQty * (productRate ? productRate : 1)).toFixed(2);;
    //                                         targetQty = foundTargetPoolHq[0].targetQty;
    //                                         PerAchAmt = (((primaryAmt == 0 ? 0 : primaryAmt) / (targetAmt == 0 ? 1 : targetAmt)) * 100).toFixed(2);
    //                                         perAchQty = (((primaryQty == 0 ? 0 : primaryQty) / (targetQty == 0 ? 1 : targetQty)) * 100).toFixed(2);
    //                                     }
    //                                 }
    //                                 // });

    //                             } else {
    //                                 targetAmt = 0.0;
    //                                 targetQty = 0;
    //                                 PerAchAmt = 0.0;
    //                                 perAchQty = 0;
    //                             }
    //                         } else {
    //                             if (asyncResult.target != undefined && asyncResult.target.length > 0) {
    //                                 let foundTarget = asyncResult.target.filter(target => {
    //                                     return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (hqMasterFromSelection.districtId).toString()
    //                                 })
    //                                 if (foundTarget.length > 0) {
    //                                     targetAmt = (foundTarget[0].targetQty * (productRate ? productRate : 1)).toFixed(2);;
    //                                     targetQty = foundTarget[0].targetQty;
    //                                     PerAchAmt = (((primaryAmt == 0 ? 0 : primaryAmt) / (targetAmt == 0 ? 1 : targetAmt)) * 100).toFixed(2);
    //                                     perAchQty = (((primaryQty == 0 ? 0 : primaryQty) / (targetQty == 0 ? 1 : targetQty)) * 100).toFixed(2);
    //                                 }
    //                             } else {
    //                                 targetAmt = 0.0;
    //                                 targetQty = 0;
    //                                 PerAchAmt = 0.0;
    //                                 perAchQty = 0;
    //                             }
    //                         }

    //                         if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
    //                             let openingQtyVar = 0, openingAmtVar = 0, seconQtyVar = 0, seconAmtVar = 0, cloQtyVar = 0;
    //                             closAmtVar = 0; priFreeQtyVar = 0; priAmtFreeVar = 0; returnFreeQtyVar = 0; returnFreeAmtVar = 0;
    //                             if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
    //                                 hqMasterFromSelection.districtIdsMappedWithPoolForSSS.forEach(poolDistrictId => {
    //                                     let foundSSSPoolHq = asyncResult.SSS.filter(sss => {
    //                                         return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss.districtId).toString() == (poolDistrictId).toString()
    //                                     })
    //                                     if (foundSSSPoolHq.length > 0) {
    //                                         openingQtyVar = parseInt(openingQtyVar) + parseInt(foundSSSPoolHq[0].openingQty);
    //                                         openingAmtVar = parseFloat(openingAmtVar + (parseInt(foundSSSPoolHq[0].openingQty) * productRate));
    //                                         seconQtyVar = foundSSSPoolHq[0].secondaryQty ? parseFloat(seconQtyVar) + parseFloat((foundSSSPoolHq[0].secondaryQty).toFixed(2)) : parseFloat(seconQtyVar) + 0;
    //                                         seconAmtVar = foundSSSPoolHq[0].totalSecondrySaleAmt ? parseFloat(seconAmtVar) + parseFloat((foundSSSPoolHq[0].totalSecondrySaleAmt).toFixed(2)) : parseFloat(seconAmtVar) + 0.0;
    //                                         cloQtyVar = parseFloat(cloQtyVar) + parseFloat((foundSSSPoolHq[0].closingQty).toFixed(2));
    //                                         closAmtVar = parseFloat(closAmtVar) + parseFloat((foundSSSPoolHq[0].closingAmt).toFixed(2));
    //                                         priFreeQtyVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priFreeQtyVar) + parseFloat(foundSSSPoolHq[0].primaryFreeQty) : parseFloat(priFreeQtyVar) + 0;
    //                                         priAmtFreeVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priAmtFreeVar) + parseFloat((foundSSSPoolHq[0].primaryFreeQty * productRate)) : parseFloat(priAmtFreeVar) + 0.0;
    //                                         returnFreeQtyVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeQtyVar) + parseFloat(foundSSSPoolHq[0].returnFreeSale) : parseFloat(returnFreeQtyVar) + 0;
    //                                         returnFreeAmtVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeAmtVar) + parseFloat((foundSSSPoolHq[0].returnFreeSale * productRate)) : parseFloat(returnFreeAmtVar) + 0.0;
    //                                     }

    //                                 });
    //                                 openingQty = openingQtyVar;
    //                                 openingAmt = openingAmtVar;
    //                                 secondaryQty = seconQtyVar;
    //                                 totalSecondrySaleAmt = seconAmtVar;
    //                                 closingQty = cloQtyVar;
    //                                 closingAmt = closAmtVar;
    //                                 primaryFreeQty = priFreeQtyVar;
    //                                 primaryFreeAmt = priAmtFreeVar;
    //                                 returnFreeSale = returnFreeQtyVar;
    //                                 returnFreeAmt = returnFreeAmtVar;
    //                             } else {
    //                                 openingQty = 0;
    //                                 openingAmt = 0.0;
    //                                 secondaryQty = 0;
    //                                 totalSecondrySaleAmt = 0.0;
    //                                 closingQty = 0;
    //                                 closingAmt = 0.0;
    //                                 primaryFreeQty = 0;
    //                                 primaryFreeAmt = 0.0;
    //                                 returnFreeSale = 0;
    //                                 returnFreeAmt = 0.0;
    //                             }
    //                         } else {
    //                             if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
    //                                 let foundSSS = asyncResult.SSS.filter(sss => {

    //                                     return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss.districtId).toString() == (hqMasterFromSelection.districtId).toString()
    //                                 })
    //                                 if (foundSSS.length > 0) {
    //                                     openingQty = foundSSS[0].openingQty;
    //                                     openingAmt = (foundSSS[0].openingQty * productRate);
    //                                     secondaryQty = foundSSS[0].secondaryQty ? (foundSSS[0].secondaryQty).toFixed(2) : 0;
    //                                     totalSecondrySaleAmt = foundSSS[0].totalSecondrySaleAmt ? (foundSSS[0].totalSecondrySaleAmt).toFixed(2) : 0.0;
    //                                     closingQty = (foundSSS[0].closingQty).toFixed(2);
    //                                     closingAmt = (foundSSS[0].closingAmt).toFixed(2);
    //                                     primaryFreeQty = foundSSS[0].primaryFreeQty ? foundSSS[0].primaryFreeQty : 0;
    //                                     primaryFreeAmt = foundSSS[0].primaryFreeQty ? (foundSSS[0].primaryFreeQty * productRate) : 0.0;
    //                                     returnFreeSale = foundSSS[0].returnFreeSale ? foundSSS[0].returnFreeSale : 0;
    //                                     returnFreeAmt = foundSSS[0].returnFreeSale ? (foundSSS[0].returnFreeSale * productRate) : 0.0;
    //                                 }

    //                             } else {
    //                                 openingQty = 0;
    //                                 openingAmt = 0.0;
    //                                 secondaryQty = 0;
    //                                 totalSecondrySaleAmt = 0.0;
    //                                 closingQty = 0;
    //                                 closingAmt = 0.0;
    //                                 primaryFreeQty = 0;
    //                                 primaryFreeAmt = 0.0;
    //                                 returnFreeSale = 0;
    //                                 returnFreeAmt = 0.0;
    //                             }
    //                         }
    //                         let finalObject = {
    //                             "districtName": districtName,
    //                             "productName": productName,
    //                             "productCode": productCode,
    //                             "productType": productType,
    //                             "productRate": productRate,
    //                             "primaryObj": {
    //                                 "primaryQty": primaryQty,
    //                                 "primaryAmt": primaryAmt
    //                             },
    //                             "returnObj": {
    //                                 "salereturnqty": salereturnqty,
    //                                 "salereturnamount": salereturnamount,
    //                                 "expiryqty": expiryqty,
    //                                 "expiryamount": expiryamount,
    //                                 "breakageqty": breakageqty,
    //                                 "breakageamount": breakageamount
    //                             },
    //                             "targetObj": {
    //                                 "targetAmt": targetAmt,
    //                                 "targetQty": targetQty
    //                             },
    //                             "achObj": {
    //                                 "PerAchAmt": 0,
    //                                 "perAchQty": 0
    //                             },
    //                             "sssObj": {
    //                                 "openingQty": openingQty,
    //                                 "openingAmt": openingAmt,
    //                                 "secondaryQty": secondaryQty,
    //                                 "totalSecondrySaleAmt": totalSecondrySaleAmt,
    //                                 "closingQty": closingQty,
    //                                 "closingAmt": closingAmt,
    //                                 "primaryFreeQty": primaryFreeQty,
    //                                 "primaryFreeAmt": primaryFreeAmt,
    //                                 "returnFreeSale": returnFreeSale,
    //                                 "returnFreeAmt": returnFreeAmt
    //                             }
    //                         };
    //                         finalArrayOfObject.push({ ...finalObject });
    //                         // Total Grand Work
    //                         totalTargetQty += targetQty;
    //                         totalTargetAmt += parseFloat(targetAmt);
    //                         totalOpeningQty += parseInt(openingQty);
    //                         totalOpeningAmt += (openingQty) * (productRate);
    //                         totalPrimaryQty += parseInt(primaryQty);
    //                         totalPrimaryAmt += parseFloat(primaryAmt);
    //                         totalPrimaryFreeQty += parseInt(primaryFreeQty);
    //                         totalPrimaryFreeAmt += parseFloat(primaryFreeAmt);
    //                         totalSecondaryQty += parseInt(secondaryQty);
    //                         grandtotalSotalSecondrySaleAmt += parseFloat(totalSecondrySaleAmt);
    //                         totalSalereturnqty += parseInt(salereturnqty);
    //                         totalSalereturnamount += parseFloat(salereturnamount);
    //                         totalReturnFreeSale += parseInt(returnFreeSale);
    //                         totalReturnFreeAmt += parseFloat(returnFreeAmt);
    //                         totalExpiryqty += parseInt(expiryqty);
    //                         totalExpiryamount += parseFloat(expiryamount);
    //                         totalBreakageqty += parseInt(breakageqty);
    //                         totalBreakageamount += parseFloat(breakageamount);
    //                         totalClosingQty += parseInt(closingQty);
    //                         totalClosingAmt += parseFloat(closingAmt);
    //                         //END
    //                     });
    //                     //Pushing Grand total Row
    //                     finalArrayOfObject.push({
    //                         "districtName": "TOTAL",
    //                         "productName": "----",
    //                         "productCode": 0000,
    //                         "productType": "----",
    //                         "productRate": 0.0,
    //                         "primaryObj": {
    //                             "primaryQty": totalPrimaryQty,
    //                             "primaryAmt": totalPrimaryAmt
    //                         },
    //                         "returnObj": {
    //                             "salereturnqty": totalSalereturnqty,
    //                             "salereturnamount": totalSalereturnamount,
    //                             "expiryqty": totalExpiryqty,
    //                             "expiryamount": totalExpiryamount,
    //                             "breakageqty": totalBreakageqty,
    //                             "breakageamount": totalBreakageamount
    //                         },
    //                         "targetObj": {
    //                             "targetAmt": totalTargetAmt,
    //                             "targetQty": totalTargetQty
    //                         },
    //                         "achObj": {
    //                             "PerAchAmt": 0,
    //                             "perAchQty": 0
    //                         },
    //                         "sssObj": {
    //                             "openingQty": totalOpeningQty,
    //                             "openingAmt": totalOpeningAmt,
    //                             "secondaryQty": totalSecondaryQty,
    //                             "totalSecondrySaleAmt": grandtotalSotalSecondrySaleAmt,
    //                             "closingQty": totalClosingQty,
    //                             "closingAmt": totalClosingAmt,
    //                             "primaryFreeQty": totalPrimaryFreeQty,
    //                             "primaryFreeAmt": totalPrimaryFreeAmt,
    //                             "returnFreeSale": totalReturnFreeSale,
    //                             "returnFreeAmt": totalReturnFreeAmt
    //                         }
    //                     })
    //                     districtWiseData.push({ district: hqMasterFromSelection.districtName, data: finalArrayOfObject });
    //                 });
    //             } else {
    //                 districtWiseData = [{ district: 'Data not found !', data: [] }];
    //             }
    //             return cb(null, districtWiseData)
    //         })
    //     });
    // };

    // Primaryandsalereturn.remoteMethod("getPrimaryAndSceondaryHQWiseDump", {
    //     description: "Target Vs Achievement",
    //     accepts: [
    //         {
    //             arg: "params",
    //             type: "object",
    //             http: { source: "body" }
    //         },
    //     ],
    //     returns: {
    //         root: true,
    //         type: "array",
    //     },
    //     http: {
    //         verb: "post",
    //     },
    // });


    //Done by Ravi 
    Primaryandsalereturn.getPrimaryAndSceondaryHQWiseDump = function (params, cb) {
        var self = this;
        var PrimaryandsalereturnCollection = this.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        //let filterPrimary = {};
        let filterTarget = {};
        let districtIdsMrr = [];
        let districtIdsMrrForTarget = [];
        let districIdMappedWithPoolForSSS = [];
        let districIdMappedWithPoolForTarget = [];
        let districtIdsErp = [];
        let districtNamesMrr = [];
        let groupTarget = {};
        let month = [];
        let year = [];
        let divisionIds = [];
        params.divisionId.forEach(element => {
            divisionIds.push(ObjectId(element))
        });
        let filter = {};
        if (params.product && params.product.length > 0) {
            filter = { where: { companyId: params.companyId, status: true, assignedDivision: { inq: divisionIds }, productCode: { inq: params.product } }, order: "productName ASC" };
        } else {
            filter = { where: { companyId: params.companyId, status: true, assignedDivision: { inq: divisionIds } }, order: "productName ASC" };
        }
        Primaryandsalereturn.app.models.Products.find(filter, async function (err, productDetailsFromMRRMaster) {
            for (let index = 0; index < params.selectedHqDetails.length; index++) {
                const element = params.selectedHqDetails[index];
                if (element.hasOwnProperty("isPoolDistrict") && element.isPoolDistrict[params.companyId]) {
                    districtIdsErp.push(parseInt(element.erpHqCode));
                    let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.districtId), status: true } });
                    if (hqMappedWithPoolDetails.length > 0) {
                        districIdMappedWithPoolForSSS.length = 0;
                        districIdMappedWithPoolForTarget.length = 0;
                        const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                        districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                        districIdMappedWithPoolForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                        hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                            hqMappedWithPool = hqMappedWithPool.toJSON();
                            districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                            districIdMappedWithPoolForSSS.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                        });
                        params.selectedHqDetails[index]['districtIdsMappedWithPoolForSSS'] = [...districIdMappedWithPoolForSSS];
                        params.selectedHqDetails[index]['districtIdsMappedWithPoolForTarget'] = [...districIdMappedWithPoolForTarget];
                    }
                } else {
                    districtIdsMrr.push(ObjectId(element.districtId));
                    districtIdsMrrForTarget.push(ObjectId(element.districtId));
                    districtIdsErp.push(parseInt(element.erpHqCode));
                    districtNamesMrr.push(element.districtName);
                }
            }
            const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
            for (var i = 0; i < monthYearArray.length; i++) {
                month.push(monthYearArray[i].month);
                year.push(monthYearArray[i].year);
            }
            filterTarget = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: districtIdsMrrForTarget },
                targetMonth: { $in: month },
                targetYear: { $in: year },
                fromDate: { $gte: new Date(params.fromDate) },
                toDate: { $lte: new Date(params.toDate) }
            }
            let filterSSS = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: districtIdsMrr },
                month: { $in: month },
                year: { $in: year },
                fromDate: { $gte: new Date(params.fromDate) },
                toDate: { $lte: new Date(params.toDate) }
            }
            let projectTarget = {
                _id: 0,
                month: "$_id.month",
                year: "$_id.year",
                productId: "$_id.productId",
                districtId: "$_id.districtId",
                targetQty: "$totalTarget",
                creditValue: "$prodData.creditValue",
                productCode: "$prodData.productCode"
            };
            groupTarget = {
                _id: {
                    productId: '$productId',
                    "month": "$targetMonth",
                    "year": "$targetYear",
                    districtId: "$districtId"
                },
                totalTarget: {
                    $sum: "$targetQuantity"
                }
            }

            async.parallel({
                target: function (cb) {
                    TargetCollection.aggregate(
                        // Stage 1
                        {
                            $match: filterTarget
                        },

                        // Stage 2
                        {
                            $group: groupTarget
                        },


                        // Stage 6
                        {
                            $project: projectTarget
                        },
                        function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            cb(null, result)
                        });

                },
                SSS: function (cb) {
                    PrimaryandsalereturnCollection.aggregate(
                        // Stage 1
                        {
                            $match: filterSSS
                        },

                        // Stage 2
                        {
                            $group: {
                                _id: {
                                    "productId": '$productId',
                                    //"netRate": "$netRate",
                                    "month": "$month",
                                    "year": "$year",
                                    "districtId": "$districtId"
                                },
                                productName: { $addToSet: "$productName" },
                                openingQty: { $sum: "$opening" },
                                totalPrimaryQty: { $sum: "$totalPrimaryQty" },
                                totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
                                secondaryQty: { $sum: "$totalSecondrySaleQty" },
                                totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
                                totalSaleReturnQty: { $sum: "$totalSaleReturnAmt" },
                                totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
                                closingQty: { $sum: "$closing" },
                                closingAmt: { $sum: "$closingAmt" },
                                primaryFreeQty: { $sum: "$primaryFreeQty" },
                                returnFreeSale: { $sum: "$returnFreeSale" },
                                expiryQty: { $sum: "$expiryQty" },
                                breakageQty: { $sum: "$breakageQty" },
                                batchRecallQty: { $sum: "$batchRecallQty" },
                            }
                        },

                        // Stage 3
                        {
                            $project: {
                                //ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                productId: "$_id.productId",
                                districtId: "$_id.districtId",
                                month: "$_id.month",
                                //netRate: { $arrayElemAt: ["$prod.netRate", 0] },
                                //ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                openingQty: 1,
                                totalPrimaryQty: 1,
                                totalPrimaryAmt: 1,
                                secondaryQty: 1,
                                totalSecondrySaleAmt: 1,
                                totalSaleReturnQty: 1,
                                totalSaleReturnAmt: 1,
                                closingQty: 1,
                                closingAmt: 1,
                                primaryFreeQty: 1,
                                returnFreeSale: 1,
                                expiryQty: 1,
                                breakageQty: 1,
                                batchRecallQty: 1
                            }
                        }, function (err, res) {
                            if (err) {
                                cb(null, [])
                            }
                            cb(null, res)
                        }

                    );

                }
            }, async function (err, asyncResult) {
                if (err) { return cb(err) }
                let districtWiseData = [];
                if (productDetailsFromMRRMaster.length > 0) {
                    params.selectedHqDetails.forEach(hqMasterFromSelection => {
                        finalArrayOfObject = [];
                        productDetailsFromMRRMaster.forEach(productMasterFromMrr => {
                            let horizontalTotalPrimaryQty = 0, horizontalTotalPrimaryAmt = 0.0, horizontalTotalReturnQty = 0, horizontalTotalReturnAmt = 0.0, horizontalTotalExpiryQty = 0, horizontalTotalExpiryAmt = 0.0, horizontalTotalBreakageQty = 0, horizontalTotalBreakageAmt = 0.0,
                                horizontalTotalOpeningQty = 0, horizontalTotalOpeningAmt = 0.0, horizontalTotalSecondaryQty = 0, horizontalTotalSecondaryAmt = 0.0, horizontalTotalClosingQty = 0, horizontalTotalClosingAmt = 0.0, horizontalTotalPrimaryFreeQty = 0, horizontalTotalPrimaryFreeAmt = 0.0,
                                horizontalTotalReturnFreeQty = 0, horizontalTotalReturnFreeAmt = 0.0, horizontalTotalTargetQty = 0, horizontalTotalTargetAmt = 0.0, horizontalTotalPerAchQty = 0, horizontalTotalPerAchAmt = 0.0;
                            productMonthWiseArrayOfObject = [];
                            let districtName = hqMasterFromSelection.districtName;
                            let productName = productMasterFromMrr.productName;
                            let productRate = productMasterFromMrr.creditValue;
                            productMonthWiseArrayOfObject.push(districtName, productName, productRate);
                            let primaryQty = 0, primaryAmt = 0.0, returnQty = 0, returnAmt = 0.0, expiryQty = 0, expiryAmt = 0.0, breakageQty = 0, breakageAmt = 0.0,
                                openingQty = 0, openingAmt = 0.0, secondaryQty = 0, secondaryAmt = 0.0, closingQty = 0, closingAmt = 0.0, primaryFreeQty = 0, primaryFreeAmt = 0.0,
                                returnFreeQty = 0, returnFreeAmt = 0.0, targetQty = 0, targetAmt = 0.0, perAchQty = 0, PerAchAmt = 0.0;
                            month.forEach(month => {

                                if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
                                    let priQtyVar = 0, priAmtVar = 0.0, returnQtyVar = 0, returnAmtVar = 0.0, expiryQtyVar = 0, expiryAmtVar = 0.0, breakageQtyVar = 0, breakageAmtVar = 0.0,
                                        openingQtyVar = 0, openingAmtVar = 0, seconQtyVar = 0, seconAmtVar = 0, cloQtyVar = 0;
                                    closAmtVar = 0; priFreeQtyVar = 0; priAmtFreeVar = 0; returnFreeQtyVar = 0; returnFreeAmtVar = 0;
                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        hqMasterFromSelection.districtIdsMappedWithPoolForSSS.forEach(poolDistrictId => {
                                            let foundSSSPoolHq = asyncResult.SSS.filter(sss => {
                                                return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss._id.districtId).toString() == (poolDistrictId).toString() && parseInt(sss.month) == parseInt(month)
                                            })
                                            if (foundSSSPoolHq.length > 0) {
                                                priQtyVar = parseInt(priQtyVar) + parseInt(foundSSSPoolHq[0].totalPrimaryQty);
                                                priAmtVar = parseInt(priAmtVar) + parseFloat((foundSSSPoolHq[0].totalPrimaryAmt).toFixed(2));
                                                returnQtyVar = parseInt(returnQtyVar) + parseInt(foundSSSPoolHq[0].totalSaleReturnQty);
                                                returnAmtVar = parseFloat(returnAmtVar) + parseFloat((foundSSSPoolHq[0].totalSaleReturnAmt).toFixed(2));
                                                expiryQtyVar = parseInt(expiryQtyVar) + parseInt(foundSSSPoolHq[0].expiryQty);
                                                expiryAmtVar = parseFloat(expiryAmtVar) + parseFloat(((foundSSSPoolHq[0].expiryQty) * (productRate)).toFixed(2));
                                                breakageQtyVar = parseInt(breakageQtyVar) + parseInt(foundSSS[0].breakageQty);
                                                breakageAmtVar = parseFloat(breakageAmtVar) + parseFloat(((foundSSS[0].breakageQty) * (productRate)).toFixed(2));
                                                openingQtyVar = parseInt(openingQtyVar) + parseInt(foundSSSPoolHq[0].openingQty);
                                                openingAmtVar = parseFloat(openingAmtVar + (parseInt(foundSSSPoolHq[0].openingQty) * productRate).toFixed(2));
                                                seconQtyVar = foundSSSPoolHq[0].secondaryQty ? parseFloat(seconQtyVar) + parseFloat((foundSSSPoolHq[0].secondaryQty).toFixed(2)) : parseFloat(seconQtyVar) + 0;
                                                seconAmtVar = foundSSSPoolHq[0].totalSecondrySaleAmt ? parseFloat(seconAmtVar) + parseFloat((foundSSSPoolHq[0].totalSecondrySaleAmt).toFixed(2)) : parseFloat(seconAmtVar) + 0.0;
                                                cloQtyVar = parseFloat(cloQtyVar) + parseFloat((foundSSSPoolHq[0].closingQty).toFixed(2));
                                                closAmtVar = parseFloat(closAmtVar) + parseFloat((foundSSSPoolHq[0].closingAmt).toFixed(2));
                                                priFreeQtyVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priFreeQtyVar) + parseFloat(foundSSSPoolHq[0].primaryFreeQty) : parseFloat(priFreeQtyVar) + 0;
                                                priAmtFreeVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priAmtFreeVar) + parseFloat((foundSSSPoolHq[0].primaryFreeQty * productRate).toFixed(2)) : parseFloat(priAmtFreeVar) + 0.0;
                                                returnFreeQtyVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeQtyVar) + parseFloat(foundSSSPoolHq[0].returnFreeSale) : parseFloat(returnFreeQtyVar) + 0;
                                                returnFreeAmtVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeAmtVar) + parseFloat((foundSSSPoolHq[0].returnFreeSale * productRate).toFixed(2)) : parseFloat(returnFreeAmtVar) + 0.0;
                                            }

                                        });
                                        primaryQty = priQtyVar;
                                        primaryAmt = priAmtVar;
                                        returnQty = returnQtyVar;
                                        returnAmt = returnAmtVar;
                                        expiryQty = expiryQtyVar;
                                        expiryAmt = expiryAmtVar;
                                        breakageQty = breakageQtyVar;
                                        breakageAmt = breakageAmtVar;
                                        openingQty = openingQtyVar;
                                        openingAmt = openingAmtVar;
                                        secondaryQty = seconQtyVar;
                                        secondaryAmt = seconAmtVar;
                                        closingQty = cloQtyVar;
                                        closingAmt = closAmtVar;
                                        primaryFreeQty = priFreeQtyVar;
                                        primaryFreeAmt = priAmtFreeVar;
                                        returnFreeQty = returnFreeQtyVar;
                                        returnFreeAmt = returnFreeAmtVar;
                                    } else {
                                        primaryQty = 0;
                                        primaryAmt = 0.0;
                                        returnQty = 0;
                                        returnAmt = 0.0;
                                        expiryQty = 0;
                                        expiryAmt = 0.0;
                                        breakageQty = 0;
                                        breakageAmt = 0.0;
                                        openingQty = 0;
                                        openingAmt = 0.0;
                                        secondaryQty = 0;
                                        secondaryAmt = 0.0;
                                        closingQty = 0;
                                        closingAmt = 0.0;
                                        primaryFreeQty = 0;
                                        primaryFreeAmt = 0.0;
                                        returnFreeQty = 0;
                                        returnFreeAmt = 0.0;
                                    }
                                } else {
                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        let foundSSS = asyncResult.SSS.filter(sss => {
                                            return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss.districtId).toString() == (hqMasterFromSelection.districtId).toString() && parseInt(sss.month) == parseInt(month)
                                        })
                                        if (foundSSS.length > 0) {
                                            primaryQty = foundSSS[0].totalPrimaryQty ? parseInt(foundSSS[0].totalPrimaryQty) : 0;
                                            primaryAmt = foundSSS[0].totalPrimaryAmt ? parseFloat((foundSSS[0].totalPrimaryAmt).toFixed(2)) : 0.0;
                                            returnQty = foundSSS[0].totalSaleReturnQty ? parseInt(foundSSS[0].totalSaleReturnQty) : 0;
                                            returnAmt = foundSSS[0].totalSaleReturnAmt ? parseFloat((foundSSS[0].totalSaleReturnAmt).toFixed(2)) : 0.0;
                                            expiryQty = foundSSS[0].expiryQty ? parseInt(foundSSS[0].expiryQty) : 0;
                                            expiryAmt = foundSSS[0].expiryQty ? parseFloat(((foundSSS[0].expiryQty) * (productRate)).toFixed(2)) : 0.0;
                                            breakageQty = foundSSS[0].breakageQty ? parseInt(foundSSS[0].breakageQty) : 0;
                                            breakageAmt = foundSSS[0].breakageQty ? parseFloat(((foundSSS[0].breakageQty) * (productRate)).toFixed(2)) : 0.0;
                                            openingQty = foundSSS[0].openingQty ? parseInt(foundSSS[0].openingQty) : 0;
                                            openingAmt = parseFloat((parseInt(foundSSS[0].openingQty) * parseFloat(productRate)).toFixed(2));
                                            secondaryQty = foundSSS[0].secondaryQty ? parseInt(foundSSS[0].secondaryQty) : 0;
                                            secondaryAmt = foundSSS[0].totalSecondrySaleAmt ? parseFloat((foundSSS[0].totalSecondrySaleAmt).toFixed(2)) : 0.0;
                                            closingQty = parseInt(foundSSS[0].closingQty);
                                            closingAmt = parseFloat((foundSSS[0].closingAmt).toFixed(2));
                                            primaryFreeQty = foundSSS[0].primaryFreeQty ? parseInt(foundSSS[0].primaryFreeQty) : 0;
                                            primaryFreeAmt = foundSSS[0].primaryFreeQty ? parseFloat((foundSSS[0].primaryFreeQty * productRate).toFixed(2)) : 0.0;
                                            returnFreeQty = foundSSS[0].returnFreeSale ? parseInt(foundSSS[0].returnFreeSale) : 0;
                                            returnFreeAmt = foundSSS[0].returnFreeSale ? parseFloat((foundSSS[0].returnFreeSale * productRate).toFixed(2)) : 0.0;
                                        }
                                    }
                                }

                                if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        hqMasterFromSelection.districtIdsMappedWithPoolForTarget.forEach(poolDistrictId => {
                                            if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                                let foundTargetPoolHq = asyncResult.target.filter(target => {
                                                    return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (poolDistrictId).toString() && parseInt(target.month) == parseInt(month)
                                                })
                                                if (foundTargetPoolHq.length > 0) {
                                                    targetAmt = foundTargetPoolHq[0].targetQty ? parseFloat((parseInt(foundTargetPoolHq[0].targetQty) * (parseFloat(productRate)).toFixed(2))) : 0.0;
                                                    targetQty = foundTargetPoolHq[0].targetQty ? parseInt(foundTargetPoolHq[0].targetQty) : 0;
                                                    PerAchAmt = parseFloat((parseFloat((primaryAmt / targetAmt)) * 100).toFixed(2));
                                                    perAchQty = parseFloat((parseFloat((primaryQty / targetQty)) * 100).toFixed(2));
                                                }
                                            }
                                        });

                                    } else {
                                        targetAmt = 0.0;
                                        targetQty = 0;
                                        PerAchAmt = 0.0;
                                        perAchQty = 0;
                                    }
                                } else {
                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        let foundTarget = asyncResult.target.filter(target => {
                                            return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (hqMasterFromSelection.districtId).toString() && parseInt(target.month) == parseInt(month)
                                        })
                                        if (foundTarget.length > 0) {
                                            targetAmt = foundTarget[0].targetQty ? parseFloat((parseInt(foundTarget[0].targetQty) * (parseFloat(productRate)).toFixed(2))) : 0.0;
                                            targetQty = foundTarget[0].targetQty ? parseInt(foundTarget[0].targetQty) : 0;
                                            PerAchAmt = parseFloat((parseFloat((primaryAmt / targetAmt)) * 100).toFixed(2));
                                            perAchQty = parseFloat((parseFloat((primaryQty / targetQty)) * 100).toFixed(2));
                                        }
                                    }
                                }
                                productMonthWiseArrayOfObject.push(targetQty, openingQty, primaryQty, primaryFreeQty, secondaryQty, perAchQty,
                                    returnQty, returnFreeQty, expiryQty, breakageQty, closingQty, targetAmt, openingAmt, primaryAmt, primaryFreeAmt,
                                    secondaryAmt, PerAchAmt, returnAmt, returnFreeAmt, expiryAmt, breakageAmt, closingAmt);

                                // Horizontal Productwise Grand Total Work
                                horizontalTotalTargetQty += parseInt(targetQty);
                                horizontalTotalOpeningQty += parseInt(openingQty);
                                horizontalTotalPrimaryQty += parseInt(primaryQty);
                                horizontalTotalPrimaryFreeQty += parseInt(primaryFreeQty);
                                horizontalTotalSecondaryQty += parseInt(secondaryQty);
                                horizontalTotalPerAchQty += parseInt(perAchQty);
                                horizontalTotalReturnQty += parseInt(returnQty);
                                horizontalTotalReturnFreeQty += parseInt(returnFreeQty);
                                horizontalTotalExpiryQty += parseInt(expiryQty);
                                horizontalTotalBreakageQty += parseInt(breakageQty);
                                horizontalTotalClosingQty += parseInt(closingQty);
                                horizontalTotalTargetAmt += parseFloat(targetAmt);
                                horizontalTotalOpeningAmt += parseFloat(openingAmt);
                                horizontalTotalPrimaryAmt += parseFloat(primaryAmt);
                                horizontalTotalPrimaryFreeAmt += parseFloat(primaryFreeAmt);
                                horizontalTotalSecondaryAmt += parseFloat(secondaryAmt);
                                horizontalTotalPerAchAmt += parseFloat(PerAchAmt);
                                horizontalTotalReturnAmt += parseFloat(returnAmt);
                                horizontalTotalReturnFreeAmt += parseFloat(returnFreeAmt);
                                horizontalTotalExpiryAmt += parseFloat(expiryAmt);
                                horizontalTotalBreakageAmt += parseFloat(breakageAmt);
                                horizontalTotalClosingAmt += parseFloat(closingAmt);
                                //END
                            });
                            productMonthWiseArrayOfObject.push(horizontalTotalTargetQty, horizontalTotalOpeningQty, horizontalTotalPrimaryQty, horizontalTotalPrimaryFreeQty,
                                horizontalTotalSecondaryQty, horizontalTotalPerAchQty, horizontalTotalReturnQty, horizontalTotalReturnFreeQty, horizontalTotalExpiryQty, horizontalTotalBreakageQty,
                                horizontalTotalClosingQty, horizontalTotalTargetAmt, horizontalTotalOpeningAmt, horizontalTotalPrimaryAmt, horizontalTotalPrimaryFreeAmt,
                                horizontalTotalSecondaryAmt, horizontalTotalPerAchAmt, horizontalTotalReturnAmt, horizontalTotalReturnFreeAmt, horizontalTotalExpiryAmt, horizontalTotalBreakageAmt, horizontalTotalClosingAmt);
                            finalArrayOfObject.push(productMonthWiseArrayOfObject);
                        })
                        districtWiseData.push({ district: hqMasterFromSelection.districtName, data: finalArrayOfObject });
                    });
                } else {
                    districtWiseData = [{ district: 'Data not found !', data: [] }];
                }
                return cb(null, { districtWiseData: districtWiseData, months: monthYearArray })
            })
        });
    };

    Primaryandsalereturn.remoteMethod("getPrimaryAndSceondaryHQWiseDump", {
        description: "Target Vs Achievement",
        accepts: [
            {
                arg: "params",
                type: "object",
                http: { source: "body" }
            },
        ],
        returns: {
            root: true,
            type: "array",
        },
        http: {
            verb: "post",
        },
    });

    //--------------------------------END-------------------------------------------------------

    Primaryandsalereturn.getPrimaryAndSceondaryHQWiseDumpMultiMonth = function (params, cb) {
        var self = this;
        var PrimaryandsalereturnCollection = this.getDataSource().connector.collection(Primaryandsalereturn.modelName);
        const TargetCollection = self.getDataSource().connector.collection(Primaryandsalereturn.app.models.Target.modelName);
        //let filterPrimary = {};
        let filterTarget = {};
        let districtIdsMrr = [];
        let districtIdsMrrForTarget = [];
        let districIdMappedWithPoolForSSS = [];
        let districIdMappedWithPoolForTarget = [];
        let districtIdsErp = [];
        let districtNamesMrr = [];
        let groupTarget = {};
        let month = [];
        let year = [];
        let divisionIds = [];
        params.divisionId.forEach(element => {
            divisionIds.push(ObjectId(element))
        });
        let filter = {};
        if (params.product && params.product.length > 0) {
            filter = { where: { companyId: params.companyId, status: true, assignedDivision: { inq: divisionIds }, productCode: { inq: params.product } }, order: "productName ASC" };
        } else {
            filter = { where: { companyId: params.companyId, status: true, assignedDivision: { inq: divisionIds } }, order: "productName ASC" };
        }
        Primaryandsalereturn.app.models.Products.find(filter, async function (err, productDetailsFromMRRMaster) {
            for (let index = 0; index < params.selectedHqDetails.length; index++) {
                const element = params.selectedHqDetails[index];
                if (element.hasOwnProperty("isPoolDistrict") && element.isPoolDistrict[params.companyId]) {
                    districtIdsErp.push(parseInt(element.erpHqCode));
                    let hqMappedWithPoolDetails = await Primaryandsalereturn.app.models.PoolHeadquarterLinking.find({ where: { districtId: ObjectId(element.districtId), status: true } });
                    if (hqMappedWithPoolDetails.length > 0) {
                        districIdMappedWithPoolForSSS.length = 0;
                        districIdMappedWithPoolForTarget.length = 0;
                        const hqMappedWithPoolDetailsJSONParse = (hqMappedWithPoolDetails[0]).toJSON();
                        districtIdsMrrForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                        districIdMappedWithPoolForTarget.push(ObjectId(hqMappedWithPoolDetailsJSONParse.mappedDistrictId));
                        hqMappedWithPoolDetails.forEach((hqMappedWithPool) => {
                            hqMappedWithPool = hqMappedWithPool.toJSON();
                            districtIdsMrr.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                            districIdMappedWithPoolForSSS.push(ObjectId(hqMappedWithPool.mappedDistrictId));
                        });
                        params.selectedHqDetails[index]['districtIdsMappedWithPoolForSSS'] = [...districIdMappedWithPoolForSSS];
                        params.selectedHqDetails[index]['districtIdsMappedWithPoolForTarget'] = [...districIdMappedWithPoolForTarget];
                    }
                } else {
                    districtIdsMrr.push(ObjectId(element.districtId));
                    districtIdsMrrForTarget.push(ObjectId(element.districtId));
                    districtIdsErp.push(parseInt(element.erpHqCode));
                    districtNamesMrr.push(element.districtName);
                }
            }
            const monthYearArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
            for (var i = 0; i < monthYearArray.length; i++) {
                month.push(monthYearArray[i].month);
                year.push(monthYearArray[i].year);
            }
            filterTarget = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: districtIdsMrrForTarget },
                targetMonth: { $in: month },
                targetYear: { $in: year },
                fromDate: { $gte: new Date(params.fromDate) },
                toDate: { $lte: new Date(params.toDate) }
            }
            let filterSSS = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: districtIdsMrr },
                month: { $in: month },
                year: { $in: year },
                fromDate: { $gte: new Date(params.fromDate) },
                toDate: { $lte: new Date(params.toDate) }
            }
            let projectTarget = {
                _id: 0,
                month: "$_id.month",
                year: "$_id.year",
                productId: "$_id.productId",
                districtId: "$_id.districtId",
                targetQty: "$totalTarget",
                creditValue: "$prodData.creditValue",
                productCode: "$prodData.productCode"
            };
            groupTarget = {
                _id: {
                    productId: '$productId',
                    "month": "$targetMonth",
                    "year": "$targetYear",
                    districtId: "$districtId"
                },
                totalTarget: {
                    $sum: "$targetQuantity"
                }
            }

            async.parallel({
                target: function (cb) {
                    TargetCollection.aggregate(
                        // Stage 1
                        {
                            $match: filterTarget
                        },

                        // Stage 2
                        {
                            $group: groupTarget
                        },


                        // Stage 6
                        {
                            $project: projectTarget
                        },
                        function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            cb(null, result)
                        });

                },
                SSS: function (cb) {
                    PrimaryandsalereturnCollection.aggregate(
                        // Stage 1
                        {
                            $match: filterSSS
                        },

                        // Stage 2
                        {
                            $group: {
                                _id: {
                                    "productId": '$productId',
                                    //"netRate": "$netRate",
                                    "month": "$month",
                                    "year": "$year",
                                    "districtId": "$districtId"
                                },
                                productName: { $addToSet: "$productName" },
                                openingQty: { $sum: "$opening" },
                                totalPrimaryQty: { $sum: "$totalPrimaryQty" },
                                totalPrimaryAmt: { $sum: "$totalPrimaryAmt" },
                                secondaryQty: { $sum: "$totalSecondrySaleQty" },
                                totalSecondrySaleAmt: { $sum: "$totalSecondrySaleAmt" },
                                totalSaleReturnQty: { $sum: "$totalSaleReturnQty" },
                                totalSaleReturnAmt: { $sum: "$totalSaleReturnAmt" },
                                closingQty: { $sum: "$closing" },
                                closingAmt: { $sum: "$closingAmt" },
                                primaryFreeQty: { $sum: "$primaryFreeQty" },
                                returnFreeSale: { $sum: "$returnFreeSale" },
                                expiryQty: { $sum: "$expiryQty" },
                                breakageQty: { $sum: "$breakageQty" },
                                batchRecallQty: { $sum: "$batchRecallQty" },
                            }
                        },

                        // Stage 3
                        {
                            $project: {
                                //ProductName: { $arrayElemAt: ["$prod.productName", 0] },
                                productId: "$_id.productId",
                                districtId: "$_id.districtId",
                                month: "$_id.month",
                                //netRate: { $arrayElemAt: ["$prod.netRate", 0] },
                                //ProductCode: { $arrayElemAt: ["$prod.productCode", 0] },
                                openingQty: 1,
                                totalPrimaryQty: 1,
                                totalPrimaryAmt: 1,
                                secondaryQty: 1,
                                totalSecondrySaleAmt: 1,
                                totalSaleReturnQty: 1,
                                totalSaleReturnAmt: 1,
                                closingQty: 1,
                                closingAmt: 1,
                                primaryFreeQty: 1,
                                returnFreeSale: 1,
                                expiryQty: 1,
                                breakageQty: 1,
                                batchRecallQty: 1
                            }
                        }, function (err, res) {
                            if (err) {
                                cb(null, [])
                            }
                            cb(null, res)
                        }

                    );

                }
            }, async function (err, asyncResult) {
                if (err) { return cb(err) }
                let districtWiseData = [];
                if (productDetailsFromMRRMaster.length > 0) {
                    params.selectedHqDetails.forEach(hqMasterFromSelection => {
                        finalArrayOfObject = [];
                        productDetailsFromMRRMaster.forEach(productMasterFromMrr => {
                            let horizontalTotalPrimaryQty = 0, horizontalTotalPrimaryAmt = 0.0, horizontalTotalReturnQty = 0, horizontalTotalReturnAmt = 0.0, horizontalTotalExpiryQty = 0, horizontalTotalExpiryAmt = 0.0, horizontalTotalBreakageQty = 0, horizontalTotalBreakageAmt = 0.0,
                                horizontalTotalOpeningQty = 0, horizontalTotalOpeningAmt = 0.0, horizontalTotalSecondaryQty = 0, horizontalTotalSecondaryAmt = 0.0, horizontalTotalClosingQty = 0, horizontalTotalClosingAmt = 0.0, horizontalTotalPrimaryFreeQty = 0, horizontalTotalPrimaryFreeAmt = 0.0,
                                horizontalTotalReturnFreeQty = 0, horizontalTotalReturnFreeAmt = 0.0, horizontalTotalTargetQty = 0, horizontalTotalTargetAmt = 0.0, horizontalTotalPerAchQty = 0, horizontalTotalPerAchAmt = 0.0;
                            productMonthWiseArrayOfObject = [];
                            let districtName = hqMasterFromSelection.districtName;
                            let productName = productMasterFromMrr.productName;
                            let productRate = productMasterFromMrr.creditValue;
                            productMonthWiseArrayOfObject.push(districtName, productName, productRate);
                            let primaryQty = 0, primaryAmt = 0.0, returnQty = 0, returnAmt = 0.0, expiryQty = 0, expiryAmt = 0.0, breakageQty = 0, breakageAmt = 0.0,
                                openingQty = 0, openingAmt = 0.0, secondaryQty = 0, secondaryAmt = 0.0, closingQty = 0, closingAmt = 0.0, primaryFreeQty = 0, primaryFreeAmt = 0.0,
                                returnFreeQty = 0, returnFreeAmt = 0.0, targetQty = 0, targetAmt = 0.0, perAchQty = 0, PerAchAmt = 0.0;
                            month.forEach(month => {

                                if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
                                    let priQtyVar = 0, priAmtVar = 0.0, returnQtyVar = 0, returnAmtVar = 0.0, expiryQtyVar = 0, expiryAmtVar = 0.0, breakageQtyVar = 0, breakageAmtVar = 0.0,
                                        openingQtyVar = 0, openingAmtVar = 0, seconQtyVar = 0, seconAmtVar = 0, cloQtyVar = 0;
                                    closAmtVar = 0; priFreeQtyVar = 0; priAmtFreeVar = 0; returnFreeQtyVar = 0; returnFreeAmtVar = 0;
                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        hqMasterFromSelection.districtIdsMappedWithPoolForSSS.forEach(poolDistrictId => {
                                            let foundSSSPoolHq = asyncResult.SSS.filter(sss => {
                                                return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss._id.districtId).toString() == (poolDistrictId).toString() && parseInt(sss.month) == parseInt(month)
                                            })
                                            if (foundSSSPoolHq.length > 0) {
                                                priQtyVar = parseInt(priQtyVar) + parseInt(foundSSSPoolHq[0].totalPrimaryQty);
                                                priAmtVar = parseInt(priAmtVar) + parseFloat((foundSSSPoolHq[0].totalPrimaryAmt).toFixed(2));
                                                returnQtyVar = parseInt(returnQtyVar) + parseInt(foundSSSPoolHq[0].totalSaleReturnQty);
                                                returnAmtVar = parseFloat(returnAmtVar) + parseFloat((foundSSSPoolHq[0].totalSaleReturnAmt).toFixed(2));
                                                expiryQtyVar = parseInt(expiryQtyVar) + parseInt(foundSSSPoolHq[0].expiryQty);
                                                expiryAmtVar = parseFloat(expiryAmtVar) + parseFloat(((foundSSSPoolHq[0].expiryQty) * (productRate)).toFixed(2));
                                                breakageQtyVar = parseInt(breakageQtyVar) + parseInt(foundSSSPoolHq[0].breakageQty);
                                                breakageAmtVar = parseFloat(breakageAmtVar) + parseFloat(((foundSSSPoolHq[0].breakageQty) * (productRate)).toFixed(2));
                                                openingQtyVar = parseInt(openingQtyVar) + parseInt(foundSSSPoolHq[0].openingQty);
                                                openingAmtVar = parseFloat(openingAmtVar) + parseFloat((parseInt(foundSSSPoolHq[0].openingQty) * (productRate)).toFixed(2));
                                                seconQtyVar = foundSSSPoolHq[0].secondaryQty ? parseFloat(seconQtyVar) + parseFloat((foundSSSPoolHq[0].secondaryQty).toFixed(2)) : parseFloat(seconQtyVar) + 0;
                                                seconAmtVar = foundSSSPoolHq[0].totalSecondrySaleAmt ? parseFloat(seconAmtVar) + parseFloat((foundSSSPoolHq[0].totalSecondrySaleAmt).toFixed(2)) : parseFloat(seconAmtVar) + 0.0;
                                                cloQtyVar = parseFloat(cloQtyVar) + parseFloat((foundSSSPoolHq[0].closingQty).toFixed(2));
                                                closAmtVar = parseFloat(closAmtVar) + parseFloat((foundSSSPoolHq[0].closingAmt).toFixed(2));
                                                priFreeQtyVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priFreeQtyVar) + parseFloat(foundSSSPoolHq[0].primaryFreeQty) : parseFloat(priFreeQtyVar) + 0;
                                                priAmtFreeVar = foundSSSPoolHq[0].primaryFreeQty ? parseFloat(priAmtFreeVar) + parseFloat((foundSSSPoolHq[0].primaryFreeQty * productRate).toFixed(2)) : parseFloat(priAmtFreeVar) + 0.0;
                                                returnFreeQtyVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeQtyVar) + parseFloat(foundSSSPoolHq[0].returnFreeSale) : parseFloat(returnFreeQtyVar) + 0;
                                                returnFreeAmtVar = foundSSSPoolHq[0].returnFreeSale ? parseFloat(returnFreeAmtVar) + parseFloat((foundSSSPoolHq[0].returnFreeSale * productRate).toFixed(2)) : parseFloat(returnFreeAmtVar) + 0.0;
                                            }

                                        });
                                        primaryQty = priQtyVar;
                                        primaryAmt = priAmtVar;
                                        returnQty = returnQtyVar;
                                        returnAmt = returnAmtVar;
                                        expiryQty = expiryQtyVar;
                                        expiryAmt = expiryAmtVar;
                                        breakageQty = breakageQtyVar;
                                        breakageAmt = breakageAmtVar;
                                        openingQty = openingQtyVar;
                                        openingAmt = openingAmtVar;
                                        secondaryQty = seconQtyVar;
                                        secondaryAmt = seconAmtVar;
                                        closingQty = cloQtyVar;
                                        closingAmt = parseFloat((closAmtVar).toFixed());
                                        primaryFreeQty = priFreeQtyVar;
                                        primaryFreeAmt = priAmtFreeVar;
                                        returnFreeQty = returnFreeQtyVar;
                                        returnFreeAmt = returnFreeAmtVar;
                                    } else {
                                        primaryQty = 0;
                                        primaryAmt = 0.0;
                                        returnQty = 0;
                                        returnAmt = 0.0;
                                        expiryQty = 0;
                                        expiryAmt = 0.0;
                                        breakageQty = 0;
                                        breakageAmt = 0.0;
                                        openingQty = 0;
                                        openingAmt = 0.0;
                                        secondaryQty = 0;
                                        secondaryAmt = 0.0;
                                        closingQty = 0;
                                        closingAmt = 0.0;
                                        primaryFreeQty = 0;
                                        primaryFreeAmt = 0.0;
                                        returnFreeQty = 0;
                                        returnFreeAmt = 0.0;
                                    }
                                } else {
                                    if (asyncResult.SSS != undefined && asyncResult.SSS.length > 0) {
                                        let foundSSS = asyncResult.SSS.filter(sss => {
                                            return (sss.productId).toString() == (productMasterFromMrr.id).toString() && (sss.districtId).toString() == (hqMasterFromSelection.districtId).toString() && parseInt(sss.month) == parseInt(month)
                                        })
                                        if (foundSSS.length > 0) {
                                            primaryQty = foundSSS[0].totalPrimaryQty ? parseInt(foundSSS[0].totalPrimaryQty) : 0;
                                            primaryAmt = foundSSS[0].totalPrimaryAmt ? parseFloat((foundSSS[0].totalPrimaryAmt).toFixed(2)) : 0.0;
                                            returnQty = foundSSS[0].totalSaleReturnQty ? parseInt(foundSSS[0].totalSaleReturnQty) : 0;
                                            returnAmt = foundSSS[0].totalSaleReturnAmt ? parseFloat((foundSSS[0].totalSaleReturnAmt).toFixed(2)) : 0.0;
                                            expiryQty = foundSSS[0].expiryQty ? parseInt(foundSSS[0].expiryQty) : 0;
                                            expiryAmt = foundSSS[0].expiryQty ? parseFloat(((foundSSS[0].expiryQty) * (productRate)).toFixed(2)) : 0.0;
                                            breakageQty = foundSSS[0].breakageQty ? parseInt(foundSSS[0].breakageQty) : 0;
                                            breakageAmt = foundSSS[0].breakageQty ? parseFloat(((foundSSS[0].breakageQty) * (productRate)).toFixed(2)) : 0.0;
                                            openingQty = foundSSS[0].openingQty ? parseInt(foundSSS[0].openingQty) : 0;
                                            openingAmt = parseFloat((parseInt(foundSSS[0].openingQty) * parseFloat(productRate)).toFixed(2));
                                            secondaryQty = foundSSS[0].secondaryQty ? parseInt(foundSSS[0].secondaryQty) : 0;
                                            secondaryAmt = foundSSS[0].totalSecondrySaleAmt ? parseFloat((foundSSS[0].totalSecondrySaleAmt).toFixed(2)) : 0.0;
                                            closingQty = parseInt(foundSSS[0].closingQty);
                                            closingAmt = parseFloat((foundSSS[0].closingAmt).toFixed(2));
                                            primaryFreeQty = foundSSS[0].primaryFreeQty ? parseInt(foundSSS[0].primaryFreeQty) : 0;
                                            primaryFreeAmt = foundSSS[0].primaryFreeQty ? parseFloat((foundSSS[0].primaryFreeQty * productRate).toFixed(2)) : 0.0;
                                            returnFreeQty = foundSSS[0].returnFreeSale ? parseInt(foundSSS[0].returnFreeSale) : 0;
                                            returnFreeAmt = foundSSS[0].returnFreeSale ? parseFloat((foundSSS[0].returnFreeSale * productRate).toFixed(2)) : 0.0;
                                        }
                                    }
                                }

                                if (hqMasterFromSelection.hasOwnProperty("isPoolDistrict") && hqMasterFromSelection.isPoolDistrict[params.companyId]) {
                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        hqMasterFromSelection.districtIdsMappedWithPoolForTarget.forEach(poolDistrictId => {
                                            if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                                let foundTargetPoolHq = asyncResult.target.filter(target => {
                                                    return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (poolDistrictId).toString() && parseInt(target.month) == parseInt(month)
                                                })
                                                if (foundTargetPoolHq.length > 0) {
                                                    targetAmt = foundTargetPoolHq[0].targetQty ? parseFloat((parseInt(foundTargetPoolHq[0].targetQty) * (parseFloat(productRate)).toFixed(2))) : 0.0;
                                                    targetQty = foundTargetPoolHq[0].targetQty ? parseInt(foundTargetPoolHq[0].targetQty) : 0;
                                                    PerAchAmt = parseFloat((parseFloat((primaryAmt / targetAmt)) * 100).toFixed(2));
                                                    perAchQty = parseFloat((parseFloat((primaryQty / targetQty)) * 100).toFixed(2));
                                                }
                                            }
                                        });

                                    } else {
                                        targetAmt = 0.0;
                                        targetQty = 0;
                                        PerAchAmt = 0.0;
                                        perAchQty = 0;
                                    }
                                } else {
                                    if (asyncResult.target != undefined && asyncResult.target.length > 0) {
                                        let foundTarget = asyncResult.target.filter(target => {
                                            return (target.productId).toString() == (productMasterFromMrr.id).toString() && (target.districtId).toString() == (hqMasterFromSelection.districtId).toString() && parseInt(target.month) == parseInt(month)
                                        })
                                        if (foundTarget.length > 0) {
                                            targetAmt = foundTarget[0].targetQty ? parseFloat((parseInt(foundTarget[0].targetQty) * (parseFloat(productRate)).toFixed(2))) : 0.0;
                                            targetQty = foundTarget[0].targetQty ? parseInt(foundTarget[0].targetQty) : 0;
                                            PerAchAmt = parseFloat((parseFloat((primaryAmt / targetAmt)) * 100).toFixed(2));
                                            perAchQty = parseFloat((parseFloat((primaryQty / targetQty)) * 100).toFixed(2));
                                        }
                                    }
                                }
                                productMonthWiseArrayOfObject.push(targetQty, openingQty, primaryQty, primaryFreeQty, secondaryQty, perAchQty,
                                    returnQty, returnFreeQty, expiryQty, breakageQty, closingQty, targetAmt, openingAmt, primaryAmt, primaryFreeAmt,
                                    secondaryAmt, PerAchAmt, returnAmt, returnFreeAmt, expiryAmt, breakageAmt, closingAmt);

                                // Horizontal Productwise Grand Total Work
                                horizontalTotalTargetQty += parseInt(targetQty);
                                horizontalTotalOpeningQty += parseInt(openingQty);
                                horizontalTotalPrimaryQty += parseInt(primaryQty);
                                horizontalTotalPrimaryFreeQty += parseInt(primaryFreeQty);
                                horizontalTotalSecondaryQty += parseInt(secondaryQty);
                                horizontalTotalPerAchQty += parseInt(perAchQty);
                                horizontalTotalReturnQty += parseInt(returnQty);
                                horizontalTotalReturnFreeQty += parseInt(returnFreeQty);
                                horizontalTotalExpiryQty += parseInt(expiryQty);
                                horizontalTotalBreakageQty += parseInt(breakageQty);
                                horizontalTotalClosingQty += parseInt(closingQty);
                                horizontalTotalTargetAmt += parseFloat(targetAmt);
                                horizontalTotalOpeningAmt += parseFloat(openingAmt);
                                horizontalTotalPrimaryAmt += parseFloat(primaryAmt);
                                horizontalTotalPrimaryFreeAmt += parseFloat(primaryFreeAmt);
                                horizontalTotalSecondaryAmt += parseFloat(secondaryAmt);
                                horizontalTotalPerAchAmt += parseFloat(PerAchAmt);
                                horizontalTotalReturnAmt += parseFloat(returnAmt);
                                horizontalTotalReturnFreeAmt += parseFloat(returnFreeAmt);
                                horizontalTotalExpiryAmt += parseFloat(expiryAmt);
                                horizontalTotalBreakageAmt += parseFloat(breakageAmt);
                                horizontalTotalClosingAmt += parseFloat(closingAmt);
                                //END
                            });
                            productMonthWiseArrayOfObject.push(horizontalTotalTargetQty, horizontalTotalOpeningQty, horizontalTotalPrimaryQty, horizontalTotalPrimaryFreeQty,
                                horizontalTotalSecondaryQty, horizontalTotalPerAchQty, horizontalTotalReturnQty, horizontalTotalReturnFreeQty, horizontalTotalExpiryQty, horizontalTotalBreakageQty,
                                horizontalTotalClosingQty, horizontalTotalTargetAmt, horizontalTotalOpeningAmt, horizontalTotalPrimaryAmt, horizontalTotalPrimaryFreeAmt,
                                horizontalTotalSecondaryAmt, horizontalTotalPerAchAmt, horizontalTotalReturnAmt, horizontalTotalReturnFreeAmt, horizontalTotalExpiryAmt, horizontalTotalBreakageAmt, horizontalTotalClosingAmt);
                            finalArrayOfObject.push(productMonthWiseArrayOfObject);
                        })
                        districtWiseData.push({ district: hqMasterFromSelection.districtName, data: finalArrayOfObject });
                    });
                } else {
                    districtWiseData = [{ district: 'Data not found !', data: [] }];
                }
                return cb(null, { districtWiseData: districtWiseData, months: monthYearArray })
            })
        });
    };

    Primaryandsalereturn.remoteMethod("getPrimaryAndSceondaryHQWiseDumpMultiMonth", {
        description: "Target Vs Achievement",
        accepts: [
            {
                arg: "params",
                type: "object",
                http: { source: "body" }
            },
        ],
        returns: {
            root: true,
            type: "array",
        },
        http: {
            verb: "post",
        },
    });

};