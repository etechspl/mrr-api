var ObjectId = require('mongodb').ObjectID;
var asyncLoop = require('node-async-loop');
var moment = require('moment');
'use strict';
var async = require('async');
var status = require('../../utils/statusMessage');
module.exports = function (Products) {


  //-------------------------create product----------------------------//
  Products.createProduct = function (params, cb) {
    console.log("params=", params)
    var self = this;
    var ProductCollection = this.getDataSource().connector.collection(Products.modelName);

    let checkState = [];
    let assignedStates = [];
    let divisionArray = [];
    let stateArray = [];

    for (var i = 0; i < params.assignedStates.length; i++) {
      let stateObject = {
        stateId: ObjectId(params.assignedStates[i]),
        stateStatus: true,
        createdAt: new Date(moment.utc().add('1070', 'minutes')),
        updatedAt: new Date(moment.utc().add('1070', 'minutes'))

      }
      assignedStates.push(stateObject);
      checkState.push(ObjectId(params.assignedStates[i]));
      stateArray.push(ObjectId(params.assignedStates[i]))
    }
    if (params.divisionId != null) {
      divisionArray.push(ObjectId(params.divisionId[0]))
    }
    let match = {}
    if (params.divisionId != null) {
      match = {
        companyId: ObjectId(params.companyId),
        assignedStates: { inq: checkState },
        assignedDivision: divisionArray,
        productName: params.productName,
        productCode: params.productCode
      }
    }
    else {
      match = {
        companyId: ObjectId(params.companyId),
        assignedStates: { inq: checkState },
        productName: params.productName,
        productCode: params.productCode
      }
    }

    Products.find({
      where: match
    }, function (err, response) {
      if (response.length > 0) {
        var err = new Error('Products Already Exists');
        err.statusCode = 409;
        err.code = 'Validation failed';
        return cb(err);
      }
      else {
        var productDetails = {
          companyId: ObjectId(params.companyId),
          assignedDivision: divisionArray,
          productName: params.productName,
          productCode: params.productCode,
          productType: params.productType,
          productShortName: params.productShortName,
          productPackageSize: params.productPackageSize,
          samplePackageSize: params.samplePackageSize,
          ptr: parseFloat(params.ptr),
          ptw: parseFloat(params.ptw),
          mrp: parseFloat(params.mrp),
          sampleRate: parseFloat(params.sampleRate),
          includeVat: parseFloat(params.includeVat),
          assignedStates: stateArray,
          assignedStatesInfo: assignedStates,
          status: true,
          createdAt: new Date(moment.utc().add('1070', 'minutes')),
          updatedAt: new Date(moment.utc().add('1070', 'minutes'))
        }
        ProductCollection.insertOne(productDetails, function (err, res) {
          if (err) {
            console.log(err);
            return cb(err);
          }
          console.log("inserted products", res)
          return cb(false, res)
        })
      }
    })

  }
  Products.remoteMethod(
    'createProduct', {
    description: 'Created Products',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  }
  )
  //-----------------------------------------------------------------//
  //-------------get product details with sample balance----by preeti arora 25-09-2019---------
  Products.getProductsWithSampleQty = function (params, cb) {
    var self = this;
    var viewProductCollection = this.getDataSource().connector.collection(Products.modelName);
    let match = {
      companyId: ObjectId(params.companyId),
      assignedStates: ObjectId(params.assignedStates),
      status: params.status
    }

    if (params.isDivisionExist == true) {
      match.assignedDivision = ObjectId(params.divisionId)
    }
    // I Preeti changed the IF condition on 3-1-2020 coz vrushaili mam has skiped one key name sampleIssue, we will chnage this at the new updation of MRR
    if (params.sampleIssue == false) {
      viewProductCollection.aggregate(
        {
          $match: match
        },
        {
          $project: {
            id: "$_id",
            productName: 1,
            status: 1,
            assignedStates: 1,
            mrp: 1,
            ptr: 1,
            ptw: 1,
            productType: 1,
            fileId: 1,
            sampleIssueBalance: { $literal: 0 }
          }
        }, function (err, finalResult) {
          return cb(false, finalResult)
        }
      );
    } else {
      async.parallel({
        productDetail: function (cb) {
          viewProductCollection.aggregate(
            {
              $match: match
            },
            {
              $project: {
                id: "$_id",
                productName: 1,
                status: 1,
                assignedStates: 1,
                mrp: 1,
                ptr: 1,
                ptw: 1,
                productType: 1,
                fileId: 1,
                sampleIssueBalance: { $literal: 0 }
              }
            }, function (err, result) {
              cb(false, result);

            }
          );
        },
        sampleDetail: function (cb) {
          Products.app.models.SampleGiftIssueDetail.getSampleBalance(params, function (err, result) {
            console.log("sample res=>", result);
            cb(false, result);

          })
        }
      }, function (err, asyResponse) {
        let finalData = [];
        for (let i = 0; i < asyResponse.productDetail.length; i++) {

          let filterObj = asyResponse.sampleDetail.filter(function (sampleObj) {
            console.log("sample->", typeof (sampleObj._id.toString()));
            console.log("products->", typeof (asyResponse.productDetail[i]._id.toString()));
            console.log(sampleObj._id.toString() == asyResponse.productDetail[i]._id.toString());
            return sampleObj._id.toString() == asyResponse.productDetail[i]._id.toString();
          });
          if (filterObj.length > 0) {
            finalData.push({
              id: asyResponse.productDetail[i]._id,
              productName: asyResponse.productDetail[i].productName,
              assignedStates: asyResponse.productDetail[i].assignedStates,
              sampleIssueBalance: filterObj[0].sampleIssueBalance,
              mrp: asyResponse.productDetail[i].mrp,
              ptr: asyResponse.productDetail[i].ptr,
              ptw: asyResponse.productDetail[i].ptw,
              productType: asyResponse.productDetail[i].productType,
              fileId: asyResponse.productDetail[i].fileId,
            })
          } else {
            finalData.push({
              id: asyResponse.productDetail[i]._id,
              productName: asyResponse.productDetail[i].productName,
              assignedStates: asyResponse.productDetail[i].assignedStates,
              sampleIssueBalance: 0,
              mrp: asyResponse.productDetail[i].mrp,
              ptr: asyResponse.productDetail[i].ptr,
              ptw: asyResponse.productDetail[i].ptw,
              productType: asyResponse.productDetail[i].productType,
              fileId: asyResponse.productDetail[i].fileId,
            })
          }

        }
        return cb(false, finalData)
      })
    }


  },
    Products.remoteMethod(
      'getProductsWithSampleQty', {
      description: 'View Products',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'object'
      },
      http: {
        verb: 'get'
      }
    }
    )
  //---------------------------------------------------
  //----------------delete methods------------------//
  Products.deleteProduct = function (params, cb) {
    var self = this;
    var ProductCollection = this.getDataSource().connector.collection(Products.modelName);

    Products.find({
      where: {
        companyId: ObjectId(params.companyId),
        _id: ObjectId(params._id)
      }
    }, function (err, response) {
      if (response.length > 0) {
        ProductCollection.update({ companyId: ObjectId(params.companyId), _id: ObjectId(params._id) }, { $set: { "status": false } }, function (err, res) {
          if (err) {
            console.log(err);
            return cb(err);
          } else {
            console.log("updated products", res)
            return cb(false, res)
          }
        })
      }
      else {
        var err = new Error('Product cannot be deleted.');
        err.statusCode = 409;
        err.code = 'Validation failed';
        return cb(err);
      }
    })
  }
  Products.remoteMethod(
    'deleteProduct', {
    description: 'Delete Products',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  }
  )
  //-------------------------------------------------//
  //---------------------edit products-----------------//
  Products.editProduct = function (params, cb) {
    var self = this;
    let selectedStates = [];
    let selectedDivision = [];

    var ProductCollection = this.getDataSource().connector.collection(Products.modelName);
    for (var i = 0; i < params.assignedStates.length; i++) {
      selectedStates.push(ObjectId(params.assignedStates[i]));
    }
    match = {
      companyId: ObjectId(params.companyId),
      _id: ObjectId(params.productId),
      assignedStates: { $in: selectedStates },
      status: true
    }
    if (params.divisionId != null) {
      for (var i = 0; i < params.divisionId.length; i++) {
        selectedDivision.push(ObjectId(params.divisionId[i]))
      }
      match.assignedDivision = { $in: selectedDivision }
    }
    let dataStateArra = [];
    let StateArr = [];
    let DivisionArr = [];

    ProductCollection.aggregate({
      $match: match
    }, function (err, result) {
      for (var k = 0; k < selectedStates.length; k++) {
        let flag = false;
        if (result.length > 0) {
          for (var i = 0; i < result[0].assignedStates.length; i++) {
            if (selectedStates[k].equals(result[0].assignedStates[i])) {
              flag = true;
              break;
            }
            else {
              continue;
            }
          }
        }
        if (flag == false) {
          stateObject = {
            stateId: ObjectId(selectedStates[k]),
            stateStatus: true,
            createdAt: new Date(),
            updatedAt: new Date(),

          }
          dataStateArra.push(stateObject);
          StateArr.push(ObjectId(selectedStates[k]))

        }

      }
      //--------------add divison editing option----------------
      if (params.divisionId != null) {
        for (var k = 0; k < selectedDivision.length; k++) {
          let flag = false;
          if (result.length > 0) {
            for (var i = 0; i < result[0].assignedDivision.length; i++) {
              if (selectedDivision[k].equals(result[0].assignedDivision[i])) {
                flag = true;
                break;
              }
              else {
                continue;
              }
            }
          }
          if (flag == false) {

            DivisionArr.push(ObjectId(selectedDivision[k]))

          }

        }
      }
      if (result.length > 0) {
        for (var i = 0; i < result[0].assignedDivision.length; i++) {
          DivisionArr.push(result[0].assignedDivision[i])
        }
        //----------------------------------
        for (var i = 0; i < result[0].assignedStates.length; i++) {
          stateObject = {
            stateId: ObjectId(result[0].assignedStates[i]),
            stateStatus: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          }
          dataStateArra.push(stateObject);
          StateArr.push(result[0].assignedStates[i])
        }
      }


      ProductCollection.update({ companyId: ObjectId(params.companyId), _id: ObjectId(params.productId) },
        {
          $set: {
            "productName": params.productName, "productCode": params.productCode, "productType": params.productType,
            "productShortName": params.productShortName,
            "productPackageSize": params.productPackageSize,
            "samplePackageSize": params.samplePackageSize, "ptw": params.ptw, "ptr": params.ptr
            , "mrp": params.mrp, "sampleRate": params.sampleRate
            , "includeVat": params.includeVat, "assignedStates": StateArr, "assignedDivision": DivisionArr, "updatedAt": new Date(moment.utc().add("1070", "minutes"))
          }
        }, function (err, res) {
          if (err) {
            console.log(err);
            return cb(err);
          } else {
            return cb(false, res)
          }
        })
    })


  }
  Products.remoteMethod(
    'editProduct', {
    description: 'Edited Products',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  }
  )
  //-------------------end-------------------------------------
  //----------------------------view products----------------------------

  Products.getProductDetail = function (params, cb) {
    console.log("params-product-", params);
    var self = this;
    var viewProductCollection = this.getDataSource().connector.collection(Products.modelName);
    let selectedStates = [];
    let selectedDivisions = [];

    for (var i = 0; i < params.stateInfo.length; i++) {
      selectedStates.push(ObjectId(params.stateInfo[i]));
    }
    if (params.divisionId != null) {
      for (var i = 0; i < params.divisionId.length; i++) {
        selectedDivisions.push(ObjectId(params.divisionId[i]));
      }
    }
    if (params.divisionId != null) {
      match = {
        companyId: ObjectId(params.companyId),
        assignedStates: { $in: selectedStates },
        assignedDivision: selectedDivisions,
        status: true
      }

    } else {
      match = {
        companyId: ObjectId(params.companyId),
        assignedStates: { $in: selectedStates },
        status: true
      }
    }
    viewProductCollection.aggregate(
      {
        // Stage 1
        $match: match
      },
      // Stage 2
      {
        $unwind: { path: "$assignedStates", preserveNullAndEmptyArrays: true }
      },

      // Stage 3
      {
        $unwind: { path: "$divisionId", preserveNullAndEmptyArrays: true }
      },
      {
        $match: match

      },
      // Stage 5
      {
        $lookup: {
          "from": "State",
          "localField": "assignedStates",
          "foreignField": "_id",
          "as": "stateData"
        }
      },
      // Stage 6
      {
        $lookup: {
          "from": "DivisionMaster",
          "localField": "divisionId.divisionId",
          "foreignField": "_id",
          "as": "divisionData"
        }
      },
      // Stage 7
      {
        $project: {
          _id: 1,
          productName: 1,
          productCode: 1,
          productType: 1,
          productShortName: 1,
          productPackageSize: 1,
          ptr: 1,
          ptw: 1,
          mrp: 1,
          samplePackageSize: 1,
          sampleRate: 1,
          includeVat: 1,
          stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
          divisionData: { $arrayElemAt: ["$divisionData.divisionName", 0] },
          stateId: { $arrayElemAt: ["$stateData._id", 0] },
          divisionId: { $arrayElemAt: ["$divisionData._id", 0] },
        }
      },
      // Stage 7
      {
        $sort: {
          productName: 1
        }
      }
      // {
      //   $group: {
      //     "_id": {
      //       _id:"$_id",
      //       productName: "$productName",
      //       productCode: "$productCode",
      //       productType:"$productType",
      //       productShortName:"$productShortName",
      //       productPackageSize:"$productPackageSize",
      //       samplePackageSize:"$samplePackageSize",
      //       ptr: "$ptr",
      //       ptw: "$ptw",
      //       mrp: "$mrp",
      //       sampleRate: "$sampleRate",
      //       includeVat: "$includeVat",
      //     },
      //     stateInfo: {
      //       $addToSet: {
      //         "stateName" : "$stateName",
      //         "id" : "$stateId"
      //     }
      //       },

      //     divisionInfo: {
      //       $addToSet:{ 
      //         "divisionName" : "$divisionData",
      //         "divisionId" : "$divisionId"
      //         }
      //     },

      //   }
      // },
      // {
      //   $unwind: "$stateInfo"
      // },
      // {
      //   $unwind: "$divisionInfo"
      // },
      // {
      // $match:{
      // "stateInfo.id":
      //   {
      //     $in:selectedStates
      //   }
      // }
      //   }
      ,
      function (err, result) {
        cb(false, result)
      })

  }
  Products.remoteMethod(
    'getProductDetail', {
    description: 'View Products',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'get'
    }
  }
  )

  // For DCR submission
  Products.getProductDetailOnStateBasis = function (params, cb) {

    var self = this;

    var viewProductCollection = this.getDataSource().connector.collection(Products.modelName);

    console.log(params.assignedStates);

    viewProductCollection.aggregate(

      // Stage 1

      {

        $match: {

          assignedStates: ObjectId(params.assignedStates),

          companyId: ObjectId(params.companyId),

          status: true



        }

      },



      // Stage 2

      {

        $group: {

          _id: {

            id: "$id",

            "productName": "$productName",

            //"ptw" :  { $substr:["$ptw",0,-1]}, 

            //"mrp" : { $substr:["$mrp",0,-1]}, 

            "ptr": { $substr: ["$ptr", 0, -1] },

          }



        }

      },
      // Stage 3
      {

        $project: {

          _id: 0,

          label: "$_id.productName",

          value: { $concat: ["$_id.id", "#-#", "$_id.productName", "#-#", "$_id.ptr"] }

        }

      }, function (err, result) {

        if (err) {

          return cb(err)

        }

        return cb(false, result)

      }

    );



  }

  Products.remoteMethod(

    'getProductDetailOnStateBasis', {

    description: 'View Products',

    accepts: [{

      arg: 'params',

      type: 'object'

    }],

    returns: {

      root: true,

      type: 'array'

    },

    http: {

      verb: 'get'

    }

  }
  )


  Products.getFocusProducts = function (params, cb) {
    const self = this;
    const ProductCollection = this.getDataSource().connector.collection(Products.modelName);
    let ids = [];
    params.productIds.forEach(element => {
      ids.push(ObjectId(element));
    });

    ProductCollection.aggregate({
      $match: {
        //status:true,
        _id: { $in: ids }
      }
    }, {
      $group: {
        _id: null,
        products: { $addToSet: "$productName" }
      }
    }, {
      $project: {
        _id: 0,
        products: 1
      }
    }, {
      cursor: {
        batchSize: 50
      },

      allowDiskUse: true
    }, (err, result) => {
      if (err) {
        cb(err)
      }
      return cb(false, result);
    })
  }

  Products.remoteMethod(
    'getFocusProducts', {
    description: 'Focus Products name based on ids',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  )


  Products.getProductsForEditForm = function (params, cb) {
    let ids = [];
    params.value.ProductInfo.forEach(element => {
      ids.push(ObjectId(element.id));
    });


    Products.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          status: true
          //assignedStates :{inq:ObjectId(params.value.stateId)},
          //id:{neq : ids}
        },
      },
      function (err, response) {
        if (response.length > 0) {
          return cb(false, response);
        } else {
          return cb(false, []);
        }

      })

  }

  Products.remoteMethod(
    'getProductsForEditForm', {
    description: 'get Products name based on ids',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'get'
    }
  }
  )

  // ERP MASTER APIs Work Start By Ravi on 04/28/2021

  Products.create_or_update = function (params, cb) {
    if (params) {
      if (params.hasOwnProperty('companyCode') && params.hasOwnProperty('productCode') && params.hasOwnProperty('productName')) {
        Products.app.models.CompanyMaster.findOne({ "where": { erpCode: parseInt(params.companyCode) } }, function (err1, companyId) {
          if (err1) { return cb(false, status.getStatusMessage(2)) }
          if (companyId) {
            Products.findOne({ where: { erpCode: parseInt(params['productCode']) } }, function (err4, isProductExits) {
              if (err4) { return cb(false, status.getStatusMessage(2)) }
              if (isProductExits) {
                isProductExits.companyId = ObjectId(isProductExits.companyId);
                isProductExits.productName = params.productName;
                isProductExits.productType = params.hasOwnProperty('productType') ? params.productType : isProductExits.productType;
                isProductExits.productShortName = params.hasOwnProperty('productShortName') ? params.productShortName : isProductExits.productShortName;
                isProductExits.productPackageSize = params.hasOwnProperty('productPackageSize') ? params.productPackageSize : isProductExits.productPackageSize;
                isProductExits.ptw = params.hasOwnProperty('ptw') ? params.ptw : isProductExits.ptw;
                isProductExits.ptr = params.hasOwnProperty('ptr') ? params.ptr : isProductExits.ptr;
                isProductExits.mrp = params.hasOwnProperty('mrp') ? params.mrp : isProductExits.mrp;
                Products.replaceOrCreate(isProductExits, function (err, replace) {
                  if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                  if (replace) {
                    console.log();
                    return cb(false, status.getStatusMessage(11))
                  }
                })
              } else {
                const productObj = {
                  "companyId": ObjectId(companyId.id),
                  "productName": params.productName,
                  "productCode": params.productCode,
                  "erpCode": params.productCode,
                  "productType": params.hasOwnProperty('productType') ? params.productType : 'NA',
                  "productShortName": params.hasOwnProperty('productShortName') ? params.productShortName : 'NA',
                  "productPackageSize": params.hasOwnProperty('productPackageSize') ? params.productPackageSize : 'NA',
                  "ptw": params.hasOwnProperty('ptw') ? params.ptw : 0,
                  "ptr": params.hasOwnProperty('ptr') ? params.ptr : 0,
                  "mrp": params.hasOwnProperty('mrp') ? params.mrp : 0,
                  "assignedStates": ["erp"],
                  "status": true,
                  "sampleRate": 0
                }
                Products.replaceOrCreate(productObj, function (err, create) {
                  if (err) { return cb(false, status.getStatusMessage(2, err.message)) }
                  if (create) {
                    return cb(false, status.getStatusMessage(10))
                  }
                })
              }
            })
          } else {
            return cb(false, status.getStatusMessage(7));
          }
        });

      } else {
        return cb(false, status.getStatusMessage(1));
      }
    } else {
      return cb(false, status.getStatusMessage(0))
    }
  }

  Products.remoteMethod(
    'create_or_update', {
    description: 'Add and Update Product  Master',
    accepts: [{
      arg: 'params',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'object'
    },
    http: {
      verb: 'post'
    }
  });

  //END

};
