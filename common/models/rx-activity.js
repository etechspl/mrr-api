'use strict';

module.exports = function(Rxactivity) {
// update API for Sync Rxactivity
  Rxactivity.updateAllRecord = function(records, cb) {

    var self = this;
    var response = [];
    var ids = [];
    var RxactivityCollection = self.getDataSource().connector.collection(Rxactivity.modelName);
    if (records.length === 0 || records === "") {
      var err = new Error('records is undefined or empty');
      err.statusCode = 404;
      err.code = 'RECORDS ARE NOT FOUND';
      return cb(err);
    }

    for (var w = 0; w < records.length; w++) {

      Rxactivity.replaceById(
        records[w].id,
        records[w],
        function(err, result) {
          if (err) {
            console.log(err);
          }
          

        }
      );
    }
    return cb(false, records);

  };
  // End Rxactivity Record


  // Delete API for Rxactivity
  Rxactivity.deleteAllRecord = function(records, cb) {


    var self = this;
    var response = [];
    var ids = [];
    var RxactivityCollection = self.getDataSource().connector.collection(Rxactivity.modelName);
    if (records.length === 0 || records === "") {
      var err = new Error('records is undefined or empty');
      err.statusCode = 404;
      err.code = 'RECORDS ARE NOT FOUND';
      return cb(err);
    }

    for (var w = 0; w < records.length; w++) {

      Rxactivity.destroyById(
        records[w].id,
        function(err) {
          if (err) {
            console.log(err);
          }


        }
      );


    }

    return cb(false, "Record has been Deleted Sucessfully");

  };
  // Update All Data for sync
    // Update All Data for sync

  Rxactivity.remoteMethod(
    'updateAllRecord', {
      description: 'update and send response all data',
      accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'post'
      }
    }
  );

  // Delete All Data
  Rxactivity.remoteMethod(
    'deleteAllRecord', {
      description: 'Delete and send response all data',
      accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'post'
      }
    }
  );



};
