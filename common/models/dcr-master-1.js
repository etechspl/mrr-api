'use strict';
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var asyncLoop = require('node-async-loop');
 module.exports = function(Dcrmaster) {

   
    Dcrmaster.observe('after save', function (ctx, next) {
        //ctx.data will come when update api is called.
        //Updating fileId in objectId in dcrExpense object.
        if (ctx.data != undefined) {
            if (ctx.data.hasOwnProperty("dcrExpense")) {
                let dcrExpenseObject = ctx.data.dcrExpense;
                for (let i = 0; i < dcrExpenseObject.length; i++) {
                    if (dcrExpenseObject[i].hasOwnProperty("fileId")) {
                        dcrExpenseObject[i]["fileId"] = ObjectId(dcrExpenseObject[i]["fileId"]);
                    }
                }
                let passingObject = {
                    id: ctx.where.id,
                    dcrExpenseObject: dcrExpenseObject,
                    updateImageFor: "updateFileIdIntoObjectId"
                }
                Dcrmaster.updateImage(passingObject, function (err, dcrResult) {
                    if (err) {
                        console.log(err)
                    }
                    next();
                });
            } else {
                next();
            }
        } else if (ctx.instance === undefined) {
            next()
        } else {
            let dcrObj = JSON.parse(JSON.stringify(ctx.instance));

            Dcrmaster.app.models.CompanyMaster.findOne({
                where: {
                    id: dcrObj.companyId
                },
                fields: {
                    isDivisionExist: true,
                    expense: true
                }
            }, function (err, res) {
                if (res !== null && res.hasOwnProperty('expense') === true) {
                    let isDivisionExist = res.isDivisionExist;
                    console.log("=======================================");
                    console.log("Da Type ", res.expense.daType);
                    console.log("=======================================");
                    switch (res.expense.daType) {
                        case "Designation-State Wise":
                            //getting Designation of User for getting DA
                            Dcrmaster.app.models.UserInfo.findOne({
                                "where": {
                                    userId: dcrObj.submitBy,
                                    companyId: dcrObj.companyId
                                }
                            },
                                function (err, user) {
                                    if (err) {
                                        next();
                                    } else {
                                        let currentDCRType = [];
                                        let expenseObj = {
                                            "visitedBlock": dcrObj.visitedBlock,
                                            "dcrId": dcrObj.id,
                                            "dcrDate": dcrObj.dcrDate,
                                            "companyId": dcrObj.companyId,
                                            "userId": dcrObj.submitBy,
                                            "workingStatus": dcrObj.workingStatus,
                                            "stateId": dcrObj.stateId,
                                            "districtId": dcrObj.districtId,
                                            "remarks": dcrObj.remarks,
                                            "da": 0,
                                            "daMgr": 0,
                                            "daAdmin": 0,

                                            "distance": 0,
                                            "distanceMgr": 0,
                                            "distanceAdmin": 0,

                                            "fare": 0,
                                            "fareMgr": 0,
                                            "fareAdmin": 0,

                                            "miscExpense": 0,
                                            "miscExpenseMgr": 0,
                                            "miscExpenseAdmin": 0,

                                            "totalExpense": 0,
                                            "totalExpenseMgr": 0,
                                            "totalExpenseAdmin": 0
                                        };
                                        //Day status is working or half working
                                        let fare = 0;
                                        let distance = 0;
                                        //setting expense object

                                        if (dcrObj.hasOwnProperty("jointWork") === true) {
                                            if (dcrObj.jointWork.length > 0) {
                                                let jointWork = [];
                                                for (let obj of dcrObj.jointWork) {
                                                    jointWork.push(obj.Name)
                                                }
                                                expenseObj["jointWork"] = jointWork;
                                            }
                                        }

                                        if (dcrObj.hasOwnProperty("dcrExpense") === true) {
                                            if (dcrObj.dcrExpense.length > 0) {
                                                let totalMisc = 0;
                                                totalMisc = dcrObj.dcrExpense.reduce((prev, curr) => {
                                                    return prev + curr.amount
                                                }, totalMisc)
                                                expenseObj["miscExpense"] = totalMisc;
                                            }
                                        }
                                        if ((dcrObj.workingStatus === 'Working' || dcrObj.workingStatus === 'Working & Half Day Leave') && dcrObj.visitedBlock.length > 0) {

                                            let from = 0;
                                            let to = dcrObj.visitedBlock.length - 1;
                                            //let finalResult = {};
                                            asyncLoop(dcrObj.visitedBlock, function (item, next) {
                                                //check if stp is exist of not
                                                Dcrmaster.app.models.STP.findOne({
                                                    where: {
                                                        companyId: dcrObj.companyId,
                                                        userId: dcrObj.submitBy,
                                                        appStatus: 'approved',
                                                        status: true,
                                                        or: [{
                                                            fromArea: item.fromId,
                                                            toArea: item.toId
                                                        }, {
                                                            fromArea: item.toId,
                                                            toArea: item.fromId
                                                        }]
                                                    },
                                                    include: [{
                                                        "relation": "frc",
                                                        "scope": {
                                                            rate: true
                                                        }
                                                    }]
                                                }, function (err, stpResult) {
                                                    //if stp exists
                                                    if (stpResult !== null) {
                                                        let stp = JSON.parse(JSON.stringify(stpResult));
                                                        //getting misc expense when dcr submitting
                                                        //here i m calculating da for the first stp pair only
                                                        if (from === 0) {
                                                            //getting DA from the DailyAllowance based on designation & statewise based on stp type
                                                            Dcrmaster.app.models.DailyAllowance.findOne({
                                                                where: {
                                                                    companyId: dcrObj.companyId,
                                                                    stateId: dcrObj.stateId,
                                                                    designation: user.designation,
                                                                    type: res.expense.daType
                                                                }
                                                            }, function (err, da) {
                                                                if (err)
                                                                    next()


                                                                expenseObj["type"] = stp.type;
                                                                if (stp.type === 'HQ') {
                                                                    expenseObj["da"] = da.daHQ;
                                                                } else if (stp.type === 'EX') {
                                                                    expenseObj["da"] = da.daEX;
                                                                } else if (stp.type === 'OUT') {

                                                                    expenseObj["da"] = da.daOUT;
                                                                }
                                                            })
                                                        }
                                                        //travelling allowance for all stp mapping a-->b--->c then sum of the distance a-->b then sum of distance b-c
                                                        distance = distance + stp.distance;
                                                        if (stp.frc.allounceToBeGet === 'KM Wise') {
                                                            fare = fare + (stp.distance * stp.frc.rate)
                                                        } else if (stp.frc.allounceToBeGet === 'Lumsum') {
                                                            fare = fare + stp.frc.rate
                                                        }
                                                        expenseObj["distance"] = distance;
                                                        expenseObj["fare"] = fare;
                                                        currentDCRType.push(stp.type);
                                                    } else {
                                                        if (from === 0) {
                                                            expenseObj["fare"] = 0;
                                                            expenseObj["distance"] = 0;
                                                            //getting DA from DailyAllouwance
                                                            Dcrmaster.app.models.Area.getAreaDailyAllowance({
                                                                companyId: dcrObj.companyId,
                                                                stateId: dcrObj.stateId,
                                                                areaId: item.toId
                                                            }, function (err, da) {
                                                                if (err)
                                                                    next()
                                                                if (da == undefined || da == null) {
                                                                    expenseObj.da = 0;
                                                                    expenseObj.type = "";
                                                                }

                                                                if (da.length > 0) {
                                                                    expenseObj.da = da[0].da;
                                                                    expenseObj.type = da[0].type
                                                                } else {
                                                                    console.log("else Replacement character");
                                                                    expenseObj.da = 0;
                                                                    expenseObj.type = "";
                                                                }
                                                            })
                                                        } else {
                                                            console.log("Replacement character");
                                                        }
                                                    }
                                                    from = from + 1;
                                                    next();
                                                })

                                            }, function (err) {
                                                console.log("I am in Final... : ", currentDCRType);
                                                let previousExpObject = [{ dcrFor: '', dcrDate: '', isOutSide: false, distance: 0, fare: 0 }, { dcrFor: '', dcrDate: '', distance: 0, fare: 0 }]
                                                let beforePreviousExpObject = {};
                                                if (currentDCRType.includes("OUT")) {
                                                    console.log("Its Out Station"); // Yesterday DCR
                                                    Dcrmaster.find({
                                                        where: {
                                                            submitBy: dcrObj.submitBy,
                                                            dcrDate: prevousDateArray[1],
                                                            DCRStatus: 1
                                                        }
                                                    }, function (err, dcrRes) {
                                                        previousExpObject[0]["dcrFor"] = "Previous";
                                                        previousExpObject[0]["dcrDate"] = prevousDateArray[1];
                                                        let yesterdayDCRType = [];
                                                        let prvDistance = 0;
                                                        let prvFare = 0;
                                                        if (dcrRes.length > 0) {
                                                            asyncLoop(dcrRes[0].visitedBlock, function (item, next) {
                                                                previousExpObject[0]["isOutSide"] = (item.fromId.toString() == item.toId.toString() ? false : true);
                                                                //check if stp is exist of not
                                                                Dcrmaster.app.models.STP.findOne({
                                                                    where: {
                                                                        companyId: dcrObj.companyId,
                                                                        userId: dcrObj.submitBy,
                                                                        appStatus: 'approved',
                                                                        status: true,
                                                                        or: [{
                                                                            fromArea: item.fromId,
                                                                            toArea: item.toId
                                                                        }, {
                                                                            fromArea: item.toId,
                                                                            toArea: item.fromId
                                                                        }]
                                                                    },
                                                                    include: [{
                                                                        "relation": "frc",
                                                                        "scope": {
                                                                            rate: true
                                                                        }
                                                                    }]
                                                                }, function (err, stpResult) {
                                                                    //if stp exists
                                                                    //console.log("Line263 : ", stpResult);

                                                                    if (stpResult !== null) {
                                                                        const stp = JSON.parse(JSON.stringify(stpResult));
                                                                        yesterdayDCRType.push(stp.type);

                                                                        prvDistance = prvDistance + stp.distance;
                                                                        if (stp.frc.allounceToBeGet === 'KM Wise') {
                                                                            prvFare = prvFare + (stp.distance * stp.frc.rate)
                                                                        } else if (stp.frc.allounceToBeGet === 'Lumsum') {
                                                                            prvFare = prvFare + stp.frc.rate
                                                                        }
                                                                        previousExpObject[0]["distance"] = prvDistance;
                                                                        previousExpObject[0]["fare"] = prvFare;
                                                                    }
                                                                    next();
                                                                })
                                                            }, function (err) {
                                                                console.log("Line 270 : ", yesterdayDCRType);
                                                                if (yesterdayDCRType.includes("OUT")) {
                                                                    Dcrmaster.find({
                                                                        where: {
                                                                            submitBy: dcrObj.submitBy,
                                                                            dcrDate: prevousDateArray[0],
                                                                            DCRStatus: 1
                                                                        }
                                                                    }, function (err, dcrRes) {
                                                                        previousExpObject[1]["dcrFor"] = "BeforePrevious";
                                                                        previousExpObject[1]["dcrDate"] = prevousDateArray[0];
                                                                        let beforePrvDistance = 0;
                                                                        let beforePrvFare = 0;
                                                                        let dayBeforeYesterdayDCRType = [];
                                                                        if (dcrRes.length > 0) {
                                                                            asyncLoop(dcrRes[0].visitedBlock, function (item, next) {
                                                                                //check if stp is exist of not
                                                                                Dcrmaster.app.models.STP.findOne({
                                                                                    where: {
                                                                                        companyId: dcrObj.companyId,
                                                                                        userId: dcrObj.submitBy,
                                                                                        appStatus: 'approved',
                                                                                        status: true,
                                                                                        or: [{
                                                                                            fromArea: item.fromId,
                                                                                            toArea: item.toId
                                                                                        }, {
                                                                                            fromArea: item.toId,
                                                                                            toArea: item.fromId
                                                                                        }]
                                                                                    },
                                                                                    include: [{
                                                                                        "relation": "frc",
                                                                                        "scope": {
                                                                                            rate: true
                                                                                        }
                                                                                    }]
                                                                                }, function (err, stpResult) {
                                                                                    //if stp exists
                                                                                    //console.log("Line305 : ", stpResult);

                                                                                    if (stpResult !== null) {
                                                                                        const stp = JSON.parse(JSON.stringify(stpResult));
                                                                                        dayBeforeYesterdayDCRType.push(stp.type);

                                                                                        beforePrvDistance = beforePrvDistance + stp.distance;
                                                                                        if (stp.frc.allounceToBeGet === 'KM Wise') {
                                                                                            beforePrvFare = beforePrvFare + (stp.distance * stp.frc.rate)
                                                                                        } else if (stp.frc.allounceToBeGet === 'Lumsum') {
                                                                                            beforePrvFare = beforePrvFare + stp.frc.rate
                                                                                        }
                                                                                        previousExpObject[1]["distance"] = beforePrvDistance;
                                                                                        previousExpObject[1]["fare"] = beforePrvFare;
                                                                                    }
                                                                                    next();
                                                                                })
                                                                            }, function (err) {
                                                                                console.log("Line 310 : ", dayBeforeYesterdayDCRType);
                                                                                if (dayBeforeYesterdayDCRType.includes("OUT")) {
                                                                                    Dcrmaster.app.models.DailyAllowance.findOne({
                                                                                        companyId: dcrObj.companyId,
                                                                                        stateId: dcrObj.stateId,
                                                                                        designation: user.designation,
                                                                                        //type: 'EX'
                                                                                    }, function (err, da) {
                                                                                        if (err)
                                                                                            next();

                                                                                        expenseObj["type"] = 'EX';
                                                                                        expenseObj["da"] = da.daEX;
                                                                                        console.log("========================");
                                                                                        //console.log(previousExpObject);
                                                                                        //New Insertion
                                                                                        Dcrmaster.app.models.TourProgram.findOne({
                                                                                            where: {
                                                                                                companyId: dcrObj.companyId,
                                                                                                createdBy: dcrObj.submitBy,
                                                                                                date: dcrObj.dcrDate
                                                                                            }
                                                                                        }, function (err, result) {
                                                                                            if (err)
                                                                                                next();
                                                                                            if (result !== null) {
                                                                                                let tp = JSON.parse(JSON.stringify(result));
                                                                                                expenseObj["plannedBlock"] = tp.approvedArea;
                                                                                            }
                                                                                            expenseObj["totalExpense"] = expenseObj.da + expenseObj.fare + expenseObj.miscExpense;
                                                                                            expenseObj["daMgr"] = expenseObj["da"];
                                                                                            expenseObj["daAdmin"] = expenseObj["da"];

                                                                                            expenseObj["distanceMgr"] = expenseObj["distance"];
                                                                                            expenseObj["distanceAdmin"] = expenseObj["distance"];

                                                                                            expenseObj["fareMgr"] = expenseObj["fare"];
                                                                                            expenseObj["fareAdmin"] = expenseObj["fare"];

                                                                                            expenseObj["miscExpenseMgr"] = expenseObj["miscExpense"];
                                                                                            expenseObj["miscExpenseAdmin"] = expenseObj["miscExpense"];

                                                                                            expenseObj["totalExpenseMgr"] = expenseObj["totalExpense"];
                                                                                            expenseObj["totalExpenseAdmin"] = expenseObj["totalExpense"];

                                                                                            expenseObj["remarksMgr"] = "";
                                                                                            expenseObj["remarksAdmin"] = "";

                                                                                            Dcrmaster.app.models.ExpenseClaimed.find({
                                                                                                where: {
                                                                                                    companyId: dcrObj.companyId,
                                                                                                    dcrId: dcrObj.dcrId,
                                                                                                    dcrDate: dcrObj.dcrDate,
                                                                                                    userId: dcrObj.submitBy
                                                                                                }
                                                                                            }, function (err, resCheckExist) {
                                                                                                if (err) {
                                                                                                    console.log("Error")
                                                                                                    next();
                                                                                                }

                                                                                                if (resCheckExist.length >= 1) {
                                                                                                    //  next();
                                                                                                } else {
                                                                                                    Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                                                                        console.log("Line 415 : Successfully inserted..");
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        })

                                                                                        //----Update Previous Expense
                                                                                        Dcrmaster.app.models.ExpenseClaimed.updateDetails({
                                                                                            companyId: ObjectId(dcrObj.companyId),
                                                                                            userId: ObjectId(dcrObj.submitBy),
                                                                                            dcrDate: previousExpObject[0].dcrDate
                                                                                        }, {
                                                                                            da: da.daOUT,
                                                                                            fare: (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0),
                                                                                            "totalExpense": (da.daOUT + (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0)),
                                                                                            "type": "OUT",
                                                                                            "daMgr": da.daOUT,
                                                                                            "daAdmin": da.daOUT,
                                                                                            "distanceMgr": previousExpObject[0].distance,
                                                                                            "distanceAdmin": previousExpObject[0].distance,
                                                                                            "fareMgr": (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0),
                                                                                            "fareAdmin": (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0),
                                                                                            "totalExpenseMgr": (da.daOUT + (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0)),
                                                                                            "totalExpenseAdmin": (da.daOUT + (previousExpObject[0].isOutSide == true ? (2 * previousExpObject[0].fare) : 0)),
                                                                                        }, function (err, updateprevious) {
                                                                                            if (err)
                                                                                                next()
                                                                                            console.log("Line  : 441", updateprevious);

                                                                                        });

                                                                                        Dcrmaster.app.models.ExpenseClaimed.updateDetails({
                                                                                            companyId: ObjectId(dcrObj.companyId),
                                                                                            userId: ObjectId(dcrObj.submitBy),
                                                                                            dcrDate: previousExpObject[1].dcrDate
                                                                                        }, {
                                                                                            da: da.daOUT,
                                                                                            fare: previousExpObject[1].fare,
                                                                                            "totalExpense": (da.daOUT + previousExpObject[1].fare),
                                                                                            "type": "OUT",
                                                                                            "daMgr": da.daOUT,
                                                                                            "daAdmin": da.daOUT,
                                                                                            "distanceMgr": previousExpObject[1].distance,
                                                                                            "distanceAdmin": previousExpObject[1].distance,
                                                                                            "fareMgr": (2 * previousExpObject[1].fare),
                                                                                            "fareAdmin": (2 * previousExpObject[1].fare),
                                                                                            "totalExpenseMgr": (da.daOUT + previousExpObject[1].fare),
                                                                                            "totalExpenseAdmin": (da.daOUT + previousExpObject[1].fare),
                                                                                        }, function (err, updatebrforePrevious) {
                                                                                            if (err)
                                                                                                next()
                                                                                            console.log("Line  : 465", updatebrforePrevious);

                                                                                        });

                                                                                    })
                                                                                } else {
                                                                                    //----Yesterday DCR is not OUT dcr
                                                                                    //New Insertion
                                                                                    Dcrmaster.app.models.TourProgram.findOne({
                                                                                        where: {
                                                                                            companyId: dcrObj.companyId,
                                                                                            createdBy: dcrObj.submitBy,
                                                                                            date: dcrObj.dcrDate
                                                                                        }
                                                                                    }, function (err, result) {
                                                                                        if (err)
                                                                                            next();
                                                                                        if (result !== null) {
                                                                                            let tp = JSON.parse(JSON.stringify(result));
                                                                                            expenseObj["plannedBlock"] = tp.approvedArea;
                                                                                        }
                                                                                        expenseObj["totalExpense"] = expenseObj.da + expenseObj.fare + expenseObj.miscExpense;
                                                                                        expenseObj["daMgr"] = expenseObj["da"];
                                                                                        expenseObj["daAdmin"] = expenseObj["da"];

                                                                                        expenseObj["distanceMgr"] = expenseObj["distance"];
                                                                                        expenseObj["distanceAdmin"] = expenseObj["distance"];

                                                                                        expenseObj["fareMgr"] = expenseObj["fare"];
                                                                                        expenseObj["fareAdmin"] = expenseObj["fare"];

                                                                                        expenseObj["miscExpenseMgr"] = expenseObj["miscExpense"];
                                                                                        expenseObj["miscExpenseAdmin"] = expenseObj["miscExpense"];

                                                                                        expenseObj["totalExpenseMgr"] = expenseObj["totalExpense"];
                                                                                        expenseObj["totalExpenseAdmin"] = expenseObj["totalExpense"];

                                                                                        expenseObj["remarksMgr"] = "";
                                                                                        expenseObj["remarksAdmin"] = "";

                                                                                        Dcrmaster.app.models.ExpenseClaimed.find({
                                                                                            where: {
                                                                                                companyId: dcrObj.companyId,
                                                                                                dcrId: dcrObj.dcrId,
                                                                                                dcrDate: dcrObj.dcrDate,
                                                                                                userId: dcrObj.submitBy
                                                                                            }
                                                                                        }, function (err, resCheckExist) {
                                                                                            if (err) {
                                                                                                console.log("Error")
                                                                                                next();
                                                                                            }

                                                                                            if (resCheckExist.length >= 1) {
                                                                                                //  next();
                                                                                            } else {
                                                                                                Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                                                                    console.log("Successfully inserted..");
                                                                                                })
                                                                                            }
                                                                                        })
                                                                                    })

                                                                                    //----Update Previous Expense
                                                                                    Dcrmaster.app.models.Expenseclaimed.updateDetails({
                                                                                        companyId: ObjectId(dcrObj.companyId),
                                                                                        userId: ObjectId(dcrObj.submitBy),
                                                                                        dcrDate: previousExpObject[0].dcrDate
                                                                                    }, {
                                                                                        da: da.daOUT,
                                                                                        fare: previousExpObject[0].fare,
                                                                                        "totalExpense": (da.daEX + previousExpObject[0].fare),
                                                                                        "type": "Ex",
                                                                                        "daMgr": da.daOUT,
                                                                                        "daAdmin": da.daOUT,
                                                                                        "distanceMgr": previousExpObject[0].distance,
                                                                                        "distanceAdmin": previousExpObject[0].distance,
                                                                                        "fareMgr": previousExpObject[0].fare,
                                                                                        "fareAdmin": previousExpObject[0].fare,
                                                                                        "totalExpenseMgr": (da.daOUT + previousExpObject[0].fare),
                                                                                        "totalExpenseAdmin": (da.daOUT + previousExpObject[0].fare),
                                                                                    }, function (err, updateprevious) {
                                                                                        if (err)
                                                                                            next()
                                                                                    });

                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    Dcrmaster.app.models.TourProgram.findOne({
                                                                        where: {
                                                                            companyId: dcrObj.companyId,
                                                                            createdBy: dcrObj.submitBy,
                                                                            date: dcrObj.dcrDate
                                                                        }
                                                                    }, function (err, result) {
                                                                        if (err)
                                                                            next();
                                                                        if (result !== null) {
                                                                            let tp = JSON.parse(JSON.stringify(result));
                                                                            expenseObj["plannedBlock"] = tp.approvedArea;
                                                                        }
                                                                        console.log("Total Expense : ", expenseObj.da + " " + expenseObj.fare + " " + expenseObj.miscExpense);
                                                                        expenseObj["totalExpense"] = expenseObj.da + expenseObj.fare;
                                                                        expenseObj["daMgr"] = expenseObj["da"];
                                                                        expenseObj["daAdmin"] = expenseObj["da"];

                                                                        expenseObj["distanceMgr"] = expenseObj["distance"];
                                                                        expenseObj["distanceAdmin"] = expenseObj["distance"];

                                                                        expenseObj["fareMgr"] = expenseObj["fare"];
                                                                        expenseObj["fareAdmin"] = expenseObj["fare"];

                                                                        expenseObj["miscExpenseMgr"] = expenseObj["miscExpense"];
                                                                        expenseObj["miscExpenseAdmin"] = expenseObj["miscExpense"];

                                                                        expenseObj["totalExpenseMgr"] = expenseObj["totalExpense"];
                                                                        expenseObj["totalExpenseAdmin"] = expenseObj["totalExpense"];

                                                                        expenseObj["remarksMgr"] = "";
                                                                        expenseObj["remarksAdmin"] = "";

                                                                        Dcrmaster.app.models.ExpenseClaimed.find({
                                                                            where: {
                                                                                companyId: dcrObj.companyId,
                                                                                dcrId: dcrObj.dcrId,
                                                                                dcrDate: dcrObj.dcrDate,
                                                                                userId: dcrObj.submitBy
                                                                            }
                                                                        }, function (err, resCheckExist) {
                                                                            if (err) {
                                                                                console.log("Error")
                                                                                next();
                                                                            }

                                                                            if (resCheckExist.length >= 1) {
                                                                                //  next();
                                                                            } else {
                                                                                Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                                                    console.log("Line 603 : Successfully inserted..");
                                                                                })
                                                                            }
                                                                        })
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    })
                                                } else {
                                                    Dcrmaster.app.models.TourProgram.findOne({
                                                        where: {
                                                            companyId: dcrObj.companyId,
                                                            createdBy: dcrObj.submitBy,
                                                            date: dcrObj.dcrDate
                                                        }
                                                    }, function (err, result) {
                                                        if (err)
                                                            next();
                                                        if (result !== null) {
                                                            let tp = JSON.parse(JSON.stringify(result));
                                                            expenseObj["plannedBlock"] = tp.approvedArea;
                                                        }
                                                        console.log("Total Expense : ", expenseObj.da + " " + expenseObj.fare + " " + expenseObj.miscExpense);
                                                        expenseObj["totalExpense"] = expenseObj.da + expenseObj.fare + expenseObj.miscExpense;
                                                        expenseObj["daMgr"] = expenseObj["da"];
                                                        expenseObj["daAdmin"] = expenseObj["da"];

                                                        expenseObj["distanceMgr"] = expenseObj["distance"];
                                                        expenseObj["distanceAdmin"] = expenseObj["distance"];

                                                        expenseObj["fareMgr"] = expenseObj["fare"];
                                                        expenseObj["fareAdmin"] = expenseObj["fare"];

                                                        expenseObj["miscExpenseMgr"] = expenseObj["miscExpense"];
                                                        expenseObj["miscExpenseAdmin"] = expenseObj["miscExpense"];

                                                        expenseObj["totalExpenseMgr"] = expenseObj["totalExpense"];
                                                        expenseObj["totalExpenseAdmin"] = expenseObj["totalExpense"];

                                                        expenseObj["remarksMgr"] = "";
                                                        expenseObj["remarksAdmin"] = "";

                                                        Dcrmaster.app.models.ExpenseClaimed.find({
                                                            where: {
                                                                companyId: dcrObj.companyId,
                                                                dcrId: dcrObj.dcrId,
                                                                dcrDate: dcrObj.dcrDate,
                                                                userId: dcrObj.submitBy
                                                            }
                                                        }, function (err, resCheckExist) {
                                                            if (err) {
                                                                console.log("Error")
                                                                next();
                                                            }

                                                            if (resCheckExist.length >= 1) {
                                                                //  next();
                                                            } else {
                                                                Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                                    console.log("Line 662 : Successfully inserted..");
                                                                })
                                                            }
                                                        })
                                                    })
                                                }
                                            });
                                        } else {
                                            //if day status or working Status is not working then
                                            Dcrmaster.app.models.ExpenseClaimed.find({
                                                where: {
                                                    companyId: dcrObj.companyId,
                                                    dcrId: dcrObj.dcrId,
                                                    dcrDate: dcrObj.dcrDate,
                                                    userId: dcrObj.submitBy
                                                }
                                            }, function (err, resCheckExist) {
                                                if (err) {
                                                    console.log("Error")
                                                    next();
                                                }

                                                if (resCheckExist.length >= 1) {
                                                    //next();
                                                } else {
                                                    Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                        if (err)
                                                            next();
                                                        console.log("Line 691 : Successfully inserted..");

                                                    })
                                                }
                                            })
                                        }
                                        next();
                                    }
                                })
                            break;

                        case "category wise":
                            //getting Category of User for getting DA

                            Dcrmaster.app.models.UserInfo.findOne({
                                "where": {
                                    userId: dcrObj.submitBy,
                                    companyId: dcrObj.companyId
                                }
                            }, function (err, user) {
                                if (err) {
                                    next();
                                } else {
                                    let expenseObj = {
                                        "visitedBlock": dcrObj.visitedBlock,
                                        "dcrId": dcrObj.id,
                                        "dcrDate": dcrObj.dcrDate,
                                        "companyId": dcrObj.companyId,
                                        "userId": dcrObj.submitBy,
                                        "workingStatus": dcrObj.workingStatus,
                                        "stateId": dcrObj.stateId,
                                        "districtId": dcrObj.districtId,
                                        "remarks": dcrObj.remarks,
                                        "da": 0,
                                        "daMgr": 0,
                                        "daAdmin": 0,

                                        "distance": 0,
                                        "distanceMgr": 0,
                                        "distanceAdmin": 0,

                                        "fare": 0,
                                        "fareMgr": 0,
                                        "fareAdmin": 0,

                                        "miscExpense": 0,
                                        "miscExpenseMgr": 0,
                                        "miscExpenseAdmin": 0,

                                        "totalExpense": 0,
                                        "totalExpenseMgr": 0,
                                        "totalExpenseAdmin": 0
                                    };

                                    //getting joint work with
                                    if (dcrObj.hasOwnProperty("jointWork") === true) {
                                        if (dcrObj.jointWork.length > 0) {
                                            let jointWork = [];
                                            for (let obj of dcrObj.jointWork) {
                                                jointWork.push(obj.Name)
                                            }
                                            expenseObj["jointWork"] = jointWork;
                                        }
                                    }

                                    if (dcrObj.hasOwnProperty("dcrExpense") === true) {
                                        if (dcrObj.dcrExpense.length > 0) {
                                            let totalMisc = 0;
                                            totalMisc = dcrObj.dcrExpense.reduce((prev, curr) => {
                                                return prev + curr.amount
                                            }, totalMisc)
                                            expenseObj["miscExpense"] = totalMisc;
                                            console.log("Total Misc Expenses ", totalMisc)
                                        }
                                    }

                                    if ((dcrObj.workingStatus === 'Working' || dcrObj.workingStatus === 'Working & Half Day Leave') && dcrObj.visitedBlock.length > 0) {
                                        let from = 0;
                                        let to = dcrObj.visitedBlock.length - 1;

                                        let fare = 0;
                                        let distance = 0;
                                        //let finalResult = {};
                                        asyncLoop(dcrObj.visitedBlock, function (item, next) {
                                            //check if stp is exist of not
                                            Dcrmaster.app.models.STP.findOne({
                                                where: {
                                                    companyId: dcrObj.companyId,
                                                    userId: dcrObj.submitBy,
                                                    appStatus: 'approved',
                                                    status: true,
                                                    or: [{
                                                        fromArea: item.fromId,
                                                        toArea: item.toId
                                                    }, {
                                                        fromArea: item.toId,
                                                        toArea: item.fromId
                                                    }]
                                                },
                                                include: [{
                                                    "relation": "frc",
                                                    "scope": {
                                                        rate: true
                                                    }
                                                }]
                                            }, function (err, stpResult) {
                                                //if stp exists
                                                if (stpResult !== null) {
                                                    let stp = JSON.parse(JSON.stringify(stpResult));
                                                    //here i m calculating da for the first stp pair only
                                                    if (from === 0) {
                                                        //getting DA from the DailyAllowance based on daType & Category & Division Id & statewise based on stp type
                                                        let daWhere = {
                                                            where: {
                                                                companyId: dcrObj.companyId,
                                                                type: res.expense.daType,
                                                                category: user.daCategory
                                                            }
                                                        }
                                                        if (isDivisionExist === true) {
                                                            daWhere.where["divisionId"] = user.divisionId[0];
                                                        }

                                                        Dcrmaster.app.models.DailyAllowance.findOne(daWhere, function (err, da) {
                                                            if (err)
                                                                next()
                                                            if (da !== null) {
                                                                if (stp.type === 'HQ') {
                                                                    expenseObj["da"] = da.daHQ;
                                                                } else if (stp.type === 'EX') {
                                                                    expenseObj["da"] = da.daEX;
                                                                } else if (stp.type === 'OUT') {
                                                                    expenseObj["da"] = da.daOUT;
                                                                } else {
                                                                    expenseObj["da"] = 0;
                                                                }
                                                                expenseObj["type"] = stp.type;
                                                            } else {
                                                                expenseObj["da"] = 0;
                                                                expenseObj["type"] = stp.type;
                                                            }
                                                        })
                                                    }

                                                    //travelling allowance for all stp mapping a-->b--->c then sum of the distance a-->b then sum of distance b-c
                                                    distance = distance + stp.distance;
                                                    if (stp.frc.allounceToBeGet === 'KM Wise') {
                                                        fare = fare + (stp.distance * stp.frc.rate)
                                                    } else if (stp.frc.allounceToBeGet === 'Lumsum') {
                                                        fare = fare + stp.frc.rate
                                                    }
                                                    expenseObj["distance"] = distance;
                                                    expenseObj["fare"] = fare;

                                                } else {

                                                    //Please check if STP is not exists

                                                    if (from === 0) {
                                                        expenseObj["fare"] = 0;
                                                        expenseObj["distance"] = 0;
                                                        //getting DA from DailyAllouwance
                                                        let daWhere = {
                                                            daType: "category wise",
                                                            companyId: dcrObj.companyId,
                                                            areaId: item.toId,
                                                            category: user.daCategory
                                                        }

                                                        if (isDivisionExist === true) {
                                                            daWhere["divisionId"] = user.divisionId[0];
                                                            daWhere["isDivisionExist"] = isDivisionExist;
                                                        }

                                                        Dcrmaster.app.models.Area.getAreaDailyAllowanceForCategory(daWhere, function (err, da) {
                                                            if (err)
                                                                next()
                                                            console.log("DA...", da)

                                                            if (da.length === 0 || da == undefined || da == null) {
                                                                expenseObj.da = 0;
                                                                expenseObj.type = "";
                                                            } else {

                                                                console.log("Da Setting  " + da[0].da);
                                                                expenseObj["da"] = da[0].da;
                                                                expenseObj["type"] = da[0].type.toUpperCase();
                                                            }
                                                        })
                                                    }
                                                }
                                                from = from + 1;
                                                next();
                                            })

                                        }, function (err) {
                                            Dcrmaster.app.models.TourProgram.findOne({
                                                where: {
                                                    companyId: dcrObj.companyId,
                                                    createdBy: dcrObj.submitBy,
                                                    date: dcrObj.dcrDate
                                                }
                                            }, function (err, result) {
                                                if (err)
                                                    next();
                                                if (result !== null) {
                                                    let tp = JSON.parse(JSON.stringify(result));
                                                    expenseObj["plannedBlock"] = tp.approvedArea;
                                                }
                                                expenseObj["distanceMgr"] = expenseObj["distance"];
                                                expenseObj["distanceAdmin"] = expenseObj["distance"];

                                                expenseObj["fareMgr"] = expenseObj["fare"];
                                                expenseObj["fareAdmin"] = expenseObj["fare"];

                                                expenseObj["miscExpenseMgr"] = expenseObj["miscExpense"];
                                                expenseObj["miscExpenseAdmin"] = expenseObj["miscExpense"];

                                                expenseObj["totalExpenseMgr"] = expenseObj["totalExpense"];
                                                expenseObj["totalExpenseAdmin"] = expenseObj["totalExpense"];

                                                expenseObj["remarksMgr"] = "";
                                                expenseObj["remarksAdmin"] = "";

                                                Dcrmaster.app.models.ExpenseClaimed.find({
                                                    where: {
                                                        companyId: dcrObj.companyId,
                                                        dcrId: dcrObj.dcrId,
                                                        dcrDate: dcrObj.dcrDate,
                                                        userId: dcrObj.submitBy
                                                    }
                                                }, function (err, resCheckExist) {
                                                    if (err) {
                                                        console.log("Error")
                                                        next();
                                                    }
                                                    if (resCheckExist.length >= 1) {
                                                        //  next();
                                                    } else {
                                                        expenseObj["totalExpense"] = expenseObj["da"] + expenseObj["fare"] + expenseObj["miscExpense"];
                                                        expenseObj["totalExpenseMgr"] = expenseObj["da"] + expenseObj["fare"]  + expenseObj["miscExpense"];
                                                        expenseObj["totalExpenseAdmin"] = expenseObj["da"] + expenseObj["fare"]  + expenseObj["miscExpense"];

                                                        expenseObj["daMgr"] = expenseObj["da"];
                                                        expenseObj["daAdmin"] = expenseObj["da"];
                                                        console.log("-----------------------------");
                                                        console.log(expenseObj)
                                                        console.log("-----------------------------");
                                                        Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                            console.log("943: Successfully inserted..");
                                                        })
                                                    }
                                                })
                                            })

                                        })
                                    } else {
                                         //if day status or working Status is not working then
                                         Dcrmaster.app.models.ExpenseClaimed.find({
                                            where: {
                                                companyId: dcrObj.companyId,
                                                dcrId: dcrObj.dcrId,
                                                dcrDate: dcrObj.dcrDate,
                                                userId: dcrObj.submitBy
                                            }
                                        }, function (err, resCheckExist) {
                                            if (err) {
                                                console.log("Error")
                                                next();
                                            }

                                            if (resCheckExist.length >= 1) {
                                                //next();
                                            } else {
                                                Dcrmaster.app.models.ExpenseClaimed.create(expenseObj, function (err, expenseRes) {
                                                    if (err)
                                                        next();
                                                    console.log("Line 691 : Successfully inserted..");

                                                })
                                            }
                                        })
                                    }
                                    next()
                                }
                            })
                            break;
                        default:
                            next()
                    }
                } else {
                    //either company id is not exist or company not using expense modue
                    next();
                }
            })
        }


    });
	/*
	Dcrmaster.updateAllRecord = function (records, cb) {
        var self = this;
        var response = [];
        var ids = [];
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var ExpenseClaimedCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.ExpenseClaimed.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        let returnData = records;
        for (let w = 0; w < records.length; w++) {
            let set = {};
            set = records[w];
            // changes done by praveen singh to udpate the id into ObjectId
            set["companyId"] = ObjectId(set["companyId"]);
            set["districtId"] = ObjectId(set["districtId"]);
            set["stateId"] = ObjectId(set["stateId"]);
            set["submitBy"] = ObjectId(set["submitBy"]);
            set["dcrDate"] = new Date(set["dcrDate"]);
            set["dcrSubmissionDate"] = new Date(set["dcrSubmissionDate"]);

            let totalMisc = 0;
            let miscObj = {};
            if (set.hasOwnProperty("dcrExpense")) {
                for (let i = 0; i < set["dcrExpense"].length; i++) {
                    if (set["dcrExpense"][i].hasOwnProperty("fileId")) {
                        set["dcrExpense"][i]["fileId"] = ObjectId(set["dcrExpense"][i]["fileId"]);
                        totalMisc += set["dcrExpense"][i]["amount"]
                    } else {
                        totalMisc += set["dcrExpense"][i]["amount"]
                    }
                }
                miscObj = set["dcrExpense"].reduce((prev, cur) => {
                    if (prev.hasOwnProperty(cur.expenseType)) {
                        prev[cur.expenseType] += cur.amount;
                    } else {
                        prev[cur.expenseType] = cur.amount;
                    }
                    return prev;
                }, miscObj);

                // getting the expenses head from the expenses collection
                Dcrmaster.app.models.Expenses.findOne({
                    where: {
                        companyId: set["companyId"]
                    }
                }, (err, expensesRes) => {
                    if (err) {
                        console.log("Error in Expenses Collections", err);
                    }
                    expensesRes.expenses.forEach(element => {
                        if (miscObj.hasOwnProperty(element) === false) {
                            miscObj[element] = 0;
                        }
                    });
                })
            }

            //getting the expense from the Expense Claimed & Update the Details of the Expense
            setTimeout(() => {
                Dcrmaster.app.models.ExpenseClaimed.findOne({
                    where: {
                        dcrId: set["id"],
                        companyId: set["companyId"],
                        userId: set["submitBy"],
                        dcrDate: set["dcrDate"]
                    }
                }, (err, res) => {
                    if (err) {
                        console.log(err);
                        console.log("Error in Expense Update of UpdateAll method in DCRMaster");
                    }
                    let updatedObj = {
                        "miscExpense": totalMisc,
                        "miscExpenseMgr": totalMisc,
                        "miscExpenseAdmin": totalMisc,
                        "totalExpense": res.da + res.fare + totalMisc,
                        "totalExpenseMgr": res.da + res.fare + totalMisc,
                        "totalExpenseAdmin": res.da + res.fare + totalMisc,
                        "miscExpSelf": miscObj,
                        "miscExpMGR": miscObj,
                        "miscExpADM": miscObj
                    }
                    ExpenseClaimedCollection.update({
                        dcrId: ObjectId(set["id"]),
                        companyId: ObjectId(set["companyId"]),
                        userId: ObjectId(set["submitBy"]),
                        dcrDate: set["dcrDate"]
                    }, { $set: updatedObj }, (err, result) => {
                            if (err) {
                                console.log("error while updating the expense claimed in DCR master")
                            }
                            //console.log("Data updated Successfully...")
                    })

                })
            }, 2000);

            DcrmasterCollection.update({ "_id": ObjectId(records[w].id) }, { $set: set },
                function (err, result) {
                    if (err) {
                        console.log("err");
                        console.log(err);
                    }
                })
        }
        return cb(false, returnData);

    };
    // End DCR Record
    // Update All Data for sync

    Dcrmaster.remoteMethod(
        'updateAllRecord', {
        description: 'update and send response all data',
        accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );
	// Consolidate DCR Details
	*/
	//--------------------------------------------------
	Dcrmaster.updateAllRecord = function (records, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var ExpenseClaimedCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.ExpenseClaimed.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        let returnData = records;
        for (let w = 0; w < records.length; w++) {
            let set = {};
            set = records[w];
            // changes done by praveen singh to udpate the id into ObjectId
            set["companyId"] = ObjectId(set["companyId"]);
            set["districtId"] = ObjectId(set["districtId"]);
            set["stateId"] = ObjectId(set["stateId"]);
            set["submitBy"] = ObjectId(set["submitBy"]);
            set["dcrDate"] = new Date(set["dcrDate"]);
            set["dcrSubmissionDate"] = new Date(set["dcrSubmissionDate"]);

            let totalMisc = 0;
            let miscObj = {};
            if (set.hasOwnProperty("dcrExpense")) {
                for (let i = 0; i < set["dcrExpense"].length; i++) {
                    if (set["dcrExpense"][i].hasOwnProperty("fileId")) {
                        set["dcrExpense"][i]["fileId"] = ObjectId(set["dcrExpense"][i]["fileId"]);
                        totalMisc += set["dcrExpense"][i]["amount"]
                    } else {
                        totalMisc += set["dcrExpense"][i]["amount"]
                    }
                }
                miscObj = set["dcrExpense"].reduce((prev, cur) => {
                    let key = cur.expenseType.replace(/\s/g, "");
                    if (prev.hasOwnProperty(key)) {
                        prev[key] += cur.amount;
                    } else {
                        prev[key] = cur.amount;
                    }
                    return prev;
                }, miscObj);

                // getting the expenses head from the expenses collection
                Dcrmaster.app.models.Expenses.findOne({
                    where: {
                        companyId: set["companyId"]
                    }
                }, (err, expensesRes) => {
                    if (err) {
                        console.log("Error in Expenses Collections", err);
                    }
                    expensesRes.expenses.forEach(element => {
                        let key = element.replace(/\s/g, "");
                        if (miscObj.hasOwnProperty(key) === false) {
                            miscObj[element] = 0;
                        }
                    });
                })
            }

            //getting the expense from the Expense Claimed & Update the Details of the Expense
            setTimeout(() => {
                Dcrmaster.app.models.ExpenseClaimed.findOne({
                    where: {
                        dcrId: set["id"],
                        companyId: set["companyId"],
                        userId: set["submitBy"],
                        dcrDate: set["dcrDate"]
                    }
                }, (err, res) => {
                    if (err) {
                        console.log(err);
                        console.log("Error in Expense Update of UpdateAll method in DCRMaster");
                    }
                    let updatedObj = {
                        "miscExpense": totalMisc,
                        "miscExpenseMgr": totalMisc,
                        "miscExpenseAdmin": totalMisc,
                        "totalExpense": res.da + res.fare + totalMisc,
                        "totalExpenseMgr": res.da + res.fare + totalMisc,
                        "totalExpenseAdmin": res.da + res.fare + totalMisc,
                        "miscExpSelf": miscObj,
                        "miscExpMGR": miscObj,
                        "miscExpADM": miscObj
                    }
                    ExpenseClaimedCollection.update({
                        dcrId: ObjectId(set["id"]),
                        companyId: ObjectId(set["companyId"]),
                        userId: ObjectId(set["submitBy"]),
                        dcrDate: set["dcrDate"]
                    }, { $set: updatedObj }, (err, result) => {
                            if (err) {
                                console.log("error while updating the expense claimed in DCR master")
                            }
                            //console.log("Data updated Successfully...")
                    })

                })
            }, 2000);

            DcrmasterCollection.update({ "_id": ObjectId(records[w].id) }, { $set: set },
                function (err, result) {
                    if (err) {
                        console.log("err");
                        console.log(err);
                    }
                })
        }
        return cb(false, returnData);

    };
	
	 Dcrmaster.remoteMethod(
        'updateAllRecord', {
        description: 'update and send response all data',
        accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'post'
        }
    }
    );
    // Consolidate DCR Details---------------------------------------->>>>>>.

    Dcrmaster.getUserWiseDCRSummary = function(params, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var aMatch = {};
        if (params.type == "State" || params.type == "Headquarter") {
            //params.companyId,
            //params.type,
            //params.stateIds,
            //params.districtIds
            Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
                function(err, userRecords) {

                    if (userRecords.length > 0) {
                        dcrCollection.aggregate(
                            // Stage 1
                            {
                                $match: {
                                    submitBy: {
                                        $in: userRecords[0].userIds
                                    },
                                    dcrDate: {
                                        $gte: new Date(params.fromDate),
                                        $lte: new Date(params.toDate)

                                    }
                                }
                            },
                            // Stage 2
                            {
                                $lookup: {
                                    "from": "DCRProviderVisitDetails",
                                    "localField": "_id",
                                    "foreignField": "dcrId",
                                    "as": "dcrDetail"
                                }
                            }, {
                                $unwind: {
                                    path: "$dcrDetail",
                                    preserveNullAndEmptyArrays: true
                                }
                            }, {
                                $group: {
                                    _id: {
                                        "submitBy": "$submitBy"
                                    },
                                    totalDCR: {
                                        $addToSet: "$dcrDate"
                                    },
									 lastDCRDate :{
                                        $last : "$dcrDate"
                                        },
                                    RMPCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "RMP"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    DrugStoreCount: {
                                        $sum: {
                                            $cond: [{
                                                $or: [{
                                                        $eq: ["$dcrDetail.providerType", "Drug"]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
									 stockistCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "Stockist"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
									 purchaseManagerCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "purchaseManager"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },hospitalManagementCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "hospitalManagement"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    doctorPOB: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "RMP"]
                                                }]
                                            }, "$dcrDetail.productPob", 0]
                                        }
                                    },
                                    vendorPOB: {
                                        $sum: {
                                            $cond: [{
                                                $or: [{
                                                        $eq: ["$dcrDetail.providerType", "Drug"]
                                                    },
                                                    {
                                                        $eq: ["$dcrDetail.providerType", "Stockist"]
                                                    }
                                                ]
                                            }, "$dcrDetail.productPob", 0]
                                        }
                                    }
                                }
                            },

                            // Stage 6
                            {
                                $lookup: {
                                    "from": "UserInfo",
                                    "localField": "_id.submitBy",
                                    "foreignField": "userId",
                                    "as": "UserDetail"
                                }
                            }, {
                                $unwind: {
                                    path: "$UserDetail",
                                    preserveNullAndEmptyArrays: true
                                }
                            }, {
                                $unwind: {
                                    path: "$UserDetail.divisionId",
                                    preserveNullAndEmptyArrays: true
                                }
                            }, {
                                $lookup: {
                                    "from": "DivisionMaster",
                                    "localField": "UserDetail.divisionId",
                                    "foreignField": "_id",
                                    "as": "division"
                                }
                            },
							  {
                                    $lookup: {
                                        "from": "UserLogin",
                                        "localField": "UserDetail.userId",
                                        "foreignField": "_id",
                                        "as": "userLogin"
                                    }
                                },
    
                                // Stage 8
                                {
                                    $unwind: {
                                        path: "$userLogin",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "UserDetail.districtId",
                                        "foreignField": "_id",
                                        "as": "districtDetail"
                                    }
                                },
        
                                // Stage 9
                                {
                                    $unwind: {
                                        path: "$districtDetail",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                            // Stage 7
                            {
                                $project: {
                                    _id: 0,
                                    userId: "$_id.submitBy",
                                    totalDCR: {
                                        $size: "$totalDCR"
                                    },
                                    divisionName: {
                                        $arrayElemAt: ["$division.divisionName", 0]
                                    },
                                    divisionId: {
                                        $arrayElemAt: ["$division._id", 0]
                                    },
									                                    lastDCRDate:"$lastDCRDate",

                                    UserName: "$UserDetail.name",
                                    UserDesignation: "$UserDetail.designation",
                                    RMPCount: 1,
                                    DrugStoreCount: 1,
								    stockistCount: 1,
                                    purchaseManagerCount: 1,
                                    hospitalManagementCount: 1,

                                    /*RMPCount: {
                                        $size: "$RMPCount"
                                    },
                                    DrugStoreCount: {
                                        $size: "$DrugStoreCount"
                                    },*/
                                    doctorPOB: 1,
                                    vendorPOB: 1,
									mobile:"$userLogin.mobile",
                                    dateOfJoining:"$userLogin.dateOfJoining",
                                    headquarter:"$districtDetail.districtName",
                                    employeeCode:"$userLogin.username",

                                }
                            }, {
                                $sort: {
                                    UserName: 1
                                }
                            }, {
                                allowDiskUse: true
                            },
                            function(err, result) {

                                if (err) {
                                    console.log(err);
                                    return cb(err);
                                }
                                if (!result) {
                                    var err = new Error('no records found');
                                    err.statusCode = 404;
                                    err.code = 'NOT FOUND';
                                    return cb(err);
                                }
                                return cb(false, result)
                            });
                    } else {
                        cb(false, []);
                    }
                });
        } else {
            var userIds = [];

            for (var i = 0; i < params.userIds.length; i++) {
                userIds.push(ObjectId(params.userIds[i]));
            }
            aMatch = {
                submitBy: {
                    $in: userIds
                },
                dcrDate: {
                    //$gte: new Date(moment.utc(params.fromDate).format()),
                    //$lte: new Date(moment.utc(params.toDate).add(1, 'days').format())
                    $gte: new Date(params.fromDate),
                    $lte: new Date(params.toDate)
                }
            }
            dcrCollection.aggregate(
                // Stage 1
                {
                    $match: aMatch
                },
                // Stage 2
                // Stage 2
                {
                    $lookup: {
                        "from": "DCRProviderVisitDetails",
                        "localField": "_id",
                        "foreignField": "dcrId",
                        "as": "dcrDetail"
                    }
                }, {
                    $unwind: {
                        path: "$dcrDetail",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $group: {
                        _id: {
                            "submitBy": "$submitBy"
                        },
                        totalDCR: {
                            $addToSet: "$dcrDate"
                        },
                        RMPCount: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$dcrDetail.providerType", "RMP"]
                                    }]
                                }, 1, 0]
                            }
                        },
                       DrugStoreCount: {
                                        $sum: {
                                            $cond: [{
                                                $or: [{
                                                        $eq: ["$dcrDetail.providerType", "Drug"]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
									 stockistCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "Stockist"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
									 purchaseManagerCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "purchaseManager"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },hospitalManagementCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "hospitalManagement"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    doctorPOB: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$dcrDetail.providerType", "RMP"]
                                                }]
                                            }, "$dcrDetail.productPob", 0]
                                        }
                                    },
						 lastDCRDate :{
                            $last : "$dcrDate"
                            },
                        doctorPOB: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$dcrDetail.providerType", "RMP"]
                                    }]
                                }, "$dcrDetail.productPob", 0]
                            }
                        },
                        vendorPOB: {
                            $sum: {
                                $cond: [{
                                    $or: [{
                                            $eq: ["$dcrDetail.providerType", "Drug"]
                                        },
                                        {
                                            $eq: ["$dcrDetail.providerType", "Stockist"]
                                        }
                                    ]
                                }, "$dcrDetail.productPob", 0]
                            }
                        }
                    }
                },

                // Stage 6
                {
                    $lookup: {
                        "from": "UserInfo",
                        "localField": "_id.submitBy",
                        "foreignField": "userId",
                        "as": "UserDetail"
                    }
                }, {
                    $unwind: {
                        path: "$UserDetail",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $unwind: {
                        path: "$UserDetail.divisionId",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $lookup: {
                        "from": "DivisionMaster",
                        "localField": "UserDetail.divisionId",
                        "foreignField": "_id",
                        "as": "division"
                    }
                },
				 {
                                    $lookup: {
                                        "from": "UserLogin",
                                        "localField": "UserDetail.userId",
                                        "foreignField": "_id",
                                        "as": "userLogin"
                                    }
                                },
    
                                // Stage 8
                                {
                                    $unwind: {
                                        path: "$userLogin",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "UserDetail.districtId",
                                        "foreignField": "_id",
                                        "as": "districtDetail"
                                    }
                                },
        
                                // Stage 9
                                {
                                    $unwind: {
                                        path: "$districtDetail",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                // Stage 7
                {
                    $project: {
                        _id: 0,
                        userId: "$_id.submitBy",
                        totalDCR: {
                            $size: "$totalDCR"
                        },
                        divisionName: {
                            $arrayElemAt: ["$division.divisionName", 0]
                        },
                        divisionId: {
                            $arrayElemAt: ["$division._id", 0]
                        },
                        UserName: "$UserDetail.name",
                        UserDesignation: "$UserDetail.designation",
                        RMPCount: 1,
			DrugStoreCount: 1,
								  
                            stockistCount: 1,
                                    purchaseManagerCount: 1,
                                    hospitalManagementCount: 1,						                                    lastDCRDate:"$lastDCRDate",

                        /*RMPCount: {
                            $size: "$RMPCount"
                        },
                        DrugStoreCount: {
                            $size: "$DrugStoreCount"
                        },*/
                        doctorPOB: 1,
                        vendorPOB: 1,
						        mobile:"$userLogin.mobile",
                                    dateOfJoining:"$userLogin.dateOfJoining",
                                    headquarter:"$districtDetail.districtName",
                                    employeeCode:"$userLogin.username",

                    }
                }, {
                    $sort: {
                        UserName: 1
                    }
                }, {
                    allowDiskUse: true
                },
                function(err, result) {
                    if (err) {
                        console.log(err);
                        return cb(err);
                    }
                    if (!result) {
                        var err = new Error('no records found');
                        err.statusCode = 404;
                        err.code = 'NOT FOUND';
                        return cb(err);
                    }
                    return cb(false, result)
                });
        }




    }
	Dcrmaster.remoteMethod(
        'getUserWiseDCRSummary', {
            description: 'consolidateDCR for selected details',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],

            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        });

    Dcrmaster.getUserDateWiseDCRDetail = function(userId, fromDate, toDate, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        dcrCollection.aggregate(
            // Stage 1
            {
                $match: {
                    submitBy: ObjectId(userId),
                    dcrDate: {
                        //$gte: new Date(moment.utc(fromDate).format()),
                        //$lte: new Date(moment.utc(toDate).add(1, 'days').format())
                        $gte: new Date(fromDate),
                        $lte: new Date(toDate)

                    }
                }
            },
            // Stage 2
            {
                $lookup: {

                    "from": "DCRProviderVisitDetails",
                    "localField": "_id",
                    "foreignField": "dcrId",
                    "as": "dcrDetail"
                }

            },
            // Stage 3
            {
                $unwind: {
                    path: "$dcrDetail",
                    preserveNullAndEmptyArrays: true
                }
            },
            // Stage 4
			{
				$lookup:{
		    "from" : "UserInfo",
			"localField" : "submitBy",
			"foreignField" : "userId",
			"as" : "userDetails"
                 }
				
			},
			{
                $unwind: {
                    path: "$userDetails",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: {
                        "dcrId": "$_id",
                        "userId": "$submitBy",
                        "dcrDate": "$dcrDate"
                    },
                    workingStatus: { $addToSet: "$workingStatus" },
                    DCRStatus: { $addToSet: "$DCRStatus" },
                      RMPCount: {
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                   //  { $ne: ["$dcrDetail.providerStatus", "unlisted"]}
                                ]
                            }, 1, 0]
                        }
                    },listedCount: {
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                    { $ne: ["$dcrDetail.providerStatus", "unlisted"]}
                                ]
                            }, 1, 0]
                        }
                    },
                    unlistedRMPcount: {
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                    {$eq:  ["$dcrDetail.providerStatus", "unlisted"]}
                                ]
                            }, 1, 0]
                        }
                    },
					doctorPOB: {
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                ]
                            }, "$dcrDetail.productPob", 0]
                        }
                    },
                    listedDoctorPOB:{
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                    { $ne: ["$dcrDetail.providerStatus", "unlisted"]}
                                ]
                            }, "$dcrDetail.productPob", 0]
                        }
                    },
					
                    unlistedDoctorPOB: {
                        $sum: {
                            $cond: [{
                                $and: [
                                    { $eq: ["$dcrDetail.providerType", "RMP"] },
                                    { $eq: ["$dcrDetail.providerStatus", "unlisted"]}
                                ]
                            }, "$dcrDetail.productPob", 0]
                        }
                    },
                    DrugStoreCount: {
                        $sum: {
                            $cond: [{
                                $or: [
                                    { $eq: ["$dcrDetail.providerType", "Drug"] },
                                ]
                            }, 1, 0]
                        }
                    },stockistCount: {
                        $sum: {
                            $cond: [{
                                $or: [
                                    { $eq: ["$dcrDetail.providerType", "Stockist"] },
                                ]
                            }, 1, 0]
                        }
                    },purchaseManagerCount: {
                        $sum: {
                            $cond: [{
                                $or: [
                                    { $eq: ["$dcrDetail.providerType", "purchaseManager"] },
                                ]
                            }, 1, 0]
                        }
                    },hopitalManagmentCount: {
                        $sum: {
                            $cond: [{
                                $or: [
                                    { $eq: ["$dcrDetail.providerType", "hospitalManagement"] },
                                ]
                            }, 1, 0]
                        }
                    },
					vendorPOB: {
                        $sum: {
                            $cond: [{
                                $or: [
                                    { $eq: ["$dcrDetail.providerType", "Drug"] },
									{ $eq: ["$dcrDetail.providerType", "Stockist"] }
                                ]
                            }, "$dcrDetail.productPob", 0]
                        }
                    }, jointCalls: {
                        $addToSet: "$dcrDetail.providerId"
                    },
                    visitedBlock: { $addToSet: "$visitedBlock" },
                    jointWork: { $addToSet: "$jointWork" },
                    deviateReason: { $addToSet: "$deviateReason" },
                    remarks: { $addToSet: "$remarks" },
				    timeIn: { $addToSet: "$timeIn" },   
					timeOut: { $addToSet: "$timeOut" },
					name: { $addToSet: "$userDetails.name" },
					punchDate: { $addToSet: "$punchDate" },   


                }
            },
            // Stage 5
            {
                $project: {
                    _id: 0,
                    dcrId: "$_id.dcrId",
                    userId: "$_id.userId",
                    dcrDate: "$_id.dcrDate",
					timeIn: { $arrayElemAt: ["$timeIn", 0] },
                    timeOut: { $arrayElemAt: ["$timeOut", 0] },
                    workingStatus: { $arrayElemAt: ["$workingStatus", 0] },
					DCRStatus: { $arrayElemAt: ["$DCRStatus", 0] },
                    RMPCount: 1,
				    listedCount: 1,
                    unlistedRMPcount: 1,
                    stockistCount:1,
					purchaseManagerCount:1,
					hopitalManagmentCount:1,
                    DrugStoreCount: 1,
                    visitedBlock: { $arrayElemAt: ["$visitedBlock", 0] },
                    jointWork: { $arrayElemAt: ["$jointWork", 0] },
                    deviateReason: { $arrayElemAt: ["$deviateReason", 0] },
                    remarks: { $arrayElemAt: ["$remarks", 0] },
					punchDate: { $arrayElemAt: ["$punchDate", 0] },

					//jointCalls: { $arrayElemAt: ["$jointCalls", 0] },
					doctorPOB : 1,
					unlistedDoctorPOB : 1,
					listedDoctorPOB : 1,

					vendorPOB : 1,
					userName:{ $arrayElemAt: ["$name", 0] },

                }
            }, {
                $sort: {
                    dcrDate: 1

                }
            },
            function(err, result) {
				console.log("result=",result)
                if (err) {
                    console.log(err);
                    return cb(err);
                }
                if (!result) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                return cb(false, result)

            });

    }

    Dcrmaster.remoteMethod('getUserDateWiseDCRDetail', {
        description: "Date wise DCR details",
        accepts: [{
                arg: 'userId',
                type: 'string'
            },
            {
                arg: 'fromDate',
                type: 'string'
            },
            {
                arg: 'toDate',
                type: 'string'
            }
        ],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }

    })

    //--------------------------Praveen Kumar 23-10-2018------------------------------
    Dcrmaster.getDCHeirarchyDCRData = function(userIdArr, cb) {
        var self = this;
        var DcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        DcrCollection.aggregate({
                $match: {
                    submitBy: { $in: userIdArr }
                }
            },
            // Stage 2
            {
                $project: {
                    id: "$_id",
                    _id: 0,
                    dcrDate: 1,
                    stateId: 1,
                    districtId: 1,
                    visitedBlock: 1,
                    workingStatus: 1,
                    jointWork: 1,
                    remarks: 1,
                    TimeIn: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    submitBy: 1,
                    //reportedFrom:1,
                    ipAddress: 1,
                    geoLocation: 1,
                    //dloBloVenue:1,
                    // isDelete:1
                }
            },
            function(err, DCHeirarchyDCR) {
                if (err) {
                    console.log(err);
                    return cb(err);
                }
                if (!DCHeirarchyDCR) {
                    var err = new Error('no records found');
                    err.statusCode = 404;
                    err.code = 'NOT FOUND';
                    return cb(err);
                }
                return cb(false, DCHeirarchyDCR);
            });

    };

    Dcrmaster.remoteMethod(
        'getDCHeirarchyDCRData', {
            description: 'getDCHeirarchyDCRData for selected details',
            accepts: [{
                arg: 'userId',
                type: 'array'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //------------------------------END----------------------------------------------------------------

    Dcrmaster.getTotalSubmmitedAndPendingDCR = function(params, cb) {
        var self = this;
        //   console.log("companyId :" + params.companyId)
        //   console.log("fromDate :" + params.fromDate)
        //   console.log("toDate :" + params.toDate)
        //   console.log("status :" + params.status)
        //   console.log("level :" + params.level)




        var DcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        if (params.level == 0) {
            Dcrmaster.getDCRStatus(
                params.companyId,
                params.fromDate,
                params.toDate,
                params.status, [],
                function(err, result) {
                    if (err) {
                        console.log(err)
                    }
                    cb(false, result);
                });

        } else if (params.level >= 2) {
            Dcrmaster.app.models.Hierarchy.find({
                where: {
                    companyId: ObjectId(params.companyId),
                    supervisorId: ObjectId(params.userId)
                }
            }, function(err, hrcyResult) {
                if (err) {
                    console.log(err);
                }
                let passingUserIds = [];
                for (let i = 0; i < hrcyResult.length; i++) {
                    passingUserIds.push(hrcyResult[i].userId);
                }

                //console.log("PKKKKK, :" + passingUserIds )
                Dcrmaster.getDCRStatus(
                    params.companyId,
                    params.fromDate,
                    params.toDate,
                    params.status,
                    passingUserIds,
                    function(err, result) {
                        if (err) {
                            console.log(err)
                        }

                        cb(false, result);
                    });
            });
        }


    };

    Dcrmaster.remoteMethod(
        'getTotalSubmmitedAndPendingDCR', {
            description: 'consolidateDCR for selected details',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'params',
                    type: 'object'
                }
            ],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    );

    //----------------------Praveen Kumar(24-10-2018)-----------------------------------
    //This method gives the total dcr and pending dcr companywise or userIdwise..........
    //This is internal method
    Dcrmaster.getDCRStatus = function(companyId, fromdate, todate, status, userIds, cb) {
        var self = this;
        //console.log("userIds, ", status)
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
            companyId,
            status,
            userIds,
            function(err, userResult) {
                //console.log(userResult)
                if (err) {
                    return err;
                }
                if (userResult.length > 0) {
                    dcrCollection.aggregate({
                        $match: {
                            companyId: ObjectId(companyId),
                            submitBy: { $in: userResult[0].userIds },
                            dcrDate: {
                                $gte: new Date(fromdate),
                                $lte: new Date(todate)
                            }
                        }
                    }, {
                        $group: {
                            _id: {
                                submitBy: "$submitBy"
                            },
                            dcrDate: {
                                $addToSet: "$dcrDate"
                            }
                        }
                    }, function(err, dcrResult) {
                        //console.log(dcrResult)
                        //Not Using project because, same method can be used to check user wise total DCR.
                        if (err) {
                            console.log(err)
                        }
                        let totalDCR = 0;
                        let totalSubmittedDCR = 0;
                        let totalPendingDCR = 0;
                        let currentDate = new Date();

                        /* for (let j = 0; j < userResult[0].obj.length; j++) {
                             let joiningDate = new Date(userResult[0].obj[j].joiningDate);
                             if (joiningDate.getMonth() == currentDate.getMonth() && joiningDate.getFullYear() == currentDate.getFullYear()) {
                                 let passingJoiningDate = moment(new Date(userResult[0].obj[j].joiningDate), 'YYYY-mm-dd');
                                 let passingCurrentDate = moment(new Date(), 'YYYY-mm-dd');
                                 totalDCR = totalDCR + days_between(new Date(passingJoiningDate), new Date(passingCurrentDate));
                             } else {
                                 totalDCR = totalDCR + parseInt(lastDay);
                             }
                             //console.log("totalDCR : " + totalDCR)
                         }
                         if (dcrResult.length > 0) {
                             for (let i = 0; i < dcrResult.length; i++) {
                                 totalSubmittedDCR = totalSubmittedDCR + dcrResult[i].dcrDate.length;
                             }
                             totalPendingDCR = totalDCR - totalSubmittedDCR;
                         } else {
                             totalPendingDCR = totalDCR;
                         }
                         let finalData = {
                             totalUser: userResult[0].userIds.length,
                             totalSubmittedDCR: totalSubmittedDCR,
                             totalPendingDCR: totalPendingDCR
                         }; */
                        //  console.log("FromDateMonth : " + new Date(fromdate).getMonth())
                        //   console.log("FromDateYear : " + new Date(fromdate).getFullYear())
                        //   console.log("currentDateMonth : " + currentDate.getMonth())

                        //   console.log("currentDateYear : " + currentDate.getFullYear())
                        for (let i = 0; i < userResult[0].obj.length; i++) {
                            let reportingDate = new Date(userResult[0].obj[i].reportingDate);
                            if (new Date(fromdate).getMonth() == currentDate.getMonth() && new Date(fromdate).getFullYear() == currentDate.getFullYear()) {
                                if (reportingDate.getMonth() == currentDate.getMonth() && reportingDate.getFullYear() == currentDate.getFullYear()) {
                                    let passingreportingDate = moment(reportingDate, 'YYYY-mm-dd');
                                    let passingCurrentDate = moment(new Date(), 'YYYY-mm-dd');
                                    totalDCR = totalDCR + days_between(new Date(passingreportingDate), new Date(passingCurrentDate));
                                } else {
                                    //new Date().getDate() - 1 // Current Date - 1
                                    totalDCR = totalDCR + new Date().getDate() - 1;
                                }
                            } else {
                                //console.log("I am in else : I am not in Current Month")
                                let lastDate = new Date(fromdate.getFullYear(), fromdate.getMonth() + 1, 0);

                                if (reportingDate.getMonth() == new Date(fromdate).getMonth() && reportingDate.getFullYear() == new Date(fromdate).getFullYear()) {
                                    let passingreportingDate = moment(reportingDate, 'YYYY-mm-dd');

                                    //console.log(lastDate)

                                    let lastDateOfMonth = moment(lastDate, 'YYYY-mm-dd');
                                    //console.log(lastDateOfMonth)
                                    totalDCR = totalDCR + days_between(new Date(passingreportingDate), new Date(lastDateOfMonth)) + 1;
                                } else {
                                    totalDCR = totalDCR + lastDate.getDate();
                                }
                            }
                        }
                        if (dcrResult.length > 0) {
                            for (let i = 0; i < dcrResult.length; i++) {
                                totalSubmittedDCR = totalSubmittedDCR + dcrResult[i].dcrDate.length;
                            }
                            totalPendingDCR = totalDCR - totalSubmittedDCR;
                        } else {
                            totalPendingDCR = totalDCR;
                        }
                        let finalData = {
                            totalUser: userResult[0].userIds.length,
                            totalSubmittedDCR: totalSubmittedDCR,
                            totalPendingDCR: totalPendingDCR
                        };
                        // console.log(finalData)
                        return cb(false, finalData);
                    })


                }
            })

    }
    Dcrmaster.remoteMethod(
        'getDCRStatus', {
            description: 'consolidateDCR for selected details',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'companyId',
                    type: 'string'
                },
                {
                    arg: 'fromdate',
                    type: 'date'
                },
                {
                    arg: 'todate',
                    type: 'date'
                },
                {
                    arg: 'status',
                    type: 'array'
                }, {
                    arg: 'userIds',
                    type: 'array'
                }
            ],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    );


    function days_between(date1, date2) {
        //console.log("date1 : " + date1);
        //console.log("date2 : " + date2);
        // The number of milliseconds in one day
        var ONE_DAY = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = Math.abs(date1_ms - date2_ms);

        // Convert back to days and return
        return Math.round(difference_ms / ONE_DAY);

    }
    //------------------------------END_-------------------------------------------------

    //-------------------------------Praveen Kumar(29-10-2018)-------------------------------------
    Dcrmaster.getBasicStatus = function(argCompanyId, argMonth, argYear, argLevel, argUserId, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        //  console.log("argCompanyId : " + argCompanyId);
        // console.log("argMonth : " + argMonth);
        // console.log("argYear : " + argYear);
        // console.log("argLevel : " + argLevel);
        // console.log("argUserId : " + argUserId);



        let nextMonth = 0;
        let nextyear = 0;
        if (argMonth == 12) {
            nextMonth = "01";

            nextyear = argYear + 1;
        } else {
            nextMonth = argMonth + 1;
            if (nextMonth < 10) {
                nextMonth = "0" + nextMonth;
            }
            nextyear = argYear;
        }

        if (argMonth < 10) {
            argMonth = "0" + argMonth;
        }

        let lastDay = new Date(argYear, argMonth, 0).getDate();
        let fromDate = argYear + "-" + argMonth + "-01";
        let toDate = argYear + "-" + argMonth + "-" + lastDay;
        async.parallel({
                SelfTPSubmittedForCurrentMonth: function(cb) {
                    if (argLevel != 0 && argLevel >= 1) {
                        //   console.log("I am MR or MGR");
                        Dcrmaster.app.models.TourProgram.find({
                            where: {
                                companyId: argCompanyId,
                                sendForApproval: true,
                                month: parseInt(argMonth),
                                year: argYear,
                                createdBy: argUserId
                            }
                        }, function(err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result.length > 0) {
                                cb(false, "Yes");
                            } else if (result.length == undefined) {
                                cb(false, "No");
                            } else {
                                cb(false, "No");
                            }

                        });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }
                },
                SelfTPApprovedForCurrentMonth: function(cb) {
                    if (argLevel != 0 && argLevel >= 1) {
                        //         console.log("I am MR or MGR");
                        Dcrmaster.app.models.TourProgram.find({
                            where: {
                                companyId: argCompanyId,
                                sendForApproval: true,
                                approvalStatus: true,
                                month: parseInt(argMonth),
                                year: argYear,
                                createdBy: argUserId
                            }
                        }, function(err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result.length > 0) {
                                cb(false, "Yes");
                            } else if (result.length == undefined) {
                                cb(false, "No");
                            } else {
                                cb(false, "No");
                            }
                        });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }
                },
                SelfTPSubmittedForNextMonth: function(cb) {

                    if (argLevel != 0 && argLevel >= 1) {
                        Dcrmaster.app.models.TourProgram.find({
                            where: {
                                companyId: argCompanyId,
                                sendForApproval: true,
                                month: parseInt(nextMonth),
                                year: nextyear,
                                createdBy: argUserId
                            }
                        }, function(err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result.length > 0) {
                                cb(false, "Yes");
                            } else if (result.length == undefined) {
                                cb(false, "No");
                            } else {
                                cb(false, "No");
                            }
                        });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }
                },
                SelfTPApprovedFoNextMonth: function(cb) {
                    if (argLevel != 0 && argLevel >= 1) {
                        Dcrmaster.app.models.TourProgram.find({
                            where: {
                                companyId: argCompanyId,
                                sendForApproval: true,
                                approvalStatus: true,
                                month: parseInt(nextMonth),
                                year: nextyear,
                                createdBy: argUserId
                            }
                        }, function(err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result.length > 0) {
                                cb(false, "Yes");
                            } else if (result.length == undefined) {
                                cb(false, "No");
                            } else {
                                cb(false, "No");
                            }
                        });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }
                },
                SelfDcrDetail: function(cb) {
                    if (argLevel != 0 && argLevel >= 1) {
                        Dcrmaster.getDCRStatus(
                            argCompanyId,
                            fromDate,
                            toDate, [true], [argUserId],
                            function(err, result) {
                                if (err) {
                                    console.log(err)
                                }
                                cb(false, result);
                            });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }

                },
                SelfCallAvg: function(cb) {
                    if (argLevel != 0 && argLevel >= 1) {
                        dcrCollection.aggregate({
                                $match: {
                                    companyId: ObjectId(argCompanyId),
                                    dcrDate: {
                                        $gte: new Date(fromDate),
                                        $lte: new Date(toDate)
                                    },
                                    workingStatus: "Working",
                                    submitBy: ObjectId(argUserId)
                                }
                            }, {
                                $lookup: {
                                    "from": "DCRProviderVisitDetails",
                                    "localField": "_id",
                                    "foreignField": "dcrId",
                                    "as": "providerDetails"
                                }
                            },

                            {
                                $unwind: {
                                    path: "$providerDetails",
                                    preserveNullAndEmptyArrays: true
                                }
                            },

                            {
                                $group: {
                                    _id: {},
                                    totalWorkingDCR: {
                                        $addToSet: "$_id"
                                    },
                                    totalRMPVisited: {
                                        $sum: {
                                            $cond: [{
                                                $and: [
                                                    { $eq: ["$providerDetails.providerType", "RMP"] }
                                                ]
                                            }, 1, 0]
                                        }
                                    },
                                    totalDrugVisited: {
                                        $sum: {
                                            $cond: [{
                                                $and: [
                                                    { $eq: ["$providerDetails.providerType", "Drug"] }
                                                ]
                                            }, 1, 0]
                                        }
                                    }


                                }
                            },

                            // Stage 5
                            {
                                $project: {
                                    _id: 0,
                                    totalWorkingDCR: { $size: "$totalWorkingDCR" },
                                    totalRMPVisited: 1,
                                    totalDrugVisited: 1,
                                    docCallAvg: { $divide: ["$totalRMPVisited", { $size: "$totalWorkingDCR" }] },
                                    venCallAvg: { $divide: ["$totalDrugVisited", { $size: "$totalWorkingDCR" }] },
                                }


                            },
                            function(err, result) {
                                if (err) {
                                    console.log(err)
                                }
                                let finalCallAvg = {
                                    doctorCallAverage: "0.00",
                                    vendorCallAverage: "0.00"
                                }
                                if (result.length > 0) {
                                    finalCallAvg.doctorCallAverage = result[0].docCallAvg.toFixed(2);
                                    finalCallAvg.vendorCallAverage = result[0].venCallAvg.toFixed(2);
                                }
                                cb(false, finalCallAvg);
                            });
                    } else if (argLevel == 0) {
                        cb(false, "");
                    }
                },
                TeamsTPForCurrentMonth: function(cb) {
                    if (argLevel > 1) {
                        //	console.log("PK....argCompanyId ",argCompanyId)
                        //	console.log("PK....argUserId ",argUserId)
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(argCompanyId),
                                supervisorId: ObjectId(argUserId)
                            }
                        }, function(err, hrcyResult) {
                            // console.log(hrcyResult)
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }
                            //console.log("PK.... ",passingUserIds)
                            Dcrmaster.app.models.TourProgram.getTotalTPStatus(
                                parseInt(argMonth),
                                argYear,
                                argCompanyId,
                                passingUserIds,
                                function(err, result) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    //console.log("TPSTATUSSSSSS", result)
                                    cb(false, result);
                                })
                        });

                    } else if (argLevel == 0) {
                        Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            argCompanyId, [true],
                            undefined, //Blank will be passed for admin level
                            function(err, result) {

                                let obj = {
                                    companyId: argCompanyId,
                                    month: parseInt(argMonth),
                                    year: argYear,
                                    status: [true],
                                    level: argLevel
                                }
                                Dcrmaster.app.models.TourProgram.usersTPStatusAPI(obj,
                                    function(err, result) {
                                        //console.log(result);
                                        if (err) {
                                            console.log(err);
                                        }
                                        cb(false, result);
                                    });
                            });

                    } else {
                        cb(false, "");
                    }
                },
                TeamsTPForNextMonth: function(cb) {
                    if (argLevel > 1) {
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(argCompanyId),
                                supervisorId: ObjectId(argUserId)
                            }
                        }, function(err, hrcyResult) {
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }
                            Dcrmaster.app.models.TourProgram.getTotalTPStatus(
                                parseInt(nextMonth),
                                nextyear,
                                argCompanyId,
                                passingUserIds,
                                function(err, result) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    // console.log(result)
                                    cb(false, result);
                                })
                        });

                    } else if (argLevel == 0) {
                        let obj = {
                            companyId: argCompanyId,
                            month: parseInt(nextMonth),
                            year: nextyear,
                            status: [true],
                            level: argLevel
                        }
                        Dcrmaster.app.models.TourProgram.usersTPStatusAPI(obj,
                            function(err, result) {
                                if (err) {
                                    console.log(err);
                                }
                                cb(false, result);
                            });

                    } else {
                        cb(false, "");
                    }
                },
                TeamsDCRDetails: function(cb) {
                    if (argLevel > 1) {
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(argCompanyId),
                                supervisorId: ObjectId(argUserId)
                            }
                        }, function(err, hrcyResult) {
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }
                            Dcrmaster.getDCRStatus(
                                argCompanyId,
                                fromDate,
                                toDate, [true],
                                passingUserIds,
                                function(err, result) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    cb(false, result);
                                });
                        });
                    } else if (argLevel == 0) {
                        Dcrmaster.getDCRStatus(
                            argCompanyId,
                            fromDate,
                            toDate, [true], [],
                            function(err, result) {
                                //console.log(result)
                                if (err) {
                                    console.log(err)
                                }
                                cb(false, result);
                            });
                    } else {
                        cb(false, "");
                    }
                },
                TeamsCallAvg: function(cb) {
                    if (argLevel > 1) {
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(argCompanyId),
                                supervisorId: ObjectId(argUserId)
                            }
                        }, function(err, hrcyResult) {
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }
                            dcrCollection.aggregate({
                                    $match: {
                                        companyId: ObjectId(argCompanyId),
                                        dcrDate: {
                                            $gte: new Date(fromDate),
                                            $lte: new Date(toDate)
                                        },
                                        workingStatus: "Working",
                                        submitBy: {
                                            $in: passingUserIds
                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DCRProviderVisitDetails",
                                        "localField": "_id",
                                        "foreignField": "dcrId",
                                        "as": "providerDetails"
                                    }
                                },

                                {
                                    $unwind: {
                                        path: "$providerDetails",
                                        preserveNullAndEmptyArrays: true
                                    }
                                },

                                {
                                    $group: {
                                        _id: {},
                                        totalWorkingDCR: {
                                            $addToSet: "$_id"
                                        },
                                        totalRMPVisited: {
                                            $sum: {
                                                $cond: [{
                                                    $and: [
                                                        { $eq: ["$providerDetails.providerType", "RMP"] }
                                                    ]
                                                }, 1, 0]
                                            }
                                        },
                                        totalDrugVisited: {
                                            $sum: {
                                                $cond: [{
                                                    $and: [
                                                        { $eq: ["$providerDetails.providerType", "Drug"] }
                                                    ]
                                                }, 1, 0]
                                            }
                                        }


                                    }
                                },

                                // Stage 5
                                {
                                    $project: {
                                        _id: 0,
                                        totalWorkingDCR: { $size: "$totalWorkingDCR" },
                                        totalRMPVisited: 1,
                                        totalDrugVisited: 1,
                                        docCallAvg: { $divide: ["$totalRMPVisited", { $size: "$totalWorkingDCR" }] },
                                        venCallAvg: { $divide: ["$totalDrugVisited", { $size: "$totalWorkingDCR" }] },
                                    }


                                },
                                function(err, result) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    let finalCallAvg = {
                                        doctorCallAverage: "0.00",
                                        vendorCallAverage: "0.00"
                                    }
                                    if (result.length > 0) {
                                        finalCallAvg.doctorCallAverage = result[0].docCallAvg.toFixed(2);
                                        finalCallAvg.vendorCallAverage = result[0].venCallAvg.toFixed(2);
                                    }
                                    cb(false, finalCallAvg);
                                });
                        });
                    } else if (argLevel == 0) {
                        Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            argCompanyId, [true],
                            undefined, //Blank will be passed for admin level
                            function(err, result) {
                                dcrCollection.aggregate({
                                        $match: {
                                            companyId: ObjectId(argCompanyId),
                                            dcrDate: {
                                                $gte: new Date(fromDate),
                                                $lte: new Date(toDate)
                                            },
                                            workingStatus: "Working",
                                            submitBy: { $in: result[0].userIds }
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "DCRProviderVisitDetails",
                                            "localField": "_id",
                                            "foreignField": "dcrId",
                                            "as": "providerDetails"
                                        }
                                    },

                                    {
                                        $unwind: {
                                            path: "$providerDetails",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },

                                    {
                                        $group: {
                                            _id: {},
                                            totalWorkingDCR: {
                                                $addToSet: "$_id"
                                            },
                                            totalRMPVisited: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [
                                                            { $eq: ["$providerDetails.providerType", "RMP"] }
                                                        ]
                                                    }, 1, 0]
                                                }
                                            },
                                            totalDrugVisited: {
                                                $sum: {
                                                    $cond: [{
                                                        $and: [
                                                            { $eq: ["$providerDetails.providerType", "Drug"] }
                                                        ]
                                                    }, 1, 0]
                                                }
                                            }


                                        }
                                    },

                                    // Stage 5
                                    {
                                        $project: {
                                            _id: 0,
                                            totalWorkingDCR: 1,
                                            totalRMPVisited: 1,
                                            totalDrugVisited: 1,
                                            docCallAvg: { $divide: ["$totalRMPVisited", { $size: "$totalWorkingDCR" }] },
                                            venCallAvg: { $divide: ["$totalDrugVisited", { $size: "$totalWorkingDCR" }] },
                                        }


                                    },
                                    function(err, result) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        let finalCallAvg = {
                                            doctorCallAverage: "0.00",
                                            vendorCallAverage: "0.00"
                                        }
                                        if (result.length > 0) {
                                            finalCallAvg.doctorCallAverage = result[0].docCallAvg.toFixed(2);
                                            finalCallAvg.vendorCallAverage = result[0].venCallAvg.toFixed(2);
                                        }
                                        cb(false, finalCallAvg);
                                    });
                            });
                    } else {
                        cb(false, "");
                    }
                }

            },
            function(err, results) {
                var returnArray = [];
                returnArray[0] = results;
                return cb(false, returnArray);
            });

    }
    Dcrmaster.remoteMethod(
        'getBasicStatus', {
            description: 'consolidateDCR for selected details',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'argCompanyId',
                    type: 'string'
                },
                {
                    arg: 'argMonth',
                    type: 'number'
                },
                {
                    arg: 'argYear',
                    type: 'number'
                }, {
                    arg: 'argLevel',
                    type: 'number'
                },
                {
                    arg: 'argUserId',
                    type: 'string'
                }
            ],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //------------------------------------END------------------------------------------------------
    //---------------------------------------Praveen Kumar(05-11-2018)-----------------------------
	Dcrmaster.getDrVenAvgStateOrEmpWise = function(companyId, fromdate, todate, level, userId, unlistedValidations, cb) {
        var self = this;
        //  console.log(companyId)
        //    console.log(fromdate)
        //   console.log(todate)
        //console.log(unlistedValidations)
        let dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        let dynamicMatch = {
            companyId: ObjectId(companyId),
            dcrDate: {
                $gte: new Date(fromdate),
                $lte: new Date(todate)
            },
            workingStatus: "Working"
        }

        let dynamicGroup = {};

        let dynamicLookUp = {
            "from": "DCRProviderVisitDetails",
            "localField": "_id",
            "foreignField": "dcrId",
            "as": "providerDetails"
        };
        let dynamicUnwind = {
            path: "$providerDetails",
            preserveNullAndEmptyArrays: true
        };

        let dynamicProject = {
            _id: 0,
            totalWorkingDCR: { $size: "$totalWorkingDCR" },
            doctorCall: 1,
            vendorCall: 1,
        };
        let dynamicLookUp1 = {};
        let dynamicSort = {};

        //-----Dynamic Group and Project For Doctors--------------------
        if (unlistedValidations.unlistedDocCall == true && unlistedValidations.unlistedDocAvg == true) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        } else if (unlistedValidations.unlistedDocCall == false && unlistedValidations.unlistedDocAvg == false) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (unlistedValidations.unlistedDocCall == true && unlistedValidations.unlistedDocAvg == false) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicGroup.doctorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }




            dynamicProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (unlistedValidations.unlistedDocCall == false && unlistedValidations.unlistedDocAvg == true) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicGroup.doctorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }


            dynamicProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        }
        //-----Dynamic Group and Project For Vendor--------------------
        if (unlistedValidations.unlistedVenCall == true && unlistedValidations.unlistedVenAvg == true) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }



            dynamicProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }


        } else if (unlistedValidations.unlistedVenCall == false && unlistedValidations.unlistedVenAvg == false) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }



            dynamicProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (unlistedValidations.unlistedVenCall == true && unlistedValidations.unlistedVenAvg == false) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]


                    }, 1, 0]
                }
            }

            dynamicGroup.vendorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }


            dynamicProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (unlistedValidations.unlistedVenCall == false && unlistedValidations.unlistedVenAvg == true) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dynamicGroup.vendorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }


            dynamicProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        }

        if (level == 0) {

            dynamicGroup._id = {
                stateId: "$stateId"
            }
            dynamicGroup.totalWorkingDCR = {
                $addToSet: "$_id"
            }


            dynamicLookUp1 = {
                "from": "State",
                "localField": "_id.stateId",
                "foreignField": "_id",
                "as": "state"
            };

            dynamicProject.stateId = "$_id.stateId";
            dynamicProject.stateName = { $arrayElemAt: ["$state.stateName", 0] };

            dynamicSort = {
                stateName: 1
            };

            Dcrmaster.app.models.State.find({
                where: {
                    assignedTo: companyId,
                    id: {
                        neq: "5900a7082aaaf719a1a8d74c"
                    }
                },
                order: "stateName ASC"
            }, function(err, stateResult) {
                if (err) {
                    console.log(err)
                }
                dcrCollection.aggregate({
                    $match: dynamicMatch
                }, {
                    $lookup: dynamicLookUp
                }, {
                    $unwind: dynamicUnwind
                }, {
                    $group: dynamicGroup
                }, {
                    $lookup: dynamicLookUp1
                }, {
                    $project: dynamicProject
                }, function(err, result) {
                    if (err) {
                        console.log(err);
                    }
                    let finalResult = [];
                    for (let i = 0; i < stateResult.length; i++) {

                        let filterObj = result.filter(function(obj) {
                            return obj.stateId.toString() == stateResult[i].id.toString();
                        });
                        if (filterObj.length > 0) {
                            finalResult.push({
                                "totalWorkingDCR": filterObj[0].totalWorkingDCR,
                                "totalRMPVisited": filterObj[0].doctorCall,
                                "totalDrugVisited": filterObj[0].vendorCall,
                                "stateId": filterObj[0].stateId,
                                "stateName": filterObj[0].stateName,
                                "docCallAvg": filterObj[0].drCallAvg.toFixed(2),
                                "venCallAvg": filterObj[0].venCallAvg.toFixed(2),
                            });
                        } else {
                            finalResult.push({
                                "totalWorkingDCR": 0,
                                "totalRMPVisited": 0,
                                "totalDrugVisited": 0,
                                "stateId": stateResult[i].id,
                                "stateName": stateResult[i].stateName,
                                "docCallAvg": 0,
                                "venCallAvg": 0
                            });
                        }
                    }
                    cb(false, finalResult);
                })
            })

        } else if (level >= 2) {
            Dcrmaster.app.models.Hierarchy.find({
                where: {
                    companyId: ObjectId(companyId),
                    supervisorId: ObjectId(userId),
                    status: true
                }
            }, function(err, hrcyResult) {
                if (err) {
                    console.log(err);
                }
                let passingUserIds = [];
                for (let i = 0; i < hrcyResult.length; i++) {
                    passingUserIds.push(hrcyResult[i].userId);
                }

                Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                    companyId, [true],
                    passingUserIds, //Blank will be passed for admin level
                    function(err, result) {
                        dynamicMatch.submitBy = {
                            $in: result[0].userIds
                        }

                        dynamicGroup._id = {
                            submitBy: "$submitBy"
                        }
                        dynamicGroup.totalWorkingDCR = {
                            $addToSet: "$_id"
                        }

                        dynamicLookUp1 = {
                            "from": "UserLogin",
                            "localField": "_id.submitBy",
                            "foreignField": "_id",
                            "as": "emp"
                        };
                        dynamicProject.empId = "$_id.submitBy";
                        dynamicProject.empName = { $arrayElemAt: ["$emp.name", 0] };


                            dynamicSort = {
                                empName: 1
                            };

                        dcrCollection.aggregate({
                            $match: dynamicMatch
                        }, {
                            $lookup: dynamicLookUp
                        }, {
                            $unwind: dynamicUnwind
                        }, {
                            $group: dynamicGroup
                        }, {
                            $lookup: dynamicLookUp1
                        }, {
                            $project: dynamicProject
                        }, function(err, dcrResult) {
                            if (err) {
                                console.log(err)
                            }
                            //  console.log(result[0])
                            let finalResult = [];
                            for (let i = 0; i < result[0].obj.length; i++) {

                                let filterObj = dcrResult.filter(function(obj) {
                                    return obj.empId.toString() == result[0].obj[i].userId.toString();
                                });
                                if (filterObj.length > 0) {
                                    finalResult.push({
                                        "totalWorkingDCR": filterObj[0].totalWorkingDCR,
                                        "totalRMPVisited": filterObj[0].doctorCall,
                                        "totalDrugVisited": filterObj[0].vendorCall,
                                        "empId": filterObj[0].id,
                                        "empName": filterObj[0].empName,
                                        "docCallAvg": filterObj[0].drCallAvg.toFixed(2),
                                        "venCallAvg": filterObj[0].venCallAvg.toFixed(2),
                                    });
                                } else {
                                    finalResult.push({
                                        "totalWorkingDCR": 0,
                                        "totalRMPVisited": 0,
                                        "totalDrugVisited": 0,
                                        "empId": result[0].obj[i].userId,
                                        "empName": result[0].obj[i].name,
                                        "docCallAvg": 0,
                                        "venCallAvg": 0
                                    });
                                }
                            }
                            cb(false, finalResult)
                        })
                    });

            });
        }
    }
	Dcrmaster.remoteMethod(
        'getDrVenAvgStateOrEmpWise', {
            description: 'consolidateDCR for selected details',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'companyId',
                    type: 'string'
                },
                {
                    arg: 'fromdate',
                    type: 'string'
                },
                {
                    arg: 'todate',
                    type: 'string'
                },
                {
                    arg: 'level',
                    type: 'number'
                }, {
                    arg: 'userId',
                    type: 'string'
                }, {
                    arg: 'unlistedValidations',
                    type: 'object'
                }
            ],
            returns: {
                root: true,
                type: 'object'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //-----------------------------------------------END-------------------------------------------

    //---------------------------------------Ravindra Singh(12-11-2018)-----------------------------
	Dcrmaster.getDrVenAvgMonthWise = function(companyId, fromdate, todate, level, userIds, unlistedValidations, cb) {
        var self = this;

        let dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        let dynamicMatch = {
            companyId: ObjectId(companyId),
            dcrDate: {
                $gte: new Date(fromdate),
                $lte: new Date(todate)
            },
            workingStatus: "Working"
        };

        let dynamicLookup = {
            "from": "DCRProviderVisitDetails",
            "localField": "_id",
            "foreignField": "dcrId",
            "as": "providerDetails"
        };

        let dynamicUnwind = {
            path: "$providerDetails",
            preserveNullAndEmptyArrays: true
        };

        let dynamicGroup = {
            _id: {
                months: { $month: '$dcrDate' },
                year: { $year: '$dcrDate' }
            },
            totalWorkingDCR: {
                $addToSet: "$_id"
            }
        };

        let dynamicProject = {
            _id: 0,
            month: "$_id.months",
            year: "$_id.year",
            totalWorkingDCR: { $size: "$totalWorkingDCR" },
            totalRMPVisited: "$doctorCall",
            totalDrugVisited: "$vendorCall",
        };

        //-----Dynamic Group and Project For Doctors--------------------
        if (unlistedValidations.unlistedDocCall == true && unlistedValidations.unlistedDocAvg == true) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        } else if (unlistedValidations.unlistedDocCall == false && unlistedValidations.unlistedDocAvg == false) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (unlistedValidations.unlistedDocCall == true && unlistedValidations.unlistedDocAvg == false) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicGroup.doctorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }




            dynamicProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (unlistedValidations.unlistedDocCall == false && unlistedValidations.unlistedDocAvg == true) {
            dynamicGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }, {
                            $ne: ["$providerDetails.providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dynamicGroup.doctorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerDetails.providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }


            dynamicProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        }
        //-----Dynamic Group and Project For Vendor--------------------
        if (unlistedValidations.unlistedVenCall == true && unlistedValidations.unlistedVenAvg == true) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }



            dynamicProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }


        } else if (unlistedValidations.unlistedVenCall == false && unlistedValidations.unlistedVenAvg == false) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }



            dynamicProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (unlistedValidations.unlistedVenCall == true && unlistedValidations.unlistedVenAvg == false) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]


                    }, 1, 0]
                }
            }

            dynamicGroup.vendorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }


            dynamicProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (unlistedValidations.unlistedVenCall == false && unlistedValidations.unlistedVenAvg == true) {
            dynamicGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerDetails.providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerDetails.providerType", "Drug"]
                                }, {
                                    $eq: ["$providerDetails.providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dynamicGroup.vendorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerDetails.providerType", "Drug"]
                        }, {
                            $eq: ["$providerDetails.providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }


            dynamicProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$totalWorkingDCR" }, 1]
                        },
                        then: { $size: "$totalWorkingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        }

        if (level == 0) {
            dcrCollection.aggregate( // Stage 1
                {
                    $match: dynamicMatch
                },

                // Stage 2
                {
                    $lookup: dynamicLookup
                },

                // Stage 3
                {
                    $unwind: dynamicUnwind
                },

                // Stage 4
                {
                    $group: dynamicGroup
                },

                // Stage 5
                {
                    $project: dynamicProject
                },

                // Options
                {
                    cursor: {
                        batchSize: 50
                    },

                    allowDiskUse: true
                },
                function(err, monthwiseResult) {
                    if (err) {
                        console.log(err);
                    }


                    let finalResult = [];
                    let currentMonth = new Date().getMonth() + 1;
                    let currentYear = new Date().getFullYear();
                    if (monthwiseResult.length > 0) {
                        for (let ii = 1; ii <= currentMonth; ii++) {
                            let filterObj = monthwiseResult.filter(function(obj) {
                                return obj.month == ii
                            });
                            if (filterObj.length > 0) {
                                finalResult.push({
                                    "totalWorkingDCR": filterObj[0].totalWorkingDCR,
                                    "totalRMPVisited": filterObj[0].totalRMPVisited,
                                    "totalDrugVisited": filterObj[0].totalDrugVisited,
                                    "month": filterObj[0].month,
                                    "year": filterObj[0].year,
                                    "docCallAvg": filterObj[0].drCallAvg.toFixed(2),
                                    "venCallAvg": filterObj[0].venCallAvg.toFixed(2),
                                });
                            } else {
                                finalResult.push({
                                    "totalWorkingDCR": 0,
                                    "totalRMPVisited": 0,
                                    "totalDrugVisited": 0,
                                    "month": ii,
                                    "year": currentYear,
                                    "docCallAvg": 0,
                                    "venCallAvg": 0
                                });
                            }
                        }
                    } else {
                        for (let ii = 1; ii <= currentMonth; ii++) {
                            finalResult.push({
                                "totalWorkingDCR": 0,
                                "totalRMPVisited": 0,
                                "totalDrugVisited": 0,
                                "month": ii,
                                "year": currentYear,
                                "docCallAvg": 0.0,
                                "venCallAvg": 0.0
                            });
                        }
                    }

                    cb(false, finalResult);
                });

        } else if (level >= 2) {
            Dcrmaster.app.models.Hierarchy.find({
                where: {
                    companyId: ObjectId(companyId),
                    supervisorId: ObjectId(userIds),
                    status: true
                }
            }, function(err, hrcyResult) {
                if (err) {
                    console.log(err);
                }
                let finalResult = [];
                let currentMonth = new Date().getMonth() + 1;
                let currentYear = new Date().getFullYear();
                if (hrcyResult.length > 0) {
                    let passingUserIds = [];
                    for (let i = 0; i < hrcyResult.length; i++) {
                        passingUserIds.push(hrcyResult[i].userId);
                    }

                    Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                        companyId, [true],
                        passingUserIds, //Blank will be passed for admin level
                        function(err, result) {
                            if (result.length > 0) {
                                dynamicMatch.submitBy = {
                                    $in: result[0].userIds
                                }
                                dcrCollection.aggregate( // Stage 1
                                    {
                                        $match: dynamicMatch
                                    },

                                    // Stage 2
                                    {
                                        $lookup: dynamicLookup
                                    },

                                    // Stage 3
                                    {
                                        $unwind: dynamicUnwind
                                    },

                                    // Stage 4
                                    {
                                        $group: dynamicGroup

                                    },

                                    // Stage 5
                                    {
                                        $project: dynamicProject
                                    },
                                    function(err, monthwiseResult) {
                                        if (err) {
                                            console.log(err);
                                        }

                                        if (monthwiseResult.length > 0) {
                                            for (let ii = 1; ii <= currentMonth; ii++) {
                                                let filterObj = monthwiseResult.filter(function(obj) {
                                                    return obj.month == ii
                                                });
                                                if (filterObj.length > 0) {
                                                    finalResult.push({
                                                        "totalWorkingDCR": filterObj[0].totalWorkingDCR,
                                                        "totalRMPVisited": filterObj[0].totalRMPVisited,
                                                        "totalDrugVisited": filterObj[0].totalDrugVisited,
                                                        "month": filterObj[0].month,
                                                        "year": filterObj[0].year,
                                                        "docCallAvg": filterObj[0].drCallAvg.toFixed(2),
                                                        "venCallAvg": filterObj[0].venCallAvg.toFixed(2),
                                                    });
                                                } else {
                                                    finalResult.push({
                                                        "totalWorkingDCR": 0,
                                                        "totalRMPVisited": 0,
                                                        "totalDrugVisited": 0,
                                                        "month": ii,
                                                        "year": currentYear,
                                                        "docCallAvg": 0,
                                                        "venCallAvg": 0
                                                    });
                                                }
                                            }
                                        } else {
                                            for (let ii = 1; ii <= currentMonth; ii++) {
                                                finalResult.push({
                                                    "totalWorkingDCR": 0,
                                                    "totalRMPVisited": 0,
                                                    "totalDrugVisited": 0,
                                                    "month": ii,
                                                    "year": currentYear,
                                                    "docCallAvg": 0.0,
                                                    "venCallAvg": 0.0
                                                });
                                            }
                                        }
                                        cb(false, finalResult);
                                    });

                            }
                        });

                } else {
                    for (let ii = 1; ii <= currentMonth; ii++) {
                        finalResult.push({
                            "totalWorkingDCR": 0,
                            "totalRMPVisited": 0,
                            "totalDrugVisited": 0,
                            "month": ii,
                            "year": currentYear,
                            "docCallAvg": 0.0,
                            "venCallAvg": 0.0
                        });
                    }
                    cb(false, finalResult);
                }
            });
        }
    }
	Dcrmaster.remoteMethod(
        'getDrVenAvgMonthWise', {
            description: 'getDrVenAvgMonthWise for selected details',
            accepts: [
                //  { arg: 'ques', type: 'array' },
                {
                    arg: 'companyId',
                    type: 'string'
                },
                {
                    arg: 'fromdate',
                    type: 'string'
                },
                {
                    arg: 'todate',
                    type: 'string'
                },
                {
                    arg: 'level',
                    type: 'number'
                }, {
                    arg: 'userIds',
                    type: 'string'
                }, {
                    arg: 'unlistedValidations',
                    type: 'object'
                }
            ],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //-----------------------------------------------END-------------------------------------------
    //---------------------------------------Praveen Kumar(24-11-2018)------------------------------
   Dcrmaster.getUntouchedDoctorDetail = function (params, cb) {

        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var providersCollection = this.getDataSource().connector.collection(Dcrmaster.app.models.Providers.modelName);
        var viewFor = params.viewFor;
        var categoryType = this;


        async.parallel({
            userResult: function (cb) {
                if (params.type == "State" || params.type == "Headquarter") {
                    Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
                        function (err, userRecords) {
                            cb(false, userRecords);
                        });
                } else if (params.type == "Area") {
                    Dcrmaster.app.models.UserAreaMapping.getAllUserIdsOnAreaBasis(
                        params.companyId,
                        params.areaIds,
                        function (err, userRecords) {

                            cb(false, userRecords);
                        });
                } else {
                    let userIds = [];
                    for (let i = 0; i < params.userIds.length; i++) {
                        userIds.push(ObjectId(params.userIds[i]));
                    }
                    if (params.designationLevel == 1) {

                        Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            params.companyId, [true],
                            userIds,
                            function (err, userRecords) {

                                cb(false, userRecords);
                            });
                    } else if (params.designationLevel > 1) {
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(params.companyId),
                                supervisorId: {
                                    inq: userIds
                                },
                                status: true
                            }
                        }, function (err, hrcyResult) {
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }
                            let finalObject = [{
                                "userIds": passingUserIds
                            }]
                            cb(false, finalObject);
                        })
                    }

                }
            },
            assignedArea: function (cb) {
                if (params.type == "State" || params.type == "Headquarter") {
                    Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
                        function (err, userRecords) {

                            let userIds = [];

                            if (userRecords.length > 0) {
                                userIds = userRecords[0].userIds
                            }

                            Dcrmaster.app.models.UserAreaMapping.find({
                                where: {
                                    userId: {
                                        inq: userIds
                                    },
                                    appStatus: {
                                        neq: "pending"
                                    },
                                    status: {
                                        inq: [true, false]
                                    }
                                }
                            }, function (err, assignedAreaResult) {
                                if (err) {
                                    console.log(err);
                                }
                                cb(false, assignedAreaResult);
                            });

                        });
                } else if (params.type == "Area") {
                    let areaIdArr = [];
                    for (var w in params.areaIds) {
                        areaIdArr.push(ObjectId(params.areaIds[w]));
                    }
                    Dcrmaster.app.models.UserAreaMapping.find({
                        where: {
                            companyId: params.companyId,
                            areaId: {
                                inq: areaIdArr
                            },
                            appStatus: {
                                neq: "pending"
                            },
                            status: {
                                inq: [true, false]
                            }
                        }
                    }, function (err, assignedAreaResult) {
                        if (err) {
                            console.log(err);
                        }
                        cb(false, assignedAreaResult);
                    });
                } else {
                    let userIds = [];
                    if (params.designationLevel == 1) {
                        for (let i = 0; i < params.userIds.length; i++) {
                            userIds.push(ObjectId(params.userIds[i]));
                        }

                        Dcrmaster.app.models.UserAreaMapping.find({
                            where: {
                                userId: {
                                    inq: userIds
                                },
                                appStatus: {
                                    neq: "pending"
                                },
                                status: {
                                    inq: [true, false]
                                }

                            }
                        }, function (err, assignedAreaResult) {
                            if (err) {
                                console.log(err);
                            }

                            cb(false, assignedAreaResult);
                        });
                    } else if (params.designationLevel > 1) {
                        for (let i = 0; i < params.userIds.length; i++) {
                            userIds.push(ObjectId(params.userIds[i]));
                        }
                        Dcrmaster.app.models.Hierarchy.find({
                            where: {
                                companyId: ObjectId(params.companyId),
                                supervisorId: {
                                    inq: userIds
                                },
                                status: true
                            }
                        }, function (err, hrcyResult) {
                            if (err) {
                                console.log(err);
                            }
                            let passingUserIds = [];
                            for (let i = 0; i < hrcyResult.length; i++) {
                                passingUserIds.push(hrcyResult[i].userId);
                            }

                            Dcrmaster.app.models.UserAreaMapping.find({
                                where: {
                                    companyId: params.companyId,
                                    userId: {
                                        inq: passingUserIds
                                    },
                                    appStatus: {
                                        neq: "pending"
                                    },
                                    status: {
                                        inq: [true, false]
                                    }
                                }
                            }, function (err, assignedAreaResult) {

                                if (err) {
                                    console.log(err);
                                }
                                cb(false, assignedAreaResult);
                            });
                        })
                    }

                }
            }
        }, function (err, result) {
			console.log("result : ",result);
            let userIds = [];
            if (params.hasOwnProperty("userIds") === true) {
                for (let i = 0; i < params.userIds.length; i++) {
                    userIds.push(ObjectId(params.userIds[i]));
                }
            }

            async.parallel({
                untouchedProvider: function (cb) {
                    dcrCollection.aggregate({
                        $match: {
                            companyId: ObjectId(params.companyId),
                            submitBy: {
                                $in: userIds
                            },
                            dcrDate: {
                                $gte: new Date(params.fromDate),
                                $lte: new Date(params.toDate)
                            }
                        }
                    }, {
                            $lookup: {
                                "from": "DCRProviderVisitDetails",
                                "localField": "_id",
                                "foreignField": "dcrId",
                                "as": "providerDetail"
                            }
                        }, {
                            $unwind: "$providerDetail"
                        }, {
                            $match: {
                                "providerDetail.providerType": params.viewFor
                            }
                        }, {
                            $group: {
                                _id: {},
                                providersId: {
                                    $addToSet: "$providerDetail.providerId"
                                }
                            }
                        }, function (err, subProviderResult) {
							console.log("subProviderResult :",subProviderResult);
                            if (err) {
                                console.log(err);
                            }

                            let areaArray = [];
                            for (let i = 0; i < result.assignedArea.length; i++) {
                                areaArray.push(result.assignedArea[i].areaId);
                            }

                            let visitedProviderArray = []
                            if (subProviderResult.length > 0) {
                                visitedProviderArray = subProviderResult[0].providersId
                            }
                            let match={
                                companyId: ObjectId(params.companyId),
                                blockId: {
                                    $in: areaArray
                                },
                                _id: {
                                    $nin: visitedProviderArray
                                },
                                providerType: {$in:params.viewFor},
                                appStatus: "approved",
                                status: {
                                    $in: [true, false]
                                },
                                finalAppDate: {
                                    $lte: new Date(params.toDate)
                                },
                                $or: [{
                                    finalDelDate: {
                                        $gte: new Date(params.fromDate),
                                        $lte: new Date(params.toDate),
                                    }
                                }, {
                                    finalDelDate: {
                                        $eq: new Date("1900-01-01T00:00:00.000Z")
                                    }
                                }]

                            }
                            if(params.category.length>0){
                                match.category={ $in: params.category }
                            }

                            providersCollection.aggregate({
                                $match: match
                            }, {
                                    $lookup: {
                                        "from": "State",
                                        "localField": "stateId",
                                        "foreignField": "_id",
                                        "as": "stateDetails"
                                    }
                                },

                                // Stage 3
                                {
                                    $lookup: {
                                        "from": "District",
                                        "localField": "districtId",
                                        "foreignField": "_id",
                                        "as": "districtDetails"
                                    }
                                },

                                // Stage 4
                                {
                                    $lookup: {
                                        "from": "Area",
                                        "localField": "blockId",
                                        "foreignField": "_id",
                                        "as": "areaDetails"
                                    }
                                }, {
                                    $lookup: {
                                        "from": "UserAreaMapping",
                                        "localField": "blockId",
                                        "foreignField": "areaId",
                                        "as": "mappedUserArea"
                                    }
                                }, {
                                    $unwind: "$mappedUserArea"
                                }, {
                                    $lookup: {
                                        "from": "UserInfo",
                                        "localField": "mappedUserArea.userId",
                                        "foreignField": "userId",
                                        "as": "userPersonalDetails"
                                    }
                                }, {
                                    $unwind: {
                                        path: "$userPersonalDetails",
                                        preserveNullAndEmptyArrays: true
                                    }
                                }, {
                                    $unwind: {
                                        path: "$userPersonalDetails.divisionId",
                                        preserveNullAndEmptyArrays: true
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DivisionMaster",
                                        "localField": "userPersonalDetails.divisionId",
                                        "foreignField": "_id",
                                        "as": "division"
                                    }
                                }, {
                                    $project: {
                                        stateId: 1,
                                        divisionName: { $arrayElemAt: ["$division.divisionName", 0] },
                                        divisionId: { $arrayElemAt: ["$division._id", 0] },
                                        stateName: {
                                            $arrayElemAt: ["$stateDetails.stateName", 0]
                                        },
                                        districtId: 1,
                                        districtName: {
                                            $arrayElemAt: ["$districtDetails.districtName", 0]
                                        },
                                        areaId: 1,
                                        areaName: {
                                            $arrayElemAt: ["$areaDetails.areaName", 0]
                                        },
                                        userName: "$userPersonalDetails.name",
                                        providerName: 1,
                                        providerCode: 1,
                                        providerType:1,
                                        category: 1,
                                        address: 1,
                                        mobile: 1,
                                        providerStatus: {
                                            "$cond": {
                                                "if": {
                                                    $and: [{
                                                        "$eq": ["$status", true]
                                                    }, {
                                                        "$eq": ["$appStatus", "approved"]
                                                    }]
                                                },
                                                "then": "Active",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            $and: [{
                                                                "$eq": ["$status", false]
                                                            }, {
                                                                "$eq": ["$delStatus", "approved"]
                                                            }]
                                                        },
                                                        "then": "InActive",
                                                        "else": "$abc"
                                                    }
                                                }
                                            }
                                        },
                                        areaStatus: {
                                            "$cond": {
                                                "if": {
                                                    $and: [{
                                                        "$eq": ["$mappedUserArea.status", true]
                                                    }, {
                                                        "$eq": ["$mappedUserArea.appStatus", "approved"]
                                                    }]
                                                },
                                                "then": "Active",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            $and: [{
                                                                "$eq": ["$mappedUserArea.status", false]
                                                            }, {
                                                                "$eq": ["$mappedUserArea.delStatus", "approved"]
                                                            }]
                                                        },
                                                        "then": "InActive",
                                                        "else": "$abc"
                                                    }
                                                }
                                            }
                                        },
                                        doa: 1,
                                        dob: 1,
                                        frequencyVisit: 1,
                                        focusProduct: 1,
                                        degree: 1,
                                        specialization: 1,
                                        email: 1,
                                        city: 1
                                    }
                                }, {
                                    $sort: {
                                        userName: 1,
                                        districtName: 1,
                                        areaStatus: 1,
                                        areaName: 1,
                                        providerStatus: 1,
                                        providerName: 1
                                    }

                                }, {
                                    allowDiskUse: true
                                },
                                function (err, result) {
                                    console.log(result[0])
                                    if (err) {
                                        console.log(err)
                                    }
                                    cb(false, result);
                                });
                        })

                }
            },
                function (err, result) {
                    cb(false, result);
                })
        })

    }

    Dcrmaster.remoteMethod('getUntouchedDoctorDetail', {
        description: "Untouched/Missed Doctor API",
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }

    })
        //-----------------------------------------------END--------------------------------------------

    //getting MgrworkedWith monthly & Yearly wise Done Praveen Singh (19-12-2018)
    Dcrmaster.mgrWorkedWithMontlyOrYearly = function(params, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        let year = params.year;
        let month = params.month;
        let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
        let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");

        dcrCollection.aggregate({
                $match: {
                    dcrDate: {
                        '$gte': new Date(fromDate),
                        '$lte': new Date(toDate)
                    },
                    companyId: ObjectId(params['companyId']),
                    submitBy: ObjectId(params['submitBy'])
                }
            }, {
                $lookup: {
                    "from": "DCRProviderVisitDetails",
                    "localField": "_id",
                    "foreignField": "dcrId",
                    "as": "DCRProviderVisit"
                }
            },

            {
                $project: {
                    _id: 0,
                    DCRProviderVisit: 1,
                    jointWork: 1
                }
            }, {
                $unwind: {
                    path: "$jointWork",
                    includeArrayIndex: "index",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $unwind: {
                    path: "$DCRProviderVisit",
                    includeArrayIndex: "visitIndex",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $project: {
                    "_id": "$DCRProviderVisit.dcrId",
                    "dcrDate": "$DCRProviderVisit.dcrDate",
                    "providerType": "$DCRProviderVisit.providerType",
                    "submitBy": "$DCRProviderVisit.submitBy",
                    //"jointWorkWithId": "$DCRProviderVisit.jointWorkWithId",
                    "blockId": "$DCRProviderVisit.blockId",
                    "districtId": "$DCRProviderVisit.districtId",
                    "stateId": "$DCRProviderVisit.stateId",
                    "jointWork": "$jointWork.Name",
                    "index": "$index",
                    "visitIndex": "$visitIndex"
                }
            },

            {
                $match: {
                    "dcrDate": {
                        $gte: new Date(fromDate),
                        $lte: new Date(toDate)
                    },
                    "index": {
                        $ne: null
                    },

                    submitBy: ObjectId(params['submitBy']),
                    "providerType": "RMP"
                }

            },

            /*{
              $lookup: {
                "from": "UserLogin",
                "localField": "jointWorkWithId",
                "foreignField": "_id",
                "as": "user"
              }
            },*/

            {
                $lookup: {
                    "from": "District",
                    "localField": "districtId",
                    "foreignField": "_id",
                    "as": "district"
                }
            },

            {
                $lookup: {
                    "from": "Area",
                    "localField": "blockId",
                    "foreignField": "_id",
                    "as": "area"
                }
            },

            {
                $project: {
                    dcrDate: 1,
                    workWith: "$jointWork",
                    district: {
                        $arrayElemAt: ["$district.districtName", 0]
                    },
                    block: {
                        $arrayElemAt: ["$area.areaName", 0]
                    },
                    providerId: 1
                }
            },

            {
                $group: {
                    _id: {
                        dcrDate: "$dcrDate"
                    },
                    workWithVisit: {
                        $sum: 1
                    },
                    workWith: {
                        $addToSet: "$workWith"
                    },
                    visitedArea: {
                        $addToSet: "$block"
                    },
                    district: {
                        $addToSet: "$district"
                    }
                }
            }, {
                $project: {
                    "_id": 0,
                    "dcrDate": "$_id.dcrDate",
                    "workWithVisit": "$workWithVisit",
                    "workWith": {
                        $arrayElemAt: ["$workWith", 0]
                    },
                    "visitedArea": "$visitedArea",
                    "district": {
                        $arrayElemAt: ["$district", 0]
                    }
                }
            },

            {
                $sort: {
                    "dcrDate": 1
                }
            },



            {
                cursor: {
                    batchSize: 50
                },

                allowDiskUse: true
            },
            function(err, response) {
                if (err) {
                    return cb(err)
                }
                return cb(false, response);
            });



    }
    Dcrmaster.remoteMethod(
        'mgrWorkedWithMontlyOrYearly', {
            description: 'Getting monthy & Yearly Manager worked with Report',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );


    // getting Manager Team Performance monthly
    /* Dcrmaster.getManagerTeamPerformance = function(params, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var UserAreaMappingCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.UserAreaMapping.modelName);
        let year = params.year;
        let month = params.month;

        let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
        let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");

        let currentDate = moment.utc().format("YYYY-MM-DDT00:00:00.000Z");
        let numberOfDay = 0;
        let numberOfMonth = moment.utc().month() + 1;
        let numberOfYear = moment.utc().year();
        if (numberOfMonth === month && numberOfYear === year) {
            numberOfDay = moment.utc().date();
        } else {
            numberOfDay = moment.utc(year + "-" + month + "-01").endOf("month").date();
        }
        let obj = {
            companyId: params.companyId,
            status: params.status,
            type: params.type,
            supervisorId: params.supervisorId
        }
		
		if (params.isDivisionExist) {
            obj.isDivisionExist = params.isDivisionExist
            obj.division = params.division
        }

        let dcrDataGroup = {

            _id: {
                "submitBy": "$submitBy"
            },
            totalDCR: {
                $addToSet: "$dcrId"
            },
            workingDCR: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$DCRStatus", 1]
                        }]
                    }, "$dcrId", "$abc"]
                }
            },
            otherDCR: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$DCRStatus", 1]
                        }]
                    }, "$dcrId", "$abc"]
                }
            },
            incompleteDCR: {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$workingStatus", "Working"]
                        }, {
                            $eq: ["$arrayIndex", null]
                        }]
                    }, 1, 0]
                }
            },
            uniqueDoctorVisit: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", 0]
                }
            }

        }
        let dcrDataProject = {


                _id: 0,
                userId: "$_id.submitBy",
                doctorCall: 1,
                vendorCall: 1,
                totalDCR: {
                    $size: "$totalDCR"
                },
                workingDCR: {
                    $size: "$workingDCR"
                },
                otherDCR: {
                    $size: "$otherDCR"
                },
                listedDoctorCall: {
                    $size: "$listedDoctorCall"
                },
                listedVendorCall: {
                    $size: "$listedVendorCall"
                },
                incompleteDCR: 1
            }
            //-----Dynamic Group and Project For Doctors--------------------
        if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == true) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == true) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgTrue = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        }
        //-----Dynamic Group and Project For Vendor--------------------
        if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == false) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == false) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, 1, 0]
                }
            }

            dcrDataGroup.vendorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.vendorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgTrue = {

                $addToSet: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        }
        Dcrmaster.app.models.Hierarchy.getManagerHierarchy(obj, function(err, users) {
            if (users.length > 0) {
                let userIds = [];
                for (let user of users) {
                    userIds.push(ObjectId(user.userId))

                }
                async.parallel({
                        dcrData: function(cb) {
                            dcrCollection.aggregate({
                                $match: {
                                    companyId: ObjectId(params.companyId),
                                    submitBy: {
                                        $in: userIds
                                    },
                                    dcrDate: {
                                        //$gte: new Date("2018-10-01"),
                                        //$lte: new Date("2018-10-31")
                                        $gte: new Date(fromDate),
                                        $lte: new Date(toDate)
                                    }
                                }
                            }, {
                                $lookup: {
                                    "from": "DCRProviderVisitDetails",
                                    "localField": "_id",
                                    "foreignField": "dcrId",
                                    "as": "dcrProviders"
                                }
                            }, {
                                $unwind: {
                                    path: "$dcrProviders",
                                    preserveNullAndEmptyArrays: true,
                                    includeArrayIndex: "arrayIndex"
                                }
                            }, {
                                $project: {

                                    dcrId: "$_id",
                                    dcrDate: "$dcrDate",
                                    workingStatus: 1,
                                    providerType: "$dcrProviders.providerType",
                                    providerId: "$dcrProviders.providerId",
                                    submitBy: 1,
                                    arrayIndex: 1,
                                    providerStatus: "$dcrProviders.providerStatus",
                                    DCRStatus: 1
                                }
                            }, {
                                $group: dcrDataGroup
                            }, {
                                $project: dcrDataProject
                            }, {
                                cursor: {
                                    batchSize: 50
                                },

                                allowDiskUse: true
                            }, function(err, result) {
                                cb(false, result)
                            });
                        },
                        tpData: function(cb) {
                            let obj = {
                                "type": "Employee Wise",
                                "status": [true, false],
                                "companyId": params.companyId,
                                "userIds": userIds,
                                "month": month,
                                "year": year
                            }
                            Dcrmaster.app.models.TourProgram.getTPStatus(obj, function(err, tpStatus) {
                                cb(false, tpStatus);
                            })
                        },
                        providerData: function(cb) {

                            UserAreaMappingCollection.aggregate({
                                    $match: {
                                        "companyId": ObjectId(params.companyId),
                                        "userId": {
                                            $in: userIds
                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "Providers",
                                        "localField": "areaId",
                                        "foreignField": "blockId",
                                        "as": "Providers"
                                    }
                                }, {
                                    $unwind: {
                                        "path": "$Providers",
                                        "preserveNullAndEmptyArrays": false
                                    }
                                }, {
                                    $project: {
                                        userId: "$userId",
                                        blockId: "$Providers.blockId",
                                        blockName: "$Providers.blockName",
                                        providerId: "$Providers._id",
                                        providerName: "$Providers.providerName",
                                        providerType: "$Providers.providerType",
                                        updatedAt: "$Providers.updatedAt",
                                        createdAt: "$Providers.createdAt",
                                        deletedAt: "$Providers.deletedAt",
                                        approvedDate: "$Providers.approvedDate",
                                        finalAppDate: "$Providers.finalAppDate",
                                        mgrDelDate: "$Providers.mgrDelDate",
                                        finalDelDate: "$Providers.finalDelDate",
                                        status: "$Providers.status",
                                        appStatus: "$Providers.appStatus",
                                        delStatus: "$Providers.delStatus",
                                        areaStatus: "$status",
                                        areaAppStatus: "$appStatus",
                                        areaDelStatus: "$delStatus",
                                        category : "$Providers.category"
                                    }
                                }, {
                                    $group: {
                                        _id: {
                                            userId: "$userId",
                                            providerType: "$providerType"
                                        },
                                        total: {
                                            $sum: {
                                                $cond: [{
                                                    $and: [{
                                                            "$eq": ["$areaStatus", true]
                                                        }, {
                                                            "$eq": ["$areaAppStatus", "approved"]
                                                        }, {
                                                            "$eq": ["$status", true]
                                                        },
                                                        {
                                                            "$eq": ["$appStatus", "approved"]
                                                        }
                                                    ],
                                                }, 1, 0]
                                            }
                                        },
                                        totalMEF: {
                                            $sum: {
                                                $cond: [{
                                                    $and: [{
                                                            "$eq": ["$areaStatus", true]
                                                        }, {
                                                            "$eq": ["$areaAppStatus", "approved"]
                                                        }, {
                                                            "$eq": ["$status", true]
                                                        },{
                                                            "$eq": ["$category", "MEF"]
                                                        },{
                                                            "$eq": ["$appStatus", "approved"]
                                                        }
                                                    ],
                                                }, 1, 0]
                                            }
                                        },
                                        totalNONMEF: {
                                            $sum: {
                                                $cond: [{
                                                    $and: [{
                                                            "$eq": ["$areaStatus", true]
                                                        }, {
                                                            "$eq": ["$areaAppStatus", "approved"]
                                                        }, {
                                                            "$eq": ["$status", true]
                                                        },{
                                                            "$eq": ["$category","NON-MEF"]
                                                        },{
                                                            "$eq": ["$appStatus", "approved"]
                                                        }
                                                    ],
                                                }, 1, 0]
                                            }
                                        },
                                        total1: {
												$sum: {
													$cond: [{
														$and: [{
															$lte: ["$finalAppDate", new Date(toDate)]
																}, {
																$eq: ["$appStatus", "approved"]
																	}, {
																$eq: ["$finalDelDate", new Date("1900-01-01T00:00:00.000Z")]
																		}]
																		}, 1, 0]
																		}
												},
												unlistedDoc: {    // 08-08-2019 PK Sir and Preeti has added this condition  mutually because we have added these basis on these sencerio like: 1. case of unlisted doctor which is not approved ywt, 2- if doctor is deleted after selected month or between selected month, 3- or total active doctor till selected date
                                                  $sum: {
                                                      $cond: [{
                                                          $and: [{
                                                              $lte: ["$createdAt", new Date(toDate)]
                                                          },{
                                                             
                                                          }, {
                                                              $eq: ["$appStatus", "unlisted"]
                                                          }]
                                                      }, 1, 0]
                                                  }
                                              },
											  delDoc: {
                                                  $sum: {
                                                      $cond: [{
                                                          $and: [{
                                                              $gte: ["$finalDelDate", new Date(fromDate)]
                                                          },{
                                                             $lte: ["$finalDelDate", new Date(toDate)]
                                                          }]
                                                      }, 1, 0]
                                                  }
                                              }
                                    }
                                },{
									$project:{
											_id : 1,
                                            total : 1,
                                            totalMEF : 1,
                                            totalNONMEF : 1,
											total1 : {$add : ["$total1","$unlistedDoc","$delDoc"]}
										}
									
								}
								, {
                                    $project: {
                                        _id: 0,
                                        userId: "$_id.userId",
                                        providerType: "$_id.providerType",
                                        total: "$total",
                                        totalMEF : "$totalMEF",
                                        totalNONMEF : "$totalNONMEF",
                                        totalActiveProvider: "$total1"

                                    }
                                },
                                function(err, providerInfo) {
                                    cb(false, providerInfo);
                                });

                        }
                    },
                    function(err, result) {
                        console.log("result : ");
                        console.log(result);
                        let finalResult = [];
                        let dcrData = result.dcrData;
                        let tpData = result.tpData;
                        let providerData = result.providerData;

                        for (let i = 0; i < users.length; i++) {

                            let providerFilterObj = providerData.filter(function(obj) {
                                return obj.userId.toString() == users[i].userId.toString();
                            });

                            if (providerFilterObj.length > 0) {
                                users[i].totalChemists = 0;
                                users[i].totalActiveVendorOnSelectedTimePeriod = 0;
                                for (let providers of providerFilterObj) {

                                    if (providers.providerType === 'Drug' || providers.providerType === 'Stockist') {
                                        users[i].totalChemists = users[i].totalChemists + providers.total;
                                        users[i].totalActiveVendorOnSelectedTimePeriod = users[i].totalActiveVendorOnSelectedTimePeriod + providers.totalActiveProvider;

                                    } else if (providers.providerType === 'RMP') {
                                        users[i].totalDoctors = providers.total;
                                        users[i].totalDoctorsMEF = providers.totalMEF;
                                        console.log("totalDoctorsMEF : ");
                                        console.log(users[i].totalDoctorsMEF);
                                        users[i].totalDoctorsNONMEF = providers.totalNONMEF;
                                        console.log("totalDoctorsNONMEF : ");
                                        console.log(users[i].totalDoctorsNONMEF);
                                        users[i].totalActiveDoctorOnSelectedTimePeriod = providers.totalActiveProvider;
                                    }
                                }
                            } else {
                                users[i].totalChemists = 0;
                                users[i].totalDoctors = 0;
                                users[i].totalDoctorsMEF =0;
                                users[i].totalDoctorsNONMEF=0;
                                users[i].untouchedProvider = 0;
                                users[i].totalActiveDoctorOnSelectedTimePeriod = 0;
                                users[i].totalActiveVendorOnSelectedTimePeriod = 0;

                            }

                            let dcrFilterObj = dcrData.filter(function(obj) {
                                return obj.userId.toString() == users[i].userId.toString()
                            });
                            if (dcrFilterObj.length > 0) {
                                users[i].workingDCR = dcrFilterObj[0].workingDCR;
                                users[i].otherDCR = dcrFilterObj[0].otherDCR;
                                users[i].doctorCall = dcrFilterObj[0].doctorCall;
                                users[i].vendorCall = dcrFilterObj[0].vendorCall;
                                users[i].listedDoctorCall = dcrFilterObj[0].listedDoctorCall;
                                users[i].listedVendorCall = dcrFilterObj[0].listedVendorCall;
                                users[i].drCallAvg = dcrFilterObj[0].drCallAvg.toFixed(2);
                                users[i].venCallAvg = dcrFilterObj[0].venCallAvg.toFixed(2);

                                users[i].listedDoctorCallAvg = dcrFilterObj[0].listedDoctorCallAvg.toFixed(2);
                                users[i].listedVendorCallAvg = dcrFilterObj[0].listedVendorCallAvg.toFixed(2);
                                users[i].totalPendingDCR = numberOfDay - dcrFilterObj[0].totalDCR;

                                //----No Use------
                                // if (users[i].totalDoctors > 0) {
                                //     users[i].listedDoctorCoverage = ((users[i].listedDoctorCall * 100) / users[i].totalDoctors).toFixed(2);
                                // } else {
                                //     users[i].listedDoctorCoverage = 0.00;
                                // }
                                //----------No Use---------------

                                if (users[i].totalActiveDoctorOnSelectedTimePeriod > 0) {
                                    users[i].listedActiveDoctorCoverage = ((users[i].listedDoctorCall * 100) / users[i].totalActiveDoctorOnSelectedTimePeriod).toFixed(2);
                                } else {
                                    users[i].listedActiveDoctorCoverage = 0.00;
                                }

                                if (users[i].totalActiveVendorOnSelectedTimePeriod > 0) {
                                    users[i].listedVendorCoverage = ((users[i].listedVendorCall * 100) / users[i].totalActiveVendorOnSelectedTimePeriod).toFixed(2);
                                } else {
                                    users[i].listedVendorCoverage = 0.00;
                                }


                                users[i].incompleteDCR = dcrFilterObj[0].incompleteDCR;
                                let nuumberOfLocked = users[i].lockingPeriod - numberOfDay;

                            } else {
                                users[i].totalDCR = 0;
                                users[i].workingDCR = 0;
                                users[i].otherDCR = 0;
                                users[i].doctorCall = 0;
                                users[i].vendorCall = 0;
                                users[i].listedDoctorCall = 0;
                                users[i].listedVendorCall = 0;
                                users[i].drCallAvg = 0.00;
                                users[i].venCallAvg = 0.00;
                                users[i].listedDoctorCallAvg = 0.00;
                                //users[i].listedDoctorCoverage = 0.00;
                                users[i].totalPendingDCR = numberOfDay;
                                users[i].listedVendorCallAvg = 0.00;
                                users[i].listedVendorCoverage = 0.00;
                                users[i].listedActiveDoctorCoverage = 0.00;
                                users[i].incompleteDCR = 0;
                            }

                            let tpFilterObj = tpData.filter(function(obj) {
                                return obj.userId.toString() == users[i].userId.toString();
                            });
                            if (tpFilterObj.length > 0) {
                                users[i].submitted = tpFilterObj[0].submitted;
                                users[i].approved = tpFilterObj[0].approved;
                            } else {
                                users[i].submitted = "";
                                users[i].approved = "";
                            }
                        }
                        return cb(false, users);
                    })
            }

        })

    }
	
    Dcrmaster.remoteMethod(
        'getManagerTeamPerformance', {
            description: 'Getting monthy & Yearly Manager worked with Report',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );*/
	
	  Dcrmaster.getManagerTeamPerformance = function (params, cb) {

        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var UserAreaMappingCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.UserAreaMapping.modelName);
        let year = params.year;
        let month = params.month;

        let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
        let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");

        let currentDate = moment.utc().format("YYYY-MM-DDT00:00:00.000Z");
        let numberOfDay = 0;
        let numberOfMonth = moment.utc().month() + 1;
        let numberOfYear = moment.utc().year();
        if (numberOfMonth === month && numberOfYear === year) {
            numberOfDay = moment.utc().date();
        } else {
            numberOfDay = moment.utc(year + "-" + month + "-01").endOf("month").date();
        }
        let obj = {
            companyId: params.companyId,
            status: params.status,
            type: params.type,
            supervisorId: params.supervisorId
        }

        if (params.isDivisionExist) {
            obj.isDivisionExist = params.isDivisionExist
            obj.division = params.division
        }

        let dcrDataGroup = {

            _id: {
                "submitBy": "$submitBy"
            },
            totalDCR: {
                $addToSet: "$dcrId"
            },
            workingDCR: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$DCRStatus", 1]
                        }]
                    }, "$dcrId", "$abc"]
                }
            },
            otherDCR: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$DCRStatus", 1]
                        }]
                    }, "$dcrId", "$abc"]
                }
            },
            incompleteDCR: {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$workingStatus", "Working"]
                        }, {
                            $eq: ["$arrayIndex", null]
                        }]
                    }, 1, 0]
                }
            },
            uniqueDoctorVisit: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", 0]
                }
            },
            nonMefDoctorCall: {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$mefAnswers","Non-MEF"]
                        }]
                    }, 1, 0]
                }
            },
             MefDoctorCall: {
                $sum: {
                    $cond: [{
                        $and: [{
                              $ne: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, 1, 0]
                }
            },


        }
        let dcrDataProject = {
            _id: 0,
            userId: "$_id.submitBy",
            doctorCall: 1,
            vendorCall: 1,
            nonMefDoctorCall:1,
            MefDoctorCall:1,
            totalDCR: {
                $size: "$totalDCR"
            },
            workingDCR: {
                $size: "$workingDCR"
            },
            otherDCR: {
                $size: "$otherDCR"
            },
            listedDoctorCall: {
                $size: "$listedDoctorCall"
            },
            listedVendorCall: {
                $size: "$listedVendorCall"
            },
            incompleteDCR: 1,
          
        }
        //-----Dynamic Group and Project For Doctors--------------------
        if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == true) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }
            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedNonMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.mefDrCallAvg ={
                $divide: ["$MefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.nonMefDrCallAvg ={
                $divide: ["$nonMefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedNonMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedNonMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }



        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataGroup.listedMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedNonMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.mefDrCallAvg ={
                $divide: ["$MefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.nonMefDrCallAvg ={
                $divide: ["$nonMefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedNonMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedNonMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataGroup.listedMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedNonMEFDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$mefAnswers", "Non-MEF"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.mefDrCallAvg ={
                $divide: ["$MefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.nonMefDrCallAvg ={
                $divide: ["$nonMefDoctorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.listedNonMefDoctorCallAvg = {
                $divide: [{
                    $size: "$listedNonMEFDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == true) {
            dcrDataGroup.doctorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgTrue = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        }
        dcrDataGroup.listedMEFDoctorCall = {
            $addToSet: {
                $cond: [{
                    $and: [{
                        $ne: ["$mefAnswers", "Non-MEF"]
                    }]
                }, "$providerId", "$abc"]
            }
        }

        dcrDataGroup.listedNonMEFDoctorCall = {
            $addToSet: {
                $cond: [{
                    $and: [{
                        $eq: ["$mefAnswers", "Non-MEF"]
                    }]
                }, "$providerId", "$abc"]
            }
        }

        dcrDataProject.mefDrCallAvg ={
            $divide: ["$MefDoctorCall", {
                $cond: {
                    if: {
                        $gte: [{ $size: "$workingDCR" }, 1]
                    },
                    then: { $size: "$workingDCR" },
                    else: 1 // used 1 for divided by zero
                }
            }]
        }
        dcrDataProject.nonMefDrCallAvg ={
            $divide: ["$nonMefDoctorCall", {
                $cond: {
                    if: {
                        $gte: [{ $size: "$workingDCR" }, 1]
                    },
                    then: { $size: "$workingDCR" },
                    else: 1 // used 1 for divided by zero
                }
            }]
        }
        dcrDataProject.listedMefDoctorCallAvg = {
            $divide: [{
                $size: "$listedMEFDoctorCall"
            }, {
                $cond: {
                    if: {
                        $gte: [{ $size: "$workingDCR" }, 1]
                    },
                    then: { $size: "$workingDCR" },
                    else: 1 // used 1 for divided by zero
                }
            }]
        }
        dcrDataProject.listedNonMefDoctorCallAvg = {
            $divide: [{
                $size: "$listedNonMEFDoctorCall"
            }, {
                $cond: {
                    if: {
                        $gte: [{ $size: "$workingDCR" }, 1]
                    },
                    then: { $size: "$workingDCR" },
                    else: 1 // used 1 for divided by zero
                }
            }]
        }
        //-----Dynamic Group and Project For Vendor--------------------
        if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == false) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCall", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == false) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, 1, 0]
                }
            }

            dcrDataGroup.vendorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.vendorCall = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.vendorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $ne: ["$providerStatus", "unlisted"]
                        },
                        {
                            $or: [{
                                $eq: ["$providerType", "Drug"]
                            }, {
                                $eq: ["$providerType", "Stockist"]
                            }]
                        }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgTrue = {

                $addToSet: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedVendorCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$workingDCR" }, 1]
                        },
                        then: { $size: "$workingDCR" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        }
        Dcrmaster.app.models.Hierarchy.getManagerHierarchy(obj, function (err, users) {
            if (users.length > 0) {
                let userIds = [];
                for (let user of users) {
                    userIds.push(ObjectId(user.userId))

                }
                async.parallel({
                    dcrData: function (cb) {
                        let match ={
                            companyId: ObjectId(params.companyId),
                            submitBy: {
                                $in: userIds
                            },
                            dcrDate: {
                                $gte: new Date(fromDate),
                                $lte: new Date(toDate)
                            }
                        }
                        
                        dcrCollection.aggregate({
                            
                            $match: {
                                companyId: ObjectId(params.companyId),
                                submitBy: {
                                    $in: userIds
                                },
                                dcrDate: {
                                    $gte: new Date(fromDate),
                                    $lte: new Date(toDate)
                                }
                            }

                        }, {
                                $lookup: {

                                    "from": "DCRProviderVisitDetails",
                                    "localField": "_id",
                                    "foreignField": "dcrId",
                                    "as": "dcrProviders"
                                }
                            }, {
                                $unwind: {
                                    path: "$dcrProviders",
                                    preserveNullAndEmptyArrays: true,
                                    includeArrayIndex: "arrayIndex"
                                }
                            }, {
                                $project: {

                                    dcrId: "$_id",
                                    dcrDate: "$dcrDate",
                                    workingStatus: 1,
                                    providerType: "$dcrProviders.providerType",
                                    providerId: "$dcrProviders.providerId",
                                    submitBy: 1,
                                    arrayIndex: 1,
                                    providerStatus: "$dcrProviders.providerStatus",
                                    DCRStatus: 1,
                                    mefAnswers : { $cond: { if:"$dcrProviders.mefAnswers", then: "$dcrProviders.mefAnswers", else: "Non-MEF" } } // condition added for vygon company only by preeti

                                }
                            }, {
                                $group: dcrDataGroup
                            }, {
                                $project: dcrDataProject
                            }, {
                                cursor: {
                                    batchSize: 50
                                },

                                allowDiskUse: true
                            }, function (err, result) {
                                cb(false, result)
                            });
                    },
                    tpData: function (cb) {
                        let obj = {
                            "type": "Employee Wise",
                            "status": [true, false],
                            "companyId": params.companyId,
                            "userIds": userIds,
                            "month": month,
                            "year": year
                        }
                        Dcrmaster.app.models.TourProgram.getTPStatus(obj, function (err, tpStatus) {
                            cb(false, tpStatus);
                        })
                    },
                    providerData: function (cb) {

                        UserAreaMappingCollection.aggregate({
                            $match: {
                                "companyId": ObjectId(params.companyId),
                                "userId": {
                                    $in: userIds
                                }
                            }
                        }, {
                                $lookup: {
                                    "from": "Providers",
                                    "localField": "areaId",
                                    "foreignField": "blockId",
                                    "as": "Providers"
                                }
                            }, {
                                $unwind: {
                                    "path": "$Providers",
                                    "preserveNullAndEmptyArrays": false
                                }
                            }, {
                                $project: {
                                    userId: "$userId",
                                    blockId: "$Providers.blockId",
                                    blockName: "$Providers.blockName",
                                    providerId: "$Providers._id",
                                    providerName: "$Providers.providerName",
                                    providerType: "$Providers.providerType",
                                    updatedAt: "$Providers.updatedAt",
                                    createdAt: "$Providers.createdAt",
                                    deletedAt: "$Providers.deletedAt",
                                    approvedDate: "$Providers.approvedDate",
                                    finalAppDate: "$Providers.finalAppDate",
                                    mgrDelDate: "$Providers.mgrDelDate",
                                    finalDelDate: "$Providers.finalDelDate",
                                    status: "$Providers.status",
                                    appStatus: "$Providers.appStatus",
                                    delStatus: "$Providers.delStatus",
                                    areaStatus: "$status",
                                    areaAppStatus: "$appStatus",
                                    areaDelStatus: "$delStatus",
                                    category: "$Providers.category"
                                }
                            }, {
                                $group: {
                                    _id: {
                                        userId: "$userId",
                                        providerType: "$providerType"
                                    },
                                    total: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    "$eq": ["$areaStatus", true]
                                                }, {
                                                    "$eq": ["$areaAppStatus", "approved"]
                                                }, {
                                                    "$eq": ["$status", true]
                                                },
                                                {
                                                    "$eq": ["$appStatus", "approved"]
                                                }
                                                ],
                                            }, 1, 0]
                                        }
                                    },
                                    totalMEF: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    "$eq": ["$areaStatus", true]
                                                }, {
                                                    "$eq": ["$areaAppStatus", "approved"]
                                                }, {
                                                    "$eq": ["$status", true]
                                                }, {
                                                    "$eq": ["$category", "MEF"]
                                                }, {
                                                    "$eq": ["$appStatus", "approved"]
                                                }
                                                ],
                                            }, 1, 0]
                                        }
                                    },
                                    totalNONMEF: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    "$eq": ["$areaStatus", true]
                                                }, {
                                                    "$eq": ["$areaAppStatus", "approved"]
                                                }, {
                                                    "$eq": ["$status", true]
                                                }, {
                                                    "$eq": ["$category", "NON-MEF"]
                                                }, {
                                                    "$eq": ["$appStatus", "approved"]
                                                }
                                                ],
                                            }, 1, 0]
                                        }
                                    },
                                    total1: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $lte: ["$finalAppDate", new Date(toDate)]
                                                }, {
                                                    $eq: ["$appStatus", "approved"]
                                                }, {
                                                    $eq: ["$finalDelDate", new Date("1900-01-01T00:00:00.000Z")]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedDoc: {    // 08-08-2019 PK Sir and Preeti has added this condition  mutually because we have added these basis on these sencerio like: 1. case of unlisted doctor which is not approved ywt, 2- if doctor is deleted after selected month or between selected month, 3- or total active doctor till selected date
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $lte: ["$createdAt", new Date(toDate)]
                                                }, {

                                                }, {
                                                    $eq: ["$appStatus", "unlisted"]
                                                }]
                                            }, 1, 0]
                                        }
                                    },
                                    delDoc: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $gte: ["$finalDelDate", new Date(fromDate)]
                                                }, {
                                                    $lte: ["$finalDelDate", new Date(toDate)]
                                                }]
                                            }, 1, 0]
                                        }
                                    }
                                }
                            }, {
                                $project: {
                                    _id: 1,
                                    total: 1,
                                    totalMEF: 1,
                                    totalNONMEF: 1,
                                    total1: { $add: ["$total1", "$unlistedDoc", "$delDoc"] }
                                }

                            }
                            , {
                                $project: {
                                    _id: 0,
                                    userId: "$_id.userId",
                                    providerType: "$_id.providerType",
                                    total: "$total",
                                    totalMEF: "$totalMEF",
                                    totalNONMEF: "$totalNONMEF",
                                    totalActiveProvider: "$total1"

                                }
                            },
                            function (err, providerInfo) {
                                cb(false, providerInfo);
                            });

                    }
                },
                    function (err, result) {
                        let finalResult = [];
                        let dcrData = result.dcrData;
                        let tpData = result.tpData;
                        let providerData = result.providerData;
                         console.log("dcrData=",dcrData);
                         
                        for (let i = 0; i < users.length; i++) {

                            let providerFilterObj = providerData.filter(function (obj) {
                                return obj.userId.toString() == users[i].userId.toString();
                            });

                            if (providerFilterObj.length > 0) {
                                users[i].totalChemists = 0;
                                users[i].totalActiveVendorOnSelectedTimePeriod = 0;
                                for (let providers of providerFilterObj) {

                                    if (providers.providerType === 'Drug' || providers.providerType === 'Stockist') {
                                        users[i].totalChemists = users[i].totalChemists + providers.total;
                                        users[i].totalActiveVendorOnSelectedTimePeriod = users[i].totalActiveVendorOnSelectedTimePeriod + providers.totalActiveProvider;

                                    } else if (providers.providerType === 'RMP') {
                                        users[i].totalDoctors = providers.total;
                                        users[i].totalDoctorsMEF = providers.totalMEF;
                                        users[i].totalDoctorsNONMEF = providers.totalNONMEF;
                                        users[i].totalActiveDoctorOnSelectedTimePeriod = providers.totalActiveProvider;
                                    }
                                }
                            } else {
                                users[i].totalChemists = 0;
                                users[i].totalDoctors = 0;
                                users[i].totalDoctorsMEF = 0;
                                users[i].totalDoctorsNONMEF = 0;
                                users[i].untouchedProvider = 0;
                                users[i].totalActiveDoctorOnSelectedTimePeriod = 0;
                                users[i].totalActiveVendorOnSelectedTimePeriod = 0;

                            }

                            let dcrFilterObj = dcrData.filter(function (obj) {
                                return obj.userId.toString() == users[i].userId.toString()
                            });
                            if (dcrFilterObj.length > 0) {
                                users[i].workingDCR = dcrFilterObj[0].workingDCR;
                                users[i].otherDCR = dcrFilterObj[0].otherDCR;
                                users[i].doctorCall = dcrFilterObj[0].doctorCall;
                                users[i].vendorCall = dcrFilterObj[0].vendorCall;
                                users[i].listedDoctorCall = dcrFilterObj[0].listedDoctorCall;
                                users[i].listedVendorCall = dcrFilterObj[0].listedVendorCall;
                                users[i].drCallAvg = dcrFilterObj[0].drCallAvg.toFixed(2);
                                users[i].venCallAvg = dcrFilterObj[0].venCallAvg.toFixed(2);

                                users[i].nonMefDoctorCall = dcrFilterObj[0].nonMefDoctorCall;
                                users[i].MefDoctorCall = dcrFilterObj[0].MefDoctorCall;
                                users[i].mefDrCallAvg = dcrFilterObj[0].mefDrCallAvg.toFixed(2);
                                users[i].nonMefDrCallAvg = dcrFilterObj[0].nonMefDrCallAvg.toFixed(2);
                                users[i].listedMefDoctorCallAvg = dcrFilterObj[0].listedMefDoctorCallAvg.toFixed(2);
                                users[i].listedNonMefDoctorCallAvg = dcrFilterObj[0].listedNonMefDoctorCallAvg.toFixed(2);

                                users[i].listedDoctorCallAvg = dcrFilterObj[0].listedDoctorCallAvg.toFixed(2);
                                users[i].listedVendorCallAvg = dcrFilterObj[0].listedVendorCallAvg.toFixed(2);
                                users[i].totalPendingDCR = numberOfDay - dcrFilterObj[0].totalDCR;

                                if (users[i].totalActiveDoctorOnSelectedTimePeriod > 0) {
                                    users[i].listedActiveDoctorCoverage = ((users[i].listedDoctorCall * 100) / users[i].totalActiveDoctorOnSelectedTimePeriod).toFixed(2);
                                } else {
                                    users[i].listedActiveDoctorCoverage = 0.00;
                                }

                                if (users[i].totalActiveVendorOnSelectedTimePeriod > 0) {
                                    users[i].listedVendorCoverage = ((users[i].listedVendorCall * 100) / users[i].totalActiveVendorOnSelectedTimePeriod).toFixed(2);
                                } else {
                                    users[i].listedVendorCoverage = 0.00;
                                }


                                users[i].incompleteDCR = dcrFilterObj[0].incompleteDCR;
                                let nuumberOfLocked = users[i].lockingPeriod - numberOfDay;

                            } else {
                                users[i].totalDCR = 0;
                                users[i].workingDCR = 0;
                                users[i].otherDCR = 0;
                                users[i].doctorCall = 0;
                                users[i].vendorCall = 0;
                                users[i].listedDoctorCall = 0;
                                users[i].listedVendorCall = 0;
                                users[i].drCallAvg = 0.00;
                                users[i].venCallAvg = 0.00;
                                users[i].listedDoctorCallAvg = 0.00;
                                //users[i].listedDoctorCoverage = 0.00;
                                users[i].totalPendingDCR = numberOfDay;
                                users[i].listedVendorCallAvg = 0.00;
                                users[i].listedVendorCoverage = 0.00;
                                users[i].listedActiveDoctorCoverage = 0.00;
                                users[i].incompleteDCR = 0;
                            }

                            let tpFilterObj = tpData.filter(function (obj) {
                                return obj.userId.toString() == users[i].userId.toString();
                            });
                            if (tpFilterObj.length > 0) {
                                users[i].submitted = tpFilterObj[0].submitted;
                                users[i].approved = tpFilterObj[0].approved;
                            } else {
                                users[i].submitted = "";
                                users[i].approved = "";
                            }
                        }
                        return cb(false, users);
                    })
            }

        })

    }

    Dcrmaster.remoteMethod(
        'getManagerTeamPerformance', {
            description: 'Getting monthy & Yearly Manager worked with Report',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
	
	
	
	
	
	
    Dcrmaster.getDateWisePerformance = function(params, cb) {

        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var matchFilter = {};
        if (params.rL == 0 || params.rL == 1) {
            if (params.rL == 0) {
                matchFilter = {
                    companyId: ObjectId(params.companyId),
                    dcrDate: {
                        $gte: new Date(params.fromDate),
                        $lte: new Date(params.toDate)
                    }

                }
            } else if (params.rL == 1) {
                matchFilter = {

                    companyId: ObjectId(params.companyId),
                    dcrDate: {
                        $gte: new Date(params.fromDate),
                        $lte: new Date(params.toDate)
                    },
                    submitBy: ObjectId(params.supervisorId)
                }
            }
            //console.log(matchFilter)
            dcrCollection.aggregate(
                // Stage 1
                {
                    $match: matchFilter
                },
                // Stage 2
                {
                    $lookup: {
                        "from": "DCRProviderVisitDetails",
                        "localField": "_id",
                        "foreignField": "dcrId",
                        "as": "dcrProviders"
                    }
                },

                // Stage 3
                {
                    $unwind: {
                        path: "$dcrProviders",
                        preserveNullAndEmptyArrays: true,
                        includeArrayIndex: "arrayIndex"
                    }
                },

                // Stage 4
                {
                    $project: {

                        dcrId: "$_id",
                        dcrDate: "$dcrDate",
                        workingStatus: 1,
                        providerType: "$dcrProviders.providerType",
                        providerId: "$dcrProviders.providerId",
                        submitBy: 1,
                        arrayIndex: 1,
                        productPob: "$dcrProviders.productPob"
                    }
                },

                // Stage 5
                {
                    $group: {
                        _id: {
                            "dcrDate": "$dcrDate"
                        },
                        totalDCR: {
                            $addToSet: "$dcrId"
                        },
                        workingDCR: {
                            $addToSet: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$workingStatus", "Working"]
                                    }]
                                }, "$dcrId", 0]
                            }
                        },
                        holidayDCR: {
                            $addToSet: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$workingStatus", "Holiday"]
                                    }]
                                }, "$dcrId", 0]
                            }
                        },
                        meetingDCR: {
                            $addToSet: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$workingStatus", "Meeting"]
                                    }]
                                }, "$dcrId", 0]
                            }
                        },
                        leaveDCR: {
                            $addToSet: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$workingStatus", "Leave"]
                                    }]
                                }, "$dcrId", 0]
                            }
                        },
                        doctorCall: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$providerType", "RMP"]
                                    }]
                                }, 1, 0]
                            }
                        },
                        vendorCall: {
                            $sum: {
                                $cond: [{
                                    $or: [{
                                        $eq: ["$providerType", "Drug"]
                                    }, {
                                        $eq: ["$providerType", "Stockist"]
                                    }]
                                }, 1, 0]
                            }
                        },
                        listedDoctorCall: {
                            $addToSet: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$providerType", "RMP"]
                                    }]
                                }, "$providerId", 0]
                            }
                        },
                        listedVendorCall: {
                            $addToSet: {
                                $cond: [{
                                    $or: [{
                                        $eq: ["$providerType", "Drug"]
                                    }, {
                                        $eq: ["$providerType", "Stockist"]
                                    }]
                                }, "$providerId", 0]
                            }
                        },
                        incompleteDCR: {
                            $sum: {
                                $cond: [{
                                    $and: [{
                                        $eq: ["$workingStatus", "Working"]
                                    }, {
                                        $eq: ["$arrayIndex", null]
                                    }]
                                }, 1, 0]
                            }
                        },
                        productPob: {
                            $sum: "$productPob"
                        }
                    }
                },

                // Stage 6
                {
                    $project: {
                        _id: 0,
                        start: { $dateToString: { format: "%Y-%m-%d", date: "$_id.dcrDate" } },
                        doctorCall: 1,
                        vendorCall: 1,
                        totalDCR: {
                            $size: "$totalDCR"
                        },
                        workingDCR: {
                            $size: {
                                $filter: {
                                    input: "$workingDCR",
                                    as: "item",
                                    cond: {
                                        $ne: ["$$item", 0]
                                    }
                                }
                            }
                        },
                        otherDCR: {
                            $add: [{
                                $size: {
                                    $filter: {
                                        input: "$leaveDCR",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            }, {
                                $size: {
                                    $filter: {
                                        input: "$meetingDCR",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            }, {
                                $size: {
                                    $filter: {
                                        input: "$holidayDCR",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            }]
                        },
                        drCallAvg: {
                            $cond: [{
                                $eq: [{
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, 0]
                            }, 0, {
                                $divide: ["$doctorCall", {
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }]
                            }]

                        },
                        venCallAvg: {
                            $cond: [{
                                $eq: [{
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, 0]
                            }, 0, {
                                $divide: ["$vendorCall", {
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }]
                            }]

                        },
                        listedDoctorCall: {
                            $size: {
                                $filter: {
                                    input: "$listedDoctorCall",
                                    as: "item",
                                    cond: {
                                        $ne: ["$$item", 0]
                                    }
                                }
                            }
                        },
                        listedVendorCall: {
                            $size: {
                                $filter: {
                                    input: "$listedVendorCall",
                                    as: "item",
                                    cond: {
                                        $ne: ["$$item", 0]
                                    }
                                }
                            }
                        },
                        listedDoctorCallAvg: {
                            $cond: [{
                                $eq: [{
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, 0]
                            }, 0, {
                                $divide: [{
                                    $size: {
                                        $filter: {
                                            input: "$listedDoctorCall",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, {
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }]
                            }]

                        },
                        listedVendorCallAvg: {
                            $cond: [{
                                $eq: [{
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, 0]
                            }, 0, {
                                $divide: [{
                                    $size: {
                                        $filter: {
                                            input: "$listedVendorCall",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, {
                                    $size: {
                                        $filter: {
                                            input: "$workingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }]
                            }]

                        },
                        incompleteDCR: 1,
                        productPob: 1
                    }
                },

                // Options
                {
                    cursor: {
                        batchSize: 50
                    },

                    allowDiskUse: true
                },
                function(err, result) {
                    if (err) {
                        return cb(err)
                    }
                    return cb(false, result)

                });
        } else {
            let obj = {
                companyId: params.companyId,
                status: [true],
                type: params.type,
                supervisorId: params.supervisorId
            }
            Dcrmaster.app.models.Hierarchy.getManagerHierarchy(obj, function(err, users) {
                let userIds = [];
                if (users.length > 0) {
                    for (let user of users) {
                        userIds.push(ObjectId(user.userId))

                    }
                }
                dcrCollection.aggregate(
                    // Stage 1
                    {
                        $match: {
                            companyId: ObjectId(params.companyId),
                            submitBy: {
                                $in: userIds
                            },
                            dcrDate: {
                                $gte: new Date(params.fromDate),
                                $lte: new Date(params.toDate)
                            }
                        }
                    },
                    // Stage 2
                    {
                        $lookup: {
                            "from": "DCRProviderVisitDetails",
                            "localField": "_id",
                            "foreignField": "dcrId",
                            "as": "dcrProviders"
                        }
                    },

                    // Stage 3
                    {
                        $unwind: {
                            path: "$dcrProviders",
                            preserveNullAndEmptyArrays: true,
                            includeArrayIndex: "arrayIndex"
                        }
                    },

                    // Stage 4
                    {
                        $project: {

                            dcrId: "$_id",
                            dcrDate: "$dcrDate",
                            workingStatus: 1,
                            providerType: "$dcrProviders.providerType",
                            providerId: "$dcrProviders.providerId",
                            submitBy: 1,
                            arrayIndex: 1,
                            productPob: 1
                        }
                    },

                    // Stage 5
                    {
                        $group: {
                            _id: {
                                "dcrDate": "$dcrDate"
                            },
                            totalDCR: {
                                $addToSet: "$dcrId"
                            },
                            workingDCR: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$workingStatus", "Working"]
                                        }]
                                    }, "$dcrId", 0]
                                }
                            },
                            holidayDCR: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$workingStatus", "Holiday"]
                                        }]
                                    }, "$dcrId", 0]
                                }
                            },
                            meetingDCR: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$workingStatus", "Meeting"]
                                        }]
                                    }, "$dcrId", 0]
                                }
                            },
                            leaveDCR: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$workingStatus", "Leave"]
                                        }]
                                    }, "$dcrId", 0]
                                }
                            },
                            doctorCall: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$providerType", "RMP"]
                                        }]
                                    }, 1, 0]
                                }
                            },
                            vendorCall: {
                                $sum: {
                                    $cond: [{
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }, 1, 0]
                                }
                            },
                            listedDoctorCall: {
                                $addToSet: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$providerType", "RMP"]
                                        }]
                                    }, "$providerId", 0]
                                }
                            },
                            listedVendorCall: {
                                $addToSet: {
                                    $cond: [{
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }, "$providerId", 0]
                                }
                            },
                            incompleteDCR: {
                                $sum: {
                                    $cond: [{
                                        $and: [{
                                            $eq: ["$workingStatus", "Working"]
                                        }, {
                                            $eq: ["$arrayIndex", null]
                                        }]
                                    }, 1, 0]
                                }
                            },
                            productPob: {
                                $sum: "$productPob"
                            }
                        }
                    },

                    // Stage 6
                    {
                        $project: {

                            _id: 0,
                            start: { $dateToString: { format: "%Y-%m-%d", date: "$_id.dcrDate" } },
                            doctorCall: 1,
                            vendorCall: 1,
                            totalDCR: {
                                $size: "$totalDCR"
                            },
                            workingDCR: {
                                $size: {
                                    $filter: {
                                        input: "$workingDCR",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            },
                            otherDCR: {
                                $add: [{
                                    $size: {
                                        $filter: {
                                            input: "$leaveDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, {
                                    $size: {
                                        $filter: {
                                            input: "$meetingDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }, {
                                    $size: {
                                        $filter: {
                                            input: "$holidayDCR",
                                            as: "item",
                                            cond: {
                                                $ne: ["$$item", 0]
                                            }
                                        }
                                    }
                                }]
                            },
                            drCallAvg: {
                                $cond: [{
                                    $eq: [{
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, 0]
                                }, 0, {
                                    $divide: ["$doctorCall", {
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }]
                                }]

                            },
                            venCallAvg: {
                                $cond: [{
                                    $eq: [{
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, 0]
                                }, 0, {
                                    $divide: ["$vendorCall", {
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }]
                                }]

                            },
                            listedDoctorCall: {
                                $size: {
                                    $filter: {
                                        input: "$listedDoctorCall",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            },
                            listedVendorCall: {
                                $size: {
                                    $filter: {
                                        input: "$listedVendorCall",
                                        as: "item",
                                        cond: {
                                            $ne: ["$$item", 0]
                                        }
                                    }
                                }
                            },
                            listedDoctorCallAvg: {
                                $cond: [{
                                    $eq: [{
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, 0]
                                }, 0, {
                                    $divide: [{
                                        $size: {
                                            $filter: {
                                                input: "$listedDoctorCall",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, {
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }]
                                }]

                            },
                            listedVendorCallAvg: {
                                $cond: [{
                                    $eq: [{
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, 0]
                                }, 0, {
                                    $divide: [{
                                        $size: {
                                            $filter: {
                                                input: "$listedVendorCall",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }, {
                                        $size: {
                                            $filter: {
                                                input: "$workingDCR",
                                                as: "item",
                                                cond: {
                                                    $ne: ["$$item", 0]
                                                }
                                            }
                                        }
                                    }]
                                }]

                            },
                            incompleteDCR: 1,
                            productPob: 1
                        }
                    },

                    // Options
                    {
                        cursor: {
                            batchSize: 50
                        },

                        allowDiskUse: true
                    },
                    function(err, result) {
                        if (err) {
                            return cb(err)
                        }
                        return cb(false, result)

                    });
            });

        }
    }
    Dcrmaster.remoteMethod(
        'getDateWisePerformance', {
            description: 'Getting monthy  Date Wise worked',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    //---------------------------Praveen Kumar(25-02-2019)-----------------------

    Dcrmaster.getEmployeesAttendance = function(param, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        if (param.type == "State" || param.type == "Headquarter") {
            Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(
                param,
                function(err, userRecords) {
                    if (err) {
                        console.log(err)
                    }
                    if (userRecords.length > 0) {
                        dcrCollection.aggregate({
                                $match: {
                                    companyId: ObjectId(param.companyId),
                                    submitBy: { $in: userRecords[0].userIds },
                                    dcrDate: {
                                        $gte: new Date(param.fromDate),
                                        $lte: new Date(param.toDate)
                                    }
                                }
                            }, {
                                $group: {
                                    _id: {
                                        submitBy: "$submitBy"
                                    },
                                    dcrData: {
                                        $push: {
                                            completeDcrDate: "$dcrDate",
                                            workingStatus: "$workingStatus",
                                            completeDcrSubmissionDate: "$dcrSubmissionDate",
                                            dcrDate: { $dayOfMonth: "$dcrDate" },
                                            dcrSubmissionDate: { $dayOfMonth: "$dcrSubmissionDate" }
                                        }
                                    }
                                }
                            }, {
                                $lookup: {
                                    "from": "UserInfo",
                                    "localField": "_id.submitBy",
                                    "foreignField": "userId",
                                    "as": "userData"
                                }
                            }, {
                                $unwind: "$userData"
                            }, {
                                $lookup: {
                                    "from": "State",
                                    "localField": "userData.stateId",
                                    "foreignField": "_id",
                                    "as": "stateData"
                                }
                            }, {
                                $lookup: {
                                    "from": "District",
                                    "localField": "userData.districtId",
                                    "foreignField": "_id",
                                    "as": "districtData"
                                }
                            }, {
                                $project: {
                                    _id: 1,
                                    dcrData: 1,
									divisionName : { $arrayElemAt: ["$userData.divisionName", 0] },
                                    stateId: { $arrayElemAt: ["$stateData._id", 0] },
                                    stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                                    districtId: { $arrayElemAt: ["$districtData._id", 0] },
                                    districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                                    userName: "$userData.name"
                                }
                            }, {
                                $sort: {
                                    stateName: 1,
                                    districtName: 1,
                                    userName: 1
                                }
                            },
                            function(err, dcrResult) {
                                if (err) {
                                    console.log(err);
                                    return cb(err);
                                }
                                if (!dcrResult) {
                                    var err = new Error('no records found');
                                    err.statusCode = 404;
                                    err.code = 'NOT FOUND';
                                    return cb(err);
                                }



                                let lastDayOfMonth = getNumberOfDaysInAMonth(param.month, param.year);
                                let finalResult = [];
                                for (let i = 0; i < userRecords[0].obj.length; i++) {
                                    //console.log(userRecords[0].obj[i].userId)
                                    let dateWiseDataArray = [];

                                    let totalWorkingDCR = 0;

                                    let filterObj = dcrResult.filter(function(obj) {
                                        return obj._id.submitBy.toString() == userRecords[0].obj[i].userId.toString();
                                    });

                                    if (filterObj.length > 0) {
                                        //Dcr Is Submitted
                                        // console.log("PKKK", filterObj)
                                        for (let d = 1; d <= lastDayOfMonth; d++) {
                                            // dateWiseDataArray
                                            //console.log(filterObj[0].dcrData)
                                            let isDateSunday = false;
                                            let myDate = new Date();
                                            myDate.setFullYear(param.year);
                                            myDate.setMonth(param.month - 1);
                                            myDate.setDate(d);
                                            if (myDate.getDay() == 0) {
                                                isDateSunday = true;
                                            }


                                            let dcrFound = filterObj[0].dcrData.filter(function(obj) {
                                                //console.log(obj)
                                                return obj.dcrDate == d;
                                            });
                                            if (dcrFound.length > 0) {
                                                //console.log(dcrFound[0].completeDcrDate + "  --- " + dcrFound[0].workingStatus)
                                                //DCR Found for particular date

                                                let foundWorkingType = param.workingStatus.filter(function(obj) {
                                                    return dcrFound[0].workingStatus == obj.name;
                                                });

                                                if (foundWorkingType.length > 0) {
                                                    totalWorkingDCR++;
                                                }

                                                let foundWorkingTypeFromTotalType = param.totalWorkingTypes.filter(function(obj) {
                                                    return dcrFound[0].workingStatus == obj.name;
                                                });


                                                if (foundWorkingTypeFromTotalType.length > 0) {
                                                    dateWiseDataArray.push({
                                                        punchDate: dcrFound[0].dcrSubmissionDate,
                                                        workingStatus: dcrFound[0].workingStatus,
                                                        key: foundWorkingTypeFromTotalType[0].key,
                                                        value: foundWorkingTypeFromTotalType[0].value,
                                                        sunday: isDateSunday,
                                                        [d]: foundWorkingTypeFromTotalType[0].value
                                                    });
                                                }

                                            } else {
                                                //DCR Not Found for particular date
                                                dateWiseDataArray.push({ value: "--", sunday: isDateSunday, [d]: "--" });
                                            }
                                        }

                                    } else {
                                        //Dcr Is Not Submitted
                                        for (let d = 1; d <= lastDayOfMonth; d++) {
                                            let isDateSunday = false;
                                            let myDate = new Date();
                                            myDate.setFullYear(param.year);
                                            myDate.setMonth(param.month - 1);
                                            myDate.setDate(d);
                                            if (myDate.getDay() == 0) {
                                                isDateSunday = true;
                                            }
                                            //console.log(myDate + " -- " + isDateSunday);

                                            dateWiseDataArray.push({ value: "--", sunday: isDateSunday, [d]: "--" });
                                        }
                                    }

                                    //dateWiseDataArray.push({ totalWorkingDCR: totalWorkingDCR }); // Pushing Total Working Days

                                    finalResult.push({
										divisionName :userRecords[0].obj[i].divisionName,
                                        stateName: userRecords[0].obj[i].stateName,
                                        districtName: userRecords[0].obj[i].districtName,
                                        name: userRecords[0].obj[i].name,
                                        dateArray: dateWiseDataArray,
                                        totalWorkingDCR: totalWorkingDCR
                                    });


                                }
                                return cb(false, finalResult)

                            });
                    } else {
                        return cb(false, []);

                    }
                });
        } else {
            var userIds = [];
            for (var i = 0; i < param.userIds.length; i++) {
                userIds.push(ObjectId(param.userIds[i]));
            }

            Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                param.companyId, [true],
                userIds,
                function(err, userRecords) {
                    //console.log(userResult)
                    if (err) {
                        return err;
                    }
                    dcrCollection.aggregate({
                            $match: {
                                companyId: ObjectId(param.companyId),
                                submitBy: { $in: userRecords[0].userIds },
                                dcrDate: {
                                    $gte: new Date(param.fromDate),
                                    $lte: new Date(param.toDate)
                                }
                            }
                        }, {
                            $group: {
                                _id: {
                                    submitBy: "$submitBy"
                                },
                                dcrData: {
                                    $push: {
                                        completeDcrDate: "$dcrDate",
                                        workingStatus: "$workingStatus",
                                        completeDcrSubmissionDate: "$dcrSubmissionDate",
                                        dcrDate: { $dayOfMonth: "$dcrDate" },
                                        dcrSubmissionDate: { $dayOfMonth: "$dcrSubmissionDate" }
                                    }
                                }
                            }
                        }, {
                            $lookup: {
                                "from": "UserInfo",
                                "localField": "_id.submitBy",
                                "foreignField": "userId",
                                "as": "userData"
                            }
                        }, {
                            $unwind: "$userData"
                        }, {
                            $lookup: {
                                "from": "State",
                                "localField": "userData.stateId",
                                "foreignField": "_id",
                                "as": "stateData"
                            }
                        }, {
                            $lookup: {
                                "from": "District",
                                "localField": "userData.districtId",
                                "foreignField": "_id",
                                "as": "districtData"
                            }
                        }, {
                            $project: {
                                _id: 1,
                                dcrData: 1,
								divisionName : { $arrayElemAt: ["$userData.divisionName", 0] },
                                stateId: { $arrayElemAt: ["$stateData._id", 0] },
                                stateName: { $arrayElemAt: ["$stateData.stateName", 0] },
                                districtId: { $arrayElemAt: ["$districtData._id", 0] },
                                districtName: { $arrayElemAt: ["$districtData.districtName", 0] },
                                userName: "$userData.name"
                            }
                        }, {
                            $sort: {
                                stateName: 1,
                                districtName: 1,
                                userName: 1
                            }
                        },
                        function(err, dcrResult) {
                            if (err) {
                                console.log(err);
                                return cb(err);
                            }
                            if (!dcrResult) {
                                var err = new Error('no records found');
                                err.statusCode = 404;
                                err.code = 'NOT FOUND';
                                return cb(err);
                            }

                            let lastDayOfMonth = getNumberOfDaysInAMonth(param.month, param.year);
                            let finalResult = [];
                            for (let i = 0; i < userRecords[0].obj.length; i++) {
                                //console.log(userRecords[0].obj[i].userId)
                                let dateWiseDataArray = [];

                                let totalWorkingDCR = 0;

                                let filterObj = dcrResult.filter(function(obj) {
                                    return obj._id.submitBy.toString() == userRecords[0].obj[i].userId.toString();
                                });

                                if (filterObj.length > 0) {
                                    //Dcr Is Submitted
                                    // console.log("PKKK", filterObj)
                                    for (let d = 1; d <= lastDayOfMonth; d++) {
                                        // dateWiseDataArray
                                        //console.log(filterObj[0].dcrData)
                                        let isDateSunday = false;
                                        let myDate = new Date();
                                        myDate.setFullYear(param.year);
                                        myDate.setMonth(param.month - 1);
                                        myDate.setDate(d);
                                        if (myDate.getDay() == 0) {
                                            isDateSunday = true;
                                        }


                                        let dcrFound = filterObj[0].dcrData.filter(function(obj) {
                                            //console.log(obj)
                                            return obj.dcrDate == d;
                                        });
                                        if (dcrFound.length > 0) {
                                            //console.log(dcrFound[0].completeDcrDate + "  --- " + dcrFound[0].workingStatus)
                                            //DCR Found for particular date

                                            let foundWorkingType = param.workingStatus.filter(function(obj) {
                                                return dcrFound[0].workingStatus == obj.name;
                                            });

                                            if (foundWorkingType.length > 0) {
                                                totalWorkingDCR++;
                                            }

                                            let foundWorkingTypeFromTotalType = param.totalWorkingTypes.filter(function(obj) {
                                                return dcrFound[0].workingStatus == obj.name;
                                            });


                                            if (foundWorkingTypeFromTotalType.length > 0) {
                                                dateWiseDataArray.push({
                                                    punchDate: dcrFound[0].dcrSubmissionDate,
                                                    workingStatus: dcrFound[0].workingStatus,
                                                    key: foundWorkingTypeFromTotalType[0].key,
                                                    value: foundWorkingTypeFromTotalType[0].value,
                                                    sunday: isDateSunday,
                                                    [d]: foundWorkingTypeFromTotalType[0].value
                                                });
                                            }

                                        } else {
                                            //DCR Not Found for particular date
                                            dateWiseDataArray.push({ value: "--", sunday: isDateSunday, [d]: "--" });
                                        }
                                    }

                                } else {
                                    //Dcr Is Not Submitted
                                    for (let d = 1; d <= lastDayOfMonth; d++) {
                                        let isDateSunday = false;
                                        let myDate = new Date();
                                        myDate.setFullYear(param.year);
                                        myDate.setMonth(param.month - 1);
                                        myDate.setDate(d);
                                        if (myDate.getDay() == 0) {
                                            isDateSunday = true;
                                        }
                                        dateWiseDataArray.push({ value: "--", sunday: isDateSunday, [d]: "--" });
                                    }
                                }

                                //dateWiseDataArray.push({ totalWorkingDCR: totalWorkingDCR }); // Pushing Total Working Days

                                finalResult.push({
									division:userRecords[0].obj[i].divisionName,
                                    stateName: userRecords[0].obj[i].stateName,
                                    districtName: userRecords[0].obj[i].districtName,
                                    name: userRecords[0].obj[i].name,
                                    dateArray: dateWiseDataArray,
                                    totalWorkingDCR: totalWorkingDCR
                                });


                            }
                            // console.log("len : " + finalResult.length)
                            return cb(false, finalResult)

                        });


                });

        }
    }

    function getNumberOfDaysInAMonth(month, year) {
        return new Date(year, month, 0).getDate();
    };

    Dcrmaster.remoteMethod('getEmployeesAttendance', {
        description: "Employee's Attendance",
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }

    })


    //----------------------------------END--------------------------------------
 //---------------------get data station wise by preeti arora 11-07-2019-------------------
    Dcrmaster.stationWiseWorking = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
       let fromDate = new moment.utc(params.year + "-" + params.month + "-01").startOf('month').format("YYYY-MM-DDT00:00:00.000Z");
       let toDate = new moment.utc(params.year + "-" + params.month + "-01").endOf("month").format("YYYY-MM-DDT00:00:00.000Z");
    

        if (params.type == "State" || params.type == "Headquarter") {
            Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(
                params,
                function (err, userRecords) {
                    if (userRecords.length > 0) {
                        let match={
                            companyId: ObjectId(params.companyId),
                            submitBy: { $in: userRecords[0].userIds },
                            dcrDate: {
                                $gte: new Date(fromDate),
                                $lte: new Date(toDate)
                            },
							workingStatus:{$in:["Conference","Working"]}
                            //"DCRStatus": 1
                        }                      

                        DcrmasterCollection.aggregate(
                            // Stage 1
                            {
                                $match: match
                                    
                            },
                            //Stage 2
                            {
                                $unwind: "$visitedBlock"
                            },
                            // Stage 3
                            {
                                $project: {
                                    "dcrDate": 1,
                                    userId: "$submitBy",
                                    dcrAreaId: { $setUnion: [["$visitedBlock.fromId"], ["$visitedBlock.toId"]] }

                                }
                            },// Stage 4
                            {
                                $unwind: "$dcrAreaId"
                            },

                            // Stage 5
                            {
                                $lookup: {
                                    "from": "Area",
                                    "localField": "dcrAreaId",
                                    "foreignField": "areaStringId",
                                    "as": "areaObj"
                                }
                            }, // Stage 6
                            {
                                $group: {
                                    _id: {
                                        userId: "$userId",
                                        dcrDate: "$dcrDate",
                                        areaType: { $arrayElemAt: ["$areaObj.type", 0] }
                                    },
                                   areaTypes: {
                                        $addToSet: { $arrayElemAt: ["$areaObj.type", 0] }
                                    }
                                }
                            },
							{
								$project:{
                                        dcrId:"$_id.dcrId",
                                        userId: "$_id.userId",
                                        dcrDate: "$_id.dcrDate",
                                        areaType: "$_id.areaType",
                                    
                                    total: {
                                        $size: "$areaTypes"
                                    }
}
								
							},

                            // Stage 7
                            {
                                $group: {
                                    _id: {
                                        userId: "$_id.userId"
                                    },
                                    HQCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [
                                                    { $eq: ["$_id.areaType", "HQ"] }
                                                ]
                                            }, "$total", 0]
                                        }
                                    },
                                    EXCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [
                                                    { $eq: ["$_id.areaType", "EX"] }
                                                ]
                                            }, "$total", 0]
                                        }
                                    }
                                    ,
                                    OUTCount: {
                                        $sum: {
                                            $cond: [{
                                                $and: [
                                                    { $eq: ["$_id.areaType", "OUT"] }
                                                ]
                                            }, "$total", 0]
                                        }
                                    }
                                }
                            }
                           ,
                            {
                                allowDiskUse: true
                            },
                            function (err, StationWiseresult) {
                                if (err) {
                                    console.log(err);
                                    return cb(err);
                                }
                                if (!StationWiseresult) {
                                    var err = new Error('No Records Found');
                                    err.statusCode = 404;
                                    err.code = 'NOT FOUND';
                                    return cb(err);
                                }
                                let finalObj=[]
                                for (let i = 0; i < userRecords[0].obj.length; i++) {
                        
                                    let userFound = StationWiseresult.filter(function (obj) {
                                        return obj._id.userId.toString() == userRecords[0].obj[i].userId.toString();
                                    });

                                if(userFound.length>0){
                                      finalObj.push({
                                       userName:userRecords[0].obj[i].name,
                                       stateName:userRecords[0].obj[i].stateName,
                                       districtName:userRecords[0].obj[i].districtName,
                                       hQCount:userFound[0].HQCount,
                                       EXCount:userFound[0].EXCount,
                                       OUTCount:userFound[0].OUTCount,

                                      })
                                }else{
                                    finalObj.push({
                                        userName:userRecords[0].obj[i].name,
                                        stateName:userRecords[0].obj[i].stateName,
                                        districtName:userRecords[0].obj[i].districtName,
                                        hQCount:0,
                                        EXCount:0,
                                        OUTCount:0
 
                                       })

                                }

                                }
                                //console.log("DCR Result-", StationWiseresult)
                            
                                return cb(false, finalObj)
                            });
                    } else {
                        cb(false, []);
                    }
                });

        } else {
           
            Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                params.companyId, [true],
                params.userIds,
                function (err, userRecords) {
                    if (err) {
                        return err;
                    }
                    let match={
                        companyId: ObjectId(params.companyId),
                        submitBy: { $in: userRecords[0].userIds },
                        dcrDate: {
                            $gte: new Date(fromDate),
                            $lte: new Date(toDate)
                        },
						workingStatus:{$in:["Conference","Working"]}

                        //"DCRStatus": 1
                    }
                    DcrmasterCollection.aggregate(

                        // Stage 1
                        {
                            $match: match
                                
                        },
                        //Stage 2
                        {
                            $unwind: "$visitedBlock"
                        },
                        // Stage 3
                        {
                            $project: {
                                "dcrDate": 1,
                                userId: "$submitBy",
                                dcrAreaId: { $setUnion: [["$visitedBlock.fromId"], ["$visitedBlock.toId"]] }
        
                            }
                        },// Stage 4
                        {
                            $unwind: "$dcrAreaId"
                        },
        
                        // Stage 5
                        {
                            $lookup: {
                                "from": "Area",
                                "localField": "dcrAreaId",
                                "foreignField": "areaStringId",
                                "as": "areaObj"
                            }
                        }, // Stage 6
                        {
                                $group: {
                                    _id: {
                                        userId: "$userId",
                                        dcrDate: "$dcrDate",
                                        areaType: { $arrayElemAt: ["$areaObj.type", 0] }
                                    },
                                   areaTypes: {
                                        $addToSet: { $arrayElemAt: ["$areaObj.type", 0] }
                                    }
                                }
                            },
							{
								$project:{
                                        dcrId:"$_id.dcrId",
                                        userId: "$_id.userId",
                                        dcrDate: "$_id.dcrDate",
                                        areaType: "$_id.areaType",
                                    
                                    total: {
                                        $size: "$areaTypes"
                                    }
}
								
							},
        
                        // Stage 7
                        {
                            $group: {
                                _id: {
                                    userId: "$_id.userId"
                                },
                                HQCount: {
                                    $sum: {
                                        $cond: [{
                                            $and: [
                                                { $eq: ["$_id.areaType", "HQ"] }
                                            ]
                                        }, "$total", 0]
                                    }
                                },
                                EXCount: {
                                    $sum: {
                                        $cond: [{
                                            $and: [
                                                { $eq: ["$_id.areaType", "EX"] }
                                            ]
                                        }, "$total", 0]
                                    }
                                }
                                ,
                                OUTCount: {
                                    $sum: {
                                        $cond: [{
                                            $and: [
                                                { $eq: ["$_id.areaType", "OUT"] }
                                            ]
                                        }, "$total", 0]
                                    }
                                }
                            }
                        }
                       ,
                        {
                            allowDiskUse: true
                        },
                        function (err, StationWiseresult) {
                            if (err) {
                                console.log(err);
                                return cb(err);
                            }
                            if (!StationWiseresult) {
                                var err = new Error('No Records Found');
                                err.statusCode = 404;
                                err.code = 'NOT FOUND';
                                return cb(err);
                            }
                            let finalObj=[]
                            for (let i = 0; i < userRecords[0].obj.length; i++) {
                    
                                let userFound = StationWiseresult.filter(function (obj) {
                                    return obj._id.userId.toString() == userRecords[0].obj[i].userId.toString();
                                });
        
                            if(userFound.length>0){
                                  finalObj.push({
                                   userName:userRecords[0].obj[i].name,
                                   stateName:userRecords[0].obj[i].stateName,
                                   districtName:userRecords[0].obj[i].districtName,
                                   hQCount:userFound[0].HQCount,
                                   EXCount:userFound[0].EXCount,
                                   OUTCount:userFound[0].OUTCount,
        
                                  })
                            }else{
                                finalObj.push({
                                    userName:userRecords[0].obj[i].name,
                                    stateName:userRecords[0].obj[i].stateName,
                                    districtName:userRecords[0].obj[i].districtName,
                                    hQCount:0,
                                    EXCount:0,
                                    OUTCount:0
        
                                   })
        
                            }
        
                            }
                            //console.log("DCR Result-", StationWiseresult)
                        
                            return cb(false, finalObj)
                        });

        }
        );
    }
}

    Dcrmaster.remoteMethod(
        'stationWiseWorking', {
            description: 'get station wise working',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //---------------------------------end-----------------------------------------
	
	
	//-------------------------------------Praveen Kumar(22-07-2019)-----------------------------
	Dcrmaster.updateImage = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
		if(params.updateImageFor == "Expense" ){
			DcrmasterCollection.update({
					"companyId" : ObjectId(params.companyId),
					"_id" : ObjectId(params.dcrId),
                    "dcrExpense.id": params.dcrExpenseId
                }, {
					$set : {"dcrExpense.$.fileId" : ObjectId(params.fileId), updatedAt : new Date() }
				
				}, function(err, dcrResult) {
                    if (err) {
                        console.log(err)
                    }
					cb(false, dcrResult)
				})	
		}else if(params.updateImageFor == "updateFileIdIntoObjectId"){
            DcrmasterCollection.update({
                "_id" : ObjectId(params.id),
            }, {
                $set : {"dcrExpense" : params.dcrExpenseObject, updatedAt : new Date() }
            
            }, function(err, dcrResult) {
                if (err) {
                    console.log(err)
                }
                cb(false, dcrResult)
            })	
        }
	}	

    Dcrmaster.remoteMethod(
        'updateImage', {
            description: 'update image',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
	//--------------------------------------------END--------------------------------------------
	 //------------------delet dCR----by preeti-------------------------
    Dcrmaster.deleteDCR = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName)
        let id=ObjectId(params.dcrId)
        DcrmasterCollection.remove({_id:id})
        Dcrmaster.app.models.DCRProviderVisitDetails.deleteVisitedProvider( params, function (err, response) {
            console.log("response=",response)
        })
        return cb(false,"done")
    } 
    ,Dcrmaster.remoteMethod(
        'deleteDCR', {
            description: 'deleteDCR',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //------------------------
	
	
 //------------------Locked DCR----by preeti--12-11-2019-----------------------
    Dcrmaster.getLockedDCR = function (params, cb) {

        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        let filter = {};
        let match = {}
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        const finalLockedData=[];

        if (params.lockFilterType == "state") {
            for (var i = 0; i < params.stateId.length; i++) {
                stateIdsArr.push(ObjectId(params.stateId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { inq: stateIdsArr },
            }
            match = {
                companyId: ObjectId(params.companyId),
                stateId: { $in: stateIdsArr },
            }
        } else if (params.lockFilterType == "Headquarter") {

            for (var i = 0; i < params.districtId.length; i++) {
                hqIdArray.push(ObjectId(params.districtId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { inq: hqIdArray }
            }
            match = {
                companyId: ObjectId(params.companyId),
                districtId: { $in: hqIdArray }
            }
        }
        else if (params.lockFilterType == "employee") {
            for (var i = 0; i < params.userId.length; i++) {
                empArray.push(ObjectId(params.userId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { inq: empArray }
            }
            match = {
                companyId: ObjectId(params.companyId),
            }
        }
        Dcrmaster.app.models.UserInfo.find({
            where: filter,
        },
            function (err, response) {
                let currentDate = moment.utc().format("YYYY-MM-DDT00:00:00.000Z");
                let splitDate = currentDate.split("-");
                let splitDay = splitDate[2].split("T");
                let Day = splitDay[0];  //current date
                let getLockedDay = 0;
                let checkLockToDay = 0;
                let checkLockFromDay = 0;
                let month = 0

                for (var i = 0; i < response.length; i++) {

                    if (response[i].lockingPeriod < params.lockDetails) { // check , user is updating locking days.
                      
					  getLockedDay = params.lockDetails - response[i].lockingPeriod // subtract from existing locking period.
                        if (Day < getLockedDay) {
                            month = splitDate[1] - 1;
                            var totalDays = moment([splitDate[0], month - 1]).daysInMonth();
                            checkLockFromDay = totalDays - (getLockedDay - Day); // to get the from date
                        } else {
                            month = splitDate[1]
                            checkLockFromDay = Day - getLockedDay;
                        }
                        if (Day < response[i].lockingPeriod) {
                            month = splitDate[1] - 1;
                            var totalDays = moment([splitDate[0], month - 1]).daysInMonth();
                            checkLockToDay = totalDays - (response[i].lockingPeriod - Day);
                        } else {
                            month = splitDate[1]
                            checkLockToDay = Day - response[i].lockingPeriod; // we will deduct already exisiting locking period.
                        }
                    }
                    let fromDate = splitDate[0] + "-" + month + "-" + checkLockFromDay;
                    let toDate = splitDate[0] + "-" + month + "-" + checkLockToDay;

                    let dcrDate = {
                        $gte: new Date(fromDate),
                        $lte: new Date(toDate)
                    }

                    match.dcrDate = dcrDate;
                    match.submitBy = ObjectId(response[i].userId);
                    DcrmasterCollection.aggregate(
                        {
                            $match: match

                        },
                        // Stage 2
                        {
                            $project: {
                                companyId: 1,
                                dcrDate: 1,
                                stateId: 1,
                                districtId: 1,
                                submitBy: 1,
                                dcrSubmissionDate: 1,

                            }
                        },

                        function (err, result) {

                            var startDate = new Date(fromDate); //YYYY-MM-DD
                            var endDate = new Date(toDate) //YYYY-MM-DD

                            var getDateArray = function (start, end) {
                                var arr = new Array();
                                var dt = new Date(start);
                                while (dt <= end) {
                                    arr.push(new Date(dt));
                                    dt.setDate(dt.getDate() + 1);
                                }
                                return arr;
                            }
                            const data = getDateArray(startDate, endDate);
                            if (result.length > 0) {
                                for (const iterator of data) {
                                    const dcrData = result.filter(obj => {
                                        return moment(obj.dcrDate).format("YYYY-MM-DD") == moment(iterator).format("YYYY-MM-DD")
                                    });
                                    if (dcrData.length == 0) {
                                        finalLockedData.push({
                                        companyId:result[0].companyId,
                                        userId:result[0].submitBy,
                                        DCRLockedDate:new Date (moment(iterator).format("YYYY-MM-DD")),
                                        stateId:result[0].stateId,
                                        districtId:result[0].districtId,
                                        DCRLockedOpenDate:new Date(),
                                        createdAt:new Date(),
                                        updatedBy:new Date()
                                    })
                                    }
                                }
                            return cb(false,finalLockedData);

                        }
                        else{

                        }
                           
                        })



                }

            })


    },
        Dcrmaster.remoteMethod(
            'getLockedDCR', {
                description: 'getLockedDCR',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );


    //------------------------
	
	
	
	//------------------------------------------------------Divya(04-11-2019)---------------------
	//Aimil Pharma Specific method
		Dcrmaster.getDoctorCallAverage = function(params, cb) {  
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var UserAreaMappingCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.UserAreaMapping.modelName);
        let begin = new Date(params.dateRange.begin);
        let end = new Date(params.dateRange.end);
        var startDate = moment(begin, "YYYY-M-DD");
        var endDate = moment(end, "YYYY-M-DD").endOf("month");

        var allMonthsInPeriod = [];
        while (startDate.isBefore(endDate)) {
            allMonthsInPeriod.push(startDate.format("YYYY-MM"));
            startDate = startDate.add(1, "month");
        };

        //  console.log("allMonthsInPeriod");console.log(allMonthsInPeriod);
        let dcrDataGroup = {

            _id: {
                "submitBy": "$submitBy",
                month: "$month",
                year: "$year"
            },
            dcrSubmission: {
                $addToSet: "$dcrId"
            },
            fieldWorkingDays: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$DCRStatus", 1]
                        }]
                    }, "$dcrId", "$abc"]
                }
            },
            leave: {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$DCRStatus", 0]
                        }, {
                            $eq: ["$workingStatus", "Holiday"]
                        }]
                    }, "$dcrId", "$abc"]
                }
            }

        }
        let dcrDataProject = {


            _id: 0,
            month: "$_id.month",
            year: "$_id.year",
            userId: "$_id.submitBy",
            dcrSubmission: {
                $size: "$dcrSubmission"
            },
            fieldWorkingDays: {
                $size: "$fieldWorkingDays"
            },
            leave: {
                $size: "$leave"
            }
        }

        //-----Dynamic Group and Project For Doctors--------------------
        if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == true) {

            dcrDataGroup.listedDoctorSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$listedDoctorSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.listedDoctorSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }
            dcrDataProject.listedDoctorCall = {
                $size: "$listedDoctorCall"
            }
            dcrDataProject.drCallAvg = {
                $divide: ["$listedDoctorSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }


        } else if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == false) {
            dcrDataGroup.listedDoctorSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == true) {
            dcrDataGroup.listedDoctorSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.doctorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedDoctorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedDoctorCallForUnlistedAvgTrue = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "RMP"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataProject.drCallAvg = {
                $divide: ["$doctorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.listedDoctorCallAvg = {
                $divide: [{
                    $size: "$listedDoctorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        }
        //-----Dynamic Group and Project For Vendor--------------------
        if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.listedChemistSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedStockistSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Stockist"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$listedChemistSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.stockistCallAvg = {
                $divide: ["$listedStockistSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == false) {
            //  console.log("inside here Vendor");
            dcrDataGroup.listedChemistSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedStockistSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Stockist"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, 1, 0]
                }
            }
            dcrDataGroup.listedChemistCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }

            dcrDataGroup.listedStockistCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                            $eq: ["$providerType", "Stockist"]
                        }, {
                            $ne: ["$providerStatus", "unlisted"]
                        }]
                    }, "$providerId", "$abc"]
                }
            }
            dcrDataProject.listedChemistCall = { $size: "$listedChemistCall" }
            dcrDataProject.listedStockistCall = { $size: "$listedStockistCall" }
            dcrDataProject.venSeenAvg = {
                $divide: ["$listedChemistSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.stockistSeenAvg = {
                $divide: ["$listedStockistSeen", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.chemistCallAvg = {
                $divide: [{
                    $size: "$listedChemistCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
            dcrDataProject.stockistCallAvg = {
                $divide: [{
                    $size: "$listedStockistCall"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == false) {
            dcrDataGroup.listedChemistSeen = {
                $sum: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, 1, 0]
                }
            }

            dcrDataGroup.vendorCallForUnlistedAvgFalse = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{

                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]


                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgFalse = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgFalse", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.stockistCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgFalse"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }
        } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == true) {
            dcrDataGroup.listedChemistSeen = {
                $sum: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, 1, 0]
                }
            }
            dcrDataGroup.vendorCallForUnlistedAvgTrue = {
                $sum: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, 1, 0]
                }
            }

            dcrDataGroup.listedVendorCall = {
                $addToSet: {
                    $cond: [{
                        $and: [{
                                $ne: ["$providerStatus", "unlisted"]
                            },
                            {
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }
                        ]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataGroup.listedVendorCallForUnlistedAvgTrue = {

                $addToSet: {
                    $cond: [{
                        $or: [{
                            $eq: ["$providerType", "Drug"]
                        }, {
                            $eq: ["$providerType", "Stockist"]
                        }]
                    }, "$providerId", "$abc"]

                }
            }

            dcrDataProject.venCallAvg = {
                $divide: ["$vendorCallForUnlistedAvgTrue", {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

            dcrDataProject.stockistCallAvg = {
                $divide: [{
                    $size: "$listedVendorCallForUnlistedAvgTrue"
                }, {
                    $cond: {
                        if: {
                            $gte: [{ $size: "$fieldWorkingDays" }, 1]
                        },
                        then: { $size: "$fieldWorkingDays" },
                        else: 1 // used 1 for divided by zero
                    }
                }]
            }

        }

        if (params.type == "State" || params.type == "Headquarter") {

            Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params, function(err, users) {
                if (users.length > 0) {

                    async.parallel({
                            dcrData: function(cb) {

                                dcrCollection.aggregate({
                                    $match: {
                                        companyId: ObjectId(params.companyId),
                                        submitBy: {
                                            $in: users[0].userIds
                                        },
                                        dcrDate: {
                                            $gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                                            $lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")

                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DCRProviderVisitDetails",
                                        "localField": "_id",
                                        "foreignField": "dcrId",
                                        "as": "dcrProviders"
                                    }
                                }, {
                                    $unwind: {
                                        path: "$dcrProviders",
                                        preserveNullAndEmptyArrays: true,
                                        includeArrayIndex: "arrayIndex"
                                    }
                                }, {
                                    $project: {
                                        dcrId: "$_id",
                                        dcrDate: "$dcrDate",
                                        workingStatus: 1,
                                        providerType: "$dcrProviders.providerType",
                                        providerId: "$dcrProviders.providerId",
                                        submitBy: 1,
                                        arrayIndex: 1,
                                        providerStatus: "$dcrProviders.providerStatus",
                                        DCRStatus: 1,
                                        month: { $month: '$dcrDate' },
                                        year: { $year: '$dcrDate' }
                                    }
                                }, {
                                    $group: dcrDataGroup
                                }, {
                                    $project: dcrDataProject
                                }, {
                                    $sort: {
                                        year: 1,
                                        month: 1
                                    }
                                }, {
                                    cursor: {
                                        batchSize: 50
                                    },

                                    allowDiskUse: true
                                }, function(err, result) {
                                    // console.log("hey");console.log(err);
                                    cb(false, result)
                                });
                            },
                            providerVisitDetails: function(cb) {
                                let doctorMatch = {},
                                    vendorMatch = {};

                                if (params.unlistedValidations.unlistedDocCall == false) {
                                    doctorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$providerType", "RMP"]
                                                }, {
                                                    $ne: ["$providerStatus", "unlisted"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                } else if (params.unlistedValidations.unlistedDocCall == true) {
                                    doctorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$providerType", "RMP"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                }

                                if (params.unlistedValidations.unlistedVenCall == false) {
                                    vendorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $or: [{
                                                    $eq: ["$providerType", "Drug"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }, {
                                                    $and: [{
                                                        $ne: ["$providerStatus", "unlisted"]
                                                    }]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                } else if (params.unlistedValidations.unlistedVenCall == true) {
                                    vendorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $or: [{
                                                    $eq: ["$providerType", "Drug"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                }

                                dcrCollection.aggregate({
                                    $match: {
                                        companyId: ObjectId(params.companyId),
                                        submitBy: {
                                            $in: users[0].userIds
                                        },
                                        dcrDate: {
                                            $gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                                            $lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")

                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DCRProviderVisitDetails",
                                        "localField": "_id",
                                        "foreignField": "dcrId",
                                        "as": "dcrProviders"
                                    }
                                }, {
                                    $unwind: {
                                        path: "$dcrProviders",
                                        preserveNullAndEmptyArrays: true,
                                        includeArrayIndex: "arrayIndex"
                                    }
                                }, {
                                    $project: {
                                        dcrId: "$_id",
                                        dcrDate: "$dcrDate",
                                        workingStatus: 1,
                                        providerType: "$dcrProviders.providerType",
                                        providerId: "$dcrProviders.providerId",
                                        providerStatus: "$dcrProviders.providerStatus",
                                        submitBy: 1,
                                        arrayIndex: 1,
                                        providerStatus: "$dcrProviders.providerStatus",
                                        DCRStatus: 1,
                                        month: { $month: '$dcrDate' },
                                        year: { $year: '$dcrDate' }
                                    }
                                }, {
                                    $group: {
                                        _id: {
                                            "submitBy": "$submitBy",
                                            "providerId": "$providerId",
                                            "month": "$month",
                                            "year": "$year"
                                        },
                                        //dcrId: { $addToSet: "$dcrId" }
                                        doctorMet: doctorMatch,
                                        vendorMet: vendorMatch
                                    }
                                }, {
                                    $project: {
                                        _id: 0,
                                        userId: "$_id.submitBy",
                                        providerId: "$_id.providerId",
                                        month: "$_id.month",
                                        year: "$_id.year",
                                        doctorMet: { $size: "$doctorMet" },
                                        vendorMet: { $size: "$vendorMet" }
                                    }
                                }, {
                                    cursor: {
                                        batchSize: 50
                                    },

                                    allowDiskUse: true
                                }, function(err, result) {
                                    cb(false, result)
                                });
                            },
                            providerData: function(cb) {
                                let providerDynamicGroup = {
                                    _id: {
                                        userId: "$userId",
                                        providerType: "$providerType"
                                    },
                                    totalProvider: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        "$eq": ["$areaStatus", true]
                                                    }, {
                                                        "$eq": ["$areaAppStatus", "approved"]
                                                    }, {
                                                        "$eq": ["$status", true]
                                                    },
                                                    {
                                                        "$eq": ["$appStatus", "approved"]
                                                    }
                                                ],
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedDoc: { // 08-08-2019 PK Sir and Preeti has added this condition  mutually because we have added these basis on these sencerio like: 1. case of unlisted doctor which is not approved ywt, 2- if doctor is deleted after selected month or between selected month, 3- or total active doctor till selected date
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $lte: ["$createdAt", new Date(params.dateRange.begin)]
                                                }, {

                                                }, {
                                                    $eq: ["$appStatus", "unlisted"]
                                                }]
                                            }, 1, 0]
                                        }
                                    }
                                };
                                let monthARRAY = monthNameAndYearBTWTwoDate(params.dateRange.begin, params.dateRange.end)
                                    //console.log(monthARRAY);

                                for (let m of monthARRAY) {
                                    providerDynamicGroup["totalProviderForSelectedPeriod_" + m.month + "_" + m.year] = {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $lte: ["$finalAppDate", new Date(m.year, m.month - 1, m.endDate)]
                                                    },
                                                    {
                                                        $eq: ["$appStatus", "approved"]
                                                    },
                                                    {
                                                        $or: [{
                                                                $and: [
                                                                    { $gte: ["$finalDelDate", new Date(m.year, m.month - 1, 1)] },
                                                                    { $lte: ["$finalDelDate", new Date(m.year, m.month - 1, m.endDate)] }
                                                                ]
                                                            },
                                                            {
                                                                $eq: ["$finalDelDate", new Date("1900-01-01T00:00:00.000Z")]
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    }
                                }

                                UserAreaMappingCollection.aggregate({
                                        $match: {
                                            "companyId": ObjectId(params.companyId),
                                            "userId": {
                                                $in: users[0].userIds
                                            }
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "Providers",
                                            "localField": "areaId",
                                            "foreignField": "blockId",
                                            "as": "Providers"
                                        }
                                    }, {
                                        $unwind: {
                                            "path": "$Providers",
                                            "preserveNullAndEmptyArrays": false
                                        }
                                    }, {
                                        $project: {
                                            userId: "$userId",
                                            blockId: "$Providers.blockId",
                                            blockName: "$Providers.blockName",
                                            providerId: "$Providers._id",
                                            providerName: "$Providers.providerName",
                                            providerType: "$Providers.providerType",
                                            updatedAt: "$Providers.updatedAt",
                                            createdAt: "$Providers.createdAt",
                                            deletedAt: "$Providers.deletedAt",
                                            approvedDate: "$Providers.approvedDate",
                                            finalAppDate: "$Providers.finalAppDate",
                                            mgrDelDate: "$Providers.mgrDelDate",
                                            finalDelDate: "$Providers.finalDelDate",
                                            status: "$Providers.status",
                                            appStatus: "$Providers.appStatus",
                                            delStatus: "$Providers.delStatus",
                                            areaStatus: "$status",
                                            areaAppStatus: "$appStatus",
                                            areaDelStatus: "$delStatus",
                                            month: { $month: '$finalAppDate' },
                                            year: { $year: '$finalAppDate' }
                                        }
                                    }, {
                                        $group: providerDynamicGroup
                                    }
                                    /* , {
                                         $project: {
                                             _id: 1,
                                             totalProvider: 1,
                                             totalProviderForSelectedPeriod: { $add: ["$totalProviderForSelectedPeriod", "$unlistedDoc", "$delDoc"] }
                                         }

                                     }, {
                                         $project: {
                                             _id: 0,
                                             month: "$_id.month",
                                             year: "$_id.year",
                                             userId: "$_id.userId",
                                             providerType: "$_id.providerType",
                                             totalProvider: "$totalProvider",
                                             totalActiveProvider: "$totalProviderForSelectedPeriod"
                                         }
                                     }, {
                                         $sort: {
                                             year: 1,
                                             month: 1,
                                             userId: 1,
                                             providerType: 1
                                         }
                                     }*/
                                    ,
                                    function(err, providerInfo) {
                                        cb(false, providerInfo);
                                    });

                            }
                        },
                        function(err, result) {
                            let finalResult = [];
                            let dcrData = result.dcrData;
                            let providerVisitData = result.providerVisitDetails;
                            let providerData = result.providerData;
                            let monthARRAY = monthNameAndYearBTWTwoDate(params.dateRange.begin, params.dateRange.end)
                            for (let i = 0; i < users[0].userIds.length; i++) {
                                //console.log("users");console.log(users[0].obj[i]);
                                let monthWiseDataArray = [],
                                    data = [];

                                for (let m of monthARRAY) {

                                    let providerFilterObj = providerData.filter(function(obj) {
                                        return obj._id.userId.toString() == users[0].userIds[i].toString();
                                    });
                                    let totalChemists = 0,
                                        totalDoctors = 0;
                                    let totalActiveVendorOnSelectedTimePeriod = 0,
                                        totalActiveDoctorOnSelectedTimePeriod = 0;

                                    if (providerFilterObj.length > 0) {

                                        for (let providers of providerFilterObj) {
                                            if (providers.hasOwnProperty("totalProviderForSelectedPeriod_" + m.month + "_" + m.year) && providers._id.providerType === 'Drug' || providers._id.providerType === 'Stockist') {
                                                totalChemists = totalChemists + providers.totalProvider;
                                                totalActiveVendorOnSelectedTimePeriod = totalActiveVendorOnSelectedTimePeriod + providers["totalProviderForSelectedPeriod_" + m.month + "_" + m.year];

                                            } else if (providers.hasOwnProperty("totalProviderForSelectedPeriod_" + m.month + "_" + m.year) && providers._id.providerType === 'RMP') {
                                                totalDoctors = totalDoctors + providers.totalProvider;
                                                totalActiveDoctorOnSelectedTimePeriod = providers["totalProviderForSelectedPeriod_" + m.month + "_" + m.year];
                                            }
                                        }
                                    }


                                    //Provider Met Data--------------------
                                    let doctorMetOnce = 0,
                                        doctorMetTwiceOrMore = 0;
                                    let providerVisit = providerVisitData.filter(function(obj) {
                                        return obj.userId.toString() == users[0].userIds[i].toString();
                                    });
                                    if (providerVisit.length > 0) {
                                        for (const iterator of providerVisit) {
                                            if (m.month == iterator.month && m.year == iterator.year && iterator.doctorMet == 1) {
                                                doctorMetOnce++;
                                            }
                                            if (m.month == iterator.month && m.year == iterator.year && iterator.doctorMet >= 2) {
                                                doctorMetTwiceOrMore++;
                                            }
                                        }
                                    }

                                    //DCR DATA--------------------
                                    let dcrSubmission = 0,
                                        fieldWorkingDays = 0,
                                        leave = 0,
                                        listedDoctorSeen = 0,
                                        chemistSeen = 0,
                                        stockistSeen = 0,
                                        chemistCallAvg = 0,
                                        stockistCallAvg = 0;

                                    let dcrFilterObj = dcrData.filter(function(obj) {
                                        return obj.userId.toString() == users[0].userIds[i].toString() && obj.month == m.month && obj.year == m.year
                                    })
                                    if (dcrFilterObj.length > 0) {
                                        dcrSubmission = dcrFilterObj[0].dcrSubmission;
                                        fieldWorkingDays = dcrFilterObj[0].fieldWorkingDays;
                                        leave = dcrFilterObj[0].leave;
                                        listedDoctorSeen = dcrFilterObj[0].listedDoctorCall;
                                        chemistSeen = dcrFilterObj[0].listedChemistCall;
                                        stockistSeen = dcrFilterObj[0].listedStockistCall;
                                        chemistCallAvg = dcrFilterObj[0].chemistCallAvg.toFixed(2);
                                        stockistCallAvg = dcrFilterObj[0].stockistCallAvg.toFixed(2);
                                    }
                                    data = [
                                        dcrSubmission,
                                        fieldWorkingDays,
                                        leave,
                                        totalActiveDoctorOnSelectedTimePeriod,
                                        listedDoctorSeen,
                                        doctorMetOnce, //'--1--',
                                        doctorMetTwiceOrMore, //'--2--',
                                        totalActiveDoctorOnSelectedTimePeriod - listedDoctorSeen,
                                        doctorMetTwiceOrMore, //'--3--',
                                        ((listedDoctorSeen / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2),
                                        (listedDoctorSeen / (fieldWorkingDays == 0 ? 1 : fieldWorkingDays)).toFixed(2),
                                        (((totalActiveDoctorOnSelectedTimePeriod - listedDoctorSeen) / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2),
                                        ((doctorMetTwiceOrMore / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2), //'--4--',
                                        chemistSeen,
                                        chemistCallAvg,
                                        stockistSeen

                                    ]
                                    monthWiseDataArray.push({
                                        year: m.monthName,
                                        month: m.year,
                                        data: data
                                    })
                                }
                                finalResult.push({
									divisionName:users[0].obj[i].divisionName,
                                    stateName: users[0].obj[i].stateName,
                                    districtName: users[0].obj[i].districtName,
                                    name: users[0].obj[i].name,
                                    empDOJ: users[0].obj[i].joiningDate,
                                    docVenData: monthWiseDataArray
                                });
                                //console.log("finalResult");console.log(data);
                            }
                            return cb(false, finalResult);
                        });
                }
            });
        } else {
            Dcrmaster.app.models.UserInfo.getAllUserIdsOnCompanyBasis(params.companyId, [true], params.employeeIds, function(err, users) {
                if (users.length > 0) {

                    async.parallel({
                            dcrData: function(cb) {

                                dcrCollection.aggregate({
                                    $match: {
                                        companyId: ObjectId(params.companyId),
                                        submitBy: {
                                            $in: users[0].userIds
                                        },
                                        dcrDate: {
                                            $gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                                            $lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")

                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DCRProviderVisitDetails",
                                        "localField": "_id",
                                        "foreignField": "dcrId",
                                        "as": "dcrProviders"
                                    }
                                }, {
                                    $unwind: {
                                        path: "$dcrProviders",
                                        preserveNullAndEmptyArrays: true,
                                        includeArrayIndex: "arrayIndex"
                                    }
                                }, {
                                    $project: {
                                        dcrId: "$_id",
                                        dcrDate: "$dcrDate",
                                        workingStatus: 1,
                                        providerType: "$dcrProviders.providerType",
                                        providerId: "$dcrProviders.providerId",
                                        submitBy: 1,
                                        arrayIndex: 1,
                                        providerStatus: "$dcrProviders.providerStatus",
                                        DCRStatus: 1,
                                        month: { $month: '$dcrDate' },
                                        year: { $year: '$dcrDate' }
                                    }
                                }, {
                                    $group: dcrDataGroup
                                }, {
                                    $project: dcrDataProject
                                }, {
                                    $sort: {
                                        year: 1,
                                        month: 1
                                    }
                                }, {
                                    cursor: {
                                        batchSize: 50
                                    },

                                    allowDiskUse: true
                                }, function(err, result) {
                                    // console.log("hey");console.log(err);
                                    cb(false, result)
                                });
                            },
                            providerVisitDetails: function(cb) {
                                let doctorMatch = {},
                                    vendorMatch = {};

                                if (params.unlistedValidations.unlistedDocCall == false) {
                                    doctorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$providerType", "RMP"]
                                                }, {
                                                    $ne: ["$providerStatus", "unlisted"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                } else if (params.unlistedValidations.unlistedDocCall == true) {
                                    doctorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $and: [{
                                                    $eq: ["$providerType", "RMP"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                }

                                if (params.unlistedValidations.unlistedVenCall == false) {
                                    vendorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $or: [{
                                                    $eq: ["$providerType", "Drug"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }, {
                                                    $and: [{
                                                        $ne: ["$providerStatus", "unlisted"]
                                                    }]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                } else if (params.unlistedValidations.unlistedVenCall == true) {
                                    vendorMatch = {
                                        $addToSet: {
                                            $cond: [{
                                                $or: [{
                                                    $eq: ["$providerType", "Drug"]
                                                }, {
                                                    $eq: ["$providerType", "Stockist"]
                                                }]
                                            }, "$dcrId", "$abc"]
                                        }
                                    }
                                }

                                dcrCollection.aggregate({
                                    $match: {
                                        companyId: ObjectId(params.companyId),
                                        submitBy: {
                                            $in: users[0].userIds
                                        },
                                        dcrDate: {
                                            $gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                                            $lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")

                                        }
                                    }
                                }, {
                                    $lookup: {
                                        "from": "DCRProviderVisitDetails",
                                        "localField": "_id",
                                        "foreignField": "dcrId",
                                        "as": "dcrProviders"
                                    }
                                }, {
                                    $unwind: {
                                        path: "$dcrProviders",
                                        preserveNullAndEmptyArrays: true,
                                        includeArrayIndex: "arrayIndex"
                                    }
                                }, {
                                    $project: {
                                        dcrId: "$_id",
                                        dcrDate: "$dcrDate",
                                        workingStatus: 1,
                                        providerType: "$dcrProviders.providerType",
                                        providerId: "$dcrProviders.providerId",
                                        providerStatus: "$dcrProviders.providerStatus",
                                        submitBy: 1,
                                        arrayIndex: 1,
                                        providerStatus: "$dcrProviders.providerStatus",
                                        DCRStatus: 1,
                                        month: { $month: '$dcrDate' },
                                        year: { $year: '$dcrDate' }
                                    }
                                }, {
                                    $group: {
                                        _id: {
                                            "submitBy": "$submitBy",
                                            "providerId": "$providerId",
                                            "month": "$month",
                                            "year": "$year"
                                        },
                                        //dcrId: { $addToSet: "$dcrId" }
                                        doctorMet: doctorMatch,
                                        vendorMet: vendorMatch
                                    }
                                }, {
                                    $project: {
                                        _id: 0,
                                        userId: "$_id.submitBy",
                                        providerId: "$_id.providerId",
                                        month: "$_id.month",
                                        year: "$_id.year",
                                        doctorMet: { $size: "$doctorMet" },
                                        vendorMet: { $size: "$vendorMet" }
                                    }
                                }, {
                                    cursor: {
                                        batchSize: 50
                                    },

                                    allowDiskUse: true
                                }, function(err, result) {
                                    cb(false, result)
                                });
                            },
                            providerData: function(cb) {
                                let providerDynamicGroup = {
                                    _id: {
                                        userId: "$userId",
                                        providerType: "$providerType"
                                    },
                                    totalProvider: {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        "$eq": ["$areaStatus", true]
                                                    }, {
                                                        "$eq": ["$areaAppStatus", "approved"]
                                                    }, {
                                                        "$eq": ["$status", true]
                                                    },
                                                    {
                                                        "$eq": ["$appStatus", "approved"]
                                                    }
                                                ],
                                            }, 1, 0]
                                        }
                                    },
                                    unlistedDoc: { // 08-08-2019 PK Sir and Preeti has added this condition  mutually because we have added these basis on these sencerio like: 1. case of unlisted doctor which is not approved ywt, 2- if doctor is deleted after selected month or between selected month, 3- or total active doctor till selected date
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                    $lte: ["$createdAt", new Date(params.dateRange.begin)]
                                                }, {

                                                }, {
                                                    $eq: ["$appStatus", "unlisted"]
                                                }]
                                            }, 1, 0]
                                        }
                                    }
                                };
                                let monthARRAY = monthNameAndYearBTWTwoDate(params.dateRange.begin, params.dateRange.end)
                                    //console.log(monthARRAY);

                                for (let m of monthARRAY) {
                                    providerDynamicGroup["totalProviderForSelectedPeriod_" + m.month + "_" + m.year] = {
                                        $sum: {
                                            $cond: [{
                                                $and: [{
                                                        $lte: ["$finalAppDate", new Date(m.year, m.month - 1, m.endDate)]
                                                    },
                                                    {
                                                        $eq: ["$appStatus", "approved"]
                                                    },
                                                    {
                                                        $or: [{
                                                                $and: [
                                                                    { $gte: ["$finalDelDate", new Date(m.year, m.month - 1, 1)] },
                                                                    { $lte: ["$finalDelDate", new Date(m.year, m.month - 1, m.endDate)] }
                                                                ]
                                                            },
                                                            {
                                                                $eq: ["$finalDelDate", new Date("1900-01-01T00:00:00.000Z")]
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }, 1, 0]
                                        }
                                    }
                                }

                                UserAreaMappingCollection.aggregate({
                                        $match: {
                                            "companyId": ObjectId(params.companyId),
                                            "userId": {
                                                $in: users[0].userIds
                                            }
                                        }
                                    }, {
                                        $lookup: {
                                            "from": "Providers",
                                            "localField": "areaId",
                                            "foreignField": "blockId",
                                            "as": "Providers"
                                        }
                                    }, {
                                        $unwind: {
                                            "path": "$Providers",
                                            "preserveNullAndEmptyArrays": false
                                        }
                                    }, {
                                        $project: {
                                            userId: "$userId",
                                            blockId: "$Providers.blockId",
                                            blockName: "$Providers.blockName",
                                            providerId: "$Providers._id",
                                            providerName: "$Providers.providerName",
                                            providerType: "$Providers.providerType",
                                            updatedAt: "$Providers.updatedAt",
                                            createdAt: "$Providers.createdAt",
                                            deletedAt: "$Providers.deletedAt",
                                            approvedDate: "$Providers.approvedDate",
                                            finalAppDate: "$Providers.finalAppDate",
                                            mgrDelDate: "$Providers.mgrDelDate",
                                            finalDelDate: "$Providers.finalDelDate",
                                            status: "$Providers.status",
                                            appStatus: "$Providers.appStatus",
                                            delStatus: "$Providers.delStatus",
                                            areaStatus: "$status",
                                            areaAppStatus: "$appStatus",
                                            areaDelStatus: "$delStatus",
                                            month: { $month: '$finalAppDate' },
                                            year: { $year: '$finalAppDate' }
                                        }
                                    }, {
                                        $group: providerDynamicGroup
                                    }
                                    /* , {
                                         $project: {
                                             _id: 1,
                                             totalProvider: 1,
                                             totalProviderForSelectedPeriod: { $add: ["$totalProviderForSelectedPeriod", "$unlistedDoc", "$delDoc"] }
                                         }

                                     }, {
                                         $project: {
                                             _id: 0,
                                             month: "$_id.month",
                                             year: "$_id.year",
                                             userId: "$_id.userId",
                                             providerType: "$_id.providerType",
                                             totalProvider: "$totalProvider",
                                             totalActiveProvider: "$totalProviderForSelectedPeriod"
                                         }
                                     }, {
                                         $sort: {
                                             year: 1,
                                             month: 1,
                                             userId: 1,
                                             providerType: 1
                                         }
                                     }*/
                                    ,
                                    function(err, providerInfo) {
                                        cb(false, providerInfo);
                                    });

                            }
                        },
                        function(err, result) {
                            let finalResult = [];
                            let dcrData = result.dcrData;
                            let providerVisitData = result.providerVisitDetails;
                            let providerData = result.providerData;
                            let monthARRAY = monthNameAndYearBTWTwoDate(params.dateRange.begin, params.dateRange.end)
                            for (let i = 0; i < users[0].userIds.length; i++) {
                                //console.log("users");console.log(users[0].obj[i]);
                                let monthWiseDataArray = [],
                                    data = [];

                                for (let m of monthARRAY) {

                                    let providerFilterObj = providerData.filter(function(obj) {
                                        return obj._id.userId.toString() == users[0].userIds[i].toString();
                                    });
                                    let totalChemists = 0,
                                        totalDoctors = 0;
                                    let totalActiveVendorOnSelectedTimePeriod = 0,
                                        totalActiveDoctorOnSelectedTimePeriod = 0;

                                    if (providerFilterObj.length > 0) {

                                        for (let providers of providerFilterObj) {
                                            if (providers.hasOwnProperty("totalProviderForSelectedPeriod_" + m.month + "_" + m.year) && providers._id.providerType === 'Drug' || providers._id.providerType === 'Stockist') {
                                                totalChemists = totalChemists + providers.totalProvider;
                                                totalActiveVendorOnSelectedTimePeriod = totalActiveVendorOnSelectedTimePeriod + providers["totalProviderForSelectedPeriod_" + m.month + "_" + m.year];

                                            } else if (providers.hasOwnProperty("totalProviderForSelectedPeriod_" + m.month + "_" + m.year) && providers._id.providerType === 'RMP') {
                                                totalDoctors = totalDoctors + providers.totalProvider;
                                                totalActiveDoctorOnSelectedTimePeriod = providers["totalProviderForSelectedPeriod_" + m.month + "_" + m.year];
                                            }
                                        }
                                    }


                                    //Provider Met Data--------------------
                                    let doctorMetOnce = 0,
                                        doctorMetTwiceOrMore = 0;
                                    let providerVisit = providerVisitData.filter(function(obj) {
                                        return obj.userId.toString() == users[0].userIds[i].toString();
                                    });
                                    if (providerVisit.length > 0) {
                                        for (const iterator of providerVisit) {
                                            if (m.month == iterator.month && m.year == iterator.year && iterator.doctorMet == 1) {
                                                doctorMetOnce++;
                                            }
                                            if (m.month == iterator.month && m.year == iterator.year && iterator.doctorMet >= 2) {
                                                doctorMetTwiceOrMore++;
                                            }
                                        }
                                    }

                                    //DCR DATA--------------------
                                    let dcrSubmission = 0,
                                        fieldWorkingDays = 0,
                                        leave = 0,
                                        listedDoctorSeen = 0,
                                        chemistSeen = 0,
                                        stockistSeen = 0,
                                        chemistCallAvg = 0,
                                        stockistCallAvg = 0;

                                    let dcrFilterObj = dcrData.filter(function(obj) {
                                        return obj.userId.toString() == users[0].userIds[i].toString() && obj.month == m.month && obj.year == m.year
                                    })
                                    if (dcrFilterObj.length > 0) {
                                        dcrSubmission = dcrFilterObj[0].dcrSubmission;
                                        fieldWorkingDays = dcrFilterObj[0].fieldWorkingDays;
                                        leave = dcrFilterObj[0].leave;
                                        listedDoctorSeen = dcrFilterObj[0].listedDoctorCall;
                                        chemistSeen = dcrFilterObj[0].listedChemistCall;
                                        stockistSeen = dcrFilterObj[0].listedStockistCall;
                                        chemistCallAvg = dcrFilterObj[0].chemistCallAvg.toFixed(2);
                                        stockistCallAvg = dcrFilterObj[0].stockistCallAvg.toFixed(2);
                                    }
                                    data = [
                                        dcrSubmission,
                                        fieldWorkingDays,
                                        leave,
                                        totalActiveDoctorOnSelectedTimePeriod,
                                        listedDoctorSeen,
                                        doctorMetOnce, //'--1--',
                                        doctorMetTwiceOrMore, //'--2--',
                                        totalActiveDoctorOnSelectedTimePeriod - listedDoctorSeen,
                                        doctorMetTwiceOrMore, //'--3--',
                                        ((listedDoctorSeen / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2),
                                        (listedDoctorSeen / (fieldWorkingDays == 0 ? 1 : fieldWorkingDays)).toFixed(2),
                                        (((totalActiveDoctorOnSelectedTimePeriod - listedDoctorSeen) / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2),
                                        ((doctorMetTwiceOrMore / (totalActiveDoctorOnSelectedTimePeriod == 0 ? 1 : totalActiveDoctorOnSelectedTimePeriod)) * 100).toFixed(2), //'--4--',
                                        chemistSeen,
                                        chemistCallAvg,
                                        stockistSeen

                                    ]
                                    monthWiseDataArray.push({
                                        year: m.monthName,
                                        month: m.year,
                                        data: data
                                    })
                                }
                                finalResult.push({
									divisionName:users[0].obj[i].divisionName,
                                    stateName: users[0].obj[i].stateName,
                                    districtName: users[0].obj[i].districtName,
                                    name: users[0].obj[i].name,
                                    empDOJ: users[0].obj[i].joiningDate,
                                    docVenData: monthWiseDataArray
                                });
                                //console.log("finalResult");console.log(data);
                            }
                            return cb(false, finalResult);
                        });
                }
            });
        }
    }

    Dcrmaster.remoteMethod(
        'getDoctorCallAverage', {
            description: 'Getting monthy & Yearly Manager worked with Report',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    function monthNameAndYearBTWTwoDate(fromDate, toDate) {
        let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let arr = [];
        let datFrom = new Date(fromDate);
        let datTo = new Date(toDate);
        let fromYear = datFrom.getFullYear();
        let toYear = datTo.getFullYear();
        let diffYear = (12 * (toYear - fromYear)) + datTo.getMonth();
        for (let i = datFrom.getMonth(); i <= diffYear; i++) {
            arr.push({
                monthName: monthNames[i % 12],
                month: (i % 12) + 1,
                year: Math.floor(fromYear + (i / 12)),
                endDate: parseInt(moment(new Date(Math.floor(fromYear + (i / 12)), (i % 12), 1)).endOf("month").format('DD'))
            });
        }
        return arr;
    }
	//----------------------------------------------------END-------------------------------------
	
	//--------------get detailed doctor dcr report preeti arora 3-12-2019---------
    Dcrmaster.getDCRDetailDoctorWise = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        
        Dcrmaster.find({
            where: {
                companyId: params.companyId,
                id: {inq:params.dcrIds}
            } ,"include": [
                {"relation": "info",
                "scope": 
                {"include":[{"relation":"providerinfo",
                "scope":{"fields":["providerName","address"]}}
                ,{"relation":"block","scope":{"fields":["areaName"]}}
                ,{"relation":"file"}]}},
                {"relation":"userInfo",
                "scope":{"fields":["name","districtName"]}
            }]
        }, function(err, DCRResult) {
			                return cb(false,DCRResult);
        })
    },
        Dcrmaster.remoteMethod(
            'getDCRDetailDoctorWise', {
                description: 'getDCRDetailDoctorWise',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
    //--------------------end--------------------
	
	 //---------------------POB details -Rahul Choudhary---07-12-2019------------------------------------------------------------------

  Dcrmaster.getPobDetails = function (params, cb) {
    
    var self = this;
    var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
    let amatch = {};
    let finalObject = [];
    Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
      function (err, userRecords) {

        if (userRecords.length > 0) {
          if (params.type == "State" || params.type == "Headquarter") {
            amatch = {
              "companyId": ObjectId(params.companyId),
              "submitBy": {
                $in: userRecords[0].userIds
              },
              "dcrDate": {
                $gte: new Date(params.fromDate),
                $lte: new Date(params.toDate)
              }
            }

          } else {
            var userIds = [];

            for (var i = 0; i < params.userIds.length; i++) {
              userIds.push(ObjectId(params.userIds[i]));
            }
            amatch = {
              "companyId": ObjectId(params.companyId),
              "submitBy": {
                $in: userIds
              },
              "dcrDate": {
                $gte: new Date(params.fromDate),
                $lte: new Date(params.toDate)
              }
            }

          }

          dcrCollection.aggregate({
              $match: amatch
            }, {
              $lookup: {
                "from": "DCRProviderVisitDetails",
                "localField": "_id",
                "foreignField": "dcrId",
                "as": "dcrDetail"
              }
            }, {
              $unwind: {
                path: "$dcrDetail",
                preserveNullAndEmptyArrays: true
              }
            },

            // Stage 5
            {
              $group: {
                _id: {
                  "submitBy": "$submitBy",
                  "month": {
                    $month: "$dcrDate"
                  },
                  "year": {
                    $year: "$dcrDate"
                  }
                },

                doctorPOB: {
                  $sum: {
                    $cond: [{
                      $and: [{
                        $eq: ["$dcrDetail.providerType", "RMP"]
                      }]
                    }, "$dcrDetail.productPob", 0]
                  }
                },
                vendorPOB: {
                  $sum: {
                    $cond: [{
                      $or: [{
                          $eq: ["$dcrDetail.providerType", "Drug"]
                        },
                        {
                          $eq: ["$dcrDetail.providerType", "Stockist"]
                        }
                      ]
                    }, "$dcrDetail.productPob", 0]
                  }
                }


              }

            },

            // Stage 6
            {
              $lookup: {
                "from": "UserInfo",
                "localField": "_id.submitBy",
                "foreignField": "userId",
                "as": "UserDetail"
              }
            },

            // Stage 7
            {
              $unwind: {
                path: "$UserDetail",
                preserveNullAndEmptyArrays: true
              }
            },
            {
              $unwind: {
                path: "$UserDetail.divisionId",
                preserveNullAndEmptyArrays: true
              }
            },
            {
              $lookup: {
                "from": "DivisionMaster",
                "localField": "UserDetail.divisionId",
                "foreignField": "_id",
                "as": "division"
              }
            },

            // Stage 8
            {
              $lookup: {
                "from": "District",
                "localField": "UserDetail.districtId",
                "foreignField": "_id",
                "as": "districtDetail"
              }
            },

            // Stage 9
            {
              $unwind: {
                path: "$districtDetail",
                preserveNullAndEmptyArrays: true
              }
            },
			
			

            // Stage 10
            {
              $project: {

                _id: 0,
                userId: "$_id.submitBy",
                month: "$_id.month",
                year: "$_id.year",
                UserName: "$UserDetail.name",
                UserDesignation: "$UserDetail.designation",
                DistrictName: "$districtDetail.districtName",
                divisionName: {
                  $arrayElemAt: ["$division.divisionName", 0]
              },
              divisionId: {
                  $arrayElemAt: ["$division._id", 0]
              },
                doctorPOB: 1,
                vendorPOB: 1


              }
            },

            // Stage 11
            {
              $group: {
                _id: {
                  userId: "$userId",
                  userName: "$UserName",
                  designation: "$UserDesignation",
                  DistrictName: "$DistrictName",
                  divisionId : "$divisionId",
                  divisionName : "$divisionName",
                },
                data: {
                  $push: {
                    month: "$month",
                    year: "$year",
                    doctorPOB: "$doctorPOB",
                    vendorPOB: "$vendorPOB"
                  }
                }
              }
            },

            function (err, POBResult) {
              if (err) {
                // console.log(err);
                return cb(err);
              }

              // console.log("POBResult.length : ");console.log(POBResult);
              if (POBResult.length > 0) {

                let currentDate = new Date();

                for (let i = 0; i < userRecords[0].userIds.length; i++) {

                  let matchedObj = POBResult.filter(function (obj) {
                    let userIds1 = JSON.stringify(userRecords[0].userIds[i]);
                    let userId = JSON.stringify(obj._id.userId);
                    return userId == userIds1;
                  });
                  //console.log(matchedObj.length);
                  if (matchedObj.length > 0) {
                    const monthArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                   
                    let pob = [];
                    for (const month of monthArray) {

                      let matchedObject = matchedObj[0].data.filter(function (obj) {
                        return month.month == obj.month && month.year == obj.year;

                      });

                      if (matchedObject.length > 0) {
                        pob.push({
                          docpob: matchedObject[0].doctorPOB,
                          venpob: matchedObject[0].vendorPOB
                        });

                      } else {
                        pob.push({
                          docpob: "-----",
                          venpob: "-----"
                        });
                      }
                    }
                    finalObject.push({
                      userName: userRecords[0].obj[i].name,
                      userDesignation: userRecords[0].obj[i].designation,
                      headquater: userRecords[0].obj[i].districtName,
                      divisionName: userRecords[0].obj[i].divisionName,
                      pob: pob

                    });

                  } else {
                    const monthArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                    let pob = [];
                    for (const month of monthArray) {
                      pob.push({
                        docpob: "-----",
                        venpob: "-----"
                      });

                    }

                    finalObject.push({
                      userName: userRecords[0].obj[i].name,
                      userDesignation: userRecords[0].obj[i].designation,
                      headquater: userRecords[0].obj[i].districtName,
                      divisionName: userRecords[0].obj[i].divisionName,
                      pob: pob
                    });
                    
                  }

                }
                
                
              } else {

                for (let i = 0; i < userRecords[0].userIds.length; i++) {
                  //console.log("userRecords[0].obj[i] :");console.log(userRecords[0].obj[i]);
                  const monthArray = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate);
                  let pob = [];
                  for (const month of monthArray) {


                    pob.push({
                      docpob: "-----",
                      venpob: "-----"
                    });

                  }

                  finalObject.push({
                    userName: userRecords[0].obj[i].name,
                    userDesignation: userRecords[0].obj[i].designation,
                    headquater: userRecords[0].obj[i].districtName,
                    divisionName: userRecords[0].obj[i].divisionName,
                    pob: pob
                  });

                }

              }
              //console.log("finalObject : ");console.log(finalObject);
              return cb(false, finalObject)
            });

        } else {
          cb(false, []);
        }
      });




  }
  Dcrmaster.remoteMethod(
    'getPobDetails', {
      description: 'Get POB Details',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],

      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }
    });
	
	
	//---------------------------PK(2019-11-14)-------------------------
	Dcrmaster.MangerDCRAnalysis = function(params, cb) {
        var self = this;
        //console.log("Method Called...", params);
        const dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        async.parallel({
            attendenceDetail: function(cb) {
                dcrCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.companyId),
                        submitBy: ObjectId(params.userId),
                        dcrDate: {
                            // $gte: new Date("2019-08-01"),
                            // $lte: new Date("2019-11-30")
                            $gte: new Date(params.fromDate),
                            $lte: new Date(params.toDate)
                        }
                    }
                }, {
                    $group: {
                        _id: {
                            month: {
                                $month: "$dcrDate"
                            },
                            year: {
                                $year: "$dcrDate"
                            },
                            workingStatus: "$workingStatus"

                        },
                        totalDays: {
                            $sum: 1
                        }
                    }
                }, {
                    $project: {
                        _id: 0,
                        dcrMonth: "$_id.month",
                        dcrYear: "$_id.year",
                        workingStatus: "$_id.workingStatus",
                        totalDays: "$totalDays"
                    }
                }, {
                    $group: {
                        _id: {
                            month: "$dcrMonth",
                            year: "$dcrYear"
                        },
                        workingStatus: {
                            $push: {
                                workingStatus: "$workingStatus",
                                totalDays: "$totalDays"
                            }
                        }
                    }
                }, {
                    $project: {
                        _id: 0,
                        month: "$_id.month",
                        year: "$_id.year",
                        workingStatus: 1
                    }
                }, {
                    $unwind: "$workingStatus"
                }, {
                    $group: {
                        _id: {
                            workingStatus: "$workingStatus.workingStatus"
                        },
                        data: {
                            $push: {
                                "month": "$month",
                                "year": "$year",
                                totalDays: { $sum: "$workingStatus.totalDays" }
                            }
                        }
                    }
                }, function(err, result) {
                    cb(false, result)
                });
            },
            workingType: function(cb) {
                const ActivityMasterCollection = self.getDataSource().connector.collection(Dcrmaster.app.models.Activity.modelName);
                ActivityMasterCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.companyId)
                    }
                }, {
                    $project: {
                        activitiesForWeb: 1
                    }
                }, function(err, result) {
                    return cb(false, result)
                });
            },
            providerMet: function(cb) {
                // let abc = {
                //     "unlistedValidations": {
                //         "unlistedDocCall": false,
                //         "unlistedVenCall": false,
                //         "unlistedDocAvg": false,
                //         "unlistedVenAvg": false
                //     }
                // }

                let dcrDataGroup = {
                    _id: {
                        //"providerId": "$providerId",
                        "month": "$month",
                        "year": "$year"
                    },
                    totalDCR: {
                        $addToSet: "$dcrId"
                    },
                    workingDCR: {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$DCRStatus", 1]
                                }]
                            }, "$dcrId", "$abc"]
                        }
                    },
                    totalDeviation: {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $ifNull: ["$deviateReason", false]
                                }]
                            }, "$dcrId", "$abc"]
                        }
                    },
                    totalJointWork: {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $size: ["$jointWork"]
                                }]
                            }, "$dcrId", "$abc"]
                        }
                    },
                    totalIndividualWork: {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $size: ["$jointWork"]
                                }]
                            }, "$abc", "$dcrId"]
                        }
                    },
                    totalJointWorkWith: {
                        "$addToSet": {
                            "$cond": [{
                                "$and": [{ "$size": ["$jointWork"] }]
                            }, { $arrayElemAt: ["$jointWork", 0] }, "$abc"]
                        }
                    }
                }
                let dcrDataProject = {
                        // _id: 0,
                        userId: "$_id.submitBy",
                        doctorCall: 1,
                        vendorCall: 1,
                        totalDCR: {
                            $size: "$totalDCR"
                        },
                        workingDCR: {
                            $size: "$workingDCR"
                        },
                        listedDoctorCall: {
                            $size: "$listedDoctorCall"
                        },
                        listedVendorCall: {
                            $size: "$listedVendorCall"
                        },
                        totalDeviation: {
                            $size: "$totalDeviation"
                        },
                        totalJointWork: {
                            $size: "$totalJointWork"
                        },
                        totalIndividualWork: {
                            $size: "$totalIndividualWork"
                        }
                    }
                    //-----Dynamic Group and Project For Doctors--------------------
                if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == true) {
                    dcrDataGroup.doctorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, 1, 0]
                        }
                    }
                    dcrDataGroup.listedDoctorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataProject.drCallAvg = {
                        $divide: ["$doctorCall", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedDoctorCallAvg = {
                        $divide: [{
                            $size: "$listedDoctorCall"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == false) {
                    dcrDataGroup.doctorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedDoctorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataProject.drCallAvg = {
                        $divide: ["$doctorCall", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedDoctorCallAvg = {
                        $divide: [{
                            $size: "$listedDoctorCall"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                } else if (params.unlistedValidations.unlistedDocCall == true && params.unlistedValidations.unlistedDocAvg == false) {
                    dcrDataGroup.doctorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.doctorCallForUnlistedAvgFalse = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedDoctorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataGroup.listedDoctorCallForUnlistedAvgFalse = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataProject.drCallAvg = {
                        $divide: ["$doctorCallForUnlistedAvgFalse", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedDoctorCallAvg = {
                        $divide: [{
                            $size: "$listedDoctorCallForUnlistedAvgFalse"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                } else if (params.unlistedValidations.unlistedDocCall == false && params.unlistedValidations.unlistedDocAvg == true) {
                    dcrDataGroup.doctorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.doctorCallForUnlistedAvgTrue = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedDoctorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }, {
                                    $ne: ["$providerStatus", "unlisted"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataGroup.listedDoctorCallForUnlistedAvgTrue = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                    $eq: ["$providerType", "RMP"]
                                }]
                            }, "$providerId", "$abc"]
                        }
                    }

                    dcrDataProject.drCallAvg = {
                        $divide: ["$doctorCallForUnlistedAvgTrue", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedDoctorCallAvg = {
                        $divide: [{
                            $size: "$listedDoctorCallForUnlistedAvgTrue"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                }
                //-----Dynamic Group and Project For Vendor--------------------
                if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == true) {
                    dcrDataGroup.vendorCall = {
                        $sum: {
                            $cond: [{

                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedVendorCall = {
                        $addToSet: {
                            $cond: [{

                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]


                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataProject.venCallAvg = {
                        $divide: ["$vendorCall", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedVendorCallAvg = {
                        $divide: [{
                            $size: "$listedVendorCall"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == false) {
                    dcrDataGroup.vendorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedVendorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataProject.venCallAvg = {
                        $divide: ["$vendorCall", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedVendorCallAvg = {
                        $divide: [{
                            $size: "$listedVendorCall"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                } else if (params.unlistedValidations.unlistedVenCall == true && params.unlistedValidations.unlistedVenAvg == false) {
                    dcrDataGroup.vendorCall = {
                        $sum: {
                            $cond: [{

                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]


                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.vendorCallForUnlistedAvgFalse = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, 1, 0]
                        }
                    }
                    dcrDataGroup.listedVendorCall = {
                        $addToSet: {
                            $cond: [{

                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]


                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataGroup.listedVendorCallForUnlistedAvgFalse = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataProject.venCallAvg = {
                        $divide: ["$vendorCallForUnlistedAvgFalse", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedVendorCallAvg = {
                        $divide: [{
                            $size: "$listedVendorCallForUnlistedAvgFalse"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }
                } else if (params.unlistedValidations.unlistedVenCall == false && params.unlistedValidations.unlistedVenAvg == true) {
                    dcrDataGroup.vendorCall = {
                        $sum: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, 1, 0]
                        }
                    }
                    dcrDataGroup.vendorCallForUnlistedAvgTrue = {
                        $sum: {
                            $cond: [{
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }, 1, 0]
                        }
                    }

                    dcrDataGroup.listedVendorCall = {
                        $addToSet: {
                            $cond: [{
                                $and: [{
                                        $ne: ["$providerStatus", "unlisted"]
                                    },
                                    {
                                        $or: [{
                                            $eq: ["$providerType", "Drug"]
                                        }, {
                                            $eq: ["$providerType", "Stockist"]
                                        }]
                                    }
                                ]
                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataGroup.listedVendorCallForUnlistedAvgTrue = {

                        $addToSet: {
                            $cond: [{
                                $or: [{
                                    $eq: ["$providerType", "Drug"]
                                }, {
                                    $eq: ["$providerType", "Stockist"]
                                }]
                            }, "$providerId", "$abc"]

                        }
                    }

                    dcrDataProject.venCallAvg = {
                        $divide: ["$vendorCallForUnlistedAvgTrue", {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                    dcrDataProject.listedVendorCallAvg = {
                        $divide: [{
                            $size: "$listedVendorCallForUnlistedAvgTrue"
                        }, {
                            $cond: {
                                if: {
                                    $gte: [{ $size: "$workingDCR" }, 1]
                                },
                                then: { $size: "$workingDCR" },
                                else: 1 // used 1 for divided by zero
                            }
                        }]
                    }

                }

                // console.log("=====================================");
                // console.log(JSON.stringify(dcrDataGroup));
                // console.log("=====================================");
                // console.log(JSON.stringify(dcrDataProject));
                // console.log("=====================================");

                dcrCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.companyId),
                        submitBy: ObjectId(params.userId),
                        dcrDate: {
                            //$gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                            //$lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")
                            // $gte: new Date("2019-08-01"),
                            // $lte: new Date("2019-11-30")
                            $gte: new Date(params.fromDate),
                            $lte: new Date(params.toDate)
                        }
                    }
                }, {
                    $lookup: {
                        "from": "DCRProviderVisitDetails",
                        "localField": "_id",
                        "foreignField": "dcrId",
                        "as": "dcrProviders"
                    }
                }, {
                    $unwind: {
                        path: "$dcrProviders",
                        preserveNullAndEmptyArrays: true,
                        includeArrayIndex: "arrayIndex"
                    }
                }, {
                    $project: {
                        dcrId: "$_id",
                        dcrDate: "$dcrDate",
                        workingStatus: 1,
                        providerType: "$dcrProviders.providerType",
                        providerId: "$dcrProviders.providerId",
                        providerStatus: "$dcrProviders.providerStatus",
                        submitBy: 1,
                        arrayIndex: 1,
                        providerStatus: "$dcrProviders.providerStatus",
                        DCRStatus: 1,
                        month: { $month: '$dcrDate' },
                        year: { $year: '$dcrDate' },
                        deviateReason: 1,
                        jointWork: 1
                    }
                }, {
                    $group: dcrDataGroup
                }, {
                    $project: dcrDataProject
                }, {
                    cursor: {
                        batchSize: 50
                    },
                    allowDiskUse: true
                }, function(err, result) {
                    //console.log(result);

                    cb(false, result)
                });
            },
            jointWork: function(cb) {
                dcrCollection.aggregate({
                    $match: {
                        companyId: ObjectId(params.companyId),
                        submitBy: ObjectId(params.userId),
                        dcrDate: {
                            //$gte: new Date(moment(params.dateRange.begin).format("YYYY-MM-DD")), //new Date("2019-09-01"),
                            //$lte: new Date(moment(params.dateRange.end).format("YYYY-MM-DD")) //new Date("2019-09-30")
                            // $gte: new Date("2019-08-01"),
                            // $lte: new Date("2019-11-30")
                            $gte: new Date(params.fromDate),
                            $lte: new Date(params.toDate)
                        },
                        DCRStatus: 1 //For Working..
                    }
                }, {
                    $group: {
                        "_id": { "month": { $month: "$dcrDate" }, "year": { $year: "$dcrDate" } },
                        "totalJointWork": { "$addToSet": { "$cond": [{ "$and": [{ "$size": ["$jointWork"] }] }, "$_id", "$abc"] } },
                        "totalJointWorkWith": { "$push": { "$cond": [{ "$and": [{ "$size": ["$jointWork"] }] }, { $arrayElemAt: ["$jointWork", 0] }, "$abc"] } },
                    }
                }, {
                    $unwind: "$totalJointWorkWith"
                }, {
                    $group: {
                        _id: {
                            month: "$_id.month",
                            year: "$_id.year",
                            jointName: "$totalJointWorkWith.Name",
                            joinId: "$totalJointWorkWith.id"
                        },
                        totalVisit: {
                            $sum: 1
                        }
                    }
                }, {
                    $group: {
                        _id: {
                            jointName: "$_id.jointName",
                            jointId: "$_id.joinId"
                        },
                        jointVisitData: {
                            $push: {
                                totalVisit: "$totalVisit",
                                month: "$_id.month",
                                year: "$_id.year"
                            }
                        }
                    }
                }, {
                    $project: {
                        _id: 0,
                        jointName: "$_id.jointName",
                        jointId: "$_id.jointId",
                        jointVisitData: 1
                    }
                }, {
                    $sort: {
                        jointName: 1
                    }
                }, function(err, result) {
                    return cb(false, result)
                })
            }
        }, function(err, asyncResult) {
            if (asyncResult) {
                const attendenceDetail = asyncResult.attendenceDetail;
                const workingType = asyncResult.workingType;
                const jointWork = asyncResult.jointWork;
                const providerMet = asyncResult.providerMet;

                const monthARRAY = monthNameAndYearBTWTwoDate(params.fromDate, params.toDate)
                    //const monthARRAY = monthNameAndYearBTWTwoDate("2019-08-01", "2019-11-30");
                let finalObject = [];
                let FinalWorkingType = [];
                for (const workingObject of workingType[0].activitiesForWeb) {
                    const attendenceFound = attendenceDetail.filter(function(obj) {
                        return obj._id.workingStatus === workingObject.name
                    });
                    if (attendenceFound.length > 0) {
                        let monthFound = [];
                        for (const mnth of monthARRAY) {
                            const attendenceMonthFound = attendenceFound[0].data.filter(function(obj) {
                                return (obj.month === mnth.month && obj.year === mnth.year)
                            });
                            if (attendenceMonthFound.length > 0) {
                                monthFound.push({
                                    month: mnth.month,
                                    year: mnth.year,
                                    totalDays: attendenceMonthFound[0].totalDays
                                });
                            } else {
                                monthFound.push({
                                    month: mnth.month,
                                    year: mnth.year,
                                    totalDays: 0
                                });
                            }
                        }
                        FinalWorkingType.push({
                            type: attendenceFound[0]._id.workingStatus,
                            monthData: monthFound
                        });
                    } else {
                        let monthData = [];
                        for (const mnth of monthARRAY) {
                            monthData.push({
                                month: mnth.month,
                                year: mnth.year,
                                totalDays: 0
                            });
                        }
                        FinalWorkingType.push({
                            type: workingObject.name,
                            monthData: monthData
                        });
                    }
                }
                //console.log(FinalWorkingType);
                let totalNumberOfDayInMonth = [];
                for (const mnth of monthARRAY) {
                    totalNumberOfDayInMonth.push(mnth.endDate);
                }

                let FinalProviderData = {
                    doctorCall: [],
                    listedDoctorCall: [],
                    listedDoctorCall: [],
                    listedDoctorCallAvg: [],
                    totalDeviation: [],
                    totalJointWork: [],
                    totalIndividualWork: [],
                    listedVendorCall: [],
                    listedVendorCallAvg: []

                };
                for (const mnth of monthARRAY) {
                    const providerDataFound = providerMet.filter(function(obj) {
                        return (obj._id.month === mnth.month && obj._id.year === mnth.year)
                    });
                    if (providerDataFound.length > 0) {
                        FinalProviderData.doctorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            doctorCall: providerDataFound[0].doctorCall
                        });
                        FinalProviderData.listedDoctorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedDoctorCall: providerDataFound[0].listedDoctorCall
                        });
                        FinalProviderData.listedDoctorCallAvg.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedDoctorCallAvg: providerDataFound[0].listedDoctorCallAvg.toFixed(2)
                        });
                        FinalProviderData.totalDeviation.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalDeviation: providerDataFound[0].totalDeviation
                        });
                        FinalProviderData.totalJointWork.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalJointWork: providerDataFound[0].totalJointWork
                        });
                        FinalProviderData.totalIndividualWork.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalIndividualWork: providerDataFound[0].totalIndividualWork
                        });
                        FinalProviderData.listedVendorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedVendorCall: providerDataFound[0].listedVendorCall
                        });
                        FinalProviderData.listedVendorCallAvg.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedVendorCallAvg: providerDataFound[0].listedVendorCallAvg.toFixed(2)
                        });

                    } else {
                        FinalProviderData.doctorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            doctorCall: 0
                        });
                        FinalProviderData.listedDoctorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedDoctorCall: 0
                        });
                        FinalProviderData.listedDoctorCallAvg.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedDoctorCallAvg: 0
                        });
                        FinalProviderData.totalDeviation.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalDeviation: 0
                        });
                        FinalProviderData.totalJointWork.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalJointWork: 0
                        });
                        FinalProviderData.totalIndividualWork.push({
                            month: mnth.month,
                            year: mnth.year,
                            totalIndividualWork: 0
                        });
                        FinalProviderData.listedVendorCall.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedVendorCall: 0
                        });
                        FinalProviderData.listedVendorCallAvg.push({
                            month: mnth.month,
                            year: mnth.year,
                            listedVendorCallAvg: 0
                        });
                    }
                }

                let FinalJointWorkData = [];
                for (const jw of jointWork) {
                    let monthData = []
                    for (const mnth of monthARRAY) {
                        const jointWorkMonthFound = jw.jointVisitData.filter(function(obj) {
                            return (obj.month === mnth.month && obj.year === mnth.year)
                        });
                        if (jointWorkMonthFound.length > 0) {
                            monthData.push({
                                month: mnth.month,
                                year: mnth.year,
                                totalVisit: jointWorkMonthFound[0].totalVisit
                            });
                        } else {
                            monthData.push({
                                month: mnth.month,
                                year: mnth.year,
                                totalVisit: 0
                            });
                        }
                    }
                    FinalJointWorkData.push({
                        emp: jw.jointName,
                        visit: monthData
                    });
                }

                finalObject.FinalWorkingType = FinalWorkingType;
                finalObject.FinalProviderData = FinalProviderData;
                finalObject.FinalJointWorkData = FinalJointWorkData;
                let FinalReturnObject = {
                        FinalWorkingType: FinalWorkingType,
                        FinalProviderData: FinalProviderData,
                        FinalJointWorkData: FinalJointWorkData,
                        FinalTotalNumberOfDayInMonth: totalNumberOfDayInMonth
                    }
                    // console.log(FinalWorkingType);
                    // console.log(FinalProviderData);
                    // console.log(FinalJointWorkData);
                cb(false, FinalReturnObject)
            } else {
                cb(false, [])
            }
        });

    };
    Dcrmaster.remoteMethod(
        'MangerDCRAnalysis', {
            description: 'Manager DCR Analysis',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
	
	 //--------------------API for scheduling sunday submission-----------------//
    Dcrmaster.SubmitSundaysOnDCRMaster = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        let DCRObject=[];

        // var year = moment().year();
        // var month = moment().month();
        // month = month + 1;
        //  let sundayDate="";
        // let fromDate = new moment.utc(year + "-" + month + "-01").startOf('month').format("YYYY-MM-DD");
        // let toDate = new moment.utc(year + "-" + month + "-01").endOf("month").format("YYYY-MM-DD");
        
        // var start = moment(fromDate), 
        //     end = moment(toDate), 
        //     day = 0;         

        // var result = [];
        // var current = start.clone();
         
        // while (current.day(7 + day).isBefore(end)) {
        //     result.push(current.clone());
        // }
     
        // sundayDate=result.map(m => m.format('YYYY-MM-DD'));
        var  date=moment(new Date()).format("YYYY-MM-DD");
          // var dayNum = moment().day();  //0 is Sunday---
        var dayNum=0;  // taking as tesing purspose only--
        if(params.length>0){
             for(var i=0;i<params.length;i++){
                         DCRObject.push({ 
                            "companyId" : ObjectId(params[i].companyId), 
                            "dcrDate" : new Date(date), 
                            "stateId" :ObjectId(params[i].stateId), 
                            "districtId" : ObjectId(params[i].districtId), 
                            "visitedBlock" : [
                
                            ], 
                            "workingStatus" : "Holiday", 
                            "jointWork" : [
                
                            ], 
                            "remarks" : "", 
                            "timeIn" : new Date(),
                            "timeOut" : new Date(),
                            "createdAt" : new Date(), 
                            "updatedAt" : new Date(),
                            "submitBy" : ObjectId(params[i].submitBy), 
                            "reportedFrom" : "web scheduler", 
                            "geoLocation" : [
                               
                            ], 
                            "dcrSubmissionDate" : new Date(),
                            "punchDate" : new Date(),
                            "callModifiedDate" : new Date(),
                            "DCRStatus" : 0, 
                            "dcrVersion" : "complete", 
                            "ipAddress" : "", 
                            "isDayPlan" : false
                         })
                        

                      
         }//---end of for loop---

         Dcrmaster.create(DCRObject, function (err, resultSubmission) {
           if (err != null) {
             
           } else {

           }
       });
 
    }else{
        return cb(false,[])
    }     

    };

    Dcrmaster.remoteMethod(
        'SubmitSundaysOnDCRMaster', {
            description: 'Insert Sundays',
            accepts: [
                {
                    arg: 'companyId',
                    type: 'string'
                }
            ]
        }
    )

    //--------------------------------------------------
	 //--------------get detailed doctor dcr report preeti arora 3-12-2019---------
    Dcrmaster.getDCRDetailDoctorWise = function (params, cb) {

        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);

        Dcrmaster.find({
            where: {
                companyId: params.companyId,
                id: { inq: params.dcrIds }
            }, "include": [
                {
                    "relation": "info",
                    "scope":
                    {
                        "include": [{
                            "relation": "providerinfo",
                            "scope": { "fields": ["providerName", "address"] }
                        }
                            , { "relation": "block", "scope": { "fields": ["areaName"] } }
                            , { "relation": "file" }]
                    }
                },
                {
                    "relation": "userInfo",
                    "scope": { "fields": ["name", "districtName"] }
                }]
        }, function (err, DCRResult) {
            return cb(false, DCRResult);

        })

    }
        ,
        Dcrmaster.remoteMethod(
            'getDCRDetailDoctorWise', {
                description: 'getDCRDetailDoctorWise',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );
    //--------------------end--------------------
	 // ------------------Get Locked DCR- Details---by preeti--12-11-2019-----------------------
    Dcrmaster.getAllLockedDCR = function (params, cb) {
        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        let filter = {};
        let match = {}
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        let divisionArr = [];
        let userNotFound = [];
        let dcrResult = [];
        if (params.lockFilterType == "state") {
            for (var i = 0; i < params.stateId.length; i++) {
                stateIdsArr.push(ObjectId(params.stateId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { inq: stateIdsArr },
            }

        } else if (params.lockFilterType == "Headquarter") {

            for (var i = 0; i < params.districtId.length; i++) {
                hqIdArray.push(ObjectId(params.districtId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { inq: hqIdArray }
            }

        }
        else if (params.lockFilterType == "employee") {
            for (var i = 0; i < params.userId.length; i++) {
                empArray.push(ObjectId(params.userId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { inq: empArray }
            }

        }
        /*if (params.divisionId != null) {
            for (var i = 0; i < params.divisionId.length; i++) {
                divisionArr.push(ObjectId(params.divisionId[i]));
            }
            filter.divisionId = { inq: divisionArr }
        }*/
		
	
        Dcrmaster.app.models.TrackLockOpenDetail.find({
            where: filter,
            "ReleasedStatus" : "pending"
        },
            function (err, response) {
                if(response.length>0){
                    Dcrmaster.app.models.UserInfo.getLockedDetails(response,function(err, res){
                        return cb(false,res)
                    })
                }else{
                    return cb(false,response)
                }
            })
    },
        Dcrmaster.remoteMethod(
            'getAllLockedDCR', {
                description: 'getLockedDCR',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );


    //------------------------
	 //-------------insert locked DCR-----------------------//
    Dcrmaster.insertAllLockedDCR = function (params, cb) {

        var self = this;
        var DcrmasterCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        let filter = {};
        let match = {}
        let stateIdsArr = [];
        let hqIdArray = [];
        let empArray = [];
        let divisionArr = [];
        const finalLockedData = [];
        let matchOfGetUsers = {};
        matchOfGetUsers.companyId = ObjectId(params.companyId);
        if (params.lockFilterType == "state") {
            for (var i = 0; i < params.stateId.length; i++) {
                stateIdsArr.push(ObjectId(params.stateId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                stateId: { inq: stateIdsArr },
            }
            matchOfGetUsers.stateId = { $in: stateIdsArr }
        } else if (params.lockFilterType == "Headquarter") {
            for (var i = 0; i < params.districtId.length; i++) {
                hqIdArray.push(ObjectId(params.districtId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                districtId: { inq: hqIdArray }
            }
            matchOfGetUsers.districtId = { $in: hqIdArray }

        }
        else if (params.lockFilterType == "employee") {
            for (var i = 0; i < params.userId.length; i++) {
                empArray.push(ObjectId(params.userId[i]));
            }
            filter = {
                companyId: ObjectId(params.companyId),
                userId: { inq: empArray }
            }
            matchOfGetUsers.userId = { $in: empArray }

        }
        if (params.divisionId != null) {
            for (var i = 0; i < params.divisionId.length; i++) {
                divisionArr.push(ObjectId(params.divisionId[i]));
            }
            filter.divisionId = { inq: divisionArr }
            matchOfGetUsers.divisionId = { $in: divisionArr }

        }

        Dcrmaster.app.models.UserInfo.find({
            where: filter,
        },
            function (err, response) {

                let userIds = [];
                for (var i = 0; i < response.length; i++) {
                    userIds.push(response[i].userId)
                }
                let currentDate = moment.utc().format("YYYY-MM-DDT00:00:00.000Z");
                let splitDate = currentDate.split("-");
                let splitDay = splitDate[2].split("T");
                let Day = splitDay[0];  //current date
                let getLockedDay = 0;
                let checkLockToDay = 0;
                let month = 0
                let userNotFound = [];
                for (var i = 0; i < response.length; i++) {
                    getLockedDay = response[i].lockingPeriod;

                    if (Day < getLockedDay) {
                        month = splitDate[1] - 1;
                        var totalDays = moment([splitDate[0], month - 1]).daysInMonth();
                        checkLockToDay = totalDays - (getLockedDay - Day); // to get the from date
                    } else {
                        month = splitDate[1]
                        checkLockToDay = Day - getLockedDay;
                    }


                    let toDate = splitDate[0] + "-" + month + "-" + checkLockToDay;
                    let dcrDate = {
                        $lte: new Date(toDate)
                    }
                    match.dcrDate = dcrDate;
                    match.companyId = ObjectId(params.companyId),
                    match.submitBy = ObjectId(response[i].userId);
                    // console.log("jsbdhjsbdhj---",match);

                    DcrmasterCollection.aggregate(
                        {
                            $match: match

                        },
                        // Stage 2
                        {
                            $sort: {
                                "dcrDate": 1
                            }
                        },

                        // Stage 4
                        {
                            $project: {
                                companyId: 1,
                                stateId: 1,
                                districtId: 1,
                                submitBy: 1,
                                dcrDate: 1,
                            }
                        },
                        function (err, result) {

                            if (result.length > 0) {
                                var startDate = result[0].dcrDate; //YYYY-MM-DD
                                var endDate = new Date(toDate) //YYYY-MM-DD

                                var getDateArray = function (start, end) {
                                    var arr = new Array();
                                    var dt = new Date(start);
                                    while (dt <= end) {
                                        arr.push(new Date(dt));
                                        dt.setDate(dt.getDate() + 1);
                                    }
                                    return arr;
                                }
                                const data = getDateArray(startDate, endDate);

                                if (result.length > 0) {

                                    for (const iterator of data) {
                                        const dcrData = result.filter(obj => {
                                            return moment(obj.dcrDate).format("YYYY-MM-DD") == moment(iterator).format("YYYY-MM-DD")
                                        });

                                        if (dcrData.length == 0) {

                                            finalLockedData.push({
                                                companyId: result[0].companyId,
                                                userId: result[0].submitBy,
                                                DCRLockedDate: new Date(moment(iterator).format("YYYY-MM-DD")),
                                                stateId: result[0].stateId,
                                                districtId: result[0].districtId,
                                                ReleasedStatus: "pending",
                                                lockOpenDate: new Date(),
                                                createdAt: new Date(),
                                                updatedBy: new Date(),

                                            })

                                        }
                                    }
                                }

                                Dcrmaster.app.models.TrackLockOpenDetail.create(finalLockedData, function (err, resultSubmission) {
                                    if (err != null) {

                                    } else {

                                    }
                                });


                            }
                        })



                }   //end of for loop

                // Dcrmaster.app.models.UserInfo.updateAll({
                //     userId: { inq: userIds }
                // }, {
                //         releaseDCRLockedDate: new Date(),
                //         updatedAt: new Date()
                //     }, function (err, res) {
                //     })


            })
        return cb(false, true);


    },
        Dcrmaster.remoteMethod(
            'insertAllLockedDCR', {
                description: 'getLockedDCR',
                accepts: [{
                    arg: 'params',
                    type: 'object'
                }],
                returns: {
                    root: true,
                    type: 'array'
                },
                http: {
                    verb: 'get'
                }
            }
        );


    //-------------------------------------------------

 //getting Manager TrerritoryWise Done 
  Dcrmaster.mgrWorkedWithTrerritoryWise = function (params, cb) {
    var self = this;
    var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
    let finalObject=[];
    dcrCollection.aggregate({
    // Stage 1
    
      $match: {
      companyId :ObjectId(params.companyId),
      submitBy : ObjectId(params.submitBy),
      dcrDate : {
       $gte : new Date(params.fromDate),
       $lte : new Date(params.toDate)
      },
      
      }
    },

    // Stage 2
    {
      $project: {
      dcrDate : 1,
              visitedBlock : 1,
                "jointWorkCount" : {
                $size : "$jointWork"
                
                },
                jointWork : 1
            }
    },

    // Stage 3
    {
      $match: {
       jointWorkCount :{
                $gt : 0
                } 
      }
    },

    // Stage 4
    {
      $unwind: {
       path: "$jointWork",
       includeArrayIndex: "index",
       preserveNullAndEmptyArrays: true
      }
    },

    // Stage 5
    {
      $unwind: {
       path: "$visitedBlock",
       includeArrayIndex: "index",
       preserveNullAndEmptyArrays: true
      }
    },

    // Stage 6
    {
      $group: {
      _id: {
           dcrDate : "$dcrDate"
        },
        visitedBlockFrom :{
        $addToSet : "$visitedBlock.from"
        },
        visitedBlockTo :{
        $addToSet : "$visitedBlock.to"
        },
        jointWorkDetail :{
        $addToSet : "$jointWork"
        }
      }
    },

    // Stage 7
    {
      $project: {
          month : {$month : "$_id.dcrDate"},
          year: {$year: "$_id.dcrDate"},
          dcrDate : "$_id.dcrDate",
          visitedBlock : {$setUnion : ["$visitedBlockFrom","$visitedBlockTo"]},
          jointWorkDetail : 1
      }
    },

    // Stage 8
    {
      $unwind: "$visitedBlock"
    },

    // Stage 9
    {
      $unwind: "$jointWorkDetail"
    },

    // Stage 10
    {
      $group: {
        _id :{
        block : "$visitedBlock",
        jointId : "$jointWorkDetail.id",
        jointName : "$jointWorkDetail.Name",
        month : "$month",
        year : "$year"
        },
         totalSum :{
              $sum : 1
              },
        data :{
        $addToSet :{
        jointId : "$jointWorkDetail.id",
        jointName : "$jointWorkDetail.Name",
        block : "$visitedBlock",
        month : "$month",
        year : "$year"
        }
        },
       
    }
    },

    // Stage 11
    {
      $project: {
        mrName: {$arrayElemAt: ["$data.jointName", 0]},
        mrId: {$arrayElemAt: ["$data.jointId", 0]},
        block :"$_id.block",
        total : "$totalSum",
        month :"$_id.month",
        year : "$_id.year",
        data : "$data"
        
      }
    },{
      $sort: {
        mrId: 1,
        blockId : 1
    }

    },// Stage 12
    {
      $group: {
      _id: {
        mrId: "$mrId",
        mrName : "$mrName",
        block : "$block",
      },
        data :{	$push : {mrId: "$mrId",block : "$block",month :"$month",year : "$year",total : "$total"} }
      
      }
    },
  // Options
  {
    cursor: {
      batchSize: 50
    },

    allowDiskUse: true
  },
  function (err, result) {
    if (err) {
      // console.log(err);
      return cb(err);
    }
if(result.length>0){

const monthArray =   monthNameAndYearBTWTwoDate(params.fromDate,params.toDate);
for (let i = 0; i < result.length; i++) {
 // console.log(result[i].data[0].total);
  let details=[];
  for (const month of monthArray) {	
    					   
    let matchedObject = result.filter(function (obj) {
      return obj.data[0].month == month.month && obj.data[0].year==month.year;
    });
    //console.log("matchedObject[i]:",matchedObject[i]);
    if (matchedObject.length > 0) {
      if(matchedObject[i]==undefined){
        details.push("0");
      }else{
      details.push(matchedObject[i].data[0].total)
      }
    } else {
      details.push("0");
    }
  }
   finalObject.push({
    userName : result[i]._id.mrName,
    areaName: result[i]._id.block,
    data: details
  });
}
}
//console.log("finalObject :",finalObject);
cb(false, finalObject);

  });    
  }
  Dcrmaster.remoteMethod(
    'mgrWorkedWithTrerritoryWise', {
      description: 'Getting Trerritory Wise Manager report',
      accepts: [{
        arg: 'params',
        type: 'object'
      }],
      returns: {
        root: true,
        type: 'array'
      },
      http: {
        verb: 'get'
      }
    }
  );
  //---------DCR Status by preeti arora--26-12-2019-----------
    Dcrmaster.getDCRStatusNew = function (params, cb) {
        var self = this;
        var dcrCollection = self.getDataSource().connector.collection(Dcrmaster.modelName);
        var aMatch = {};
            Dcrmaster.app.models.UserInfo.getAllUserIdsBasedOnType(params,
                function (err, userRecords) {
                    if (userRecords.length > 0) {
                        dcrCollection.aggregate(
                            // Stage 1
                            {
                                $match:{ 
                                    submitBy: { $in: userRecords[0].userIds },
                                    dcrDate: {
                                        $gte: new Date(params.fromDate),
                                        $lte: new Date(params.toDate)
                                    },
                                    companyId:ObjectId(params.companyId)
                            
                            }
                            },

                            // Stage 2
                            {
                                $group: {
                                    _id: {
                                        "submitBy": "$submitBy"
                                    },
                                    totalDCR: {
                                        $addToSet: "$dcrDate"
                                    },
                                    lastDCRDate: {
                                        $last: "$dcrDate"
                                    }

                                }
                            },

                            // Stage 3
                            {
                                $lookup: {
                                    "from": "UserInfo",
                                    "localField": "_id.submitBy",
                                    "foreignField": "userId",
                                    "as": "UserDetail"
                                }
                            },

                            // Stage 4
                            {
                                $unwind: {
                                    path: "$UserDetail",
                                    preserveNullAndEmptyArrays: true
                                }
                            },

                            // Stage 5
                            {
                                $unwind: {
                                    path: "$UserDetail.divisionId",
                                    preserveNullAndEmptyArrays: true
                                }
                            },

                            // Stage 6
                            {
                                $lookup: {
                                    "from": "District",
                                    "localField": "UserDetail.districtId",
                                    "foreignField": "_id",
                                    "as": "district"
                                }
                            },

                            // Stage 7
                            {
                                $lookup: {
                                    "from": "UserLogin",
                                    "localField": "UserDetail.userId",
                                    "foreignField": "_id",
                                    "as": "userLogin"
                                }
                            },

                            // Stage 8
                            {
                                $unwind: {
                                    path: "$userLogin",
                                    preserveNullAndEmptyArrays: true
                                }
                            },

                            // Stage 9
                            {
                                $lookup: {
                                    "from": "DivisionMaster",
                                    "localField": "UserDetail.divisionId",
                                    "foreignField": "_id",
                                    "as": "division"
                                }
                            },

                            // Stage 10
                            {
                                $project: {
                                    _id: 0,
                                    userId: "$_id.submitBy",
                                    totalDCR: {
                                        $size: "$totalDCR"
                                    },
                                    divisionName: {
                                        $arrayElemAt: ["$division.divisionName", 0]
                                    },
                                    divisionId: {
                                        $arrayElemAt: ["$division._id", 0]
                                    },
                                    lastDCRDate: "$lastDCRDate",
                                    UserName: "$UserDetail.name",
                                    Headquarter: { $arrayElemAt: ["$district.districtName", 0] },
                                    UserDesignation: "$UserDetail.designation",
                                    Mobile: "$userLogin.mobile",
                                    dateOfJoining: "$userLogin.dateOfJoining",
                                    employeeCode: "$userLogin.username",
                                    DCRStatus:{$literal:"YES"}

                                }
                            },function(err,response){
                             for (const user of userRecords[0].obj) {
                                 let data = [];

                                 const userNotFound = response.filter(function (obj) {
                                   return user.userId.toString() == obj.userId.toString() 
                                 });

                                 if(userNotFound.length==0){
                                    response.push({
                                        userId:user.userId ,
                                        totalDCR: 0,
                                        divisionName: user.divisionName,
                                        UserName:user.name,
                                        Headquarter: user.districtName,
                                        UserDesignation: user.designation,
                                        Mobile: user.mobile,
                                        dateOfJoining: user.dateOfJoining,
                                        employeeCode: user.employeeCode,
										lastDCRDate:"---",
                                        DCRStatus:"NO"
                                    })

                                 }
                                 
                             } 
                                console.log("response==",response)
                                return cb(false,response)
                            
                            }
                        )

                    }else{
						return cb(false,[])
					}
                })

        
       


    }
    Dcrmaster.remoteMethod(
        'getDCRStatusNew', {
            description: 'This method is used to get DCR Status',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );
    //---------------------------

};
