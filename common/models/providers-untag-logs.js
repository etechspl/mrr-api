'use strict';
var ObjectId = require('mongodb').ObjectID;
module.exports = function(ProvidersUntagLogs) {


    
    ProvidersUntagLogs.getProviderLocationUnTaggedCounts = function (params, cb) {

        var ProvidersUntagLogsCollection = this.getDataSource().connector.collection(ProvidersUntagLogs.modelName);

        ProvidersUntagLogsCollection.aggregate(

          // Pipeline
          [
            // Stage 1
            {
              $match: {
               companyId: ObjectId(params.companyId),
               userId: ObjectId(params.userId),
              }
            },
        
            // Stage 2
            {
              $group: {
              
                              _id: {
                                companyId:"$companyId",
                                stateId : "$stateId",
                                districtId : "$districtId",
                                blockId:"$blockId",
                                userId:"$userId",
                                providerId:"$providerId"
                              },
                              total:{$sum:1}
                              
              }
            },
        
            // Stage 3
            {
              $lookup: {
                  "from" : "State",
                  "localField" : "_id.stateId",
                  "foreignField" : "_id",
                  "as" : "stateDetails"
              }
            },
        
            // Stage 4
            {
              $lookup: {
                  "from" : "District",
                  "localField" : "_id.districtId",
                  "foreignField" : "_id",
                  "as" : "districtDetails"
              }
            },
        
            // Stage 5
            {
              $lookup: {
                  "from" : "UserInfo",
                  "localField" : "_id.userId",
                  "foreignField" : "userId",
                  "as" : "userDetails"
              }
            },
        
            // Stage 6
            {
              $lookup: {
                  "from" : "Area",
                  "localField" : "_id.blockId",
                  "foreignField" : "_id",
                  "as" : "blockDetails"
              }
            },
        
            // Stage 7
            {
              $lookup: {
                  "from" : "Providers",
                  "localField" : "_id.providerId",
                  "foreignField" : "_id",
                  "as" : "providerDetails"
              }
            },
        
            // Stage 8
            {
              $project: {
                 id :1,
                 state : { $arrayElemAt: ["$stateDetails.stateName", 0] },
                 district : { $arrayElemAt: ["$districtDetails.districtName", 0] },
                 username : { $arrayElemAt: ["$userDetails.name", 0] },
                 block :{ $arrayElemAt: ["$blockDetails.areaName", 0] },
                 provider :{ $arrayElemAt: ["$providerDetails.providerName", 0] },
                 total: "$total"
              }
            }
          ],
        
          // Options
          {
            cursor: {
              batchSize: 50
            },
            allowDiskUse: true
          },function (err, result) {
            if (result.length > 0) {
              return cb(false, result);
          } else {
              return cb(false, []);
          }
                
            });
          
    }

    ProvidersUntagLogs.remoteMethod(
        'getProviderLocationUnTaggedCounts', {
        description: 'This method is used to get the all providers that have untagged location',
        accepts: [{
            arg: 'param',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }
    }
    )
       

};
