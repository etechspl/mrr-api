﻿'use strict';

module.exports = function(Logininfo) {
Logininfo.onlineTrackerDetail = function(params, cb) {
       
       /*
        Sample params object
        //State Wise
        {          "designationLevel":0,          "companyId" : "5bea8423e7e95f19bc9bc417",          "type" : "State",           "stateIds" : ["57a3e5a04b80a50555312b07"],           "fromDate" : "2018-06-01",           "toDate" : "2018-07-31",          "districtIds":[]         }
       */
        var self = this;
        var LoginInfoCollection = self.getDataSource().connector.collection(Logininfo.modelName);
        async.parallel({
            userResult: function(cb) {
             if(params.designationLevel == 0){
                if (params.type == "State" || params.type == "Headquarter") {

                    Logininfo.app.models.UserInfo.getAllUserIdsBasedOnType(
                        params.companyId,
                        params.type,
                        params.stateIds,
                        params.districtIds,
                        function(err, userRecords) {
                            cb(false, userRecords);
                        });
                } else if (params.type == "Employee Wise") {
                    Logininfo.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                        params.companyId,
                        params.status,
                        params.userIds,
                        function(err, userRecords) {
                            cb(false, userRecords);
                        });
                }
             }else if(params.designationLevel>1 && params.designationLevel != 0){
                    var hierarchyParams={};
                   if (params.type == "State" || params.type == "Headquarter") {

                    hierarchyParams={
                     companyId: params.companyId, 
                     type: params.type,
                     stateIds: params.stateIds,
                     districtIds: params.districtIds, 
                     supervisorIds: params.supervisorIds
                    }
                    Logininfo.app.models.Hierarchy.getAllUserIdsOfManagerBasedOnType(
                        hierarchyParams,
                        function(err, userRecords) {
                            cb(false, userRecords);
                        });
                    } else if (params.type == "Employee Wise") {
                        hierarchyParams={
                         companyId: params.companyId, 
                         type: params.type,
                         stateIds: params.stateIds,
                         supervisorIds: params.supervisorIds,
                         userId: params.userIds,
                        }
                        Logininfo.app.models.Hierarchy.getAllUserIdsOfManagerBasedOnType(
                            hierarchyParams,
                            function(err, userRecords) {
                                cb(false, userRecords);
                            });
                    } 
             }else if(params.designationLevel == 1){
                    Dcrprovidervisitdetails.app.models.UserInfo.getAllUserIdsOnCompanyBasis(
                            params.companyId,
                            params.status,
                            params.userIds,
                            function(err, userRecords) {
                                cb(false, userRecords);
                    });
                    
             }
            }
        }, function(err, asyncResult) {
           
            if (err) {
                console.log(err);
            }
            if (asyncResult.userResult.length > 0) {
               		LoginInfoCollection.aggregate(
                    
                    {
                        $match: {
                        	 "userId" : { $in: asyncResult.userResult[0].userIds},
                        	 "companyId" :ObjectId(params.companyId),
 							 "loginTime": {
                                  $gte: new Date(params.fromDate),
                                  $lte: new Date(params.toDate)
                            }
                        }
                    },
                    {
                        $project: {
                          loginTime:1,logoutTime:1,userId:1,ipAddress:1
                        }
                    },{
                        $sort:{
                            loginTime:1
                        }
                    },
                    function(err, loginInfo) {
                       
                       let finalResult = [];
                        for (let i = 0; i < asyncResult.userResult[0].obj.length; i++) {

                            let filterObj = loginInfo.filter(function(obj) {
                                return obj.userId.toString() == asyncResult.userResult[0].obj[i].userId;
                            });
                            
                            if (filterObj.length > 0) {
                                for(var d=0; d<filterObj.length; d++){
                                    
                                        finalResult.push({
                                        "stateName": asyncResult.userResult[0].obj[i].stateName,
                                        "Headquarter": asyncResult.userResult[0].obj[i].districtName,
                                        "empName": asyncResult.userResult[0].obj[i].name,
                                        "designation": asyncResult.userResult[0].obj[i].designation,
                                        "userId": filterObj[d].userId,
                                        "login": filterObj[d].loginTime,//filterObj[0].data,
                                        "logout" : filterObj[d].logoutTime,
                                        "stacticIp": filterObj[d].ipAddress

                                    });
                                      
                                }
                            } else {
                                    
                                    
                                finalResult.push({
                                    "stateName": asyncResult.userResult[0].obj[i].stateName,
                                    "Headquarter": asyncResult.userResult[0].obj[i].districtName,
                                    "empName": asyncResult.userResult[0].obj[i].name,
                                    "designation": asyncResult.userResult[0].obj[i].designation,
                                    "userId": "---",
                                    "login": "---",
                                    "logout" : "---",
                                    "stacticIp": "---"
                                    
                                });
                                
                            }
                            //Sort the finalResult on the basis of stateName
                              finalResult.sort(function(a, b) {
                                return a["stateName"] - b["stateName"] || a["Headquarter"] - b["Headquarter"] || a["empName"] - b["empName"];
                              });
                        }
                      cb(false, finalResult)
                        
                    })
               
                
            } else {
                cb(false, []);
            }
        });
    }
    Logininfo.remoteMethod('onlineTrackerDetail', {
        description: "Online Tracker details",
        accepts: [{
            arg: 'params',
            type: 'object'
        }],
        returns: {
            root: true,
            type: 'array'
        },
        http: {
            verb: 'get'
        }

    })
};
