'use strict';
var async = require('async');
var ObjectId = require('mongodb').ObjectID;
module.exports = function (Meftracker) {
    Meftracker.createMef = function (param, cb) {
        var self = this;
        var MefCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        if (param.length == 0 || param == "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        let productDiscussedIds = [];
        let jointWorkIds = [];
        for (let i = 0; i < param.length; i++) {
            for (let j = 0; j < param[i].productDiscussed.length; j++) {
                productDiscussedIds.push(ObjectId(param[i].productDiscussed[j]));
            }
            param[i].productDiscussed = productDiscussedIds;
        }
        for (let ii = 0; ii < param.length; ii++) {
            if (param[ii].jointWorkWith.length > 0) {
                for (let jj = 0; jj < param[ii].jointWorkWith.length; jj++) {
                    jointWorkIds.push(ObjectId(param[ii].jointWorkWith[jj]));
                }
                param[ii].jointWorkWith = jointWorkIds;
            }
        }
        Meftracker.create(param, function (err, result) {
            return cb(false, result);
        });
    };

    Meftracker.remoteMethod(
        'createMef', {
            description: 'createMef',
            accepts: [{
                arg: 'param',
                type: 'array'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    Meftracker.updateAllRecord = function (records, cb) {


        var self = this;
        var response = [];
        var ids = [];
        var MeftrackerCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        let returnData = records;

        for (var w = 0; w < returnData.length; w++) {

            var set = {};
            set = returnData[w];
            self.update({
                id: returnData[w].id
            },//{$set: },
                set,
                function (err, result) {
                    console.log("RESULT TP");
                    console.log(result);
                    if (err) {
                        console.log(err);
                    }


                });


        }
        //console.log("after : ",returnData)

        return cb(false, records);

    };
    // End mef Record


    // Delete API for mef
    Meftracker.deleteAllRecord = function (records, cb) {


        var self = this;
        var response = [];
        var ids = [];
        var MeftrackerCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        if (records.length === 0 || records === "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }

        for (var w = 0; w < records.length; w++) {

            Meftracker.destroyById(
                records[w].id,
                function (err) {
                    if (err) {
                        console.log(err);
                    }


                }
            );


        }

        return cb(false, "Record has been Deleted Sucessfully");

    };
    // Update All Data for sync

    Meftracker.remoteMethod(
        'updateAllRecord', {
            description: 'update and send response all data',
            accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );

    // Delete All Data

    Meftracker.remoteMethod(
        'deleteAllRecord', {
            description: 'Delete and send response all data',
            accepts: [{ arg: 'records', type: 'array', http: { source: 'body' } }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'post'
            }
        }
    );

    Meftracker.getMefDetail = function (params, cb) {
        var self = this;
        var MEFTrackerCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        let dMatch = {};
        let userIds = [];

        if (params.formDetail.userInfo != null) {
            for (var iii = 0; iii < params.formDetail.userInfo.length; iii++) {
                userIds.push(ObjectId(params.formDetail.userInfo[iii]));
            }
        }
        dMatch = {
            "companyId": ObjectId(params.companyId),
            "submitBy": {
                $in: userIds
            },
            "date": {
                $gte: new Date(params.formDetail.fromDate),
                $lte: new Date(params.formDetail.toDate)
            }
        }

        async.parallel({
            mefDetailWithoutDiscussedProduct: function (cb) {
                MEFTrackerCollection.aggregate({
                    $match: dMatch
                },
                    {
                        $lookup: {
                            "from": "Area",
                            "localField": "areaId",
                            "foreignField": "_id",
                            "as": "area"
                        }
                    },
                    {
                        $lookup: {
                            "from": "Providers",
                            "localField": "providerId",
                            "foreignField": "_id",
                            "as": "provider"
                        }
                    },
                    {
                        $lookup: {
                            "from": "UserLogin",
                            "localField": "submitBy",
                            "foreignField": "_id",
                            "as": "userData"
                        }
                    }, {
                        $project: {
                            _id: 0,
                            areaName: { $arrayElemAt: ["$area.areaName", 0] },
                            providerId: { $arrayElemAt: ["$provider._id", 0] },
                            providerName: { $arrayElemAt: ["$provider.providerName", 0] },
                            userName: { $arrayElemAt: ["$userData.name", 0] },
                            mefDCRDate: "$date",
                            mefDate: { $dateToString: { format: "%d-%m-%Y", date: { $add: ["$date", 0] } } },
                            competitorProduct: 1,
                            productDiscussed: 1,
                            drProductProblem: 1,
                            problemSolnToDr: 1,
                            doctorFeedback: 1,
                            isQuerySolnGiven: 1,
                            querySoln: 1,//if yes specifi reason
                            seniorHelp: 1,//if no help need
                            isInformToSenior: 1,
                            isSolnFromSenior: 1,
                            solnGivenBySenior: 1,
                            quoteGiven: 1,
                            quoteDate: { $dateToString: { format: "%d-%m-%Y", date: { $add: ["$quoteDate", 0] } } },
                            quoteStatus: 1,
                            quoteOthers: 1,
                            conversionDone: 1,
                            conversionNoReason: 1
                        }
                    }, function (err, mefDetails) {
                        cb(false, mefDetails);
                    });
            },
            discussedProduct: function (cb) {
                // console.log(dMatch);
                MEFTrackerCollection.aggregate({
                    $match: dMatch
                }, {
                        $unwind: "$productDiscussed"
                    },

                    // Stage 2
                    {
                        $lookup: {
                            "from": "Products",
                            "localField": "productDiscussed",
                            "foreignField": "_id",
                            "as": "productDiscussed"
                        }
                    },

                    // Stage 3
                    {
                        $project: {
                            date: "$date",
                            providerId: "$providerId",
                            prodDisscused: { $arrayElemAt: ["$productDiscussed.productName", 0] }
                        }
                    },

                    // Stage 4
                    {
                        $group: {
                            _id: {
                                date: "$date",
                                providerId: "$providerId"
                            },
                            productDisscused: { $addToSet: "$prodDisscused" }
                        }
                    }, function (err, res) {
                        cb(false, res)
                    });
            },
            focusProductAndVisits: function (cb) {
                // console.log(dMatch);
                MEFTrackerCollection.aggregate({
                    $match: dMatch
                }, {
                        $group: {
                            _id: {},
                            dateAndProviderId: { $addToSet: { dateProvider: ["$providerId", "$date"] } },
                            providerId: { $addToSet: "$providerId" }
                        }
                    }, function (err, res) {
                        if (res.length > 0) {
                            let focusAndVisitArray = [];
                            dMatch.providerId = { $in: res[0].providerId }
                            Meftracker.app.models.Providers.getFocusProduct(res[0].providerId, function (err, focusProductName) {
                                Meftracker.app.models.DCRProviderVisitDetails.getProviderVisitDates(dMatch, function (err, visitDates) {
                                    for (let i = 0; i < res[0].providerId.length; i++) {
                                        let focusAndVisitObj = {};
                                        var filterObjForFocus = focusProductName.filter(function (obj) {
                                            return obj._id.providerId.toString() == res[0].providerId[i].toString();
                                        });
                                        if (filterObjForFocus.length > 0) {
                                            var focusName = '';
                                            for (let m = 0; m < filterObjForFocus[0].focusProduct.length; m++) {
                                                if (m == 0) {
                                                    focusName = filterObjForFocus[0].focusProduct[m];
                                                } else {
                                                    focusName = focusName + ' , ' + filterObjForFocus[0].focusProduct[m];
                                                }
                                            }
                                            focusAndVisitObj.focusProductName = focusName;
                                        }

                                        var filterObjForVisits = visitDates.filter(function (obj) {
                                            return obj._id.providerId.toString() == res[0].providerId[i].toString();
                                        });
                                        if (filterObjForVisits.length > 0) {
                                            var visitDate = '';
                                            for (let mm = 0; mm < filterObjForVisits[0].visitDates.length; mm++) {
                                                if (mm == 0) {
                                                    visitDate = filterObjForVisits[0].visitDates[mm];
                                                } else {
                                                    visitDate = visitDate + ' , ' + filterObjForVisits[0].visitDates[mm];
                                                }
                                            }
                                            focusAndVisitObj.visitDates = visitDate;
                                        }
                                        focusAndVisitObj.providerId = res[0].providerId[i];
                                        focusAndVisitArray.push(focusAndVisitObj);
                                    }
                                    cb(false, focusAndVisitArray);
                                });
                            });

                        }
                    });
            }
        },
            function (err, results) {
                var finalResult = [];
                var finalResult = results.mefDetailWithoutDiscussedProduct;
                for (let j = 0; j < results.mefDetailWithoutDiscussedProduct.length; j++) {
                    var filterObj = results.focusProductAndVisits.filter(function (obj) {
                        return obj.providerId.toString() == results.mefDetailWithoutDiscussedProduct[j].providerId.toString();
                    });
                    if (filterObj.length > 0) {
                        finalResult[j].focusProduct = filterObj[0].focusProductName;
                        finalResult[j].visitDates = filterObj[0].visitDates;
                    }

                    var filterObjProdDiscussed = results.discussedProduct.filter(function (obj) {
                        return obj._id.providerId.toString() == results.mefDetailWithoutDiscussedProduct[j].providerId.toString() && obj._id.date.toString() == results.mefDetailWithoutDiscussedProduct[j].mefDCRDate.toString();
                    });
                    if (filterObjProdDiscussed.length > 0) {
                        finalResult[j].productDiscussed = filterObjProdDiscussed[0].productDisscused;
                    }
                }
                return cb(false, finalResult);
            });
    }
    Meftracker.remoteMethod(
        'getMefDetail', {
            description: 'getMefDetail',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    Meftracker.getMefDetailForEditForm = function (params, cb) {
        var self = this;
        var MEFTrackerCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        MEFTrackerCollection.aggregate({
            $match: {
                companyId: ObjectId(params.companyId),
                date: new Date(params.date),
                submitBy: ObjectId(params.submitBy)
            }
        },
            {
                $lookup: {
                    "from": "Providers",
                    "localField": "providerId",
                    "foreignField": "_id",
                    "as": "provider"
                }
            },
            {
                $lookup: {
                    "from": "Area",
                    "localField": "areaId",
                    "foreignField": "_id",
                    "as": "area"
                }
            }, {
                $project: {
                    _id: 1,
                    areaName: { $arrayElemAt: ["$area.areaName", 0] },
                    providerId: { $arrayElemAt: ["$provider._id", 0] },
                    providerName: { $arrayElemAt: ["$provider.providerName", 0] },
                }
            }, function (err, mefDetailsForEdit) {
                cb(false, mefDetailsForEdit);
            });


    }
    Meftracker.remoteMethod(
        'getMefDetailForEditForm', {
            description: 'getMefDetailForEditForm',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );

    Meftracker.modifyMef = function (param, cb) {
        var self = this;
        var MefCollection = self.getDataSource().connector.collection(Meftracker.modelName);
        console.log(param);
        if (param.length == 0 || param == "") {
            var err = new Error('records is undefined or empty');
            err.statusCode = 404;
            err.code = 'RECORDS ARE NOT FOUND';
            return cb(err);
        }
        let productDiscussedIds = [];
        let jointWorkIds = [];
        for (let i = 0; i < param.length; i++) {
            for (let j = 0; j < param[i].productDiscussed.length; j++) {
                productDiscussedIds.push(ObjectId(param[i].productDiscussed[j]));
            }
            param[i].productDiscussed = productDiscussedIds;
        }
        for (let ii = 0; ii < param.length; ii++) {
            if (param[ii].jointWorkWith.length > 0) {
                for (let jj = 0; jj < param[ii].jointWorkWith.length; jj++) {
                    jointWorkIds.push(ObjectId(param[ii].jointWorkWith[jj]));
                }
                param[i].jointWorkWith = jointWorkIds;
            }
        }
        var whereId = ObjectId(param[0].id);
        delete param[0].id;
        param[0].updatedAt=new Date();
        Meftracker.update({
            "_id": whereId
        }, param[0], function (err, result) {
            return cb(false, result);
        });
    };

    Meftracker.remoteMethod(
        'modifyMef', {
            description: 'modifyMef',
            accepts: [{
                arg: 'param',
                type: 'array'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );


};
