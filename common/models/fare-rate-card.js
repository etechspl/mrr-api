'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
var async = require('async');
module.exports = function(Fareratecard) {

    //---------------edit FRC Codes by Preeti Arora 17-01-2020----------------
    
    Fareratecard.editFRC = function (params, cb) {
        var self = this;
        var FRCCollection = self.getDataSource().connector.collection(Fareratecard.modelName);
        let daBackup=[];
        Fareratecard.find({
            where:{
                _id:ObjectId(params.id),
                companyId:ObjectId(params.companyId),

            }
        },function(err,res){
            if(err){
                sendMail.sendMail({collectionName: "Fareratecard", errorObject: err, paramsObject: params, methodName: 'editFRC'});
            }
           if(res.length>0){
               if(res[0].hasOwnProperty('daBackup')){
                for(var i=0;i<res[0].daBackup.length;i++){
                    daBackup.push(res[0].daBackup[i])
                }
                daBackup.push({
                    fromDistance: res[0].fromDistance,
                    toDistance: res[0].toDistance,
                    allounceToBeGet: res[0].allounceToBeGet,
                    frcCode: res[0].frcCode,
                    applicableTo: res[0].applicableTo,
                    rate:res[0].rate,
                    description:res[0].description,
                    frcType: res[0].frcType,
                    updatedAt:new Date()
                })

               }
               else{
                   daBackup.push({
                    fromDistance: res[0].fromDistance,
                    toDistance: res[0].toDistance,
                    allounceToBeGet: res[0].allounceToBeGet,
                    frcCode: res[0].frcCode,
                    applicableTo: res[0].applicableTo,
                    rate:res[0].rate,
                    description:res[0].description,
                    frcType: res[0].frcType,
                    updatedAt:new Date()
                })
               }

               let where={
                   _id:ObjectId(params.id),
                   companyId:ObjectId(params.companyId),
                }
               let updateObj={
                fromDistance: params.fromDistance,
                toDistance: params.toDistance,
                allounceToBeGet: params.allounceToBeGet,
                frcCode: params.frcCode,
                applicableTo: params.applicableTo,
                rate:params.rate,
                description:params.description,
                frcType: params.frcType,
                updatedAt:new Date(),
                daBackup:daBackup
               }
               Fareratecard.update(where,updateObj,function(err,res){
                   if(err){
                    sendMail.sendMail({collectionName: "Fareratecard", errorObject: err, paramsObject: params, methodName: 'editFRC'});
                   }
                return cb(false,res)
                   
               })

           }else{
           return cb(false,[])
		   }
        })

    };

    Fareratecard.remoteMethod(
        'editFRC', {
            description: 'editFRC',
            accepts: [{
                arg: 'params',
                type: 'object'
            }],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    )

    //------------------------End--------------------------------
};
