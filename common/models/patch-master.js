var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var NodeGeocoder = require('node-geocoder');
'use strict';
module.exports = function(Patchmaster) {
    Patchmaster.createPatch = function (param, cb) { 
        var self = this;
        var PatchCollection = this.getDataSource().connector.collection(Patchmaster.modelName);
        let providerIds=[];
        let areaIds=[];
        param.providerId.forEach(element => {
            providerIds.push(ObjectId(element))
        });
        param.areaId.forEach(element => {
            areaIds.push(ObjectId(element))
        });
        let find={
            patchStatus:param.status,
            patchName:param.patchName,
            companyId:ObjectId(param.companyId),
            userId:ObjectId(param.userId)
        }
        Patchmaster.find({where:find},function(err,res) {
            if(err){
                return cb(err)
            }else{
                if(res.length>0){
                  return cb(false,["Group Already Created !!","error"])
                }else{
                    let obj={
                        companyId:ObjectId(param.companyId),
				        areaId:areaIds,
                        providerId:providerIds,
                        userId:ObjectId(param.userId),
                        stateId:ObjectId(param.stateId),
                        districtId:ObjectId(param.districtId),
                        patchName:param.patchName,
                        patchStatus:param.status,
                        createdAt:new Date(),
                        updatedAt:new Date()
                    }
                    PatchCollection.insert(obj,function(err,res){
                        if(err){
                           return cb(err)
                        }else{
                            return cb(false,["Patch Created Successfully !!","success"])
                        }

                    })
                }
            }
            
        })

    }
    Patchmaster.remoteMethod(
        'createPatch', {
        description: 'Create Patch',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: 'body' }
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post'
        }
    }
    )
    Patchmaster.getPatchDetail = function (param, cb) {
        var self = this;
        var PatchCollection = this.getDataSource().connector.collection(Patchmaster.modelName);
        var ProviderCollection = self.getDataSource().connector.collection(Patchmaster.app.models.Providers.modelName);
        let providerIds = []
        param.providerId.forEach(element => {
            providerIds.push(ObjectId(element))
        });
        var match = {
            _id: { $in: providerIds },
            companyId: ObjectId(param.companyId),
        }
        ProviderCollection.aggregate(
            [
                // Stage 1
                {
                    $match: match
                },

                // Stage 2
                {
                    $lookup: {
                        "from": "Area",
                        "localField": "blockId",
                        "foreignField": "_id",
                        "as": "area"
                    }
                },

                // Stage 3
                {
                    $project: {
                        _id: 1,
                        providerName: 1,
                        providerCode: 1,
                        providerType: 1,
                        category: 1,
                        specialization: 1,
                        areaName: { $arrayElemAt: ["$area.areaName", 0] }
                    }
                }
            ],

            // Options
            {
                cursor: {
                    batchSize: 50
                }
            }, function (err, res) {
                if (err) {
                    return cb(err)
                }
                if (res.length > 0) {
                    console.log(res);
                    return cb(false, res)
                }
                return cb(false, [])
            }

            // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef

        );

    }
    Patchmaster.remoteMethod(
        'getPatchDetail', {
        description: 'Get Patch Detail',
        accepts: [{
            arg: 'param',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'get'
        }
    }
    )

    Patchmaster.removePatches = function (param, cb) {
        var self = this;
        var PatchCollection = this.getDataSource().connector.collection(Patchmaster.modelName);
        let providerIds = [];

        Patchmaster.find({
            where: {
                userId: param.userId,
                companyId: param.companyId,
                patchName: param.patchName
            }
        }, function (err, res) {
            if (err) {
                return (err)
            }
            if (res.length > 0) {
                res.forEach((element, i,arr) => {
                    element.providerId.forEach((provider, index) => {
                        param.providerId.filter(obj => {
                            if (obj.toString() == provider.toString()) {
                                element.providerId.splice(index, 1)
                            }
                        })
                    });
                    let where={
                        userId:ObjectId(param.userId),
                        companyId:ObjectId(param.companyId),
                        patchName:param.patchName
                    }
                    let update={
                        updatedAt:new Date(),
                        providerId:element.providerId
                    }
                    PatchCollection.update(
                        where,
                        {
                          $set: update,
                        },
                        function (err, res) {
                          if (err) return cb(false);
                            if (Object.is(arr.length - 1, i)) {
                                return cb(false, "updated");
                              }
                         
                        }
                      );
                });

            } else {
                return cb(false, [])
            }
        })


    }
    Patchmaster.remoteMethod(
        'removePatches', {
        description: 'Remove Patches',
        accepts: [{
            arg: 'param',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'get'
        }
    }
    )


    Patchmaster.updatePatch = function (param, cb) {
        var self = this;
        var PatchCollection = this.getDataSource().connector.collection(Patchmaster.modelName);
        let providerIds = [];
        let areaIds = [];
        param.providerId.forEach(element => {
            providerIds.push(ObjectId(element))
        });
        param.areaId.forEach(element => {
            areaIds.push(ObjectId(element))
        });
        let find = {
            patchStatus: param.status,
            patchName: param.patchName,
            companyId: ObjectId(param.companyId),
            userId: ObjectId(param.userId)
        }
        Patchmaster.find({ where: find }, function (err, res) {
            if (err) {
                return cb(err)
            } else {
                if (res.length > 0) {
                    res.forEach((element,i,arr) => {
                       param.providerId.forEach((paramPro,i) => {
                        element.providerId.push(ObjectId(paramPro))
                    });
                    let where={
                        userId:ObjectId(param.userId),
                        companyId:ObjectId(param.companyId),
                        patchName:param.patchName
                    }
                    let update={
                        updatedAt:new Date(),
                        providerId:element.providerId
                    }
                    PatchCollection.update(
                        where,
                        {
                          $set: update,
                        },
                        function (err, res) {
                          if (err) return cb(false);
                            if (Object.is(arr.length - 1, i)) {
                                return cb(false, "updated");
                              }
                         
                        }
                      );

                    });
                
                   
                } else {
                    let obj = {
                        companyId: ObjectId(param.companyId),
                        areaId: areaIds,
                        providerId: providerIds,
                        userId: ObjectId(param.userId),
                        stateId: ObjectId(param.stateId),
                        districtId: ObjectId(param.districtId),
                        patchName: param.patchName,
                        patchStatus: param.status,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }
                    PatchCollection.insert(obj, function (err, res) {
                        if (err) {
                            return cb(err)
                        } else {
                            return cb(false, ["Patch Created Successfully !!", "success"])
                        }

                    })
                }
            }

        })

    }
    Patchmaster.remoteMethod(
        'updatePatch', {
        description: 'Update Patch',
        accepts: [{
            arg: 'param',
            type: 'object',
            http: { source: 'body' }
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post'
        }
    }
    )

    Patchmaster.getPatchDetailsUserWise = function (param, cb) {
        var self = this;
        var PatchCollection = this.getDataSource().connector.collection(Patchmaster.modelName);
        var obj = {
            companyId: ObjectId(param.companyId),
        }
        Patchmaster.app.models.Hierarchy.getManagerHierarchy(param, function (err, users) {
            if (err) {
                sendMail.sendMail({
                    collectionName: "PatchMaster",
                    errorObject: err,
                    paramsObject: params,
                    methodName: "PatchMaster.getManagerTeamPerformance"
                });
            }
            let userIds = [ObjectId(param.supervisorId)];
            if (users.length > 0) {
                for (let user of users) {
                    userIds.push(ObjectId(user.userId))
                }}
                obj["userId"]= { $in: userIds };
                PatchCollection.aggregate(
                    [
                        // Stage 1
                        {
                            $match: obj
                        },
                        // Stage 3
                        {
                            $project: {
                                _id:0,
                                id: "$_id",
                                patchName: 1,
                                patchStatus: 1,
                                userId: 1,
                                companyId: 1,
                                providerId:1
                            }
                        }
                    ],
        
                    // Options
                    {
                        cursor: {
                            batchSize: 50
                        }
                    }, function (err, res) {
                        if (err) {
                            return cb(err)
                        }
                        if (res.length > 0) {
                            return cb(false, res)
                        }
                        return cb(false, [])
                    }
                );

            })
      
       

    }
    Patchmaster.remoteMethod(
        'getPatchDetailsUserWise', {
        description: 'Get Patch Details UserWise',
        accepts: [{ 
            arg: 'param',
            type: 'object',
        }],
        returns: {
            root: true,
            type: 'object'
        },
        http: {
            verb: 'get'
        }
    }
    )
};
