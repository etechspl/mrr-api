'use strict';
var sendMail = require("../../utils/sendMail");
var ObjectId = require("mongodb").ObjectID;
var moment = require("moment");

module.exports = function(Mgrinvestmentvisitdetails) {

    // Delete API for TP
    Mgrinvestmentvisitdetails.deleteMatchedRecord = function(params, cb) {
    var self = this;
    var InvestmentproviderCollection = self
      .getDataSource()
      .connector.collection(Mgrinvestmentvisitdetails.modelName);
    if (params.length === 0 || params === "") {
      var err = new Error("records is undefined or empty");
      err.statusCode = 404;
      err.code = "RECORDS ARE NOT FOUND";
      return cb(err);
    }
    InvestmentproviderCollection.remove(
      {
        investmentId: ObjectId(params.investmentId),
        providerType: { $in: params.providerType }
      },
      function(err, res) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Mgrinvestmentvisitdetails",
            errorObject: err,
            paramsObject: params,
            methodName: "deleteMatchedRecord"
          });
          console.log(err);
        }
        //return cb(false,res)
      }
    );
    return cb(false, "Record has been Deleted Sucessfully");
  };
  Mgrinvestmentvisitdetails.remoteMethod("deleteMatchedRecord", {
    description: "delete data",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "get"
    }
  });



  Mgrinvestmentvisitdetails.getUserJointworkDetails = function(userId,fromDate,toDate,cb) {
    var self = this;
    var InvestmentDetailCollection = self.getDataSource().connector.collection(Mgrinvestmentvisitdetails.modelName);
    InvestmentDetailCollection.aggregate(
      // Stage 1
      {
        $match: {
          submitBy: ObjectId(userId),
          submissionDate: {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
          }
        }
      },
      // Stage 2
      {
        $unwind: {
          path: "$jointWorkWithId",
          preserveNullAndEmptyArrays: true
        }
      },
      // Stage 3
      {
        $group: {
          _id: {
            jointWorkWith: "$jointWorkWithId"
          },
          totalCalls: {
            $addToSet: "$providerId"
          },
          submissionDate: {
            $addToSet: "$submissionDate"
          }
        }
      },
      // Stage 4
      {
        $match: {
          "_id.jointWorkWith": { $ne: null }
        }
      }, function(err, result) {
        // console.log("result=",result)
        if (err) {
          sendMail.sendMail({
            collectionName: "Mgrinvestmentvisitdetails",
            errorObject: err,
            paramsObject: [userId, fromDate, toDate],
            methodName: "getUserJointworkDetails"
          });
          // console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
      );
  }

  Mgrinvestmentvisitdetails.remoteMethod("getUserJointworkDetails", {
    description: "user joint wise investment details",
    accepts: [
      {
        arg: "userId",
        type: "string"
      },
      {
        arg: "fromDate",
        type: "string"
      },
      {
        arg: "toDate",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  

};
