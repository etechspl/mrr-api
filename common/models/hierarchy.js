"use strict";
var async = require("async");
var ObjectId = require("mongodb").ObjectID;
module.exports = function (Hierarchy) {
  Hierarchy.gettingUserReportingManager = function (params, cb) {
    var self = this;
    var hierarchyCollection = this.getDataSource().connector.collection(
      Hierarchy.modelName
    );
    var match = {
      userId: ObjectId(params.userId),
      companyId: ObjectId(params.companyId)
    };
    hierarchyCollection.aggregate(
      {
        $match: match
      },
      function (err, allReportingManager) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        if (!allReportingManager) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, allReportingManager);
      }
    );
  };
  Hierarchy.remoteMethod("gettingUserReportingManager", {
    description:
      "This method is used to get the all reporting manager of the paticular users",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  Hierarchy.getManagerHierarchy = function (params, cb) {
    var self = this;
    var hierarchyCollection = this.getDataSource().connector.collection(
      Hierarchy.modelName
    );
    var matchForLowerHierarcy = {};
    let matchForUpperHierarcy = {};
    if (params.designation === undefined) {
      matchForLowerHierarcy = {
        supervisorId: ObjectId(params.supervisorId),
        companyId: ObjectId(params.companyId),
        status: true
      };
      matchForUpperHierarcy = {
        userId: ObjectId(params.supervisorId),
        companyId: ObjectId(params.companyId),
        status: true
      };
    } else {
      matchForLowerHierarcy = {
        supervisorId: ObjectId(params.supervisorId),
        companyId: ObjectId(params.companyId),
        status: true,
        userDesignationLevel: {
          $in: params.designation
        }
      };
      matchForUpperHierarcy = {
        userId: ObjectId(params.supervisorId),
        companyId: ObjectId(params.companyId),
        status: true,
        userDesignationLevel: {
          $in: params.designation
        }
      };
    }
    //-------------------------PK(2019-06-25)-------------------------
    let dynamicMatchForDivision = {};
    if (
      params.isDivisionExist == undefined ||
      params.isDivisionExist == false
    ) {
      //Skip...
    } else if (params.isDivisionExist == true) {
      let divisionIds = [];
      for (let i = 0; i < params.division.length; i++) {
        divisionIds.push(ObjectId(params.division[i]));
      }
      dynamicMatchForDivision = {
        "userInfo.divisionId": {
          $in: divisionIds
        }
      };
    }
    //---------------------------END----------------------------------
    async.parallel({
      LowerHierarcy: function (cb) {
        hierarchyCollection.aggregate(
          {
            $match: matchForLowerHierarcy
          },
          {
            $lookup: {
              from: "UserInfo",
              localField: "userId",
              foreignField: "userId",
              as: "userInfo"
            }
          },
          {
            $lookup: {
              from: "UserLogin",
              localField: "userId",
              foreignField: "_id",
              as: "userLogin"
            }
          },
          //--------------------PK(2019-06-25)---------------------------
          {
            $unwind: "$userInfo"
          },
          {
            $unwind: "$userLogin"
          },
          {
            $match: dynamicMatchForDivision
          },
          {
            $unwind: {
              path: "$userInfo.divisionId",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "DivisionMaster",
              localField: "userInfo.divisionId",
              foreignField: "_id",
              as: "division"
            }
          },
          //-------------------------END---------------------------------
          {
            $lookup: {
              from: "State",
              localField: "userInfo.stateId",
              foreignField: "_id",
              as: "states"
            }
          },
          {
            $lookup: {
              from: "District",
              localField: "userInfo.districtId",
              foreignField: "_id",
              as: "districts"
            }
          },
          {
            $project: {
              name: "$userInfo.name",
              userId: "$userInfo.userId",
              stateId: "$userInfo.stateId",
              employeeCode: "$userLogin.employeeCode",
              stateName: {
                $arrayElemAt: ["$states.stateName", 0]
              },
              districtId: "$userInfo.districtId",
              districtName: {
                $arrayElemAt: ["$districts.districtName", 0]
              },
              designation: "$userInfo.designation",
              designationLevel: "$userInfo.designationLevel",
              divisionId: {
                $arrayElemAt: ["$division._id", 0]
              },
              divisionName: {
                $arrayElemAt: ["$division.divisionName", 0]
              }
            }
          },
          {
            cursor: {
              batchSize: 50
            },
            allowDiskUse: true
          },
          function (err, LowerHierarcy) {
            return cb(false, LowerHierarcy);
          }
        );
      },
      UpperHierarcy: function (cb) {
        hierarchyCollection.aggregate(
          {
            $match: matchForUpperHierarcy
          },
          {
            $lookup: {
              from: "UserInfo",
              localField: "supervisorId",
              foreignField: "userId",
              as: "userInfo"
            }
          },
          {
            $lookup: {
              from: "UserLogin",
              localField: "userId",
              foreignField: "_id",
              as: "userLogin"
            }
          },
          {
            $unwind: "$userLogin"
          },
          //--------------------PK(2019-06-25)---------------------------
          {
            $unwind: "$userInfo"
          },
          {
            $match: dynamicMatchForDivision
          },
          {
            $unwind: {
              path: "$userInfo.divisionId",
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: "DivisionMaster",
              localField: "userInfo.divisionId",
              foreignField: "_id",
              as: "division"
            }
          },
          //-------------------------END---------------------------------
          {
            $lookup: {
              from: "State",
              localField: "userInfo.stateId",
              foreignField: "_id",
              as: "states"
            }
          },
          {
            $lookup: {
              from: "District",
              localField: "userInfo.districtId",
              foreignField: "_id",
              as: "districts"
            }
          },
          {
            $project: {
              name: "$userInfo.name",
              userId: "$userInfo.userId",//"$userInfo._id",
              stateId: "$userInfo.stateId",
              employeeCode: "$userLogin.employeeCode",

              stateName: {
                $arrayElemAt: ["$states.stateName", 0]
              },
              districtId: "$userInfo.districtId",
              districtName: {
                $arrayElemAt: ["$districts.districtName", 0]
              },
              designation: "$userInfo.designation",
              designationLevel: "$userInfo.designationLevel",
              divisionId: {
                $arrayElemAt: ["$division._id", 0]
              },
              divisionName: {
                $arrayElemAt: ["$division.divisionName", 0]
              }
            }
          },
          {
            cursor: {
              batchSize: 50
            },
            allowDiskUse: true
          },
          function (err, upperManager) {
            return cb(false, upperManager);
          }
        );
      }
    },
      function (err, results) {
        if (params.type === "lower") {
          return cb(false, results.LowerHierarcy);
        } else if (params.type === "upper") {
          return cb(false, results.UpperHierarcy);
        } else if (
          (params.type === "lowerWithMgr") |
          (params.type === "upperWithMgr") |
          (params.type === "lowerWithMgrWithUpper") |
          (params.type === "lowerWithUpper")
        ) {
          let userInfoCollection = self
            .getDataSource()
            .connector.collection(Hierarchy.app.models.UserInfo.modelName);
          userInfoCollection.aggregate(
            {
              $match: {
                userId: ObjectId(params.supervisorId)
              }
            },
            {
              $unwind: {
                path: "$divisionId",
                preserveNullAndEmptyArrays: true
              }
            },
            {
              $lookup: {
                from: "DivisionMaster",
                localField: "divisionId",
                foreignField: "_id",
                as: "divisionInfo"
              }
            }, {
            $lookup: {
              from: "UserLogin",
              localField: "userId",
              foreignField: "_id",
              as: "loginInfo"
            }
          },
            {
              $lookup: {
                from: "State",
                localField: "stateId",
                foreignField: "_id",
                as: "stateInfo"
              }
            },
            {
              $lookup: {
                from: "District",
                localField: "districtId",
                foreignField: "_id",
                as: "districtInfo"
              }
            },
            {
              $project: {
                userId: 1,
                name: 1,
                designation: 1,
                designationLevel: 1,
                state: { $arrayElemAt: ["$stateInfo", 0] },
                employeeCode: { $arrayElemAt: ["$loginInfo.employeeCode", 0] },
                district: { $arrayElemAt: ["$districtInfo", 0] },
                divisionId: { $arrayElemAt: ["$divisionInfo._id", 0] },
                divisionName: {
                  $arrayElemAt: ["$divisionInfo.divisionName", 0]
                }
              }
            },
            function (err, resp) {
              let res = JSON.parse(JSON.stringify(resp));
              if (params.type === "lowerWithMgr") {
                results.LowerHierarcy.push({
                  userId: res[0].userId,
                  name: res[0].name,
                  employeeCode: res[0].employeeCode,
                  designation: res[0].designation,
                  designationLevel: res[0].designationLevel,
                  stateId: res[0].state._id,
                  stateName: res[0].state.stateName,
                  districtName: res[0].district.districtName,
                  districtId: res[0].district._id,
                  designation: res[0].designation,
                  divisionId: res[0].divisionId,
                  divisionName: res[0].divisionName
                });
                return cb(false, results.LowerHierarcy);
              } else if (params.type === "upperWithMgr") {
                results.UpperHierarcy.push({
                  userId: res[0].userId,
                  name: res[0].name,
                  employeeCode: res[0].employeeCode,

                  designation: res[0].designation,
                  designationLevel: res[0].designationLevel,
                  stateId: res[0].state._id,
                  stateName: res[0].state.stateName,
                  districtName: res[0].district._id,
                  districtId: res[0].district.districtId,
                  divisionId: res[0].divisionId,
                  divisionName: res[0].divisionName
                });
                return cb(false, results.UpperHierarcy);
              } else if (params.type === "lowerWithUpper") {
                let finalHierarchyInfoArray = [];
                if (results.LowerHierarcy.length > 0) {
                  for (var i = 0; i < results.LowerHierarcy.length; i++) {
                    finalHierarchyInfoArray.push({
                      id: results.LowerHierarcy[i]._id,
                      userId: results.LowerHierarcy[i].userId,
                      stateName: results.LowerHierarcy[i].stateName,
                      districtName: results.LowerHierarcy[i].districtName,
                      name: results.LowerHierarcy[i].name,
                      employeeCode: results.LowerHierarcy[i].employeeCode,
                      designation: results.LowerHierarcy[i].designation,
                      designationLevel:
                        results.LowerHierarcy[i].designationLevel,
                      hierarchy: "lower"
                      //divisionId: results.LowerHierarcy[i].divisionId,
                      //divisionName: results.LowerHierarcy[i].divisionName
                    });
                    if (params.isDivisionExist == true) {
                      obj.divisionId = results.LowerHierarcy[i].divisionId;
                      obj.divisionName = results.LowerHierarcy[i].divisionName;
                    }
                  }
                }
                if (results.UpperHierarcy.length > 0) {
                  for (var j = 0; j < results.UpperHierarcy.length; j++) {
                    let obj = {
                      id: results.UpperHierarcy[j]._id,
                      userId: results.UpperHierarcy[j].userId,
                      stateName: results.UpperHierarcy[j].stateName,
                      // employeeCode:results.UpperHierarcy[i].employeeCode,

                      districtName: results.UpperHierarcy[j].districtName,
                      name: results.UpperHierarcy[j].name,
                      designation: results.UpperHierarcy[j].designation,
                      designationLevel:
                        results.UpperHierarcy[j].designationLevel,
                      hierarchy: "upper"
                      //divisionId: results.UpperHierarcy[i].divisionId,
                      //divisionName: results.UpperHierarcy[i].divisionName
                    };
                    if (params.isDivisionExist == true) {
                      obj.divisionId = results.UpperHierarcy[j].divisionId;
                      obj.divisionName = results.UpperHierarcy[j].divisionName;
                    }
                    finalHierarchyInfoArray.push(obj);
                  }
                }
                return cb(false, finalHierarchyInfoArray);
              } else {
                let finalHierarchyInfoArray = [];
                if (results.LowerHierarcy.length > 0) {
                  for (var i = 0; i < results.LowerHierarcy.length; i++) {
                    finalHierarchyInfoArray.push({
                      id: results.LowerHierarcy[i]._id,
                      userId: results.LowerHierarcy[i].userId,
                      stateName: results.LowerHierarcy[i].stateName,
                      employeeCode: results.LowerHierarcy[i].employeeCode,

                      districtName: results.LowerHierarcy[i].districtName,
                      name: results.LowerHierarcy[i].name,
                      designation: results.LowerHierarcy[i].designation,
                      designationLevel:
                        results.LowerHierarcy[i].designationLevel,
                      hierarchy: "lower"
                      //divisionId: results.LowerHierarcy[i].divisionId,
                      //divisionName: results.LowerHierarcy[i].divisionName
                    });
                    if (params.isDivisionExist == true) {
                      obj.divisionId = results.LowerHierarcy[i].divisionId;
                      obj.divisionName = results.LowerHierarcy[i].divisionName;
                    }
                  }
                }
                finalHierarchyInfoArray.push({
                  //  id: results.LowerHierarcy[i]._id,
                  userId: res[0].userId,
                  name: res[0].name,
                  employeeCode: res[0].employeeCode,

                  stateId: res[0].state._id,
                  stateName: res[0].state.stateName,
                  districtName: res[0].district.districtName,
                  districtId: res[0].district._id,
                  designation: res[0].designation,
                  divisionId: res[0].divisionId,
                  divisionName: res[0].divisionName
                });
                if (results.UpperHierarcy.length > 0) {
                  for (var j = 0; j < results.UpperHierarcy.length; j++) {
                    finalHierarchyInfoArray.push({
                      id: results.UpperHierarcy[j]._id,
                      userId: results.UpperHierarcy[j].userId,
                      stateName: results.UpperHierarcy[j].stateName,
                      districtName: results.UpperHierarcy[j].districtName,
                      name: results.UpperHierarcy[j].name,
                      // employeeCode:results.UpperHierarcy[j].employeeCode,
                      designation: results.UpperHierarcy[j].designation,
                      designationLevel:
                        results.UpperHierarcy[j].designationLevel,
                      hierarchy: "upper"
                      //divisionId: results.UpperHierarcy[i].divisionId,
                      //divisionName: results.UpperHierarcy[i].divisionName
                    });
                    if (params.isDivisionExist == true) {
                      obj.divisionId = results.UpperHierarcy[j].divisionId;
                      obj.divisionName = results.UpperHierarcy[j].divisionName;
                    }
                  }
                }
                return cb(false, finalHierarchyInfoArray);
              }
            });
        } else if (params.type === 'lowerWithUpperWithAdmin') {

          const userInfoCollection = self.getDataSource().connector.collection(Hierarchy.app.models.UserInfo.modelName);

          userInfoCollection.aggregate({

            $match: {

              companyId: ObjectId(params.companyId),

              designationLevel: 0,

              status: true

            }

          }, {
            $lookup: {
              from: "UserLogin",
              localField: "userId",
              foreignField: "_id",
              as: "loginInfo"
            }
          }, {

            $project: {

              userId: 1,

              name: 1,
              employeeCode: {
                $arrayElemAt: ["$loginInfo.employeeCode", 0]
              },

              designation: 1,

              designationLevel: 1

            }

          }, function (err, resp) {



            let res = JSON.parse(JSON.stringify(resp));

            let finalHierarchyInfoArray = [];

            if (res.length > 0) {

              res.forEach(elem => {

                finalHierarchyInfoArray.push({

                  userId: elem.userId,

                  name: elem.name,
                  employeeCode: elem.employeeCode,

                  designation: elem.designation,

                  designationLevel: elem.designationLevel,

                  hierarchy: 'admin'

                });

              });

            }

            if (results.LowerHierarcy.length > 0) {

              for (var i = 0; i < results.LowerHierarcy.length; i++) {

                finalHierarchyInfoArray.push({

                  id: results.LowerHierarcy[i]._id,

                  userId: results.LowerHierarcy[i].userId,
                  employeeCode: results.LowerHierarcy[i].employeeCode,


                  stateName: results.LowerHierarcy[i].stateName,

                  districtName: results.LowerHierarcy[i].districtName,

                  name: results.LowerHierarcy[i].name,

                  designation: results.LowerHierarcy[i].designation,

                  designationLevel: results.LowerHierarcy[i].designationLevel,

                  hierarchy: 'lower'

                });

                if (params.isDivisionExist == true) {

                  obj.divisionId = results.LowerHierarcy[i].divisionId;

                  obj.divisionName = results.LowerHierarcy[i].divisionName;

                }

              }

            }

            if (results.UpperHierarcy.length > 0) {

              for (var j = 0; j < results.UpperHierarcy.length; j++) {

                let obj = {

                  id: results.UpperHierarcy[j]._id,

                  userId: results.UpperHierarcy[j].userId,

                  stateName: results.UpperHierarcy[j].stateName,
                  employeeCode: results.UpperHierarcy[i].employeeCode,


                  districtName: results.UpperHierarcy[j].districtName,

                  name: results.UpperHierarcy[j].name,

                  designation: results.UpperHierarcy[j].designation,

                  designationLevel: results.UpperHierarcy[j].designationLevel,

                  hierarchy: 'upper'

                }

                if (params.isDivisionExist == true) {

                  obj.divisionId = results.UpperHierarcy[j].divisionId;

                  obj.divisionName = results.UpperHierarcy[j].divisionName;

                }

                finalHierarchyInfoArray.push(obj);

              }

            }

            return cb(false, finalHierarchyInfoArray);



          })

        }
      }
    );
  };
  Hierarchy.remoteMethod("getManagerHierarchy", {
    description:
      "This method is used to get the all users under manager Hierarchy",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------- by ravi 28-12-2018-------------------------------
  Hierarchy.getTCs = function (supervisorId, cb) {
    var self = this;
    var hierarchyCollection = self
      .getDataSource()
      .connector.collection(Hierarchy.modelName);
    hierarchyCollection.aggregate(
      {
        $match: {
          supervisorId: ObjectId(supervisorId),
          //userDesignation: {$in:['MR','SE']},
          status: true
        }
      },
      {
        $sort: {
          userName: 1
        }
      },
      function (err, result) {
        return cb(false, result);
      }
    );
  };
  Hierarchy.remoteMethod("getTCs", {
    description: "get TC data for selected details",
    accepts: [
      //  { arg: 'ques', type: 'array' },
      {
        arg: "supervisorId",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------------_____END------------------------
  Hierarchy.deleteHierarchys = function (param, cb) {
    var self = this;
    var hierarchyCollection = self
      .getDataSource()
      .connector.collection(Hierarchy.modelName);
    let _idsArr = [];
    if (param.length > 0) {
      for (let i = 0; i < param.length; i++) {
        _idsArr.push(ObjectId(param[i]._id));
      }
    }
    hierarchyCollection.update(
      {
        _id: { $in: _idsArr }
      },
      {
        $set: {
          status: false,
          deletedAt: new Date()
        }
      },
      {
        multi: true
      },
      function (err, res) {
        if (err) {
          return cb(err);
        } else {
          return cb(false, res);
        }
      }
    );
  };
  Hierarchy.remoteMethod("deleteHierarchys", {
    description: "deleteHierarchys",
    accepts: [
      {
        arg: "param",
        type: "array"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  // Manager Hiearachy i.e mapped userId in array
  Hierarchy.getManagerHierarchyInArray = function (params, cb) {
    var self = this;
    var hierarchyCollection = this.getDataSource().connector.collection(
      Hierarchy.modelName
    );
    var supervisorIds = [];
    for (var k = 0; k < params.supervisorId.length; k++) {
      supervisorIds.push(ObjectId(params.supervisorId[k]));
    }
    var match = {
      supervisorId: { $in: supervisorIds },
      companyId: ObjectId(params.companyId),
      status: true
    };
    hierarchyCollection.aggregate(
      // Stage 1
      {
        $match: match
      },
      // Stage 2
      {
        $group: {
          _id: {},
          userId: { $push: "$userId" }
        }
      },
      function (err, result) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Hierarchy.remoteMethod("getManagerHierarchyInArray", {
    description:
      "This method is used to get the all users under manager Hierarchy",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------by preeti arora--------------------------------------------------
  Hierarchy.createHierarchy = function (params, cb) {
    var createhierarchyCollection = this.getDataSource().connector.collection(
      Hierarchy.modelName
    );
    var mgrid = [];
    var inertArr = [];
    if (params[0].supervisorId != null) {
      for (var i = 0; i < params.length; i++) {
        mgrid.push(ObjectId(params[i].immediateSupervisorId));
        var createhir = {
          userId: ObjectId(params[i].userId),
          companyId: ObjectId(params[i].companyId),
          userName: params[i].userName,
          userDesignation: params[i].userDesignation,
          userDesignationLevel: params[i].userDesignationLevel,
          supervisorId: ObjectId(params[i].supervisorId),
          supervisorName: params[i].immediateSupervisorName,
          supervisorDesignation: params[i].supervisorDesignation,
          immediateSupervisorId: ObjectId(params[i].immediateSupervisorId),
          immediateSupervisorName: params[i].immediateSupervisorName,
          status: params[i].status,
          createdAt: new Date(),
          updatedAt: new Date()
        };
        inertArr.push(createhir);
      }
    }
    if (params[0].supervisorId == null) {
      Hierarchy.find(
        {
          where: {
            companyId: params[0].companyId,
            userId: params[0].userId
          }
        },
        function (err, response) {
          if (response.length > 0) {
            createhierarchyCollection.update(
              {
                companyId: ObjectId(params[0].companyId),
                userId: ObjectId(params[0].userId)
              },
              {
                $set: { userName: params[0].userName, status: params[0].status }
              },
              { multi: true },
              function (err, res) {
                if (err) {
                  console.log(err);
                  return cb(err);
                } else {
                  return cb(false, res);
                }
              }
            );
          } else {
            return cb(false, "");
          }
        }
      );
    } else {
      for (var i = 0; i < params.length; i++) {
        Hierarchy.find(
          {
            where: {
              companyId: params[i].companyId,
              userId: params[i].userId,
              supervisorId: { inq: mgrid }
            }
          },
          function (err, response) {
            if (response.length > 0) {
              createhierarchyCollection.update(
                {
                  companyId: ObjectId(params[0].companyId),
                  userId: ObjectId(params[0].userId)
                },
                {
                  $set: {
                    userName: params[0].userName,
                    status: params[0].status
                  }
                },
                { multi: true },
                function (err, res) {
                  if (err) {
                    console.log(err);
                    return cb(err);
                  } else {
                    return cb(false, res);
                  }
                }
              );
            } else {
              createhierarchyCollection.insertMany(inertArr, function (
                err,
                res
              ) {
                if (err) {
                  console.log(err);
                  return cb(err);
                } else {
                  return cb(false, res);
                }
              });
            } // end else..........
          }
        );
      }
    }
  };
  Hierarchy.remoteMethod("createHierarchy", {
    description: "Edit user",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "post"
    }
  });
  // Manager Hiearachy based on statewise or district wise or designation wise
  Hierarchy.getManagerHierarchyInArrayBasedOnStateOrDistrictOrDesignationwise = function (
    params,
    cb
  ) {
    var self = this;
    var hierarchyCollection = this.getDataSource().connector.collection(
      Hierarchy.modelName
    );
    var supervisorIds = [];
    for (var k = 0; k < params.supervisorId.length; k++) {
      supervisorIds.push(ObjectId(params.supervisorId[k]));
    }
    var match = {
      supervisorId: {
        $in: supervisorIds
      },
      companyId: ObjectId(params.companyId),
      status: true
    };
    let matchHierarchy = {};
    let stateIds = [];
    let districtIds = [];
    if (params.type.toLowerCase() === "state") {
      for (let stateId of params["stateId"]) {
        stateIds.push(ObjectId(stateId));
      }
      matchHierarchy.stateId = {
        $in: stateIds
      };
    } else if (params.type.toLowerCase() === "headquarter") {
      for (let stateId of params["stateId"]) {
        stateIds.push(ObjectId(stateId));
      }
      matchHierarchy.stateId = {
        $in: stateIds
      };
      for (let districtId of params["districtIds"]) {
        districtIds.push(ObjectId(districtId));
      }
      matchHierarchy.districtId = {
        $in: districtIds
      };
    } else if (params.type.toLowerCase() === "employee wise") {
      //match.userDesignation = params.designation;
      matchHierarchy.userDesignation = params.designation;
    }
    hierarchyCollection.aggregate(
      {
        $match: match
      },
      {
        $lookup: {
          from: "UserInfo",
          localField: "userId",
          foreignField: "userId",
          as: "userInfo"
        }
      },
      {
        $project: {
          userId: 1,
          stateId: {
            $arrayElemAt: ["$userInfo.stateId", 0]
          },
          districtId: {
            $arrayElemAt: ["$userInfo.districtId", 0]
          },
          userDesignation: 1
        }
      },
      {
        $match: matchHierarchy
      },
      // Stage 2
      {
        $group: {
          _id: {},
          userIds: {
            $addToSet: "$userId"
          }
        }
      },
      function (err, result) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Hierarchy.remoteMethod(
    "getManagerHierarchyInArrayBasedOnStateOrDistrictOrDesignationwise",
    {
      description:
        "This method is used to get the all users under manager Hierarchy based on stateId or districtId or designation",
      accepts: [
        {
          arg: "params",
          type: "object"
        }
      ],
      returns: {
        root: true,
        type: "array"
      },
      http: {
        verb: "get"
      }
    }
  );

  Hierarchy.updateHierarchyStatus = function (params, cb) {
    var self = this;
    var hierarchyCollection = self
      .getDataSource()
      .connector.collection(Hierarchy.modelName);
    hierarchyCollection.updateMany(
      {
        companyId: ObjectId(params.companyId),
        userId: ObjectId(params.modifyUserId)
      },
      {
        $set: {
          status: params.changeTo,
          deletedAt: new Date()
        }
      },
      function (err, result) {

        console.log("result", result);

        if (err) {
          sendMail.sendMail({
            collectionName: "Hierarchy",
            errorObject: err,
            paramsObject: params,
            methodName: "updateHierarchyStatus"
          });
          return cb(err);
        } else {

          return cb(false, result);

        }
      }
    );

    hierarchyCollection.updateMany(
      {
        companyId: ObjectId(params.companyId),
        supervisorId: ObjectId(params.modifyUserId)
      },
      {
        $set: {
          status: params.changeTo,
          deletedAt: new Date()
        }
      },
      function (err, result11) {

        console.log("result", result11);

        if (err) {
          sendMail.sendMail({
            collectionName: "Hierarchy",
            errorObject: err,
            paramsObject: params,
            methodName: "updateHierarchyStatus"
          });
          return cb(err);
        } else {

          return cb(false, result11);

        }
      }
    );

  }

  Hierarchy.remoteMethod("updateHierarchyStatus", {
    description: "Delete user",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "get"
    }
  });

  /*Done by ravindra for target vs acheivement report 
  getting unique districts with pool HQs key
*/

  Hierarchy.getManagerHierarchyWithUniqueHQs = function (params, cb) {
    var self = this;
    var hierarchyCollection = this.getDataSource().connector.collection(Hierarchy.modelName);
    //console.log('Params In Heirarchy', params);
    hierarchyCollection.aggregate(
      {
        $match: params
      },
      // Stage 2
      {
        $lookup: {
          from: "UserInfo",
          localField: "userId",
          foreignField: "userId",
          as: "userInfo"
        }
      },

      // Stage 3
      {
        $unwind: { path: "$userInfo", preserveNullAndEmptyArrays: true }
      },

      // Stage 4
      {
        $lookup: {
          from: "District",
          localField: "userInfo.districtId",
          foreignField: "_id",
          as: "districts"
        }
      },

      // Stage 5
      {
        $unwind: { path: "$districts", preserveNullAndEmptyArrays: true }
      },

      // Stage 6
      {
        $group: {
          _id: {},
          uniqueDistricts: { $addToSet: { districtId: "$districts._id", districtName: "$districts.districtName", erpCodeForMainHQ: "$districts.erpCodeForMainHQ", mappedWithPoolDistrictId: "$districts.mappedWithPoolDistrictId", erpHqCode: "$districts.erpHqCode",isPoolDistrict:"$districts.isPoolDistrict"  } },
          districtId: { $addToSet: "$districts._id" }
        }
      }, function (err, LowerHierarcy) {
        // console.log('LowerHierarcy', LowerHierarcy);
        if (err) { return cb(err) }
        else { return cb(null, LowerHierarcy); }
      }
    );

    //---------------------------END----------------------------------

  };
  Hierarchy.remoteMethod("getManagerHierarchyWithUniqueHQs", {
    description:
      "This method is used to get the all users under manager Hierarchy",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });

  //==========END==========================

};
