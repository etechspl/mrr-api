'use strict';
var sendMail = require('../../utils/sendMail');
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');
var asyncLoop = require('node-async-loop');

module.exports = function(Companymaster) {
	
//--------------------API for scheduling sunday submission-----------------//
Companymaster.SubmitSundays = function(params, cb) {
    let companyIds=[];
    var date=moment(new Date()).format("YYYY-MM-DD");
    Companymaster.find(
        {
         where:{
            "validation.IsSubmitAutoSunday":true
         },include:[
             {
                 "relation":"Holiday",
                 "scope":{
                     where:{holidayDate:date}
                 }
             }
         ]
    },function(err,result){
        if(err){
            sendMail.sendMail({collectionName: "Companymaster", errorObject: err, paramsObject: params, methodName: 'SubmitSundays'});
        }
        var holidayObj=[];
        var companyRes=JSON.parse(JSON.stringify(result));
         if(result.length>0){
          companyRes.forEach(companyRes => {
            if(companyRes.Holiday.length>0){
                holidayObj.push({
                    holidayName: companyRes[i].Holiday[0].holidayName,
                    holidayDate:companyRes[i].Holiday[0].holidayDate,
                })
            }
            companyIds.push(ObjectId(companyRes.id))
             
         });
         let object={
             companyId:companyIds,
             holidayId:holidayObj
         }

        Companymaster.app.models.UserLogin.SubmitSundaysOnUserInfo(object,function(err, finalResult){
           //return cb(false,finalResult)
        })
    }else{
        return cb(false,[])
    }
    })

};

    Companymaster.remoteMethod(
        'SubmitSundays', {
            description: 'Insert Sundays',
            accepts: [
                {
                    arg: 'companyId',
                    type: 'string'
                }
            ]
        }
    )

    //--------------------------------------------------

};
