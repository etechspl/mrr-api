﻿var ObjectId = require("mongodb").ObjectID;
var moment = require("moment");
var sendMail = require("../../utils/sendMail");
("use strict");
module.exports = function (Userinfo) {
  //-------------------by Preeti Arora upadte division id in object----------------------
  Userinfo.observe("after save", function (ctx, next) {
    if (ctx.instance === undefined) {
      next();
    } else {
      let userInfoObj = JSON.parse(JSON.stringify(ctx.instance));
      let DivisioIds = [];
      if (userInfoObj.hasOwnProperty("divisionId")) {
        for (var i = 0; i < userInfoObj.divisionId.length; i++) {
          DivisioIds.push(ObjectId(userInfoObj.divisionId[i]));
        }
        let UpdatedId = ObjectId(userInfoObj.id);
        Userinfo.update({ id: UpdatedId }, { divisionId: DivisioIds }, function (
          err,
          divisionRes
        ) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: ctx,
              methodName: "observe"
            });
          }
          next();
        });
      } else {
        next();
      }
    }
  });
  //-------------------------end-----------------
  Userinfo.getUniqueDesignation = function (companyId, cb) {
    var self = this;
    var UserLoginCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    UserLoginCollection.distinct("designation", "designationLevel", function (
      err,
      records
    ) {
      if (err) {
        sendMail.sendMail({
          collectionName: "Userinfo",
          errorObject: err,
          paramsObject: companyId,
          methodName: "getUniqueDesignation"
        });
        return cb(err);
      } else {
        return cb(null, records);
      }
    });
  };
  Userinfo.remoteMethod("getUniqueDesignation", {
    description: "Get Unique Designation Company Wise",
    accepts: [
      //  { arg: 'ques', type: 'array' },
      {
        arg: "companyId",
        type: "string"
      }
    ]
  });
  //Praveen Singh Getting Users list based on State Ids
  // Last updated 10-05-2019, change method from single state to multiple state.
  //Last updated 19-08-2019, implementation of division concept : Praveen Kumar
  Userinfo.getUsers = function (param, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    //-------add this code to filter the the details---by preeti arora 27-12-2019--
    let passingStateIds = [];
    let passingHqIds = [];
    let passingUserIds = [];
    let status = "";

    status = typeof (param.status) == "boolean" ? [param.status] : param.status
    let dynamicMatch = {
      companyId: ObjectId(param.companyId),
      status: { $in: status }
    };
    if (param.type == "State") {
      for (let i = 0; i < param.stateIds.length; i++) {
        passingStateIds[i] = ObjectId(param.stateIds[i]);
      }
      dynamicMatch.stateId = { $in: passingStateIds };
    }
    if (param.type == "Headquarter") {
      for (let i = 0; i < param.district.length; i++) {
        passingHqIds[i] = ObjectId(param.district[i]);
      }
      dynamicMatch.districtId = { $in: passingHqIds };
    }
    if (param.type == "Employee Wise") {
      for (let i = 0; i < param.userId.length; i++) {
        passingUserIds[i] = ObjectId(param.userId[i]);
      }
      dynamicMatch.userId = { $in: passingUserIds };
    }
    let optionalStage = {
      $match: {}
    };
    if (param.isDivisionExist == true) {
      let divisionIds = [];
      for (let i = 0; i < param.division.length; i++) {
        divisionIds[i] = ObjectId(param.division[i]);
      }
      dynamicMatch.divisionId = { $in: divisionIds };
      optionalStage = {
        $unwind: {
          path: "$divisionId",
          preserveNullAndEmptyArrays: true
        }
      };
    }
    //-------------end -----
    //------this code will be commented--------
    /*let passingStateIds = [];
        for (let i = 0; i < param.stateIds.length; i++) {
            passingStateIds[i] = ObjectId(param.stateIds[i])
        }
        let dynamicMatch = {
            companyId: ObjectId(param.companyId),
            stateId: {
                $in: passingStateIds
            }
        }
        let optionalStage = {
            $match: {}
        }
        if (param.isDivisionExist == true) {
            let divisionIds = [];
            for (let i = 0; i < param.division.length; i++) {
                divisionIds[i] = ObjectId(param.division[i])
            }
            dynamicMatch.divisionId = { $in: divisionIds }
            optionalStage = {
                $unwind: {
                    path: "$divisionId",
                    preserveNullAndEmptyArrays: true
                }
            }
        }*/
    //----------------end--
    UserInfoCollection.aggregate(
      {
        $match: dynamicMatch
      },
      optionalStage,
      {
        $lookup: {
          from: "DivisionMaster",
          localField: "divisionId",
          foreignField: "_id",
          as: "division"
        }
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "users"
        }
      },
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "districts"
        }
      },
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "states"
        }
      },
      {
        $lookup: {
          from: "BlockUser",
          localField: "userBlockId",
          foreignField: "_id",
          as: "blockDetails"
        }
      },
      {
        $lookup: {
          from: "Hierarchy",
          localField: "userId",
          foreignField: "userId",
          as: "Manager"
        }
      },
      {
        $project: {
          id: "$userId",
          companyId: "$companyId",
          stateId: {
            $arrayElemAt: ["$states._id", 0]
          },
          stateName: {
            $arrayElemAt: ["$states.stateName", 0]
          },
          districtId: {
            $arrayElemAt: ["$districts._id", 0]
          },
          districtName: {
            $arrayElemAt: ["$districts.districtName", 0]
          },
          name: {
            $arrayElemAt: ["$users.name", 0]
          },
          username: {
            $arrayElemAt: ["$users.username", 0]
          },
          password: {
            $arrayElemAt: ["$users.viewPassword", 0]
          },
          lockingPeriod: "$lockingPeriod",
          mpin: {
            $arrayElemAt: ["$users.mPIN", 0]
          },
          status: "$status",
          email: {
            $arrayElemAt: ["$users.email", 0]
          },
          mobile: {
            $arrayElemAt: ["$users.mobile", 0]
          },
          dateOfBirth: { $arrayElemAt: ["$users.dateOfBirth", 0] },
          dateOfAnniversary: { $arrayElemAt: ["$users.dateOfAnniversary", 0] },
          dateOfAnniversary: { $arrayElemAt: ["$users.dateOfAnniversary", 0] },
          employeeCode: { $arrayElemAt: ["$users.employeeCode", 0] },
          dateOfJoining: { $arrayElemAt: ["$users.dateOfJoining", 0] },
          address: { $arrayElemAt: ["$users.address", 0] },
          aadhaar: { $arrayElemAt: ["$users.aadhaar", 0] },
          PAN: { $arrayElemAt: ["$users.PAN", 0] },
          designation: "$designation",
          designationLevel: "$designationLevel",
          deactivationDate: "$deactivationDate",
          isUpdateGeoLoc: "$isUpdateGeoLoc",
          statusReason:"$statusReason",
          divisionName: {
            $arrayElemAt: ["$division.divisionName", 0]
          },
		  //--------bank details---------
		  bankName: { $arrayElemAt: ["$users.bankName", 0] },
          bankAccountNumber: { $arrayElemAt: ["$users.bankAccountNumber", 0] },
          ifsc: { $arrayElemAt: ["$users.ifsc", 0] },
          manager: "$Manager",
          //---------User Blocked Details
          userBlockRowId: { $arrayElemAt: ["$blockDetails._id", 0] },
          blockReason: { $arrayElemAt: ["$blockDetails.blockReason", 0] },
          blockDate: { $arrayElemAt: ["$blockDetails.blockDate", 0] },
          releaseDate: { $arrayElemAt: ["$blockDetails.releaseDate", 0] },
          blockedStatus: {
            $cond: {
              if: { $gt: [{ $size: "$blockDetails" }, 0] },
              then: {
                $cond: {
                  if: {
                    $or: [
                      {
                        $cond: [
                          {
                            $gte: [
                              { $arrayElemAt: ["$blockDetails.blockDate", 0] },
                              new Date()
                            ]
                          },
                          true,
                          false
                        ]
                      }
                    ]
                  },
                  then: false,
                  else: true
                }
              },
              else: false
            }
          }
        }
      },
      {
        $sort: {
          divisionName: 1,
          stateName: 1,
          districtName: 1,
          designationLevel: -1,
          name: 1
        }
      },
      {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: param,
            methodName: "getUsers"
          });
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        if (result.length > 0) {
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getUsers", {
    description: "get Users based on stateId",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------getting all  manager all states------------//
  Userinfo.getAllManager = function (companyId, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    UserInfoCollection.aggregate(
      {
        $match: {
          companyId: ObjectId(companyId),
          designationLevel: {
            $gt: 1
          },
          status: true
        }
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "users"
        }
      },
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "districts"
        }
      },
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "states"
        }
      },
      {
        $project: {
          id: "$userId",
          stateId: {
            $arrayElemAt: ["$states._id", 0]
          },
          stateName: {
            $arrayElemAt: ["$states.stateName", 0]
          },
          districtId: {
            $arrayElemAt: ["$districts._id", 0]
          },
          districtName: {
            $arrayElemAt: ["$districts.districtName", 0]
          },
          name: {
            $arrayElemAt: ["$users.name", 0]
          },
          username: {
            $arrayElemAt: ["$users.username", 0]
          },
          password: {
            $arrayElemAt: ["$users.viewPassword", 0]
          },
          lockingPeriod: "$lockingPeriod",
          mpin: {
            $arrayElemAt: ["$users.mPIN", 0]
          },
          status: "$status",
          email: {
            $arrayElemAt: ["$users.email", 0]
          },
          mobile: {
            $arrayElemAt: ["$users.mobile", 0]
          },
          designation: "$designation",
          designationLevel: "$designationLevel"
        }
      },
      {
        $sort: {
          designationLevel: -1,
          name: 1
        }
      },
      {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: companyId,
            methodName: "getAllManager"
          });
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getAllManager", {
    description: "get Users based on stateId",
    accepts: [
      {
        arg: "companyId",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------------------------
  //getting managers list state wise
  Userinfo.getManagerStatewise = function (companyId, stateId, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    UserInfoCollection.aggregate(
      {
        $match: {
          stateId: ObjectId(stateId),
          companyId: ObjectId(companyId),
          designationLevel: {
            $gt: 1
          },
          status: true
        }
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "users"
        }
      },
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "districts"
        }
      },
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "states"
        }
      },
      {
        $project: {
          id: "$userId",
          stateId: {
            $arrayElemAt: ["$states._id", 0]
          },
          stateName: {
            $arrayElemAt: ["$states.stateName", 0]
          },
          districtId: {
            $arrayElemAt: ["$districts._id", 0]
          },
          districtName: {
            $arrayElemAt: ["$districts.districtName", 0]
          },
          name: {
            $arrayElemAt: ["$users.name", 0]
          },
          username: {
            $arrayElemAt: ["$users.username", 0]
          },
          password: {
            $arrayElemAt: ["$users.viewPassword", 0]
          },
          lockingPeriod: "$lockingPeriod",
          mpin: {
            $arrayElemAt: ["$users.mPIN", 0]
          },
          status: "$status",
          email: {
            $arrayElemAt: ["$users.email", 0]
          },
          mobile: {
            $arrayElemAt: ["$users.mobile", 0]
          },
          designation: "$designation",
          designationLevel: "$designationLevel"
        }
      },
      {
        $sort: {
          designationLevel: -1,
          name: 1
        }
      },
      {
        cursor: {
          batchSize: 50
        },
        designation: "$designation",
        designationLevel: "$designationLevel",
        userId: "$userId"
      },
      {
        $sort: {
          designationLevel: -1,
          name: 1
        }
      },
      {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: [companyId, stateId],
            methodName: "getManagerStatewise"
          });
          // console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getManagerStatewise", {
    description: "get Users based on stateId",
    accepts: [
      {
        arg: "companyId",
        type: "string"
      },
      {
        arg: "stateId",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------------------Praveen Kuumar(03-10-2018)-------------------------------------------
  Userinfo.getAllUserIdsOnDistrictBasis = function (
    companyId,
    stateId,
    districtId,
    cb
  ) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    UserInfoCollection.aggregate(
      // Stage 1
      {
        $match: {
          //   "companyId" : ObjectId("5b2e22083225df01ba7d6059"),
          // "stateId" : ObjectId("5b0293c8e615a52fb9bd2c64"),
          //"districtId" : ObjectId("5b3385ae510ac0524307b362"),
          companyId: ObjectId(companyId),
          stateId: ObjectId(stateId),
          districtId: ObjectId(districtId),
          status: true,
          designationLevel: {
            $ne: 0
          }
        }
      },
      // Stage 2
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "userPersonalDetail"
        }
      },
      // Stage 3
      {
        $unwind: "$userPersonalDetail"
      },
      // Stage 4
      {
        $project: {
          _id: 0,
          userId: 1,
          name: "$userPersonalDetail.name",
          designation: 1
        }
      },
      // Stage 5
      {
        $group: {
          _id: {},
          userIds: {
            $addToSet: "$userId"
          },
          obj: {
            $addToSet: {
              userId: "$userId",
              name: "$name",
              designation: "$designation"
            }
          }
        }
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: [companyId, stateId],
            methodName: "getAllUserIdsOnDistrictBasis"
          });
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getAllUserIdsOnDistrictBasis", {
    description: "get User Id On District Basis",
    accepts: [
      {
        arg: "companyId",
        type: "string"
      },
      {
        arg: "statemoduleId",
        type: "string"
      },
      {
        arg: "districtId",
        type: "string"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //-------------------------------------------END---------------------------------------------------------
  Userinfo.getReportingManager = function (params, cb) {
    var self = this;
    var userInfoCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    var match = {
      designationLevel: {},
      companyId: ObjectId(params.companyId)
    };
    if (params.designationLevel === 1) {
      match.designationLevel["$gt"] = params.designationLevel;
    } else {
      match.designationLevel["$gte"] = params.designationLevel;
    }
    // if designation is less than equal to 3 then only state manager
    //will come else all state manager will come of the company
    if (params.designationLevel <= 3) {
      match.stateId = ObjectId(params.stateId);
    }
    userInfoCollection.aggregate(
      {
        $match: match
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "users"
        }
      },
      {
        $project: {
          _id: 1,
          name: {
            $arrayElemAt: ["$users.name", 0]
          },
          userId: {
            $arrayElemAt: ["$users._id", 0]
          },
          designationLevel: "$designationLevel",
          designation: "$designation"
        }
      },
      {
        $match: {
          designationLevel: {
            $ne: 0
          }
        }
      },
      {
        $sort: {
          designationLevel: -1
        }
      },
      function (err, reportingManager) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getReportingManager"
          });
          // console.log(err);
          return cb(err);
        }
        if (!reportingManager) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, reportingManager);
      }
    );
  };
  Userinfo.remoteMethod("getReportingManager", {
    description: "Reporting Mangaers while creating the users",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  /*Userinfo.getAllUserIdsBasedOnType = function(params, cb) {
     //// console.log("params--",params)
        var self = this;
        var UserInfoCollection = self.getDataSource().connector.collection(Userinfo.modelName);
        var filters = {};
        let divisionArr = [];
        filters["companyId"] = ObjectId(params.companyId);
        filters["status"] = true;
    if(params.hasOwnProperty("isDivisionExist")){
        if (params.isDivisionExist === true) {
            for (division of params.division) {
                divisionArr.push(ObjectId(division));
            }
            filters["divisionId"] = {
                $in: divisionArr
            }
        }
    }
        if (params.type === "State") {
            var stateIds = [];
            for (var i = 0; i < params["stateIds"].length; i++) {
                stateIds.push(ObjectId(params["stateIds"][i]));
            }
            filters["stateId"] = {
                $in: stateIds
            };
        } else if (params.type === "Headquarter") {
            var hqIds = [];
            for (var i = 0; i < params.districtIds.length; i++) {
                hqIds.push(ObjectId(params.districtIds[i]));
            }
            filters["districtId"] = {
                $in: hqIds
            }
        }
        let unwindDivision = {
            path: "$divisionId",
            preserveNullAndEmptyArrays: true
        };
        let lookupDivision = {
            "from": "DivisionMaster",
            "localField": "divisionId",
            "foreignField": "_id",
            "as": "divisionInfo"
        };
        let lookupState = {
            "from": "State",
            "localField": "stateId",
            "foreignField": "_id",
            "as": "stateInfo"
        };
        let lookupDistrict = {
            "from": "District",
            "localField": "districtId",
            "foreignField": "_id",
            "as": "districtInfo"
        }
        let project = {
            _id: 0,
            userId: 1,
            divisionName: {
                $arrayElemAt: ["$divisionInfo.divisionName", 0]
            },
            divisionId: {
                $arrayElemAt: ["$divisionInfo._id", 0]
            },
            name: 1,
            designation: 1,
            joiningDate: "$promotedOn",
            stateId: 1,
            stateName: "$stateInfo.stateName",
            districtId: 1,
            districtName: "$districtInfo.districtName"
        }
        let groupObj = {
            _id: {},
            userIds: {
                $addToSet: "$userId"
            },
            obj: {
                $addToSet: {
                    userId: "$userId",
                    name: "$name",
                    divisionId: "$divisionId",
                    divisionName: "$divisionName",
                    designation: "$designation",
                    joiningDate: "$joiningDate",
                    stateId: "$stateId",
                    stateName: "$stateName",
                    districtId: "$districtId",
                    districtName: "$districtName"
                }
            }
        };
    //// console.log("filters--",filters)
        UserInfoCollection.aggregate({
                $match: filters
            }, {
                $unwind: unwindDivision
            }, {
                $lookup: lookupDivision
            }, {
                $lookup: lookupState
            }, {
                $lookup: lookupDistrict
            }, {
                $unwind: "$stateInfo"
            }, {
                $unwind: "$districtInfo"
            }, {
                $project: project
            }, {
                $sort: {
                    "divisionName": 1,
                    "stateName": 1,
                    "districtName": 1,
                    "name": 1
                }
            }, {
                $group: groupObj
            },
            function(err, result) {
                return cb(false, result)
            });
    };
    Userinfo.remoteMethod(
        'getAllUserIdsBasedOnType', {
            description: 'get User Id On District Basis',
            accepts: [{
                    arg: 'params',
                    type: 'object'
                }
                // {
                //       arg: 'companyId',
                //       type: 'string'
                //   }, {
                //       arg: 'type',
                //       type: 'string'
                //   }, {
                //       arg: 'stateIds',
                //       type: 'array'
                //   },
                //   {
                //       arg: 'districtIds',
                //       type: 'array'
                //   }
            ],
            returns: {
                root: true,
                type: 'array'
            },
            http: {
                verb: 'get'
            }
        }
    );*/
  Userinfo.getAllUserIdsBasedOnType = function (params, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    var filters = {};
    let divisionArr = [];
    let status = "";

    filters["companyId"] = ObjectId(params.companyId);
    if (params.hasOwnProperty("status")) {
      status = typeof (params.status) == "boolean" ? [params.status] : params.status
    } else {
      status = [true]
    }
    filters["status"] = { $in: status }

    if (params.hasOwnProperty("isDivisionExist")) {
      if (params.isDivisionExist === true) {
        for (division of params.division) {
          divisionArr.push(ObjectId(division));
        }
        filters["divisionId"] = {
          $in: divisionArr
        };
      }
    }
    if (params.type === "State") {
      var stateIds = [];
      for (var i = 0; i < params["stateIds"].length; i++) {
        stateIds.push(ObjectId(params["stateIds"][i]));
      }
      filters["stateId"] = {
        $in: stateIds
      };
    } else if (params.type === "Headquarter") {
      var hqIds = [];
      for (var i = 0; i < params.districtIds.length; i++) {
        hqIds.push(ObjectId(params.districtIds[i]));
      }
      filters["districtId"] = {
        $in: hqIds
      };
    } else if (params.type === "Employee Wise") {
      var userIds = [];
      for (var i = 0; i < params.userIds.length; i++) {
        userIds.push(ObjectId(params.userIds[i]));
      }
      filters["userId"] = {
        $in: userIds
      };
    }

    filters["designationLevel"] = { $ne: 0 }  //maine yeh line dali h bass sir .. dekhna ak bar koi braces??

    let unwindDivision = {
      path: "$divisionId",
      preserveNullAndEmptyArrays: true
    };
    let lookupDivision = {
      from: "DivisionMaster",
      localField: "divisionId",
      foreignField: "_id",
      as: "divisionInfo"
    };
    let lookupState = {
      from: "State",
      localField: "stateId",
      foreignField: "_id",
      as: "stateInfo"
    };
    let lookupDistrict = {
      from: "District",
      localField: "districtId",
      foreignField: "_id",
      as: "districtInfo"
    };
    let project = {
      _id: 0,
      userId: 1,
      divisionName: {
        $arrayElemAt: ["$divisionInfo.divisionName", 0]
      },
      divisionId: {
        $arrayElemAt: ["$divisionInfo._id", 0]
      },
      name: 1,
      designation: 1,
      joiningDate: "$promotedOn",
      stateId: 1,
      stateName: "$stateInfo.stateName",
      districtId: 1,
      districtName: "$districtInfo.districtName",
      Mobile: { $arrayElemAt: ["$userLogin.mobile", 0] },
      dateOfJoining: { $arrayElemAt: ["$userLogin.dateOfJoining", 0] },
      employeeCode: { $arrayElemAt: ["$userLogin.employeeCode", 0] }
    };
    let groupObj = {
      _id: {},
      userIds: {
        $addToSet: "$userId"
      },
      obj: {
        $addToSet: {
          userId: "$userId",
          name: "$name",
          divisionId: "$divisionId",
          divisionName: "$divisionName",
          designation: "$designation",
          joiningDate: "$joiningDate",
          stateId: "$stateId",
          stateName: "$stateName",
          districtId: "$districtId",
          districtName: "$districtName",
          mobile: "$Mobile",
          dateOfJoining: "$dateOfJoining",
          employeeCode: "$employeeCode"
        }
      }
    };
    console.log("filters--",filters)
    UserInfoCollection.aggregate(
      {
        $match: filters
      },
      {
        $unwind: unwindDivision
      },
      {
        $lookup: lookupDivision
      },
      {
        $lookup: lookupState
      },
      {
        $lookup: lookupDistrict
      },
      {
        $unwind: "$stateInfo"
      },
      {
        $unwind: "$districtInfo"
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "userLogin"
        }
      },
      {
        $project: project
      },
      {
        $sort: {
          divisionName: 1,
          stateName: 1,
          districtName: 1,
          name: 1
        }
      },
      {
        $group: groupObj
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getAllUserIdsBasedOnType"
          });
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getAllUserIdsBasedOnType", {
    description: "get User Id On District Basis",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
      // {
      //       arg: 'companyId',
      //       type: 'string'
      //   }, {
      //       arg: 'type',
      //       type: 'string'
      //   }, {
      //       arg: 'stateIds',
      //       type: 'array'
      //   },
      //   {
      //       arg: 'districtIds',
      //       type: 'array'
      //   }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });

  Userinfo.getUserInfoBasedOnDesignation = function (param, cb) {
    var self = this;
    var UserInfoCollection = self.getDataSource().connector.collection(Userinfo.modelName);
    let dynamicMatch = {};

    //========condition added by preeti-------------------------

    const status = typeof (param.status) == "boolean" ? [param.status] : param.status;
    const designation = typeof (param.designationLevel) == "string" ? [param.designationLevel] : param.designationLevel;
    //-------------------------------------------

    if (param.stateId == undefined) {
      // modify by ravi and using in delete hierarchy filters
      dynamicMatch = {
        companyId: ObjectId(param.companyId),
        designationLevel: {
          $in: designation
        },
        status: {
          $in: status
        }
      };
    } else {
      let stateArr = [];
      //========condition added by preeti-------------------------

      const stateIds = typeof (param.stateId) == "string" ? [param.stateId] : param.stateId
      //========------------------------

      if (stateIds.length > 0) {
        for (var i = 0; i < stateIds.length; i++) {
          stateArr.push(ObjectId(stateIds[i]));
        }
      }
      dynamicMatch = {
        companyId: ObjectId(param.companyId),
        designationLevel: {
          $in: designation
        },
        status: {
          $in: status
        },
        stateId: {
          $in: stateArr
        }
      };
    }
    if (param.isDivisionExist == true && param.divisionId !== null) {
      let passingDivisionIds = [];

      //========condition added by preeti-------------------------

      const divisionIds = typeof (param.division) == "string" ? [param.division] : param.division

      //-----------------------------------

      for (let i = 0; i < divisionIds.length; i++) {
        passingDivisionIds[i] = ObjectId(divisionIds[i]);
      }
      dynamicMatch.divisionId = {
        $in: passingDivisionIds
      };
    }
    //added by praveen singh for getting users based on district ids
    if (param.districtId !== undefined) {
      let districtArr = [];
      //========condition added by preeti-------------------------

      const districtId = typeof (param.districtId) === "string" ? [param.districtId] : param.districtId;

      //---------------------------------------------------------

      if (param.districtId.length > 0) {
        for (var i = 0; i < districtId.length; i++) {
          districtArr.push(ObjectId(districtId[i]));
        }
      }
      dynamicMatch.districtId = {
        $in: districtArr
      };
    }

    if (param.checkingLevel == 0) {

      UserInfoCollection.aggregate(
        // Stage 1
        {
          $match: dynamicMatch
        },
        // Stage 4
        {
          $project: {
            _id: 0,
            userId: 1,
            name: 1
          }
        },
        // Stage 4
        {
          $sort: {
            name: 1
          }
        },
        function (err, result) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: param,
              methodName: "getUserInfoBasedOnDesignation"
            });
          }
          return cb(false, result);
        }
      );
    } else if (param.checkingLevel >= 2) {

      Userinfo.app.models.Hierarchy.find(
        {
          where: {
            companyId: ObjectId(param.companyId),
            supervisorId: ObjectId(param.checkingUserId),
            userDesignationLevel: {
              inq: designation
            },
            status: true
          }
        },
        function (err, hrcyResult) {
          // // console.log(hrcyResult)
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: param,
              methodName: "getUserInfoBasedOnDesignation"
            });
            // console.log(err);
          }
          let passingUserIds = [];
          for (let i = 0; i < hrcyResult.length; i++) {
            passingUserIds.push(hrcyResult[i].userId);
          }
          dynamicMatch.userId = {
            $in: passingUserIds
          };

          UserInfoCollection.aggregate(
            // Stage 1
            {
              $match: dynamicMatch
            },
            // Stage 4
            {
              $project: {
                _id: 0,
                userId: 1,
                name: 1
              }
            },
            // Stage 4
            {
              $sort: {
                name: 1
              }
            },
            function (err, result) {
              if (err) {
                sendMail.sendMail({
                  collectionName: "Userinfo",
                  errorObject: err,
                  paramsObject: param,
                  methodName: "getUserInfoBasedOnDesignation"
                });
              }
              //// console.log(result)
              //return cb(false, result)
              if (result.length > 0) {
                return cb(false, result);
              } else {
                return cb(false, []);
              }
            }
          );
        }
      );
    }
  };

  Userinfo.remoteMethod("getUserInfoBasedOnDesignation", {
    description: "All Employees Detail based on CompanyId and stateId",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------------------Praveen Kuumar(03-10-2018)-------------------------------------------
  Userinfo.getAllUserIdsOnCompanyBasis = function (
    companyId,
    status,
    userIds,
    cb
  ) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    let dynamicMatch = {};
    let userIdArr = [];
    if (userIds != undefined && userIds.length > 0) {
      for (var w in userIds) {
        userIdArr.push(ObjectId(userIds[w]));
      }
      var status = typeof (status) == "boolean" ? [status] : status

      dynamicMatch = {
        companyId: ObjectId(companyId),
        status: { $in: status },
        userId: {
          $in: userIdArr
        }
      };
    } else {
      dynamicMatch = {
        companyId: ObjectId(companyId),
        status: { $in: status },
        designationLevel: {
          $ne: 0
        }
      };
    }

    UserInfoCollection.aggregate(
      // Stage 1
      {
        $match: dynamicMatch
      },
      // Stage 2
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "stateInfo"
        }
      },
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "districtInfo"
        }
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "userPersonalDetail"
        }
      },
      // Stage 3
      {
        $unwind: "$userPersonalDetail"
      },
      {
        $unwind: "$stateInfo"
      },
      {
        $unwind: "$districtInfo"
      },
      // Stage 4
      {
        $project: {
          _id: 0,
          userId: 1,
          name: "$userPersonalDetail.name",
          dateOfJoining: "$userPersonalDetail.dateOfJoining",
          employeeCode: "$userPersonalDetail.employeeCode",
          designation: 1,
          joiningDate: "$promotedOn",
          stateId: 1,
          stateName: "$stateInfo.stateName",
          districtId: 1,
          districtName: "$districtInfo.districtName"
        }
      },
      {
        $sort: {
          stateName: 1,
          districtName: 1,
          name: 1
        }
      },
      // Stage 5
      {
        $group: {
          _id: {},
          userIds: {
            $addToSet: "$userId"
          },
          obj: {
            $addToSet: {
              userId: "$userId",
              name: "$name",
              designation: "$designation",
              joiningDate: "$joiningDate",
              stateId: "$stateId",
              stateName: "$stateName",
              districtId: "$districtId",
              districtName: "$districtName",
              employeeCode: "$employeeCode",
              dateOfJoining: "$dateOfJoining"
            }
          }
        }
      },
      function (err, result) {
        console.log("user result=>", result);

        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: [companyId, status, userIds],
            methodName: "getAllUserIdsOnCompanyBasis"
          });
        }
        // // console.log(result);
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getAllUserIdsOnCompanyBasis", {
    description: "get User Id On District Basis",
    accepts: [
      {
        arg: "companyId",
        type: "string"
      },
      {
        arg: "status",
        type: "array"
      },
      {
        arg: "userIds",
        type: "array"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //-------------------------------------------END---------------------------------------------------------
  Userinfo.getAllUserIdsOnMultiDistrictBasis = function (param, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    let districtIds = [];
    for (let i = 0; i < param.districts.length; i++) {
      districtIds.push(ObjectId(param.districts[i]));
    }
    UserInfoCollection.aggregate(
      // Stage 1
      {
        $match: {
          companyId: ObjectId(param.companyId),
          status: true,
          districtId: {
            $in: districtIds
          },
          designationLevel: { $in: param.designationLevel },
          rL: { $in: param.rL }
        }
      },
      // Stage 2
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "UserData"
        }
      },
      // Stage 3
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "District"
        }
      },
      // Stage 4
      {
        $unwind: "$UserData"
      },
      // Stage 5
      {
        $unwind: "$District"
      },
      // Stage 6
      {
        $project: {
          userId: "$userId",
          districtId: "$districtId",
          districtName: "$District.districtName",
          name: "$UserData.name",
          designation: "$designation"
        }
      },
      function (err, response) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: param,
            methodName: "getAllUserIdsOnMultiDistrictBasis"
          });
        }
        if (response.length > 0) {
          return cb(false, response);
        } else {
          return cb(false, []);
        }
      }
    );
  };
  Userinfo.remoteMethod("getAllUserIdsOnMultiDistrictBasis", {
    description: "get User Id On District Basis",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------Praveen Kumar(04-12-2018)--------------------------------------
  Userinfo.getUniqueStateOnManager = function (parameters, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    Userinfo.app.models.Hierarchy.find(
      {
        where: {
          companyId: parameters.companyId,
          supervisorId: parameters.supervisorId,
          //userDesignation: "MR",
          status: true
        }
      },
      function (err, hrcyResult) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: parameters,
            methodName: "getUniqueStateOnManager"
          });
          // console.log(err);
        }
        let passingUserIds = [];
        for (let i = 0; i < hrcyResult.length; i++) {
          passingUserIds.push(hrcyResult[i].userId);
        }
        let dynamicMatch = {};
        if (parameters.isDivisionExist == true) {
          let divisionIds = [];
          for (let i = 0; i < parameters.division.length; i++) {
            divisionIds.push(ObjectId(parameters.division[i]));
          }
          dynamicMatch = {
            companyId: ObjectId(parameters.companyId),
            userId: {
              $in: passingUserIds
            },
            divisionId: {
              $in: divisionIds
            }
          };
        } else if (parameters.isDivisionExist == false) {
          dynamicMatch = {
            companyId: ObjectId(parameters.companyId),
            userId: {
              $in: passingUserIds
            }
          };
        }
        UserInfoCollection.aggregate(
          {
            $match: dynamicMatch
          },
          {
            $lookup: {
              from: "State",
              localField: "stateId",
              foreignField: "_id",
              as: "stateDetails"
            }
          },
          {
            $project: {
              stateId: {
                $arrayElemAt: ["$stateDetails._id", 0]
              },
              stateName: {
                $arrayElemAt: ["$stateDetails.stateName", 0]
              }
            }
          },
          {
            $sort: {
              stateName: 1
            }
          },
          {
            $group: {
              _id: {},
              stateIds: {
                $addToSet: "$stateId"
              },
              stateObj: {
                $addToSet: {
                  id: "$stateId",
                  stateName: "$stateName"
                }
              }
            }
          },
          function (err, result) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Userinfo",
                errorObject: err,
                paramsObject: parameters,
                methodName: "getUniqueStateOnManager"
              });
            }
            // console.log("Pkkk",result);
            return cb(false, result);
          }
        );
      }
    );
  };
  Userinfo.remoteMethod("getUniqueStateOnManager", {
    description: "get State and District on Manager Basis",
    accepts: [
      {
        arg: "parameters",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  Userinfo.getUniqueHeadquarterOnManager = function (parameters, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    Userinfo.app.models.Hierarchy.find(
      {
        where: {
          companyId: parameters.companyId,
          supervisorId: parameters.supervisorId,
          //userDesignation: "MR",
          status: true
        }
      },
      function (err, hrcyResult) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: parameters,
            methodName: "getUniqueHeadquarterOnManager"
          });
          // console.log(err);
        }
        let passingUserIds = [];
        for (let i = 0; i < hrcyResult.length; i++) {
          passingUserIds.push(hrcyResult[i].userId);
        }
        let passingStateIds = [];
        for (let i = 0; i < parameters.stateIds.length; i++) {
          passingStateIds.push(ObjectId(parameters.stateIds[i]));
        }
        let dynamicMatch = {};
        if (passingStateIds.length > 0) {
          dynamicMatch = {
            companyId: ObjectId(parameters.companyId),
            userId: {
              $in: passingUserIds
            },
            stateId: {
              $in: passingStateIds
            },
            status: true
          };
        } else {
          // For by default all unique hq on manager level
          if (parameters.isContainStateFilter == true) {
            dynamicMatch = {
              companyId: ObjectId(parameters.companyId),
              userId: {
                $in: passingUserIds
              },
              status: true,
              stateId: "" // No state is slected
            };
          } else if (parameters.isContainStateFilter == false) {
            dynamicMatch = {
              companyId: ObjectId(parameters.companyId),
              userId: {
                $in: passingUserIds
              },
              status: true
            };
          }
        }
        //---------------PK : Code Added on (24-06-2019)--------------------
        if (parameters.isDivisionExist == true) {
          let divisionIds = [];
          for (let i = 0; i < parameters.division.length; i++) {
            divisionIds.push(ObjectId(parameters.division[i]));
          }
          dynamicMatch.divisionId = {
            $in: divisionIds
          };
        }
        //--------------------------END-------------------------------
        UserInfoCollection.aggregate(
          {
            $match: dynamicMatch
          },
          {
            $lookup: {
              from: "District",
              localField: "districtId",
              foreignField: "_id",
              as: "districtDetails"
            }
          },
          {
            $project: {
              districtId: {
                $arrayElemAt: ["$districtDetails._id", 0]
              },
              districtName: {
                $arrayElemAt: ["$districtDetails.districtName", 0]
              }
            }
          },
          {
            $sort: {
              districtName: 1
            }
          },
          {
            $group: {
              _id: {},
              districtObj: {
                $addToSet: {
                  id: "$districtId",
                  districtName: "$districtName"
                }
              },
              districtIds: {
                $addToSet: "$districtId"
              }
            }
          },
          function (err, result) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Userinfo",
                errorObject: err,
                paramsObject: parameters,
                methodName: "getUniqueHeadquarterOnManager"
              });
            }
            return cb(false, result);
          }
        );
      }
    );
  };
  Userinfo.remoteMethod("getUniqueHeadquarterOnManager", {
    description: "get State and District on Manager Basis",
    accepts: [
      {
        arg: "parameters",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //------------------------------------END--------------------------------------------------
  //-----------------Rahul-31-12-2018------------------END--------------------------------------------------
  Userinfo.updateUserStatus = function (params, cb) {
    var self = this;
    var userCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    userCollection.update(
      {
        companyId: ObjectId(params.companyId),
        userId: ObjectId(params.modifyUserId)
      },
      {
        $set: {
          updatedAt: new Date(),
          updatedBy: ObjectId(params.updatedBy),
          status: params.changeTo,
          deactivationDate: new Date(),
          statusReason:params.msg
        }
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "updateUserStatus"
          });
          return cb(err);
        } else {



          Userinfo.app.models.Hierarchy.updateHierarchyStatus(params, function (err, result) {
            if (err) {
              return cb(err);
            } else {
              return cb(false, result);
            }
          });

        }
      }
    );
  };
  Userinfo.remoteMethod("updateUserStatus", {
    description: "Delete user",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "get"
    }
  });
  //------------------------------------END--------------------------------------------------
  Userinfo.editUserinfo = function (params, cb) {
    var self = this;
    let rL;
    var UserinfoEditCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    if (params.designationLevel === 1) {
      rL = 1;
    } else {
      rL = 2;
    }
    if (params.blockMsg === null) {
      params.blockMsg = "";
    }
    if (params.blockDate === null) {
      params.blockDate = "";
    }
    Userinfo.find(
      {
        where: {
          companyId: ObjectId(params.companyId),
          userId: ObjectId(params._id)
        }
      },
      function (err, response) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "editUserinfo"
          });
        }
        if (response.length > 0) {
          UserinfoEditCollection.update(
            {
              companyId: ObjectId(params.companyId),
              userId: ObjectId(params._id)
            },
            {
              $set: {
                designation: params.designation,
                name: params.name,
                designationLevel: params.designationLevel,
                reportingDate: params.reportingDate,
                status: params.status,
                lockingPeriod: params.lockingPeriod,
                rL: rL,
                updatedBy: ObjectId(params.updatedBy),
                updatedAt: new Date(),
                blockDate: params.blockDate,
                blockMsg: params.blockMsg
              }
            },
            function (err, result) {
              if (err) {
                sendMail.sendMail({
                  collectionName: "Userinfo",
                  errorObject: err,
                  paramsObject: params,
                  methodName: "editUserinfo"
                });
                // console.log(err);
                return cb(err);
              } else {
                return cb(false, result);
                // console.log("User has been updated")
              }
            }
          );
        } else {
          var err = new Error("User cannot be Edit.");
          err.statusCode = 409;
          err.code = "Validation failed";
          return cb(err);
        }
      }
    );
  };
  Userinfo.remoteMethod("editUserinfo", {
    description: "Edit user",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: {
      verb: "get"
    }
  });
  //by ravi on 14-01-2019
  Userinfo.getAreaCountEmployeewise = function (companyId, userId, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let userIds = [];
    if (userId.length > 0) {
      for (let i = 0; i < userId.length; i++) {
        userIds.push(ObjectId(userId[i]));
      }
    }
    userCollection.aggregate(
      // Stage 1
      {
        $match: {
          companyId: ObjectId(companyId),
          userId: { $in: userIds },
          status: true
        }
      },
      // Stage 2
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "District"
        }
      },
      // Stage 3
      {
        $lookup: {
          from: "UserAreaMapping",
          localField: "userId",
          foreignField: "userId",
          as: "MappedArea"
        }
      },
      // Stage 4
      {
        $unwind: "$MappedArea"
      },
      // Stage 5
      {
        $lookup: {
          from: "Area",
          localField: "MappedArea.areaId",
          foreignField: "_id",
          as: "Area"
        }
      },
      // Stage 6
      {
        $unwind: "$Area"
      },
      // Stage 7
      {
        $match: {
          "Area.status": false,
          "Area.appStatus": "pending"
        }
      },
      // Stage 8
      {
        $group: {
          _id: {
            userId: "$userId",
            userName: "$name",
            district: "$District"
          },
          count: {
            $sum: 1
          }
        }
      },
      // Stage 9
      {
        $project: {
          _id: 0,
          userId: "$_id.userId",
          userName: "$_id.userName",
          District: {
            $arrayElemAt: ["$_id.district.districtName", 0]
          },
          count: "$count"
        }
      },
      function (err, userAreaCount) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: [companyId, userId],
            methodName: "getAreaCountEmployeewise"
          });
          // console.log(err);
          return cb(err);
        }
        if (!userAreaCount) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, userAreaCount);
      }
    );
  };
  Userinfo.remoteMethod("getAreaCountEmployeewise", {
    description:
      "This method is used to get the all area count of the paticular users",
    accepts: [
      {
        arg: "companyId",
        type: "string"
      },
      {
        arg: "userId",
        type: "array"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //end
  //--------------------------Praveen Kumar(11-04-2019)------------------------------
  Userinfo.getUniqueDesignationOnStateOrDistrict = function (param, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let dynamicMatch = {};
    if (param.lookingFor == "onState") {
      let stateIdArray = [];
      for (let i = 0; i < param.stateIds.length; i++) {
        stateIdArray.push(ObjectId(param.stateIds[i]));
      }
      dynamicMatch = {
        companyId: ObjectId(param.companyId),
        stateId: {
          $in: stateIdArray
        },
        status: true
      };
    } else if (param.lookingFor == "onDistrict") {
      let districtIdArray = [];
      for (let i = 0; i < param.districtIds.length; i++) {
        districtIdArray.push(ObjectId(param.districtIds[i]));
      }
      dynamicMatch = {
        companyId: ObjectId(param.companyId),
        districtId: {
          $in: districtIdArray
        },
        status: true
      };
    }
    userCollection.aggregate(
      {
        $match: dynamicMatch
      },
      {
        $group: {
          _id: {
            designation: "$designation"
          }
        }
      },
      {
        $lookup: {
          from: "Designation",
          localField: "_id.designation",
          foreignField: "designation",
          as: "desig"
        }
      },
      {
        $unwind: "$desig"
      },
      {
        $match: {
          "desig.companyId": ObjectId(param.companyId)
        }
      },
      {
        $project: {
          _id: 0,
          designation: "$desig.designation",
          fullName: "$desig.fullName",
          designationLevel: "$desig.designationLevel"
        }
      },
      {
        $sort: {
          designationLevel: 1
        }
      },
      function (err, designationResult) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: param,
            methodName: "getUniqueDesignationOnStateOrDistrict"
          });
          // console.log(err);
          return cb(err);
        }
        if (!designationResult) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, designationResult);
      }
    );
  };
  Userinfo.remoteMethod("getUniqueDesignationOnStateOrDistrict", {
    description:
      "This method is used to get the all Unique Designation which exists on state or district",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------------------END--------------------------------------------
  //-----------------Rahul Choudhary-11:03-2019----------------------------------------------------
  //getting designation list state wise
  Userinfo.getAllDesignationOnStateBasis = function (param, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    //update condition code by ravi on 11-05-2019
    var dMatch = {};
    var stateIds = [];
    var districtIds = [];
    if (param.reportType == "Statewise" || param.reportType == "Employeewise") {
      dMatch = {
        stateId: ObjectId(param.stateId),
        companyId: ObjectId(param.companyId),
        designationLevel: { $nin: [0] },
        status: true
      };
    } else if (param.reportType == "Headquarterwise") {
      if (param.stateId.length > 0) {
        for (let i = 0; i < param.stateId.length; i++) {
          stateIds.push(ObjectId(param.stateId[i]));
        }
      }
      if (param.districtInfo.length > 0) {
        for (let ii = 0; ii < param.districtInfo.length; ii++) {
          districtIds.push(ObjectId(param.districtInfo[ii]));
        }
      }
      dMatch = {
        stateId: { $in: stateIds },
        companyId: ObjectId(param.companyId),
        districtId: { $in: districtIds },
        designationLevel: { $nin: [0] },
        status: true
      };
    }
    //end
    UserInfoCollection.aggregate(
      {
        $match: dMatch
      },
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "users"
        }
      },
      {
        $project: {
          designation: "$designation",
          designationLevel: "$designationLevel"
        }
      },
      // Stage 4
      {
        $group: {
          _id: {
            designation: "$designation",
            designationLevel: "$designationLevel"
          }
        }
      },
      {
        $sort: {
          designationLevel: -1
        }
      },
      {
        cursor: {
          batchSize: 50
        },
        allowDiskUse: true
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: param,
            methodName: "getAllDesignationOnStateBasis"
          });
          // console.log(err);
          return cb(err);
        }
        if (!result) {
          var err = new Error("no records found");
          err.statusCode = 404;
          err.code = "NOT FOUND";
          return cb(err);
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getAllDesignationOnStateBasis", {
    description: "get designation based on stateId",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------------------------------------------------------
  //praveen singh getUser Info with user ids based on designations or state wise or districtwise
  Userinfo.getUserIds = function (params, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    let stateIds = [];
    let match = {};
    let districtIds = [];
    let divisionId = [];

    if (params.isDivisionExist == true) {
      for (var i = 0; i < params.division.length; i++) {
        divisionId.push(ObjectId(params.division[i]));
      }
      match.divisionId = { $in: divisionId };
    }

    if (params["type"].toLowerCase() === "state") {
      for (let stateId of params["stateId"]) {
        stateIds.push(ObjectId(stateId));
      }
      match.stateId = {
        $in: stateIds
      };
    } else if (params["type"].toLowerCase() === "headquarter") {
      for (let stateId of params["stateId"]) {
        stateIds.push(ObjectId(stateId));
      }
      match.stateId = {
        $in: stateIds
      };
      for (let districtId of params["districtIds"]) {
        districtIds.push(ObjectId(districtId));
      }
      match.districtId = {
        $in: districtIds
      };
    } else if (params["type"].toLowerCase() === "employee wise") {
      match.designation = params.designation;
    }
    if (params.rL === 0) {
      if (params.isDivisionExist == true) {
        let match = {
          companyId: ObjectId(params["companyId"]),
          divisionId: { $in: divisionId },
          status: {
            $in: params.status
          },
          desigationLevel: {
            $ne: 0
          }
        };
      } else {
        let match = {
          companyId: ObjectId(params["companyId"]),
          status: {
            $in: params.status
          },
          desigationLevel: {
            $ne: 0
          }
        };
      }

      UserInfoCollection.aggregate(
        {
          $match: match
        },
        {
          $group: {
            _id: null,
            userIds: {
              $addToSet: "$userId"
            }
          }
        },
        function (err, result) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: params,
              methodName: "getUserIds"
            });
          }
          return cb(false, result);
        }
      );
    } else if (params.rL > 1) {
      // if reporting level is manager then getting his hierarchy
      Userinfo.app.models.Hierarchy.getManagerHierarchyInArrayBasedOnStateOrDistrictOrDesignationwise(
        params,
        function (err, result) {
          cb(false, result);
        }
      );
    }
  };
  Userinfo.remoteMethod("getUserIds", {
    description:
      "get Employees based on designation or statewise or districtwise",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------Praveen Kumar(09-08-2019)------------------------------
  Userinfo.getDesignationBasedOnDivision = function (param, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let dynamicMatch = {};
    let divisionIds = [];
    for (let i = 0; i < param.division.length; i++) {
      divisionIds.push(ObjectId(param.division[i]));
    }
    if (param.userLevel == 0) {
      dynamicMatch = {
        companyId: ObjectId(param.companyId),
        designationLevel: { $ne: 0 },
        status: true,
        divisionId: {
          $in: divisionIds
        }
      };
      if (param.hasOwnProperty("stateId") === true) {
        let stateArr = [];
        for (const state of param.stateId) {
          stateArr.push(ObjectId(state));
        }
        dynamicMatch.stateId = {
          $in: stateArr
        };
      }
      if (param.hasOwnProperty("districtId") === true) {
        let districtArr = [];
        for (const district of param.districtId) {
          districtArr.push(ObjectId(district));
        }
        dynamicMatch.districtId = {
          $in: districtArr
        };
      }
      userCollection.aggregate(
        {
          $match: dynamicMatch
        },
        {
          $group: {
            _id: {
              designation: "$designation"
            }
          }
        },
        {
          $lookup: {
            from: "Designation",
            localField: "_id.designation",
            foreignField: "designation",
            as: "desig"
          }
        },
        {
          $unwind: "$desig"
        },
        {
          $match: {
            "desig.companyId": ObjectId(param.companyId)
          }
        },
        {
          $project: {
            _id: 0,
            designation: "$desig.designation",
            fullName: "$desig.fullName",
            designationLevel: "$desig.designationLevel"
          }
        },
        {
          $sort: {
            designationLevel: 1
          }
        },
        function (err, designationResult) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: param,
              methodName: "getDesignationBasedOnDivision"
            });
            // console.log(err);
            return cb(err);
          }
          if (!designationResult) {
            var err = new Error("no records found");
            err.statusCode = 404;
            err.code = "NOT FOUND";
            return cb(err);
          }
          return cb(false, designationResult);
        }
      );
    } else if (param.userLevel >= 2) {
      Userinfo.app.models.Hierarchy.find(
        {
          where: {
            companyId: ObjectId(param.companyId),
            supervisorId: ObjectId(param.userId),
            status: true
            //divisionId: {
            //    $in: divisionIds
            //}
          }
        },
        function (err, hrcyResult) {
          if (err) {
            sendMail.sendMail({
              collectionName: "Userinfo",
              errorObject: err,
              paramsObject: param,
              methodName: "getDesignationBasedOnDivision"
            });
            return err;
          }
          if (hrcyResult.length > 0) {
            let passingUserIds = [];
            for (let i = 0; i < hrcyResult.length; i++) {
              passingUserIds.push(hrcyResult[i].userId);
            }
            dynamicMatch = {
              companyId: ObjectId(param.companyId),
              userId: {
                $in: passingUserIds
              },
              status: true,
              divisionId: {
                $in: divisionIds
              }
            };
            userCollection.aggregate(
              {
                $match: dynamicMatch
              },
              {
                $group: {
                  _id: {
                    designation: "$designation"
                  }
                }
              },
              {
                $lookup: {
                  from: "Designation",
                  localField: "_id.designation",
                  foreignField: "designation",
                  as: "desig"
                }
              },
              {
                $unwind: "$desig"
              },
              {
                $match: {
                  "desig.companyId": ObjectId(param.companyId)
                }
              },
              {
                $project: {
                  _id: 0,
                  designation: "$desig.designation",
                  fullName: "$desig.fullName",
                  designationLevel: "$desig.designationLevel"
                }
              },
              {
                $sort: {
                  designationLevel: 1
                }
              },
              function (err, designationResult) {
                if (err) {
                  sendMail.sendMail({
                    collectionName: "Userinfo",
                    errorObject: err,
                    paramsObject: param,
                    methodName: "getDesignationBasedOnDivision"
                  });
                  // console.log(err);
                  return cb(err);
                }
                if (!designationResult) {
                  var err = new Error("no records found");
                  err.statusCode = 404;
                  err.code = "NOT FOUND";
                  return cb(err);
                }
                return cb(false, designationResult);
              }
            );
          }
        }
      );
    }
  };
  Userinfo.remoteMethod("getDesignationBasedOnDivision", {
    description:
      "This method is used to get the all Unique Designation which exists on Division",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //----------------------------END--------------------------------------------------------
  // -------------------------get resigned user details by preeti arora--------------------------------------
  Userinfo.getResignedEmployeeDetails = function (params, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let stateId = [];
    let districtId = [];
    let divisionId = [];
    let userId = [];
    let where = {
      status: false,
      companyId: ObjectId(params.companyId),
      deactivationDate: {
        $gte: new Date(params.fromDate),
        $lte: new Date(params.toDate)
      }
    };
    if (params.type == "State") {
      for (var i = 0; i < params.stateIds.length; i++) {
        stateId.push(ObjectId(params.stateIds[i]));
      }
      where.stateId = { $in: stateId };
    }
    if (params.type == "Headquarter") {
      for (var i = 0; i < params.districtIds.length; i++) {
        districtId.push(ObjectId(params.districtIds[i]));
      }
      where.districtId = { $in: districtId };
    }
    if (params.isDivisionExist == true) {
      for (var i = 0; i < params.division.length; i++) {
        divisionId.push(ObjectId(params.division[i]));
      }
      where.divisionId = { $in: divisionId };
    }
    if (params.type == "Employee Wise") {
      for (var i = 0; i < params.userIds.length; i++) {
        userId.push(ObjectId(params.userIds[i]));
      }
      where.userId = { $in: userId };
    }
    // console.log("where : ",where);
    userCollection.aggregate(
      // Stage 1
      {
        $match: where
      },
      // Stage 2
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "district"
        }
      },
      // Stage 3
      {
        $lookup: {
          from: "DCRMaster",
          localField: "userId",
          foreignField: "submitBy",
          as: "dcrDetails"
        }
      },
      // Stage 4
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "userData"
        }
      },
      // Stage 5
      {
        $unwind: "$userData"
      },
      {
        $unwind: "$divisionId"
      },
      {
        $lookup: {
          from: "DivisionMaster",
          localField: "divisionId",
          foreignField: "_id",
          as: "divisionData"
        }
      },
      {
        $unwind: "$divisionData"
      },
      // Stage 6
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "state"
        }
      },
      // Stage 7
      {
        $unwind: "$district"
      },
      // Stage 8
      {
        $unwind: "$state"
      },
      // Stage 9
      {
        $unwind: { path: "$dcrDetails", preserveNullAndEmptyArrays: true }
      },
      // Stage 10
      {
        $sort: {
          "dcrDetails.dcrDate": 1
        }
      },
      // Stage 11
      {
        $group: {
          _id: {
            submitBy: "$dcrDetails.submitBy"
          },
          lastDCRDate: {
            $last: "$dcrDetails.dcrDate"
          },
          firstDCRDate: {
            $first: "$dcrDetails.dcrDate"
          },
          Name: { $addToSet: "$name" },
          district: { $addToSet: "$district.districtName" },
          designation: { $addToSet: "$designation" },
          state: { $addToSet: "$state.stateName" },
          employeeId: { $addToSet: "$userData.username" },
          divisionName: { $addToSet: "$divisionData.divisionName" }
        }
      },
      // Stage 12
      {
        $project: {
          Name: { $arrayElemAt: ["$Name", 0] },
          employeeId: { $arrayElemAt: ["$employeeId", 0] },
          state: { $arrayElemAt: ["$state", 0] },
          designation: { $arrayElemAt: ["$designation", 0] },
          district: { $arrayElemAt: ["$district", 0] },
          lastDCRDate: "$lastDCRDate",
          firstDCRDate: "$firstDCRDate",
          divisionName: { $arrayElemAt: ["$divisionName", 0] }
        }
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getResignedEmployeeDetails"
          });
        }
        // console.log("dcr result =",result);
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getResignedEmployeeDetails", {
    description: "This method is used to get resigned employees details",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //---------------------------end----------------------------
  // -------------------------get getLockedDetails user details by preeti arora--------------------------------------
  Userinfo.getLockedDetails = function (params, cb) {
    // console.log("-------------------------------------------");
    // console.log("Param is  ",params)
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let userId = [];
    let where = {
      companyId: ObjectId(params[0].companyId)
    };
    for (var i = 0; i < params.length; i++) {
      userId.push(ObjectId(params[i].userId));
    }
    where.userId = { $in: userId };
    userCollection.aggregate(
      // Stage 1
      {
        $match: where
      },
      // Stage 2
      {
        $lookup: {
          from: "District",
          localField: "districtId",
          foreignField: "_id",
          as: "district"
        }
      },
      // Stage 3
      {
        $lookup: {
          from: "DCRMaster",
          localField: "userId",
          foreignField: "submitBy",
          as: "dcrDetails"
        }
      },
      // Stage 4
      {
        $lookup: {
          from: "UserLogin",
          localField: "userId",
          foreignField: "_id",
          as: "userData"
        }
      },
      // Stage 5
      {
        $unwind: "$userData"
      },
      // Stage 6
      {
        $lookup: {
          from: "State",
          localField: "stateId",
          foreignField: "_id",
          as: "state"
        }
      },
      // Stage 7
      {
        $unwind: "$district"
      },
      // Stage 8
      {
        $unwind: "$state"
      },
      // Stage 9
      {
        $unwind: "$dcrDetails"
      },
      // Stage 10
      {
        $sort: {
          "dcrDetails.dcrDate": 1
        }
      },
      // Stage 11
      {
        $group: {
          _id: {
            submitBy: "$dcrDetails.submitBy"
          },
          lastDCRDate: {
            $last: "$dcrDetails.dcrDate"
          },
          firstDCRDate: {
            $first: "$dcrDetails.dcrDate"
          },
          Name: { $addToSet: "$name" },
          district: { $addToSet: "$district.districtName" },
          designation: { $addToSet: "$designation" },
          state: { $addToSet: "$state.stateName" },
          employeeId: { $addToSet: "$userData.username" }
        }
      },
      // Stage 12
      {
        $project: {
          Name: { $arrayElemAt: ["$Name", 0] },
          employeeId: { $arrayElemAt: ["$employeeId", 0] },
          state: { $arrayElemAt: ["$state", 0] },
          designation: { $arrayElemAt: ["$designation", 0] },
          district: { $arrayElemAt: ["$district", 0] },
          lastDCRDate: "$lastDCRDate",
          firstDCRDate: "$firstDCRDate"
        }
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getLockedDetails"
          });
        }
        return cb(false, result);
      }
    );
  };
  Userinfo.remoteMethod("getLockedDetails", {
    description: "This method is used to get resigned employees details",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //---------------------------end----------------------------
  //---------release user by preeti arora-------------
  Userinfo.releaseUserforOpeningLock = function (params, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    // console.log("parameters --",params);
    Userinfo.updateAll(
      {
        userId: { inq: params.userId }
      },
      {
        releaseDCRLockedDate: new Date(),
        updatedAt: new Date()
      },
      function (err, res) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "releaseUserforOpeningLock"
          });
        }
        if (res) {
          Userinfo.app.models.TrackLockOpenDetail.updateAll(
            {
              ReleasedStatus: "pending",
              userId: { inq: params.userId }
            },
            {
              ReleasedStatus: "approved",
              updatedAt: new Date()
            },
            function (err, res) {
              if (err) {
                sendMail.sendMail({
                  collectionName: "Userinfo",
                  errorObject: err,
                  paramsObject: params,
                  methodName: "releaseUserforOpeningLock"
                });
              }
              if (res) {
                return cb(false, res);
              }
            }
          );
        } else {
          return cb(false, err);
        }
      }
    );
  };
  Userinfo.remoteMethod("releaseUserforOpeningLock", {
    description: "This method is used to relaese the user",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //---------------------------
  // -------------------------get getLockedDetails user details by preeti arora--------------------------------------
  Userinfo.getAllStockistHqWise = function (params, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(
      Userinfo.modelName
    );
    let where = {
      companyId: ObjectId(params.companyId),
      districtId: ObjectId(params.districtId)
    };
    if (params.divisionId != null) {
      where.divisionId = ObjectId(params.divisionId[0]);
    }
    // console.log("==============================")
    // console.log("where : ",where)
    userCollection.aggregate(
      {
        $match: where
      },
      {
        $lookup: {
          from: "Providers",
          localField: "districtId",
          foreignField: "districtId",
          as: "providerData"
        }
      },
      {
        $unwind: "$providerData"
      },
      {
        $match: {
          "providerData.providerType": "Stockist"
        }
      },
      {
        $project: {
          stkId: "$providerData._id",
          stkName: "$providerData.providerName",
          name: 1,
          userId: 1,
          stateId: 1,
          districtId: 1
        }
      },
      function (err, result) {
        if (err != null) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getAllStockistHqWise"
          });
          // console.log("erro");
        } else {
          return cb(false, result);
        }
      }
    );
  };
  Userinfo.remoteMethod("getAllStockistHqWise", {
    description: "This method is used to all stockist hq wise",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //-------------end------------------------------------------
  //------------------get compamy details by preeti arora 16-01-2020-------
  Userinfo.getCompanyCreationDetails = function (params, cb) {
    var self = this;
    var UserLoginCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    UserLoginCollection.aggregate(
      [
        // Stage 1
        {
          $match: {
            //status:true
          }
        },
        // Stage 2
        {
          $lookup: {
            from: "CompanyMaster",
            localField: "companyId",
            foreignField: "_id",
            as: "companyDetails"
          }
        },
        // Stage 3
        {
          $unwind: "$companyDetails"
        },
        // Stage 4
        {
          $lookup: {
            from: "UserLogin",
            localField: "userId",
            foreignField: "_id",
            as: "loginDetails"
          }
        },
        // Stage 5
        {
          $unwind: "$loginDetails"
        },
        // Stage 6
        {
          $group: {
            _id: {
              companyId: "$companyId"
            },
            activeUsers: {
              $sum: {
                $cond: [
                  {
                    $and: [
                      {
                        $eq: ["$status", true]
                      }
                    ]
                  },
                  1,
                  0
                ]
              }
            },
            inActiveUsers: {
              $sum: {
                $cond: [
                  {
                    $and: [
                      {
                        $eq: ["$status", false]
                      }
                    ]
                  },
                  1,
                  0
                ]
              }
            },
            company: { $addToSet: "$companyDetails" },
            AdminContact: {
              $addToSet: {
                $cond: [
                  {
                    $and: [{ $eq: ["$designationLevel", 0] }]
                  },
                  "$loginDetails",
                  "$abc"
                ]
              }
            }
          }
        },
        // Stage 7
        {
          $project: {
            companyName: { $arrayElemAt: ["$company.name", 0] },
            maxUser: { $arrayElemAt: ["$company.maxUser", 0] },
            isDivisionExist: { $arrayElemAt: ["$company.isDivisionExist", 0] },
            createdAt: { $arrayElemAt: ["$company.createdAt", 0] },
            updatedAt: { $arrayElemAt: ["$company.updatedAt", 0] },
            status: { $arrayElemAt: ["$company.status", 0] },
            finaliseUser: { $arrayElemAt: ["$company.finaliseUser", 0] },
            activeUsers: 1,
            inActiveUsers: 1,
            adminContact: { $arrayElemAt: ["$AdminContact.mobile", 0] },
            adminEmail: { $arrayElemAt: ["$AdminContact.email", 0] },
            username: { $arrayElemAt: ["$AdminContact.username", 0] },
            password: { $arrayElemAt: ["$AdminContact.viewPassword", 0] }
          }
        }
      ],
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: params,
            methodName: "getCompanyCreationDetails"
          });
        }
        if (result.length > 0) {
          return cb(false, result);
        } else {
          return cb(false, []);
        }
      }
    );
  };
  Userinfo.remoteMethod("getCompanyCreationDetails", {
    description: "This method is used to all active users of all company",
    accepts: [
      {
        arg: "params",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //-------------end--------------------------
  //-----------------Rahul Choudhary-16:01-2020-and edit by Nipun---------------------------------------------------
  Userinfo.getUserDetailsForDayPlanLockOpen = function (param, cb) {
    // console.table(param);
    // console.log('==========> ', new Date(moment(param.lockOpenDate).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")));
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    var OpenDetailsCollection = self
      .getDataSource()
      .connector.collection(Userinfo.app.models.Dayplanlockopen.modelName);
    //update condition code by ravi on 11-05-2019
    var dMatch = {};
    var stateIds = [];
    var districtIds = [];
    var employeeIds = [];
    var divisionIds = [];
    dMatch = {
      companyId: ObjectId(param.companyId)
    };
    if (param.type == "State") {
      if (param.stateId.length > 0) {
        for (let i = 0; i < param.stateId.length; i++) {
          stateIds.push(ObjectId(param.stateId[i]));
        }
      }
      dMatch.stateId = { $in: stateIds };
    } else if (param.type == "Headquarter") {
      if (param.districtId.length > 0) {
        for (let ii = 0; ii < param.districtId.length; ii++) {
          districtIds.push(ObjectId(param.districtId[ii]));
        }
      }
      dMatch.districtId = { $in: districtIds };
    } else if (param.type == "Employee Wise") {
      if (param.employeeId.length > 0) {
        for (let ii = 0; ii < param.employeeId.length; ii++) {
          employeeIds.push(ObjectId(param.employeeId[ii]));
        }
      }
      dMatch.userId = { $in: employeeIds };
    }
    if (param.isDivisionExist == true) {
      if (param.divisionId.length > 0) {
        for (let i = 0; i < param.divisionId.length; i++) {
          divisionIds.push(ObjectId(param.divisionId[i]));
        }
      }
      dMatch.divisionId = { $in: divisionIds };
    }
    UserInfoCollection.aggregate(
      {
        $match: dMatch
      },
      function (err, result) {
        if (err) {
          sendMail.sendMail({
            collectionName: "Userinfo",
            errorObject: err,
            paramsObject: param,
            methodName: "getUserDetailsForDayPlanLockOpen"
          });
        }
        if (result.length > 0) {
          const newArray = result.map(res => {
            delete res._id;
            let programType =
              param && param.lockOpenDate ? "dayProgram" : "tourProgram";
            return { ...res, programType, type: param.type };
          });
          OpenDetailsCollection.insert(newArray, function (err, res) {
            if (err) {
              sendMail.sendMail({
                collectionName: "Userinfo",
                errorObject: err,
                paramsObject: param,
                methodName: "getUserDetailsForDayPlanLockOpen"
              });
            }
            if (res.ops.length > 0) {
              if (param && param.lockOpenDate && param.lockOpenDate !== null) {
                UserInfoCollection.updateMany(
                  dMatch,
                  {
                    $set: {
                      dayPlanLockTime: new Date(
                        moment(param.lockOpenDate).format(
                          "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
                        )
                      )
                    }
                  },
                  function (err, resp) {
                    if (err) {
                      sendMail.sendMail({
                        collectionName: "Userinfo",
                        errorObject: err,
                        paramsObject: param,
                        methodName: "getUserDetailsForDayPlanLockOpen"
                      });
                      // console.log(err);
                      return cb(err);
                    } else {
                      resp.dayplanUpdated = true;
                      return cb(false, resp);
                    }
                  }
                );
              } else if (param && param.date && param.date !== null) {
                let tpLockMonthDayBackup = [];
                if (result[0].tpLockMonthDayBackup) {
                  for (var i = 0; i < result[0].tpLockMonthDayBackup.length; i++) {
                    tpLockMonthDayBackup.push(result[0].tpLockMonthDayBackup[i])
                  }
                  tpLockMonthDayBackup.push({
                    tpLockMonthDay: parseInt(result[0].tpLockMonthDay),
                    updatedBy: ObjectId(param.updatedBy),
                    updatedAt: new Date()
                  })

                }
                else {
                  tpLockMonthDayBackup.push({
                    tpLockMonthDay: parseInt(result[0].tpLockMonthDay),
                    updatedBy: ObjectId(param.updatedBy),
                    updatedAt: new Date()
                  })
                }

                UserInfoCollection.updateMany(
                  dMatch,
                  {
                    $set: {
                      tpLockMonthDay: param.date,
                      tpLockMonthDayBackup: tpLockMonthDayBackup
                    }
                  },
                  function (err, resp) {
                    if (err) {
                      sendMail.sendMail({
                        collectionName: "Userinfo",
                        errorObject: err,
                        paramsObject: param,
                        methodName: "getUserDetailsForDayPlanLockOpen"
                      });
                      // console.log(err);
                      return cb(err);
                    } else {
                      resp.dayplanUpdated = true;
                      return cb(false, resp);
                    }
                  }
                );
              }
            }
          });
        } else if (!result.length) {
          res = {
            msg: "No matching user found in selected state"
          };
          return cb(false, res);
        }
      }
    );
  };
  Userinfo.remoteMethod("getUserDetailsForDayPlanLockOpen", {
    description: "get UserDetails For Day Plan Lock Open.",
    accepts: [
      {
        arg: "param",
        type: "object"
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "get"
    }
  });
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------Praveen Kumar(06-04-2019)------------------------------
  Userinfo.getAllDesignationForMail = function (param, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(Userinfo.modelName);
    let dynamicMatch = {};
    if (param.userLevel == 0) {
      dynamicMatch = {
        userId: { $ne: ObjectId(param.userId) },
        companyId: ObjectId(param.companyId),
        status: true
      }
      userCollection.aggregate({
        $match: dynamicMatch
      }, {
        $group: {
          _id: {
            designation: "$designation"
          }
        }
      }, {
        $lookup: {
          "from": "Designation",
          "localField": "_id.designation",
          "foreignField": "designation",
          "as": "desig"
        }
      }, {
        $unwind: "$desig"
      }, {
        $match: {
          "desig.companyId": ObjectId(param.companyId)
        }
      }, {
        $project: {
          _id: 0,
          designation: "$desig.designation",
          fullName: "$desig.fullName",
          designationLevel: "$desig.designationLevel"
        }
      }, {
        $sort: {
          designationLevel: 1
        }
      },
        function (err, designationResult) {
          if (err) {
            console.log(err);
            return cb(err);
          }
          if (!designationResult) {
            var err = new Error('no records found');
            err.statusCode = 404;
            err.code = 'NOT FOUND';
            return cb(err);
          }
          return cb(false, designationResult);
        });
    } else if (param.userLevel >= 2) {
      Userinfo.app.models.Hierarchy.getManagerHierarchy({
        companyId: param.companyId,
        supervisorId: param.userId,
        type: 'lowerWithUpperWithAdmin'
      }, function (err, userResult) {
        if (err) return cb(false, err)

        if (userResult.length > 0) {
          userCollection.aggregate({
            $match: {
              companyId: ObjectId(param.companyId),
              userId: {
                $in: userResult.map(user => ObjectId(user.userId))
              },
              status: true
            }
          }, {
            $group: {
              _id: {
                designation: "$designation"
              }
            }
          }, {
            $lookup: {
              "from": "Designation",
              "localField": "_id.designation",
              "foreignField": "designation",
              "as": "desig"
            }
          }, {
            $unwind: "$desig"
          }, {
            $match: {
              "desig.companyId": ObjectId(param.companyId)
            }
          }, {
            $project: {
              _id: 0,
              designation: "$desig.designation",
              fullName: "$desig.fullName",
              designationLevel: "$desig.designationLevel"
            }
          }, {
            $sort: {
              designationLevel: 1
            }
          },
            function (err, designationResult) {
              if (err) {
                console.log(err);
                return cb(err);
              }
              if (!designationResult) {
                var err = new Error('no records found');
                err.statusCode = 404;
                err.code = 'NOT FOUND';
                return cb(err);
              }
              return cb(false, designationResult);
            });
        }

      });

    }
  }
  Userinfo.remoteMethod(
    'getAllDesignationForMail', {
    description: 'This method is used to get the all Unique Designation which exists on Division',
    accepts: [{
      arg: 'param',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  );
  Userinfo.getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp = function (param, cb) {
    var self = this;
    var userCollection = this.getDataSource().connector.collection(Userinfo.modelName);
    let dynamicMatch = {};
    if (param.checkingLevel == 0) {
      if (param.lookingOn == "division") {
        dynamicMatch.divisionId = { $in: param.divisionIds.map(div => ObjectId(div)) };
      } else if (param.lookingOn == "state") {
        dynamicMatch.stateId = { $in: param.stateIds.map(state => ObjectId(state)) };
      } else if (param.lookingOn == "district") {
        dynamicMatch.districtId = { $in: param.districtIds.map(district => ObjectId(district)) };
      }
      dynamicMatch.designation = { $in: param.desigations };
      dynamicMatch.status = { $in: param.status };
      dynamicMatch.companyId = ObjectId(param.companyId);

      userCollection.aggregate({
        $match: dynamicMatch
      }, {
        $lookup: {
          "from": "District",
          "localField": "districtId",
          "foreignField": "_id",
          "as": "districtDetails"
        }
      }, {
        $project: {
          _id: 0,
          userId: 1,
          name: 1,
          designation: 1,
          designationLevel: 1,
          stateId: 1,
          districtId: 1,
          districtName: { $arrayElemAt: ["$districtDetails.districtName", 0] },
          status: 1
        }
      }, {
        $sort: {
          designationLevel: 1
        }
      },
        function (err, userResult) {
          if (err) {
            console.log(err);
            return cb(err);
          }
          if (!userResult) {
            var err = new Error('no records found');
            err.statusCode = 404;
            err.code = 'NOT FOUND';
            return cb(err);
          }
          return cb(false, userResult);
        });
    } else if (param.checkingLevel >= 1) {
      Userinfo.app.models.Hierarchy.getManagerHierarchy({
        companyId: param.companyId,
        supervisorId: param.userId,
        type: 'lowerWithUpperWithAdmin'
      }, function (err, hrcyResult) {
        if (err) {
          return err;
        }
        if (hrcyResult.length > 0) {
          if (param.lookingOn == "division") {
            dynamicMatch.divisionId = { $in: param.divisionIds.map(div => ObjectId(div)) };
          } else if (param.lookingOn == "state") {
            dynamicMatch.stateId = { $in: param.stateIds.map(state => ObjectId(state)) };
          } else if (param.lookingOn == "district") {
            dynamicMatch.districtId = { $in: param.districtIds.map(district => ObjectId(district)) };
          }
          if (param.hasOwnProperty("desigations")) {
            dynamicMatch.designation = { $in: param.desigations };
          }
          dynamicMatch.status = { $in: param.status };
          dynamicMatch.companyId = ObjectId(param.companyId);
          dynamicMatch.userId = { $in: hrcyResult.map(user => ObjectId(user.userId)) };

          // console.log("Hierarchy Result : ", hrcyResult);
          // console.log("Dynamic Match : ", dynamicMatch);

          userCollection.aggregate({
            $match: dynamicMatch
          }, {
            $lookup: {
              "from": "District",
              "localField": "districtId",
              "foreignField": "_id",
              "as": "districtDetails"
            }
          }, {
            $project: {
              _id: 0,
              userId: 1,
              name: 1,
              designation: 1,
              designationLevel: 1,
              stateId: 1,
              districtId: 1,
              districtName: { $arrayElemAt: ["$districtDetails.districtName", 0] },
              status: 1
            }
          }, {
            $sort: {
              designationLevel: 1
            }
          },
            function (err, userResult) {
              if (err) {
                console.log(err);
                return cb(err);
              }
              if (!userResult) {
                var err = new Error('no records found');
                err.statusCode = 404;
                err.code = 'NOT FOUND';
                return cb(err);
              }
              return cb(false, userResult);
            });
        } else {
          return cb(false, [])
        }
      });
    }

  }
  Userinfo.remoteMethod(
    'getEmployeeBasedOnDivisionOrStateOrDistrictAndDesignationORAllEmp', {
    description: 'This method is used to get the all Employee exists on divison or state or district and designation or All Emp. This method specially used for Mail filter',
    accepts: [{
      arg: 'param',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  );
  //----------------------------------END---------------------------------------

  //--------update the add location button status here by preeti arora 18-09-2020--------------//
  Userinfo.updateStatus = function (param, cb) {
    var self = this;
    var UserInfoCollection = self
      .getDataSource()
      .connector.collection(Userinfo.modelName);
    param.forEach((element, i, array) => {
      let set = {}
      if (element.hasOwnProperty("isUpdateGeoLoc")) {
        if (element.isUpdateGeoLoc == true) {
          set["isUpdateGeoLoc"] = false
        } else {
          set["isUpdateGeoLoc"] = true
        }
      } else {
        set["isUpdateGeoLoc"] = false
      }
      let where = {
        companyId: element.companyId,
        userId: element.userId
      }
      Userinfo.update(where, set, function (err, res) {
        if (err) {
          console.log(err);
        } else {
          if (Object.is(array.length - 1, i)) {
            return cb(false, "Data Updated")
          }
        }

      })

    }
    );

  };
  Userinfo.remoteMethod("updateStatus", {
    description: "get Users based on stateId",
    accepts: [{ arg: "param", type: "array", http: { source: "body" } }],
    returns: {
      root: true,
      type: "array"
    },
    http: {
      verb: "post"
    }
  });


  //--------------------API for Birthdays And Anniversaries-----------------//
  Userinfo.getBirthdaysAndAnniversariesFromUserLogin = function (param) {
    let matchObj = {
      status: true,
      companyId: ObjectId(param.companyId),
    }
    return new Promise((resolve, reject) => {
      try {
        var self = this;
        var UserinfoCollection = this.getDataSource().connector.collection(Userinfo.modelName);

        UserinfoCollection.aggregate(
          {
            "$match": matchObj
          },
          {
            "$lookup": {
              "from": "UserLogin",
              "localField": "userId",
              "foreignField": "_id",
              "as": "userInfo"
            }
          },
          {
            "$unwind": {
              "path": "$userInfo",
              "preserveNullAndEmptyArrays": true
            }
          },
          {
            "$lookup": {
              "from": "State",
              "localField": "stateId",
              "foreignField": "_id",
              "as": "stateInfo"
            }
          },
          {
            "$unwind": {
              "path": "$stateInfo",
              "preserveNullAndEmptyArrays": true
            }
          },
          {
            $match:{
                "userInfo.dateOfBirth":{$exists:true},
                "userInfo.dateOfAnniversary":{$exists:true}
          }
        },
          {
            "$project": {
              "name": 1.0,
              "type": {$literal: "employee"},
              "dob": "$userInfo.dateOfBirth",
              "doa": "$userInfo.dateOfAnniversary",
              "state": "$stateInfo.stateName",
             "DOBmonth": { $month: "$userInfo.dateOfBirth" },
              "DOBdate": { $dayOfMonth: "$userInfo.dateOfBirth" },
             "DOAmonth": { $month: "$userInfo.dateOfAnniversary" },
              "DOAdate": { $dayOfMonth: "$userInfo.dateOfAnniversary" }
            },
          },
          {
            "$match": {
              $or: [
                { $and: [{ DOBmonth: param.month }, { DOBdate: param.date }] },
                { $and: [{ DOAmonth: param.month }, { DOAdate: param.date }] }
              ]
            }
          },
          function (err, userLoginRes) {
            if (err) reject(err);
            return resolve(userLoginRes);
          })
      } catch (err) {
        reject(err);
      }
    })

  };

  Userinfo.remoteMethod(
    'getBirthdaysAndAnniversariesFromUserLogin', {
    description: 'This method is used to get the Birthdays And Anniversaries from User Login',
    accepts: [{
      arg: 'param',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  )

  Userinfo.getAllUsersBirthdaysAndAnniversaries = async function (param) {
    var self = this;
    // var UserLoginCollection = this.getDataSource().connector.collection(Userinfo.modelName);
    // var ProviderCollection = this.getDataSource().connector.collection(Userinfo.app.models.Providers.modelName);
    var userLoginDynamicMatch = {}
    var providerDynamicMatch = {}
    const monthToday = new Date().getMonth() + 1;
    const dateToday = new Date().getDate();
    if (param.rl !== 2) {
      userLoginDynamicMatch['status'] = true;
      userLoginDynamicMatch['companyId'] = ObjectId(param.companyId);
      userLoginDynamicMatch['month'] = monthToday;
      userLoginDynamicMatch['date'] = dateToday;

      providerDynamicMatch['status'] = true;
      providerDynamicMatch['companyId'] = ObjectId(param.companyId);
      providerDynamicMatch['month'] = monthToday;
      providerDynamicMatch['date'] = dateToday;
      if (param.rl == 1) {
        providerDynamicMatch['userId'] = ObjectId(param.userId);
      }
      const userLoginData = await this.getBirthdaysAndAnniversariesFromUserLogin(userLoginDynamicMatch);
      const providersData = await Userinfo.app.models.Providers.getBirthdaysAndAnniversariesFromProviders(providerDynamicMatch);
      let finalObj = {
        birthdayToday: [],
        anniversaryToday: []
      }
      const updatedData = [...userLoginData, ...providersData].forEach((item) => {
        if (monthToday == item.DOBmonth && dateToday == item.DOBdate) {
          finalObj.birthdayToday.push(item);
        }
        if (monthToday == item.DOAmonth && dateToday == item.DOAdate) {
          finalObj.anniversaryToday.push(item);
        }
      })
      return finalObj;
    } else {
      userLoginDynamicMatch['status'] = true;
      userLoginDynamicMatch['companyId'] = ObjectId(param.companyId);
      userLoginDynamicMatch['month'] = monthToday;
      userLoginDynamicMatch['date'] = dateToday;

      const hierarchyResult = await Userinfo.app.models.Hierarchy.find({
        where: {
          companyId: param.companyId,
          supervisorId: param.userId,
          status: true,
        },
      });
      let userIds = hierarchyResult.map((item) => {
        return (ObjectId(item.userId));
      });

      providerDynamicMatch['status'] = true;
      providerDynamicMatch['companyId'] = ObjectId(param.companyId);
      providerDynamicMatch['userId'] = { $in: userIds };
      providerDynamicMatch['month'] = monthToday;
      providerDynamicMatch['date'] = dateToday;

      const userLoginData = await this.getBirthdaysAndAnniversariesFromUserLogin(userLoginDynamicMatch);
      const providersData = await Userinfo.app.models.Providers.getBirthdaysAndAnniversariesFromProviders(providerDynamicMatch);
      let finalObj = {
        birthdayToday: [],
        anniversaryToday: []
      }
      const updatedData = [...userLoginData, ...providersData].forEach((item) => {
        if (monthToday == item.DOBmonth && dateToday == item.DOBdate) {
          finalObj.birthdayToday.push(item);
        }
        if (monthToday == item.DOAmonth && dateToday == item.DOAdate) {
          finalObj.anniversaryToday.push(item);
        }
      })
      return finalObj;
    }

  }
  Userinfo.remoteMethod(
    'getAllUsersBirthdaysAndAnniversaries', {
    description: 'This method is used to get the Birthdays And Anniversaries for the reminder',
    accepts: [{
      arg: 'param',
      type: 'object'
    }],
    returns: {
      root: true,
      type: 'array'
    },
    http: {
      verb: 'get'
    }
  }
  );


};
