exports.getStatusMessage = (param, errorMessage) => {
      var status = [
            { "status": "Failed", "message": "Parameter Not Found !!" },
            { "status": "Failed", "message": "Mandatory Fields Required !!" },
            { "status": "Failed", "message": "There might be some problem, Please try once again !!" },
            { "status": "success", "message": "State created successfully !!" },
            { "status": "success", "message": "State updated successfully !!" },
            { "status": "success", "message": "Headquarter created successfully !!" },
            { "status": "success", "message": "Headquarter updated successfully !!" },
            { "status": "Failed", "message": "StateId not found for Headquarter mapping !!" },
            { "status": "success", "message": "Party created successfully !!" },
            { "status": "success", "message": "Party updated successfully !!" },
            { "status": "success", "message": "Product created successfully !!" },
            { "status": "success", "message": "Product updated successfully !!" },
            { "status": "success", "message": "Employee created successfully !!" },
            { "status": "success", "message": "Employee updated successfully !!" },
      ]
      if (errorMessage) {
            let error = status[param];
            error['Error Message'] = errorMessage;
            return error;
      } else {
            return status[param];
      }
}