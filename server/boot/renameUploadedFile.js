module.exports = function(app) {
    app.dataSources.storage.connector.getFilename = function(file, req, res) { 
        //console.log(file)  
        var origFilename = file.name;
        var parts = origFilename.split('.'),
        extension = parts[parts.length-1];
        var newFilename = (new Date()).getTime()+'.'+extension;
       // console.log(newFilename)
        return newFilename;
    };
};